const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        .sass('resources/assets/sass/frontend/app.scss', 'public/assets/dist/app.css')
        .sass('resources/assets/sass/backend/backend.scss', 'public/assets/dist/backend.css')
        .webpack('resources/assets/js/backend/app.js', 'public/assets/dist/backend.js')
        .webpack('resources/assets/js/backend/vue-app.js', 'public/assets/dist/backend-vue.js')
        .webpack('frontend/app.js', 'public/assets/dist/app.js')
        .version(['assets/dist/app.js', 'assets/dist/app.css', 'assets/dist/backend.js', 'assets/dist/backend.css'], 'public');
});