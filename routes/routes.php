<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web', 'FrontPageLanguage'], 'as' => 'login.'], function () {
    Route::any('keep-alive-ping', 'MainController@index');

    Route::any('access', 'AccessController@access')->name('access');

    Route::any('/', 'SplashPageController@index')->name('index');

    // Disconnect guest
    Route::get('disconnect', 'SplashPageController@disconnectGuest');

    // if device has an active connection, redirect to status page for methods given below
    Route::group(['middleware' => ['RedirectIfConnected']], function() {

        Route::group(['middleware' => ['ajax']], function() {
            // login form
            Route::post('login', 'SplashPageController@postLoginForm')->name('login');

            // Registration
            Route::post('register', 'SplashPageController@postRegisterForm')->name('register');
            Route::post('register/check-code', 'SplashPageController@postRegisterAccessCodeForm');
            Route::get('register/resend-code', 'SplashPageController@resendAccessCode');

            // Social login access code form
            Route::post('check-access-code', 'SplashPageController@postSocialAccessCodeForm');
        });

        // Social login routes
        Route::get('auth/{provider}', 'SocialLoginController@redirectToAuthProvider');
        Route::get('auth/{provider}/callback', 'SocialLoginController@handleAuthProviderCallback');
    });

    Route::any('preview/{loginPage?}/{inside?}', 'PreviewController@preview')->name('preview');

    if (\App::environment() == 'local') {
        Route::any('test', 'MainController@test')->name('test');
        Route::any('mid', 'MainController@mid')->name('mid');
    }

});

// Assets routes
Route::get('asset', 'AssetController@asset')->name('asset');
