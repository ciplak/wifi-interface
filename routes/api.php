<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api/v1', 'namespace' => 'Api', 'middleware' => ['api']], function () {

    // cmx data
    Route::group(['prefix' => 'cmx'], function () {
        Route::get('/', 'CmxController@index');
        Route::post('/', 'CmxController@store');
    });

    Route::get('guests', 'GuestController@index');

    Route::get('devices', 'DeviceController@index');

    Route::get('locations', 'LocationController@index');

    Route::group(['middleware' => ['auth:api', 'tenant']], function () {

        // settings
        Route::group(['prefix' => 'settings'], function () {
            Route::get('facebook', 'SettingController@facebook');
        });

        // device vendors
        Route::group(['prefix' => 'device-vendors'], function () {
            Route::get('list', 'DeviceVendorController@getList');

            Route::get('/', 'DeviceVendorController@index');
            Route::post('/', 'DeviceVendorController@store');
            Route::put('{id}', 'DeviceVendorController@update');
            Route::delete('{id}', 'DeviceVendorController@delete');
        });

        // device models
        Route::group(['prefix' => 'device-models'], function () {
            Route::get('types', 'DeviceModelController@types');
            Route::get('handlers', 'DeviceModelController@handlers');

            Route::get('/', 'DeviceModelController@index');
            Route::post('/', 'DeviceModelController@store');
            Route::put('{id}', 'DeviceModelController@update');
            Route::delete('{id}', 'DeviceModelController@delete');
        });

        // roles
        Route::group(['prefix' => 'roles'], function () {
            Route::get('/', 'RoleController@index');
            Route::post('/', 'RoleController@store');
            Route::put('{id}', 'RoleController@update');
            Route::delete('{id}', 'RoleController@delete');
        });

        // currencies
        Route::group(['prefix' => 'currencies'], function () {
            Route::get('/', 'CurrencyController@index');
            Route::post('/', 'CurrencyController@store');
            Route::put('{id}', 'CurrencyController@update');
            Route::delete('{id}', 'CurrencyController@delete');
        });

        // activity logs
        Route::group(['prefix' => 'activity-logs'], function () {
            Route::get('/', 'ActivityLogController@index');
        });

        // tenants
        Route::group(['prefix' => 'tenants'], function () {
            Route::get('profile', 'TenantController@getProfile');
            Route::put('profile', 'TenantController@updateProfile');
        });

        // users
        Route::group(['prefix' => 'users'], function () {
            Route::get('profile', 'UserController@profile');
            Route::put('profile', 'UserController@updateProfile');
        });

        // notifications
        Route::group(['prefix' => 'notifications'], function () {
            Route::get('recent', 'NotificationController@recent');
        });

        // reports
        Route::group(['prefix' => 'reports'], function () {
            Route::post('stats/{location?}', 'ReportController@stats');
        });

        // radius servers
        Route::group(['prefix' => 'radius-servers'], function () {
            Route::get('/', 'RadiusServerController@index');
            Route::post('/', 'RadiusServerController@store');
            Route::put('{id}', 'RadiusServerController@update');
            Route::delete('{id}', 'RadiusServerController@delete');
        });

        // service profiles
        Route::group(['prefix' => 'service-profiles'], function () {
            Route::get('/', 'ServiceProfileController@index');
//        Route::post('/', 'ServiceProfileController@store');
//        Route::put('{id}', 'ServiceProfileController@update');
//        Route::delete('{id}', 'ServiceProfileController@delete');
        });

        Route::group(['prefix' => 'media'], function() {
            Route::get('/', 'MediaController@getAll');
            Route::post('/', 'MediaController@upload');
            Route::delete('/{id}', 'MediaController@delete');
        });

        Route::group(['prefix' => 'settings'], function() {
            Route::get('/', 'SettingController@index');
            Route::put('/', 'SettingController@update');
        });

        // access codes
        Route::group(['prefix' => 'access-codes'], function () {
            Route::get('/', 'AccessCodeController@getAccessCode');
            Route::post('/', 'AccessCodeController@generate');
        });

        // sms credits
        Route::group(['prefix' => 'sms-credits'], function () {
            Route::post('{tenantId}', 'SmsCreditController@store');
            Route::delete('{tenantId}/{id}', 'SmsCreditController@delete');
        });

    });


});