<?php
/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/
if (!env('ADMIN_PORT', null) || env('ADMIN_PORT') == Request::getPort()) {

    // Admin page routes
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => ['web', 'auth', 'loadSettings', 'tenant', 'AdminPageLanguage', 'impersonate']], function () {

        Route::get('/', 'DashboardController@index')->name('index');

        Route::get('{id}/impersonate', 'UserController@impersonate')->name('impersonate');
        Route::get('stop-impersonating', 'UserController@stopImpersonate')->name('impersonate.stop');

        // advertisements
        Route::group(['as' => 'advertisement.', 'prefix' => 'advertisements'], function() {
            Route::get('/', 'AdvertisementController@index')->name('index');
            Route::get('add', 'AdvertisementController@add')->name('add');
            Route::get('{id}/edit', 'AdvertisementController@edit')->name('edit');
            Route::post('save/{id?}', 'AdvertisementController@save')->name('save');
            Route::get('delete/{id}', 'AdvertisementController@delete')->name('delete');
        });

        // proximity marketing
        Route::group(['as' => 'proximity_marketing.', 'prefix' => 'proximity_marketing'], function() {
            Route::get('/', 'ProximityMarketingController@index')->name('index');
            Route::get('add', 'ProximityMarketingController@add')->name('add');
            Route::get('{id}/edit', 'ProximityMarketingController@edit')->name('edit');
            Route::post('save/{id?}', 'ProximityMarketingController@save')->name('save');
            Route::get('delete/{id}', 'ProximityMarketingController@delete')->name('delete');
            Route::post('estimate', 'ProximityMarketingController@estimate')->name('estimate');
        });

        // campaign
        Route::group(['as' => 'campaign.', 'prefix' => 'campaign'], function () {
            Route::get('/', 'CampaignController@index')->name('index');
            Route::get('add', 'CampaignController@add')->name('add');
            Route::get('{id}/edit', 'CampaignController@edit')->name('edit');
            Route::post('save/{id?}', 'CampaignController@save')->name('save');
            Route::get('delete/{id}', 'CampaignController@delete')->name('delete');
        });

        // about
        Route::get('about', 'DashboardController@about')->name('about');
        Route::get('media', 'MediaController@index')->name('media.index')->middleware(['permission:media.manage']);
        Route::get('cmx-map', 'DashboardController@map')->name('map');

        Route::get('lang/{lang}', 'DashboardController@lang')->name('lang');
        Route::get('change-tenant/{id}', 'UserController@switchTenant')->name('switchTenant')->middleware(['permission:tenants.switch']);

        // sms providers
        Route::group(['as' => 'sms_provider.', 'prefix' => 'sms-providers', 'middleware' => ['permission:sms-providers.manage']], function() {
            Route::get('/', 'SmsProviderController@index')->name('index');
            Route::get('add', 'SmsProviderController@add')->name('add');
            Route::get('{id}/edit', 'SmsProviderController@edit')->name('edit');
            Route::post('save/{id?}', 'SmsProviderController@save')->name('save');
            Route::get('{id}/delete', 'SmsProviderController@delete')->name('delete');
        });

        // users
        Route::group(['as' => 'user.', 'prefix' => 'users', 'middleware' => ['permission:users.manage']], function() {
            Route::get('/', 'UserController@index')->name('index');
            Route::get('add', 'UserController@add')->name('add');
            Route::get('{id}/edit', 'UserController@edit')->name('edit');
            Route::post('save/{id?}', 'UserController@save')->name('save');
            Route::get('{id}/delete', 'UserController@delete')->name('delete');
            Route::get('{id}/invalidateSession/{sessionId}', 'UserController@invalidateSessions')->name('invalidateSession');
            Route::get('{id}/ban', 'UserController@banUser')->name('ban');
        });

        Route::group(['prefix' => 'reports', 'namespace' => 'Report', 'as' => 'report.', 'middleware' => ['permission:reports.view']], function () {

            // Guest
            Route::match(['GET', 'POST'], 'guests', 'GuestController@index')->name('guest');
            Route::post('guests/export', 'GuestController@export')->name('guestExport');

            // Guest sessions
            Route::match(['GET', 'POST'], 'guest-sessions', 'GuestSessionsController@index')->name('guest_sessions');
            Route::post('guest_sessions/export', 'GuestSessionsController@export')->name('guestSessionsExport');

            // Guest impressions
            Route::match(['GET', 'POST'], 'impression', 'ImpressionController@index')->name('impression');
            Route::post('impression/export', 'ImpressionController@export')->name('impression.export');

            // Presence
            Route::match(['GET', 'POST'], 'presence', 'PresenceController@index')->name('presence');
            Route::post('presence/export', 'PresenceController@export')->name('presenceExport');

            // Loyalty
            Route::match(['GET', 'POST'], 'loyalty', 'LoyaltyController@index')->name('loyalty');
            Route::post('loyalty/export', 'LoyaltyController@export')->name('loyaltyExport');

            // Demographics
            Route::match(['GET', 'POST'], 'demographics', 'DemographicsController@index')->name('demographics');
            Route::post('demographics/export', 'DemographicsController@export')->name('demographicsExport');

            // Trend Analysis
            Route::get('trend-analysis', 'TrendAnalysisController@index')->name('trendAnalysis');

            // Daily activity
            Route::get('daily-activity', 'DailyActivityController@index')->name('dailyActivity');

            // Device usage shares
            Route::get('device-usage-shares', 'DeviceUsageSharesController@index')->name('deviceUsageShares');// Device usage shares

            // Visited URLS
            Route::match(['GET', 'POST'], 'visited-urls', 'VisitedUrlController@index')->name('visited_urls');
            Route::post('visited-urls/export', 'VisitedUrlController@export')->name('visited_urls.export');
        });

        // service profiles
        Route::group(['as' => 'service_profile.', 'prefix' => 'service-profiles', 'middleware' => ['permission:service-profiles.manage']], function() {
            Route::get('/', 'ServiceProfileController@index')->name('index');
            Route::get('add', 'ServiceProfileController@add')->name('add');
            Route::get('{id}/edit', 'ServiceProfileController@edit')->name('edit');
            Route::post('save/{id?}', 'ServiceProfileController@save')->name('save');
            Route::get('{id}/delete', 'ServiceProfileController@delete')->name('delete');
        });

        // access codes
        Route::group(['as' => 'access_code.', 'prefix' => 'access-codes'], function() {

            Route::group(['middleware' => 'permission:access-codes.view|access-codes.manage'], function() {
                Route::get('/', 'AccessCodeController@index')->name('index');
                Route::post('export', 'AccessCodeExportController@export')->name('export');
            });

            Route::post('generate', 'AccessCodeController@generate')->middleware(['permission:access-codes.manage'])->name('generate');
            Route::get('{id}/delete', 'AccessCodeController@delete')->middleware(['permission:access-codes.manage'])->name('delete');
        });

        // login pages
        Route::group(['as' => 'splash_page.', 'prefix' => 'splash-pages', 'middleware' => ['permission:login-pages.manage*']], function() {
            Route::get('/', 'SplashPageController@index')->name('index');
            Route::get('add', 'SplashPageController@add')->name('add');
            Route::get('{id}/edit', 'SplashPageController@edit')->name('edit');

            Route::post('create', 'SplashPageController@post')->name('create');
            Route::post('update/{id?}', 'SplashPageController@update')->name('update');
            Route::get('{id}/delete', 'SplashPageController@delete')->name('delete');

            Route::get('clone/{id}', 'SplashPageController@createClone')->name('clone');
            Route::get('{id}/translate/{locale}', 'SplashPageController@createTranslation')->name('translate');
            Route::post('publish', 'SplashPageController@publish')->name('publish');
        });

        // settings
        Route::group(['as' => 'setting.', 'middleware' => ['permission:settings.update']], function() {
            Route::get('settings', 'SettingController@index')->name('index');
            Route::get('global-settings', 'SettingController@globalSettings')->name('global')->middleware('permission:settings.global');
            Route::post('global-settings', 'SettingController@saveGlobalSettings')->name('saveGlobal')->middleware('permission:settings.global');
            Route::post('settings/save', 'SettingController@save')->name('save');
        });

        // alert messages
        Route::get('alert-messages', 'AlertMessageController@index')->middleware('permission:alert-messages.view')->name('alert_message.index');

        // venues
        Route::group(['as' => 'venue.', 'prefix' => 'venues', 'middleware' => ['permission:venues.manage']], function() {
            Route::get('/', 'VenueController@index')->name('index');
            Route::get('add', 'VenueController@add')->name('add');
            Route::get('{id}/edit', 'VenueController@edit')->name('edit');
            Route::get('{id}/delete', 'VenueController@delete')->name('delete');
            Route::post('save/{id?}', 'VenueController@save')->name('save');
        });

        // zones
        Route::group(['as' => 'zone.', 'prefix' => 'zones', 'middleware' => ['permission:venues.manage']], function() {
            Route::get('add', 'ZoneController@add')->name('add');
            Route::get('{id}/edit', 'ZoneController@edit')->name('edit');
            Route::get('{id}/delete', 'ZoneController@delete')->name('delete');
            Route::post('save/{id?}', 'ZoneController@save')->name('save');
        });

        // devices
        Route::group(['as' => 'device.', 'prefix' => 'devices', 'middleware' => ['permission:devices.manage']], function() {
            Route::match(['GET', 'POST'], '/', 'DeviceController@index')->name('index');
            Route::get('add', 'DeviceController@add')->name('add');
            Route::get('{id}/edit', 'DeviceController@edit')->name('edit');
            Route::post('save/{id?}', 'DeviceController@save')->name('save');
            Route::get('{id}/delete', 'DeviceController@delete')->name('delete');
            Route::get('{id}/restore', 'DeviceController@restore')->name('restore');

            Route::post('import', 'DeviceImportController@import')->name('import');
            Route::get('download-import-file', 'DeviceImportController@downloadImportFile')->name('download_import_file');
        });

        // tenants
        Route::group(['as' => 'tenant.', 'prefix' => 'tenants'], function() {
            Route::get('profile', 'TenantController@profile')->middleware('permission:tenants.update-profile')->name('profile');
        });

        // activity logs
        Route::get('activity-log', 'ActivityLogController@index')->middleware('permission:activity-logs.view')->name('activity_log.index');

        // roles & permissions
        Route::group(['as' => 'role.', 'prefix' => 'roles', 'middleware' => ['permission:roles.manage']], function() {
            Route::get('/', 'RoleController@index')->name('index');
            Route::get('permissions', 'RoleController@permissions')->name('permissions');
            Route::post('permissions/update', 'RoleController@updatePermissions')->name('updatePermissions');
        });

        // logs
        Route::get('logs', 'LogViewerController@index')->name('logs')->middleware(['permission:logs.view']);

        // device vendors & models
        Route::get('device-vendors-and-models', 'DeviceVendorController@index')->name('device_vendor.index')->middleware(['permission:device_vendors.manage']);

        // currencies
        Route::get('currencies', 'CurrencyController@index')->name('currency.index')->middleware(['permission:currencies.manage']);

        // radius servers
        Route::get('radius-servers', 'RadiusServerController@index')->name('radius_server.index')->middleware(['permission:radius_servers.manage']);

        // tenants
        Route::group(['as' => 'tenant.', 'prefix' => 'tenants', 'middleware' => ['permission:tenants.manage']], function() {
            Route::get('/', 'TenantController@index')->name('index');
            Route::get('add', 'TenantController@add')->name('add');
            Route::get('{id}/edit', 'TenantController@edit')->name('edit');
            Route::post('save/{id?}', 'TenantController@save')->name('save');
            Route::get('{id}/delete', 'TenantController@delete')->name('delete');

            Route::get('delete-sms-credit/{tenantId}/{id}', 'TenantController@deleteCreditHistory')->name('delete_sms_credit');
        });

        // Reports
        if ((\App::environment() == 'local')) {
            Route::get('reports', 'ReportController@index')->name('report.index');
        }

        Route::post('reports/export', 'ReportController@export')->name('report.export')->middleware(['permission:reports.view']);

        // Guest
        Route::group(['as' => 'guest.', 'prefix' => 'guests', 'middleware' => ['permission:guests.manage']], function() {
            Route::get('/', 'GuestController@index')->name('index');
            Route::get('add', 'GuestController@add')->name('add');
            Route::get('{id}/edit', 'GuestController@edit')->name('edit');
            Route::get('{id}', 'GuestController@details')->name('details');
            Route::post('save/{id?}', 'GuestController@save')->name('save');
            Route::post('{id}/generate-access-code', 'GuestController@generateAccessCode')->name('generateAccessCode');
            Route::get('{id}/disconnect', 'GuestController@disconnect')->name('disconnect');
        });

        // User's own profile
        Route::get('users/profile', 'UserController@profile')->name('user.profile');
        Route::post('users/update-profile', 'UserController@updateProfile')->name('user.updateProfile');
    });

}
