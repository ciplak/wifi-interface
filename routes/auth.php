<?php

Route::group(['middleware' => ['web']], function() {
    // Admin login routes
    Route::get('admin/login', 'Auth\LoginController@getLogin')->name('login');
    Route::post('admin/login', 'Auth\LoginController@login');
    Route::get('admin/logout', 'Auth\LoginController@logout')->name('logout');

//    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
});