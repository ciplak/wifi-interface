<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
    /**
     * Checks that variable not NULL
     *
     * @param        $actual
     * @param string $message
     */
    public function assertNull($actual, $message = '')
    {
        return parent::assertNull($actual, $message);
    }

    /**
     * Checks that variable is not NULL
     *
     * @param        $actual
     * @param string $message
     */
    public function assertNotNull($actual, $message = '')
    {
        return parent::assertNotNull($actual, $message);
    }

    /**
     * Checks that two variables are equal.
     *
     * @param        $expected
     * @param        $actual
     * @param string $message
     *
     * @return mixed
     */
    public function assertEquals($expected, $actual, $message = '')
    {
        return parent::assertEquals($expected, $actual, $message);
    }

    /**
     * Save page source
     *
     * @param string $filename
     */
    public function savePage($filename = 'page')
    {
        if (!trim($filename)) {
            $filename = 'page';
        }

        $this->getModule('Laravel5')->_savePageSource(codecept_output_dir() . $filename . '.html');
    }
}
