<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://wifi.dev';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function disableExceptionHandling()
    {
        app()->instance(\App\Exceptions\Handler::class, new class extends \App\Exceptions\Handler
        {
            protected $dontReport = [
                \Illuminate\Auth\AuthenticationException::class,
                \Illuminate\Auth\Access\AuthorizationException::class,
                \Symfony\Component\HttpKernel\Exception\HttpException::class,
                \Illuminate\Database\Eloquent\ModelNotFoundException::class,
                \Illuminate\Session\TokenMismatchException::class,
                \Illuminate\Validation\ValidationException::class,
            ];

            public function __construct()
            {

            }

            public function report(Exception $e)
            {

            }

            public function render($request, Exception $e)
            {
                foreach ($this->dontReport as $item) {
                    if ($e instanceof $item) {
                        return;
                    }
                }

                throw $e;
            }
        });
    }
}
