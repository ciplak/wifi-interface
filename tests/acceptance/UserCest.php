<?php

use App\Models\User;

class UserCest
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
        info('-- before: ' . request()->url());
    }

    public function _after(AcceptanceTester $I)
    {

    }

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See user index page');
        $I->amOnRoute('admin.user.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.user.index');
        $I->see(trans('user.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPageFormValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test user add page form validation');
        $I->amOnRoute('admin.user.add');
        $I->submitForm('form', []);
        $I->canSeeCurrentRouteIs('admin.user.add');
        $I->seeFormErrorMessages([
            'name'     => null,
            'email'    => null,
            'role'     => null,
            'password' => null,
        ]);
    }

    public function testAddPageFormNameValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test user add page form name validation');
        $I->amOnRoute('admin.user.add');
        $I->submitForm('form', [
            'name' => 'test',
        ]);
        $I->canSeeCurrentRouteIs('admin.user.add');
        $I->seeFormErrorMessage('name', null);
    }

    public function testAddPageVenueRoleValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test user add page venue role validation');
        $I->amOnRoute('admin.user.add');
        $I->submitForm('form', [
            'name'                  => $this->faker->name,
            'email'                 => $this->faker->email,
            'password'              => $password = $this->faker->randomLetter,
            'password_confirmation' => $password,
            'role'                  => 'venue',
        ]);
        $I->canSeeCurrentRouteIs('admin.user.add');
        $I->seeFormErrorMessage('property_id');
    }

    public function testAddPageFrontDeskRoleValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test user add page front desk role validation');
        $I->amOnRoute('admin.user.add');
        $I->submitForm('form', [
            'name'                  => $this->faker->name,
            'email'                 => $this->faker->email,
            'password'              => $password = $this->faker->word,
            'password_confirmation' => $password,
            'role'                  => 'front-desk',
        ]);
        $I->canSeeCurrentRouteIs('admin.user.add');
        $I->seeFormErrorMessage('property_id');
    }

    public function testAddPagePasswordConfirmationValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test user add page password validation');
        $I->amOnRoute('admin.user.add');
        $I->submitForm('form', [
            'name'                  => $this->faker->name,
            'email'                 => $this->faker->email,
            'password'              => $this->faker->name,
            'password_confirmation' => $this->faker->name,
        ]);
        $I->canSeeCurrentRouteIs('admin.user.add');
        $I->seeFormErrorMessage('password', trans('validation.confirmed', ['attribute' => 'password']));
    }

    public function testAddPageNameLengthValidationFail(AcceptanceTester $I)
    {
        $I->wantTo('Test user add page name validation fail');
        $I->amOnRoute('admin.user.add');
        $I->submitForm('form', [
            'name'                  => 'test',
            'email'                 => $this->faker->email,
            'role'                  => 'tenant',
            'password'              => $password = '12345678',
            'password_confirmation' => $password,
        ]);
        $I->canSeeCurrentRouteIs('admin.user.add');
        $I->seeFormErrorMessage('name', trans('validation.min.string', ['attribute' => 'name', 'min' => 5]));
    }

    public function testAddPageNameLengthValidationSuccess(AcceptanceTester $I)
    {
        $I->wantTo('Test user add page name validation success');
        $I->amOnRoute('admin.user.add');
        $I->submitForm('form', [
            'name'                  => 'tester',
            'email'                 => $this->faker->email,
            'role'                  => 'tenant',
            'password'              => $password = '12345678',
            'password_confirmation' => $password,
        ]);
        $I->canSeeCurrentRouteIs('admin.user.index');
        $I->canSeeResponseCodeIs(200);
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('Test user edit page');

        /** @var User $user */
        $user = factory(User::class)->create();

        $I->amOnRoute('admin.user.edit', [$user]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'name'  => $user->name,
            'email' => $user->email,
            'role'  => $user->role,
        ]);
    }

    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Test user update');

        /** @var \App\Models\Venue $venue */
        $venue = factory(\App\Models\Venue::class)->create();

        /** @var User $user */
        $user = factory(User::class)->create();

        /** @var \App\Models\Role $role */
        $role = factory(\App\Models\Role::class)->create();

        $attributes = [
            'name'        => $this->faker->name,
            'email'       => $this->faker->email,
            'role'        => $role->name,
            'property_id' => $venue->id,
        ];

        $I->amOnRoute('admin.user.edit', [$user]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $attributes);
        $I->seeCurrentRouteIs('admin.user.index', [$user]);
        $I->seeResponseCodeIs(200);

        /** @var User $recordToCheck */
        $recordToCheck = $I->grabRecord(User::class, [
            'tenant_id' => $user->tenant_id,
            'id'        => $user->id,
        ]);


        $I->assertEquals($recordToCheck->getAttribute('name'), $attributes['name']);
        $I->assertEquals($recordToCheck->getAttribute('email'), $attributes['email']);
        $I->assertEquals($recordToCheck->getAttribute('property_id'), $attributes['property_id']);
    }
}