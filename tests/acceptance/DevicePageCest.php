<?php

use App\Models\Device;
use App\Models\DeviceModel;
use App\Models\Zone;

class DevicePageCest
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {

    }

    /**
     * @param AcceptanceTester $I
     *
     * @return Zone
     */
    private function createLocation(AcceptanceTester $I)
    {
        /** @var \App\Models\Venue $venue */
        $venue = $I->haveRecord(\App\Models\Venue::class, [
            'parent_id' => \Auth::user()->tenant_id,
            'name'      => 'Test venue',
        ]);

        /** @var Zone $zone */
        $zone = $I->haveRecord(Zone::class, [
            'parent_id' => $venue->id,
            'name'      => 'Test zone for ' . $venue->name,
        ]);

        return $zone;
    }

    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See device index page');
        $I->amOnRoute('admin.device.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.device.index');
        $I->see(trans('device.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPageFormValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test device add page form validation');
        $I->amOnRoute('admin.device.add');
        $I->submitForm('form', []);
        $I->canSeeCurrentRouteIs('admin.device.add');
        $I->seeFormErrorMessages([
            'property_id'     => null,
            'name'            => null,
            'ip'              => null,
            'mac'             => null,
            'device_model_id' => null,
        ]);
    }

    public function testAddPageIpValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test device add page form ip validation');

        /** @var Zone $location */
        $location = factory(\App\Models\Zone::class)->create();

        /** @var DeviceModel $deviceModel */
        $deviceModel = factory(\App\Models\DeviceModel::class)->create();

        $I->amOnRoute('admin.device.add');
        $I->submitForm('form', [
            'property_id'     => $location->hashed_id,
            'name'            => 'Test Device',
            'description'     => 'Device test description',
            'device_model_id' => $deviceModel->id,
            'ip'              => 'Invalid IP',
            'mac'             => $this->faker->macAddress,
        ]);
        $I->canSeeCurrentRouteIs('admin.device.add');
        $I->seeFormErrorMessage('ip', trans('validation.ip', ['attribute' => 'ip']));
    }

    public function testAddPageMacValidation(AcceptanceTester $I)
    {
        $I->wantTo('test device add page form mac validation');

        /** @var Zone $location */
        $location = factory(\App\Models\Zone::class)->create();

        /** @var DeviceModel $deviceModel */
        $deviceModel = factory(\App\Models\DeviceModel::class)->create();

        $I->amOnRoute('admin.device.add');
        $I->submitForm('form', [
            'property_id'     => $location->hashed_id,
            'name'            => 'Test Device',
            'description'     => 'Device test description',
            'device_model_id' => $deviceModel->id,
            'ip'              => $this->faker->ipv4,
            'mac'             => 'Invalid Mac',
        ]);
        $I->canSeeCurrentRouteIs('admin.device.add');
        $I->seeFormErrorMessage('mac', trans('validation.mac', ['attribute' => 'mac']));
    }

    public function testAddPageDuplicateIpValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test device add page form duplicate ip');

        $ip = $this->faker->ipv4;
        $mac = $this->faker->macAddress;
        /** @var Zone $location */
        $location = factory(\App\Models\Zone::class)->create();

        /** @var DeviceModel $deviceModel */
        $deviceModel = factory(\App\Models\DeviceModel::class)->create();

        $I->haveRecord(Device::class, [
            'tenant_id'       => \Auth::user()->tenant_id,
            'property_id'     => $location->id,
            'name'            => 'Test Device',
            'description'     => 'Device test description',
            'device_model_id' => $deviceModel->id,
            'ip'              => $ip,
            'mac'             => $mac,
        ]);
        $I->amOnRoute('admin.device.add');
        $I->submitForm('form', [
            'property_id'     => $location->hashed_id,
            'name'            => 'Test Device',
            'description'     => 'Device test description',
            'device_model_id' => $deviceModel->id,
            'ip'              => $ip,
            'mac'             => $this->faker->macAddress,
        ]);
        $I->canSeeCurrentRouteIs('admin.device.add');
        $I->seeFormErrorMessage('ip', trans('validation.unique', ['attribute' => 'ip']));
    }

    public function testAddPageDuplicateMacValidation(AcceptanceTester $I)
    {
        $I->wantTo('test device add page form duplicate mac address');

        $ip = $this->faker->ipv4;
        $mac = $this->faker->macAddress;

        /** @var Zone $location */
        $location = factory(\App\Models\Zone::class)->create();

        /** @var DeviceModel $deviceModel */
        $deviceModel = factory(\App\Models\DeviceModel::class)->create();

        $I->haveRecord(Device::class, [
            'tenant_id'       => \Auth::user()->tenant_id,
            'property_id'     => $location->id,
            'name'            => 'Test Device',
            'description'     => 'Device test description',
            'device_model_id' => $deviceModel->id,
            'ip'              => $ip,
            'mac'             => $mac,
        ]);
        $I->amOnRoute('admin.device.add');
        $I->submitForm('form', [
            'property_id'     => $location->hashed_id,
            'name'            => 'Test Device',
            'description'     => 'Device test description',
            'device_model_id' => $deviceModel->id,
            'ip'              => $this->faker->ipv4,
            'mac'             => $mac,
        ]);
        $I->canSeeCurrentRouteIs('admin.device.add');
        $I->seeFormErrorMessage('mac', trans('validation.unique', ['attribute' => 'mac']));
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('test device add page');

        $ip = $this->faker->ipv4;
        $mac = $this->faker->macAddress;

        /** @var Zone $location */
        $location = factory(\App\Models\Zone::class)->create();

        /** @var DeviceModel $deviceModel */
        $deviceModel = factory(\App\Models\DeviceModel::class)->create();

        $I->amOnRoute('admin.device.add');
        $I->submitForm('form', [
            'property_id'     => $location->hashed_id,
            'name'            => 'Test Device',
            'description'     => 'Device test description',
            'device_model_id' => $deviceModel->id,
            'ip'              => $ip,
            'mac'             => $mac,
        ]);
        $I->canSeeCurrentRouteIs('admin.device.index');
        $I->seeRecord('devices', [
            'ip'              => $ip,
            'mac'             => $mac,
            'device_model_id' => $deviceModel->id,
            'property_id'     => $location->id,
        ]);
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('See device edit page');

        /** @var Device $device */
        $device = factory(\App\Models\Device::class)->create();

        $I->amOnRoute('admin.device.edit', [$device]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'property_id'     => hashids_encode($device->property_id),
            'name'            => $device->name,
            'description'     => $device->description,
            'device_model_id' => $device->device_model_id,
            'ip'              => $device->ip,
            'mac'             => $device->mac,
        ]);
    }

    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Update device');

        /** @var Device $device */
        $device = factory(\App\Models\Device::class)->create();

        $attributes = [
            'name'        => $this->faker->name,
            'description' => $this->faker->text(),
            'ip'          => $this->faker->ipv4,
            'mac'         => $this->faker->macAddress,
        ];

        $I->amOnRoute('admin.device.edit', [$device]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $attributes);
        $I->amOnRoute('admin.device.index');
        $I->seeResponseCodeIs(200);

        /** @var array $recordToCheck */
        $recordToCheck = $I->grabRecord('devices', [
            'tenant_id' => \Auth::user()->tenant_id,
            'id'        => $device->id,
        ]);

        foreach ($attributes as $key => $value) {
            $I->assertEquals($recordToCheck[$key], $value);
        }
    }

    public function testDelete(AcceptanceTester $I)
    {
        $I->wantTo('Delete a device');

        /** @var Device $device */
        $device = factory(\App\Models\Device::class)->create();

        $I->amOnRoute('admin.device.index');
        $I->amOnRoute('admin.device.delete', [$device]);
        $I->amOnRoute('admin.device.index');
        $I->seeResponseCodeIs(200);

        /** @var array $deviceToCheck */
        $deviceToCheck = $I->grabRecord('devices', [
            'tenant_id'       => \Auth::user()->tenant_id,
            'property_id'     => $device->property_id,
            'name'            => $device->name,
            'description'     => $device->description,
            'device_model_id' => $device->device_model_id,
            'ip'              => $device->ip,
            'mac'             => $device->mac,
        ]);

        $I->assertNotNull($deviceToCheck['deleted_at']);
    }

    public function testRestore(AcceptanceTester $I)
    {
        $I->wantTo('Restore a device');

        /** @var Device $device */
        $device = factory(\App\Models\Device::class)->create();

        $I->amOnRoute('admin.device.index');
        $I->amOnRoute('admin.device.delete', [$device]);
        $I->amOnRoute('admin.device.index');
        $I->seeResponseCodeIs(200);
        $I->amOnRoute('admin.device.restore', [$device]);
        $I->amOnRoute('admin.device.index');
        $I->seeResponseCodeIs(200);

        /** @var array $deviceToCheck */
        $deviceToCheck = $I->grabRecord('devices', [
            'tenant_id'       => \Auth::user()->tenant_id,
            'property_id'     => $device->property_id,
            'name'            => $device->name,
            'description'     => $device->description,
            'device_model_id' => $device->device_model_id,
            'ip'              => $device->ip,
            'mac'             => $device->mac,
        ]);

        $I->assertNull($deviceToCheck['deleted_at']);
    }
}