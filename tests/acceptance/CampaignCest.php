<?php

use App\Models\Campaign;

class CampaignCest
{
    private $faker;


    /**
     * CampaignCest constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See campaign index page');
        $I->amOnRoute('admin.campaign.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.campaign.index');
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('See campaign add page');
        $I->amOnRoute('admin.campaign.add');
        $I->seeResponseCodeIs(200);
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testAddPageFillTitleValidation(AcceptanceTester $I)
    {
        $I->wantTo('See campaign add page fill title and select parameter select date time');
        $I->amOnRoute('admin.campaign.add');
        $I->submitForm('form', [
            'parameter_type_date' => $this->faker->date,
        ]);
        $I->canSeeCurrentRouteIs('admin.campaign.add');
        $I->seeFormErrorMessage('title', trans('validation.required', ['attribute' => 'title']));
        $I->dontSee('The parameter type date field is required when parameter type is 1');
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testAddPageDoNotFillTitleValidation(AcceptanceTester $I)
    {
        $I->wantTo('See campaign add page don\'t fill title');
        $I->amOnRoute('admin.campaign.add');
        $I->submitForm('form', [
            'title' => $this->faker->title,
        ]);
        $I->canSeeCurrentRouteIs('admin.campaign.add');
        $I->dontSee('The title field is required.');
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testAddPagFillFormValidation(AcceptanceTester $I)
    {
        $I->wantTo('See campaign add page fill title');
        $I->amOnRoute('admin.campaign.add');
        $I->submitForm('form', [
            'title'       => $this->faker->title,
            'reengage'    => $this->faker->title,
            'visit_count' => $this->faker->title,
        ]);
        $I->canSeeCurrentRouteIs('admin.campaign.add');
        $I->see(trans('validation.integer', ['attribute' => 'reengage']));
        $I->see(trans('validation.integer', ['attribute' => 'visit count']));

        $I->dontSee('The title field is required.');
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('See campaign edit page');

        /** @var Campaign $campaign */
        $campaign = factory(Campaign::class)->create();

        $I->amOnRoute('admin.campaign.edit', [$campaign]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'title'               => $campaign->title,
            'parameter_type'      => $campaign->parameter_type,
            'parameter_type_date' => $campaign->parameter_type_date,
            'parameter_type_time' => array_search($campaign->parameter_type_time, config('times.time')),
        ]);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Test campaign update');

        /** @var Campaign $campaign */
        $campaign = factory(Campaign::class)->create();

        $attributes = [
            'title'               => $this->faker->title,
            'parameter_type'      => Campaign::PARAMETER_TYPE_PERIODIC,
            'parameter_type_date' => $this->faker->dateTimeThisMonth->format('Y-m-d'),
            'parameter_type_time' => rand(0, 48),
        ];

        $I->amOnRoute('admin.campaign.edit', [$campaign]);

        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $attributes);
        $I->amOnRoute('admin.campaign.index');
        $I->seeResponseCodeIs(200);

        /** @var Campaign $recordToCheck */
        $recordToCheck = $I->grabRecord(Campaign::class, [
            'tenant_id' => \Auth::user()->tenant_id,
            'id'        => $campaign->id,
        ]);

        $I->assertEquals($recordToCheck->title, $attributes['title']);
        $I->assertEquals($recordToCheck->parameter_type, $attributes['parameter_type']);
        $I->assertEquals($recordToCheck->parameter_type_date, $attributes['parameter_type_date']);
        $I->assertEquals(Carbon\Carbon::parse($recordToCheck->parameter_type_time)->format('H:i'), config('times.time.' . $attributes['parameter_type_time']));
    }

    public function _testDelete(AcceptanceTester $I)
    {
        $I->wantTo('Test campaign delete');

        /** @var Campaign $campaign */
        $campaign = factory(Campaign::class)->create();

        $I->amOnRoute('admin.campaign.delete', [$campaign]);
        $I->amOnRoute('admin.campaign.index');
        $I->seeResponseCodeIs(200);

        $campaignToCheck = $I->grabRecord('campaigns', [
            'title'               => $campaign->title,
            'parameter_type'      => $campaign->parameter_type,
            'parameter_type_date' => $campaign->parameter_type_date,
            'parameter_type_time' => $campaign->parameter_type_time,
        ]);


        $I->assertNotNull($campaignToCheck['created_at']);
    }
}