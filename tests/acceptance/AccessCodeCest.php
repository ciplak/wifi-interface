<?php

use Faker\Factory;

class AccessCodeCest
{
    public $faker;

    /**
     * AccessCodeCest constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See access codes index page');
        $I->amOnRoute('admin.access_code.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.access_code.index');
        $I->see(trans('access_code.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('See access code index page');
        $I->amOnRoute('admin.access_code.index');
        $I->seeResponseCodeIs(200);
        $I->see(trans('access_code.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    // TODO Codeception necessary
}