<?php

use App\Models\Venue;
use App\Models\Zone;

class ZoneCest
{
    public $faker;

    /**
     * ZoneCest constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See zone index page');
        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.venue.index');
//        $I->see(trans('zone.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddVenueValidateLocation(AcceptanceTester $I)
    {
        $I->wantTo('Add venue validate for location');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->states('with-splash-pages')->create();

        $I->amOnRoute('admin.zone.add', ['venue' => $venue]);
        $I->canSeeCurrentRouteIs('admin.zone.add');
        $I->see(trans('zone.page_title'));
        $I->seeInFormFields('form', [
            'venue_id' => $venue->hashed_id,
        ]);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testAddPagePreLoginPageValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test location add page form pre-login page validation');
        $I->amOnRoute('admin.zone.add');

        /** @var \App\Models\SplashPage $preLogin */
        $preLogin = factory(\App\Models\SplashPage::class)->create();

        $I->submitForm('form', [
            'login_page_id' => $preLogin->id,
            'name'          => 'test pre login',
        ]);
        $I->canSeeCurrentRouteIs('admin.zone.add');

        $I->seeRecord('login_pages', [
            'id' => $preLogin->id,
        ]);

        $I->dontSee('The name field is required');
    }

    public function testAddZone(AcceptanceTester $I)
    {
        $I->wantTo('Add zone');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->states('with-splash-pages')->create();

        /** @var \App\Models\ServiceProfile $serviceProfile */
        $serviceProfile = factory(\App\Models\ServiceProfile::class)->create();

        /** @var \App\Models\SplashPage $preLogin */
        $preLogin = factory(\App\Models\SplashPage::class)->create();

        /** @var \App\Models\SplashPage $postLogin */
        $postLogin = factory(\App\Models\SplashPage::class)->create([
            'type' => \App\Models\SplashPage::TYPE_ONLINE,
        ]);

        $I->amOnRoute('admin.zone.add', ['venue' => $venue]);
        $I->canSeeCurrentRouteIs('admin.zone.add');
        $I->see(trans('zone.page_title'));

        $I->submitForm('form', [
            'venue_id'                => $venue->hashed_id,
            'name'                    => $this->faker->name,
            'description'             => $this->faker->text,
            'login_page_id'           => $preLogin->id,
            'post_login_page_id'      => $postLogin->id,
            'default_service_profile' => $serviceProfile->id,
        ]);

        $I->canSeeCurrentRouteIs('admin.venue.index');
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testIndexPageFilter(AcceptanceTester $I)
    {
        $I->wantTo('Test zone index page filter');

        /** @var Zone $zone */
        $zone = factory(Zone::class)->states('with-splash-pages')->create();

        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.venue.index');
        $I->see(trans('venue.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
        $I->submitForm('.panel-actions form', [
            'name' => $zone->name,
        ]);

        $I->canSeeCurrentRouteIs('admin.venue.index');
        $I->seeResponseCodeIs(200);
        $I->see($zone->name);
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('See zone edit page');

        /** @var Zone $zone */
        $zone = factory(Zone::class)->states('with-splash-pages')->create();

        $I->amOnRoute('admin.zone.edit', [$zone]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'venue_id'           => hashids_encode($zone->parent_id),
            'name'               => $zone->name,
            'description'        => $zone->description,
            'login_page_id'      => $zone->login_page_id,
            'post_login_page_id' => $zone->post_login_page_id,
        ]);
    }

    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Test zone update');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->states('with-splash-pages')->create();

        /** @var Zone $zone */
        $zone = factory(Zone::class)->states('with-splash-pages')->create();

        /** @var \App\Models\ServiceProfile $serviceProfile */
        $serviceProfile = factory(\App\Models\ServiceProfile::class)->create();

        /** @var \App\Models\SplashPage $preLogin */
        $preLogin = factory(\App\Models\SplashPage::class)->create();

        /** @var \App\Models\SplashPage $postLogin */
        $postLogin = factory(\App\Models\SplashPage::class)->create([
            'type' => \App\Models\SplashPage::TYPE_ONLINE,
        ]);

        $formAttributes = [
            'venue_id'                => $venue->hashed_id,
            'name'                    => $this->faker->name,
            'description'             => $this->faker->text,
            'default_service_profile' => $serviceProfile->id,
            'login_page_id'           => $preLogin->id,
            'post_login_page_id'      => $postLogin->id,
        ];

        $I->amOnRoute('admin.zone.edit', [$zone]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $formAttributes);
        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);

        /** @var \App\Models\Zone $recordToCheck */
        $recordToCheck = $I->grabRecord(\App\Models\Zone::class, [
            'id' => $zone->id,
        ]);

        $formAttributes = (object)$formAttributes;
        $I->assertEquals($recordToCheck->parent_id, hashids_decode($formAttributes->venue_id));
        $I->assertEquals($recordToCheck->name, $formAttributes->name);
        $I->assertEquals($recordToCheck->description, $formAttributes->description);
        $I->assertEquals($recordToCheck->login_page_id, $formAttributes->login_page_id);
        $I->assertEquals($recordToCheck->post_login_page_id, $formAttributes->post_login_page_id);
    }

    public function testDelete(AcceptanceTester $I)
    {
        $I->wantTo('Test zone delete');

        /** @var Zone $zone */
        $zone = factory(Zone::class)->states('with-splash-pages')->create();

        $I->amOnRoute('admin.zone.delete', [$zone]);
        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);

        /** @var Zone $zoneToCheck */
        $zoneToCheck = $I->grabRecord('properties', [
            'id' => $zone->id,
        ]);
        $I->assertNotNull($zoneToCheck['deleted_at']);
    }
}