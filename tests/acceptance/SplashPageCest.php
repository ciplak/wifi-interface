<?php

use Faker\Factory;

class SplashPageCest
{
    public $faker;

    /**
     * SplashPageCest constructor.
     */
    public function __construct()
    {
        /** @var TYPE_NAME $faker */
        $this->faker = Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See splash page index page');
        $I->amOnRoute('admin.splash_page.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.splash_page.index');
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('See splash add page');
        $I->amOnRoute('admin.splash_page.add');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.splash_page.add');
        $I->see(trans('splash_page.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPageNameValidation(AcceptanceTester $I)
    {
        $I->wantTo('See splash add page fill name');
        $I->amOnRoute('admin.splash_page.add');
        $I->submitForm('form', []);
        $I->canSeeCurrentRouteIs('admin.splash_page.add');
        $I->see(trans('validation.required', ['attribute' => 'name']));
    }

    public function _testAddPreLogin(AcceptanceTester $I)
    {
        // TODO DO!
        $I->wantTo('Add splash page');
        $I->amOnRoute('admin.splash_page.add');
        $I->canSeeCurrentRouteIs('admin.splash_page.add');
        $I->see(trans('splash_page.page_title'));

        $I->submitForm('form', [
            'name'        => $this->faker->name,
            'type'        => \App\Models\SplashPage::TYPE_OFFLINE,
            'settings' => [],
            'fields' => [],
            'field-list' => '',
            'registration_fields' => [],

            'loginFields' => [
                'email' => [
                    'enabled' => 1,
                ],
            ],
        ]);

        $I->canSeeCurrentRouteIs('admin.splash_page.index');
    }


    public function _testEditPreLoginPage(AcceptanceTester $I)
    {
        // TODO DO!
        $I->wantTo('See splash page pre login edit page');

        /** @var \App\Models\SplashPage $login */
        $login = factory(\App\Models\SplashPage::class)->create();

        $I->amOnRoute('admin.splash_page.edit', [$login]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'name' => $login->name,
        ]);
    }

    public function _testUpdatePreLogin(AcceptanceTester $I)
    {
        // TODO DO!
        $I->wantTo('Test splash page pre login update');

        /** @var \App\Models\SplashPage $login */
        $login = factory(\App\Models\SplashPage::class)->create();

        $formAttributes = [
            'name'          => $this->faker->name,
            'login_page_id' => $login->id,
        ];

        $I->amOnRoute('admin.splash_page.edit', [$login]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $formAttributes);
        $I->amOnRoute('admin.splash_page.index');
        $I->seeResponseCodeIs(200);

        /** @var \App\Models\Venue $recordToCheck */
        $recordToCheck = $I->grabRecord(\App\Models\Property::class, [
            'id' => $login->id,
        ]);

        $formAttributes = (object)$formAttributes;
        $I->assertEquals($recordToCheck->name, $formAttributes->name);
    }

    public function _testDelete(AcceptanceTester $I)
    {
        // TODO DO!
        $I->wantTo('Test splash page pre login delete');

        /** @var \App\Models\SplashPage $login */
        $login = factory(\App\Models\SplashPage::class)->create();

        $I->amOnRoute('admin.splash_page.delete', [$login]);
        $I->amOnRoute('admin.splash_page.index');
        $I->seeResponseCodeIs(200);

        /** @var \App\Models\Property $preLoginToCheck */
        $preLoginToCheck = $I->grabRecord('login_pages', [
            'id' => $login->id,
        ]);
        $I->assertNotNull($preLoginToCheck['deleted_at']);
    }
}