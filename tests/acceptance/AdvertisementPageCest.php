<?php

use App\Models\Advertisement;

class AdvertisementPageCest
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _after(AcceptanceTester $I)
    {

    }

    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See advertisement index page');
        $I->amOnRoute('admin.advertisement.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.advertisement.index');
        $I->see(trans('advertisement.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPageFormValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test advertisement add page form validation');
        $I->amOnRoute('admin.advertisement.add');
        $I->submitForm('form', [
            'type' => Advertisement::TYPE_TEXT,
        ]);
        $I->canSeeCurrentRouteIs('admin.advertisement.add');
        $I->seeFormErrorMessages([
            'title'      => null,
            'body'       => null,
            'start_date' => null,
            'end_date'   => null,
        ]);
    }

    public function testAddTextBanner(AcceptanceTester $I)
    {
        $I->wantTo('Test adding text banner advertisement');

        $date = $this->faker->dateTimeThisMonth;

        $attributes = [
            'tenant_id'  => \Auth::user()->tenant_id,
            'title'      => 'Text Banner Test',
            'type'       => Advertisement::TYPE_TEXT,
            'body'       => $this->faker->realText(),
            'url'        => $this->faker->url,
            'start_date' => $date->format('Y-m-d'),
            'end_date'   => $date->add(new DateInterval('P1W'))->format('Y-m-d'),
            'status'     => Advertisement::STATUS_PUBLISHED,
        ];

        $I->amOnRoute('admin.advertisement.add');
        $I->submitForm('form', $attributes);
        $I->canSeeCurrentRouteIs('admin.advertisement.index');
        $I->seeRecord('advertisements', $attributes);
    }

    public function testAddImageBanner(AcceptanceTester $I)
    {
        $I->wantTo('Test adding image banner advertisement');

        $date = $this->faker->dateTimeThisMonth;

        $attributes = [
            'tenant_id'  => \Auth::user()->tenant_id,
            'title'      => 'Image Banner Test',
            'type'       => Advertisement::TYPE_IMAGE,
            'url'        => $this->faker->url,
            'start_date' => $date->format('Y-m-d'),
            'end_date'   => $date->add(new DateInterval('P1W'))->format('Y-m-d'),
            'status'     => Advertisement::STATUS_PUBLISHED,
        ];

        $I->amOnRoute('admin.advertisement.add');
        $I->attachFile('input[name=image]', 'test.jpg');
        $I->submitForm('form', $attributes);
        $I->canSeeCurrentRouteIs('admin.advertisement.index');
        $I->seeRecord('advertisements', $attributes);
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('Test advertisement edit page');

        $advertisement = factory(Advertisement::class)->create();

        $I->amOnRoute('admin.advertisement.edit', [$advertisement]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'title'      => $advertisement->title,
            'type'       => $advertisement->type,
            'url'        => $advertisement->url,
            'start_date' => $advertisement->start_date->format('Y-m-d'),
            'end_date'   => $advertisement->end_date->format('Y-m-d'),
            'status'     => $advertisement->status,
        ]);
    }

    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Test advertisement update');

        $advertisement = factory(Advertisement::class)->create();

        $date = $this->faker->dateTimeThisMonth;
        $attributes = [
            'title'      => $this->faker->text(),
            'type'       => Advertisement::TYPE_TEXT,
            'body'       => $this->faker->realText(),
            'url'        => $this->faker->url,
            'start_date' => $date->format('Y-m-d'),
            'end_date'   => $date->add(new DateInterval('P5D'))->format('Y-m-d'),
            'status'     => Advertisement::STATUS_DRAFT,
        ];

        $I->amOnRoute('admin.advertisement.edit', [$advertisement]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $attributes);
        $I->amOnRoute('admin.advertisement.index');
        $I->seeResponseCodeIs(200);

        $recordToCheck = $I->grabRecord('advertisements', [
            'tenant_id' => \Auth::user()->tenant_id,
            'id'        => $advertisement->id,
        ]);

        foreach ($attributes as $key => $value) {
            $I->assertEquals($recordToCheck[$key], $value);
        }
    }

    public function testDelete(AcceptanceTester $I)
    {
        $I->wantTo('Test advertisement delete');

        $advertisement = factory(Advertisement::class)->create();
        $I->amOnRoute('admin.advertisement.delete', [$advertisement]);
        $I->amOnRoute('admin.advertisement.index');
        $I->seeResponseCodeIs(200);

        $I->dontSeeRecord('advertisements', [
            'tenant_id' => \Auth::user()->tenant_id,
            'id'        => $advertisement->id,
        ]);
    }
}