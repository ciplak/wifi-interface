<?php

use App\Models\Guest;

class GuestPageCest
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
        info('-- before: ' . request()->url());
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    /**
     * @param Guest $guest
     *
     * @return \App\Models\WifiSession
     */
    private function createSession($guest)
    {
        // todo replace with factory
        $date = $this->faker->dateTime;

        $now = \Carbon\Carbon::now();

        $data = [
            'tenant_id'         => \Auth::user()->tenant_id,
            'venue_id'          => $guest->venue_id,
            'timestamp'         => $date,
            'start_time'        => $now,
            'end_time'          => $now->addMinutes(15),
            'guest_device_id'   => null,
            'duration'          => rand(1, 8) * 15 * 60,
            'downloaded_data'   => 0,
            'uploaded_data'     => 0,
            'guest_id'          => $guest->id,
            'last_updated_time' => date('Y-m-d H:i:s'),
        ];

        return \App\Models\WifiSession::create($data);
    }

    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See guest index page');
        $I->amOnRoute('admin.guest.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.guest.index');
        $I->see(trans('guest.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testIndexPageFilter(AcceptanceTester $I)
    {
        $I->wantTo('Test guest index page filter');

        /** @var Guest $guest */
        $guest = factory(Guest::class)->create();

        $I->amOnRoute('admin.guest.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.guest.index');
        $I->see(trans('guest.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');

        $I->submitForm('.panel-actions form', [
            'filter_venue' => $guest->venue->hashed_id,
            'name'         => $guest->name,
            'email'        => $guest->email,
            'phone'        => $guest->phone,
        ]);

        $I->canSeeCurrentRouteIs('admin.guest.index');
        $I->seeResponseCodeIs(200);
        $I->see($guest->name);
        $I->see($guest->email);
        $I->see($guest->phone);
    }

    public function testDetailsPage(AcceptanceTester $I)
    {
        $I->wantTo('See guest details page');

        /** @var Guest $guest */
        $guest = factory(Guest::class)->create();

        $I->amOnRoute('admin.guest.details', [$guest]);
        $I->seeResponseCodeIs(200);
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
        $I->see($guest->name);
        $I->see($guest->username);
//        $I->see($guest->birthday);
//        $I->see($guest->phone);
    }

    public function testAddPageLocationValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test guest add page location validation');
        $I->amOnRoute('admin.guest.add');
        $I->submitForm('form', []);
        $I->canSeeCurrentRouteIs('admin.guest.add');
        $I->seeFormErrorMessage('venue_id', trans('validation.required', ['attribute' => trans('validation.attributes.venue_id')]));
    }

    public function testAddPageNameValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test guest add page name validation');

        $venue = factory(\App\Models\Venue::class)->create();

        $I->amOnRoute('admin.guest.add');
        $I->submitForm('form', [
            'location_id' => $venue->hashed_id,
            'name'        => 'ali',
        ]);
        $I->canSeeCurrentRouteIs('admin.guest.add');
        $I->seeFormErrorMessage('name', trans('validation.min.string', ['attribute' => 'name', 'min' => 5]));
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('Test guest edit page');

        /** @var Guest $guest */
        $guest = factory(Guest::class)->create();

        $I->amOnRoute('admin.guest.edit', [$guest]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'venue_id' => $guest->venue->hashed_id,
            'name'     => $guest->name,
            'email'    => $guest->email,
//            'birthday' => $guest->birthday ? \Carbon\Carbon::createFromFormat('Y-m-d', $guest->birthday)->format('Y-m-d') : null,
            'gender'   => $guest->gender,
            'phone'    => $guest->phone,
        ]);
    }

    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Test guest update');

        /** @var Guest $guest */
        $guest = factory(Guest::class)->create();

        $birthday = $this->faker->dateTimeBetween('-40 years', '-18 years');
        $attributes = [
            'name'     => $this->faker->name,
            'email'    => $this->faker->email,
            'birthday' => $birthday->format('d/m/Y'),
            'gender'   => Guest::GENDER_FEMALE,
            'phone'    => $this->faker->phoneNumber,
        ];

        $I->amOnRoute('admin.guest.edit', [$guest]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $attributes);
        $I->seeCurrentRouteIs('admin.guest.details', [$guest]);
        $I->seeResponseCodeIs(200);

        /** @var Guest $recordToCheck */
        $recordToCheck = $I->grabRecord(Guest::class, [
            'tenant_id' => $guest->tenant_id,
            'id'        => $guest->id,
        ]);

        $attributes['birthday'] = $birthday->format('Y-m-d');

        foreach ($attributes as $key => $value) {
            $I->assertEquals($recordToCheck->getAttribute($key), $value);
        }
    }

    public function testEditPageSessionsTab(AcceptanceTester $I)
    {
        $I->wantTo('Test guest edit page sessions tab');

        /** @var Guest $guest */
        $guest = factory(Guest::class)->create();
        $session = $this->createSession($guest);

        $I->amOnRoute('admin.guest.details', [$guest]);
        $I->seeResponseCodeIs(200);
        $I->seeNumberOfElements('#sessions tbody tr', 1);
        $I->see(trans('messages.session.duration'));
        $I->see(gmdate('H:i:s', $session->duration));
    }
}