<?php

use App\Models\ProximityMarketingCampaign;
use App\Models\Venue;

class ProximityMarketingCest
{
    private $faker;

    /**
     * ProximityMarketingCest constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See proximity marketing index page');
        $I->amOnRoute('admin.proximity_marketing.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.proximity_marketing.index');
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('See proximity marketing add page');
        $I->amOnRoute('admin.proximity_marketing.add');
        $I->seeResponseCodeIs(200);
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testAddPageFillTitleValidation(AcceptanceTester $I)
    {
        $I->wantTo('See proximity_marketing add page fill title and select parameter select date time');
        $I->amOnRoute('admin.proximity_marketing.add');
        $I->submitForm('form', [
            'parameter_type_date' => ProximityMarketingCampaign::PARAMETER_TYPE_VISIT,
        ]);
        $I->canSeeCurrentRouteIs('admin.proximity_marketing.add');
        $I->see(trans('validation.required', ['attribute' => 'title']));
        $I->see(trans('validation.required', ['attribute' => 'start date']));
        $I->see(trans('validation.required', ['attribute' => 'end date']));
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testAddPageDoNotFillTitleValidation(AcceptanceTester $I)
    {
        $I->wantTo('See proximity marketing add page don\'t fill title');
        $I->amOnRoute('admin.proximity_marketing.add');
        $I->submitForm('form', [
            'title' => $this->faker->title,
        ]);
        $I->canSeeCurrentRouteIs('admin.proximity_marketing.add');

        $I->dontSee(trans('validation.required', ['attribute' => 'title']));
        $I->see(trans('validation.required', ['attribute' => 'start date']));
        $I->see(trans('validation.required', ['attribute' => 'end date']));
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testAddPagFillFormValidation(AcceptanceTester $I)
    {
        $I->wantTo('See proximity marketing add page fill title');
        $I->amOnRoute('admin.proximity_marketing.add');
        $I->submitForm('form', [
            'title' => $this->faker->title,
        ]);
        $I->canSeeCurrentRouteIs('admin.proximity_marketing.add');
        $I->dontSee(trans('validation.required', ['attribute' => 'title']));
        $I->see(trans('validation.required', ['attribute' => 'start date']));
        $I->see(trans('validation.required', ['attribute' => 'end date']));
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('See proximity marketing edit page');

        /** @var ProximityMarketingCampaign $proximityMarketing */
        $proximityMarketing = factory(ProximityMarketingCampaign::class)->create();

        $I->amOnRoute('admin.proximity_marketing.edit', [$proximityMarketing]);
        $I->seeResponseCodeIs(200);

        $I->seeInFormFields('form', [
            'title'      => $proximityMarketing->title,
            'start_date' => $proximityMarketing->start_date->format('Y-m-d'),
            'end_date'   => $proximityMarketing->end_date->format('Y-m-d'),
        ]);
    }

    public function _testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Test proximity marketing update');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->create();

        /** @var ProximityMarketingCampaign $proximityMarketing */
        $proximityMarketing = factory(ProximityMarketingCampaign::class)->create();

        $attributes = [
            'location_id' => $venue->hashed_id,
            'title'       => $proximityMarketing->title,
            'start_date'  => $proximityMarketing->start_date,
            'end_Date'    => $proximityMarketing->end_date,
        ];

        $I->amOnRoute('admin.proximity_marketing.edit', [$proximityMarketing]);

        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $attributes);
        $I->amOnRoute('admin.proximity_marketing.index');
        $I->seeResponseCodeIs(200);

        /** @var array $recordToCheck */
        $recordToCheck = $I->grabRecord('proximity_marketing_campaigns', [
            'location_id' => $venue->hashed_id,
            'title'       => $proximityMarketing->title,
            'start_date'  => $proximityMarketing->start_date,
            'end_Date'    => $proximityMarketing->end_date,
        ]);

        foreach ($attributes as $key => $value) {
            $I->assertEquals($recordToCheck[$key], $value);
        }
    }

    public function _testDelete(AcceptanceTester $I)
    {
        $I->wantTo('Test campaign delete');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->create();

        /** @var ProximityMarketingCampaign $proximityMarketing */
        $proximityMarketing = factory(ProximityMarketingCampaign::class)->create();

        $I->amOnRoute('admin.proximity_marketing.delete', [$proximityMarketing]);
        $I->amOnRoute('admin.proximity_marketing.index');
        $I->seeResponseCodeIs(200);

        $campaignToCheck = $I->grabRecord('proximity_marketing_campaigns', [
            'location_id' => $venue->hashed_id,
            'title'       => $proximityMarketing->title,
            'start_date'  => $proximityMarketing->start_date,
            'end_Date'    => $proximityMarketing->end_date,
        ]);


        $I->assertNotNull($campaignToCheck['created_at']);
    }
}