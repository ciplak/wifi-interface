<?php

use App\Models\Venue;

/**
 * @property \Faker\Generator faker
 */
class VenueCest
{
    /**
     * LocationCest constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See venue index page');
        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.venue.index');
        $I->see(trans('venue.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('See venue add page');
        $I->amOnRoute('admin.venue.add');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.venue.add');
        $I->see(trans('venue.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddVenue(AcceptanceTester $I)
    {
        $I->wantTo('Add venue');

        /** @var \App\Models\ServiceProfile $serviceProfile */
        $serviceProfile = factory(\App\Models\ServiceProfile::class)->create();

        /** @var \App\Models\SplashPage $preLogin */
        $preLogin = factory(\App\Models\SplashPage::class)->create();

        /** @var \App\Models\SplashPage $postLogin */
        $postLogin = factory(\App\Models\SplashPage::class)->create([
            'type' => \App\Models\SplashPage::TYPE_ONLINE,
        ]);

        $I->amOnRoute('admin.venue.add');
        $I->canSeeCurrentRouteIs('admin.venue.add');
        $I->see(trans('venue.page_title'));

        $I->submitForm('form', [
            'name'                    => $this->faker->name,
            'description'             => $this->faker->text,
            'login_page_id'           => $preLogin->id,
            'post_login_page_id'      => $postLogin->id,
            'default_service_profile' => $serviceProfile->id,
        ]);
        $I->canSeeCurrentRouteIs('admin.venue.index');
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testIndexPageFilter(AcceptanceTester $I)
    {
        $I->wantTo('Test zone index page filter');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->states('with-splash-pages')->create();

        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.venue.index');
        $I->see(trans('venue.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
        $I->submitForm('.panel-actions form', [
            'name' => $venue->name,
        ]);

        $I->canSeeCurrentRouteIs('admin.venue.index');
        $I->seeResponseCodeIs(200);
        $I->see($venue->name);
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('See venue edit page');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->states('with-splash-pages')->create();

        $I->amOnRoute('admin.venue.edit', [$venue]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'name'               => $venue->name,
            'description'        => $venue->description,
            'login_page_id'      => $venue->login_page_id,
            'post_login_page_id' => $venue->post_login_page_id,
        ]);
    }

    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Test venue update');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->states('with-splash-pages')->create();

        /** @var \App\Models\ServiceProfile $serviceProfile */
        $serviceProfile = factory(\App\Models\ServiceProfile::class)->create();

        /** @var \App\Models\SplashPage $preLogin */
        $preLogin = factory(\App\Models\SplashPage::class)->create();

        /** @var \App\Models\SplashPage $postLogin */
        $postLogin = factory(\App\Models\SplashPage::class)->create([
            'type' => \App\Models\SplashPage::TYPE_ONLINE,
        ]);

        $formAttributes = [
            'name'                    => $this->faker->name,
            'description'             => $this->faker->text,
            'default_service_profile' => $serviceProfile->id,
            'login_page_id'           => $preLogin->id,
            'post_login_page_id'      => $postLogin->id,
        ];

        $I->amOnRoute('admin.venue.edit', [$venue]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $formAttributes);
        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);

        /** @var Venue $recordToCheck */
        $recordToCheck = $I->grabRecord(Venue::class, [
            'id' => $venue->id,
        ]);

        $formAttributes = (object)$formAttributes;
        $I->assertEquals($recordToCheck->name, $formAttributes->name);
        $I->assertEquals($recordToCheck->description, $formAttributes->description);
        $I->assertEquals($recordToCheck->login_page_id, $formAttributes->login_page_id);
        $I->assertEquals($recordToCheck->post_login_page_id, $formAttributes->post_login_page_id);
    }

    public function testDelete(AcceptanceTester $I)
    {
        $I->wantTo('Test venue delete');

        /** @var Venue $venue */
        $venue = factory(Venue::class)->states('with-splash-pages')->create();

        $I->amOnRoute('admin.venue.delete', [$venue]);
        $I->amOnRoute('admin.venue.index');
        $I->seeResponseCodeIs(200);

        /** @var Venue $venueToCheck */
        $venueToCheck = $I->grabRecord('properties', [
            'id' => $venue->id,
        ]);
        $I->assertNotNull($venueToCheck['deleted_at']);
    }
}