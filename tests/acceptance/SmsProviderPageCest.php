<?php


class SmsProviderPageCest
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _after(AcceptanceTester $I)
    {

    }

    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See sms provider index page');
        $I->amOnRoute('admin.sms_provider.index');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentRouteIs('admin.sms_provider.index');
        $I->see(trans('sms_provider.page_title'));
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPageFormValidation(AcceptanceTester $I)
    {
        $I->wantTo('Test sms provider add page form validation');
        $I->amOnRoute('admin.sms_provider.add');
        $I->submitForm('form', []);
        $I->canSeeCurrentRouteIs('admin.sms_provider.add');
        $I->seeFormErrorMessages([
            'name'       => null,
            'gateway_id' => null,
        ]);
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('test sms provider add page');
        $I->amOnRoute('admin.sms_provider.add');
        /**/
        $I->submitForm('form', [
            'name'        => 'Test',
            'description' => 'Test',
            'gateway_id'  => \App\Models\SmsProvider::GATEWAY_CLICKATELL,
        ]);/**/
        $I->canSeeCurrentRouteIs('admin.sms_provider.index');
        $I->seeRecord('sms_providers', [
            'name'        => 'Test',
            'description' => 'Test',
            'gateway_id'  => \App\Models\SmsProvider::GATEWAY_CLICKATELL,
        ]);
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('See sms provider edit page');

        $smsProvider = factory(\App\Models\SmsProvider::class)->create();

        $I->amOnRoute('admin.sms_provider.edit', [$smsProvider]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'name'        => $smsProvider->name,
            'description' => $smsProvider->description,
            'gateway_id'  => $smsProvider->gateway_id,
        ]);
    }
}