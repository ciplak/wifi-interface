<?php

use App\Date;
use App\Models\ServiceProfile;

class ServiceProfileCest
{
    public $faker;

    /**
     * ServiceProfileCest constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function _before(AcceptanceTester $I)
    {
        $user = \App\Models\User::first();
        $I->amLoggedAs($user);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function testIndexPage(AcceptanceTester $I)
    {
        $I->wantTo('See service profile index page');
        $I->amOnPage('admin/service-profiles');
        $I->seeResponseCodeIs(200);
        $I->canSeeCurrentUrlEquals('/admin/service-profiles');
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPage(AcceptanceTester $I)
    {
        $I->wantTo('See service profile add page');
        $I->amOnPage(route('admin.service_profile.add'));
        $I->seeResponseCodeIs(200);
        $I->dontSee('Exception');
        $I->dontSee('Whoops');
    }

    public function testAddPageNameAndDurationValidation(AcceptanceTester $I)
    {
        $I->wantTo('See service profile page validation');
        $I->amOnPage('admin/service-profiles/add');
        $I->submitForm('form', []);
        $I->canSeeCurrentUrlEquals('/admin/service-profiles/add');
        $I->see(trans('validation.required', ['attribute' => trans('service_profile.columns.name')]));
        $I->seeFormErrorMessages([
            'duration' => trans('validation.required', ['attribute' => 'duration']),
        ]);
    }

    public function testAddPageFillNameAndDurationValidation(AcceptanceTester $I)
    {
        $I->wantTo('See service profile add page validation');
        $I->amOnRoute('admin.service_profile.add');

        $params = [
            'duration' => 131,
            'name'     => $this->faker->text(),
        ];

        $I->submitForm('form', $params);
        $I->seeCurrentRouteIs('admin.service_profile.index');
        $I->canSeeResponseCodeIs(200);
    }

    public function testAddPageSelectedTypeRecurringValidation(AcceptanceTester $I)
    {
        $I->wantTo('See service profile add page select type recurring, see renewal_frequency and renewal_frequency_type validation');
        $I->amOnRoute('admin.service_profile.add');
        $I->selectOption('form select[name=type]', ['value' => ServiceProfile::TYPE_RECURRING]);
        $I->seeElement('input', ['name' => 'renewal_frequency']);
        $I->seeElement('select', ['name' => 'renewal_frequency_type']);
    }

    public function testAddPageIfSelectedTypeRecurringValidation(AcceptanceTester $I)
    {
        $I->wantTo('See service profile add page select type recurring validation');
        $I->amOnRoute('admin.service_profile.add');
        $I->selectOption('form select[name=type]', ['value' => ServiceProfile::TYPE_RECURRING]);
        $I->dontSee('The renewal frequency must be at least 1.');
    }

    public function testAddPageIfSelectedModel(AcceptanceTester $I)
    {
        $I->wantTo('See service profile add page select model paid validation');
        $I->amOnRoute('admin.service_profile.add');
        $I->selectOption('form select[name=model]', ['value' => ServiceProfile::MODEL_PAID]);
        $I->seeElement('input', ['name' => 'price']);
    }

    public function testEditPage(AcceptanceTester $I)
    {
        $I->wantTo('Test service profile edit page');

        /** @var ServiceProfile $serviceProfile */
        $serviceProfile = factory(ServiceProfile::class)->create();

        $I->amOnRoute('admin.service_profile.edit', [$serviceProfile]);
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields('form', [
            'duration'               => $serviceProfile->duration,
            'duration_type'          => $serviceProfile->duration_type,
            'type'                   => $serviceProfile->type,
            'renewal_frequency'      => $serviceProfile->renewal_frequency,
            'renewal_frequency_type' => $serviceProfile->renewal_frequency_type,
            'model'                  => $serviceProfile->model,
            'price'                  => $serviceProfile->price,
            'max_download_bandwidth' => $serviceProfile->max_download_bandwidth,
            'max_upload_bandwidth'   => $serviceProfile->max_upload_bandwidth,
            'qos_level'              => $serviceProfile->qos_level,
            'service_pin'            => $serviceProfile->service_pin,
            'status'                 => $serviceProfile->status,
            'expiry_date'            => $serviceProfile->expiry_date ? \Carbon\Carbon::parse($serviceProfile->expiry_date)->format('Y-m-d') : '',
            'name'                   => $serviceProfile->name,
            'description'            => $serviceProfile->description,
        ]);
    }

    public function testUpdate(AcceptanceTester $I)
    {
        $I->wantTo('Update service profile');

        /** @var ServiceProfile $serviceProfile */
        $serviceProfile = factory(ServiceProfile::class)->create();

        $expiryDate = $this->faker->dateTimeThisMonth;

        $attributes = [
            'duration'               => rand(1, 8) * 15,
            'duration_type'          => Date::DURATION_TYPE_MINUTES,
            'type'                   => 1,
            'renewal_frequency'      => 1,
            'renewal_frequency_type' => 2,
            'model'                  => ServiceProfile::MODEL_PAID,
            'price'                  => 14.53,
            'max_download_bandwidth' => 2048,
            'max_upload_bandwidth'   => 512,
            'qos_level'              => 3,
            'expiry_date'            => $expiryDate->format('Y-m-d'),
            'service_pin'            => 0,
            'status'                 => 1,
            'name'                   => $this->faker->text,
            'description'            => $this->faker->text,
        ];


        $I->amOnRoute('admin.service_profile.edit', [$serviceProfile]);
        $I->seeResponseCodeIs(200);
        $I->submitForm('form', $attributes);
        $I->amOnRoute('admin.service_profile.index');
        $I->seeResponseCodeIs(200);

        /** @var ServiceProfile $recordToCheck */
        $recordToCheck = $I->grabRecord(ServiceProfile::class, [
            'tenant_id' => \Auth::user()->tenant_id,
            'id'        => $serviceProfile->id,
        ]);

        $I->assertEquals($recordToCheck->name, $attributes['name']);
        $I->assertEquals($recordToCheck->description, $attributes['description']);
        $I->assertEquals($recordToCheck->duration, $attributes['duration']);
        $I->assertEquals($recordToCheck->duration_type, $attributes['duration_type']);
        $I->assertEquals($recordToCheck->type, $attributes['type']);
        $I->assertEquals($recordToCheck->renewal_frequency, $attributes['renewal_frequency']);
        $I->assertEquals($recordToCheck->renewal_frequency_type, $attributes['renewal_frequency_type']);
        $I->assertEquals($recordToCheck->model, $attributes['model']);
        $I->assertEquals($recordToCheck->price, $attributes['price']);
        $I->assertEquals($recordToCheck->max_download_bandwidth, $attributes['max_download_bandwidth']);
        $I->assertEquals($recordToCheck->max_upload_bandwidth, $attributes['max_upload_bandwidth']);
        $I->assertEquals($recordToCheck->qos_level, $attributes['qos_level']);
        $I->assertEquals($recordToCheck->expiry_date, $attributes['expiry_date']);
        $I->assertEquals($recordToCheck->service_pin, $attributes['service_pin']);
        $I->assertEquals($recordToCheck->status, $attributes['status']);
    }

    public function testDelete(AcceptanceTester $I)
    {
        $I->wantTo('Delete service profile');

        /** @var ServiceProfile $serviceProfile */
        $serviceProfile = factory(ServiceProfile::class)->create();

        $I->amOnRoute('admin.service_profile.delete', [$serviceProfile]);
        $I->amOnRoute('admin.service_profile.index');
        $I->seeResponseCodeIs(200);

        /** @var array $serviceProfileToCheck */
        $serviceProfileToCheck = $I->grabRecord('service_profiles', [
            'tenant_id' => $serviceProfile->tenant_id,
            'id'        => $serviceProfile->id,
        ]);

        $I->assertNotNull($serviceProfileToCheck['deleted_at']);
    }
}