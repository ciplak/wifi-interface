<?php

return [
    'cdn' => [
        'static.iperawifi.com' => ''
    ],

    'radius_server' => env('RADIUS_SERVER', 'TekRadius'),

    'per_page' => 15,

    'new_users_range' => 7,

    'custom_field_count' => 3,

    'copyright_text' => '&copy; %year% - IPera',

    'wifi_communicator_url' => env('WIFI_COMMUNICATOR_URL'),
];