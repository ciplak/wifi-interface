<?php
return [
    'gateways' => [
        1 => [
            'id'         => 1,
            'name'       => 'Clickatell',
            'key'        => 'clickatell',
            'parameters' => ['api_id', 'auth_token'],
        ],
        2 => [
            'id'         => 2,
            'name'       => 'Twilio',
            'key'        => 'twilio',
            'parameters' => ['account_sid', 'auth_token', 'from'],
        ],
        3 => [
            'id'         => 3,
            'name'       => 'iSmartSms',
            'key'        => 'ISmartSms',
            'parameters' => ['username', 'password'],
        ],
        4 => [
            'id'         => 4,
            'name'       => 'Accinge',
            'key'        => 'accinge',
            'parameters' => ['username', 'password', 'sender_id'],
        ],
        5 => [
            'id'         => 5,
            'name'       => 'MobilPark',
            'key'        => 'mobilpark',
            'parameters' => ['username', 'password', 'from'],
        ],
        6 => [
            'id'         => 6,
            'name'       => 'MobilDev',
            'key'        => 'mobildev',
            'parameters' => ['key', 'secret', 'originator', 'msisdn'],
        ],
        7 => [
            'id'         => 7,
            'name'       => 'Mayer Mobile',
            'key'        => 'mayermobile',
            'parameters' => ['username', 'password', 'sender'],
        ],
    ],
];