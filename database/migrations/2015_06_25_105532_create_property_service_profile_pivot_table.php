<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyServiceProfilePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_service_profiles', function(Blueprint $table) {
            $table->integer('property_id')->unsigned()->index();
            $table->integer('service_profile_id')->unsigned()->index();
            $table->unsignedTinyInteger('is_default')->default(0);

            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('service_profile_id')->references('id')->on('service_profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_service_profiles');
    }
}
