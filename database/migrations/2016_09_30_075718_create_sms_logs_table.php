<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('sms_provider_id')->default(null)->nullable();
            $table->unsignedTinyInteger('type');
            $table->unsignedTinyInteger('event_type')->default(null)->nullable();
            $table->string('to')->default(null)->nullable();
            $table->text('message')->default(null)->nullable();
            $table->string('queue_id',50)->nullable();
            $table->text('data')->nullable()->comment('Json Extra Data');
            $table->text('response')->nullable();
            $table->unsignedTinyInteger('status')->default(null)->nullable();;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sms_logs');
    }
}
