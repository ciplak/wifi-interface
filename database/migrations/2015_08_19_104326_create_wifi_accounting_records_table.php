<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWifiAccountingRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wifi_accounting_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('session_id');
            $table->string('status_type', 30)->nullable();
            $table->bigInteger('input_octs')->nullable()->unsigned()->default(0);
            $table->bigInteger('out_octs')->nullable()->unsigned()->default(0);
            $table->string('username', 128)->nullable();
            $table->string('nas_ip_addr', 15)->nullable();
            $table->string('nas_identifier')->nullable();
            $table->string('nas_port', 40)->nullable();
            $table->string('nas_port_id', 255)->nullable();
            $table->string('nas_port_type', 40)->nullable();
            $table->string('service_type', 40)->nullable();
            $table->string('framed_ip_addr', 15)->nullable();
            $table->string('calling_station_id', 128)->nullable();
            $table->string('called_station_id', 128)->nullable();
            $table->integer('acct_sess_time')->nullable()->unsigned();
            $table->string('disconnect_cause', 128)->nullable();
            $table->dateTime('timestamp');
            $table->integer('amount')->nullable();
            $table->unsignedTinyInteger('is_deleted')->default(0);
            $table->tinyInteger('record_status')->nullable()->unsigned()->default(0);
            $table->dateTime('created_time')->nullable();
            $table->dateTime('last_updated_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wifi_accounting_records');
    }
}
