<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoginPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_pages', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('venue_id')->nullable();
            $table->unsignedInteger('parent_id')->nullable()->default(null);
            $table->char('locale', 2)->index();

            $table->unsignedTinyInteger('type')->default(0);

            $table->string('name');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('logo')->nullable();
            $table->mediumText('settings')->nullable();
            $table->unsignedTinyInteger('status')->default(0);

            $table->text('custom_html')->nullable();
            $table->text('custom_css')->nullable();
            $table->text('custom_js')->nullable();
            $table->text('compiled_html')->nullable();

            $table->unsignedTinyInteger('login_field')->default(0);
            $table->unsignedTinyInteger('show_sms_code')->default(0);
            $table->unsignedTinyInteger('send_sms')->default(0);
            $table->unsignedTinyInteger('send_email')->default(0);

            $table->unsignedTinyInteger('social_login_requires_code')->default(0);
            $table->unsignedTinyInteger('overwrite_guests')->default(0);

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();

            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('venue_id')->references('id')->on('properties');
            $table->foreign('parent_id')->references('id')->on('login_pages');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('properties', function(Blueprint $table) {
            $table->foreign('login_page_id')->references('id')->on('login_pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function(Blueprint $table) {
            $table->dropColumn('login_page_id');
        });

        Schema::drop('login_pages');
    }
}
