<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSmsProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_providers', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();

            $table->string('name');
            $table->string('description')->nullable();
            $table->unsignedTinyInteger('gateway_id');
            $table->text('parameters')->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
        });

        Schema::table('properties', function(Blueprint $table) {
            $table->foreign('sms_provider_id')->references('id')->on('sms_providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function(Blueprint $table) {
            $table->dropColumn('sms_provider_id');
        });

        Schema::drop('sms_providers');
    }
}
