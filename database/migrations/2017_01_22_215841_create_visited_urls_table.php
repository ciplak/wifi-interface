<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitedUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visited_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable()->index();
            $table->string('source_ip')->nullable();
            $table->unsignedSmallInteger('source_port')->nullable();
            $table->string('destination_ip')->nullable();
            $table->unsignedSmallInteger('destination_port')->nullable();
            $table->unsignedInteger('device_id')->nullable()->index();
            $table->string('ap_mac')->nullable()->index();
            $table->string('client_mac')->nullable()->index();
            $table->string('method')->nullable();
            $table->string('url')->nullable();
            $table->string('host')->nullable();
            $table->dateTime('visited_at')->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visited_urls');
    }
}
