<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('venue_id')->nullable();

            $table->string('name', 32)->nullable();
            $table->string('username')->unique();
            $table->string('email')->nullable();
            $table->date('birthday')->nullable();
            $table->unsignedTinyInteger('age_range')->nullable();
            $table->tinyInteger('gender')->unsigned()->default(0);
            $table->string('facebook_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('instagram_id')->nullable();
            $table->string('google_id')->nullable();
            $table->string('linkedin_id')->nullable();
            $table->string('foursquare_id')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('phone')->nullable();
            $table->char('nationality', 2)->nullable();
            $table->text('custom_data')->nullable();
            $table->string('custom_field_1')->nullable();
            $table->string('custom_field_2')->nullable();
            $table->string('custom_field_3')->nullable();
            $table->dateTime('smart_login_time')->nullable();
            $table->dateTime('login_time')->nullable();
            $table->dateTime('last_visit_time')->nullable();
            $table->unsignedInteger('total_visits')->default(0);;
            $table->unsignedBigInteger('total_duration')->default(0);
            $table->unsignedBigInteger('total_downloaded_data')->default(0);
            $table->unsignedBigInteger('total_uploaded_data')->default(0);
            $table->unsignedTinyInteger('connection_status')->default(0);
            $table->unsignedTinyInteger('direct_login')->default(0);
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('avatar')->nullable();
            $table->string('access_code', 20)->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('venue_id')->references('id')->on('properties');
            $table->index('email');
            $table->index('facebook_id');
            $table->index('twitter_id');
            $table->index('passport_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('guests');
    }
}
