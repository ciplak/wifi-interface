<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWifiSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wifi_sessions', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('venue_id')->nullable();
            $table->unsignedInteger('zone_id')->nullable();

            $table->string('parent_session_id')->nullable();
            $table->string('session_id')->nullable();
            $table->string('username', 128)->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->integer('guest_device_id')->unsigned()->nullable();
            $table->string('guest_device_ip')->nullable();
            $table->string('guest_device_mac')->nullable()->index();
            $table->string('access_code', 20)->nullable();
            $table->dateTime('guest_device_logout_time')->nullable();
            $table->integer('device_id')->unsigned()->nullable();
            $table->integer('duration')->nullable();
            $table->bigInteger('downloaded_data')->nullable();
            $table->bigInteger('uploaded_data')->nullable();
            $table->tinyInteger('session_status')->nullable()->default(0);
            $table->integer('guest_id')->unsigned()->nullable();
            $table->integer('service_profile_id')->unsigned()->nullable();
            $table->tinyInteger('connection_status')->nullable()->default(0);
            $table->unsignedTinyInteger('login_type')->nullable()->default(0);
            $table->dateTime('last_updated_time')->nullable();
            $table->dateTime('timestamp')->nullable();
            $table->string('internal_session_id')->nullable();
            $table->unsignedInteger('wifi_sessions_origin_id')->nullable();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('venue_id')->references('id')->on('properties');
            $table->foreign('zone_id')->references('id')->on('properties');
            $table->foreign('guest_device_id')->references('id')->on('guest_devices');
            $table->foreign('device_id')->references('id')->on('devices');
            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('service_profile_id')->references('id')->on('service_profiles');
            $table->foreign('wifi_sessions_origin_id')->references('id')->on('wifi_sessions_origin');

            $table->index('start_time');
            $table->index('end_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wifi_sessions');
    }
}
