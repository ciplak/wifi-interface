<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineGuestsHourlyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_guests_hourly', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('hour');
            $table->integer('guest_count')->unsigned()->default(0);
            $table->integer('connection_count')->unsigned()->default(0);
            $table->datetime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('online_guests_hourly');
    }
}
