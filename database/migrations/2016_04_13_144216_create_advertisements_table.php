<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->string('title');
            $table->unsignedTinyInteger('type')->default(0);
            $table->text('body')->nullable();
            $table->string('image')->nullable();
            $table->string('url')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advertisements');
    }
}
