<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuestDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_devices', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('guest_id')->unsigned()->nullable();

            $table->tinyInteger('device_type_id')->unsigned();
            $table->integer('service_profile_id')->unsigned()->nullable();
            $table->string('description')->nullable();
            $table->string('platform')->nullable();
            $table->string('platform_version')->nullable();
            $table->string('browser')->nullable();
            $table->string('browser_version')->nullable();
            $table->string('mac')->nullable()->index();
            $table->string('ip');
            $table->tinyInteger('connection_status')->unsigned()->default(0);
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->string('status_info')->nullable();
            $table->unsignedTinyInteger('login_type')->nullable()->default(0);
            $table->dateTime('login_time')->nullable();
            $table->dateTime('logout_time')->nullable();
            $table->bigInteger('duration')->unsigned()->default(0);
            $table->bigInteger('total_duration')->unsigned()->default(0);
            $table->bigInteger('downloaded_data')->unsigned()->default(0);
            $table->bigInteger('uploaded_data')->unsigned()->default(0);
            $table->bigInteger('total_downloaded_data')->unsigned()->default(0);
            $table->bigInteger('total_uploaded_data')->unsigned()->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('service_profile_id')->references('id')->on('service_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('guest_devices');
    }
}
