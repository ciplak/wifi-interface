<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_profiles', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();

            $table->string('name');
            $table->string('description')->nullable();
            $table->date('expiry_date')->nullable()->default(null);
            $table->unsignedTinyInteger('type');
            $table->unsignedTinyInteger('renewal_frequency');
            $table->unsignedTinyInteger('renewal_frequency_type');
            $table->unsignedTinyInteger('model');
            $table->decimal('price')->unsigned();

            $table->unsignedTinyInteger('service_pin');
            $table->unsignedTinyInteger('access_code_display_page')->default(0);

            $table->unsignedTinyInteger('duration');
            $table->unsignedSmallInteger('duration_total_minutes');
            $table->unsignedTinyInteger('duration_type');

            $table->unsignedMediumInteger('max_download_bandwidth')->default(0);
            $table->unsignedMediumInteger('max_upload_bandwidth')->default(0);
            $table->unsignedTinyInteger('qos_level')->nullable()->default(null);

            $table->unsignedTinyInteger('status');

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_profiles');
    }
}
