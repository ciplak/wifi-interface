<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuestActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_activities', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('property_id')->nullable();
            $table->unsignedInteger('device_id')->nullable();
            $table->unsignedInteger('guest_id')->nullable()->index();
            $table->unsignedInteger('guest_device_id')->nullable()->index();
            $table->unsignedInteger('action_id')->index();
            $table->string('ip', 17)->nullable();
            $table->char('mac', 15)->nullable();
            $table->string('mac')->nullable()->index();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('device_id')->references('id')->on('devices');
            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('guest_device_id')->references('id')->on('guest_devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('guest_activities');
    }
}
