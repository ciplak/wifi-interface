<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->string('title');
            $table->unsignedInteger('location_id')->default(null)->nullable();
            $table->unsignedTinyInteger('status')->default(0);

            $table->unsignedTinyInteger('parameter_type')->default(0);
            $table->date('parameter_type_date');
            $table->time('parameter_type_time');
            $table->unsignedBigInteger('day_time')->default(0);
            $table->unsignedTinyInteger('periodic')->default(0);
            $table->unsignedBigInteger('reengage')->default(0);

            $table->unsignedTinyInteger('gender')->default(0);
            $table->unsignedTinyInteger('age')->default(0);
            $table->unsignedTinyInteger('birthday')->nullable();
            $table->unsignedBigInteger('visit_count')->default(0);

            $table->text('sms_content')->nullable();
            $table->text('email_content')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('location_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaigns');
    }
}
