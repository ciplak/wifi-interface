<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->unsignedTinyInteger('property_type_id');

            $table->string('name');
            $table->string('description')->nullable();

            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('post_code')->nullable();
            $table->string('country_code', 2)->nullable()->index();

            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();

            $table->unsignedTinyInteger('use_custom_text')->default(0);
            $table->text('contents')->nullable();

            $table->string('post_login_url')->nullable();
            $table->unsignedInteger('login_page_id')->nullable();
            $table->unsignedInteger('post_login_page_id')->default(0)->nullable();
            $table->text('ssids')->nullable();
            $table->unsignedInteger('sms_provider_id')->nullable();
            $table->unsignedTinyInteger('is_default')->default(0);

            $table->unsignedTinyInteger('smart_login')->nullable()->default(null);
            $table->unsignedTinyInteger('smart_login_frequency')->default(0)->nullable();
            $table->unsignedTinyInteger('smart_login_frequency_type')->default(0);

            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
