<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProximityMarketingCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proximity_marketing_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->string('title');
            $table->unsignedInteger('location_id')->default(null)->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedTinyInteger('event_type')->default(0);
            $table->unsignedSmallInteger('event_delay')->default(0);

            $table->unsignedTinyInteger('parameter_type')->default(0);
            $table->unsignedBigInteger('parameter_visit_based')->default(0);
            $table->text('parameter_type_weekdays')->nullable();
            $table->string('parameter_type_time_start', 10)->nullable()->default(null);
            $table->string('parameter_type_time_end', 10)->nullable()->default(null);
            $table->string('frequency', 30);

            $table->unsignedTinyInteger('gender')->default(0);
            $table->unsignedTinyInteger('age')->default(0);

            $table->unsignedSmallInteger('visit_option')->default(null)->nullable();
            $table->unsignedSmallInteger('number_of_visit')->default(null)->nullable();
            $table->unsignedSmallInteger('visit_is')->default(null)->nullable();
            $table->unsignedSmallInteger('visit_more_than')->default(null)->nullable();
            $table->unsignedSmallInteger('every_x_visit')->default(null)->nullable();

            $table->text('communication_channel')->nullable();

            $table->text('email_content')->nullable();
            $table->text('twitter_content')->nullable();
            $table->text('push_notification_content')->nullable();
            $table->text('sms_content')->nullable();
            $table->unsignedTinyInteger('start_age')->nullable()->default(null);
            $table->unsignedTinyInteger('end_age')->nullable()->default(null);



            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('location_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proximity_marketing_campaigns');
    }
}
