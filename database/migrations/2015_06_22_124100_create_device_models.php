<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeviceModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_models', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('device_type_id');
            $table->unsignedInteger('device_vendor_id');
            $table->string('name');
            $table->unsignedSmallInteger('device_handler_id')->index();

            $table->foreign('device_vendor_id')->references('id')->on('device_vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_models');
    }
}
