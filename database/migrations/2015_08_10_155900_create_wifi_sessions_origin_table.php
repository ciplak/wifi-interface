<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWifiSessionsOriginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('wifi_sessions_origin')) {
            Schema::create('wifi_sessions_origin', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('tenant_id')->nullable();
                $table->unsignedInteger('venue_id')->nullable();
                $table->unsignedInteger('guest_id')->nullable();
                $table->unsignedInteger('guest_device_id')->nullable();
                $table->unsignedTinyInteger('login_type')->nullable();
                $table->unsignedInteger('service_profile_id')->nullable();
                $table->dateTime('guest_device_logout_time')->nullable();

                $table->datetime('timestamp');
                $table->string('session_id')->nullable();
                $table->string('username', 128)->nullable();
                $table->string('group_name', 128)->nullable();
                $table->string('nas_ip_addr', 15)->nullable();
                $table->string('nas_identifier')->nullable();
                $table->string('nas_port', 40)->nullable();
                $table->string('nas_port_type', 40)->nullable();
                $table->string('nas_port_id')->nullable();
                $table->string('service_type', 40)->nullable();
                $table->string('framed_ip_addr', 15)->nullable();
                $table->string('calling_station_id', 64)->nullable();
                $table->string('called_station_id', 64)->nullable();
                $table->string('audit_session_id', 64)->nullable();
                $table->dateTime('start_time')->nullable();
                $table->unsignedTinyInteger('session_status')->nullable()->default(0);
                $table->unsignedTinyInteger('connection_status')->nullable()->default(0);
                $table->dateTime('last_updated_time')->nullable();
                $table->string('internal_session_id')->nullable();
                $table->dateTime('end_time')->nullable();

                $table->index('calling_station_id');
                $table->index('called_station_id');
                $table->index('start_time');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wifi_sessions_origin');
    }
}
