<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('api_token', 60)->nullable()->unique();
            $table->unsignedInteger('property_id')->nullable();
            $table->unsignedInteger('tenant_id')->nullable();
            $table->char('locale', 2)->nullable();
            $table->string('avatar')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->rememberToken();
            $table->dateTime('banned_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('property_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
