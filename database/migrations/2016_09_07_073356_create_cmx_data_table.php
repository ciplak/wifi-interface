<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmxDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmx_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('device_id')->nullable();
            $table->char('mac', 17)->nullable();
            $table->string('type', 16)->nullable();
            $table->mediumText('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cmx_data');
    }
}
