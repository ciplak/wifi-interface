<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();

            $table->integer('device_model_id')->unsigned();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('mac', 17);
            $table->string('ip', 15);
            $table->integer('property_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('device_model_id')->references('id')->on('device_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }
}
