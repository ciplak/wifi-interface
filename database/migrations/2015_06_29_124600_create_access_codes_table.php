<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccessCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_codes', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('venue_id')->nullable();

            $table->integer('guest_id')->unsigned()->nullable();
            $table->integer('service_profile_id')->unsigned();
            $table->unsignedTinyInteger('type'); // 0: shared, 1: Access code, 2: SMS

            $table->string('code', 20);
            $table->date('expiry_date')->nullable();

            $table->unsignedTinyInteger('use_count')->default(0);
            $table->dateTime('logout_time')->nullable()->default(null);

            $table->unsignedTinyInteger('status');
            $table->integer('created_by')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('properties');
            $table->foreign('venue_id')->references('id')->on('properties');
            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('service_profile_id')->references('id')->on('service_profiles');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access_codes');
    }
}
