<?php

use Illuminate\Database\Seeder;

class SmsProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\SmsProvider::create([
            'name' => 'Clickatell',
            'gateway_id' => \App\Models\SmsProvider::GATEWAY_CLICKATELL,
            'parameters' => [
                'api_id' => '3549888',
                'username' => 'ozgurgoktas',
                'password' => 'cbWDcMTMRgRaZfcbcb'
            ]
        ]);

        \App\Models\SmsProvider::create([
            'name' => 'Twilio',
            'gateway_id' => \App\Models\SmsProvider::GATEWAY_TWILIO,
            'parameters' => [
                'account_sid' => 'AC5cacab421dd7e6d70b44e5544fef59e2ACAC',
                'auth_token' => 'd85fef591655cc6cb3aa2048c84628d1d8d8'
            ]
        ]);

        \App\Models\SmsProvider::create([
            'name' => 'iSmartSMS',
            'gateway_id' => \App\Models\SmsProvider::GATEWAY_ISMART,
            'parameters' => [
                'username' => 'test',
                'password' => 'test'
            ]
        ]);
    }
}
