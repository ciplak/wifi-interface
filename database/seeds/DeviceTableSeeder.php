<?php

use Illuminate\Database\Seeder;

class DeviceTableSeeder extends Seeder
{
    public function run()
    {
        $vendors = [
            'Cisco' => [
                'WLC 7.6' => \App\Models\DeviceModel::DEVICE_TYPE_WLC,
                'AIR-CAP2602E-E-K9' => \App\Models\DeviceModel::DEVICE_TYPE_ACCESS_POINT,
                'AIR-CAP2602I-E-K9' => \App\Models\DeviceModel::DEVICE_TYPE_ACCESS_POINT,
                'AIR-CAP1552EU-E-K9' => \App\Models\DeviceModel::DEVICE_TYPE_ACCESS_POINT,
                'AIR-CAP3702I-E-K9' => \App\Models\DeviceModel::DEVICE_TYPE_ACCESS_POINT,
            ],
            'Xirrus' => [
                'XR-620' => \App\Models\DeviceModel::DEVICE_TYPE_ACCESS_POINT
            ]
        ];


        foreach ($vendors as $vendorName => $models) {
            $vendor = \App\Models\DeviceVendor::firstOrCreate([
                'name' => $vendorName
            ]);

            if ($models) {

                foreach ($models as $modelName => $type) {

                    $deviceModel = \App\Models\DeviceModel::firstOrCreate([
                        'device_type_id' => $type,
                        'device_vendor_id' => $vendor->id,
                        'name' => $modelName
                    ]);

                }

            }
        }

        $zoneId = \App\Models\Zone::where('is_default', 1)->first()->id;

        \App\Models\Device::create([
            'device_model_id' => 1,
            'property_id' => $zoneId,
            'name' => 'WLC',
            'mac' => '74:a0:2f:a3:49:71',
            'ip' => '192.168.168.253',
        ]);

        \App\Models\Device::create([
            'device_model_id' => 5,
            'property_id' => $zoneId,
            'name' => 'Cisco AP',
            'mac' => '68:86:a7:bf:d0:70',
            'ip' => '192.168.168.252',
        ]);

        \App\Models\Device::create([
            'device_model_id' => 6,
            'property_id' => $zoneId,
            'name' => 'Xirrus AP',
            'mac' => '50:60:28:03:E6:D8',
            'ip' => '192.168.168.113',
        ]);

        /*
        $i = 1;
        $faker = Faker\Factory::create();

        foreach ($vendors as $key => $vendor) {
            foreach ($vendor as $modelName) {
                \App\Models\Device::create([
                    'device_model_id' => $i++,
                    'property_id' => $zoneId,
                    'parent_id' => null,
                    'name' => $modelName,
                    'mac' => $faker->macAddress,
                    'ip' => $faker->ipv4,
                ]);

            }
        }/**/

        //factory('App\Models\Device', 10)->create();
    }
}
