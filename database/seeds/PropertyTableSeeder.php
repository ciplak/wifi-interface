<?php

use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    public function run()
    {
        $tenant = \App\Models\Property::create([
            'property_type_id' => \App\Models\Property::TYPE_TENANT,
            'name' => 'IPERA',
            'description' => 'New York',
            'address' => '115 East 23rd Street 5th Floor',
            'city' => 'New York',
            'post_code' => '10010',
            'state' => 'New York',
            'country_code' => 'US',
            'is_default' => true,
            'use_custom_text' => 1,
        ]);
/**/
        $venue = \App\Models\Property::create([
            'parent_id' => $tenant->id,
            'property_type_id' => \App\Models\Property::TYPE_VENUE,
            'name' => 'IPERA',
            'description' => 'New York',
            'address' => '115 East 23rd Street 5th Floor',
            'city' => 'New York',
            'post_code' => '10010',
            'state' => 'New York',
            'country_code' => 'US',
//            'login_page_id' => 1,
//            'sms_provider_id' => ,
            'is_default' => true,
            'use_custom_text' => 1,
        ]);

        \App\Models\Zone::create([
            'parent_id' => $venue->id,
            'property_type_id' => \App\Models\Property::TYPE_ZONE,
            'name' => 'Default zone for IPERA',
            'is_default' => true
        ]);/**/
    }
}
