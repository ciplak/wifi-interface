<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PropertyTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
//      $this->call(ServiceProfileTableSeeder::class);
//      $this->call(CurrencyTableSeeder::class);
//      $this->call(SmsProviderTableSeeder::class);
//      $this->call(LoginPageTableSeeder::class);
//      $this->call(DeviceTableSeeder::class);
//      $this->call(GuestTableSeeder::class);
    }
}
