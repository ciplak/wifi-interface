<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SmsLogTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\SmsLog::class, 10)->create();
    }
}
