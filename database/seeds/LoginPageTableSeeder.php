<?php

use Illuminate\Database\Seeder;

class LoginPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\SplashPage::create([
            'name' => 'Default',
            'title' => [
                'en' => 'Welcome to IPera WiFi',
                'ar' => 'مرحبا بكم في مطار عمان'
            ],
            'description' => null,
            'logo' => 'logo.jpg',
            'settings' => [
                'bg_color' => '#ffffff',
                'text_color' => '#333333',
                'box_color' => '#ffffff',
                'button_color' => '#f33500',
                'button_text_color' => '#ffffff',
                'button_hover_color' => '#d43306',
                'button_hover_text_color' => '#ffffff',
    
                'bg_image' => 'background.jpg',

                'links' => [
                    1 => [
                        'url' => 'http://www.iperasolutions.com',
                        'title' => [
                            'en' => 'IPera WiFi',
                            'ar' => 'ايبرا واي فاي'
                        ]
                    ],
                    2 => [
                        'url' => '',
                        'title' => ['en' => '', 'ar' => '']
                    ],
                    3 => [
                        'url' => '',
                        'title' => ['en' => '', 'ar' => '']
                    ]
                ],

                'fields' => [
                    'name' => [
                        'enabled' => '1',
                    ],
                    'birthday' => [
                        'optional' => '1',
                    ],
                    'phone' => [
                        'optional' => '1',
                    ],
                    'cf1' => [
                        'optional' => '1',
                        'title' => [
                            'en' => 'Middle Name',
                            'ar' => 'الاسم الأوسط',
                        ]
                    ],
                    'cf2' => [
                        'optional' => '1',
                        'title' => [
                            'en' => 'Father\'s Name',
                            'ar' => 'اسم الاب',
                        ]
                    ],
                    'cf3' => [
                        'optional' => '1',
                        'title' => [
                            'en' => 'Mother\'s Name',
                            'ar' => 'اسم الأم'
                        ]
                    ]
                ],
                'fieldOrder' => [
                    'name',
                    'email',
                    'birthday',
                    'phone',
                    'passport_no',
                    'post_code',
                    'gender',
                    'nationality',
                    'cf1',
                    'cf2',
                    'cf3',
                ],

            ],
            'status' => '1',
            'template_id' => '1',

            'login_types' => [
                100 => [
                    'enabled' => '1',
                    'title' => [
                        'en' => 'Login with Facebook',
                        'ar' => 'تسجيل الدخول بواسطة القيس بوك'
                    ],
                    'description' => [
                        'en' => 'You need a Facebook account to login',
                        'ar' => 'تحتاج الى حساب الفيسبوك للدخول'
                    ]
                ],
                2 => [
                    'enabled' => '1',
                    'title' => [
                        'en' => 'Get One-time Password via SMS',
                        'ar' => 'الحصول على كلمة مرور لمرة واحدة عن طريق SMS'
                    ],
                    'description' => [
                        'en' => 'For Oman GSM Number holders only',
                        'ar' => 'للاصحاب الارقام العمانيه فقط'
                    ]
                ],
                3 => [
                    'enabled' => '1',
                    'title' => [
                        'en' => 'Register',
                        'ar' => 'تسجيل'
                    ],
                    'description' => [
                        'en' => 'Get an access code',
                        'ar' => 'الحصول على رمز الدخول'
                    ]
                ],
                4 => [
                    'enabled' => '1',
                    'title' => [
                        'en' => 'I have an access code',
                        'ar' => 'لدي رمز وصول'
                    ],
                    'description' => [
                        'en' => 'Collect from Information Desk or Information Kiosk',
                        'ar' => 'تحصيل من مكتب المساعدة'
                    ]
                ],
            ]
        ]);

    }
}
