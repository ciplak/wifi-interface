<?php

use Illuminate\Database\Seeder;

class WifiSessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\WifiSession::class, 5000)->create();
    }
}
