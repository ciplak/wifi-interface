<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $user = \App\Models\User::create([
                'name'      => 'IPera Admin',
                'email'     => 'admin@iperawifi.com',
                'password'  => \Hash::make('IPera2017'),
                'api_token' => str_random(60),
                'tenant_id' => 1,
            ]);

            $role = \App\Models\Role::where('name', 'developer')->first();
            $user->attachRole($role);

            $user = \App\Models\User::create([
                'name'      => 'IPera Support',
                'email'     => 'support@iperawifi.com',
                'password'  => \Hash::make('123456'),
                'api_token' => str_random(60),
                'tenant_id' => 1,
            ]);

            $role = \App\Models\Role::where('name', 'support')->first();
            $user->attachRole($role);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
