<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $developer = Role::create([
                'name'         => 'developer',
                'display_name' => 'Developer',
                'description'  => 'Developer',
            ]);

            $developer->perms()->sync(\App\Models\Permission::pluck('id')->toArray());

            $support = Role::create([
                'name'         => 'support',
                'display_name' => 'Support',
                'description'  => 'IPera Support',
            ]);

            $support->perms()->sync(\App\Models\Permission::whereIn('name', $this->getSupportPermissionList())->pluck('id')->toArray());

            $tenant = Role::create([
                'name'         => 'tenant',
                'display_name' => 'Tenant Admin',
                'description'  => 'Tenant Admin',
            ]);

            $tenant->perms()->sync(\App\Models\Permission::whereIn('name', $this->getTenantPermissionList())->pluck('id')->toArray());

            $venue = Role::create([
                'name'         => 'venue',
                'display_name' => 'Venue Admin',
                'description'  => 'Venue Admin',
            ]);

            $venue->perms()->sync(\App\Models\Permission::whereIn('name', $this->getVenuePermissionList())->pluck('id')->toArray());

            $frontDesk = Role::create([
                'name'         => 'front-desk',
                'display_name' => 'Front Desk',
                'description'  => 'Front Desk',
            ]);

            $frontDesk->perms()->sync(\App\Models\Permission::whereIn('name', $this->getFrontDeskPermissionList())->pluck('id')->toArray());
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return array
     */
    private function getSupportPermissionList()
    {
        return [
            'tenants.manage',
            'alert-messages.view',
            'tenants.update-profile',
            'sms-providers.manage',
            'users.manage',
            'users.manage-global',
            'users.impersonate',
            'users.ban',
            'settings.global',
            'settings.update',
            'settings.advanced',
            'marketing',
            'service-profiles.manage',
            'service-profiles.view',
            'venues.add',
            'venues.delete',
            'venues.manage',
            'venues.manage-global',
            'guests.manage',
            'guests.manage-global',
            'devices.manage',
            'devices.manage-global',
            'access-codes.manage-global',
            'access-codes.manage',
            'access-codes.view',
            'login-pages.manage',
            'login-pages.manage-global',
            'activity-logs.view',
            'activity-logs.view-global',
            'reports.view',
            'reports.view-global',
            'developer.menu',
            'logs.view',
        ];
    }

    /**
     * @return array
     */
    private function getTenantPermissionList()
    {
        return [
            'tenants.update-profile',
            'sms-providers.manage',
            'users.manage',
            'users.manage-global',
            'settings.update',
            'settings.advanced',
            'marketing',
            'service-profiles.manage',
            'service-profiles.view',
            'venues.add',
            'venues.delete',
            'venues.manage',
            'venues.manage-global',
            'guests.manage',
            'guests.manage-global',
            'devices.manage',
            'devices.manage-global',
            'access-codes.manage-global',
            'access-codes.manage',
            'access-codes.view',
            'login-pages.manage',
            'login-pages.manage-global',
            'reports.view',
            'reports.view-global'
        ];
    }

    /**
     * @return array
     */
    private function getVenuePermissionList()
    {
        return [
            'users.manage',
            'service-profiles.view',
            'venues.manage',
            'guests.manage',
            'devices.manage',
            'access-codes.manage',
            'access-codes.view',
            'login-pages.manage',
            'reports.view',
        ];
    }

    /**
     * @return array
     */
    private function getFrontDeskPermissionList()
    {
        return [
            'guests.manage',
            'access-codes.view',
        ];
    }
}
