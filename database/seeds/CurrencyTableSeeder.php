<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Currency::create([
            'name' => 'US Dollars',
            'symbol' => '$',
            'is_prefix' => true
        ]);

        \App\Models\Currency::create([
            'name' => 'Euro',
            'symbol' => '€',
            'is_prefix' => true
        ]);
    }
}
