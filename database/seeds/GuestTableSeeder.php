<?php

use Illuminate\Database\Seeder;

class GuestTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\Guest::class, 30)->create();
        factory(\App\Models\GuestDevice::class, 30)->create();
        factory(\App\Models\WifiSession::class, 30)->create();
    }
}
