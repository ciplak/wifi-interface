<?php

use App\Models\ServiceProfile;
use Illuminate\Database\Seeder;

class ServiceProfileTableSeeder extends Seeder
{
    public function run()
    {
        $userId = App\Models\User::first()->id;

        $data = [
            'tenant_id' => \App\Models\Tenant::latest()->first()->id,
            'renewal_frequency' => 1,
            'renewal_frequency_type' => 2,
            'model' => ServiceProfile::MODEL_FREE,
            'type' => ServiceProfile::TYPE_RECURRING,
            'price' => 0,
            'duration' => 30,
            'duration_total_minutes' => 30,
            'duration_type' => \App\Date::DURATION_TYPE_MINUTES,
            'service_pin' => ServiceProfile::SERVICE_PIN_ENABLED,
            'status' => true,
            'created_by' => $userId,
            'updated_by' => $userId,
        ];

        foreach (getLocales() as $locale => $name) {
            $data[$locale] = ['name' => 'Default service profile'];
        }

        $serviceProfile = ServiceProfile::create($data);


        $venue = \App\Models\Venue::venues()->first();
        $venue->serviceProfiles()->sync([$serviceProfile->id => ['is_default' => 1]]);
    }
}
