<?php

use Illuminate\Database\Seeder;

class GuestDeviceTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\GuestDevice::class, 1000)->create();
    }
}
