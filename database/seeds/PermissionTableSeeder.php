<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'roles.manage'               => 'Manage Roles',
            'tenants.manage'             => 'Manage Tenants',
            'tenants.switch'             => 'Can switch between tenants',
            'alert-messages.view'        => 'View alert messages',
            'tenants.update-profile'     => 'Update tenant profile',
            'sms-providers.manage'       => 'Manage SMS providers',
            'users.manage'               => 'Manage Users',
            'users.manage-global'        => 'Manage Users',
            'users.impersonate'          => 'Impersonate user',
            'users.ban'                  => 'Ban user',
            'settings.global'            => 'Update global settings',
            'settings.update'            => 'Update settings',
            'settings.advanced'          => 'Update advanced settings for tenant',
            'marketing'                  => 'Marketing',
            'developers.manage'          => 'Manage Developers',
            'service-profiles.manage'    => 'Manage service profiles',
            'service-profiles.view'      => 'View service profiles',
            'venues.add'                 => 'Add Venue',
            'venues.delete'              => 'Delete Venue',
            'venues.manage'              => 'Manage venues',
            'venues.manage-global'       => 'Manage venues',
            'guests.manage'              => 'Manage guests',
            'guests.manage-global'       => 'Manage guests',
            'devices.manage'             => 'Manage devices',
            'devices.manage-global'      => 'Manage devices globally',
            'access-codes.manage-global' => 'Manage global access codes',
            'access-codes.manage'        => 'Manage access codes',
            'access-codes.view'          => 'View access codes',
            'login-pages.manage'         => 'Manage login pages',
            'login-pages.manage-global'  => 'Manage login pages globally',
            'activity-logs.view'         => 'View activity logs',
            'activity-logs.view-global'  => 'View global activity logs',
            'reports.view'               => 'View reports',
            'reports.visited_urls.view'  => 'View visited urls reports',
            'reports.view-global'        => 'View global reports',
            'media.manage'               => 'Manage media',
            'developer.menu'             => 'Can see developer menu',
            'logs.view'                  => 'View system logs',
            'translations.manage'        => 'Manage translations',
            'currencies.manage'          => 'Manage currencies',
            'device_vendors.manage'      => 'Manage device vendors & models',
            'radius_servers.manage'      => 'Manage radius servers',
            'support.manage'             => 'Manage support users',
        ];

        foreach ($permissions as $name => $display_name) {
            \App\Models\Permission::firstOrCreate([
                'name'         => $name,
                'display_name' => $display_name,
            ]);
        }
    }
}
