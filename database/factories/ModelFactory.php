<?php
use Faker\Generator;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Generator $faker) {
    return [
        'tenant_id'      => \Auth::user()->tenant_id,
        'name'           => $faker->name,
        'email'          => $faker->safeEmail,
        'password'       => str_random(10),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\SmsLog::class, function (Generator $faker) {

    $credit = rand(1, 10) * 100;

    \DB::table('settings')
        ->where('tenant_id', 1)
        ->where('name', 'sms.total_credits')
        ->increment('value', $credit);

    return [
        'tenant_id'       => 1,
        'sms_provider_id' => 1,
        'data'            => [
            'ref_no'      => uniqid(),
            'description' => $faker->text(),
            'credit'      => $credit,
        ],
        'type'            => 2, // INSTALL CREDIT
        'event_type'      => 1,
        'message'         => '',
        'created_at'      => $faker->dateTimeThisMonth,
        'status'          => rand(0, 1),
    ];
});


$factory->define(\App\Models\Device::class, function (Generator $faker) {
    $deviceModel = factory(\App\Models\DeviceModel::class)->create();
    $location = factory(\App\Models\Zone::class)->create();

    return [
        'tenant_id'       => \Auth::user()->tenant_id,
        'property_id'     => $location->id,
        'name'            => $faker->text,
        'description'     => $faker->text,
        'device_model_id' => $deviceModel->id,
        'ip'              => $faker->ipv4,
        'mac'             => $faker->macAddress,
    ];
});

$factory->define(\App\Models\ServiceProfile::class, function (Generator $faker) {
    $user = App\Models\User::first();

    $durationTotalMinutes = $duration = rand(1, 10);
    $durationType = rand(1, 3);

    switch ($durationType) {
        case \App\Date::DURATION_TYPE_HOURS:
            $durationTotalMinutes *= 60;
            break;

        case \App\Date::DURATION_TYPE_DAYS:
            $durationTotalMinutes *= 24 * 60;
    }

    $data = [
        'tenant_id'              => $user->tenant_id,
        'name'                   => $faker->text,
        'description'            => $faker->text,
        'renewal_frequency'      => rand(1, 10),
        'renewal_frequency_type' => rand(2, 4),
        'model'                  => rand(1, 2),
        'type'                   => rand(1, 2),
        'price'                  => rand(1, 10) * 5,
        'duration'               => $duration,
        'duration_total_minutes' => $durationTotalMinutes,
        'duration_type'          => $durationType,
        'expiry_date'            => $faker->date(),
        'service_pin'            => rand(0, 1),
        'status'                 => $faker->boolean(),
        'created_by'             => $user->id,
        'updated_by'             => $user->id,
    ];

    return $data;
});

$factory->define(\App\Models\Guest::class, function (Generator $faker) {
    return [
        'tenant_id'    => \Auth::user()->tenant_id,
        'venue_id'     => factory(\App\Models\Venue::class)->create()->id,
        'name'         => $faker->name,
        'email'        => $faker->email,
        'birthday'     => $faker->dateTimeBetween('-70 years', '-18 years'),
        'gender'       => rand(0, 2),
        'phone'        => $faker->phoneNumber,
        'direct_login' => 0,
    ];
});

$factory->define(\App\Models\GuestDevice::class, function (Generator $faker) {
    $serviceProfiles = Cache::remember('seeder.guest_device.service_profiles', 15, function () {
        return \App\Models\ServiceProfile::pluck('id')->toArray();
    });

    $guestList = Cache::remember('seeder.guest_device.guests', 15, function () {
        return \App\Models\Guest::pluck('id')->toArray();
    });

    $browsers = ['Opera', 'Edge', 'Chrome', 'Firefox', 'Safari', 'IE', 'Netscape', 'Mozilla'];
    $platforms = ['Windows', 'iPhone', 'OS X', 'BlackBerryOS', 'AndroidOS', 'ChromeOS'];

    $serviceProfileId = $faker->randomElement($serviceProfiles);

    /**
     * @var \App\Models\ServiceProfile $serviceProfile
     */
    $serviceProfile = \App\Models\ServiceProfile::find($serviceProfileId);

    $loginTime = $faker->dateTimeBetween('-1 days');
    $duration = rand($serviceProfile->duration_total_minutes / 6, $serviceProfile->duration_total_minutes);
    $downloaded = $duration * $faker->numberBetween(1, 1024) * 1024;
    $uploaded = $duration * $faker->numberBetween(1, 1024) * 128;

    return [
        'guest_id'              => $faker->randomElement($guestList),
        'device_type_id'        => rand(1, 3),
        'service_profile_id'    => $serviceProfileId,
        'platform'              => $faker->randomElement($platforms),
        'platform_version'      => '',
        'browser'               => $faker->randomElement($browsers),
        'browser_version'       => $faker->numberBetween(5, 15),
        'mac'                   => $faker->macAddress,
        'ip'                    => $faker->ipv4,
        'connection_status'     => $faker->boolean(),
        'status'                => $faker->boolean(),
        'login_type'            => rand(1, 7),
        'login_time'            => $loginTime,
        'logout_time'           => $loginTime->add(new DateInterval('PT' . $duration . 'M')),
        'duration'              => $duration * 60,
        'total_duration'        => $serviceProfile->duration_total_minutes * 60,
        'downloaded_data'       => $downloaded,
        'total_downloaded_data' => $downloaded,
        'uploaded_data'         => $uploaded,
        'total_uploaded_data'   => $uploaded,
    ];
});

$factory->define(\App\Models\WifiSession::class, function (Generator $faker) {
    $date = $faker->dateTime;

    $guestList = Cache::remember('seeder.guest_device.guests', 1, function () {
        return \App\Models\Guest::has('devices')->pluck('id')->toArray();
    });

    $deviceList = Cache::remember('seeder.wifi_sessions.devices', 1, function () {
        return \App\Models\Device::pluck('id')->toArray();
    });

    $guestId = $faker->randomElement($guestList);

    $guest = \App\Models\Guest::with('devices')->find($guestId);

    $guestDevice = isset($guest->devices[0]) ? $guest->devices[0] : \App\Models\GuestDevice::first();

    return [
        'timestamp'          => $date,
        'start_time'         => $faker->dateTimeThisMonth, //$guestDevice->login_time,
        'end_time'           => $faker->dateTimeThisMonth->add(new DateInterval('PT' . 36000 . 'M')), //$guestDevice->logout_time,
        'guest_device_id'    => $guestDevice->id,
        'device_id'          => $faker->randomElement($deviceList),
        'duration'           => $guestDevice->duration,
        'downloaded_data'    => $guestDevice->downloaded_data,
        'uploaded_data'      => $guestDevice->uploaded_data,
        'guest_id'           => $guestId,
        'service_profile_id' => $guestDevice->service_profile_id,
        'login_type'         => $guestDevice->login_type,
        'last_updated_time'  => date('Y-m-d H:i:s'),
    ];
});

$factory->define(App\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'name'         => $faker->name,
        'display_name' => $faker->name,
    ];
});

$factory->define(App\Models\Advertisement::class, function (Faker\Generator $faker) {
    $date = $faker->dateTimeThisMonth;

    return [
        'tenant_id'  => \Auth::user()->tenant_id,
        'title'      => $faker->text(),
        'type'       => App\Models\Advertisement::TYPE_TEXT,
        'body'       => $faker->realText(),
        'url'        => $faker->url,
        'start_date' => $date->format('Y-m-d'),
        'end_date'   => $date->add(new DateInterval('P1W'))->format('Y-m-d'),
        'status'     => App\Models\Advertisement::STATUS_PUBLISHED,
    ];
});


$factory->define(App\Models\DeviceVendor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text,
    ];
});

$factory->define(App\Models\DeviceModel::class, function (Faker\Generator $faker) {
    $deviceVendor = factory(App\Models\DeviceVendor::class)->create();

    return [
        'device_type_id'    => App\Models\DeviceModel::DEVICE_TYPE_WLC,
        'device_vendor_id'  => $deviceVendor->id,
        'device_handler_id' => 1,
        'name'              => $faker->text,
    ];
});

$factory->define(App\Models\DeviceVendor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text,
    ];
});

$factory->define(App\Models\Venue::class, function (Faker\Generator $faker) {
    return [
        'parent_id'   => \Auth::user()->tenant_id,
        'name'        => $faker->text,
        'description' => $faker->text,
    ];
});

$factory->state(App\Models\Venue::class, 'with-splash-pages', function (Faker\Generator $faker) {
    /** @var \App\Models\SplashPage $preLogin */
    $preLogin = factory(\App\Models\SplashPage::class)->create();

    /** @var \App\Models\SplashPage $postLogin */
    $postLogin = factory(\App\Models\SplashPage::class)->create([
        'type' => \App\Models\SplashPage::TYPE_ONLINE,
    ]);

    return [
        'login_page_id'      => $preLogin->id,
        'post_login_page_id' => $postLogin->id,
    ];
});

$factory->define(App\Models\Zone::class, function (Faker\Generator $faker) {
    return [
        'parent_id' => factory(\App\Models\Venue::class)->create()->id,
        'name'      => $faker->text,
    ];
});

$factory->state(App\Models\Zone::class, 'with-splash-pages', function (Faker\Generator $faker) {
    /** @var \App\Models\SplashPage $preLogin */
    $preLogin = factory(\App\Models\SplashPage::class)->create();

    /** @var \App\Models\SplashPage $postLogin */
    $postLogin = factory(\App\Models\SplashPage::class)->create([
        'type' => \App\Models\SplashPage::TYPE_ONLINE,
    ]);

    return [
        'login_page_id'      => $preLogin->id,
        'post_login_page_id' => $postLogin->id,
    ];
});

$factory->define(App\Models\Campaign::class, function (Faker\Generator $faker) {
    return [
        'tenant_id'           => \Auth::user()->tenant_id,
        'title'               => $faker->text(),
        'parameter_type'      => \App\Models\Campaign::PARAMETER_TYPE_DATE_TIME,
        'parameter_type_date' => '2016-10-20',
        'parameter_type_time' => config('times.time.3'),
    ];
});

$factory->define(App\Models\SmsProvider::class, function (Faker\Generator $faker) {
    return [
        'tenant_id'   => \Auth::user()->tenant_id,
        'name'        => 'Test SMS Provider',
        'description' => 'Test SMS provider description',
        'gateway_id'  => \App\Models\SmsProvider::GATEWAY_CLICKATELL,
        'parameters'  => [
            'api_id'   => 'API ID',
            'username' => 'Username',
            'password' => 'Password',
        ],
    ];
});

$factory->define(App\Models\SplashPage::class, function (Faker\Generator $faker) {
    return [
        'tenant_id' => \Auth::user()->tenant_id,
        'type'      => \App\Models\SplashPage::TYPE_OFFLINE,
        'name'      => $faker->text,
    ];
});

$factory->define(App\Models\ProximityMarketingCampaign::class, function (Faker\Generator $faker) {
    /** @var \App\Models\Venue $venue */
    $venue = factory(\App\Models\Venue::class)->create();

    return [
        'tenant_id'      => \Auth::user()->tenant_id,
        'title'          => 'Test Proximity Marketing Campaign',
        'parameter_type' => \App\Models\ProximityMarketingCampaign::PARAMETER_TYPE_VISIT,
        'start_date'     => '1970-01-01',
        'end_date'       => '2016-12-30',
        'frequency'      => \App\Models\ProximityMarketingCampaign::FREQUENCY_TYPE_DAY,
        'location_id'    => $venue->id,
    ];
});