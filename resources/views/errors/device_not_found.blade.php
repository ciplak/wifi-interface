@extends('errors.layout')

@section('title')
    Something went wrong
@endsection

@section('details')
    Couldn&#039;t find access point "<?php echo $error->getMac()?>". Please check it&#039;s configured properly.
@endsection