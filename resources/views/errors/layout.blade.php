<html id="error-page">
<head>
    <title>Be right back.</title>
    <style type="text/css">

        body, html {
            height: 100%;
            color: #6d6d6d;
        }

        body {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 1.428571429;
            background-color: #fff;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            display: table;
            font-weight: 100;
        }

        .container {
            color:#fff;
            text-align: center;
            vertical-align: middle;
        }

        h1 {
            font-size: 4em;
            margin-top: 0;
        }

        .main {
            background: #466069;
            padding: 100px 20px;
        }

        .details{
            font-size: 2em;
        }
    </style>
</head>
<body>
<div class="main">
    <div class="container">
        <h1>@yield('title')</h1>

        <div class="details">@yield('details')</div>
    </div>
</div>
</body>
</html>
