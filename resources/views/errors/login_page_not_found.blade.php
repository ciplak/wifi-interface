@extends('errors.layout')

@section('title')
    Something went wrong
@endsection

@section('details')
    Couldn&#039;t find login page for {{ $error->getProperty()->name }}. Please check if the login page exists.
@endsection