@extends('errors.layout')

@section('title')
    Something went wrong
@endsection

@section('details')
    There has been a problem retrieving information about your session. Possible causes of this problem could include: visiting this URL directly, cookies being blocked by your browser, clicking the back button in your browser, or a misconfigured portal. Please try browsing the web. If you are not already authenticated, you will be directed back to the start of the access journey.
@endsection
