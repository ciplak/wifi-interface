<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 */
echo Form::open(['autocomplete' => 'off']) ?>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo trans('messages.admin_login') ?></h4>
            </div>

            <div class="panel-body">

                <?php if ($errors->count()): ?>
                    <div class="alert alert-danger">
                        <?php echo implode('<br>', $errors->all()) ?>
                    </div>
                <?php endif ?>

                <div class="form-group">
                    <?php echo Form::text('email', \Request::old('email'), ['class' => 'form-control', 'id' => 'email', 'placeholder' => trans('messages.email'), 'autofocus']) ?>
                </div>

                <div class="form-group">
                    <?php echo Form::password('password', ['class' => 'form-control', 'placeholder' => trans('messages.password')]) ?>
                </div>
            </div>

            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-primary btn-large"><?php echo trans('messages.login') ?></button>
            </div>
        </div>
    </div>
</div>
<?php echo Form::close() ?>