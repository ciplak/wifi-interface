<div id="map" style="height: 800px;"></div>


<script>
    var data = <?php
        /** @var \Illuminate\Support\Collection */
        echo $data
        ?>;
    function initMap() {

        var center = new google.maps.LatLng(41.020077867771, 28.890107246116);

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: center
        });

        var  markers = [];
        for (var i = 0; i < data.length - 1; i++) {
            var latLng = new google.maps.LatLng(data[i].lat, data[i].lng);

            var marker = new google.maps.Marker({'position': latLng});
            markers.push(marker);
        }

        var circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.1,
            map: map,
            center: center,
            radius: <?php echo $radius ?>
        });

        var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://googlemaps.github.io/js-marker-clusterer/images/m'});

    }
</script>
<script src="https://googlemaps.github.io/js-marker-clusterer/src/markerclusterer.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=<?php echo env('GOOGLE_MAPS_KEY') ?>&callback=initMap"></script>