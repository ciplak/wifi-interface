<?php
/**
 * @var \App\Models\Advertisement $advertisement
 * @var array $htmlAttributes
 */
?>
<div<?php echo \HTML::attributes($htmlAttributes) ?>>
    <!--<div class="advertisement-title">
        <?php echo e($advertisement->title) ?>
    </div>-->

    <div class="advertisement-body">
        <?php if ($advertisement->type == \App\Models\Advertisement::TYPE_TEXT): ?>
          <div class="advertisement-text">
            <?php if ($advertisement->url): ?>
                <a href="<?php echo $advertisement->url ?>">
                    <?php echo nl2br(e($advertisement->body)) ?>
                </a>
            <?php else: ?>
                <?php echo nl2br(e($advertisement->body)) ?>
            <?php endif ?>
          </div>
        <?php else: ?>
            <div class="advertisement-image-container">
                <?php if ($advertisement->url): ?>
                    <a href="<?php echo $advertisement->url ?>">
                        <img src="<?php echo $advertisement->image_url ?>" alt="<?php echo $advertisement->title ?>" class="advertisement-image">
                    </a>
                <?php else: ?>
                    <img src="<?php echo $advertisement->image_url ?>" alt="<?php echo $advertisement->title ?>" class="advertisement-image">
                <?php endif ?>
            </div>
        <?php endif ?>
    </div>
</div>
