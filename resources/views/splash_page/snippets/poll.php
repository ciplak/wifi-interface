<?php
/**
 * @var \App\Models\Poll $poll
 */
?>
<div class="poll-container">
    <div class="poll-title">
        <?php echo e($poll->title) ?>
    </div>

    <div class="poll-options">
        <?php if ($poll->options): ?>
            <ul>
            <?php foreach ($poll->options as $option): ?>
                <li><?php echo e($option->title) ?></li>
            <?php endforeach; ?>
            </ul>
        <?php endif ?>
    </div>
</div>
