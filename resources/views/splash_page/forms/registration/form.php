<?php
/**
 * @var array $htmlAttributes
 * @var array $translations
 * @var array $masks
 * @var array $options
 */
?>
<registration-form inline-template :country-code="'<?php echo config('front_page_settings.default_country_phone_code') ?>'" :max-resend="<?php echo (int)config('front_page_settings.access_code.max_resend_count', 3) ?>" :counter-start="<?php echo (int)config('front_page_settings.access_code.resend_wait_time', 30) ?>">
    <div<?php echo \HTML::attributes($htmlAttributes) ?>>

        <div class="please-wait-text" v-if="redirecting">
            <?php echo trans('login.please_wait') ?>
        </div>

        <div v-if="error_message" class="alert alert-danger">
            {{ error_message }}
        </div>

        <div v-if="message" class="alert alert-warning">
            {{ message }}
        </div>

        <form v-if="!require_code" @submit.prevent="submit" autocomplete="on">
            <?php
            /** @var \App\Models\SplashPage $splashPage */
            $enabledFields = $splashPage->getEnabledRegistrationFields();
            \App\Services\FormRenderer::render($enabledFields, 'register-form-', $masks, $options)
            ?>

            <div class="form-group" :class="{'has-error': form.errors.has('accept_terms_and_conditions')}">

                <label id="registration-form-terms-and-conditions-label" class="terms-and-conditions-label">
                    <input id="registration-form-terms-and-conditions-checkbox" type="checkbox" v-model="form.accept_terms_and_conditions" :true-value="1" :false-value="null"> <?php echo trans('login.accept_terms_and_conditions', ['termsAndConditions' => '<span id="registration-form-terms-and-conditions-text">' . trans('login.terms_and_conditions') . '</span>']) ?>
                </label>
                <span class="help-block">{{ form.errors.get('accept_terms_and_conditions') }}</span>

            </div>

            <div class="form-group">
                <button :disabled="form.busy || redirecting" type="submit" class="btn submit-button" id="registration-form-submit-button">
                    <?php echo count($enabledFields) ? $translations['register'] : $translations['continue'] ?>
                </button>
            </div>
        </form>

        <div v-else>
            <form @submit.prevent="checkCode">

                <div class="form-group" :class="{'has-error': form.errors.has('code')}">
                    <label for="register-code" class="required"><?php echo trans('login.access_code') ?></label>
                    <input type="text" class="form-control" id="register-code" placeholder="<?php echo trans('login.access_code') ?>" required v-model="form.code">
                    <span class="help-block">{{ form.errors.get('code') }}</span>
                </div>

                <div class="form-group">
                    <button :disabled="form.busy || redirecting" type="submit" class="btn submit-button" id="register-form-submit-button"><?php echo $translations['login'] ?></button>
                </div>

                <div v-if="resend_enabled" class="form-group">
                    <button @click="resend" :disabled="form.busy || redirecting || !can_resend" type="button" class="btn submit-button" id="register-form-resend-button"><?php echo trans('login.resend_access_code') ?></button>

                    <div v-if="!can_resend" class="form-group resend-remaining-time-message" id="register-form-resend-remaining-time-message">
                        <?php echo trans('splash_page.resend_remaining_time_messages'); ?>
                    </div>
                </div>

            </form>
        </div>

    </div>
</registration-form>