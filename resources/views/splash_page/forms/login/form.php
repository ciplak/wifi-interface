<?php
/**
 * @var array $htmlAttributes
 * @var array $translations
 * @var array $masks
 * @var array $options
 */
?>
<login-form :country-code="'<?php echo config('front_page_settings.default_country_phone_code') ?>'" inline-template>
    <div<?php echo \HTML::attributes($htmlAttributes) ?>>

        <div class="please-wait-text" v-if="redirecting">
            <?php echo trans('login.please_wait') ?>
        </div>

        <div v-if="error_message" class="alert alert-danger">
            {{ error_message }}
        </div>

        <form @submit.prevent="submit" autocomplete="off">

            <?php
            /** @var \App\Models\SplashPage $splashPage */
            \App\Services\FormRenderer::render($splashPage->getEnabledLoginFields(), 'login-form-', $masks, $options)
            ?>

            <div class="form-group" :class="{'has-error': form.errors.has('code')}">
                <label for="login-form-code" class="required"><?php echo trans('login.access_code') ?></label>
                <input type="text" class="form-control" id="login-form-code" placeholder="<?php echo trans('login.access_code')?>" required v-model="form.code">
                <span class="help-block">{{ form.errors.get('code') }}</span>
            </div>

            <div class="form-group">
                <button :disabled="form.busy || redirecting" type="submit" class="btn submit-button" id="login-form-submit-button"><?php echo $translations['login'] ?></button>
            </div>
        </form>

    </div>

</login-form>