<?php
/**
 * @var \App\Models\SplashPage $splashPage
 * @var string                 $title
 */
$title = isset($splashPage->title) && isset($splashPage->title[App::getLocale()]) ? $splashPage->title[App::getLocale()] : '';
?>
<!DOCTYPE html>
<html lang="<?php echo App::getLocale() ?>" dir="<?php echo isRTL() ? 'rtl' : 'ltr' ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo e($title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo asset('assets/img/favicon/favicon.ico') ?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo asset('assets/img/favicon/apple-icon-57x57.png') ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo asset('assets/img/favicon/apple-icon-60x60.png') ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset('assets/img/favicon/apple-icon-72x72.png') ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo asset('assets/img/favicon/apple-icon-76x76.png') ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset('assets/img/favicon/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo asset('assets/img/favicon/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo asset('assets/img/favicon/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo asset('assets/img/favicon/apple-icon-152x152.png') ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo asset('assets/img/favicon/apple-icon-180x180.png') ?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo asset('assets/img/favicon/android-icon-192x192.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo asset('assets/img/favicon/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo asset('assets/img/favicon/favicon-96x96.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo asset('assets/img/favicon/favicon-16x16.png') ?>">

    <?php echo HTML::style('assets/dist/app.css') ?>
    <?php echo HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css') ?>
    <?php echo HTML::script('assets/dist/app.js') ?>

    <style>
        html, body {
            background-color: rgb(236, 240, 245);
            min-height: 100%;
            padding: 0;
            margin: 0;
            font-family: 'Source Sans Pro', "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        nav {
            background-color: #fff
        }

        iframe {
            display: block;
            overflow: auto;
            border: 0;
            padding: 0;
            margin: 0 auto;
        }

        .frame {
            height: 49px;
            margin: 0;
            padding: 0;
            border-bottom: 1px solid #ddd;
        }

        .frame a {
            color: #666;
        }

        .frame a:hover {
            color: #222;
        }

        .frame .buttons a {
            height: 49px;
            line-height: 49px;
            display: inline-block;
            text-align: center;
            width: 50px;
            border-left: 1px solid #ddd;
        }

        .frame .brand {
            color: #444;
            font-size: 20px;
            line-height: 49px;
            display: inline-block;
            padding-left: 10px;
        }

        .frame .brand small {
            font-size: 14px;
        }

        a, a:hover {
            text-decoration: none;
        }

        .container-fluid {
            padding: 0;
            margin: 0;
        }

        .text-muted {
            color: #999;
        }

    </style>
</head>

<body>

<header>
    <nav class="frame" role="navigation">
        <div class="container-fluid">
                <span class="brand">
                    Preview - <?php echo $splashPage->name ?>
                </span>
            <div class="buttons pull-right">
                <a class="first" href="<?php echo route('login.preview', ['loginPage' => $splashPage, 'inside' => 1]) ?>" title="Remove frame"><i class="fa fa-times fa-lg"></i></a>
                <a class="hidden-xs" id="display-full" href="#" data-toggle="tooltip" data-placement="bottom" title="Display Desktop"><i class="fa fa-desktop fa-lg"></i></a>
                <a class="hidden-xs" id="display-940" href="#" data-toggle="tooltip" data-placement="bottom" title="Display Tablet"><i class="fa fa-tablet fa-lg"></i></a>
                <a class="hidden-xs" id="display-480" href="#" data-toggle="tooltip" data-placement="bottom" title="Display Mobile"><i class="fa fa-mobile fa-lg"></i></a>
            </div>
        </div><!-- /.container -->
    </nav><!--/.navbar-->
</header>

<iframe src="<?php echo route('login.preview', ['loginPage' => $splashPage, 'inside' => 1]) ?>" id="preview-iframe"></iframe>

<script>
    $(function () {
        function _fix() {
            var h = $(window).height();
            var w = $(window).width();
            $("#preview-iframe").css({
                width: w + "px",
                height: (h - 50) + "px"
            });
        }

        _fix();
        $(window).resize(function () {
            _fix();
        });
        //$('[data-toggle="tooltip"]').tooltip();

        function iframe_width(width) {
            $("#preview-iframe").animate({width: width}, 500);
        }

        $("#display-full").click(function (e) {
            e.preventDefault();
            iframe_width("100%");
        });

        $("#display-940").click(function (e) {
            e.preventDefault();
            iframe_width("940px");
        });

        $("#display-480").click(function (e) {
            e.preventDefault();
            iframe_width("480px");
        });

    });

</script>
</body>
</html>