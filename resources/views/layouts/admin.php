<!DOCTYPE html>
<html lang="<?php echo Config::get('app.locale') ?>" dir="<?php echo isRTL() ? 'rtl' : 'ltr'?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="csrf-token" name="csrf-token" content="<?php echo csrf_token() ?>">

    <link rel="icon" href="<?php echo asset('assets/img/favicon/favicon.ico') ?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo asset('assets/img/favicon/apple-icon-57x57.png') ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo asset('assets/img/favicon/apple-icon-60x60.png') ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset('assets/img/favicon/apple-icon-72x72.png') ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo asset('assets/img/favicon/apple-icon-76x76.png') ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset('assets/img/favicon/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo asset('assets/img/favicon/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo asset('assets/img/favicon/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo asset('assets/img/favicon/apple-icon-152x152.png') ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo asset('assets/img/favicon/apple-icon-180x180.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo asset('assets/img/favicon/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo asset('assets/img/favicon/favicon-96x96.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo asset('assets/img/favicon/favicon-16x16.png') ?>">

    <title><?php echo config('app.name') ?></title>

    <?php echo HTML::style(elixir(isRTL() ? 'assets/dist/backend-rtl.css' : 'assets/dist/backend.css', '')) ?>
    <?php echo view('layouts.admin._scripts')->render() ?>
</head>

<body class="hold-transition sidebar-mini sidebar-collapse <?php if (isRTL()) echo 'rtl' ?>">
    <div id="app" v-cloak>
        <div>

            <?php echo view('layouts.admin._navigation')->render() ?>

            <div class="wrapper">

                <?php echo view('layouts.admin._sidebar')->render() ?>

                <div class="content-wrapper">

                    <?php
                    /** @var bool $maintenanceMode */
                    if ($maintenanceMode): ?>
                        <div class="alert alert-warning text-center"><?php echo trans('settings.maintenance_mode_text') ?></div>
                    <?php endif ?>

                    <?php //echo view('layouts.admin._page_header') ?>

                    <section class="content">

                        <?php
                        /** @var string $layout_content */
                        echo $layout_content ?>

                    </section><!-- /.content -->
                </div><!-- /.content-wrapper -->

                <footer class="main-footer no-print">
                    <div class="pull-right hidden-xs">
                        <strong>Version</strong> <?php echo config('app.version') ?>
                    </div>
                    <strong><?php echo copyright() ?> All rights reserved.
                </footer>

            </div><!-- /.wrapper -->

            <div v-show="loading" id="loading">
                <div><i class="fa fa-refresh fa-spin"></i> <?php echo trans('messages.loading') ?></div>
            </div>

        </div>
    </div><!-- /app -->

<?php echo view('admin.session_expired_modal')?>

<?php echo HTML::script('assets/dist/backend-vue.js') ?>

<script>
$(function() {
    <?php foreach (['success', 'error'] as $flashType):
        if ($message = Session::get($flashType)) :
            echo "toastr.$flashType('$message');";
            Session::forget($flashType);
        endif;
    endforeach ?>
});
</script>
<?php \App\Services\Scripts::render() ?>
</body>
</html>
