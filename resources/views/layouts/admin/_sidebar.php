<aside class="main-sidebar">

    <section class="sidebar">
        <ul class="sidebar-menu">
                <li class="hidden-xs">
                    <a href="#" data-toggle="offcanvas" role="button" data-tooltip="true" data-placement="bottom" title="<?php echo trans('messages.toggle_sidebar') ?>">
                        <i class="fa fa-bars"></i> <span><?php echo trans('messages.toggle_sidebar') ?></span>
                    </a>
                </li>

                <li<?php echo ' class="', active_class(if_route(['admin.index'])), '"' ?>><a href="<?php echo route('admin.index') ?>"><i class='fa fa-dashboard'></i> <span><?php echo trans('dashboard.page_title') ?></span></a></li>

                <?php if (\Entrust::can(['venues.manage', 'guests.manage', 'devices.manage'])): ?>
                <li class="treeview <?php echo active_class(if_route_pattern(['admin.venue.*', 'admin.guest.*', 'admin.device.*'])) ?>">
                    <a href="#"><i class='fa fa-sitemap'></i> <span><?php echo trans('messages.organization') ?></span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <?php if (\Entrust::can('guests.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.guest.*'])), '"' ?>><a href="<?php echo route('admin.guest.index') ?>"><?php echo trans('guest.page_title') ?></a></li><?php endif ?>
                        <?php if (\Entrust::can('venues.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.venue.*'])), '"' ?>><a href="<?php echo route('admin.venue.index') ?>"><?php echo trans('venue.page_title') ?></a></li><?php endif ?>
                        <?php if (\Entrust::can('devices.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.device.*'])), '"' ?>><a href="<?php echo route('admin.device.index') ?>"><?php echo trans('device.page_title') ?></a></li><?php endif ?>
                    </ul>
                </li>

                <?php if (\Entrust::can(['service-profiles.manage', 'access-codes.*', 'login-pages.manage'])): ?>
                <li class="treeview <?php echo active_class(if_route_pattern(['admin.service_profile.*', 'admin.access_code.*', 'admin.splash_page.*'])) ?>">
                    <a href="#"><i class='fa fa-link'></i> <span><?php echo trans('messages.internet') ?></span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <?php if (\Entrust::can('access-codes.*')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.access_code.*'])), '"' ?>><a href="<?php echo route('admin.access_code.index') ?>"><?php echo trans('access_code.page_title') ?></a></li><?php endif ?>
                        <?php if (\Entrust::can('login-pages.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.splash_page.*'])), '"' ?>><a href="<?php echo route('admin.splash_page.index') ?>"><?php echo trans('splash_page.page_title') ?></a></li><?php endif ?>
                        <?php if (\Entrust::can('service-profiles.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.service_profile.*'])), '"' ?>><a href="<?php echo route('admin.service_profile.index') ?>"><?php echo trans('service_profile.page_title') ?></a></li><?php endif?>
                    </ul>
                </li>
                <?php endif ?>

                <?php if (\Entrust::can('marketing')): ?>
                <li class="treeview <?php echo active_class(if_route_pattern(['admin.advertisement.*', 'admin.proximity_marketing.*'])) ?>">
                    <a href="#"><i class='fa fa-bullhorn'></i> <span><?php echo trans('messages.marketing') ?></span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.advertisement.*'])), '"' ?>><a href="<?php echo route('admin.advertisement.index') ?>"><?php echo trans('advertisement.page_title') ?></a></li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.proximity_marketing.*'])), '"' ?>><a href="<?php echo route('admin.proximity_marketing.index') ?>"><?php echo trans('proximity_marketing.page_title') ?></a></li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.campaign.*'])), '"' ?>><a href="<?php echo route('admin.campaign.index') ?>"><?php echo trans('campaign.page_title') ?></a></li>
                    </ul>
                </li>
                <?php endif ?>

                <?php if (\Entrust::can('reports.view')): ?>
                <li class="treeview <?php echo active_class(if_route_pattern(['admin.report.*'])) ?>">
                    <a href="#"><i class='fa fa-line-chart'></i> <span><?php echo trans('report.page_title') ?></span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"> <?php echo trans('report.guests') ?>
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.guest'])), '"' ?>><a href="<?php echo route('admin.report.guest') ?>"><?php echo trans('report.guest_list') ?></a></li>
                                <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.guest_sessions'])), '"' ?>><a href="<?php echo route('admin.report.guest_sessions') ?>"><?php echo trans('report.guest_sessions') ?></a></li>
                                <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.impression'])), '"' ?>><a href="<?php echo route('admin.report.impression') ?>"><?php echo trans('report.impressions') ?></a></li>
                                <?php if (\Entrust::can(['reports.visited_urls.view'])): ?>
                                <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.visited_urls'])), '"' ?>><a href="<?php echo route('admin.report.visited_urls') ?>"><?php echo trans('report.visited_urls') ?></a></li>
                                <?php endif ?>
                            </ul>
                        </li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.demographics'])), '"' ?>><a href="<?php echo route('admin.report.demographics') ?>"><?php echo trans('report.demographics') ?></a></li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.presence'])), '"' ?>><a href="<?php echo route('admin.report.presence') ?>"><?php echo trans('report.presence.title') ?></a></li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.loyalty'])), '"' ?>><a href="<?php echo route('admin.report.loyalty') ?>"><?php echo trans('report.loyalty.title') ?></a></li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.trendAnalysis'])), '"' ?>><a href="<?php echo route('admin.report.trendAnalysis') ?>"><?php echo trans('report.trend_analysis.title') ?></a></li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.dailyActivity'])), '"' ?>><a href="<?php echo route('admin.report.dailyActivity') ?>"><?php echo trans('report.daily_activity') ?></a></li>
                        <li<?php echo ' class="', active_class(if_route_pattern(['admin.report.deviceUsageShares'])), '"' ?>><a href="<?php echo route('admin.report.deviceUsageShares') ?>"><?php echo trans('report.device_usage_shares') ?></a></li>
                    </ul>
                </li>
                <?php endif?>

                <?php if (\Entrust::can(['sms-providers.manage', 'users.manage', 'settings.update', 'media.manage'])): ?>
                <li class="treeview <?php echo active_class(if_route_pattern(['admin.user.*', 'admin.setting.*', 'admin.sms_provider.*', 'admin.media.*'])) ?>">
                    <a href="#"><i class='fa fa-cog'></i> <span><?php echo trans('messages.management') ?></span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <?php if (\Entrust::can('users.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.user.*'])), '"' ?>><a href="<?php echo route('admin.user.index') ?>"><?php echo trans('user.page_title') ?></a></li><?php endif?>
                        <?php if (\Entrust::can('settings.update')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.setting.*'])), '"' ?>><a href="<?php echo route('admin.setting.index') ?>"><?php echo trans('settings.page_title') ?></a></li><?php endif ?>
                        <?php if (\Entrust::can('sms-providers.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.sms_provider.*'])), '"' ?>><a href="<?php echo route('admin.sms_provider.index') ?>"><?php echo trans('sms_provider.page_title') ?></a></li><?php endif?>
                        <?php if (\Entrust::can('media.manage')): ?><li<?php echo ' class="', active_class(if_route_pattern(['admin.media.*'])), '"' ?>><a href="<?php echo route('admin.media.index') ?>"><?php echo trans('media.title') ?></a></li><?php endif?>
                    </ul>
                </li>
                <?php endif ?>

                <?php endif ?>
            </ul><!-- /.sidebar-menu -->
    </section><!-- /sidebar -->

</aside><!-- /main-sidebar -->