<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    window.App = {
        csrfToken: '<?php echo csrf_token() ?>',
        echo: {enabled: false},
        pusher: {key: '<?php echo env('PUSHER_KEY') ?>' },
        apiRoot: '<?php echo url('api/v1/')?>',
        locale: '<?php echo App::getLocale() ?>',
        user: {
            tenantId: <?php echo \Auth::guest() ? 0 : \Auth::user()->tenant_id ?>,
            token: '<?php echo \Auth::guest() ? '' : \Auth::user()->api_token?>',
        }
    }
</script>
<?php echo HTML::script(elixir('assets/dist/backend.js', '')) ?>