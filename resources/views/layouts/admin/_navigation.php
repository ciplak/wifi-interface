<navbar inline-template>
    <header class="main-header">
        <nav class="navbar navbar-default navbar-fixed-top">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>

                    <button type="button" class="navbar-toggle sidebar-toggle" data-toggle="offcanvas" role="button" data-tooltip="true" data-placement="bottom" title="<?php echo trans('messages.toggle_sidebar') ?>">
                        <span class="sr-only"><?php echo trans('messages.toggle_sidebar') ?></span>
                        <i class="fa fa-bars"></i>
                    </button>

                    <a class="navbar-brand" href="<?php echo route('admin.index') ?>">
                        <?php if ($logoUrl = logo_url()): ?>
                            <img src="<?php echo $logoUrl ?>" alt="" style="height:40px; width: auto">
                        <?php endif ?>
                    </a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right flip">
                        <?php if (\Entrust::can('developer.menu')): ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i> Dev<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <?php if (\Entrust::can('settings.global')): ?>
                                    <li><a href="<?php echo route('admin.setting.global') ?>"><i class="fa fa-gear"></i> <?php echo trans('messages.settings') ?></a></li>
                                    <li class="divider"></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('roles.manage')): ?>
                                    <li><a href="<?php echo route('admin.role.index') ?>"><i class="fa fa-users"></i> <?php echo trans('role.page_title') ?></a></li>
                                    <li><a href="<?php echo route('admin.role.permissions') ?>"><i class="fa fa-unlock-alt"></i> <?php echo trans('messages.permissions') ?></a></li>
                                    <li class="divider"></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('tenants.manage')): ?>
                                    <li><a href="<?php echo route('admin.tenant.add')?>"><i class="fa fa-plus"></i> Add tenant</a></li>
                                    <li><a href="<?php echo route('admin.tenant.index') ?>"><i class="fa fa-file-text-o"></i> <?php echo trans('tenant.page_title') ?></a></li>
                                    <li class="divider"></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('logs.view')): ?>
                                    <li><a href="<?php echo route('admin.logs') ?>" target="logs"><i class="fa fa-file-text-o"></i> <?php echo trans('messages.system_logs') ?></a></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('activity-logs.view')): ?>
                                    <li><a href="<?php echo route('admin.activity_log.index') ?>"><i class="fa fa-list-alt"></i> <?php echo trans('messages.activity_logs') ?></a></li>
                                    <li class="divider"></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('device_vendors.manage')): ?>
                                    <li><a href="<?php echo route('admin.device_vendor.index') ?>"><i class="fa fa-wrench"></i> <?php echo trans('device_vendor.page_title') ?></a></li>
                                    <li class="divider"></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('currencies.manage')): ?>
                                    <li><a href="<?php echo route('admin.currency.index') ?>"><i class="fa fa-money"></i> <?php echo trans('currency.page_title') ?></a></li>
                                    <li class="divider"></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('radius_servers.manage')): ?>
                                    <li><a href="<?php echo route('admin.radius_server.index') ?>"><i class="fa fa-server"></i> <?php echo trans('radius_server.page_title') ?></a></li>
                                    <li class="divider"></li>
                                    <?php endif ?>

                                    <?php if (\Entrust::can('translations.manage')): ?>
                                    <li><a href="<?php echo url('admin/translations') ?>"><i class="fa fa-flag"></i> Translations </a></li>
                                    <?php endif ?>
                                </ul>
                            </li>
                        <?php endif ?>

                        <?php if (\Entrust::can('alert-messages.view')): ?>
                            <notifications inline-template>
                                <li class="dropdown notifications-menu">
                                    <!-- Menu toggle button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="true" data-placement="bottom" title="<?php echo trans('alert_message.page_title') ?>">
                                        <i class="fa fa-bell-o"></i> <span class="visible-xs-inline"><?php echo trans('alert_message.page_title') ?></span>
                                        <span class="label label-danger" style="    position: absolute;
            top: 12px;
            right: 5px;
            text-align: center;
            font-size: 9px;
            padding: 2px 3px;
            line-height: .9;">{{ count }}</span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li class="header">You have {{ count }} notifications</li>
                                        <li v-if="count">
                                            <ul class="menu">
                                                <li v-for="item in items"><a href="#">{{ item.message }}</a></li>
                                            </ul>
                                        </li v-if="count">
                                        <li class="footer"><a href="<?php echo route('admin.alert_message.index') ?>"><?php echo trans('alert_message.view_all') ?></a></li>
                                    </ul>

                                </li>
                            </notifications>
                        <?php endif ?>

                        <li>
                            <a href="<?php echo route('admin.about') ?>" data-tooltip="true" data-placement="bottom" title="<?php echo trans('messages.about') ?>">
                                <i class="fa fa-question-circle"></i> <span class="visible-xs-inline"><?php echo trans('messages.about') ?></span>
                            </a>
                        </li>

                        <?php if (\Auth::user()->isImpersonating()): ?>
                        <li>
                            <a href="<?php echo route('admin.impersonate.stop') ?>">
                                <i class="fa fa-user-secret"></i>
                                <?php echo trans('user.stop_impersonating') ?>
                            </a>
                        </li>
                        <?php endif ?>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <?php if (\Auth::user()->avatar) echo HTML::image(\Auth::user()->avatar, '', ['class' => 'user-image', 'style' => 'border-radius:25px'])  ?>
                                <?php echo Auth::user()->name ?>
                                <?php if (\Entrust::hasRole('developer')) echo ' - <strong>' . \Auth::user()->tenant->name . '</strong>' ?>
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo route('admin.user.profile') ?>"><i class="fa fa-user"></i> <?php echo trans('messages.profile') ?></a></li>
                                <li class="divider"></li>
                                <?php if (\Entrust::can('tenants.update-profile')): ?>
                                    <li>
                                        <a href="<?php echo route('admin.tenant.profile') ?>"><i class="fa fa-info-circle"></i>
                                            <?php echo trans('tenant.columns.info') ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <?php
                                    if (isset($tenants)):
                                        foreach ($tenants as $tenantId => $tenantName): ?>
                                            <li>
                                                <a href="<?php echo route('admin.switchTenant', [$tenantId])?>">
                                                    <?php echo \Auth::user()->tenant_id == $tenantId ? '<i class="fa fa-check"></i>' : '<i class="fa fa-fw">&nbsp;</i>'  ?>
                                                    <?php echo $tenantName ?>
                                                    <?php if (\Entrust::hasRole('developer')) echo ' - <strong>#', $tenantId, '</strong>' ?>
                                                </a>
                                            </li>
                                        <?php endforeach;
                                        echo '<li class="divider"></li>';
                                    endif ?>
                                <?php endif ?>
                                <li><a href="<?php echo route('logout') ?>" class="npj"><i class="fa fa-sign-out"></i> <?php echo trans('messages.logout') ?></a></li>
                            </ul>
                        </li>

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
    </header>
</navbar>