<?php
/**
 * @var array $venues
 * @var array $statuses
 */
echo Form::open(['class' => 'form-inline margin-bottom', 'method' => 'GET']) ?>

<?php if (\Entrust::can('guests.manage-global')): ?>
    <div class="form-group">
        <?php echo Form::select('venue', $venues, request('venue'), ['class' => 'form-control', 'id' => 'filter_venue', 'placeholder' => trans('venue.all')]) ?>
    </div>
<?php endif ?>

<div class="form-group">
    <?php echo Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.name')]) ?>
</div>

<div class="form-group">
    <?php echo Form::text('email', request('email'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.email')]) ?>
</div>

<div class="form-group">
    <?php echo Form::text('phone', request('phone'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.phone')]) ?>
</div>

<div class="form-group">
    <?php echo Form::text('mac', request('mac'), ['class' => 'form-control', 'placeholder' => trans('messages.mac_address')]) ?>
</div>

<div class="form-group">
    <?php echo Form::select('connection_status', $statuses, request('connection_status'), ['class' => 'form-control', 'placeholder' => trans('guest.connection_status.all')]) ?>
</div>

<a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
<button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>
<?php echo Form::close() ?>