<script>
$(function() {
    $('#btn-generate-code').click(function() {
        var button = $(this);
        button.button('loading');

        $.post('<?php echo route('admin.guest.generateAccessCode', [$model])?>', function(data) {
            if (data.status) {
                $('#access-codes').html(data.html);
                bootbox.alert('<p class="text-center">' + data.message + '</p><h3 class="text-center">' + data.code + '</h3>');
            } else {
                bootbox.alert('<?php echo trans('access_code.could_not_create')?>');
            }

            button.button('reset');
        }, 'json');
        return false;
    });
});
</script>