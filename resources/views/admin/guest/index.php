<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('guest.page_title') ?></h4>

            <div class="panel-control">
                <a href="<?php echo route('admin.guest.add') ?>"  class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
            </div>
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">
        <?php echo view('admin.guest.filters') ?>

        <?php echo view('admin.guest.grid') ?>
    </div><!-- /panel-body -->

</div><!-- /panel -->