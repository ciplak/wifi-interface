<div class="table-responsive">

    <?php
    /**
     * @var \App\Models\Guest[]|\Illuminate\Support\Collection $models
     */
    if ($models->isEmpty()): ?>

        <p class="alert alert-warning"><?php echo trans('guest.not_found') ?></p>

    <?php else: ?>

        <table class="table table-striped guest-list">
        <thead>
        <tr>
            <th class="col-avatar"></th>
            <th><?php echo trans('guest.columns.name') ?></th>
            <th><?php echo trans('guest.columns.location') ?></th>
            <th><?php echo trans('guest.columns.email') ?></th>
            <th><?php echo trans('guest.columns.phone') ?></th>
            <th><?php echo trans('guest.columns.connection_status') ?></th>
            <th><?php echo trans('guest.columns.gender') ?></th>
            <th><?php echo trans('guest.columns.last_visit') ?></th>
            <th><?php echo trans('guest.columns.total_visits') ?></th>
            <th><?php echo trans('messages.created_at') ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody id="user-table-body">

        <?php
        foreach ($models as $model): ?>
            <tr>
                <td><?php echo $model->avatar_picture ?></td>
                <td><?php echo HTML::linkRoute('admin.guest.details', $model->display_name, [$model]) ?></td>
                <td><?php echo $model->venue_name ?></td>
                <td><?php echo $model->email_link ?></td>
                <td><?php echo e($model->phone); ?></td>
                <td><span class="label label-<?php echo $model->connection_status_class?>"><?php echo $model->connection_status_value ?></span></td>
                <td><?php echo $model->gender_icon ?></td>
                <td><?php echo $model->last_visit_time_formatted ?></td>
                <td><?php echo $model->total_visits ?></td>
                <td><?php echo $model->created_at_formatted ?></td>
                <td class="text-right">
                    <?php if ($model->is_online): ?>
                        <a href="<?php echo route('admin.guest.disconnect', [$model]) ?>" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('guest.disconnect') ?>"><i class="fa fa-unlink"></i> </a>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach ?>

        </tbody>
    </table>

    <?php pagination($models) ?>

    <?php endif ?>

</div><!-- /table-responsive -->