<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\Guest $model
 * @var array $venues
 * @var array $genderOptions
 * @var array $nationalityOptions
 * @var array $directLoginOptions
 */
echo Form::model($model,
    ['class' => 'form-horizontal', 'autocomplete' => 'off', 'route' => ['admin.guest.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('guest.title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo $model->exists ? route('admin.guest.details', [$model]) : route('admin.guest.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>
            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">

        <?php if ($errors->count()): ?>
            <div class="alert alert-danger">
                <?php echo implode('<br>', $errors->all())?>
            </div>
        <?php endif ?>

        <?php if ($model->exists):?>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo trans('guest.columns.username') ?></label>
                <div class="col-md-4 form-control-static">
                    <?php echo e($model->username) ?>
                </div>
            </div>
        <?php endif ?>

        <?php if (\Entrust::can('guests.manage-global')): ?>
            <div class="form-group<?php if ($errors->has('venue_id')) echo ' has-error' ?>">
                <?php echo Form::label('venue_id', trans('guest.columns.location'), ['class' => 'control-label col-md-2 required']) ?>
                <div class="col-md-4">
                    <?php echo Form::select('venue_id', $venues, $model ? hashids_encode($model->venue_id) : null, ['class' => 'form-control select', 'placeholder' => trans('venue.select')])?>
                </div>
                <?php if ($errors->has('venue_id')): ?>
                    <div class="col-md-10 col-md-offset-2">
                        <span class="help-block"><?php echo $errors->first('venue_id') ?></span>
                    </div>
                <?php endif ?>
            </div>
        <?php endif ?>

        <div class="form-group">
            <?php echo Form::label('name', trans('guest.columns.name'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::text('name', null, ['class' => 'form-control', 'autofocus', 'placeholder' => trans('guest.full_name')]) ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo Form::label('email', trans('guest.columns.email'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::input('email', 'email', null, ['class' => 'form-control'])?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('birthday')) echo ' has-error' ?>">
            <?php echo Form::label('birthday', trans('guest.columns.birthday'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::text('birthday', $model->exists ? $model->birthday_picker_formatted : null, ['class' => 'form-control', 'data-mask', 'data-inputmask-alias' => 'date'])?>
            </div>
            <?php if ($errors->has('birthday')): ?>
                <div class="col-md-10 col-md-offset-2">
                    <span class="help-block"><?php echo $errors->first('birthday') ?></span>
                </div>
            <?php endif ?>
        </div>

        <div class="form-group">
            <?php echo Form::label('gender', trans('guest.columns.gender'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('gender', $genderOptions, null, ['class' => 'form-control', 'placeholder' => trans('guest.gender.select')])?>
            </div>
        </div>

        <div class="form-group">
            <?php echo Form::label('phone', trans('guest.columns.phone'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::input('tel', 'phone', null, ['class' => 'form-control'])?>
            </div>
        </div>

        <div class="form-group">
            <?php echo Form::label('nationality', trans('guest.columns.nationality'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('nationality', $nationalityOptions, null, ['class' => 'form-control select', 'placeholder' => trans('guest.nationality.select')])?>
            </div>
        </div>

        <div class="form-group">
            <?php echo Form::label('passport_no', trans('guest.columns.passport_no'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::text('passport_no', null, ['class' => 'form-control'])?>
            </div>
        </div>

        <div class="form-group">
            <?php echo Form::label('direct_login', trans('guest.columns.direct_login'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('direct_login', $directLoginOptions, null, ['class' => 'form-control'])?>
            </div>
        </div>

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>