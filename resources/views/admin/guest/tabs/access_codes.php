<h2 class="page-header"><?php echo trans('access_code.page_title') ?>
    <a href="#" class="a btn btn-success pull-right" id="btn-generate-code" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..."><?php echo trans('access_code.generate') ?></a>
</h2>

<div id="access-codes">
    <?php if ($model->accessCodes->count()): ?>
        <?php echo view('admin.access_code.grid', ['models' => $model->accessCodes, 'partial' => true])->render() ?>
    <?php endif ?>
</div>