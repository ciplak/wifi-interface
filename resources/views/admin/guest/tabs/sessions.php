<?php
/**
 * @var \App\Models\Guest $model
 * @var \App\Models\WifiSession[] $sessions
 * @var array $statuses
 */
?>
<div class="table-responsive">

    <?php if ($sessions->isEmpty()): ?>

        <p class="alert alert-warning"><?php echo trans('guest.session_not_found') ?></p>

    <?php else: ?>
        <?php if (\Entrust::can('reports.view')): ?>
        <div class="margin-bottom text-right">
            <a href="<?php echo route('admin.report.guest_sessions', ['name' => $model->username])?>" class="btn btn-default"><?php echo trans('report.guest_sessions') ?></a>
        </div>
        <?php endif ?>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th><?php echo trans('messages.start_time') ?></th>
                <th><?php echo trans('messages.session.duration') ?></th>
                <th><?php echo trans('messages.total_downloaded_data') ?></th>
                <th><?php echo trans('guest_device.columns.wifi_area') ?></th>
                <th><?php echo trans('device.hotspot') ?></th>
                <th><?php echo trans('guest.columns.connection_status') ?></th>
                <th><?php echo trans('service_profile.title') ?></th>
                <th><?php echo trans('device.columns.mac') ?></th>
                <th><?php echo trans('device.columns.ip') ?></th>
                <th><?php echo trans('access_code.columns.code') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($sessions as $session): ?>
                <tr>
                    <td><?php echo $session->start_time_formatted?></td>
                    <td><?php echo $session->total_duration ?></td>
                    <td><?php echo $session->downloaded_data_human_readable ?></td>
                    <td><?php echo $session->property_name ?></td>
                    <td><?php echo $session->device_name ?></td>
                    <td><?php echo $session->connection_status_icon ?></td>
                    <td><?php echo $session->serviceProfile ? HTML::linkRoute('admin.service_profile.edit', $session->serviceProfile->name_with_duration, ['serviceProfile' => $session->serviceProfile]) : '' ?></td>
                    <td><?php echo $session->guest_device_mac ? \HTML::linkRoute('admin.guest.index', $session->guest_device_mac, ['mac' => $session->guest_device_mac]) : '' ?></td>
                    <td><?php echo $session->guest_device_ip ?></td>
                    <td><?php echo $session->access_code ?></td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>

    <?php endif ?>

</div><!-- /table-responsive -->