<?php
/**
 * @var \App\Models\Guest $model
 * @var array $statuses
 */
?>

<div class="form-horizontal">
    <h2 class="page-header"><?php echo trans('guest.info') ?></h2>

    <div class="col-md-4">

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.location') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo $model->venue_name ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.connection_status') ?></label>
            <div class="col-sm-4 form-control-static">
                <span class="label label-<?php echo $model->connection_status_class ?>"><?php echo $model->connection_status_value ?></span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.name') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo e($model->name) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.username') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo e($model->username) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.phone') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo e($model->phone) ?>
            </div>
        </div>

        <?php if ($model->birthday): ?>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.birthday') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo $model->birthday_formatted, " ({$model->age})" ?>
            </div>
        </div>
        <?php endif ?>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.email') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo e($model->email) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.gender') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo $model->gender_icon ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.nationality') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo $model->nationality_name ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.passport_no') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo e($model->passport_no) ?>
            </div>
        </div>

    </div>

    <div class="col-md-4">

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.direct_login') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php if ($model->direct_login_enabled): ?>
                    <span class="label label-success"><?php echo trans('messages.yes') ?></span>
                <?php else: ?>
                    <span class="label label-danger"><?php echo trans('messages.no') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.last_visit') ?></label>

            <div class="col-sm-4 form-control-static">
                <?php echo $model->last_visit_time_formatted ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('guest.columns.total_visits') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo $model->total_visits ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('messages.total_downloaded_data') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo human_filesize($model->total_downloaded_data) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo trans('messages.total_uploaded_data') ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo human_filesize($model->total_uploaded_data) ?>
            </div>
        </div>

    </div>

    <div class="col-md-4">
        <?php if ($model->avatar): ?>
            <div><?php echo HTML::image($model->avatar, $model->name) ?></div>
        <?php endif ?>

        <?php if ($model->facebook_id): ?><a href="http://facebook.com/<?php echo $model->facebook_id ?>" target="_blank"><i class="icon-circle fa fa-facebook"></i></a><?php endif ?>
        <?php if ($model->twitter_id): ?><a href=" https://twitter.com/intent/user?user_id=<?php echo $model->twitter_id ?>" target="_blank"><i class="icon-circle fa fa-twitter"></i></a><?php endif ?>

        <?php for ($i = 1; $i <= Config::get('main.custom_field_count'); $i++): ?>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $model->custom_data["custom_field_{$i}"]['title'][App::getLocale()] ?? '' ?></label>
            <div class="col-sm-4 form-control-static">
                <?php echo e($model->custom_data["custom_field_{$i}"]['value']) ? : '&nbsp;' ?>
            </div>
        </div>
        <?php endfor ?>

    </div>

</div>

<div class="col-md-12">
    <?php echo view('admin.guest.tabs.devices', ['statuses' => $statuses])->render() ?>
</div>

<div class="col-md-12">
    <?php echo view('admin.guest.tabs.access_codes')->render() ?>
</div>