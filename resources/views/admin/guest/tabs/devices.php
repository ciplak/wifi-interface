<?php
/**
 * @var \App\Models\GuestDevice[]|\Illuminate\Support\Collection $devices
 */
?>
<h2 class="page-header"><?php echo trans('device.page_title') ?></h2>

<div class="table-responsive">

    <?php if ($devices->isEmpty()): ?>

        <p class="alert alert-warning"><?php echo trans('device.not_found') ?></p>

    <?php else: ?>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th><?php echo trans('guest_device.columns.type') ?></th>
                <th><?php echo trans('service_profile.title') ?></th>
                <th><?php echo trans('messages.mac_address') ?></th>
                <th><?php echo trans('messages.ip') ?></th>
                <th><?php echo trans('guest_device.columns.description') ?></th>
                <th><?php echo trans('guest_device.columns.platform') ?></th>
                <th><?php echo trans('guest_device.columns.browser') ?></th>
                <th><?php echo trans('guest.columns.connection_status') ?></th>
                <th><?php echo trans('messages.total_downloaded_data') ?></th>
                <th><?php echo trans('messages.total_uploaded_data') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($devices as $device): ?>
                <tr>
                    <td><?php echo $device->type_name ?></td>
                    <td><?php if ($device->serviceProfile) echo HTML::linkRoute('admin.service_profile.edit', $device->serviceProfile->name_with_duration, ['serviceProfile' => $device->serviceProfile])?></td>
                    <td><?php echo $device->mac ?></td>
                    <td><?php echo $device->ip ?></td>
                    <td><?php echo $device->description ?></td>
                    <td><?php echo $device->platform ?></td>
                    <td><?php echo $device->browser ?></td>
                    <td><?php echo $device->connection_status_icon ?></td>
                    <td><?php echo human_filesize($device->total_downloaded_data) ?></td>
                    <td><?php echo human_filesize($device->total_uploaded_data) ?></td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>

    <?php endif ?>

</div><!-- /table-responsive -->