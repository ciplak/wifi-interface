<?php
/**
 * @var \App\Models\Guest $model
 */

echo Form::open(['class' => 'form-horizontal', 'autocomplete' => 'off']) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('guest.page_title') ?></h4>

            <div class="panel-control">
                <a href="<?php echo route('admin.guest.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>
                <?php if ($model->is_online): ?>
                    <a href="<?php echo route('admin.guest.disconnect', [$model]) ?>" class="btn btn-default" data-tooltip="true" title="<?php echo trans('guest.disconnect') ?>"><i class="fa fa-unlink"></i> </a>
                <?php endif ?>
                <a href="<?php echo route('admin.guest.edit', [$model]) ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.edit') ?>"><i class="fa fa-edit"></i> </a>
            </div>
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">

        <div>

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php echo trans('messages.profile') ?></a></li>
                <!--<li role="presentation"><a href="#service-profiles" aria-controls="messages" role="tab" data-toggle="tab">--><?php //echo trans('device.page_title') ?><!--</a></li>-->
                <li role="presentation"><a href="#sessions" aria-controls="settings" role="tab" data-toggle="tab"><?php echo trans('guest.sessions') ?></a></li>
            </ul><!-- /nav-tabs -->

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="profile">
                    <?php echo view('admin.guest.tabs.profile')->render() ?>
                </div>

                <!--<div role="tabpanel" class="tab-pane" id="service-profiles">-->
                    <?php //echo view('admin.guest.tabs.devices', ['model' => $model, 'statuses' => $statuses])->render() ?>
                <!--</div>-->

                <div role="tabpanel" class="tab-pane" id="sessions">
                    <?php echo view('admin.guest.tabs.sessions', ['sessions' => $sessions])->render() ?>
                </div>

            </div><!-- /tab-content -->

        </div><!-- /tabs -->

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>