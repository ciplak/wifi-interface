<script>
    $(function () {
        var communication_channel_list = $("#communication-channel-list");
        communication_channel_list.sortable({
            axis: 'y',
            items: '.box',
            handle: '.box-header',
            placeholder: 'placeholder',
            forcePlaceholderSize: true,
            revert: 100,
            tolerance: 'pointer',

            update: function(event, ui) {
                $(ui.item).css('top', 0 + "px");
            },
            sort: function(event, ui) {
                var top = ui.item.offset().top;
                $(ui.item).css('top', top-250 + "px");
            }
        });

        var data = [],
            array = [],
            parameter_type = $("#parameter-type"),
            age_tab = $("#age-type"),
            number_of_visit_wrapper = $(".number-of-visit-wrapper"),
            visit_based_wrapper = $("#visit-based-wrapper"),
            visit_option_init_value = $("#visit_option_init_value").val(),
            weekdays_value = $("#weekdays_value").val();

        if (weekdays_value) {
            array = weekdays_value.split(',').map(function(n) {
                return Number(n);
            });
        }

        $(".has-active-class").on("click", function(){
            var key = $(this).data('key');
            var found = $.inArray(key, array) > -1;
            if (found) {
                array.splice($.inArray(key, array), 1);
            } else {
                array.push(key);
            }
            $("#weekdays_value").attr('value',array.toString());
        });



        $(".btn-primary").on("click", function () {
            var communication_channel_sort = [];
            $("#communication-channel-list .box").each(function () {
                communication_channel_sort.push($(this).data('key'));
            });

            $("#communication_channel").val(communication_channel_sort);
        });

        parameter_type.on("change", function () {
            var selected = parseInt($(this).val()),
                date_time_wrapper = $("#date-time-wrapper");
            if (selected == <?php echo \App\Models\ProximityMarketingCampaign::PARAMETER_TYPE_DATE_TIME ?>) {
                date_time_wrapper.show();
                visit_based_wrapper.hide();
            } else {
                date_time_wrapper.hide();
                visit_based_wrapper.show();
            }
        });

        age_tab.on("change", function(){
            var selected = parseInt($(this).val()),
                age_parameter = $("#age-parameter"),
                start_age = $("#s-age"),
                end_age = $("#e-age");

            if (selected == <?php echo \App\Models\ProximityMarketingCampaign::AGE_TYPE_UNKNOWN_AGE ?>) {
                age_parameter.hide();
                start_age.hide();
                end_age.hide();
            } else if (selected == <?php echo \App\Models\ProximityMarketingCampaign::AGE_TYPE_LESS_THAN ?> || selected == <?php echo \App\Models\ProximityMarketingCampaign::AGE_TYPE_MORE_THAN ?>) {
                age_parameter.show();
                start_age.show();
                end_age.hide();
            } else {
                age_parameter.show();
                start_age.show();
                end_age.show();
            }
        });

        $("#select-visit-option-<?php echo \App\Models\ProximityMarketingCampaign::PARAMETER_TYPE_VISIT_NUMBER_OF_VISITS ?>").on("click", function(){
            number_of_visit_wrapper.show();
        });

        $("#select-visit-option-<?php echo \App\Models\ProximityMarketingCampaign::PARAMETER_TYPE_VISIT_INCLUDE_VISITORS ?>").on("click", function(){
            number_of_visit_wrapper.hide();
        });

        parameter_type.trigger('change');
        age_tab.trigger('change');
        if(visit_option_init_value == <?php echo \App\Models\ProximityMarketingCampaign::PARAMETER_TYPE_VISIT_NUMBER_OF_VISITS ?>) {
            $("#select-visit-option-<?php echo \App\Models\ProximityMarketingCampaign::PARAMETER_TYPE_VISIT_NUMBER_OF_VISITS ?>").trigger('click');
        } else {
            $("#select-visit-option-<?php echo \App\Models\ProximityMarketingCampaign::PARAMETER_TYPE_VISIT_INCLUDE_VISITORS ?>").trigger('click');
        }

        $("#e-age").on("keyup", function(){
            data = initValues();
            ajaxRequest(data);
        });

        $("#s-age").on("keyup", function(){
            data = initValues();
            ajaxRequest(data);
        });

        $("#gender, #age-type").on("change", function() {
            data = initValues();
            ajaxRequest(data);
        });

        function initValues() {
            var data = [],
                gender = $("#gender").val(),
                ageType = $("#age-type").val(),
                ageStart = $("#s-age").val(),
                ageEnd = $("#e-age").val();
            data.push({gender : gender, age: ageType, start_age: ageStart, end_age: ageEnd});
            return data;
        }
        function ajaxRequest(data){
            $.ajax({
                type: "POST",
                url: "<?php echo route('admin.proximity_marketing.estimate') ?>",
                data: {'data': data},
                dataType: "json",
                success: function(response){
                    $("#estimated").text(response.value);
                },
                error: function(response){
//                    alert("Error:" + response.msg);
                }

            });
        }

    });
</script>