<?php
/**
 * @var \App\Models\ProximityMarketingCampaign[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('proximity_marketing.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.proximity_marketing.add') ?>" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
        </div>

    </div>

    <div class="panel-body">

        <?php
        /**
         * @var \App\Models\DeviceModel[]|\Illuminate\Database\Eloquent\Collection $models
         */
        ?>
        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('proximity_marketing.not_found') ?></p>

            <?php else: ?>

                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th><?php echo trans('proximity_marketing.columns.title') ?></th>
                        <th><?php echo trans('proximity_marketing.columns.event_type') ?></th>
                        <th><?php echo trans('proximity_marketing.columns.communication_channel') ?></th>
                        <th><?php echo trans('proximity_marketing.columns.status') ?></th>
                        <th class="col-actions"></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($models as $model): ?>
                        <tr>
                            <td><?php echo HTML::linkRoute('admin.proximity_marketing.edit', $model->title, [$model]) ?></td>
                            <td><?php echo $model->event_type_value ?></td>
                            <td><?php echo $model->communication_channel_value(true) ?></td>
                            <td><?php echo $model->status_icon ?></td>
                            <td class="col-actions">
                                <a href="<?php echo route('admin.proximity_marketing.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>

                </table>

                <?php pagination($models) ?>

            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->
