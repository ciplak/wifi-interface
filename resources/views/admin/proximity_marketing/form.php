<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\ProximityMarketingCampaign $model
 * @var array $statusOptions
 * @var array $locations
 * @var array $eventTypeOptions
 * @var array $ageTypeOptions
 * @var array $parameterTypeOptions
 * @var array $parameterTypeVisitOptions
 * @var array $frequenceTypeOptions
 * @var array $genderTypeOptions
 * @var array $communicationChannelTypeOptions
 */
echo Form::model($model, ['class' => 'form-horizontal', 'id' => 'form-proximity-marketing', 'autocomplete' => 'off', 'files' => true, 'route' => ['admin.proximity_marketing.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('proximity_marketing.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.proximity_marketing.index') ?>" class="btn btn-default" data-tooltip="true"
               title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

            <button class="btn btn-primary" type="submit" data-tooltip="true"
                    title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">
        <div class="form-group<?php if ($errors->has('title')) echo ' has-error' ?>">
            <?php echo Form::label('title', trans('proximity_marketing.columns.title'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-4">
                <?php echo Form::text('title', null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('title')): ?>
                    <span class="help-block"><?php echo $errors->first('title') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('location_id')) echo ' has-error' ?>">
            <?php echo Form::label('location_id', trans('proximity_marketing.columns.location_id'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::select('location_id', $locations, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('location_id')): ?>
                    <span class="help-block"><?php echo $errors->first('location_id') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('start_date') || $errors->has('end_date')) echo ' has-error' ?>">
            <?php echo Form::label('url', trans('proximity_marketing.columns.date'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-4">
                <div class="input-daterange input-group" id="date-range-picker">
                    <?php echo Form::text('start_date', $model->start_date_value, ['class' => 'form-control']) ?>
                    <span class="input-group-addon">to</span>
                    <?php echo Form::text('end_date', $model->end_date_value, ['class' => 'form-control']) ?>
                </div>
                <?php if ($errors->has('start_date') || $errors->has('end_date')): ?>
                    <span
                            class="help-block"><?php echo $errors->first('start_date'), ' ', $errors->first('end_date') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('event_type')) echo ' has-error' ?>">
            <label for="type" class="control-label col-md-2">
                <?php echo trans('proximity_marketing.columns.event_type') ?>
                <i class="fa fa-question-circle" data-toggle="tooltip"
                   title="<?php echo trans('proximity_marketing.tooltip.event_type') ?>"></i>
            </label>

            <div class="col-md-2">
                <?php echo Form::select('event_type', $eventTypeOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('event_type')): ?>
                    <span class="help-block"><?php echo $errors->first('event_type') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('event_delay')) echo ' has-error' ?>">
            <label for="event_delay" class="control-label col-md-2">
                <?php echo trans('proximity_marketing.columns.event_delay') ?>
                <i class="fa fa-question-circle" data-toggle="tooltip"
                   title="<?php echo trans('proximity_marketing.tooltip.delay') ?>"></i>
            </label>

            <div class="col-md-2">
                <?php echo Form::text('event_delay', null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('event_delay')): ?>
                    <span class="help-block"><?php echo $errors->first('event_delay') ?></span>
                <?php endif ?>
            </div>
        </div>

        <hr/>

        <div class="form-group<?php if ($errors->has('parameter_type')) echo ' has-error' ?>">
            <label for="parameter_type" class="control-label col-md-2">
                <?php echo trans('proximity_marketing.columns.parameter_type') ?>
            </label>

            <div class="col-md-2">
                <?php echo Form::select('parameter_type', $parameterTypeOptions, null, ['class' => 'form-control', 'id' => 'parameter-type']) ?>
                <?php if ($errors->has('parameter_type')): ?>
                    <span class="help-block"><?php echo $errors->first('parameter_type') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group" id="visit-based-wrapper">
            <label for="parameter_type" class="control-label col-md-2">
                <?php echo trans('proximity_marketing.columns.visit_based') ?>
            </label>

            <div class="col-md-2">
                <?php foreach ($parameterTypeVisitOptions as $key => $parameterTypeVisitOption): ?>
                    <div class="radio" id="visit-option-<?php echo $key ?>">
                        <label
                                id="select-visit-option-<?php echo $key ?>"><?php echo Form::radio('visit_option', $key, ($model->visit_option == $key) ? true : false) ?>
                            <?php echo e($parameterTypeVisitOption); ?></label>,
                        <i class="fa fa-question-circle" data-toggle="tooltip"
                           title="<?php echo trans('proximity_marketing.tooltip.visit_based_' . $key) ?>"></i>
                    </div>
                <?php endforeach ?>
            </div>

            <input type="hidden" id="visit_option_init_value" value="<?php echo $model->visit_option ?>"/>

            <div class="col-md-2 number-of-visit-wrapper">
                <?php foreach ($parameterTypeNumberOfVisits as $key => $parameterTypeNumberOfVisit): ?>
                    <div class="radio" id="visit-option-<?php echo $key ?>">
                        <label
                                id="select-visit-option-<?php echo $key ?>"><?php echo Form::radio('number_of_visit', $key, ($model->number_of_visit == $key) ? true : false) ?>
                            <?php echo e($parameterTypeNumberOfVisit); ?></label>
                    </div>
                <?php endforeach ?>
            </div>

            <div class="col-md-1 number-of-visit-wrapper">
                <?php $array = ['visit_is', 'visit_more_than', 'every_x_visit']; ?>
                <?php foreach ($parameterTypeNumberOfVisits as $key => $parameterTypeNumberOfVisit): ?>
                    <?php foreach ($array as $itemKey => $value) { ?>
                        <?php if ($itemKey == $key) { ?>
                            <?php echo Form::text($value, null, ['class' => 'form-control']) ?>
                        <?php } ?>
                    <?php } ?>
                <?php endforeach ?>
            </div>
        </div>

        <div id="date-time-wrapper"
             class="form-group<?php if ($errors->has('parameter_date_time_based')) echo ' has-error' ?>">

            <label for="parameter_type" class="control-label col-md-2">
                <?php echo trans('proximity_marketing.columns.weekdays') ?>
            </label>

            <div class="col-md-2">
                <div class="btn-group" data-toggle="buttons">
                    <?php foreach ($model->weekdays_value as $key => $weekDay) { ?>
                        <?php if ($weekDay['isChecked']) {
                            $x[] = $key;
                        } ?>
                        <label data-key="<?php echo $weekDay['key'] ?>"
                               class="btn has-active-class btn-default <?php echo $weekDay['isChecked'] ? 'active' : ''; ?> btn-sm">
                            <input type="checkbox"
                                   name="parameter_type_weekdays[<?php echo $weekDay['key'] ?>]">
                            <span><?php echo $weekDay['value']; ?></span>
                        </label>
                    <?php } ?>
                </div>
            </div>

            <?php if (isset($x)) { ?>
                <input type="hidden" id="weekdays_value" name="parameter_type_weekdays2"
                       value="<?php echo implode(',', $x); ?>"/>
            <?php } ?>

            <div class="col-md-1" id="parameter-type-start-time-wrapper">
                <?php echo Form::select('parameter_type_time_start', config('times.time'), $model->parameter_type_time_start, ['class' => 'form-control select', 'id' => 'parameter-type-time-start']) ?>
                <?php if ($errors->has('parameter_type_time_start')): ?>
                    <span class="help-block"><?php echo $errors->first('parameter_type_time_start') ?></span>
                <?php endif ?>
            </div>

            <div class="col-md-1" id="parameter-type-end-time-wrapper">
                <?php echo Form::select('parameter_type_time_end', config('times.time'), $model->parameter_type_time_end, ['class' => 'form-control select', 'id' => 'parameter-type-time-end']) ?>
                <?php if ($errors->has('parameter_type_time_end')): ?>
                    <span class="help-block"><?php echo $errors->first('parameter_type_time_end') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('frequency')) echo ' has-error' ?>">

            <label for="frequency" class="control-label col-md-2">
                <?php echo trans('proximity_marketing.columns.frequency') ?>
                <i class="fa fa-question-circle" data-toggle="tooltip"
                   title="<?php echo trans('proximity_marketing.tooltip.frequency') ?>"></i>
            </label>

            <div class="col-md-2">
                <?php echo Form::select('frequency', $frequenceTypeOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('frequency')): ?>
                    <span class="help-block"><?php echo $errors->first('frequency') ?></span>
                <?php endif ?>
            </div>
        </div>

        <hr/>

        <div class="form-group<?php if ($errors->has('gender')) echo ' has-error' ?>">
            <?php echo Form::label('gender', trans('guest.columns.gender'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('gender', $genderTypeOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('gender')): ?>
                    <span class="help-block"><?php echo $errors->first('gender') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('age')) echo ' has-error' ?>">
            <?php echo Form::label('age', trans('proximity_marketing.columns.age'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('age', $ageTypeOptions, null, ['class' => 'form-control', "id" => "age-type"]) ?>
                <?php if ($errors->has('age')): ?>
                    <span class="help-block"><?php echo $errors->first('age') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div id="age-parameter" class="form-group<?php if ($errors->has('age_parameter')) echo ' has-error' ?>">
            <?php echo Form::label('age_parameter', trans('proximity_marketing.columns.age_parameter'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-1" id="start-age">
                <?php echo Form::text('start_age', null, ['class' => 'form-control', 'id' => 's-age']) ?>
                <?php if ($errors->has('age_parameter')): ?>
                    <span class="help-block"><?php echo $errors->first('age_parameter') ?></span>
                <?php endif ?>
            </div>

            <div class="col-md-1" id="end-age">
                <?php echo Form::text('end_age', null, ['class' => 'form-control', 'id' => 'e-age']) ?>
                <?php if ($errors->has('age_parameter')): ?>
                    <span class="help-block"><?php echo $errors->first('age_parameter') ?></span>
                <?php endif ?>
            </div>


        </div>
        <hr/>


        <div class="form-group<?php if ($errors->has('communication_channel')) echo ' has-error' ?>">

            <label for="communication_channel" class="control-label col-md-2">
                <?php echo trans('proximity_marketing.columns.communication_channel') ?>
            </label>


            <div class="col-md-6">
                <div id="communication-channel-list" class="dd-list">
                    <?php foreach ($model->communication_channel_value as $communicationKey => $communicationName) { ?>
                        <div class="box box-default box-solid collapsed-box clearfix dd-item" data-key="<?php echo $communicationKey; ?>">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo $communicationName; ?></h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" style="display: none;">
                                <?php
                                $communication = str_replace(['-', ' '], ['', '_'], strtolower($communicationName)) . '_content';
                                $class = $communication === 'email_content' ? 'ckeditor' : '';
                                ?>
                                <div class="form-group<?php if ($errors->has('$communication')) echo ' has-error' ?>">
                                    <div class="col-md-12">
                                        <?php echo Form::textarea($communication, null, ['class' => "form-control $class", 'rows' => 7, 'maxlength' => 160]) ?>
                                        <?php if ($errors->has($communication)): ?>
                                            <span class="help-block"><?php echo $errors->first($communication) ?></span>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <input type="hidden" name="communication_channel" id="communication_channel" value="<?php echo $model->communication_channel; ?>"/>
        </div>

        <div class="form-group<?php if ($errors->has('status')) echo ' has-error' ?>">
            <?php echo Form::label('status', trans('proximity_marketing.columns.status'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('status', $statusOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('status')): ?>
                    <span class="help-block"><?php echo $errors->first('status') ?></span>
                <?php endif ?>
            </div>
        </div>

        <hr/>

        <div class="form-group<?php if ($errors->has('users_fit_are_performed')) echo ' has-error' ?>">
            <?php echo Form::label('users_fit_are_performed',
                trans('proximity_marketing.columns.users_fit_are_performed'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::label('estimated', $renderAudience, ['class' => 'control-label', 'id' => 'estimated']) ?>
                <?php if ($errors->has('users_fit_are_performed')): ?>
                    <span class="help-block"><?php echo $errors->first('users_fit_are_performed') ?></span>
                <?php endif ?>
            </div>
        </div>

    </div><!-- /panel-body -->
</div><!-- /panel -->
<?php echo Form::close() ?>

