<device-vendors inline-template>
    <div>

        <div class="text-right">
            <a @click="showForm()" href="#" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
        </div>

        <div class="table-responsive">

            <p v-show="!items.length" class="alert alert-warning"><?php echo trans('device_vendor.not_found') ?></p>

            <table v-show="items.length" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><?php echo trans('device_vendor.columns.name') ?></th>
                    <th style="width: 20px;"></th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items">
                    <td><a href="#" @click="showForm(item)">{{ item.name }}</a></td>
                    <td><a href="#" @click="remove(item)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a></td>
                </tr>
                </tbody>
            </table>

        </div><!-- /table-responsive -->

        <?php echo view('admin.device_vendor.modal')->render() ?>

    </div>
</device-vendors>