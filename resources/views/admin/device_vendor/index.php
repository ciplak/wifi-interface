<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('device_vendor.page_title') ?></h4>
    </div>

    <div class="panel-body">

        <div>
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-device-vendors" aria-controls="device-vendors" role="tab" data-toggle="tab"><?php echo trans('device_vendor.tab_title') ?></a></li>
                    <li role="presentation"><a href="#tab-device-models" aria-controls="device-models" role="tab" data-toggle="tab"><?php echo trans('device_model.tab_title') ?></a></li>
                </ul><!-- /nav-tabs -->

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-device-vendors">
                        <?php echo view('admin.device_vendor.grid')->render() ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab-device-models">
                        <?php echo view('admin.device_model.grid')->render() ?>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- /panel-body -->

</div><!-- /panel -->