<form @submit.prevent="save" class="form-horizontal">
    <div class="form-group" :class="{'has-error': form.errors.has('name')}">
        <label for="name" class="control-label col-md-3 required"><?php echo trans('device_vendor.columns.name')?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" id="name" v-model="form.name"/>
            <span class="help-block">{{ form.errors.get('name') }}</span>
        </div>
    </div>
</form>