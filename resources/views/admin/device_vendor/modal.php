<div class="modal fade" id="modal-device-vendor-form" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button @click="closeForm" type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close')?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo trans('device_vendor.page_title') ?>{{ updatingItem ? " - Editing " + updatingItem.name : ''}}</h4>
            </div><!-- /modal-header -->

            <div class="modal-body">
                <?php echo view('admin.device_vendor.form')->render()?>
            </div><!-- /modal-body -->

            <div class="modal-footer">
                <button :disabled="form.busy" @click="save" type="button" class="btn btn-primary">
                    <span v-if="form.busy">
                        <i class="fa fa-refresh fa-spin"></i> <?php echo trans('messages.saving') ?>
                    </span>
                    <span v-else>
                        <i class="fa fa-check"></i> <?php echo trans('messages.save') ?>
                    </span>
                </button>
            </div><!-- /modal-footer -->

        </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
</div><!-- /modal -->