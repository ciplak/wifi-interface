<script>
$(function() {
    $("#sms_content").maxlength();

    $("#parameter_type").change(function () {
        var selected = parseInt($(this).val());

        if (selected == <?php echo \App\Models\Campaign::PARAMETER_TYPE_DATE_TIME ?>) {
            $("#parameter-type-date-wrapper").show();
            $("#periodic-wrapper").hide();
        } else {
            $("#parameter-type-date-wrapper").hide();
            $("#periodic-wrapper").show();
        }
    });

    $("#parameter_type").trigger('change');
});
</script>