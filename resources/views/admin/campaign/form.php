<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\Campaign           $model
 * @var array                          $statusOptions
 * @var array                          $locations
 * @var array                          $parameterTypeOptions
 * @var array                          $periodicTypeOptions
 * @var array                          $ageTypeOptions
 * @var array                          $genderTypeOptions
 * @var array                          $communicationChannelTypeOptions
 */

echo Form::model($model, ['class' => 'form-horizontal', 'autocomplete' => 'off', 'files' => true, 'route' => ['admin.campaign.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('campaign.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.campaign.index') ?>" class="btn btn-default" data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>
    </div>

    <div class="panel-body">

        <div class="form-group<?php if ($errors->has('title')) echo ' has-error' ?>">
            <?php echo Form::label('title', trans('campaign.columns.title'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-8">
                <?php echo Form::text('title', null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('title')): ?>
                    <span class="help-block"><?php echo $errors->first('title') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('location_id')) echo ' has-error' ?>">
            <?php echo Form::label('location_id', trans('campaign.columns.location_id'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::select('location_id', $locations, null, ['class' => 'form-control select', 'placeholder' => trans('device.location.all')]) ?>
                <?php if ($errors->has('location_id')): ?>
                    <span class="help-block"><?php echo $errors->first('location_id') ?></span>
                <?php endif ?>
            </div>
        </div>

        <hr/>

        <div class="form-group<?php if ($errors->has('parameter_type')) echo ' has-error' ?>">
            <?php echo Form::label('parameter_type', trans('campaign.columns.parameter_type'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::select('parameter_type', $parameterTypeOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('parameter_type')): ?>
                    <span class="help-block"><?php echo $errors->first('parameter_type') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2"></div>

            <div class="col-md-2<?php if ($errors->has('parameter_type_date')) echo ' has-error' ?>" id="parameter-type-date-wrapper">
                <?php echo Form::text('parameter_type_date', null, ['class' => 'form-control future-date', 'id' => 'parameter-type-date', 'placeholder' => 'Date *']) ?>
                <?php if ($errors->has('parameter_type_date')): ?>
                    <span class="help-block"><?php echo $errors->first('parameter_type_date') ?></span>
                <?php endif ?>
            </div>

            <div class="col-md-2" id="periodic-wrapper">
                <?php echo Form::select('periodic', $periodicTypeOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('periodic')): ?>
                    <span class="help-block"><?php echo $errors->first('periodic') ?></span>
                <?php endif ?>
            </div>

            <div class="col-md-2" id="parameter-type-time-wrapper">
                <?php echo Form::select('parameter_type_time', config('times.time'), $model->paremeter_type_time, ['class' => 'form-control select', 'id' => 'parameter-type-time']) ?>
                <?php if ($errors->has('parameter_type_time')): ?>
                    <span class="help-block"><?php echo $errors->first('parameter_type_time') ?></span>
                <?php endif ?>
            </div>
        </div>

        <hr/>

        <div class="form-group<?php if ($errors->has('reengage')) echo ' has-error' ?>">
            <?php echo Form::label('reengage', trans('campaign.columns.reengage'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-1">
                <?php echo Form::text('reengage', null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('reengage')): ?>
                    <span class="help-block"><?php echo $errors->first('reengage') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('gender')) echo ' has-error' ?>">
            <?php echo Form::label('gender', trans('guest.columns.gender'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('gender', $genderTypeOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('gender')): ?>
                    <span class="help-block"><?php echo $errors->first('gender') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('age')) echo ' has-error' ?>">
            <?php echo Form::label('age', trans('campaign.columns.age'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('age', $ageTypeOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('age')): ?>
                    <span class="help-block"><?php echo $errors->first('age') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('visit_count')) echo ' has-error' ?>">
            <label for="visit_count" class="control-label col-md-2">
                <?php echo trans('campaign.columns.visit_count') ?>
                <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('campaign.columns.visit_count') ?>"></i>
            </label>

            <div class="col-md-1">
                <?php echo Form::text('visit_count', null, ['class' => 'form-control', 'id' => 'visit_count']) ?>
                <?php if ($errors->has('visit_count')): ?>
                    <span class="help-block"><?php echo $errors->first('visit_count') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-4 col-md-offset-2">
                <div class="checkbox">
                    <label class="control-label">
                        <?php echo Form::checkbox('birthday', 1), ' ', trans('campaign.columns.birthday') ?>
                    </label>
                </div>
            </div>
        </div>

        <hr/>

        <div class="form-group<?php if ($errors->has('sms_content')) echo ' has-error' ?>">
            <?php echo Form::label('sms_content', trans('campaign.columns.sms_content'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::textarea('sms_content', null, ['class' => 'form-control', 'rows' => 3, 'maxlength' => 160]) ?>
                <?php if ($errors->has('sms_content')): ?>
                    <span class="help-block"><?php echo $errors->first('sms_content') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('email_content')) echo ' has-error' ?>">
            <?php echo Form::label('email_content', trans('campaign.columns.email_content'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::textarea('email_content', null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('email_content')): ?>
                    <span class="help-block"><?php echo $errors->first('email_content') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('status')) echo ' has-error' ?>">
            <?php echo Form::label('status', trans('campaign.columns.status'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('status', $statusOptions, null, ['class' => 'form-control']) ?>
                <?php if ($errors->has('status')): ?>
                    <span class="help-block"><?php echo $errors->first('status') ?></span>
                <?php endif ?>
            </div>
        </div>
    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>