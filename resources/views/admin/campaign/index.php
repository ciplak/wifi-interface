<?php
/**
 * @var \App\Models\Campaign[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('campaign.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.campaign.add') ?>" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
        </div>

    </div>

    <div class="panel-body">
        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('campaign.not_found') ?></p>

            <?php else: ?>

                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th><?php echo trans('campaign.columns.title') ?></th>
                        <th><?php echo trans('campaign.columns.parameter_type') ?></th>
                        <th><?php echo trans('campaign.columns.location_id') ?></th>
                        <th><?php echo trans('campaign.columns.status') ?></th>
                        <th class="col-actions"></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($models as $model): ?>
                        <tr>
                            <td><?php echo HTML::linkRoute('admin.campaign.edit', $model->title, [$model]) ?></td>
                            <td><?php echo $model->parameter_value ?></td>
                            <td><?php echo $model->location_name ?></td>
                            <td><?php echo $model->status_icon ?></td>
                            <td class="col-actions">
                                <a href="<?php echo route('admin.campaign.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>

                </table>

                <?php pagination($models) ?>

            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->
