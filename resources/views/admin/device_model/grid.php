<device-models inline-template>
    <div>

        <div class="text-right">
            <a @click="showForm()" href="#" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
        </div>

        <div class="table-responsive">

            <p v-show="!items.length"
               class="alert alert-warning"><?php echo trans('device_vendor.not_found') ?></p>

            <table v-show="items.length" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><?php echo trans('device_model.columns.vendor') ?></th>
                    <th><?php echo trans('device_model.columns.name') ?></th>
                    <th><?php echo trans('device_model.columns.type') ?></th>
                    <th><?php echo trans('device_model.columns.handler') ?></th>
                </tr>
                </thead>

                <tbody>

                <tr v-for="item in items">
                    <td>{{ item.vendor_name }}</td>
                    <td><a href="#" @click="showForm(item)">{{ item.name }}</a></td>
                    <td>{{ item.type_value }}</td>
                    <td>{{ item.handler_display_name }}</td>
                </tr>

                </tbody>

            </table>

        </div><!-- /table-responsive -->

        <?php echo view('admin.device_model.modal')->render() ?>

    </div>
</device-models>
