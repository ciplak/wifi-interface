<form @submit.prevent="save" class="form-horizontal">
    <div class="form-group" :class="{'has-error': form.errors.has('device_vendor_id')}">
        <label for="device_vendor_id" class="control-label col-md-3 required"><?php echo trans('device_model.columns.vendor')?></label>
        <div class="col-md-9">
            <select v-model="form.device_vendor_id" id="device_vendor_id" class="form-control" placeholder="<?php echo trans('messages.select')?>">
                <option v-for="option in deviceVendorOptions" :value="option.id">{{ option.name }}</option>
            </select>
            <span class="help-block">{{ form.errors.get('device_vendor_id') }}</span>
        </div>
    </div>
    
    <div class="form-group" :class="{'has-error': form.errors.has('name')}">
        <label for="name" class="control-label col-md-3 required"><?php echo trans('device_model.columns.name')?></label>
        <div class="col-md-9">
            <input type="text" id="name" class="form-control" v-model="form.name"/>
            <span class="help-block">{{ form.errors.get('name') }}</span>
        </div>
    
    </div>
    
    <div class="form-group" :class="{'has-error': form.errors.has('device_type_id')}">
        <label for="device_type_id" class="control-label col-md-3 required"><?php echo trans('device_model.columns.type')?></label>
        <div class="col-md-4">
            <select v-model="form.device_type_id" id="device_type_id" class="form-control" placeholder="<?php echo trans('device_model.type.select')?>">
                <option v-for="(val, key) in typeOptions" :value="key">{{ val }}</option>
            </select>
            <span class="help-block">{{ form.errors.get('device_type_id') }}</span>
        </div>
    </div>
    
    <div class="form-group" :class="{'has-error': form.errors.has('device_handler_id')}">
        <label for="device_handler_id" class="control-label col-md-3 required"><?php echo trans('device_model.columns.handler')?></label>
        <div class="col-md-4">
            <select v-model="form.device_handler_id" id="device_handler_id" class="form-control" placeholder="<?php echo trans('messages.select')?>">
                <option v-for="(val, key) in handlerOptions" :value="key">{{ val }}</option>
            </select>
            <span class="help-block">{{ form.errors.get('device_handler_id') }}</span>
        </div>
    </div>

</form>