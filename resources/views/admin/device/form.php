<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\Device $model
 * @var \App\Models\Device[] $devices
 * @var array $deviceModels
 * @var \App\Models\Property[] $properties
 */
echo Form::model($model, ['class' => 'form-horizontal', 'autocomplete' => 'off', 'route' => ['admin.device.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('device.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.device.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

            <?php if ($model->exists): ?>
                <?php if ($model->trashed()) { ?>
                    <a href="<?php echo route('admin.device.restore', [$model]) ?>" class="btn btn-warning" data-tooltip="true" title="<?php echo trans('messages.restore') ?>">
                        <i class="fa fa-refresh"></i>
                    </a>
                <?php } else { ?>
                    <a href="<?php echo route('admin.device.delete', [$model]) ?>" class="btn btn-danger btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>">
                        <i class="fa fa-trash-o"></i>
                    </a>
                <?php } ?>
            <?php endif  ?>

            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">

        <div class="form-group<?php if ($errors->has('property_id')) echo ' has-error' ?>">
            <?php echo Form::label('property_id', trans('device.columns.location'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-4">
                <?php echo Form::select('property_id', $properties, $model->exists ? null : request('location'), ['class' => 'form-control select', 'placeholder' => trans('device.location.select')]) ?>
                <?php if ($errors->has('property_id')): ?>
                    <span class="help-block"><?php echo $errors->first('property_id') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('name')) echo ' has-error' ?>">
            <?php echo Form::label('name', trans('device.columns.name'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-4">
                <?php echo Form::text('name', null, ['class' => 'form-control'])?>
                <?php if ($errors->has('name')): ?>
                    <span class="help-block"><?php echo $errors->first('name') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo Form::label('description', trans('device.columns.description'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::text('description', null, ['class' => 'form-control'])?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('ip')) echo ' has-error' ?>">
            <?php echo Form::label('ip', trans('messages.ip'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::text('ip', null, ['class' => 'form-control', 'data-mask', 'data-inputmask-alias' => 'ip'])?>
                <?php if ($errors->has('ip')): ?>
                    <span class="help-block"><?php echo $errors->first('ip') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('mac')) echo ' has-error' ?>">
            <?php echo Form::label('mac', trans('messages.mac_address'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::text('mac', null, ['class' => 'form-control', 'data-mask', 'data-inputmask-alias' => 'mac'])?>
                <?php if ($errors->has('mac')): ?>
                    <span class="help-block"><?php echo $errors->first('mac') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('device_model_id')) echo ' has-error' ?>">
            <?php echo Form::label('device_model_id', trans('device.columns.model'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-4">
                <?php echo Form::select('device_model_id', $deviceModels, null, ['class' => 'form-control select', 'placeholder' => trans('device.select_model')]) ?>
                <?php if ($errors->has('device_model_id')): ?>
                    <span class="help-block"><?php echo $errors->first('device_model_id') ?></span>
                <?php endif ?>
            </div>
        </div>

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>