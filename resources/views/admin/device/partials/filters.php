<?php
/**
 * @var array $locations
 * @var array $deviceModels
 * @var array $deviceStatusOptions
 */
echo Form::open(['class' => 'form-inline margin-bottom', 'method' => 'GET']) ?>

    <div class="form-group">
        <?php echo Form::select('location', $locations, request('location'), ['class' => 'form-control select', 'placeholder' => trans('device.location.all')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => trans('device.columns.name')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::select('device_model', $deviceModels, request('device_model'), ['class' => 'form-control select', 'placeholder' => trans('device.model.all')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::select('status', $deviceStatusOptions, request('status'), ['class' => 'form-control select', 'placeholder' => trans('device.status.all')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::text('mac', request('mac'), ['class' => 'form-control', 'placeholder' => trans('messages.mac_address')]) ?>
    </div>

    <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
    <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>

<?php echo Form::close() ?>