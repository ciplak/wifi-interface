<div class="modal fade" id="device-import-modal" tabindex="-1" role="dialog" aria-labelledby="device-import-form">
    <?php echo Form::open(['class' => 'form-horizontal', 'id' => 'device-import-form', 'files' => true, 'route' => ['admin.device.import']])?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="device-import-form-title"><?php echo trans('device.bulk_import.page_title')?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?php echo Form::label('device-file', trans('messages.file'), ['class' => 'control-label col-md-2'])?>
                    <div class="col-md-10">
                        <?php echo Form::file('device-file', ['class' => 'form-control'])?>
                        <p class="help-block"><?php echo trans('device.bulk_import.description') ?></p>
                        <p class="help-block"><a href="<?php echo route('admin.device.download_import_file')?>"><?php echo trans('device.bulk_import.click_to_download_sample_file') ?></a></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo trans('messages.cancel') ?></button>
                <button type="submit" class="btn btn-primary" name="btn-import"><i class="fa fa-upload"></i> <?php echo trans('messages.import') ?></button>
            </div>
        </div>
    </div>
    <?php echo Form::close()?>
</div>