<?php
/**
 * @var \App\Models\Device[]|\Illuminate\Database\Eloquent\Collection $models
 */
$partial = isset($partial) && $partial;
?>
<div class="table-responsive">

    <?php if ($models->isEmpty()): ?>

        <p class="alert alert-warning"><?php echo trans('device.not_found') ?></p>

    <?php else: ?>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" class="check-all" title="<?php echo trans('messages.select_all') ?>">
                </th>
                <th><?php echo trans('device.columns.name') ?></th>
                <th><?php echo trans('device.columns.location') ?></th>
                <th><?php echo trans('device.columns.description') ?></th>
                <th><?php echo trans('device.columns.model') ?></th>
                <th><?php echo trans('device.columns.ip') ?></th>
                <th><?php echo trans('device.columns.mac') ?></th>
                <?php if (!$partial):?>
                <th class="col-actions"></th>
                <?php endif ?>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($models as $model): ?>
                <tr<?php if ($model->deleted_at) echo ' class="danger"' ?>>
                    <td><?php echo Form::checkbox('devices[]', $model->getRouteKey()) ?></td>
                    <td><?php echo HTML::linkRoute('admin.device.edit', $model->name, [$model]) ?></td>
                    <td><?php echo $model->property ? $model->property->name : '' ?></td>
                    <td><?php echo e($model->description) ?></td>
                    <td><?php echo $model->model ? $model->model->full_name : ''?></td>
                    <td><?php echo e($model->ip) ?></td>
                    <td><?php echo e($model->mac) ?></td>
                    <?php if (!$partial):?>
                        <td class="col-actions">
                            <?php if ($model->trashed()): ?>
                                <a href="<?php echo route('admin.device.restore', [$model]) ?>" class="btn btn-warning btn-sm btn-restore" data-tooltip="true" title="<?php echo trans('messages.restore') ?>">
                                    <i class="fa fa-refresh"></i>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo route('admin.device.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            <?php endif ?>
                        </td>
                    <?php endif?>
                </tr>
            <?php endforeach ?>
            </tbody>

        </table>

        <?php if (!$partial) pagination($models) ?>

    <?php endif ?>

</div><!-- /table-responsive -->