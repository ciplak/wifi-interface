<script>
$(function() {
    $('#device-import-form').submit(function() {
        $(this).find('.modal-footer').html('<div class="text-center"><i class="fa fa-refresh fa-spin"></i> <?php echo trans('messages.please_wait')?></div>');
    });

    var deviceListForm = $('#device-list-form'),
        action = $('#device_action');

    $('#btn-delete-selected').click(function () {

        if (!deviceListForm.find(':checkbox:checked').length) {
            bootbox.alert('<?php echo trans('device.select_devices_to_delete') ?>');
            return false;
        }

        action.val('delete');

        bootbox.confirm({
            message: '<?php echo trans('messages.delete') ?>',
            buttons: {
                'cancel': {
                    label: "Don't delete",
                    className: "btn-default"
                },
                'confirm': {
                    label: "Delete",
                    className: "btn-danger",
                }
            },
            callback: function (result) {
                if (!result) return;

                deviceListForm.submit();

            }
        });

        return false;
    });

    $('#btn-restore-selected').click(function () {

        if (!deviceListForm.find(':checkbox:checked').length) {
            bootbox.alert('<?php echo trans('device.select_devices_to_restore') ?>');
            return false;
        }

        action.val('restore');

        bootbox.confirm({
            message: '<?php echo trans('messages.restore') ?>',
            buttons: {
                'cancel': {
                    label: "Don't restore",
                    className: "btn-default"
                },
                'confirm': {
                    label: "Restore",
                    className: "btn-warning"
                }
            },
            callback: function (result) {
                if (!result) return;

                deviceListForm.submit();

            }
        });

        return false;
    });

    // delete button confirmation
    $('.btn-restore').click(function () {
        var $this = $(this),
            message = '<?php echo trans('device.delete.restore_item') ?>';

        bootbox.confirm({
            message: message,
            buttons: {
                'cancel': {
                    label: "<?php echo trans('device.dont_restore') ?>",
                    className: "btn-default"
                },
                'confirm': {
                    label: "<?php echo trans('device.restore') ?>",
                    className: "btn-warning"
                }
            },
            callback: function (result) {
                if (!result) return;

                location.href = $this.attr('href');

            }
        });

        return false;
    });
});
</script>