<?php
/**
 * @var \App\Models\Device[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('device.page_title') ?></h4>

            <div class="panel-control">
                <a href="#" class="btn btn-default" data-toggle="modal" data-target="#device-import-modal" data-backdrop="static" data-keyboard="false" data-tooltip="true" title="<?php echo trans('messages.import')?>"><i class="fa fa-upload"></i></a>
                <a href="#" id="btn-restore-selected" class="btn btn-warning" data-tooltip="true" title="<?php echo trans('messages.restore_selected') ?>"><i class="fa fa-refresh"></i></a>
                <a href="#" id="btn-delete-selected" class="btn btn-danger" data-tooltip="true" title="<?php echo trans('messages.delete_selected') ?>"><i class="fa fa-trash-o"></i></a>
                <a href="<?php echo route('admin.device.add', ['location' => request('location')]) ?>" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
            </div>
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">

        <?php echo view('admin.device.partials.filters')->render() ?>
        <?php echo Form::open(['id' => 'device-list-form', 'route' => ['admin.device.index']]) ?>
        <?php echo Form::hidden('device_action', null, ['id' => 'device_action']) ?>
        <?php echo view('admin.device.partials.grid', ['models' => $models])->render()?>
        <?php echo Form::close() ?>

    </div><!-- /panel-body -->

</div><!-- /panel -->

<?php echo view('admin.device.partials.import_modal')->render() ?>