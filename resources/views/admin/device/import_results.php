<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('device.bulk_import.page_title') ?></h4>

        <div class="panel-control">
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#device-import-modal" data-backdrop="static" data-keyboard="false" data-tooltip="true" title="<?php echo trans('messages.import')?>"><i class="fa fa-upload"></i></a>
            <a href="<?php echo route('admin.device.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>
        </div>

    </div>

    <div class="panel-body">

        <?php if ($results['results']['success']): ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="<?php echo trans('messages.close') ?>"><span aria-hidden="true">&times;</span></button>
                <?php echo trans('device.imported', ['count' => $results['results']['success']]) ?>
            </div>
        <?php endif ?>

        <?php if ($results['results']['errors']): ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="<?php echo trans('messages.close') ?>"><span aria-hidden="true">&times;</span></button>
                <?php echo trans('device.not_imported', ['count' => $results['results']['errors']]) ?>
            </div>
        <?php endif ?>

        <?php if (isset($results['errors']) && $results['errors']): ?>
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                <tr>
                    <td>Row</td>
                    <?php foreach ($results['headers'] as $title): ?>
                    <td><?php echo $title ?></td>
                    <?php endforeach; ?>
                    <td>Errors</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results['errors'] as $rowNo => $result): ?>
                    <tr>
                        <td><?php echo $rowNo ?></td>
                        <?php foreach ($result['data'] as $item): ?>
                            <td><?php echo $item ?></td>
                        <?php endforeach; ?>
                        <td><?php echo implode('<br>', $result['errors']) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif ?>

    </div>
</div>

<?php echo view('admin.device.partials.import_modal')->render() ?>