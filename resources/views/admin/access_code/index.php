<?php
/**
 * @var \App\Models\AccessCode[]|\Illuminate\Database\Eloquent\Collection $models
 * @var array $serviceProfiles
 * @var array $statuses
 * @var array $types
 * @var array $typesForCreate
 * @var array $expiredOptions
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('access_code.page_title') ?></h4>

            <div class="panel-control">
                <?php if (\Entrust::can('access-codes.manage')): ?>
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-access-code-form" data-tooltip="true" title="<?php echo trans('access_code.generate') ?>"><i class="fa fa-plus"></i></a>
                <?php endif ?>

                <div class="btn-group" data-tooltip="true" title="<?php echo trans('messages.export.title') ?>">
                    <button type="button" class="btn btn-default btn-export" data-format="xlsx"><i class="fa fa-share"></i></button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" class="btn-export" data-format="csv"><?php echo trans('messages.export.csv') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="xlsx"><?php echo trans('messages.export.xlsx') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="pdf"><?php echo trans('messages.export.pdf') ?></a></li>
                    </ul>
                </div>

            </div>
        </div>

    </div>

    <div class="panel-body">

        <?php echo view('admin.access_code.filters')->render() ?>

        <div class="table-responsive">
            <?php echo view('admin.access_code.grid', ['models' => $models])->render() ?>
        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->

<?php echo view('admin.access_code.modal') ?>