<script>
$(function() {
    var filterForm = $('form.filter-form');

    $('.btn-export').click(function() {

        var inputs = [];
        $(':input:not(button)', filterForm).each(function (i, el) {
            var $el = $(el);
            inputs.push('<input type="hidden" name="' + $el.attr('name') + '" value="' + $el.val() + '">');
        });

        var form = $('<form action="<?php echo route('admin.access_code.export') ?>" method="POST">' +
            '<input type="hidden" name="format" value="' + $(this).data('format') + '">' +
            '<?php echo csrf_field() ?>' + inputs.join('') + '</form>');

        form.appendTo($('body'));
        form.submit().remove();

        return false;
    });
})
</script>