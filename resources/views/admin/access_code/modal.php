<?php
/**
 * @var string $formTypeOptions
 * @var string $formLocationOptions
 * @var string $formServiceProfileOptions
 */
?>
<div class="modal fade in" id="modal-access-code-form" tabindex="-1" role="dialog" aria-labelledby="AccessCodeForm" aria-hidden="false">
    <access-code-form inline-template :venues='<?php echo $formLocationOptions ?>' :service-profiles='<?php echo $formServiceProfileOptions ?>' :types='<?php echo $formTypeOptions ?>'>

        <form @submit.prevent="submit">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button @click="closeForm" type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close') ?>">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title"><?php echo trans('access_code.generate') ?></h4>
                    </div>
                    <div class="modal-body">
                        <?php echo view('admin.access_code.form')->render() ?>
                    </div>
                    <div class="modal-footer">
                        <button :disabled="busy" @click="closeForm" type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('messages.cancel') ?></button>
                        <button :disabled="busy" type="submit" class="btn btn-success">

                            <span v-if="form.busy">
                                <i class="fa fa-refresh fa-spin"></i> <?php echo trans('messages.please_wait') ?>
                            </span>
                            <span v-else>
                                <?php echo trans('messages.create') ?>
                            </span>

                        </button>
                    </div>
                </div>
            </div>

        </form>
    </access-code-form>
</div>