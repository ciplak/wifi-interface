<?php
/**
 * @var \App\Models\AccessCode[]|\Illuminate\Database\Eloquent\Collection $models
 */
$partial = isset($partial) ? $partial : false;
if ($models->isEmpty()): ?>

    <p class="alert alert-warning"><?php echo trans('access_code.not_found') ?></p>

<?php else:
?>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th><?php echo trans('access_code.columns.location') ?></th>
            <th><?php echo trans('access_code.columns.code') ?></th>
            <th><?php echo trans('access_code.columns.type') ?></th>
            <th><?php echo trans('service_profile.title') ?></th>
            <th><?php echo trans('access_code.columns.expiry_date') ?></th>
            <th class="col-actions"><?php echo trans('access_code.columns.status') ?></th>
            <th><?php echo trans('access_code.columns.use_count') ?></th>
            <th><?php echo trans('guest.title') ?></th>
            <?php if (!$partial): ?>
            <th><?php echo trans('messages.created_by') ?></th>
            <th><?php echo trans('messages.created_at') ?></th>
            <?php endif ?>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($models as $model): ?>
        <tr <?php if ($model->is_expired) echo 'class="danger"' ?>>
            <td><?php echo $model->venue_name ?></td>
            <td><?php echo $model->code ?></td>
            <td><?php echo $model->type_name ?></td>
            <td><?php echo $model->serviceProfile->name_with_duration ?></td>
            <td><?php echo $model->expiry_date_formatted ?></td>
            <td><?php echo $model->status_value_html ?></td>
            <td><?php echo $model->use_count ?></td>
            <td><?php echo $model->guest_display_name ?></td>
            <?php if (!$partial): ?>
            <td><?php echo $model->created_by_name ?></td>
            <td><?php echo $model->created_at_formatted ?></td>
            <td class="col-actions">
                <?php if (\Entrust::can('access-codes.manage')): ?>
                <a href="<?php echo route('admin.access_code.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a>
                <?php endif ?>
            </td>
            <?php endif ?>
        </tr>
        <?php endforeach ?>

        </tbody>
    </table>

    <?php if (!$partial) pagination($models) ?>

<?php endif ?>