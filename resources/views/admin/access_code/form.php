<?php
/**
 * @var array $venues
 * @var array $serviceProfiles
 * @var array $typesForCreate
 */
?>
<div class="form-horizontal">
    <?php if (\Entrust::can('access-codes.manage-global')): ?>
    <div class="form-group" :class="{'has-error': form.errors.has('venue_id')}">
        <label for="venue_id" class="control-label col-md-3 required"><?php echo trans('venue.title') ?></label>
        <div class="col-md-9">
            <select class="form-control" id="venue_id" v-model="form.venue_id">
                <option v-for="option in venues" :value="option.value">{{ option.name }}</option>
            </select>
            <span class="help-block">{{ form.errors.get('venue_id') }}</span>
        </div>
    </div>
    <?php endif ?>

    <div class="form-group" :class="{'has-error': form.errors.has('service_profile_id')}">
        <label for="service_profile_id" class="control-label col-md-3 required"><?php echo trans('service_profile.title') ?></label>
        <div class="col-md-9">
            <select class="form-control" id="service_profile_id" v-model="form.service_profile_id">
                <option v-for="option in serviceProfiles" :value="option.value">{{ option.name }}</option>
            </select>
            <span class="help-block">{{ form.errors.get('service_profile_id') }}</span>
        </div>
    </div>

    <div class="form-group" :class="{'has-error': form.errors.has('type')}">
        <label for="type" class="control-label col-md-3 required"><?php echo trans('access_code.columns.type') ?></label>
        <div class="col-md-9">
            <select class="form-control" id="type" v-model="form.type">
                <option v-for="option in types" :value="option.value">{{ option.name }}</option>
            </select>
            <span class="help-block">{{ form.errors.get('type') }}</span>
        </div>
    </div>

    <div v-if="form.type == <?php echo \App\Models\AccessCode::TYPE_SHARED?>" class="form-group" :class="{'has-error': form.errors.has('code')}">
        <label for="code" class="control-label col-md-3"><?php echo trans('access_code.columns.code') ?></label>
        <div class="col-md-6">
            <input class="form-control" type="text" name="code" id="code" v-model="form.code">
            <span class="help-block">{{ form.errors.get('code') }}</span>
        </div>
    </div>

    <div v-if="form.type != <?php echo \App\Models\AccessCode::TYPE_SHARED?>" class="form-group" :class="{'has-error': form.errors.has('quantity')}">
        <?php echo Form::label('quantity', trans('access_code.quantity'), ['class' => 'control-label col-md-3']) ?>
        <div class="col-md-3">
            <input class="form-control input-numeric" type="number" min="1" max="1000" pattern="\d" name="quantity" id="quantity" v-model="form.quantity">
        </div>
        <div class="col-md-9 col-md-offset-3">
            <span class="help-block">{{ form.errors.get('quantity') }}</span>
        </div>
    </div>

    <div class="form-group" :class="{'has-error': form.errors.has('expiry_date')}">
        <?php echo Form::label('expiry_date', trans('access_code.columns.expiry_date'), ['class' => 'control-label col-md-3']) ?>

        <div class="col-md-6">
            <?php echo Form::text('expiry_date', null, ['class' => 'form-control', 'v-model' => 'form.expiry_date', 'v-future-datepicker' => ''])?>
            <span class="help-block">{{ form.errors.get('expiry_date') }}</span>
        </div>
    </div>

</div>