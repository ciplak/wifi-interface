<?php
/**
 * @var array $venueOptions
 * @var array $typeOptions
 * @var array $expiredOptions
 * @var array $serviceProfileOptions
 * @var array $statusOptions
 */
echo Form::open(['class' => 'form-inline margin-bottom filter-form', 'method' => 'GET']) ?>

    <?php if (\Entrust::can('access-codes.manage-global')): ?>
    <div class="form-group">
        <?php echo Form::select('venue', $venueOptions, request('venue'), ['class' => 'form-control', 'id' => 'filter_venue', 'placeholder' => trans('venue.all')]) ?>
    </div>
    <?php endif ?>

    <div class="form-group">
        <?php echo Form::text('code', request('code'), ['class' => 'form-control', 'placeholder' => trans('access_code.columns.code')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::select('type', $typeOptions, request('type'), ['class' => 'form-control', 'id' => 'filter_type', 'placeholder' => trans('access_code.type.all')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::select('expired', $expiredOptions, request('expired'), ['class' => 'form-control', 'placeholder' => trans('messages.all')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::select('service_profile', $serviceProfileOptions, request('service_profile'), ['class' => 'form-control', 'id' => 'filter_service_profile', 'placeholder' => trans('service_profile.select')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::select('status', $statusOptions, request('status'), ['class' => 'form-control', 'placeholder' => trans('access_code.status.all')]) ?>
    </div>

    <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
    <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>

<?php echo Form::close() ?>