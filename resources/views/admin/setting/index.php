<div class="row">
    <div class="col-md-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?php echo trans('settings.page_title') ?></h4>
        </div>

            <ul class="list-group stacked-tabs" role="tablist">
                <?php
                /**
                 * @var \App\Tab[] $tabs
                 * @var string $activeTabKey
                 */

                foreach ($tabs as $tab):
                    if ($tab->key == $activeTabKey) {
                        $activeTab = $tab;
                    }
                ?>
                    <a href="<?php echo route('admin.setting.index', ['tab' => $tab->key]) ?>"
                       class="list-group-item <?php if ($tab->key == $activeTabKey) echo 'active' ?>">
                        <i class="fa fa-btn fa-fw fa-<?php echo $tab->icon ?>"></i>&nbsp;<?php echo $tab->name ?>
                    </a>
                <?php endforeach ?>
            </ul>

    </div><!-- /panel -->

</div><!-- /col -->

    <div class="col-md-10">
        <?php echo Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => ['admin.setting.save', 'tab' => Request::get('tab')]]) ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?php echo $activeTab->name ?></h4>

            <div class="panel-control">
                <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
            </div>
        </div>

        <div class="panel-body">
            <?php echo view('admin.setting.tabs.' . $activeTab->view)->render() ?>
        </div>
    </div><!-- /panel -->
    <?php echo Form::close() ?>
</div><!-- /col -->

</div><!-- /row -->