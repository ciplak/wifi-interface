<?php
/**
 * @var array $timezones
 * @var array $dateFormats
 * @var array $timeFormats
 * @var array $days
 * @var array $accessCodeTypes
 * @var array $accessCodeChars
 */
$logo = setting('logo');
?>
<div class="form-group">
    <?php echo Form::label('logo', trans('settings.logo'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::file('logo', ['class' => 'form-control file-input margin-bottom', 'accept' => 'image/*']) ?>
        <div id="logo-preview-container" class="text-center margin-top <?php if (!$logo) echo 'hidden' ?>" style="border:1px solid #ccc; padding:20px">
            <?php if ($logo): ?>
                <img src="<?php echo logo_url() ?>" alt="logo" id="logo-preview" class="img-responsive center-block" height="40">
            <?php endif ?>
        </div>
    </div>
</div>

<hr class="featurette-divider">

<div class="form-group">
    <label for="settings[country_phone_code_type]" class="control-label col-md-4">
        <?php echo trans('settings.available_country_codes.title') ?>
        <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.available_country_codes.tooltip') ?>"></i>
    </label>

    <div class="col-md-4">
        <?php echo Form::select('settings[country_phone_code_type]', [trans('settings.available_country_codes.all'), trans('settings.available_country_codes.only_default')], setting('country_phone_code_type'), ['class' => 'form-control', 'select', 'id' => 'settings[country_phone_code_type]']) ?>
    </div>
</div>

<div class="form-group">
    <label for="settings[default_country_phone_code]" class="control-label col-md-4">
        <?php echo trans('settings.default_country_phone_code.title') ?>
        <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.default_country_phone_code.tooltip') ?>"></i>
    </label>

    <div class="col-md-4">
        <?php echo Form::select('settings[default_country_phone_code]', config('countries.phone_code_names'), setting('default_country_phone_code'), ['class' => 'form-control select', 'id' => 'settings[default_country_phone_code]']) ?>
    </div>
</div>

<div class="form-group">
    <label for="settings[isolate_guests]" class="control-label col-md-4">
        <?php echo trans('settings.isolate_guests.title') ?>
        <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.isolate_guests.tooltip') ?>"></i>
    </label>

    <div class="col-md-4">
        <?php echo Form::select('settings[isolate_guests]', [trans('messages.no'), trans('messages.yes')], setting('isolate_guests'), ['class' => 'form-control', 'select']) ?>
    </div>
</div>

<h5 class="page-header"><?php echo trans('settings.date_time')?></h5>

<div class="form-group">
    <label for="settings[timezone]" class="control-label col-md-4">
        <?php echo trans('settings.timezone.title') ?>
        <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.timezone.tooltip') ?>"></i>
    </label>

    <div class="col-md-4">
        <?php echo Form::select('settings[timezone]', $timezones, setting('timezone'), ['class' => 'form-control select', 'id' => 'settings[timezone]']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[date_format]', trans('settings.date_format'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::select('settings[date_format]', $dateFormats, setting('date_format'), ['class' => 'form-control', 'select']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[time_format]', trans('settings.time_format'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::select('settings[time_format]', $timeFormats, setting('time_format'), ['class' => 'form-control', 'select']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[start_of_week]', trans('settings.start_of_week'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::select('settings[start_of_week]', $days, setting('start_of_week'), ['class' => 'form-control']) ?>
    </div>
</div>

<h5 class="page-header"><?php echo trans('settings.access_code.title')?></h5>

<div class="form-group">
    <?php echo Form::label('settings[access_code.type]', trans('settings.access_code.type'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::select('settings[access_code.type]', $accessCodeTypes, setting('access_code.type'), ['class' => 'form-control', 'select']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[access_code.chars]', trans('settings.access_code.chars'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::select('settings[access_code.chars]', $accessCodeChars, setting('access_code.chars'), ['class' => 'form-control', 'select']) ?>
    </div>
</div>
<!--
<div class="form-group">
    <?php echo Form::label('settings[access_code.expiry_period]', trans('settings.access_code.expiry_period'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::select('settings[access_code.expiry_period]', $accessCodeExpiryOptions, setting('access_code.expiry_period'), ['class' => 'form-control', 'select']) ?>
    </div>
</div>
-->
<div class="form-group">
    <?php echo Form::label('settings[access_code.length]', trans('settings.access_code.length'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::select('settings[access_code.length]', [4 => '4', '5', '6', '7', '8', '9', '10'], setting('access_code.length'), ['class' => 'form-control', 'select']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[shared_device_count]', trans('settings.shared_device_count'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-2">
        <?php echo Form::number('settings[shared_device_count]', setting('shared_device_count'), ['class' => 'form-control input-numeric', 'min' => 0, 'pattern' => '\d']) ?>
    </div>
</div>