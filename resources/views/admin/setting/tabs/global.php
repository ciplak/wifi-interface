<div class="col-md-12">
    <settings inline-template>
    <div>
        <form @submit.prevent="update" class="form-horizontal">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo trans('messages.settings') ?></h4>

                    <div class="panel-control">
                        <button :disabled="form.busy" @click="update" type="submit" class="btn btn-primary">
                            <span v-if="form.busy">
                                <i class="fa fa-refresh fa-spin"></i> <?php echo trans('messages.save') ?>
                            </span>
                            <span v-else>
                                <i class="fa fa-check"></i> <?php echo trans('messages.save') ?>
                            </span>
                        </button>
                    </div>
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <label for="maintenance_mode" class="control-label col-md-2"><?php echo trans('settings.maintenance_mode')?></label>
                        <div class="col-md-4">
                            <div class="checkbox">
                                <label>
                                    <input id="maintenance_mode" type="checkbox" v-model="form.maintenance_mode" :true-value="1" :false-value="0"> <?php echo trans('settings.enable_maintenance_mode')?>
                                </label>
                            </div>
                        </div>
                    </div>

                    <h5 class="page-header"><?php echo trans('messages.email')?></h5>

                    <div class="form-group">
                        <label for="email.host" class="control-label col-md-2"><?php echo trans('settings.email.host')?></label>
                        <div class="col-md-4">
                            <input type="text" id="email.host" class="form-control" v-model="form.email.host" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email.port" class="control-label col-md-2"><?php echo trans('settings.email.port')?></label>
                        <div class="col-md-2">
                            <input type="text" id="email.port" class="form-control" v-model="form.email.port" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email.encryption" class="control-label col-md-2"><?php echo trans('settings.email.encryption.title')?></label>

                        <div class="col-md-1">
                            <select class="form-control" id="email.encryption" v-model="form.email.encryption">
                                <option><?php echo trans('settings.email.encryption.none')?></option>
                                <option value="ssl"><?php echo trans('settings.email.encryption.ssl')?></option>
                                <option value="tls"><?php echo trans('settings.email.encryption.tls')?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email.username" class="control-label col-md-2"><?php echo trans('settings.email.username')?></label>
                        <div class="col-md-4">
                            <input type="text" id="email.username" class="form-control" v-model="form.email.username" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email.password" class="control-label col-md-2"><?php echo trans('settings.email.password')?></label>
                        <div class="col-md-4">
                            <input type="text" id="email.password" class="form-control" v-model="form.email.password" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email.from.address" class="control-label col-md-2"><?php echo trans('settings.email.from.address')?></label>
                        <div class="col-md-4">
                            <input type="text" id="email.from.address" class="form-control" v-model="form.email.from.address" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email.from.name" class="control-label col-md-2"><?php echo trans('settings.email.from.name')?></label>
                        <div class="col-md-4">
                            <input type="text" id="email.from.name" class="form-control" v-model="form.email.from.name" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email.to" class="control-label col-md-2"><?php echo trans('settings.email.to')?></label>
                        <div class="col-md-4">
                            <input type="text" id="email.to" class="form-control" v-model="form.email.to" />
                        </div>
                    </div>

                </div><!-- /panel-body -->

            </div><!-- /panel -->

        </form>
    </div>
    </div>
</settings>
</div><!-- /col -->

