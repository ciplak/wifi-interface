<facebook-preview inline-template>
    <div>
        <p class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo trans('settings.social.facebook.description') ?></p>

        <div class="col-md-6">
            <div class="form-group">
                <label for="settings-social-facebook-message" class="control-label col-md-4">
                    <?php echo trans('settings.social.facebook.message') ?>

                    <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.social.facebook.message_tooltip') ?>"></i>
                </label>

                <div class="col-md-8">
                    <textarea class="form-control" id="settings-social-facebook-message" cols="50" rows="3" v-model="facebook.message" name="settings[social.facebook.message]"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="settings-social-facebook-link" class="control-label col-md-4">
                    <?php echo trans('settings.social.facebook.link') ?>

                    <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.social.facebook.link_tooltip') ?>"></i>
                </label>

                <div class="col-md-8">
                    <input type="text" class="form-control" id="settings-social-facebook-link" v-model="facebook.link" name="settings[social.facebook.link]">
                </div>
            </div>

            <div class="form-group" v-show="facebook.link">
                <label for="settings-social-facebook-caption" class="control-label col-md-4">
                    <?php echo trans('settings.social.facebook.caption') ?>

                    <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.social.facebook.caption_tooltip') ?>"></i>
                </label>

                <div class="col-md-8">
                    <input type="text" class="form-control" id="settings-social-facebook-caption" maxlength="35" v-model="facebook.caption" name="settings[social.facebook.caption]">
                </div>
            </div>

            <div class="form-group">
                <label for="settings-social-facebook-place-id" class="control-label col-md-4">
                    <?php echo trans('settings.social.facebook.place_id') ?>

                    <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.social.facebook.place_id_tooltip') ?>"></i>
                </label>

                <div class="col-md-8">
                    <input type="text" class="form-control" id="settings-social-facebook-place-id" v-model="facebook.place" name="settings[social.facebook.place_id]">
                </div>
            </div>

        </div><!-- /col -->

        <div class="col-md-6">

            <div id="facebook-preview" v-show="showPreview">

                <div class="clearfix header">
                    <img src="<?php echo asset('assets/img/avatar.gif') ?>" alt="" class="avatar">

                    <div class="user-info">
                        <strong class="link">John Doe</strong> <span v-show="facebook.place">at <span class="link">Place Name</span></span>
                    </div>

                    <div class="info">
                        42 mins<!--<span role="presentation" aria-hidden="true"> · </span>Istanbul-->
                    </div>
                </div>

                <div class="message">
                    <p>{{ facebook.message | nl2br }}</p>
                </div>

                <div v-show="facebook.link" class="clearfix link-preview">
                    <div class="pull-left">
                        <img src="<?php echo asset('assets/img/website_preview.png') ?>" alt="" width="158" height="158">
                    </div>

                    <div class="link-details">

                        <div class="title">
                            Web page title for {{ facebook.link }}
                        </div>

                        <div class="description">
                            Sampe description for {{ facebook.link }}
                        </div>

                        <div class="caption">{{ facebook.caption }}</div>

                    </div>
                </div>
            </div>

        </div><!-- /col -->

    </div>
</facebook-preview>