<div class="form-group">
    <?php echo Form::label('settings[email.from.name]', trans('settings.email.from.name'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-4">
        <?php echo Form::text('settings[email.from.name]', setting('email.from.name'), ['class' => 'form-control']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[email.from.address]', trans('settings.email.from.address'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-4">
        <?php echo Form::text('settings[email.from.address]', setting('email.from.address'), ['class' => 'form-control']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[email.to]', trans('settings.email.to'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-4">
        <?php echo Form::text('settings[email.to]', setting('email.to'), ['class' => 'form-control']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[email.cc]', trans('settings.email.cc'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-4">
        <?php echo Form::text('settings[email.cc]', setting('email.cc'), ['class' => 'form-control']) ?>
    </div>
</div>
