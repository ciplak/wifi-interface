<p class="alert alert-info">
    <i class="fa fa-info-circle"></i> <?php echo trans('settings.sms.content_description')  ?>
</p>

<?php foreach (getLocales() as $locale => $name):
    $options = ['class' => 'form-control', 'maxlength' => 140];

    if (isRTL($locale)):
        $options['class'] .= ' rtl';
    endif;
    ?>
<div class="form-group">
    <?php echo Form::label("settings[sms.content.{$locale}]", trans('locale.options.' . $locale), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-4">
        <?php echo Form::textarea("settings[sms.content.{$locale}]", setting('sms.content.' . $locale), $options) ?>
    </div>
</div>
<?php endforeach; ?>