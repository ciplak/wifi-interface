<h5 class="page-header"><?php echo trans('settings.licence.title')?></h5>

<div class="form-group">
    <?php echo Form::label('settings[licence.owner]', trans('settings.licence.owner'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::text('settings[licence.owner]', setting('licence.owner'), ['class' => 'form-control']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[licence.info]', trans('settings.licence.info'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-4">
        <?php echo Form::text('settings[licence.info]', setting('licence.info'), ['class' => 'form-control']) ?>
    </div>
</div>

<h5 class="page-header"><?php echo trans('settings.sms.title')?></h5>

<div class="form-group">
    <label for="settings[sms.warning_threshold]" class="control-label col-md-4">
        <?php echo trans('settings.sms.warning_threshold') ?>
        <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('settings.sms.warning_threshold_tooltip') ?>"></i>
    </label>

    <div class="col-md-2">
        <?php echo Form::number('settings[sms.warning_threshold]', setting('sms.warning_threshold'), ['class' => 'form-control input-numeric', 'min' => 0, 'pattern' => '\d']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[access_code.max_resend_count]', trans('settings.access_code.max_resend_count'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-2">
        <?php echo Form::number('settings[access_code.max_resend_count]', setting('access_code.max_resend_count'), ['class' => 'form-control input-numeric', 'min' => 0, 'pattern' => '\d']) ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('settings[access_code.resend_wait_time]', trans('settings.access_code.resend_wait_time'), ['class' => 'control-label col-md-4']) ?>
    <div class="col-md-2">
        <?php echo Form::number('settings[access_code.resend_wait_time]', setting('access_code.resend_wait_time'), ['class' => 'form-control input-numeric', 'min' => 0, 'pattern' => '\d']) ?>
    </div>
    <label class="form-control-static"><?php echo trans('messages.date.seconds'); ?></label>
</div>