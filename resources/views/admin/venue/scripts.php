<script>
$(function() {
    <?php if (count($errors) == 1 && $errors->has('default_service_profile')):?>
    $('a[href=#tab-service-profiles]').tab('show');
    <?php endif ?>

    var serviceProfileList = $('#service-profile-list');

    $('#btn-save-profiles').click(function () {

        var checked = $('.cb-service-profile:checked');

        if (!checked.length) {
            alert('select service profile');
            return false;
        }

        checked.each(function (i, el) {
            var id = $(el).val();

            if ($('#service-profile-option-' + id).length) {
                return true;
            }
            var profile = $('<div class="radio" id="service-profile-option-' + id + '"><label><input type="radio" name="default_service_profile" value="' + id + '" />' + $(el).data('name') + '</label><input type="hidden" name="service_profiles[]" value="' + id + '" /> <a href="#" class="btn btn-xs btn-remove"><i class="fa fa-times"></i></a></div>');

            profile.appendTo(serviceProfileList);
        });

        $('#mdl-service-profiles').modal('hide');
    });

    serviceProfileList.on('click', 'a.btn-remove', function (event) {
        $(event.target).closest('div.radio').remove();
        return false;
    });

    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.

    CKEDITOR.config.toolbar = [
        ['Format', 'Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-', 'Print'],
        ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Table', '-', 'TextColor']
    ];

    <?php foreach (getLocales() as $locale => $name): ?>
    CKEDITOR.replace("terms_and_conditions_<?php echo $locale?>", {contentsLangDirection: '<?php echo isRTL($locale) ? 'rtl' : 'ltr'?>'});
    <?php endforeach ?>

    <?php if ($isZone):?>
    $(function () {
        var editors = $('#editors');
        $('#use_custom_text').change(function () {
            editors.toggleClass('hidden');
        });
    });
    <?php endif?>
});
</script>
