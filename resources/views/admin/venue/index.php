<?php
/**
 * @var \App\Models\Venue[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('venue.page_title') ?></h4>

            <div class="panel-control">
                <?php if (\Entrust::can('venues.add')):?>
                <a href="<?php echo route('admin.venue.add') ?>"  class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
                <?php endif ?>
            </div>
        </div><!-- /affix-header -->

    </div><!-- /panel-heading -->

    <div class="panel-body">

        <?php echo view('admin.venue.filters')->render()?>

        <div class="table-responsive">
            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('venue.not_found') ?></p>

            <?php else: ?>

            <table class="table table-condensed table-striped table-hover">
                <thead>
                <tr>
                    <th><?php echo trans('venue.columns.name') ?></th>
                    <th><?php echo trans('venue.columns.description') ?></th>
                    <th><?php echo trans('splash_page.type.offline') ?></th>
                    <th><?php echo trans('splash_page.type.online') ?></th>
                    <th><?php echo trans('venue.ssids') ?></th>
                    <th class="col-actions"></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($models as $model): ?>
                <tr>
                    <td>
                        <?php echo HTML::linkRoute('admin.venue.edit', $model->name, [$model]) ?>
                        <?php if ($model->is_default): ?><span class="label label-info"><?php echo trans('messages.default'); endif ?></span>
                    </td>
                    <td><?php echo e($model->description) ?></td>
                    <td><?php echo $model->login_page_name ?></td>
                    <td><?php echo $model->post_login_page_name ?></td>
                    <td><?php echo $model->ssid_name_list ?></td>
                    <td class="col-actions">
                        <a href="<?php echo route('admin.zone.add', ['venue' => $model]) ?>" class="btn btn-success btn-sm" data-tooltip="true" title="<?php echo trans('zone.add') ?>"><i class="fa fa-plus"></i> </a>
                        <?php if (\Entrust::can('venues.delete')): ?>
                        <a href="<?php echo route('admin.venue.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i> </a>
                        <?php endif ?>
                        <a href="<?php echo route('admin.device.index', ['location' => $model]) ?>" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('device.page_title') ?>"><i class="fa fa-wifi"></i> </a>
                    </td>
                </tr>
                    <?php if ($model->zones):?>
                        <?php foreach ($model->zones as $zone): ?>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;
                                    <i class="fa fa-ellipsis-v"></i>
                                    &nbsp;&nbsp;&nbsp;

                                    <?php echo HTML::linkRoute('admin.zone.edit', $zone->name, [$zone]) ?>
                                    <?php if ($zone->is_default): ?><span class="label label-info"><?php echo trans('messages.default') ?></span><?php endif ?>
                                </td>
                                <td><?php echo e($zone->description) ?></td>
                                <td><?php echo $zone->login_page_name ?></td>
                                <td><?php echo $zone->post_login_page_name ?></td>
                                <td><?php echo $zone->ssid_name_list ?></td>
                                <td class="col-actions">
                                    <a href="<?php echo route('admin.zone.delete', [$zone]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i> </a>
                                    <a href="<?php echo route('admin.device.index', ['location' => $zone]) ?>" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('device.page_title') ?>"><i class="fa fa-wifi"></i> </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif?>
                <?php endforeach ?>
                </tbody>
            </table>

            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->