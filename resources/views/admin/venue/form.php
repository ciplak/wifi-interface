<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\Property $model
 * @var array $smsProviders
 * @var array $tenants
 * @var array $loginPages
 * @var \App\Models\ServiceProfile[] $allServiceProfiles
 */
echo Form::model($model, ['class' => 'form-horizontal', 'autocomplete' => 'off', 'route' => ['admin.venue.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('venue.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.venue.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

            <?php if ($model->exists): ?>
            <?php if (!$model->is_default) { ?>
                <a href="<?php echo route('admin.venue.delete', [$model]) ?>" class="btn btn-danger btn-delete" data-tooltip="true"  title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i> </a>
            <?php } ?>
            <a href="<?php echo route('admin.device.index', ['location' => $model]) ?>" class="btn btn-default" data-tooltip="true" title="<?php echo trans('device.page_title') ?>"><i class="fa fa-wifi"></i> </a>
            <?php endif  ?>

            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">

        <?php if ($errors->count()): ?>
            <div class="alert alert-danger">
                <?php echo implode('<br>', $errors->all())?>
            </div>
        <?php endif ?>

        <div>

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab-general" aria-controls="generals" role="tab" data-toggle="tab"><?php echo trans('messages.general') ?></a></li>
<!--                <li role="presentation"><a href="#tab-service-profiles" aria-controls="service-profiles" role="tab" data-toggle="tab">--><?php //echo trans('service_profile.page_title') ?><!--</a></li>-->
                <li role="presentation"><a href="#tab-terms-and-conditions" aria-controls="terms-and-conditions" role="tab" data-toggle="tab"><?php echo trans('messages.terms_and_conditions') ?></a></li>
                <li role="presentation"><a href="#tab-ssids" aria-controls="ssids" role="tab" data-toggle="tab"><?php echo trans('venue.ssids') ?></a></li>
            </ul><!-- /nav-tabs -->

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-general">
                    <?php echo view('admin.venue.tabs.general', [
                        'model' => $model,
                        'smsProviders' => $smsProviders,
                        'loginPages' => $loginPages
                    ])->render() ?>
                </div>

                <!--<div role="tabpanel" class="tab-pane" id="tab-service-profiles">
                    <?php /*echo view('admin.venue.tabs.service_profile.index', [
                        'model' => $model,
                        'allServiceProfiles' => $allServiceProfiles
                    ])->render() */?>
                </div>-->

                <div role="tabpanel" class="tab-pane" id="tab-terms-and-conditions">
                    <?php echo view('admin.venue.tabs.terms_and_conditions', [
                        'model' => $model,
                    ])->render() ?>
                </div>

                <div role="tabpanel" class="tab-pane" id="tab-ssids">
                    <?php echo view('admin.venue.tabs.ssids', [
                        'model' => $model,
                    ])->render() ?>
                </div>

            </div><!-- /tab-content -->

        </div><!-- /tabs -->

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>

