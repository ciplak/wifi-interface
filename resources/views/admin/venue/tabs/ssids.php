<?php

$loginPageOptions = collect($loginPages)->put(0, trans('messages.select'))->map(function ($item, $key) {
    return ['value' => $key, 'text' => $item];
})->toJson();


$postLoginPageOptions = collect($postLoginPages)->put(0, trans('messages.select'))->map(function ($item, $key) {
    return ['value' => $key, 'text' => $item];
})->toJson();

?>
<ssid-list inline-template :items='<?php echo $model->ssids ? json_encode($model->ssids) : '[]' ?>' :login-pages='<?php echo $loginPageOptions ?>' :post-login-pages='<?php echo $postLoginPageOptions ?>'>
    <div>
        <p class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo trans('venue.ssid_description') ?></p>

        <table v-if="itemList.length > 0" class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th><?php echo trans('venue.ssid_name') ?></th>
                    <th><?php echo trans('splash_page.type.offline') ?></th>
                    <th><?php echo trans('splash_page.type.online') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item, index) in itemList">
                    <td>
                        <input type="text" class="form-control" placeholder="<?php echo trans('venue.ssid_name') ?>" :name="'ssids[' + index + '][name]'" v-model="item.name">
                    </td>

                    <td>
                        <select :name="'ssids[' + index + '][login_page_id]'" class="form-control" v-model="item.login_page_id">
                            <option v-for="option in loginPages" :value="option.value">
                                {{ option.text }}
                            </option>
                        </select>
                    </td>

                    <td>
                        <select :name="'ssids[' + index + '][post_login_page_id]'" class="form-control" v-model="item.post_login_page_id">
                            <option v-for="option in postLoginPages" :value="option.value">
                                {{ option.text }}
                            </option>
                        </select>
                    </td>

                    <td>
                        <button @click="remove(item)" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                    </td>

                </tr>
            </tbody>
        </table>

        <button @click="add" type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>

    </div>

</ssid-list>