<?php
/**
 * @var \App\Models\ServiceProfile[]|\Illuminate\Database\Eloquent\Collection $serviceProfiles
 * @var \App\Models\Property $model
 * @var \App\Models\ServiceProfile[]|\Illuminate\Database\Eloquent\Collection $allServiceProfiles
 */
?>
<div class="text-right margin-bottom">
    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#mdl-service-profiles"><i class="fa fa-search"></i> <?php echo trans('venue.manage_service_profiles') ?></a>
    <!--<a href="--><?php //echo route('admin.venue.addServiceProfile', [$model]) ?><!--"  class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>-->
</div>

<div id="service-profile-list">

    <?php //echo view('admin.venue.tabs.service_profile.grid', ['model' => $model])->render() ?>
    <?php if ($serviceProfiles->count()): ?>

        <?php foreach ($serviceProfiles as $serviceProfile): ?>
        <div class="radio" id="service-profile-option-<?php echo $serviceProfile->id ?>">
            <label><?php echo Form::radio('default_service_profile', $serviceProfile->id, $serviceProfile->pivot->is_default) ?> <?php echo e($serviceProfile->name), ' - ', $serviceProfile->duration_value ?></label>
            <input type="hidden" name="service_profiles[]" value="<?php echo $serviceProfile->id ?>">
            <a href="#" class="btn btn-xs btn-remove"><i class="fa fa-times"></i></a>
        </div>
        <?php endforeach ?>

    <?php else: ?>

        <?php if ($model instanceof \App\Models\Zone): ?>
            <div class="alert alert-warning">
                <?php echo trans('zone.service_profile_inherited') ?>
            </div>
        <?php endif ?>

    <?php endif ?>

</div><!-- /table-responsive -->

<div class="modal fade in" id="mdl-service-profiles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close') ?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo trans('service_profile.page_title') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo view('admin.service_profile.partials.grid', ['partial' => true, 'models' => $allServiceProfiles])->render() ?>

            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo trans('messages.close') ?></a>
                <a href="#" class="btn btn-success" id="btn-save-profiles"><i class="fa fa-plus"></i> <?php echo trans('messages.add') ?></a>
            </div>
        </div>
    </div>
</div>