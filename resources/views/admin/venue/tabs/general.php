<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\Property $model
 * @var array $currencies
 * @var array $smsProviders
 * @var array $countries
 * @var array $loginPages
 * @var array $postLoginPages
 * @var array $smartLoginOptions
 * @var array $smartLoginFrequencyTypeOptions
 */
?>
<div class="form-group<?php if ($errors->has('name')) echo ' has-error' ?>">
    <?php echo Form::label('name', trans('venue.columns.name'), ['class' => 'control-label col-md-2 required']) ?>
    <div class="col-md-10">
        <?php echo Form::text('name', null, ['class' => 'form-control', 'autofocus'])?>
        <?php if ($errors->has('name')): ?>
            <span class="help-block"><?php echo $errors->first('name') ?></span>
        <?php endif ?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('description', trans('venue.columns.description'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-10">
        <?php echo Form::text('description', null, ['class' => 'form-control'])?>
    </div>
</div>
<!--
<div class="form-group<?php if ($errors->has('address')) echo ' has-error' ?>">
    <?php echo Form::label('address', trans('venue.columns.address'), ['class' => 'control-label col-md-2 required']) ?>
    <div class="col-md-10">
        <?php echo Form::text('address', null, ['class' => 'form-control'])?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('city', trans('venue.columns.city'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-2">
        <?php echo Form::text('city', null, ['class' => 'form-control'])?>
    </div>

    <?php echo Form::label('post_code', trans('venue.columns.post_code'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-1">
        <?php echo Form::text('post_code', null, ['class' => 'form-control'])?>
    </div>
</div>

<div class="form-group">
    <?php echo Form::label('state', trans('venue.columns.state'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-2">
        <?php echo Form::text('state', null, ['class' => 'form-control'])?>
    </div>

    <?php echo Form::label('country_code', trans('venue.columns.country'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-md-2">
        <?php echo Form::select('country_code', $countries, null, ['class' => 'form-control select']) ?>
    </div>
</div>
-->
<div class="form-group<?php if ($errors->has('login_page_id')) echo ' has-error' ?>">
    <?php echo Form::label('login_page_id', trans('splash_page.type.offline'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-sm-2">
        <?php echo Form::select('login_page_id', $loginPages, null, ['class' => 'form-control select', 'placeholder' => trans('messages.select')]) ?>
    </div>
</div>

<div class="form-group">
    <div<?php if ($errors->has('post_login_page_id')) echo ' class="has-error"' ?>>
        <?php echo Form::label('post_login_page_id', trans('splash_page.type.online'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-sm-2">
            <?php echo Form::select('post_login_page_id', $postLoginPages, null, ['class' => 'form-control select', 'placeholder' => trans('messages.select')]) ?>
        </div>
    </div>

    <div<?php if ($errors->has('post_login_url')) echo ' class="has-error"' ?>>
        <label for="post_login_url" class="control-label col-md-2">
            <?php echo trans('venue.columns.post_login_url') ?>
            <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('messages.post_login_url.tooltip') ?>"></i>
        </label>

        <div class="col-sm-2">
            <?php echo Form::text('post_login_url', null, ['class' => 'form-control', 'id' => 'post_login_url']) ?>
        </div>

        <?php if ($errors->has('post_login_url')): ?>
            <div class="col-md-6 col-md-offset-6">
                <span class="help-block"><?php echo $errors->first('post_login_url') ?></span>
            </div>
        <?php endif ?>
    </div>
</div>

<div class="form-group<?php if ($errors->has('default_service_profile')) echo ' has-error' ?>">
    <?php echo Form::label('default_service_profile', trans('service_profile.title'), ['class' => 'control-label col-md-2 required']) ?>
    <div class="col-sm-2">
        <?php echo Form::select('default_service_profile', $allServiceProfiles->pluck('name', 'id'), $model->getSelectedDefaultServiceProfileId(), ['class' => 'form-control select', 'placeholder' => trans('messages.select')]) ?>
    </div>
    <?php if ($errors->has('default_service_profile')): ?>
        <div class="col-md-10 col-md-offset-2">
            <span class="help-block"><?php echo $errors->first('default_service_profile') ?></span>
        </div>
    <?php endif ?>
</div>

<div class="form-group<?php if ($errors->has('sms_provider_id')) echo ' has-error' ?>">
    <?php echo Form::label('sms_provider_id', trans('sms_provider.title'), ['class' => 'control-label col-md-2']) ?>
    <div class="col-sm-2">
        <?php echo Form::select('sms_provider_id', $smsProviders, null, ['class' => 'form-control select', 'placeholder' => trans('messages.select')]) ?>
    </div>
</div>

<div class="form-group <?php if ($errors->has('smart_login') || $errors->has('smart_login_frequency')) echo 'has-error' ?>">
    <label for="smart_login" class="control-label col-md-2">
        <?php echo trans('venue.columns.smart_login') ?>
        <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('messages.property.smart_login.tooltip') ?>"></i>
    </label>

    <div class="col-md-2">
        <?php echo Form::select('smart_login', $smartLoginOptions, null, ['class' => 'form-control select', 'id' => 'smart_login']) ?>
    </div>

    <div class="smart-login-options hidden">
        <div class="col-md-1">
            <?php echo Form::text('smart_login_frequency', null, ['class' => 'form-control']) ?>
        </div>
        <div class="col-md-1">
            <?php echo Form::select('smart_login_frequency_type', $smartLoginFrequencyTypeOptions, null, ['class' => 'form-control']) ?>
        </div>
    </div>

    <?php if ($errors->has('smart_login_frequency')): ?>
        <div class="col-md-10 col-md-offset-2">
            <span class="help-block"><?php echo $errors->first('smart_login_frequency') ?></span>
        </div>
    <?php endif ?>
</div><!-- /form-group -->

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
            <label>
                <?php
                $options = [];
                if ($model->is_default) {
                    $options['disabled'] = 'disabled';
                }
                echo Form::checkbox('is_default', 1, null, $options), ' ', trans('messages.default')
                ?>
            </label>
        </div>
    </div>
</div>