<?php
/**
 * @var \App\Models\Property $model
 */
$content = $model->terms_and_conditions;
$isZone = $model instanceof \App\Models\Zone;
view()->share('isZone', $isZone);
?>

<?php if ($isZone):?>
    <div class="checkbox">
        <label>
            <?php echo Form::checkbox('use_custom_text', 1, null, ['id' => 'use_custom_text'])?>
            <?php echo trans('zone.override_terms_and_conditions') ?>
        </label>
    </div>
<?php endif ?>

<div id="editors" <?php if ($isZone && !$model->use_custom_text) echo 'class="hidden"' ?>>
<?php foreach (getLocales() as $locale => $name): ?>
<div class="col-md-6">
    <?php echo Form::label("terms_and_conditions[$locale]", trans('locale.options.' . $locale), ['class' => 'control-label']) ?>
    <?php echo Form::textarea("terms_and_conditions[$locale]", isset($content[$locale]) ? $content[$locale] : '', ['class' => 'form-control', 'id' => "terms_and_conditions_$locale"])?>
</div>
<?php endforeach ?>
</div>