<?php
echo Form::open(['class' => 'form-inline margin-bottom', 'method' => 'GET']) ?>

    <div class="form-group">
        <?php echo Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => trans('messages.name')]) ?>
    </div>

    <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
    <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>

<?php echo Form::close() ?>