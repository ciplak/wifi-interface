<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('tenant.columns.info') ?></h4>
    </div>

    <div class="panel-body">

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab-info" aria-controls="home" role="tab" data-toggle="tab"><?php echo trans('tenant.columns.info') ?></a></li>
                <?php if ($model->exists): ?>
                    <li role="presentation"><a href="#tab-sms" aria-controls="settings" role="tab" data-toggle="tab"><?php echo trans('tenant.sms_credits.title') ?></a></li>
                <?php endif ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-info">
                    <?php echo view('admin.tenant.partials.profile.form')->render() ?>
                </div>

                <?php if ($model->exists): ?>
                    <div role="tabpanel" class="tab-pane" id="tab-sms">
                        <?php echo view('admin.tenant.partials.profile.sms_history')->render() ?>
                    </div>
                <?php endif ?>
            </div>

        </div>

    </div><!-- /panel-body -->

</div><!-- /panel -->