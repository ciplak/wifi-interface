<!-- Small modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal">Click</button>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="credit" class="control-label">Credit:</label>
                    <input type="text" class="form-control" id="credit" name="credit">
                </div>

                <div class="form-group">
                    <label for="refno" class="control-label">Ref No:</label>
                    <input type="text" class="form-control" id="refno" name="refno">
                </div>

                <div class="form-group">
                    <label for="description" class="control-label">Description:</label>
                    <textarea class="form-control" id="description" name="description"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->







