<?php
/**
 * @var array $venues
 * @var array $serviceProfiles
 * @var array $typesForCreate
 */
?>
<div class="form-horizontal">

    <div class="form-group" :class="{'has-error': form.errors.has('credit')}">
        <?php echo \Form::label('credit', trans('tenant.sms_credits.credits'), ['class' => 'control-label col-md-3']) ?>

        <div class="col-md-6">
            <?php echo \Form::text('credit', null, ['class' => 'form-control', 'v-model' => 'form.credit']) ?>
            <span class="help-block">{{ form.errors.get('credit') }}</span>
        </div>
    </div>

    <div class="form-group" :class="{'has-error': form.errors.has('description')}">
        <?php echo \Form::label('description', trans('tenant.sms_credits.description'), ['class' => 'control-label col-md-3']) ?>

        <div class="col-md-6">
            <?php echo \Form::text('description', null, ['class' => 'form-control', 'v-model' => 'form.description']) ?>
            <span class="help-block">{{ form.errors.get('description') }}</span>
        </div>
    </div>

</div>