<?php
/**
 * @var \App\Http\Middleware\Tenant $model
 */
?>
<div class="clearfix">
    <div class="pull-right">
        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-sms-credit-form" data-tooltip="true" title="<?php echo trans('tenant.sms_credits.install') ?>"><i class="fa fa-plus"></i></a>
    </div>

    <label class="control-label col-md-2"><?php echo trans('tenant.sms_credits.total'), ': '?> <span id="total-sms-credits"><?php echo $model->total_sms_credits ?></span></label>
    <label class="control-label col-md-2"><?php echo trans('tenant.sms_credits.average_daily_sms_usage'), ': ', $model->average_daily_sms_usage ?></label>
</div>

<div id="sms-history-container">
    <?php echo view('admin.tenant.partials.sms_credits.history')->render(); ?>
</div>

<?php echo view('admin.tenant.partials.sms_credits.modal')->render(); ?>

<?php echo view('admin.tenant.partials.sms_credits.chart')->render(); ?>
