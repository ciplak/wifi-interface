<?php
/**
 * @var string $smsUsageChartCategories
 * @var string $smsUsageChartData
 */
?>
<div id="sms-credit-chart"></div>
<script>
    $(function () {
        $('#sms-credit-chart').highcharts({
            chart: {
                type: 'area'
            },
            title: {
                text: '<?php echo trans('tenant.sms_credits.average_daily_sms_usage') ?>'
            },
            xAxis: {
                categories: <?php echo $model->smsUsageChartCategories ?>,
                title: {
                    text: '<?php echo trans('messages.duration.days') ?>'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: '<?php echo trans('messages.count')?>'
                }

            },
            plotOptions: {
                area: {
                    fillOpacity: 0.5
                }
            },
            series: [
                {
                    name: '<?php echo trans('tenant.sms_credits.average_daily_sms_usage') ?>',
                    data: <?php echo $model->smsUsageChartData ?>
                }
            ]
        });
    });
</script>