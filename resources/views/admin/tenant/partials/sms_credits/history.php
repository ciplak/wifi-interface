<?php
/**
 * @var \Illuminate\Support\Collection|\App\Models\SmsLog[] $credits
 */
if (!$credits->isEmpty()):
    ?>
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th><?php echo trans('tenant.sms_credits.ref_no') ?></th>
            <th><?php echo trans('tenant.sms_credits.credits') ?></th>
            <th><?php echo trans('tenant.sms_credits.description') ?></th>
            <th><?php echo trans('tenant.sms_credits.date') ?></th>
            <th class="col-datetime"></th>
            <th class="col-actions"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($credits AS $credit): ?>
            <tr>
                <td><?php echo $credit->ref_no ?></td>
                <td><?php echo $credit->credit ?></td>
                <td><?php echo e($credit->description) ?></td>
                <td class="col-datetime"><?php echo $credit->created_at_formatted ?></td>
                <td></td>
                <td class="col-actions">
                    <a href="<?php echo route('admin.tenant.delete_sms_credit', ['tenantId' => $credit->tenant_id, 'id' => $credit->id]) ?>"
                       class="btn btn-xs btn-danger btn-delete-credits">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>

<?php endif ?>

<script>
    // delete button confirmation
    $(function () {
        $("body").on('click', '.btn-delete-credits', function (e) {
            var $this = $(this),
                message = 'Delete this item?';

            bootbox.confirm({
                message: message,
                buttons: {
                    'cancel': {
                        label: "Don't delete",
                        className: "btn-default"
                    },
                    'confirm': {
                        label: "Delete",
                        className: "btn-danger"
                    }
                },
                callback: function (result) {
                    if (!result) return;

                    $.getJSON($this.attr('href'), function (data) {
                        if (data.status == true) {
                            $this.closest('tr').remove();
                            $("#total-sms-credits").text(data.totalSmsCredits);
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    });

                }
            });

            return false;
        });

    });

</script>
