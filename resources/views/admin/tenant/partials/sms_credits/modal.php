<div class="modal fade in" id="modal-sms-credit-form" tabindex="-1" role="dialog" aria-labelledby="SmsCreditForm" aria-hidden="false">
    <sms-credits-form inline-template :tenant-id="<?php echo $model->id ?>">

        <form @submit.prevent="submit">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button @click="closeForm" type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close') ?>">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo trans('tenant.sms_credits.title') ?></h4>
                    </div>
                    <div class="modal-body">
                        <?php echo view('admin.tenant.partials.sms_credits.modal_form')->render() ?>
                    </div>
                    <div class="modal-footer">
                        <button @click="closeForm" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo trans('messages.cancel') ?></button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> <?php echo trans('messages.add') ?></button>
                    </div>
                </div>
            </div>

        </form>
    </sms-credits-form>
</div>