<tenant-profile-form inline-template>

    <form class="form-horizontal" autocomplete="off" @submit.prevent="updateProfile">

        <div class="text-right margin-bottom">
            <button :disabled="form.busy" class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>">
                    <span v-if="form.busy">
                        <i class="fa fa-refresh fa-spin"></i> <?php echo trans('messages.updating') ?>
                    </span>
                <i v-else class="fa fa-check"></i>
            </button>
        </div>

        <div class="form-group" :class="{'has-error': form.errors.has('name')}">
            <label for="name" class="control-label col-md-2 required"><?php echo trans('tenant.columns.name') ?></label>
            <div class="col-md-10">
                <input type="text" name="name" id="name" class="form-control" autofocus v-model="form.name" />
                <span class="help-block">{{ form.errors.get('name') }}</span>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="control-label col-md-2"><?php echo trans('tenant.columns.description')?></label>
            <div class="col-md-10">
                <input type="text" name="description" id="description" class="form-control" v-model="form.description" />
            </div>
        </div>

        <div class="form-group" :class="{'has-error': form.errors.has('address')}">
            <label for="address" class="control-label col-md-2 required"><?php echo trans('messages.address')?></label>
            <div class="col-md-10">
                <input type="text" name="address" id="address" class="form-control" v-model="form.address" />
                <span class="help-block">{{ form.errors.get('address') }}</span>
            </div>
        </div>

        <div class="form-group">
            <label for="city" class="control-label col-md-2"><?php echo trans('messages.city')?></label>
            <div class="col-md-2">
                <input type="text" name="city" id="city" class="form-control" v-model="form.city" />
            </div>

            <label for="post_code" class="control-label col-md-2"><?php echo trans('messages.post_code')?></label>
            <div class="col-md-1">
                <input type="text" name="post_code" id="post_code" class="form-control" v-model="form.post_code" />
            </div>
        </div>

        <div class="form-group">
            <label for="state" class="control-label col-md-2"><?php echo trans('messages.state')?></label>
            <div class="col-md-2">
                <input type="text" name="state" id="state" class="form-control" v-model="form.state" />
            </div>

            <label for="country_code" class="control-label col-md-2"><?php echo trans('messages.country')?></label>
            <div class="col-md-2">
                <?php echo Form::select('country_code', $countries, null, ['class' => 'form-control', 'v-model' => 'form.country_code']) ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('login_page_id') || $errors->has('post_login_page_id')) echo ' has-error' ?>">
            <?php echo Form::label('login_page_id', trans('splash_page.type.offline'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-sm-2">
                <?php echo Form::select('login_page_id', $preLoginPages, null, ['class' => 'form-control', 'placeholder' => trans('messages.select'), 'v-model' => 'form.login_page_id']) ?>
            </div>

            <?php echo Form::label('post_login_page_id', trans('splash_page.type.online'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-sm-2">
                <?php echo Form::select('post_login_page_id', $postLoginPages, null, ['class' => 'form-control', 'placeholder' => trans('messages.select'), 'v-model' => 'form.post_login_page_id']) ?>
            </div>
        </div>

        <div class="form-group">
            <label for="sms_provider_id" class="control-label col-md-2"><?php echo trans('sms_provider.title')?></label>
            <div class="col-sm-2">
                <?php echo Form::select('sms_provider_id', $smsProviders, null, ['class' => 'form-control', 'placeholder' => trans('messages.select'), 'v-model' => 'form.sms_provider_id']) ?>
            </div>
        </div>

        <div class="form-group">
            <label for="post_login_url" class="control-label col-md-2"><?php echo trans('messages.post_login_url.title') ?></label>
            <div class="col-sm-2">
                <input type="text" name="post_login_url" id="post_login_url" class="form-control" v-model="form.post_login_url" />
            </div>
        </div>

    </form>

</tenant-profile-form>