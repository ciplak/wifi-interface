<sms-credits-history inline-template>
    <div>
        <strong><?php echo trans('tenant.sms_credits.total') ?></strong>: {{ total_sms_credits }}
        <strong><?php echo trans('tenant.sms_credits.average_daily_sms_usage') ?></strong>: {{ average_daily_sms_usage }}

        <div v-if="items.length">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><?php echo trans('tenant.sms_credits.ref_no') ?></th>
                    <th><?php echo trans('tenant.sms_credits.credits') ?></th>
                    <th><?php echo trans('tenant.sms_credits.description') ?></th>
                    <th><?php echo trans('tenant.sms_credits.date') ?></th>
                </tr>
                </thead>

                <tbody>
                <tr v-for="item in items">
                    <td>{{ item.ref_no }}</td>
                    <td>{{ item.credit }}</td>
                    <td>{{ item.description }}</td>
                    <td class="col-datetime">{{ item.created_at }}</td>
                </tr>
                </tbody>

            </table>
        </div>
        <div v-else>
            <div class="alert alert-warning"><?php echo trans('tenant.sms_credits.not_found') ?></div>
        </div>
    </div>

</sms-credits-history>