<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\Property $model
 * @var array $preLoginPages
 * @var array $postLoginPages
 * @var array $smsProviders
 */
echo Form::model($model, ['class' => 'form-horizontal', 'autocomplete' => 'off', 'route' => ['admin.tenant.save', $model]]) ?>

    <div class="text-right margin-bottom">
        <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
    </div>

    <div class="form-group<?php if ($errors->has('name')) echo ' has-error' ?>">
        <?php echo Form::label('name', trans('tenant.columns.name'), ['class' => 'control-label col-md-2 required']) ?>
        <div class="col-md-10">
            <?php echo Form::text('name', null, ['class' => 'form-control', 'autofocus'])?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('description', trans('tenant.columns.description'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-md-10">
            <?php echo Form::text('description', null, ['class' => 'form-control'])?>
        </div>
    </div>

    <div class="form-group<?php if ($errors->has('address')) echo ' has-error' ?>">
        <?php echo Form::label('address', trans('messages.address'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-md-10">
            <?php echo Form::text('address', null, ['class' => 'form-control'])?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('city', trans('messages.city'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-md-2">
            <?php echo Form::text('city', null, ['class' => 'form-control'])?>
        </div>

        <?php echo Form::label('post_code', trans('messages.post_code'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-md-1">
            <?php echo Form::text('post_code', null, ['class' => 'form-control'])?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('state', trans('messages.state'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-md-2">
            <?php echo Form::text('state', null, ['class' => 'form-control'])?>
        </div>

        <?php echo Form::label('country_code', trans('messages.country'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-md-2">
            <?php echo Form::select('country_code', $countries, null, ['class' => 'form-control select']) ?>
        </div>
    </div>

    <div class="form-group<?php if ($errors->has('login_page_id') || $errors->has('post_login_page_id')) echo ' has-error' ?>">
        <?php echo Form::label('login_page_id', trans('splash_page.type.offline'), ['class' => 'control-label col-md-2 required']) ?>
        <div class="col-sm-2">
            <?php echo Form::select('login_page_id', $preLoginPages, null, ['class' => 'form-control select', 'placeholder' => trans('messages.select')]) ?>
        </div>

        <?php echo Form::label('post_login_page_id', trans('splash_page.type.online'), ['class' => 'control-label col-md-2 required']) ?>
        <div class="col-sm-2">
            <?php echo Form::select('post_login_page_id', $postLoginPages, null, ['class' => 'form-control select', 'placeholder' => trans('messages.select')]) ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('sms_provider_id', trans('sms_provider.title'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-sm-2">
            <?php echo Form::select('sms_provider_id', $smsProviders, null, ['class' => 'form-control select', 'placeholder' => trans('messages.select')]) ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('post_login_url', trans('venue.columns.post_login_url'), ['class' => 'control-label col-md-2']) ?>
        <div class="col-sm-2">
            <?php echo Form::text('post_login_url', null, ['class' => 'form-control']) ?>
        </div>
    </div>

<?php echo Form::close() ?>