<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('tenant.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.tenant.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>
        </div>

    </div>

    <div class="panel-body">

        <?php if ($errors->count()): ?>
            <div class="alert alert-danger">
                <?php echo implode('<br>', $errors->all())?>
            </div>
        <?php endif ?>

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab-info" aria-controls="home" role="tab" data-toggle="tab"><?php echo trans('tenant.columns.info') ?></a></li>
                <?php if ($model->exists): ?>
                    <li role="presentation"><a href="#tab-sms" aria-controls="settings" role="tab" data-toggle="tab"><?php echo trans('tenant.sms_credits.title') ?></a></li>
                <?php endif ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-info">
                    <?php echo view('admin.tenant.partials.info')->render() ?>
                </div>

                <?php if ($model->exists): ?>
                    <div role="tabpanel" class="tab-pane" id="tab-sms">
                        <?php echo view('admin.tenant.partials.sms_credits.form')->render() ?>
                    </div>
                <?php endif ?>
            </div>

        </div>

    </div><!-- /panel-body -->

</div><!-- /panel -->
<script>
    $(function () {
        $(document).on('click', '.nav-tabs a[href="#tab-sms"]', function (e) {
            e.preventDefault();
            resizeCharts();
        });
    });
</script>