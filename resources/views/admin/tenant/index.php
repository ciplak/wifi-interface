<?php
/**
 * @var \App\Models\Property[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('tenant.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.tenant.add') ?>" class="btn btn-success"><i class="fa fa-plus"></i></a>
        </div>

    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('tenant.not_found') ?></p>

            <?php else: ?>

                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th><?php echo trans('tenant.columns.id') ?></th>
                        <th><?php echo trans('tenant.columns.name') ?></th>
                        <th><?php echo trans('tenant.columns.description') ?></th>
                        <th><?php echo trans('messages.country') ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    foreach ($models as $model): ?>
                        <tr>
                            <td><?php echo $model->id ?></td>
                            <td><?php echo HTML::linkRoute('admin.tenant.edit', $model->name, [$model]) ?></td>
                            <td><?php echo $model->description ?></td>
                            <td><?php echo $model->country_name ?></td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>

            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->








