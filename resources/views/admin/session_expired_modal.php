<div class="modal fade" id="modal-session-expired" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Session Expired
                </h4>
            </div>

            <div class="modal-body">
                Your session has expired. Please login again to continue.
            </div>

            <!-- Modal Actions -->
            <div class="modal-footer">
                <a href="<?php echo route('login')?>">
                    <button type="button" class="btn btn-primary">
                        Go To Login
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>