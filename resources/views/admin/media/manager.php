<media-manager inline-template url="<?php echo url('api/v1/media') ?>"<?php if (isset($model_id)) echo 'model-id="', $model_id, '"'?>>
    <div id="media-manager">

        <div v-if="errors">
            <div class="alert alert-danger">
                {{ errors | json }}
            </div>
        </div>

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab-media-uploader" role="tab" data-toggle="tab"><?php echo trans('media.upload_files') ?></a></li>
            <li role="presentation"><a href="#tab-media-library" role="tab" data-toggle="tab"><?php echo trans('media.media_library') ?></a></li>
        </ul><!-- /nav-tabs -->

        <div class="tab-content">

            <div role="tabpanel" class="tab-pane active" id="tab-media-uploader">

                <dropzone id="media-manager-dropzone" url="<?php echo url('api/v1/media') ?>">
                    <div class="dropzone text-center">
                        <div class="dz-message">
                            <?php echo trans('media.drop_files_here_to_upload') ?>

                            <p><?php echo trans('media.or') ?></p>

                            <p><a href="#" class="btn btn-default center-block btn-large trigger-dropzone" style="width:200px"><?php echo trans('media.select_file') ?></i></a></p>

                            <p><?php echo trans('media.max_upload_file_size', ['size' => '1MB']) ?></p>
                        </div>
                    </div>
                </dropzone>

            </div><!-- /tab-pane -->

            <div role="tabpanel" class="tab-pane" id="tab-media-library">

                <div v-show="showProgress" class="progress" id="total-progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" :style="{ width: progress + '%' }"></div>
                </div>

                    <div class="col-md-9">

                        <div class="row" style="max-height: 400px; overflow-y: scroll">
                            <div v-if="items.length">
                                <div v-for="item in items" class="col-md-2 col-sm-2 col-xs-3">
                                    <a @click="selectItem(item)" href="#" class="thumbnail" :class="{'selected-media' : selectedItem == item}">
                                        <img :src="item.url" :alt="item.file" style="height:80px; width:60px;">
                                    </a>
                                </div>
                            </div>
                            <div v-else>
                                <div class="alert alert-warning">
                                    <?php echo trans('media.media_library_empty') ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div v-if="selectedItem" class="selected-media-details">
                            <a :href="selectedItem.url" target="_image">
                                <img :src="selectedItem.url" :alt="selectedItem.file" class="img-responsive center-block selected-thumbnail">
                            </a>

                            <p class="selected-filename">{{ selectedItem.file }}</p>
                            <p>{{ selectedItem.uploaded_at }}</p>
                            <p>{{ selectedItem.size_for_humans }}</p>
                            <p>{{ selectedItem.dimensions }}</p>
                            <p>
                                <a href="#" :disabled="busy" @click="remove(selectedItem)" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> <?php echo trans('messages.delete') ?></a>
                            </p>
                        </div>
                    </div>

            </div><!-- /tab-pane -->

        </div><!-- /tab-content -->

    </div>
</media-manager>
