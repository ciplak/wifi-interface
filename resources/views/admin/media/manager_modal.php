<media-manager-modal inline-template :show-snippets="true">

    <div class="modal fade" tabindex="-1" role="dialog" id="media-uploader-modal">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo trans('media.insert_media') ?></h4>
                </div>

                <div class="modal-body">

                    <?php echo view('admin.media.manager') ?>

                </div>

                <div class="modal-footer">
                    <span v-if="showSnippets">
                        <button @click="embedImageTag" :disabled="!selectedItem" class="btn btn-primary" data-dismiss="modal"><?php echo trans('media.insert_image_tag') ?></button>
                        <button @click="embedImageUrl" :disabled="!selectedItem" class="btn btn-primary" data-dismiss="modal"><?php echo trans('media.insert_image_url') ?></button>
                    </span>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('messages.close')?></button>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</media-manager-modal>