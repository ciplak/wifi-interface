<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo trans('media.title') ?></h3>
    </div>

    <div class="panel-body">
        <?php echo view('admin.media.manager')->render() ?>
    </div>
</div>
