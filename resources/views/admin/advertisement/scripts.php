<script>
$(function() {
    var typeList = $('#type');

    typeList.change(function () {
        var val = parseInt($(this).val());

        if (val == 1) {
            $('#body').closest('.form-group').addClass('hidden');
            $('#image').closest('.form-group').removeClass('hidden');
        } else {
            $('#body').closest('.form-group').removeClass('hidden');
            $('#image').closest('.form-group').addClass('hidden');

        }
    });

    typeList.trigger('change');
});
</script>