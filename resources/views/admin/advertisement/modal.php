<div class="modal fade" tabindex="-1" role="dialog" id="mdl-advertisement">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo trans('advertisement.page_title') ?></h4>
            </div>

            <div class="modal-body">

                <p><?php echo trans('advertisement.select_to_insert') ?></p>

                <?php
                /** @var array $advertisementList */
                echo Form::select('advertisement_list', $advertisementList, null, ['class' => 'form-control select', 'placeholder' => trans('messages.select'), 'id' => 'advertisement-list', 'style' => 'width:100%']) ?>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('messages.close')?></button>
                <button type="button" class="btn btn-primary" id="btn-advertisement-modal-add"><?php echo trans('messages.add')?></button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
$(function() {
    $('#btn-advertisement-modal-add').click(function() {
        var id = $('#advertisement-list').val();
        if (!id) {
            bootbox.alert('<?php echo trans('advertisement.select_to_insert') ?>');
            return false;
        }
        embedSnippet("Advertisement ad='" + id + "'");
        $('#mdl-advertisement').modal('hide');
        return false;
    });
});
</script>