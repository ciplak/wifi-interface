<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\Advertisement $model
 * @var array $typeOptions
 * @var array $statusOptions
 */
echo Form::model($model, ['class' => 'form-horizontal', 'autocomplete' => 'off', 'files' => true, 'route' => ['admin.advertisement.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('advertisement.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.advertisement.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">

        <div class="form-group<?php if ($errors->has('title')) echo ' has-error' ?>">
            <?php echo Form::label('title', trans('advertisement.columns.title'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-8">
                <?php echo Form::text('title', null, ['class' => 'form-control'])?>
                <?php if ($errors->has('title')): ?>
                    <span class="help-block"><?php echo $errors->first('title') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('type')) echo ' has-error' ?>">
            <?php echo Form::label('type', trans('advertisement.columns.type'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::select('type', $typeOptions, null, ['class' => 'form-control'])?>
                <?php if ($errors->has('type')): ?>
                    <span class="help-block"><?php echo $errors->first('type') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('body')) echo ' has-error'; if ($model->type == 1) echo ' hidden' ?>">
            <?php echo Form::label('body', trans('advertisement.columns.body'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-8">
                <?php echo Form::textarea('body', null, ['class' => 'form-control ckeditor', 'rows' => 5])?>
                <?php if ($errors->has('body')): ?>
                    <span class="help-block"><?php echo $errors->first('body') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($model->type == \App\Models\Advertisement::TYPE_TEXT) echo ' hidden' ?>">
            <?php echo Form::label('image', trans('advertisement.columns.image'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-8">
                <?php echo Form::file('image', ['class' => 'form-control file-input margin-bottom', 'accept' => 'image/*']) ?>
                <div id="logo-preview-container" class="text-center margin-top <?php if (!$model->image) echo 'hidden' ?>" style="border:1px solid #ccc; padding:20px">
                    <?php if ($model->image): ?>
                        <a href="<?php echo $model->image_url ?>" target="_full">
                            <img src="<?php echo $model->image_url ?>" alt="" class="img-responsive center-block" style="height: 250px;">
                        </a>
                    <?php endif ?>
                </div>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('url')) echo ' has-error' ?>">
            <?php echo Form::label('url', trans('advertisement.columns.url'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-8">
                <?php echo Form::text('url', null, ['class' => 'form-control'])?>
                <?php if ($errors->has('url')): ?>
                    <span class="help-block"><?php echo $errors->first('url') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('start_date') || $errors->has('end_date')) echo ' has-error' ?>">
            <?php echo Form::label('url', trans('advertisement.columns.date'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <div class="input-daterange input-group" id="date-range-picker">
                    <?php echo Form::text('start_date', $model->start_date_value, ['class' => 'form-control']) ?>
                    <span class="input-group-addon">to</span>
                    <?php echo Form::text('end_date', $model->end_date_value, ['class' => 'form-control']) ?>
                </div>
                <?php if ($errors->has('start_date') || $errors->has('end_date')): ?>
                    <span class="help-block"><?php echo $errors->first('start_date'), ' ', $errors->first('end_date') ?></span>
                <?php endif ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('status')) echo ' has-error' ?>">
            <?php echo Form::label('status', trans('advertisement.columns.status'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-2">
                <?php echo Form::select('status', $statusOptions, null, ['class' => 'form-control'])?>
                <?php if ($errors->has('status')): ?>
                    <span class="help-block"><?php echo $errors->first('status') ?></span>
                <?php endif ?>
            </div>
        </div>

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>
