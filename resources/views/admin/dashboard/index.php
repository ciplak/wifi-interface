<dashboard inline-template :locations='<?php echo $locations ?>'>
    <div>

        <div class="rowe margin-bottom form-inline text-right">
            <?php if (\Entrust::can('reports.view-global')): ?>

            <select class="form-control" id="location" placeholder="<?php echo trans('venue.all') ?>" :disabled="$parent.loading" @change="loadData" v-model="location">
                <option v-for="(value, key) in locations" :value="key">{{ value }}</option>
            </select>

            <div v-if="datesEnabled" id="dashboard-report-date-range" class="form-control">
                <i class="fa fa-calendar"></i>
                <span>{{ startDateFormatted }} - {{ endDateFormatted }}</span>
            </div>
            <?php endif ?>
        </div>

        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                <info-box :data="data.numberOfOnlineGuests" :text="'<?php echo trans('report.online_guests') ?>'" :icon="'users'"></info-box>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                <info-box :data="data.numberOfMaxOnlineGuests" :text="'<?php echo trans('dashboard.max_online_guests') ?>'" :icon="'users'"></info-box>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                <info-box :data="data.numberOfLogins" :text="'<?php echo trans('dashboard.number_of_logins') ?>'" :icon="'sign-in'"></info-box>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                <info-box :data="data.numberOfImpressions" :text="'<?php echo trans('dashboard.impressions') ?>'" :icon="'eye'"></info-box>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                <info-box :data="data.numberOfNewLogins" :text="'<?php echo trans('dashboard.new_logins') ?>'" :icon="'sign-in'"></info-box>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                <info-box :data="data.totalTrafficOfDay" :text="'<?php echo trans('dashboard.total_traffic') ?>'" :icon="'line-chart'"></info-box>
            </div>

        </div><!-- /row -->

        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-block">
                        <online-users-chart></online-users-chart>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-block">
                        <device-usage-shares-chart></device-usage-shares-chart>
                    </div>
                </div>
            </div>

        </div><!-- /row -->

    </div>
</dashboard>