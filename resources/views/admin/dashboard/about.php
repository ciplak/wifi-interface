<div class="row">
    <div class="col-md-4 col-md-offset-4">

        <div class="panel panel-default">
            <div class="panel-body">

                <div class="text-center">
                    <h1><?php echo config('app.name') ?> <?php echo config('app.version') ?></h1>

                    <?php if ($licenceOwner = setting('licence.owner')): ?>
                        <p>Licensed to <?php echo $licenceOwner ?></p>
                    <?php endif ?>

                    <?php if ($licenceInfo = setting('licence.info')): ?>
                        <p>(<?php echo $licenceInfo ?>)</p>
                    <?php endif ?>
                </div>

            </div>
        </div><!-- /panel -->

    </div><!-- /col -->
</div><!-- /row -->