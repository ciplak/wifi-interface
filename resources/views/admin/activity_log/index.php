<activity-log inline-template>

    <div class="panel panel-default panel-actions">

        <div class="panel-heading">
            <div class="affix-header">
                <h4 class="panel-title"><?php echo trans('messages.activity_logs') ?></h4>
            </div>
        </div>

        <div class="panel-body">

                <div class="table-responsive">
                    <div>

                        <p v-if="!items.length" class="alert alert-warning"><?php echo trans('messages.activity_log.not_found') ?></p>

                        <div v-else class="table-responsive">
                            <pagination :pagination="pagination" :callback="loadData" :options="paginationOptions"></pagination>

                            <table  class="table table-striped guest-list">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th><?php echo trans('tenant.title') ?></th>
                                    <th><?php echo trans('user.title') ?></th>
                                    <th><?php echo trans('messages.ip') ?></th>
                                    <th><?php echo trans('messages.message') ?></th>
                                    <th><?php echo trans('messages.time') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(item, index) in items">
                                    <th>{{ pagination.from + index }}</th>
                                    <td>{{ item.tenant.name }}</td>
                                    <td>{{ item.user.name }}</td>
                                    <td>{{ item.ip_address }}</td>
                                    <td>{{ item.description }}</td>
                                    <td>{{ item.created_at }}</td>
                                </tr>
                                </tbody>
                            </table>

                            <pagination :pagination="pagination" :callback="loadData" :options="paginationOptions"></pagination>

                        </div>
                    </div>

                </div><!-- /table-responsive -->

            </div><!-- /panel-body -->

    </div><!-- /panel -->

</activity-log>
