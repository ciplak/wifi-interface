<?php
/**
 * @var \App\Models\Guest[]|\Illuminate\Support\MessageBag $models
 * @var array $nationalityOptions
 * @var array $genderOptions
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('report.guests') ?></h4>

            <div class="panel-control">

                <div class="btn-group" data-tooltip="true" title="<?php echo trans('messages.export.title') ?>">
                    <button type="button" class="btn btn-default btn-export" data-format="xlsx"><i class="fa fa-share"></i></button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" class="btn-export" data-format="csv"><?php echo trans('messages.export.csv') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="xlsx"><?php echo trans('messages.export.xlsx') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="pdf"><?php echo trans('messages.export.pdf') ?></a></li>
                    </ul>
                </div>
            </div><!-- /panel-control -->
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">
        <?php echo view('admin.report.guest.filters')->render() ?>

        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('guest.not_found') ?></p>

            <?php else: ?>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?php echo trans('guest.columns.name') ?></th>
                        <th><?php echo trans('guest.columns.email') ?></th>
                        <th><?php echo trans('guest.columns.phone') ?></th>
                        <th><?php echo trans('guest.columns.nationality') ?></th>
                        <th><?php echo trans('guest.columns.passport_no') ?></th>
                        <th><?php echo trans('guest.columns.birthday') ?></th>
                        <th><?php echo trans('guest.columns.gender') ?></th>
                        <th><?php echo trans('guest.columns.last_visit') ?></th>
                        <th><?php echo trans('guest.columns.total_visits') ?></th>
                    </tr>
                    </thead>
                    <tbody id="user-table-body">

                    <?php
                    $canManageGuests = \Entrust::can('guests.manage');
                    foreach ($models as $model): ?>
                        <tr>
                            <td><?php echo $canManageGuests ? \HTML::linkRoute('admin.guest.details', e($model->display_name), [$model]) : e($model->display_name) ?></td>
                            <td><?php echo e($model->email) ?></td>
                            <td><?php echo e($model->phone); ?></td>
                            <td><?php echo $model->nationality_name ?></td>
                            <td><?php echo e($model->passport_no) ?></td>
                            <td><?php echo $model->birthday_formatted ?></td>
                            <td><?php echo $model->gender_name ?></td>
                            <td><?php echo $model->last_visit_time_formatted ?></td>
                            <td><?php echo $model->total_visits ?></td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>

                <?php pagination($models) ?>

            <?php endif ?>

        </div>
        <!-- /table-responsive -->

    </div>
    <!-- /panel-body -->

</div><!-- /panel -->

<script>
    $(function() {
        var filterForm = $('form.filter-form');

        $('.btn-export').click(function() {

            var inputs = [];
            $(':input:not(button)', filterForm).each(function (i, el) {
                var $el = $(el);
                inputs.push('<input type="hidden" name="' + $el.attr('name') + '" value="' + $el.val() + '">');
            });

            var form = $('<form action="<?php echo route('admin.report.guestExport') ?>" method="POST">' +
                '<input type="hidden" name="format" value="' + $(this).data('format') + '">' +
                '<?php echo csrf_field() ?>' + inputs.join('') + '</form>');

            form.appendTo($('body'));
            form.submit().remove();

            return false;
        });
    });
</script>