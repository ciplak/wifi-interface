<?php
/**
 * @var array $locations
 * @var array $nationalityOptions
 * @var array $genderOptions
 * @var array $connectionStatuses
 */
echo Form::open(['class' => 'form-horizontal margin-bottom filter-form', 'method' => 'GET']) ?>

<div class="rowe">
    <div class="form-group">
        <div class="col-md-2">
            <?php echo Form::label('name', trans('guest.columns.name')) ?>
            <?php echo Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.name')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('email', trans('guest.columns.email')) ?>
            <?php echo Form::text('email', request('email'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.email')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('phone', trans('guest.columns.phone')) ?>
            <?php echo Form::text('phone', request('phone'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.phone')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('nationality', trans('guest.columns.nationality')) ?>
            <?php echo Form::select('nationality', $nationalityOptions, request('nationality'), ['class' => 'form-control', 'placeholder' => trans('guest.nationality.all')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('passport_no', trans('guest.columns.passport_no')) ?>
            <?php echo Form::text('passport_no', request('passport_no'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.passport_no')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('gender', trans('guest.columns.gender')) ?>
            <?php echo Form::select('gender', $genderOptions, request('gender'), ['class' => 'form-control', 'placeholder' => trans('guest.gender.all')]) ?>
        </div>

    </div>
</div>

<div class="rowe">
    <div class="form-group">

        <div class="col-md-2">
            <?php echo Form::label('location', trans('guest.columns.location')) ?>
            <?php echo Form::select('location', $locations, request('location'), ['class' => 'form-control select', 'placeholder' => trans('venue.all')]) ?>
        </div>

        <!--<div class="col-md-2">-->
        <!--    --><?php //echo Form::select('hotspot', [null => trans('device.all')] + \App\Models\Device::pluck('name', 'id')->toArray(), request('hotspot'), ['class' => 'form-control select', 'placeholder' => trans('guest.columns.phone')]) ?>
        <!--</div>-->

        <div class="col-md-2">
            <?php echo Form::label('connection_status', trans('guest.columns.connection_status')) ?>
            <?php echo Form::select('connection_status', $connectionStatuses, request('connection_status'), ['class' => 'form-control', 'placeholder' => trans('guest.connection_status.all')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('date', trans('messages.date.title')) ?>
            <?php echo Form::select('date', date_options(), request('date'), ['class' => 'form-control select date-range-select']) ?>
        </div>

        <div class="col-md-4">
            <label for="">&nbsp;</label>
            <div class="input-daterange input-group <?php if (request('date') != \App\Date::RANGE_INTERVAL) echo 'hidden' ?>" id="date-range-picker">
                <?php echo Form::text('start_date', request('start_date', date('Y-m-d')), ['class' => 'form-control']) ?>
                <span class="input-group-addon">to</span>
                <?php echo Form::text('end_date', request('end_date', date('Y-m-d')), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-md-2">
            <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>
        </div>
    </div>

</div>
<?php echo Form::close() ?>