<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('report.trend_analysis.title') ?></h4>

        <div class="panel-control">
            <a href="#" class="btn btn-default  btn-export-chart" data-tooltip="true" data-url="<?php echo route('admin.report.export') ?>" data-chart-container="online-users" title="<?php echo trans('messages.export.title') ?>"><i class="fa fa-share"></i></a>
        </div>

    </div>

    <div class="panel-body">
        <?php echo view('admin.report.trend_analysis.filters')->render() ?>
        <?php echo view('admin.report.trend_analysis.chart')->render() ?>
    </div>

</div>