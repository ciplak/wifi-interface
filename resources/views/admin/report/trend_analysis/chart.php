<?php
/**
 * @var string $dates
 * @var string $data
 */
?>
<div id="online-users"></div>

<script>
    $(function () {

        $('#online-users').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '<?php echo trans('report.trend_analysis.title') ?>'
            },
            xAxis: {
                categories: ["<?php echo $dates ?>"],
                title: {
                    text: '<?php echo trans('messages.date.title') ?>'
                }
            },
            yAxis: {
                title: {
                    text: '<?php echo trans('messages.count') ?>'
                }

            },
            tooltip: {
//                pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}',
//                useHTML: true
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: <?php echo $data ?>
        });
    });
</script>