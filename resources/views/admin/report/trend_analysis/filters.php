<?php
echo Form::open(['class' => 'form-inline margin-bottom filter-form', 'method' => 'GET']) ?>

    <?php if (\Entrust::can('venues.manage-global')): ?>
        <div class="form-group">
            <?php echo Form::select('location', $locations, request('location'), ['class' => 'form-control select', 'placeholder' => trans('venue.all')]) ?>
        </div>
    <?php endif ?>

        <!--<div class="form-group">-->
<?php //echo Form::select('hotspot', [null => trans('device.all')] + \App\Models\Device::pluck('name', 'id')->toArray(), request('hotspot'), ['class' => 'form-control select']) ?>
        <!--</div>-->

    <div class="form-group">
        <?php echo Form::select('date', [
            \App\Date::RANGE_LAST_WEEK => 'Last week',
            \App\Date::RANGE_LAST_MONTH => 'Last Month',
            \App\Date::RANGE_INTERVAL => 'Interval'
        ], request('date'),
            ['class' => 'form-control select date-range-select']) ?>
    </div>

    <div class="form-group">
        <div class="input-daterange input-group <?php if (request('date') != \App\Date::RANGE_INTERVAL) echo 'hidden' ?>" id="date-range-picker">
            <?php echo Form::text('start_date', request('start_date', date('Y-m-d')), ['class' => 'form-control']) ?>
            <span class="input-group-addon">to</span>
            <?php echo Form::text('end_date', request('end_date', date('Y-m-d')), ['class' => 'form-control']) ?>
        </div>
    </div>

    <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
    <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>

<?php echo Form::close() ?>