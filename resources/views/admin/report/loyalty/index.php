<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('report.loyalty.title') ?></h4>

        <div class="panel-control">
            <a href="#" class="btn btn-default  btn-export-chart" data-tooltip="true"
               data-url="<?php echo route('admin.report.export') ?>" data-chart-container="visitors-loyalty"
               title="<?php echo trans('messages.export.title') ?>"><i class="fa fa-share"></i></a>
        </div>

    </div>

    <div class="panel-body">
        <?php echo view('admin.report.loyalty.filters')->render() ?>
        <?php echo view('admin.report.loyalty.chart')->render() ?>
    </div>
</div>

