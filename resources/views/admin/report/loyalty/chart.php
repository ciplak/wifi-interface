<div id="visitors-loyalty"></div>
<script>

    $(function () {
        $('#visitors-loyalty').highcharts({
            chart: {
                type: 'pie'
            },

            title: {
                text: '<?php echo trans('report.loyalty.title') ?>'
            },

            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },

            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    }
                }
            },

            series: [{
                innerSize: '40%',
                data:  <?php /** @var array $data*/ echo json_encode($data) ?>,
                showInLegend: true,
                dataLabels: {
                    enabled: true,
                    style: {
                        textShadow: false
                    },
                    formatter: function() {
                        return Math.round(this.percentage * 100) / 100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }]

        });
    });
</script>
