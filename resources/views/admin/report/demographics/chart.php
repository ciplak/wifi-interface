<div id="demographics"></div>

<script>
$(function () {
    $('#demographics').highcharts({
        chart: {
            type: 'column'
        },

        title: {
            text: '<?php echo trans('report.demographics') ?>'
        },

        xAxis: {
            title: {
                text: '<?php echo trans('messages.age') ?>'
            },
            categories: <?php echo json_encode($ageRanges) ?>
        },

        yAxis: {
            min: 0,
            title: {
                text: '<?php echo trans('messages.percentage') ?>'
            }
        },

        plotOptions: {
            column: {
                stacking: 'percent'
            },

            series: {
                dataLabels: {
                    enabled: true,
                    style: {
                        textShadow: false
                    },
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },

        series: <?php echo json_encode($data) ?>

    });
});
</script>