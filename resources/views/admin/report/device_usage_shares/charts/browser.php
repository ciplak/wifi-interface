<div id="device-browser-usage-share" style="height: 400px;"></div>

<script>

    $(function () {
        var data = <?php echo json_encode($browserData) ?>;

        $('#device-browser-usage-share').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: '<?php echo trans('report.device_browsers') ?>'
            },

            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },

            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false

                    }
                },
                series: {
                    dataLabels: {
                        style: {
                            textShadow: false
                        },
                        enabled: true,
                        formatter: function () {
                            return Math.round(this.percentage * 100) / 100 + ' %';
                        },
                        distance: -30,
                        color: 'white'
                    }

                }
            },
            legend: {
                enabled: true
            },

            series: [{
                type: 'pie',
                data: data
            }]


        });
    });
</script>