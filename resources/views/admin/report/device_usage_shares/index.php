<?php echo view('admin.report.device_usage_shares.filters')->render(); ?>

<div class="row">
    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo trans('report.device_usage_shares') ?></h4>

                <div class="panel-control">
                    <a href="#" class="btn btn-default btn-export-chart" data-tooltip="true" data-url="<?php echo route('admin.report.export') ?>" data-chart-container="device-usage-share" title="<?php echo trans('messages.export.title') ?>"><i class="fa fa-share"></i></a>
                </div>
            </div>

            <div class="panel-body">
                <?php echo view('admin.report.device_usage_shares.charts.usage_shares'); ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo trans('report.device_platforms') ?></h4>

                <div class="panel-control">
                    <a href="#" class="btn btn-default btn-export-chart" data-tooltip="true" data-url="<?php echo route('admin.report.export') ?>" data-chart-container="device-platform-usage-share" title="<?php echo trans('messages.export.title') ?>"><i class="fa fa-share"></i></a>
                </div>
            </div>

            <div class="panel-body">
                <?php echo view('admin.report.device_usage_shares.charts.platform')->render(); ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo trans('report.device_browsers') ?></h4>

                <div class="panel-control">
                    <a href="#" class="btn btn-default btn-export-chart" data-tooltip="true" data-url="<?php echo route('admin.report.export') ?>" data-chart-container="device-browser-usage-share" title="<?php echo trans('messages.export.title') ?>"><i class="fa fa-share"></i></a>
                </div>
            </div>

            <div class="panel-body">
                <?php echo view('admin.report.device_usage_shares.charts.browser')->render(); ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo trans('report.device_vendors') ?></h4>

                <div class="panel-control">
                    <a href="#" class="btn btn-default btn-export-chart" data-tooltip="true" data-url="<?php echo route('admin.report.export') ?>" data-chart-container="device-vendors-share" title="<?php echo trans('messages.export.title') ?>"><i class="fa fa-share"></i></a>
                </div>
            </div>

            <div class="panel-body">
                <?php echo view('admin.report.device_usage_shares.charts.vendors')->render(); ?>
            </div>
        </div>
    </div>

</div>
