<div class="panel panel-default">
    <div class="panel-body">
        <div id="sign-up-method"></div>
    </div>
</div>

<script>
$(function () {

    //Highcharts.setOptions({
    //    colors: ['#3b5998', '#ED561B', '#DDDF00', '#4099ff', '#64E572']
    //});

    $('#sign-up-method').highcharts({
        chart: {
            type: 'pie'
        },

        title: {
            text: 'Sign-up Methods'
        },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: true,
                dataLabels: {
                    enabled: false

                }
            },

            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },

        series: [{
            name: 'Sign-up Method',
            data: [
                ['Facebook',        57],
                ['Click-through',   27],
                ['SMS',             10],
                ['Twitter',         4],
                ['Others',          2]
            ]
        }]

    });
});
</script>