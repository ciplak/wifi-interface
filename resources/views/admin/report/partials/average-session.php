<div class="panel panel-default">
    <div class="panel-body">
        <div id="average-session"></div>
    </div>
</div>
<script>
var data = [<?php
 foreach ([84, 78, 123, 116, 133, 138, 65] as $data):
    echo $data * 1000, ',';
 endforeach;
?>
];
$(function () {
    $('#average-session').highcharts({
        title: {
            text: 'Average Sessions',
        },
        xAxis: {
            categories: ['Day 1', 'Day 2', 'Day 3', 'Day 4', 'Day 5', 'Day 6', 'Day 7']
        },

        yAxis: {
            title: {
                text: 'Duration',
                margin: 10
            },

            type: 'datetime',

            dateTimeLabelFormats: {
                second: '%M:%S',
                minute: '%M:%S'
            }
        },

        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Highcharts.dateFormat('%M:%S', this.y);
                    }
                }
            }
        },
        series: [{
            showInLegend: false,
            data: data
        }]
    });
});
</script>