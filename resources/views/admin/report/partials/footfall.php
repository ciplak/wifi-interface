<div class="panel panel-default">
    <div class="panel-body">
        <div id="footfall"></div>
    </div>
</div>

<script>
$(function () {
var dataSum = 1240000;

    $('#footfall').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Footfall'
        },
        subtitle: {
            text: 'Number of WiFi devices detected based on their MAC address'
        },
        xAxis: {
            categories: ['0AM - 7AM', '7AM - 11AM', '11AM - 2PM', '2PM - 5PM', '5PM - 10PM', '10PM - 0AM'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Quantity',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },

        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            },

            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        var percent = (this.y / dataSum * 100);
                        return this.y + " (" + Math.round(percent) + "%)";
                    },
                    distance: -30,
                    //color:'white'
                }
            }
        },

        series: [{
            showInLegend: false,
            data: [74400, 136400, 232200, 285200, 458800, 62000]
        }]

    });
});
</script>