<div class="panel panel-default">
    <div class="panel-body">
        <div id="capture-rate"></div>
    </div>
</div>

<script>
$(function () {

    $('#capture-rate').highcharts({
        chart: {
            type: 'pie'
        },

        title: {
            text: 'Capture Rate'
        },

        subtitle: {
            text: 'Weekly Values'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: true,
                dataLabels: {
                    enabled: false

                }
            },

            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },

        series: [{
            data: [
                ['Passing',  192000],
                ['Visitors', 128000],
                ['Connect',  64000],
                ['Users',    16000],
            ]
        }]

    });
});
</script>