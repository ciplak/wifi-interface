<div class="panel panel-default">
    <div class="panel-body">
        <div id="conversion-rate"></div>
    </div>
</div>

<script>
$(function () {

    $('#conversion-rate').highcharts({
        chart: {
            type: 'pie'
        },

        title: {
            text: 'Conversion Rate'
        },

        subtitle: {
            text: 'Weekly Values'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: true,
                dataLabels: {
                    enabled: false

                }
            },

            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },

        legend: {
            labelFormatter: function() {
              return this.name + " (" + this.y + ")";
            }
        },

        series: [{
            name: 'Sign-up Method',
            data: [
                ['Impression',  18600],
                ['Connected', 12100],
            ]
        }]

    });
});
</script>