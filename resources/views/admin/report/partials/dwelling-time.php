<div class="panel panel-default">
    <div class="panel-body">
        <div id="dwelling-time"></div>
    </div>
</div>

<script>
$(function () {

    $('#dwelling-time').highcharts({
        chart: {
            type: 'pie'
        },

        title: {
            text: 'Dwelling Time'
        },

        subtitle: {
            text: 'Weekly Values'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: true,
                dataLabels: {
                    enabled: false

                }
            },

            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return this.y;
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            labelFormat: '{name} ({percentage:.1f}%)',
        },

        series: [{
            name: 'Sign-up Method',
            data: [
                ['&lt;10 min.',  36000],
                ['10-30.min', 84000],
                ['30-60 min.',  128000],
                ['&gt;60 min',    152000],
            ]
        }]

    });
});
</script>