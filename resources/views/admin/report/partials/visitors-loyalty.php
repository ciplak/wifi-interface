<div class="panel panel-default">
    <div class="panel-body">
        <div id="visitors-loyalty"></div>
    </div>
</div>

<script>
$(function () {

    $('#visitors-loyalty').highcharts({
        chart: {
            type: 'pie'
        },

        title: {
            text: 'Visitors Loyalty'
        },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                }
            }
        },

        series: [{
            innerSize: '40%',
            data: [
                ['One visit',  23],
                ['2-5 visits', 47],
                ['5-10 visits',21],
                ['10+ visits',  9]
            ],
            showInLegend: true,
            dataLabels: {
                enabled: true,
                style: {
                    textShadow: false
                },
                formatter: function() {
                    return Math.round(this.percentage*100)/100 + ' %';
                },
                distance: -30,
                color:'white'
            }
        }]

    });
});
</script>