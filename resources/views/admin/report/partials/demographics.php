<div class="panel panel-default">
    <div class="panel-body">
        <div id="demographics"></div>
    </div>
</div>
<script>
$(function () {
    $('#demographics').highcharts({
        chart: {
            type: 'column'
        },

        title: {
            text: 'Demographics'
        },

        xAxis: {
            title: {
                text: 'Age'
            },
            categories: ['&lt;20', '21-25', '36-40', '41-55', '&gt;55']
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            max: 100,
            title: {
                text: 'Percentage'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            },

            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },

        series: [{
            name: 'Female',
            color: '#e03078',
            data: [45, 52, 36, 41, 29]
        }, {
            name: 'Male',
            color: '#3278fe',
            data: [55, 48, 64, 59, 71]
        }]
    });
});
</script>