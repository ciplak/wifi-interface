<div class="row">
    <div class="col-md-6">
        <?php echo view('admin.report.partials.average-session') ?>
    </div>

    <div class="col-md-6">
        <?php echo view('admin.report.partials.visitors-loyalty') ?>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <?php echo view('admin.report.partials.demographics') ?>
    </div>

    <div class="col-md-6">
        <?php echo view('admin.report.partials.sign-up-method') ?>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <?php echo view('admin.report.partials.capture-rate') ?>
    </div>

    <div class="col-md-6">
        <?php echo view('admin.report.partials.dwelling-time') ?>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <?php echo view('admin.report.partials.footfall') ?>
    </div>

    <div class="col-md-6">
        <?php echo view('admin.report.partials.conversion-rate') ?>
    </div>

</div>