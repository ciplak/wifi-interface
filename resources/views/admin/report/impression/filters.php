<?php
/**
 * @var array $locations
 * @var array $nationalityOptions
 * @var array $genderOptions
 * @var array $connectionStatuses
 */
echo Form::open(['class' => 'form-horizontal margin-bottom filter-form', 'method' => 'GET']) ?>

    <div class="rowe">
        <div class="form-group">
            <div class="col-md-2">
                <?php echo Form::label('name', trans('impression.filters.name')) ?>
                <?php echo Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.name')]) ?>
            </div>

            <div class="col-md-2">
                <?php echo Form::label('ip', trans('impression.filters.ip')) ?>
                <?php echo Form::text('ip', request('ip'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.ip')]) ?>
            </div>

            <div class="col-md-2">
                <?php echo Form::label('mac', trans('impression.filters.mac')) ?>
                <?php echo Form::text('mac', request('mac'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.mac')]) ?>
            </div>

            <div class="col-md-2">
                <?php echo Form::label('date', trans('messages.date.title')) ?>
                <?php echo Form::select('date', date_options(), request('date'), ['class' => 'form-control select date-range-select']) ?>
            </div>

            <div class="col-md-4">
                <label for="">&nbsp;</label>
                <div class="input-daterange input-group <?php if (request('date') != \App\Date::RANGE_INTERVAL) echo 'hidden' ?>"
                     id="date-range-picker">
                    <?php echo Form::text('start_date', request('start_date', date('Y-m-d')), ['class' => 'form-control']) ?>
                    <span class="input-group-addon">to</span>
                    <?php echo Form::text('end_date', request('end_date', date('Y-m-d')), ['class' => 'form-control']) ?>
                </div>
            </div>

            <div class="col-md-2">
                <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
                <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>
            </div>
        </div>

    </div>

    <div class="rowe">
        <div class="form-group">
            <div class="col-md-2">
                <?php echo Form::label('device_type_id', trans('impression.filters.device_type_id')) ?>
                <?php echo Form::select('device_type_id', $deviceTypes, request('device_type_id'), ['class' => 'form-control']) ?>
            </div>

            <div class="col-md-2">
                <?php echo Form::label('description', trans('impression.filters.description')) ?>
                <?php echo Form::text('description', request('description'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.description')]) ?>
            </div>

            <div class="col-md-2">
                <?php echo Form::label('platform', trans('impression.filters.platform')) ?>
                <?php echo Form::text('platform', request('platform'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.platform')]) ?>
            </div>

            <div class="col-md-2">
                <?php echo Form::label('browser', trans('impression.filters.browser')) ?>
                <?php echo Form::text('browser', request('browser'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.browser')]) ?>
            </div>
        </div>
    </div>
<?php echo Form::close() ?>