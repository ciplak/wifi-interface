<?php
/**
 * @var \App\Models\GuestActivity[]|\Illuminate\Support\MessageBag $models
 * @var array                                                      $nationalityOptions
 * @var array                                                      $genderOptions
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('report.impressions') ?></h4>

            <div class="panel-control">

                <div class="btn-group" data-tooltip="true" title="<?php echo trans('messages.export.title') ?>">
                    <button type="button" class="btn btn-default btn-export" data-format="xlsx"><i
                                class="fa fa-share"></i></button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" class="btn-export"
                               data-format="csv"><?php echo trans('messages.export.csv') ?></a></li>
                        <li><a href="#" class="btn-export"
                               data-format="xlsx"><?php echo trans('messages.export.xlsx') ?></a></li>
                        <li><a href="#" class="btn-export"
                               data-format="pdf"><?php echo trans('messages.export.pdf') ?></a></li>
                    </ul>
                </div>
            </div><!-- /panel-control -->
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">
        <?php echo view('admin.report.impression.filters')->render() ?>

        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('impression.not_found') ?></p>

            <?php else: ?>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?php echo trans('impression.columns.name') ?></th>
                        <th><?php echo trans('impression.columns.username') ?></th>
                        <th><?php echo trans('impression.columns.device_type_id') ?></th>
                        <th><?php echo trans('impression.columns.description') ?></th>
                        <th><?php echo trans('impression.columns.platform') ?></th>
                        <th><?php echo trans('impression.columns.browser') ?></th>
                        <th><?php echo trans('impression.columns.ip') ?></th>
                        <th><?php echo trans('impression.columns.mac') ?></th>
                        <th><?php echo trans('impression.columns.created_at') ?></th>
                    </tr>
                    </thead>
                    <tbody id="user-table-body">

                    <?php
                    foreach ($models as $model): ?>
                        <tr>
                            <td><?php echo $model->name ?></td>
                            <td><?php echo $model->username; ?></td>
                            <td><?php echo $model->guest_device_type_name ?></td>
                            <td><?php echo $model->description ?></td>
                            <td><?php echo $model->platform ?></td>
                            <td><?php echo $model->browser ?></td>
                            <td><?php echo $model->ip ?></td>
                            <td><?php echo $model->mac ?></td>
                            <td><?php echo $model->created_at_formatted ?></td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>

                <?php pagination($models) ?>

            <?php endif ?>

        </div>
        <!-- /table-responsive -->

    </div>
    <!-- /panel-body -->

</div><!-- /panel -->

<script>
    $(function () {
        var filterForm = $('form.filter-form');

        $('.btn-export').click(function () {

            var inputs = [];
            $(':input:not(button)', filterForm).each(function (i, el) {
                var $el = $(el);
                inputs.push('<input type="hidden" name="' + $el.attr('name') + '" value="' + $el.val() + '">');
            });

            var form = $('<form action="<?php echo route('admin.report.impression.export') ?>" method="POST">' +
                '<input type="hidden" name="format" value="' + $(this).data('format') + '">' +
                '<?php echo csrf_field() ?>' + inputs.join('') + '</form>');

            form.appendTo($('body'));
            form.submit().remove();

            return false;
        });
    });
</script>