<?php
/**
 * @var array $locations
 * @var array $devices
 */
echo Form::open(['class' => 'form-inline margin-bottom filter-form', 'method' => 'GET']) ?>

    <?php if (\Entrust::can('venues.manage-global')): ?>
    <div class="form-group">
        <?php echo Form::select('location', $locations, request('location'), ['class' => 'form-control select', 'placeholder' => trans('venue.all')]) ?>
    </div>
    <?php endif ?>

    <div class="form-group">
        <?php echo Form::select('type', [trans('messages.date.weekly'), trans('messages.date.monthly')], request('type'), ['class' => 'form-control']) ?>
    </div>


    <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
    <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>

<?php echo Form::close() ?>