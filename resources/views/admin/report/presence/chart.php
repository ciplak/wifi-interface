<div id="presence"></div>

<script>
var data = [<?php echo $values ?>];

$(function () {
    $('#presence').highcharts({
        title: {
            text: '<?php echo trans('report.presence.average_sessions') ?>',
        },
        tooltip: {
            formatter: function () {
                return '<strong>' + this.x + '</strong><br>' +
                    Highcharts.dateFormat('%H:%M:%S', this.y);
            }
        },
        xAxis: {
            categories: <?php echo json_encode($xAxis)?>
        },

        yAxis: {
            title: {
                text: '<?php echo trans('report.presence.y_axis.title') ?>',
                margin: 10
            },

            type: 'datetime',

            dateTimeLabelFormats: {
                second: '%H:%M:%S',
                minute: '%H:%M:%S',
                hour: '%H:%M:%S',
                day: '%H:%M:%S'
            }
        },

        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M:%S', this.y);
                    }
                }
            }
        },
        series: [{
            showInLegend: false,
            data: data
        }]
    });
});
</script>
