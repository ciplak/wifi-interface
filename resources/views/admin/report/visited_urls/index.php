<?php
/**
 * @var \App\Models\VisitedUrl[]|\Illuminate\Support\MessageBag $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('visited_url.page_title') ?></h4>

            <div class="panel-control">

                <div class="btn-group" data-tooltip="true" title="<?php echo trans('messages.export.title') ?>">
                    <button type="button" class="btn btn-default btn-export" data-format="xlsx"><i class="fa fa-share"></i></button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" class="btn-export" data-format="csv"><?php echo trans('messages.export.csv') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="xlsx"><?php echo trans('messages.export.xlsx') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="pdf"><?php echo trans('messages.export.pdf') ?></a></li>
                    </ul>
                </div>
            </div><!-- /panel-control -->
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">
        <?php echo view('admin.report.visited_urls.filters')->render() ?>

        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('visited_url.not_found') ?></p>

            <?php else: ?>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?php echo trans('visited_url.columns.ap_mac') ?></th>
                        <th><?php echo trans('visited_url.columns.client_mac') ?></th>
                        <th><?php echo trans('visited_url.columns.source_ip') ?></th>
                        <th><?php echo trans('visited_url.columns.destination_ip') ?></th>
                        <th><?php echo trans('impression.columns.device_type_id') ?></th>
                        <th><?php echo trans('impression.columns.description') ?></th>
                        <th><?php echo trans('impression.columns.platform') ?></th>
                        <th><?php echo trans('impression.columns.browser') ?></th>
                        <th><?php echo trans('visited_url.columns.url') ?></th>
                        <th class="col-datetime"><?php echo trans('visited_url.columns.visited_at') ?></th>
                    </tr>
                    </thead>
                    <tbody id="user-table-body">

                    <?php
                    foreach ($models as $model): ?>
                        <tr>
                            <td><?php echo \HTML::linkRoute('admin.device.index', $model->ap_mac, ['mac' => $model->ap_mac]) ?></td>
                            <td><?php echo \HTML::linkRoute('admin.guest.index', $model->client_mac, ['mac' => $model->client_mac]) ?></td>
                            <td><?php echo $model->source_with_port ?></td>
                            <td><?php echo $model->destination_with_port ?></td>
                            <td><?php echo $model->guest_device_type_name ?></td>
                            <td><?php echo $model->description ?></td>
                            <td><?php echo $model->platform ?></td>
                            <td><?php echo $model->browser ?></td>
                            <td><?php echo \HTML::link($model->url, str_limit($model->url)) ?></td>
                            <td><?php echo $model->visited_at_formatted ?></td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>

                <?php pagination($models) ?>

            <?php endif ?>

        </div>
        <!-- /table-responsive -->

    </div>
    <!-- /panel-body -->

</div><!-- /panel -->

<script>
    $(function() {
        var filterForm = $('form.filter-form');

        $('.btn-export').click(function() {

            var inputs = [];
            $(':input:not(button)', filterForm).each(function (i, el) {
                var $el = $(el);
                inputs.push('<input type="hidden" name="' + $el.attr('name') + '" value="' + $el.val() + '">');
            });

            var form = $('<form action="<?php echo route('admin.report.visited_urls.export') ?>" method="POST">' +
                '<input type="hidden" name="format" value="' + $(this).data('format') + '">' +
                '<?php echo csrf_field() ?>' + inputs.join('') + '</form>');

            form.appendTo($('body'));
            form.submit().remove();

            return false;
        });
    });
</script>