<?php
/**
 * @var array $deviceTypes
 */
echo Form::open(['class' => 'form-horizontal margin-bottom filter-form', 'method' => 'GET']) ?>

<div class="rowe">
    <div class="form-group">
        <div class="col-md-2">
            <?php echo Form::label('ap_mac', trans('visited_url.columns.ap_mac')) ?>
            <?php echo Form::text('ap_mac', request('ap_mac'), ['class' => 'form-control', 'placeholder' => trans('visited_url.columns.ap_mac')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('client_mac', trans('visited_url.columns.client_mac')) ?>
            <?php echo Form::text('client_mac', request('client_mac'), ['class' => 'form-control', 'placeholder' => trans('visited_url.columns.client_mac')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('source_ip', trans('visited_url.columns.source_ip')) ?>
            <?php echo Form::text('source_ip', request('source_ip'), ['class' => 'form-control', 'placeholder' => trans('visited_url.columns.source_ip')]) ?>
        </div>


        <div class="col-md-2">
            <?php echo Form::label('destination_ip', trans('visited_url.columns.destination_ip')) ?>
            <?php echo Form::text('destination_ip', request('destination_ip'), ['class' => 'form-control', 'placeholder' => trans('visited_url.columns.destination_ip')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('device_type_id', trans('impression.filters.device_type_id')) ?>
            <?php echo Form::select('device_type_id', $deviceTypes, request('device_type_id'), ['class' => 'form-control']) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('description', trans('impression.filters.description')) ?>
            <?php echo Form::text('description', request('description'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.description')]) ?>
        </div>

    </div>
</div>

<div class="rowe">
    <div class="form-group">

        <div class="col-md-2">
            <?php echo Form::label('platform', trans('impression.filters.platform')) ?>
            <?php echo Form::text('platform', request('platform'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.platform')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('browser', trans('impression.filters.browser')) ?>
            <?php echo Form::text('browser', request('browser'), ['class' => 'form-control', 'placeholder' => trans('impression.columns.browser')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('url', trans('visited_url.columns.url')) ?>
            <?php echo Form::text('url', request('url'), ['class' => 'form-control', 'placeholder' => trans('visited_url.columns.url')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('date', trans('messages.date.title')) ?>
            <?php echo Form::select('date', date_options(), request('date'), ['class' => 'form-control select date-range-select']) ?>
        </div>

        <div class="col-md-4">
            <label>&nbsp;</label>
            <div class="input-daterange input-group <?php if (request('date') != \App\Date::RANGE_INTERVAL) echo 'hidden' ?>"
                 id="date-range-picker">
                <?php echo Form::text('start_date', request('start_date', date('Y-m-d')), ['class' => 'form-control']) ?>
                <span class="input-group-addon">to</span>
                <?php echo Form::text('end_date', request('end_date', date('Y-m-d')), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-md-2">
            <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>
        </div>

    </div>
</div>

<?php echo Form::close() ?>