<?php
/**
 * @var array $operators
 * @var array $connectionStatuses
 */
echo Form::open(['class' => 'form-horizontal margin-bottom filter-form', 'method' => 'GET']) ?>
<div class="rowe">
    <div class="form-group">
        <div class="col-md-2">
            <?php echo Form::label('uploaded_data', trans('guest_session.filters.uploaded_data')) ?>
            <div class="input-group input-group-with-select">
                <?php echo Form::text('uploaded_data', request('uploaded_data'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.uploaded_data')]) ?>
                <?php echo Form::select('upload_operator', $operators, request('upload_operator'), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('downloaded_data', trans('guest_session.filters.downloaded_data')) ?>
            <div class="input-group input-group-with-select">
                <?php echo Form::text('downloaded_data', request('downloaded_data'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.downloaded_data')]) ?>
                <?php echo Form::select('download_operator', $operators, request('download_operator'), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('name', trans('guest_session.filters.duration')) ?>
            <div class="input-group input-group-with-select">
                <?php echo Form::number('duration', request('duration'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.duration'), 'min' => 0]) ?>
                <?php echo Form::select('duration_operator', $durationOperators, request('duration_operator'), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('date', trans('guest_session.filters.date')) ?>
            <?php echo Form::select('date', date_options(), request('date'), ['class' => 'form-control select date-range-select']) ?>
        </div>

        <div class="col-md-4">
            <label for="">&nbsp;</label>
            <div class="input-daterange input-group <?php if (request('date') != \App\Date::RANGE_INTERVAL) echo 'hidden' ?>" id="date-range-picker">
                <?php echo Form::text('start_date', request('start_date', date('Y-m-d')), ['class' => 'form-control']) ?>
                <span class="input-group-addon">to</span>
                <?php echo Form::text('end_date', request('end_date', date('Y-m-d')), ['class' => 'form-control']) ?>
            </div>
        </div>

    </div>

</div>

<div class="rowe">
    <div class="form-group">
        <div class="col-md-2">
            <?php echo Form::label('name', trans('guest_session.filters.name')) ?>
            <?php echo Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.name')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('guest_device_mac', trans('guest_session.filters.mac')) ?>
            <?php echo Form::text('guest_device_mac', request('guest_device_mac'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.mac')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('guest_device_ip', trans('guest_session.filters.ip')) ?>
            <?php echo Form::text('guest_device_ip', request('guest_device_ip'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.ip')]) ?>
        </div>
        
        <div class="col-md-2">
            <?php echo Form::label('access_code', trans('guest_session.filters.access_code')) ?>
            <?php echo Form::text('access_code', request('access_code'), ['class' => 'form-control', 'placeholder' => trans('guest.columns.access_code')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('connection_status', trans('guest_session.filters.connection_status')) ?>
            <?php echo Form::select('connection_status', $connectionStatuses, request('connection_status'), ['class' => 'form-control', 'placeholder' => trans('guest.connection_status.all')]) ?>
        </div>

        <div class="col-md-2">
            <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>
        </div>

    </div>
</div>

<?php echo Form::close() ?>