<?php
/**
 * @var \App\Models\WifiSession[]|\Illuminate\Support\MessageBag $models
 * @var array $nationalityOptions
 * @var array $genderOptions
 */
?>
<div class="panel panel-default panel-actions">
    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('report.guest_sessions') ?></h4>

            <div class="panel-control">

                <div class="btn-group" data-tooltip="true" title="<?php echo trans('messages.export.title') ?>">
                    <button type="button" class="btn btn-default btn-export" data-format="xlsx"><i class="fa fa-share"></i></button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" class="btn-export" data-format="csv"><?php echo trans('messages.export.csv') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="xlsx"><?php echo trans('messages.export.xlsx') ?></a></li>
                        <li><a href="#" class="btn-export" data-format="pdf"><?php echo trans('messages.export.pdf') ?></a></li>
                    </ul>
                </div>
            </div><!-- /panel-control -->
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">
        <?php echo view('admin.report.guest_sessions.filters')->render() ?>

        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('guest.guest_session_not_found') ?></p>

            <?php else: ?>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?php echo trans('guest.columns.name') ?></th>
                        <th><?php echo trans('guest.columns.username') ?></th>
                        <th><?php echo trans('guest.columns.mac') ?></th>
                        <th><?php echo trans('guest.columns.ip') ?></th>
                        <th><?php echo trans('guest.columns.access_code') ?></th>
                        <th><?php echo trans('guest.columns.duration') ?></th>
                        <th><?php echo trans('guest.columns.download') ?></th>
                        <th><?php echo trans('guest.columns.upload') ?></th>
                        <th><?php echo trans('guest.columns.connection_status') ?></th>
                        <th><?php echo trans('guest.columns.time') ?></th>
                    </tr>
                    </thead>
                    <tbody id="user-table-body">

                    <?php
                    $canManageGuests = \Entrust::can('guests.manage');
                    foreach ($models as $model): ?>
                        <tr>
                            <td>
                                <?php
                                echo $canManageGuests
                                    ? \HTML::linkRoute('admin.guest.details', e($model->name ?: $model->username), [hashids_encode($model->guest_id)])
                                    : e($model->name ?: $model->username)
                                ?>
                            </td>
                            <td><?php echo $model->username ?></td>
                            <td><?php echo $model->guest_device_mac ?></td>
                            <td><?php echo $model->guest_device_ip ?></td>
                            <td><?php echo $model->access_code ?></td>
                            <td><?php echo $model->total_duration ?></td>
                            <td><?php echo $model->downloaded_data_human_readable ?></td>
                            <td><?php echo $model->uploaded_data_human_readable ?></td>
                            <td><span class="label label-<?php echo $model->connection_status_class?>"><?php echo $model->connection_status_value ?></span></td>
                            <td><?php echo $model->start_time_formatted ?></td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>

                <?php pagination($models) ?>

            <?php endif ?>

        </div>
        <!-- /table-responsive -->

    </div>
    <!-- /panel-body -->

</div><!-- /panel -->

<script>
    $(function() {
        var filterForm = $('form.filter-form');

        $('.btn-export').click(function() {

            var inputs = [];
            $(':input:not(button)', filterForm).each(function (i, el) {
                var $el = $(el);
                inputs.push('<input type="hidden" name="' + $el.attr('name') + '" value="' + $el.val() + '">');
            });

            var form = $('<form action="<?php echo route('admin.report.guestSessionsExport') ?>" method="POST">' +
                '<input type="hidden" name="format" value="' + $(this).data('format') + '">' +
                '<?php echo csrf_field() ?>' + inputs.join('') + '</form>');

            form.appendTo($('body'));
            form.submit().remove();

            return false;
        });
    });
</script>