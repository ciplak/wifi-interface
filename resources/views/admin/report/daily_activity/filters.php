<?php
/**
 * @var array $locations
 */
echo Form::open(['class' => 'form-inline margin-bottom filter-form', 'method' => 'GET']) ?>

    <?php if (\Entrust::can('venues.manage-global')): ?>
        <div class="form-group">
            <?php echo Form::select('location', $locations, request('location'), ['class' => 'form-control select', 'placeholder' => trans('venue.all')]) ?>
        </div>
    <?php endif ?>

    <!--<div class="form-group">-->
<?php //echo Form::select('hotspot', [null => trans('device.all')] + \App\Models\Device::pluck('name', 'id')->toArray(), request('hotspot'), ['class' => 'form-control select']) ?>
    <!--</div>-->

    <div class="form-group">
        <?php echo Form::text('date', request('date'), ['class' => 'form-control date', 'id' => 'date', 'placeholder' => trans('messages.date.title')]) ?>
    </div>

    <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
    <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>

<?php echo Form::close() ?>