<div class="form-group" :class="{'has-error': form.errors.has('name')}">
    <label for="name" class="control-label col-md-3 required"><?php echo trans('currency.columns.name')?></label>
    <div class="col-md-9">
        <input type="text" id="name" class="form-control" v-model="form.name"/>
        <span class="help-block">{{ form.errors.get('name') }}</span>
    </div>
</div>

<div class="form-group" :class="{'has-error': form.errors.has('description')}">
    <label for="description" class="control-label col-md-3"><?php echo trans('currency.columns.description')?></label>
    <div class="col-md-9">
        <input type="text" id="description" class="form-control" v-model="form.description"/>
    </div>
</div>

<div class="form-group" :class="{'has-error': form.errors.has('symbol')}">
    <label for="symbol" class="control-label col-md-3 required"><?php echo trans('currency.columns.symbol')?></label>
    <div class="col-md-2">
        <input type="text" id="symbol" class="form-control" maxlength="5" v-model="form.symbol"/>
    </div>
    <div class="col-sm-offset-3 col-sm-9">
        <span class="help-block">{{ form.errors.get('symbol') }}</span>
    </div>
</div>

<div class="form-group" :class="{'has-error': form.errors.has('is_prefix')}">
    <div class="col-sm-offset-3 col-sm-9">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" v-model="form.is_prefix"/> <?php echo trans('currency.columns.is_prefix')?>
            </label>
        </div>
    </div>
</div>