<div class="modal fade" id="modal-currency-form" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form @submit.prevent="save" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button @click="closeForm" type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close')?>"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo trans('currency.page_title') ?></h4>
                </div><!-- /modal-header -->

                <div class="modal-body">
                    <?php echo view('admin.currency.form')->render()?>
                </div><!-- /modal-body -->

                <div class="modal-footer">
                    <button :disabled="form.busy" @click="save" type="submit" class="btn btn-primary">
                        <span v-if="form.busy">
                            <i class="fa fa-refresh fa-spin"></i> <?php echo trans('messages.saving') ?>
                        </span>
                        <span v-else>
                            <i class="fa fa-check"></i> <?php echo trans('messages.save') ?>
                        </span>
                    </button>
                </div><!-- /modal-footer -->

            </div><!-- /modal-content -->
        </form>
    </div><!-- /modal-dialog -->
</div><!-- /modal -->