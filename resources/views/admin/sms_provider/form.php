<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\SmsProvider $model
 * @var array $smsGateways
 * @var array $smsGatewayOptions
 */
echo Form::model($model,
    ['class' => 'form-horizontal', 'autocomplete' => 'off', 'route' => ['admin.sms_provider.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('sms_provider.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.sms_provider.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

            <?php if ($model->exists): ?>
                <a href="<?php echo route('admin.sms_provider.delete', ['smsProvider' => $model]) ?>" class="btn btn-danger btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a>
            <?php endif ?>

            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">

        <?php if ($errors->count()): ?>
        <div class="alert alert-danger">
            <?php echo implode('<br>', $errors->all())?>
        </div>
        <?php endif ?>

        <div class="form-group<?php if ($errors->has('name')) echo ' has-error' ?>">
            <?php echo Form::label('name', trans('sms_provider.columns.name'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-4">
                <?php echo Form::text('name', null, ['class' => 'form-control', 'autofocus'])?>
            </div>
        </div>

        <div class="form-group">
            <?php echo Form::label('description', trans('sms_provider.columns.description'), ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-4">
                <?php echo Form::text('description', null, ['class' => 'form-control'])?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('gateway_id')) echo ' has-error' ?>">
            <?php echo Form::label('gateway_id', trans('sms_provider.columns.gateway'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::select('gateway_id', $smsGatewayOptions, null, ['class' => 'form-control', 'placeholder' => trans('messages.select')]) ?>
            </div>
        </div>

        <?php foreach ($smsGateways as $smsGateway):
            $smsGatewayId = $smsGateway['id'];
            ?>
            <div id="gateway-<?php echo $smsGatewayId ?>" class="gateway-parameters">
                <?php foreach ($smsGateway['parameters'] as $parameter): ?>
                    <div class="form-group">
                        <?php echo Form::label("parameters[$smsGatewayId][$parameter]", trans("sms_provider.gateway.{$smsGateway['key']}.$parameter"), ['class' => 'control-label col-md-2']) ?>
                        <div class="col-md-4">
                            <?php echo Form::text("parameters[$smsGatewayId][$parameter]", $model->parameter($parameter), ['class' => 'form-control']) ?>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>