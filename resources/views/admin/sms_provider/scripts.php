<script>
$(function() {
    var params = $('.gateway-parameters'),
        gatewayId = $('#gateway_id');

    gatewayId.change(function() {
        params.hide();
        var val = parseInt($(this).val());
        $('#gateway-' + val).show();
    });

    gatewayId.trigger('change');
});
</script>