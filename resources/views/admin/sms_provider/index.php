<?php
/**
 * @var App\Models\SmsProvider[]|\Illuminate\Database\Eloquent\Collection $models
 * @var array $gateways
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('sms_provider.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.sms_provider.add') ?>" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
        </div>

    </div>

    <div class="panel-body">
        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('sms_provider.not_found') ?></p>

            <?php else: ?>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><?php echo trans('sms_provider.columns.name') ?></th>
                    <th><?php echo trans('sms_provider.columns.description') ?></th>
                    <th><?php echo trans('sms_provider.columns.gateway') ?></th>
                    <th class="col-actions"></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($models as $model): ?>
                <tr>
                    <td><?php echo HTML::linkRoute('admin.sms_provider.edit', $model->name, [$model]) ?></td>
                    <td><?php echo e($model->description) ?></td>
                    <td><?php if ($model->gateway_id) echo $gateways[$model->gateway_id] ?></td>
                    <td class="col-actions"><a href="<?php echo route('admin.sms_provider.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete ajax-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a></td>
                </tr>
                <?php endforeach ?>
                </tbody>
            </table>

            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->