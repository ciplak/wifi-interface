<div class="row">
    <div class="col-md-10">
        <?php echo Form::textarea('custom_html', null, ['class' => 'form-control', 'id' => 'html']) ?>
    </div>

    <div class="col-md-2" id="snippets">
        <?php echo view('admin.splash_page.tabs.snippets') ?>
    </div>

</div>