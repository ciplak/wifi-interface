<div class="snippet-list <?php if ($model->type == \App\Models\SplashPage::TYPE_ONLINE) echo 'hidden' ?>" id="offline-snippets">
    <ul class="list-unstyled">
        <li>Forms</li>
        <li><a href="#" data-snippet="RegistrationForm register-text='Register'">Registration form</a> <a href="#" data-registration-form-config class="btn btn-default btn-xs"><i class="fa fa-cog"></i></a></li>
        <li><a href="#" data-snippet="LoginForm login-text='Login'">Login form</a> <a href="#" data-login-form-config class="btn btn-default btn-xs"><i class="fa fa-cog"></i></a></li>
        <!--<li><a href="#" data-snippet="PasswordResetForm">Password reset form</a></li>-->
    </ul>

    <ul class="list-unstyled">
        <li>Authentication links</li>
        <li><a href="#" data-snippet="AuthLink:Facebook">Facebook</a></li>
        <li><a href="#" data-snippet="AuthLink:Twitter">Twitter</a></li>
        <li><a href="#" data-snippet="AuthLink:Linkedin">LinkedIn</a></li>
        <li><a href="#" data-snippet="AuthLink:Instagram">Instagram</a></li>
        <li><a href="#" data-snippet="AuthLink:Google">Google</a></li>
        <li><a href="#" data-snippet="AuthLink:Foursquare">Foursquare</a></li>
    </ul>

    <ul class="list-unstyled">
        <li>Authentication attributes</li>
        <li><a href="#" data-snippet="AuthAttr:Facebook">Facebook</a></li>
        <li><a href="#" data-snippet="AuthAttr:Twitter">Twitter</a></li>
        <li><a href="#" data-snippet="AuthAttr:Instagram">Instagram</a></li>
        <li><a href="#" data-snippet="AuthAttr:Linkedin">Linkedin</a></li>
        <li><a href="#" data-snippet="AuthAttr:Google">Google</a></li>
        <li><a href="#" data-snippet="AuthAttr:Foursquare">Foursquare</a></li>
    </ul>

    <ul class="list-unstyled">
        <li>Language</li>
        <li><a href="#" data-snippet="LanguageSelector">Language selector</a></li>
        <li><a href="#" data-snippet="CurrentLanguage">Current Language</a></li>
    </ul>

    <ul id="snippets" class="list-unstyled">
        <li>Images</li>
        <li><a href="#" data-toggle="modal" data-target="#media-uploader-modal">Image Tag</a></li>
        <li><a href="#" data-toggle="modal" data-target="#media-uploader-modal">Image Url</a></li>
    </ul>

    <ul class="list-unstyled">
        <li>Misc</li>
        <li><a href="#" data-snippet="TermsAndConditionsText">Terms and conditions text</a></li>
        <li><a href="#" data-snippet="ErrorMessages text='An unexpected error has occurred. Please try again later'">Error messages</a></li>
        <li><a href="#" data-snippet-modal="advertisement"><?php echo trans('advertisement.page_title') ?></a></li>
    </ul>
</div>

<div class="snippet-list <?php if ($model->type == \App\Models\SplashPage::TYPE_OFFLINE) echo 'hidden' ?>" id="online-snippets">
    <ul class="list-unstyled">
        <li>Online</li>
        <li><a href="#" data-snippet="CountdownTimer">Countdown Timer</a></li>
    </ul>

    <ul class="list-unstyled">
        <li>Language</li>
        <li><a href="#" data-snippet="LanguageSelector">Language selector</a></li>
        <li><a href="#" data-snippet="CurrentLanguage">Current Language</a></li>
    </ul>

    <ul id="snippets" class="list-unstyled">
        <li>Images</li>
        <li><a href="#" data-toggle="modal" data-target="#media-uploader-modal">Image Tag</a></li>
        <li><a href="#" data-toggle="modal" data-target="#media-uploader-modal">Image Url</a></li>
    </ul>

    <ul class="list-unstyled">
        <li>Misc</li>
        <li><a href="#" data-snippet="Guest:Name">Guest Name</a></li>
        <li><a href="#" data-snippet="Link:Disconnect">Disconnect link</a></li>
        <li><a href="#" data-snippet-modal="advertisement"><?php echo trans('advertisement.page_title') ?></a></li>
    </ul>
</div>
