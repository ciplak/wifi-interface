<p class="text-right" style="border-bottom: 1px solid #ccc; padding-bottom: 10px;">
    <a href="<?php echo route('login.preview', $model) ?>" class="btn btn-default" id="page-preview" target="page-preview" data-tooltip="true" title="<?php echo trans('messages.preview') ?>"><i class="fa fa-eye"></i></a>
    <a href="#" class="btn btn-default" id="refresh-preview" data-tooltip="true" title="<?php echo trans('messages.refresh') ?>"><i class="fa fa-refresh"></i></a>
</p>

<iframe src="<?php echo route('login.preview', ['loginPage' => $model, 'inside' => 1]) ?>" scrolling="yes" frameborder="0" width="100%" height="600" id="preview-iframe" style="overflow-x: hidden; overflow-y: scroll"></iframe>