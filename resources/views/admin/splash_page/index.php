<?php
/**
 * @var \App\Models\SplashPage[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('splash_page.page_title') ?></h4>

            <div class="panel-control">
                <a href="<?php echo route('admin.splash_page.add') ?>"  class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
            </div>
        </div><!-- /affix-header -->
    </div><!-- /panel-heading -->

    <div class="panel-body">
        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('splash_page.not_found') ?></p>

            <?php else: ?>
                <?php echo view('admin.splash_page.modals.publish')->render() ?>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><?php echo trans('splash_page.columns.name') ?></th>
                    <th></th>
                    <th><?php echo trans('splash_page.columns.type') ?></th>
                    <th class="col-datetime"><?php echo trans('messages.created_by') ?></th>
                    <th><?php echo trans('messages.updated_by') ?></th>
                    <th style="col-datetime"><?php echo trans('messages.created_at') ?></th>
                    <th style="width: 300px;"></th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($models as $model): ?>
                <tr>
                    <td><?php echo ($model->is_global && \Entrust::can('login-pages.manage-global')) || !$model->is_global ? HTML::linkRoute('admin.splash_page.edit', $model->name, [$model]) : $model->name ?></td>
                    <td><?php if ($model->is_global) echo '<span class="label label-info">', trans('messages.global') ,'</span>' ?></td>
                    <td><?php echo $model->type_icon ?></td>
                    <td><?php echo $model->created_by_name?></td>
                    <td><?php echo $model->updated_by_name?></td>
                    <td><?php echo $model->created_at_formatted ?></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo trans('messages.other_languages') ?> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php
                                $translations = $model->getTranslationsIdList();
                                foreach (getLocales() as $key => $name):
                                    if ($key == $model->locale) continue;

                                    if (isset($translations[$key])): ?>
                                        <li><a href="<?php echo route('admin.splash_page.edit', [$translations[$key]]) ?>"><?php echo trans('locale.options.' . $key) ?><span class="pull-right"><i class="fa fa-pencil"></i></span></a></li>
                                    <?php else: ?>
                                        <li><a href="<?php echo route('admin.splash_page.translate', [$model, $key]) ?>"><?php echo trans('locale.options.' . $key) ?><span class="pull-right"><i class="fa fa-plus"></i></span></a></li>
                                    <?php endif ?>

                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <a href="<?php echo route('admin.splash_page.clone', [$model]) ?>" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('messages.duplicate') ?>"><i class="fa fa-clone"></i></a>
                        <a href="<?php echo route('login.preview', [$model]) ?>" class="btn btn-default btn-sm" target="_preview" data-tooltip="true" title="<?php echo trans('messages.preview') ?>"><i class="fa fa-eye"></i></a>
                        <?php if (($model->is_global && \Entrust::can('login-pages.manage-global')) || !$model->is_global): ?>
                        <a href="#" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('messages.publish') ?>" data-splash-page="<?php echo $model->getRouteKey() ?>" data-toggle="modal" data-target="#modal-publish-splash-page"><i class="fa fa-arrow-circle-up"></i></a>
                        <a href="<?php echo route('admin.splash_page.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a>
                        <?php endif ?>
                    </td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>

                <?php pagination($models) ?>

            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->