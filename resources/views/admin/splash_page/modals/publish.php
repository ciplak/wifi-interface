<?php
/**
 * @var array $locations
 */
?>
<div class="modal fade in" id="modal-publish-splash-page" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close') ?>"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo trans('splash_page.publish_settings') ?></h4>
            </div>
            <div class="modal-body">
                <p><?php echo trans('splash_page.publish_settings_description') ?></p>
                <div class="form-group">
                    <?php echo Form::select('publish_location', $locations, request('location'), ['class' => 'form-control select', 'id' => 'publish_location', 'style' => 'width:100%', 'placeholder' => trans('device.location.all')]) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('messages.cancel') ?></button>
                <button id="btn-publish-splash-page" type="button" class="btn btn-primary"><?php echo trans('messages.publish') ?></button>
            </div>
        </div>
    </div>

</div>

<script>
    $(function() {
        var splashPage,
            publishModal = $('#modal-publish-splash-page');

        publishModal.on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            splashPage = button.data('splash-page');
        });

        publishModal.on('hidden.bs.modal', function() {
            $('#publish_location').val(null).trigger('change');
        });

        $('#btn-publish-splash-page').click(function() {
            var data = {
                splash_page: splashPage,
                publish_location: $('#publish_location').val()
            };

            $.post('<?php echo route('admin.splash_page.publish')?>', data, function(response) {
                publishModal.modal('hide');
                toastr.success('<?php echo trans('splash_page.published') ?>');
            });

            return false;
        });

    });
</script>