<?php
/**
 * @var \App\Models\SplashPage $model
 */
?>
<div class="modal fade in" id="share-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close') ?>"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?php echo trans('messages.share') ?></h4>
            </div>
            <div class="modal-body">

                <p>Send link below to preview the login page with other devices or use QR code scanner to preview it on mobile devices.</p>

                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo route('login.preview', ['loginPage' => $model, 'inside' => 1]) ?>" readonly>
                </div>

                <div id="login-page-qrcode" style="margin: 0 auto; width: 256px;"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo trans('messages.close') ?></button>
            </div>
        </div>
    </div>

</div>