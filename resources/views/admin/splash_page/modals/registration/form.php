<div class="modal fade" id="mdl-registration-fields" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo trans('messages.close') ?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo trans('splash_page.registration') ?></h4>
            </div>
            <div class="modal-body">
                <div class="clearfix">
                    <?php echo view('admin.splash_page.modals.registration.fields')->render() ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo trans('messages.close') ?></button>
            </div>
        </div>
    </div>
</div>
