<?php
/**
 * @var \App\Models\SplashPage $model
 */
?>
<div class="col-md-6 col-sm-6">
    <div class="text-right">
        <ul class="list-inline">
            <li><?php echo trans('messages.required') ?></li>
            <li><?php echo trans('messages.enabled') ?></li>
        </ul>
    </div>

    <?php echo Form::hidden('field-list', null, ['id' => 'field-list']) ?>
    <ol class="dd-list" id="registration-fields">
        <?php
        $checkboxStyles = [
            'data-toggle' => 'toggle',
            'data-onstyle' => 'success',
            'data-on' => trans('messages.yes'),
            'data-off' => trans('messages.no'),
            'data-size' => 'small'
        ];

        $orderedFields = $model->exists ? $model->setting('registration_fields') : $model->getRegistrationFields();

        foreach ($orderedFields as $fieldName => $fieldInfo):
            $fieldTitle = trans("login.form.$fieldName");
            if (isset($allFields[$fieldName]['custom']) && $allFields[$fieldName]['custom']) {
                $fieldTitle = trans('splash_page.custom_field_no', ['no' => str_replace('cf', '', $fieldName)]);
            }

            ?>
            <li class="dd-item clearfix" id="fieldOrder-<?php echo $fieldName ?>">

                <div class="handle pull-left">
                    <i class="fa fa-bars"></i>
                </div>

                <div class="dd-content pull-left" class="registration-field-title-<?php echo $fieldName ?>"><?php echo $fieldTitle ?></div>
                <div class="pull-right">
                    <div class="checkbox-inline">
                        <label>
                            <?php echo Form::checkbox("fields[$fieldName][enabled]", 1, isset($fieldInfo['enabled']) ? $fieldInfo['enabled'] : null, $checkboxStyles + ['id' => "registration_fields_{$fieldName}_enabled"]) ?>
                        </label>
                    </div>

                </div>

                <div class="pull-right">
                    <div class="checkbox-inline">
                        <label>
                            <?php echo Form::checkbox("fields[$fieldName][required]", 1, isset($fieldInfo['required']) ? $fieldInfo['required'] : null, $checkboxStyles + ['id' => "registration_fields_{$fieldName}_required"]) ?>
                        </label>
                    </div>

                </div>
            </li>
        <?php endforeach ?>
    </ol>

</div>

<div class="col-md-6 col-sm-6">
    <h5 class="heading" style="margin-top: 0">Access Code Options</h5>

    <div class="form-group">
        <div class="col-md-12 margin-bottom">
            <div class="checkbox">
                <label class="checkbox-inline">
                    <?php echo Form::checkbox('send_email', 1, null, ['id' => 'send_email']) ?> <?php echo trans('splash_page.send_email') ?>
                </label>

                <label class="checkbox-inline">
                    <?php echo Form::checkbox('send_sms', 1, null, ['id' => 'send_sms']) ?> <?php echo trans('splash_page.send_sms') ?>
                </label>
            </div>

            <div class="checkbox">
                <label class="checkbox-inline">
                    <?php echo Form::checkbox('social_login_requires_code', 1, null, ['id' => 'social_login_requires_code']) ?> <?php echo trans('splash_page.social_login_requires_code') ?>
                </label>
            </div>

            <div class="checkbox">
                <label class="checkbox-inline">
                    <?php echo Form::checkbox('overwrite_guests', 1, null, ['id' => 'overwrite_guests']) ?> <?php echo trans('splash_page.overwrite_guests') ?>
                </label>
            </div>
        </div>
    </div>

    <?php for ($i = 1; $i <= config('main.custom_field_count'); $i++): ?>
        <h5 class="heading"><?php echo trans('splash_page.custom_field_no_title', ['no' => $i])?></h5>
        <?php
        foreach(getLocales() as $locale => $name):
            $inputClass = 'form-control';
            $inputClass .= isRTL($locale) ? ' rtl' : ' ltr';
            ?>

            <div class="form-group">
                <div class="col-md-12">
                    <?php echo Form::text("fields[custom_field_{$i}][title][$locale]", array_get($model->settings, "registration_fields.custom_field_{$i}.title.$locale"), ['class' => $inputClass, 'placeholder' => trans('locale.options.' . $locale), 'id' => 'custom-field-' . $locale . '-' . $i]) ?>
                </div>
            </div>

        <?php endforeach ?>
    <?php endfor ?>

</div>

<script>
    $(function() {
        var registrationFields = $("#registration-fields");
        registrationFields.sortable({
            axis: 'y',
            handle: '.handle',
            placeholder: 'placeholder',
            forcePlaceholderSize: true,
            revert: 100
        });

        $('#login-page-form').submit(function() {
            $('#field-list').val(registrationFields.sortable('serialize', { expression: /(.+)[-=](.+)/ }));
        });

        var chkSendEmail = $('#send_email'),
            chkSendSms = $('#send_sms'),
            emailEnabledField = $('#registration_fields_email_enabled'),
            emailRequiredField = $('#registration_fields_email_required'),
            phoneEnabledField = $('#registration_fields_phone_enabled'),
            phoneRequiredField = $('#registration_fields_phone_required');

        var emailCheck = smsCheck = 0;
        chkSendEmail.click(function() {
            if (chkSendEmail.prop('checked')) {
                emailCheck = 1;
                emailEnabledField.prop('checked', true).trigger('change');
                emailRequiredField.prop('checked', true).trigger('change');
                emailCheck = 0;
            }
        });

        function setSendEmailCheckBox() {
            if (!(emailEnabledField.prop('checked') && emailRequiredField.prop('checked'))) {
                chkSendEmail.prop('checked', false);
            }
        }

        emailEnabledField.change(function() {
            if (!emailCheck) setSendEmailCheckBox();
        });

        emailRequiredField.change(function() {
            if (!emailCheck) setSendEmailCheckBox();
        });

        function setSendSmsCheckBox() {
            if (!(phoneEnabledField.prop('checked') && phoneRequiredField.prop('checked'))) {
                chkSendSms.prop('checked', false);
            }
        }

        chkSendSms.click(function() {
            if (chkSendSms.prop('checked')) {
                smsCheck = 1;
                phoneEnabledField.prop('checked', true).trigger('change');
                phoneRequiredField.prop('checked', true).trigger('change');
                smsCheck = 0;
            }
        });

        phoneEnabledField.change(function() {
            if (!smsCheck) setSendSmsCheckBox();
        });

        phoneRequiredField.change(function() {
            if (!smsCheck) setSendSmsCheckBox();
        });

    });
</script>
