<?php
/**
 * @var \App\Models\SplashPage $model
 */
?>
<div class="col-md-12 col-sm-12">
    <div class="text-right">
        <ul class="list-inline">
            <li><?php echo trans('messages.enabled') ?></li>
        </ul>
    </div>

    <?php echo Form::hidden('login-field-list', null, ['id' => 'login-field-list']) ?>
    <ol class="dd-list" id="login-fields">
        <?php
        $checkboxStyles = [
            'class' => 'login-form-field',
            'data-toggle' => 'toggle',
            'data-onstyle' => 'success',
            'data-on' => trans('messages.yes'),
            'data-off' => trans('messages.no'),
            'data-size' => 'small'
        ];

        $orderedLoginFields = $model->exists ? $model->setting('login_fields') : $model->getLoginFields();

        foreach ($orderedLoginFields as $fieldId => $fieldInfo):
            $fieldTitle = trans("login.form.$fieldId");
            if (isset($allLoginFields[$fieldId]['custom']) && $allLoginFields[$fieldId]['custom']) {
                $fieldTitle = trans('splash_page.custom_field_no', ['no' => str_replace('cf', '', $fieldId)]);
            }
            if (!$model->exists && $fieldId == 'email') $fieldInfo['enabled'] = true;
            ?>
            <li class="dd-item clearfix" id="loginFieldOrder-<?php echo $fieldId ?>">

                <div class="handle pull-left">
                    <i class="fa fa-bars"></i>
                </div>

                <div class="dd-content pull-left"><?php echo $fieldTitle ?></div>
                <div class="pull-right">
                    <div class="checkbox-inline">
                        <label>
                            <?php echo Form::checkbox("loginFields[$fieldId][enabled]", 1, isset($fieldInfo['enabled']) ? $fieldInfo['enabled'] : null, $checkboxStyles) ?>
                        </label>
                    </div>

                </div>

            </li>
        <?php endforeach ?>
    </ol>

</div>

<script>
    $(function() {
        var loginFields = $("#login-fields");
        loginFields.sortable({
            axis: 'y',
            handle: '.handle',
            placeholder: 'placeholder',
            forcePlaceholderSize: true,
            revert: 100
        });

        $('#login-page-form').submit(function() {
            $('#login-field-list').val(loginFields.sortable('serialize', { expression: /(.+)[-=](.+)/ }));
        });
    });
</script>
