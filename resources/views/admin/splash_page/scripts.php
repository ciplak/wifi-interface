<script src="//ajax.aspnetcdn.com/ajax/jshint/r07/jshint.js"></script>
<script src="http://csslint.net/js/csslint.js"></script>
<!--<script src="//htmlhint.com/js/htmlhint.js"></script>-->

<script>
    function refreshPreview() {
        document.getElementById('preview-iframe').contentWindow.location.reload();
    }

    $(function () {
        <?php if ($model->exists): ?>
        var $name = $('#name'),
            $nameGroup = $name.closest('.form-group'),
            $nameHelpBlock = $('#name-help-block');

        $('#login-page-form').submit(function() {
            var $this = $(this);

            $nameGroup.removeClass('has-error');
            $nameHelpBlock.text('');

            $.post($this.attr('action'), $this.serialize()).then(function(response, statusText, xhr) {
                if (response.status) {
                    toastr.success('<?php echo trans('splash_page.saved') ?>');
                    refreshPreview();
                }
            }, 'json').fail(function(response) {
                if (response.status == 422) {
                    if (response.responseJSON.name.length) {
                        $nameGroup.addClass('has-error');
                        $nameHelpBlock.text(response.responseJSON.name[0]);
                    }
                }
                toastr.error('<?php echo trans('splash_page.not_saved') ?>');
            });
            return false;
        });

        $('a#refresh-preview').click(function() {
            refreshPreview();
            return false;
        });

        $('#login-page-qrcode').qrcode({
            render: 'div',
            text: '<?php echo route('login.preview', ['loginPage' => $model, 'inside' => 1]) ?>',
            size: 100,
            mSize: 0.8,
        });
        <?php else: ?>

        var loginPageType = $('#login-page-type');

        loginPageType.change(function() {
            $('.snippet-list').addClass('hidden');

            if ($(this).val() == 1) {
                $('#online-snippets').removeClass('hidden');
            } else {
                $('#offline-snippets').removeClass('hidden');
            }
        });

        loginPageType.trigger('change');
        <?php endif ?>

        $('#login-page-form').submit(function () {
            $(this).attr('action', $(this).attr('action') + window.location.hash);
        });

        htmlEditor.refresh();
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            htmlEditor.refresh();
            cssEditor.refresh();
            jsEditor.refresh();
        });

        $(document.body).on('click', '[data-snippet]', function() {
            embedSnippet($(this).data('snippet'));

            $('#media-uploader-modal').modal('hide');

            return false;
        });

        $('a[data-registration-form-config]').click(function() {
            $('#mdl-registration-fields').modal();
        });

        $('a[data-login-form-config]').click(function() {
            $('#mdl-login-fields').modal();
        });

        $('a[data-snippet-modal="advertisement"').click(function() {
            $('#mdl-advertisement').modal();
            return false;
        });
    });

    var htmlEditor = CodeMirror.fromTextArea(document.getElementById("html"), {
        value: true,
        mode: 'htmlmixed',
        lineNumbers: true,
        autoCloseTags: true,
        autoCloseBrackets: true,
        matchBrackets: true,
        showCursorWhenSelecting: true,
        theme: "monokai",
        matchTags: {bothTags: true},
        extraKeys: {"Ctrl-Space": "autocomplete", "Ctrl-J": "toMatchingTag"},
        foldGutter: true,
        styleActiveLine: true,
        viewportMargin: 10,
        gutters: ["CodeMirror-lint-markers", "CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        //lint: true
    });

    //emmetCodeMirror(htmlEditor);

    var cssEditor = CodeMirror.fromTextArea(document.getElementById("css-editor"), {
        value: true,
        mode: 'css',
        lineNumbers: true,
        autoCloseTags: true,
        autoCloseBrackets: true,
        matchBrackets: true,
        showCursorWhenSelecting: true,
        theme: "monokai",
        matchTags: {bothTags: true},
        extraKeys: {"Ctrl-Space": "autocomplete", "Ctrl-J": "toMatchingTag"},
        foldGutter: true,
        styleActiveLine: true,
        viewportMargin: 10,
        gutters: ["CodeMirror-lint-markers", "CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        lint: true
    });

    //    emmetCodeMirror(cssEditor);

    var jsEditor = CodeMirror.fromTextArea(document.getElementById("js-editor"), {
        value: true,
        mode: 'javascript',
        lineNumbers: true,
        autoCloseTags: true,
        autoCloseBrackets: true,
        matchBrackets: true,
        showCursorWhenSelecting: true,
        theme: "monokai",
        matchTags: {bothTags: true},
        extraKeys: {"Ctrl-Space": "autocomplete", "Ctrl-J": "toMatchingTag"},
        foldGutter: true,
        styleActiveLine: true,
        viewportMargin: 10,
        gutters: ["CodeMirror-lint-markers", "CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        lint: true
    });

    //    emmetCodeMirror(jsEditor);

    var snippetStartTag = "[[";
    var snippetEndTag = "]]";

    function checkIfAlreadyEmbedded(snippet) {
        var content = htmlEditor.getValue(),
            regexPattern = new RegExp('\\[\\[(\\s*)' + snippet + '(.*)\\]\\]', 'ig'),
            status = regexPattern.test(content);

        if (status) {
            bootbox.alert("You have already embedded this snippet in your html code");
        }

        return status;
    }

    function embedSnippet(snippet) {
        var snippetBase = snippet.split(' ')[0].toLowerCase();

        switch (snippetBase) {
            case 'registrationform':
            case 'loginform':
            case 'passwordresetform':
            case 'languageselector':
                if (checkIfAlreadyEmbedded(snippetBase)) {
                    return;
                }
                break;
        }
        htmlEditor.replaceSelection(snippetStartTag + snippet + snippetEndTag);
    }
</script>