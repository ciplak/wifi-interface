<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\SplashPage         $model
 * @var array                          $typeOptions
 * @var array                          $locales
 */
$route = $model->exists ? ['admin.splash_page.update', $model] : ['admin.splash_page.create', $model];
echo Form::model($model, ['class' => 'form-horizontal', 'id' => 'login-page-form', 'autocomplete' => 'off', 'files' => true, 'route' => $route]) ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-actions">

        <div class="panel-heading">
            <div class="affix-header">
                <h4 class="panel-title"><?php echo trans('splash_page.page_title') ?></h4>

                <div class="panel-control">
                    <a href="<?php echo route('admin.splash_page.index') ?>" class="btn btn-default" data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

                    <?php if ($model->exists): ?>
                        <a href="<?php echo route('admin.splash_page.clone', [$model]) ?>" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('messages.duplicate') ?>"><i class="fa fa-clone"></i></a>
                        <a href="#" class="btn btn-default" data-tooltip="true" title="<?php echo trans('messages.publish') ?>" data-splash-page="<?php echo $model->getRouteKey() ?>" data-toggle="modal" data-target="#modal-publish-splash-page"><i class="fa fa-arrow-circle-up"></i></a>
                        <a href="<?php echo route('admin.splash_page.delete', [$model]) ?>" class="btn btn-danger btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a>
                    <?php endif  ?>

                    <button class="btn btn-primary" name="save" value="publish" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>

                </div>
            </div>
        </div>

        <div class="panel-body">

            <?php if ($errors->count()): ?>
                <div class="alert alert-danger">
                    <?php echo implode('<br>', $errors->all())?>
                </div>
            <?php endif ?>

            <div>
                <div class="form-group<?php if ($errors->has('name')) echo ' has-error' ?>">
                    <div class="col-md-10">
                        <?php echo Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'autofocus', 'placeholder' => trans('splash_page.columns.name')]); ?>
                        <span class="help-block" id="name-help-block"></span>
                    </div><!-- /col -->

                    <?php if (!$model->exists): ?>
                    <div class="col-md-2">
                        <?php echo Form::select('type', $typeOptions, null, ['class' => 'form-control', 'id' => 'login-page-type']); ?>
                    </div><!-- /col -->

                    <?php else: ?>

                    <div class="col-md-2">
                        <div class="btn-group btn-block">

                            <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="visible-lg-inline"><?php echo trans('messages.language') ?> </span><?php echo trans('locale.options.' . $model->locale) ?> <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu btn-block">
                                <?php
                                foreach ($locales as $key => $name):
                                    if ($key == $model->locale) continue;

                                    if ($key == 'en') : ?>
                                        <li><a href="<?php echo route('admin.splash_page.edit', [hashids_encode($model->parent_id)]) ?>"><?php echo trans('locale.options.' . $key) ?><span class="pull-right"><i class="fa fa-pencil"></i></span></a></li>
                                    <?php
                                        continue;
                                    endif;

                                    if (isset($translations[$key])): ?>
                                    <li><a href="<?php echo route('admin.splash_page.edit', [hashids_encode($translations[$key])]) ?>"><?php echo trans('locale.options.' . $key) ?><span class="pull-right"><i class="fa fa-pencil"></i></span></a></li>
                                    <?php else: ?>
                                    <li><a href="<?php echo route('admin.splash_page.translate', [$model, $key]) ?>"><?php echo trans('locale.options.' . $key) ?><span class="pull-right"><i class="fa fa-plus"></i></span></a></li>
                                    <?php endif ?>

                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div><!-- /col -->
                    <?php endif ?>

                </div>

                <ul class="pull-right list-inline" style="margin-bottom: 0;">
                    <li><a href="#" class="btn btn-default pull-right" data-toggle="modal" data-target="#share-modal"><i class="fa fa-share"></i> <?php echo trans('messages.share') ?></a></li>
                    <li><a href="#" class="btn btn-default pull-right" data-toggle="modal" data-target="#media-uploader-modal"><i class="fa fa-image"></i> <?php echo trans('splash_page.images') ?></a></li>
                </ul>

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-html" aria-controls="html" role="tab" data-toggle="tab"><?php echo trans('splash_page.custom_html') ?></a></li>
                    <li role="presentation"><a href="#tab-css" aria-controls="css" role="tab" data-toggle="tab"><?php echo trans('splash_page.custom_css') ?></a></li>
                    <li role="presentation"><a href="#tab-js" aria-controls="js" role="tab" data-toggle="tab"><?php echo trans('splash_page.custom_js') ?></a></li>
                    <?php if ($model->exists): ?>
                    <li role="presentation"><a href="#tab-preview" aria-controls="preview" role="tab" data-toggle="tab"><?php echo trans('messages.preview') ?></a></li>
                    <?php endif  ?>
                </ul><!-- /nav-tabs -->

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-html">
                        <?php echo view('admin.splash_page.tabs.html')->render() ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab-css">
                        <?php echo view('admin.splash_page.tabs.css')->render() ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab-js">
                        <?php echo view('admin.splash_page.tabs.js')->render() ?>
                    </div>

                    <?php if ($model->exists): ?>
                    <div role="tabpanel" class="tab-pane" id="tab-preview">
                        <?php echo view('admin.splash_page.tabs.preview')->render() ?>
                    </div>
                    <?php endif ?>
                </div><!-- /tab-content -->

            </div>


        </div><!-- /panel-body -->

    </div><!-- /panel -->

    </div><!-- /col -->
</div><!-- /row -->

<!-- Modal -->
<?php echo view('admin.media.manager_modal', ['showSnippets' => true])->render() ?>
<?php echo view('admin.splash_page.modals.registration.form')->render() ?>
<?php echo view('admin.splash_page.modals.login.form')->render() ?>

<?php echo Form::close() ?>

<?php echo view('admin.splash_page.modals.share')->render() ?>
<?php echo view('admin.splash_page.modals.publish')->render() ?>

<?php echo view('admin.advertisement.modal')->render() ?>