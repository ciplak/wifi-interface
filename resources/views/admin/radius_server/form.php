
<div class="form-group" :class="{'has-error': form.errors.has('type')}">
    <label for="type" class="control-label col-md-3 required"><?php echo trans('radius_server.columns.type')?></label>
    <div class="col-md-9">
        <select v-model="form.type" class="form-control">
            <option v-for="option in serverTypes" :value="option.value">
                {{ option.text }}
            </option>
        </select>
        <span class="help-block">{{ form.errors.get('type') }}</span>
    </div>
</div>

<div class="form-group" :class="{'has-error': form.errors.has('ip')}">
    <label for="ip" class="control-label col-md-3 required"><?php echo trans('messages.ip')?></label>
    <div class="col-md-9">
        <input type="text" id="ip" class="form-control" v-model="form.ip"/>
        <span class="help-block">{{ form.errors.get('ip') }}</span>
    </div>
</div>

<div class="form-group" :class="{'has-error': form.errors.has('secret')}">
    <label for="secret" class="control-label col-md-3 required"><?php echo trans('messages.secret')?></label>
    <div class="col-md-9">
        <input type="text" id="secret" class="form-control" v-model="form.secret"/>
        <span class="help-block">{{ form.errors.get('secret') }}</span>
    </div>
</div>

<div class="form-group" :class="{'has-error': form.errors.has('auth_port')}">
    <label for="auth_port" class="control-label col-md-3 required"><?php echo trans('radius_server.columns.auth_port')?></label>
    <div class="col-md-3">
        <input type="text" id="auth_port" class="form-control" v-model="form.auth_port"/>
        <span class="help-block">{{ form.errors.get('auth_port') }}</span>
    </div>
</div>

<div class="form-group" :class="{'has-error': form.errors.has('acc_port')}">
    <label for="acc_port" class="control-label col-md-3 required"><?php echo trans('radius_server.columns.acc_port')?></label>
    <div class="col-md-3">
        <input type="text" id="acc_port" class="form-control" v-model="form.acc_port"/>
        <span class="help-block">{{ form.errors.get('acc_port') }}</span>
    </div>
</div>
