<radius-servers inline-template>
    <div>

        <div class="panel panel-default panel-actions">

            <div class="panel-heading">
                <h4 class="panel-title"><?php echo trans('radius_server.page_title') ?></h4>

                <div class="panel-control">
                    <a @click="showForm()" href="#" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
                </div>

            </div>

            <div class="panel-body">

                <div class="table-responsive">

                    <p v-show="!items.length" class="alert alert-warning"><?php echo trans('radius_server.not_found') ?></p>

                    <table v-show="items.length" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th><?php echo trans('radius_server.columns.type') ?></th>
                            <th><?php echo trans('messages.ip') ?></th>
                            <th><?php echo trans('radius_server.columns.auth_port') ?></th>
                            <th><?php echo trans('radius_server.columns.acc_port') ?></th>
                            <th class="col-actions"></th>
                        </tr>
                        </thead>

                        <tbody>

                        <tr v-for="item in items">
                            <td>{{ item.type_name }}</td>
                            <td><a href="#" @click="showForm(item)">{{ item.ip }}</a></td>
                            <td>{{ item.auth_port }}</td>
                            <td>{{ item.acc_port }}</td>
                            <td class="col-actions"><a href="#" @click="remove(item)" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a></td>
                        </tr>

                        </tbody>

                    </table>

                </div><!-- /table-responsive -->

            </div><!-- /panel-body -->

        </div><!-- /panel -->

        <?php echo view('admin.radius_server.modal')->render() ?>

    </div>
</radius-servers>