<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('alert_message.page_title') ?></h4>
    </div>

    <div class="panel-body">

        <?php echo view('admin.alert_message.filters')->render() ?>
        <?php echo view('admin.alert_message.grid')->render() ?>

    </div><!-- /panel-body -->

</div><!-- /panel -->