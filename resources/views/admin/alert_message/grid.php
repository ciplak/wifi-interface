<?php
/**
 * @var \App\Models\AlertMessage[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="table-responsive">

    <?php if ($models->isEmpty()): ?>

        <p class="alert alert-warning"><?php echo trans('alert_message.not_found') ?></p>

    <?php else: ?>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th><?php echo trans('alert_message.columns.priority') ?></th>
                <th><?php echo trans('alert_message.columns.message') ?></th>
                <th><?php echo trans('alert_message.columns.description') ?></th>
                <th class="col-datetime"><?php echo trans('alert_message.columns.date') ?></th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($models as $model): ?>
                <tr>
                    <td>
                        <span class="label label-<?php echo $model->priority_label_type ?>">
                        <?php echo $model->priority_value ?>
                        </span>
                    </td>
                    <td><?php echo $model->message ?></td>
                    <td><?php echo $model->description ?></td>
                    <td><?php echo $model->created_at ?></td>
                </tr>
            <?php endforeach ?>
            </tbody>

        </table>

        <?php pagination($models) ?>

    <?php endif ?>

</div><!-- /table-responsive -->