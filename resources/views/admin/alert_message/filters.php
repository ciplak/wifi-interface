<?php
/**
 * @var array $priorityOptions
 */
echo Form::open(['class' => 'form-inline margin-bottom', 'method' => 'GET']) ?>

    <div class="form-group">
        <?php echo Form::select('priority', $priorityOptions, request('priority'), ['class' => 'form-control select', 'placeholder' => trans('alert_message.priority.all')]) ?>
    </div>

    <div class="form-group">
        <?php echo Form::text('message', request('message'), ['class' => 'form-control', 'placeholder' => trans('alert_message.columns.message')]) ?>
    </div>

    <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
    <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>

<?php echo Form::close() ?>