<?php echo Form::open(['route' => ['admin.role.updatePermissions']])?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="affix-header">
                <h4 class="panel-title"><?php echo trans('permission.page_title') ?></h4>

                <div class="panel-control">
                    <button type="submit" class="btn btn-primary" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
                </div>
            </div>
        </div>

        <div class="panel-body">

            <?php /** @var \App\Models\Role[] $roles */
            if ($roles):?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-fixed">
                        <thead>
                        <tr>
                            <th><input type="text" class="form-control" id="role-search-input" placeholder="search..."></th>
                            <?php foreach ($roles as $role): ?>
                                <th><?php echo $role->display_name, ' (', $role->name, ')'?></th>
                            <?php endforeach;?>
                        </tr>
                        </thead>
                        <tbody id="fbody">
                        <?php /** @var \App\Models\Permission[] $permissions */
                        foreach ($permissions as $permission): ?>
                            <tr>
                                <td><?php echo "<strong>{$permission->display_name}</strong><br>({$permission->name})" ?></td>
                                <?php foreach ($roles as $role): ?>
                                    <th><?php echo Form::checkbox("roles[{$role->name}][{$permission->id}]", $permission->id ,$role->hasPermission($permission->name)) ?></th>
                                <?php endforeach;?>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?php endif ?>
        </div>
        
    </div>
<?php echo Form::close()?>

<script>
    $(function() {
        $("#role-search-input").keyup(function () {
            //split the current value of searchInput
            var data = this.value.split(" ");
            //create a jquery object of the rows
            var jo = $("#fbody").find("tr");
            if (this.value == "") {
                jo.show();
                return;
            }
            //hide all the rows
            jo.hide();

            //Recusively filter the jquery object to get results.
            jo.filter(function (i, v) {
                var $t = $(this);
                for (var d = 0; d < data.length; ++d) {
                    if ($t.is(":contains('" + data[d] + "')")) {
                        return true;
                    }
                }
                return false;
            })
            //show the rows that match.
                .show();
        }).focus(function () {
            this.value = "";
            $(this).css({
                "color": "black"
            });
            $(this).unbind('focus');
        });
    });
</script>
