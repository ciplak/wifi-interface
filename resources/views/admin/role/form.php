<form @submit.prevent="save" class="form-horizontal">

    <div class="form-group" :class="{'has-error': form.errors.has('name')}">
        <label for="name" class="control-label col-md-3 required"><?php echo trans('role.columns.name')?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" v-model="form.name"/>
            <span class="help-block">{{ form.errors.get('name') }}</span>
        </div>
    </div>

    <div class="form-group" :class="{'has-error': form.errors.has('display_name')}">
        <label for="name" class="control-label col-md-3"><?php echo trans('role.columns.display_name')?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" v-model="form.display_name"/>
            <span class="help-block">{{ form.errors.get('display_name') }}</span>
        </div>
    </div>

    <div class="form-group" :class="{'has-error': form.errors.has('description')}">
        <label for="name" class="control-label col-md-3"><?php echo trans('role.columns.description')?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" v-model="form.description"/>
            <span class="help-block">{{ form.errors.get('description') }}</span>
        </div>
    </div>

</form>