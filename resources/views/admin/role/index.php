<roles inline-template>
    <div>

        <form class="form-horizontal">
            <div class="panel panel-default panel-actions">

                <div class="panel-heading">
                    <div class="affix-header">
                        <h4 class="panel-title"><?php echo trans('role.page_title') ?></h4>

                        <div class="panel-control">
                            <a @click="showForm()" href="#" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">

                    <div class="table-responsive">

                        <p v-show="!items.length" class="alert alert-warning"><?php echo trans('role.not_found') ?></p>

                        <table v-show="items.length" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th><?php echo trans('role.columns.name') ?></th>
                                <th><?php echo trans('role.columns.display_name') ?></th>
                                <th><?php echo trans('role.columns.description') ?></th>
                                <th style="width: 20px;"></th>
                            </tr>
                            </thead>

                            <tbody>

                            <tr v-for="item in items">
                                <td><a href="#" @click="showForm(item)">{{ item.name }}</a></td>
                                <td>{{ item.display_name }}</td>
                                <td>{{ item.description }}</td>
                                <td><a v-show="item.name != 'developer'" href="#" @click="remove(item)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a></td>
                            </tr>

                            </tbody>

                        </table>

                    </div><!-- /table-responsive -->

                </div><!-- /panel-body -->

            </div><!-- /panel -->
        </form>

        <?php echo view('admin.role.modal')->render() ?>

    </div>
</roles>