<?php
/**
 * @var \App\Models\ServiceProfile[]|\Illuminate\Database\Eloquent\Collection $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <div class="affix-header">
            <h4 class="panel-title"><?php echo trans('service_profile.page_title') ?></h4>

            <div class="panel-control">
                <a href="<?php echo route('admin.service_profile.add') ?>"  class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
            </div>
        </div><!-- /affix-header -->

    </div><!-- /panel-heading -->

    <div class="panel-body">
        <div class="table-responsive">
            <?php echo view('admin.service_profile.partials.filters') ?>

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('service_profile.not_found') ?></p>

            <?php else: ?>

                <?php echo view('admin.service_profile.partials.grid', ['models' => $models])->render()?>

                <?php pagination($models) ?>
            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->