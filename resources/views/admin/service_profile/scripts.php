<script>
$(function() {
    <?php
    use App\Models\ServiceProfile;

    ?>
    var recurringOptions = $('.recurring-options'),
        paidModelOptions = $('.paid-model-options'),
        expiryDateOptions = $('.expiry-date-options');

    var type = $('#type'),
        serviceModel = $('#model'),
        expiryDate = $('#expiry_date');

    type.change(function () {
        if ($(this).val() == <?php echo ServiceProfile::TYPE_RECURRING ?>) {
            recurringOptions.removeClass('hidden');
        } else {
            recurringOptions.addClass('hidden');
        }
    });

    serviceModel.change(function () {
        if ($(this).val() == <?php echo ServiceProfile::MODEL_PAID ?>) {
            paidModelOptions.removeClass('hidden');
        } else {
            paidModelOptions.addClass('hidden');
        }
    });

    expiryDate.change(function () {
        if ($(this).val() == <?php echo ServiceProfile::STATUS_ACTIVE ?>) {
            expiryDateOptions.removeClass('hidden');
        } else {
            expiryDateOptions.addClass('hidden');
        }
    });

    type.trigger('change');
    serviceModel.trigger('change');
    expiryDate.trigger('change');
});
</script>