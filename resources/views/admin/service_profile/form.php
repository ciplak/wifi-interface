<?php
use App\Models\ServiceProfile;

/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var ServiceProfile $model
 */
echo Form::model($model, ['class' => 'form-horizontal', 'autocomplete' => 'off', 'route' => ['admin.service_profile.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('service_profile.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.service_profile.index') ?>" class="btn btn-default"  data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>

            <?php if ($model->exists): ?>
            <a href="<?php echo route('admin.service_profile.delete', [$model]) ?>" class="btn btn-danger btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i> </a>
            <?php endif ?>

            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">

        <?php echo view('admin.service_profile.partials.fields', ['model' => $model])->render() ?>

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>
