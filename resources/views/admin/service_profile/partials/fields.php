<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\ServiceProfile $model
 * @var array $types
 * @var array $durationTypes
 * @var array $renewalFrequencyTypes
 * @var array $modelOptions
 * @var array $servicePinOptions
 * @var array $statusOptions
 * @var array $qosLevels
 */

$inputClass = 'form-control';
$inputClass .= isRTL(app()->getLocale()) ? ' rtl' : ' ltr';
?>
<div class="col-md-6">

    <div class="form-group<?php if ($errors->has('name')) echo ' has-error' ?>">
        <?php echo Form::label('name', trans('service_profile.columns.name'), ['class' => 'control-label col-md-3 required']) ?>
        <div class="col-md-9">
            <?php echo Form::text('name', null, ['class' => $inputClass, 'autofocus']) ?>
            <?php if ($errors->has('name')): ?>
                <span class="help-block"><?php echo $errors->first('name') ?></span>
            <?php endif ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('description', trans('service_profile.columns.description'), ['class' => 'control-label col-md-3']) ?>
        <div class="col-md-9">
            <?php echo Form::text('description', null, ['class' => $inputClass]) ?>
            <?php if ($errors->has('description')): ?>
                <span class="help-block"><?php echo $errors->first('description') ?></span>
            <?php endif ?>
        </div>
    </div>

    <div class="form-group <?php if ($errors->has('duration')) echo 'has-error' ?>">
        <?php echo Form::label('duration', trans('service_profile.columns.duration'), ['class' => 'control-label col-md-3 required']) ?>
        <div class="col-md-2">
            <?php echo Form::number('duration', null, ['class' => 'form-control input-numeric', 'pattern' => '\d', 'min' => 1]) ?>
        </div>
        <div class="col-md-3">
            <?php echo Form::select('duration_type', $durationTypes, null, ['class' => 'form-control']) ?>
        </div>
        <?php if ($errors->has('duration')): ?>
            <div class="col-md-9 col-md-offset-3">
                <span class="help-block"><?php echo $errors->first('duration') ?></span>
            </div>
        <?php endif ?>
    </div><!-- /form-group -->

    <div class="form-group <?php if ($errors->has('type') || $errors->has('renewal_frequency')) echo 'has-error' ?>">
        <label for="type" class="control-label col-md-3">
            <?php echo trans('service_profile.columns.type') ?>
            <i class="fa fa-question-circle" data-toggle="tooltip" data-html="true" title="<?php echo trans('service_profile.type.tooltip') ?>"></i>
        </label>

        <div class="col-md-3">
            <?php echo Form::select('type', $types, null, ['class' => 'form-control', 'id' => 'type']) ?>
        </div>

        <div class="recurring-options hidden">
            <div class="col-md-2">
                <?php echo Form::text('renewal_frequency', null, ['class' => 'form-control input-numeric']) ?>
            </div>

            <div class="col-md-2">
                <?php echo Form::select('renewal_frequency_type', $renewalFrequencyTypes, null, ['class' => 'form-control']) ?>
            </div>

        </div>

        <?php if ($errors->has('renewal_frequency')): ?>
            <div class="col-md-9 col-md-offset-3">
                <span class="help-block"><?php echo $errors->first('renewal_frequency') ?></span>
            </div>
        <?php endif ?>

    </div><!-- /form-group -->

    <div class="form-group">
        <?php echo Form::label('model', trans('service_profile.columns.model'), ['class' => 'control-label col-md-3']) ?>
        <div class="col-md-3">
            <?php echo Form::select('model', $modelOptions, null, ['class' => 'form-control']) ?>
        </div>

        <div class="paid-model-options hidden">
            <?php echo Form::label('price', trans('service_profile.columns.price'), ['class' => 'control-label col-md-1']) ?>
            <div class="col-md-2">
                <?php echo Form::text('price', null, ['class' => 'form-control input-numeric']) ?>
            </div>
        </div>
    </div>
    <!-- /form-group -->

    <div class="form-group">
        <?php echo Form::label('status', trans('messages.status'), ['class' => 'control-label col-md-3']) ?>
        <div class="col-md-3">
            <?php echo Form::select('status', $statusOptions, null, ['class' => 'form-control']) ?>
        </div>
    </div>

</div><!-- /col -->

<div class="col-md-6">

    <div class="form-group">
        <?php echo Form::label('max_download_bandwidth', trans('service_profile.columns.max_download_bandwidth'), ['class' => 'control-label col-md-5']) ?>
        <div class="col-md-4">
            <?php echo Form::number('max_download_bandwidth', null, ['class' => 'form-control input-numeric', 'min' => 0, 'pattern' => '\d']) ?>
        </div>
        <label class="form-control-static">kbps (non-Cisco)</label>
    </div><!-- /form-group -->

    <div class="form-group">
        <?php echo Form::label('max_upload_bandwidth', trans('service_profile.columns.max_upload_bandwidth'), ['class' => 'control-label col-md-5']) ?>
        <div class="col-md-4">
            <?php echo Form::number('max_upload_bandwidth', null, ['class' => 'form-control input-numeric', 'min' => 0, 'pattern' => '\d']) ?>
        </div>
        <label class="form-control-static">kbps (non-Cisco)</label>
    </div><!-- /form-group -->

    <div class="form-group">
        <label for="qos_level" class="control-label col-md-5">
            <?php echo trans('service_profile.columns.qos_level') ?>
            <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('service_profile.qos_level.tooltip') ?>"></i>
        </label>

        <div class="col-md-4">
            <?php echo Form::select('qos_level', $qosLevels, null, ['class' => 'form-control select', 'id' => 'qos_level', 'placeholder' => trans('messages.none')]) ?>
        </div>
        <label class="form-control-static">(for Cisco WLC)</label>
    </div><!-- /form-group -->

    <div class="form-group">
        <?php echo Form::label('expiry_date', trans('service_profile.columns.expiry_date'), ['class' => 'control-label col-md-5']) ?>

        <div class="col-md-4">
            <?php echo Form::text('expiry_date', $model->expiry_date_value, ['class' => 'form-control future-date']) ?>
        </div>
    </div><!-- /form-group -->

    <div class="form-group">
        <label for="service_pin" class="control-label col-md-5">
            <?php echo trans('service_profile.columns.service_pin') ?>
            <i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo trans('service_profile.service_pin.tooltip') ?>"></i>
        </label>

        <div class="col-md-4">
            <?php echo Form::select('service_pin', $servicePinOptions, null, ['class' => 'form-control', 'id' => 'service_pin']) ?>
        </div>
    </div><!-- /form-group -->

</div><!-- /col -->