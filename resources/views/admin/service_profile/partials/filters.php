<?php
/**
 * @var array $types
 * @var array $modelOptions
 * @var array $statusOptions
 * @var array $servicePinOptions
 */
echo Form::open(['class' => 'form-horizontal margin-bottom filter-form', 'method' => 'GET']); ?>
<div class="rowe">
    <div class="form-group">
        <div class="col-md-2">
            <?php echo Form::label('name', trans('service_profile.columns.name')); ?>
            <?php echo Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => trans('service_profile.columns.name')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('type', trans('service_profile.columns.type')); ?>
            <?php echo Form::select('type', $types, request('type'), ['class' => 'form-control', 'placeholder' => trans('service_profile.type.all')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('model', trans('service_profile.columns.model')); ?>
            <?php echo Form::select('model', $modelOptions, request('model'), ['class' => 'form-control', 'placeholder' => trans('service_profile.model.all')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('duration', trans('service_profile.filters.duration')) ?>
            <div class="input-group input-group-with-select">
                <?php echo Form::number('duration', request('duration'), ['class' => 'form-control', 'placeholder' => trans('service_profile.filters.duration'), 'min' => 0]) ?>
                <?php echo Form::select('duration_operator', $durationOperators, request('duration_operator'), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('service_pin', trans('service_profile.columns.service_pin')) ?>
            <?php echo Form::select('service_pin', $servicePinOptions, request('service_pin'), ['class' => 'form-control', 'placeholder' => trans('service_profile.service_pin.all')]) ?>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-2">
            <?php echo Form::label('status', trans('service_profile.columns.status')) ?>
            <?php echo Form::select('status', $statusOptions, request('status'), ['class' => 'form-control', 'placeholder' => trans('messages.all')]) ?>
        </div>

        <div class="col-md-2">
            <?php echo Form::label('expiry_date', trans('service_profile.columns.expiry_date')) ?>
            <?php echo Form::select('expiry_date', date_options(), request('expiry_date'), ['class' => 'form-control select date-range-select']) ?>
        </div>

        <div class="col-md-4">
            <label for="">&nbsp;</label>
            <div class="input-daterange input-group <?php if (request('date') != \App\Date::RANGE_INTERVAL) echo 'hidden' ?>" id="date-range-picker">
                <?php echo Form::text('start_date', request('start_date', date('Y-m-d')), ['class' => 'form-control']) ?>
                <span class="input-group-addon">to</span>
                <?php echo Form::text('end_date', request('end_date', date('Y-m-d')), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-md-2">
            <a href="#" class="btn btn-default btn-reset" data-tooltip="true" title="<?php echo trans('messages.clear_form') ?>"><i class="fa fa-times"></i></a>
            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.search') ?>"><i class="fa fa-search"></i></button>
        </div>

    </div>
</div>
<?php echo Form::close(); ?>
