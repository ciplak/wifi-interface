<?php
/**
 * @var \App\Models\ServiceProfile[]|\Illuminate\Database\Eloquent\Collection $models
 */
$partial = !isset($partial) ? false : $partial;
?>
<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <?php if ($partial):?><th><?php echo Form::checkbox('check_all', 1, null, ['class' => 'check-all'])?></th><?php endif?>
            <th><?php echo trans('service_profile.columns.name') ?></th>
            <th><?php echo trans('service_profile.columns.type') ?></th>
            <th><?php echo trans('service_profile.columns.model') ?></th>
            <th><?php echo trans('service_profile.columns.duration') ?></th>
            <th><?php echo trans('service_profile.columns.expiry_date') ?></th>
            <th><?php echo trans('service_profile.columns.service_pin') ?></th>
            <th><?php echo trans('service_profile.columns.max_download_bandwidth') ?></th>
            <th><?php echo trans('service_profile.columns.max_upload_bandwidth') ?></th>
            <th class="col-bool"><?php echo trans('messages.status') ?></th>
            <?php if (!$partial):?><th class="actions"></th><?php endif?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($models as $model): ?>
            <tr<?php if ($model->is_expired) echo ' class="danger"' ?>>
                <?php if ($partial):?><td><?php echo Form::checkbox("service_profile_id[{$model->id}]", $model->id, null, ['class' => 'cb-service-profile', 'data-name' => $model->name]) ?></td><?php endif?>
                <td><?php echo \Entrust::can('service-profiles.manage') ? HTML::linkRoute('admin.service_profile.edit', $model->name, [$model]) : $model->name ?></td>
                <td><?php echo $model->type_value ?></td>
                <td><?php echo $model->model_name, $model->price_html ?></td>
                <td><?php echo $model->duration_value ?></td>
                <td>
                    <?php echo $model->expiry_date ?>
                    <?php if ($model->is_expired): printf('<span class="label label-danger">%s</span>', trans('service_profile.expired')); endif ?>
                </td>
                <td><?php echo $model->service_pin_html ?></td>
                <td><?php if ($model->max_download_bandwidth) echo $model->max_download_bandwidth . ' kbps' ?></td>
                <td><?php if ($model->max_upload_bandwidth ) echo $model->max_upload_bandwidth  . ' kbps' ?></td>
                <td><?php echo $model->status_icon ?></td>
                <?php if (!$partial):?>
                <td class="col-actions">
                    <a href="<?php echo route('admin.service_profile.delete', [$model]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
                <?php endif?>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>