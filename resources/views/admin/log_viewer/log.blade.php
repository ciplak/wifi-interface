<div class="row">
    <div class="col-sm-3 col-md-2 sidebar">
        <ul class="list-group">
            @foreach($files as $file)
                <li class="list-group-item @if ($current_file == $file) active @endif">
                    <a href="?l={{ base64_encode($file) }}">
                        {{$file}}

                    </a>

                    <span class="pull-right">
                        <a href="?del={{ base64_encode($current_file) }}" class="btn btn-danger btn-xs delete-log"><i class="fa fa-trash-o"></i></a>
                    </span>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="col-sm-9 col-md-10">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="table-container">
                    @if ($logs === null)
                        <div>
                            Log file >50M, please download it.
                        </div>
                    @else
                        @if (count($logs))
                        <div class="margin-bottom">
                            <a href="?dl={{ base64_encode($current_file) }}" class="btn btn-default"><i class="fa fa-download"></i> Download file</a>
                            -
                            <a id="delete-log" href="?del={{ base64_encode($current_file) }}" class="btn btn-danger delete-log"><i class="fa fa-trash-o"></i> Delete file</a>
                        </div>
                        @endif

                        <table id="table-log" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Level</th>
                                <th>Date</th>
                                <th>Content</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($logs as $key => $log)
                                <tr>
                                    <td class="text-{{{$log['level_class']}}}"><span class="glyphicon glyphicon-{{{$log['level_img']}}}-sign" aria-hidden="true"></span> &nbsp;{{$log['level']}}</td>
                                    <td class="date">{{{$log['date']}}}</td>
                                    <td class="text">
                                        @if ($log['stack']) <a class="pull-right expand btn btn-default btn-xs" data-display="stack{{{$key}}}"><span class="glyphicon glyphicon-search"></span></a>@endif
                                        {{{$log['text']}}}
                                        @if (isset($log['parameters'])) <br><br><strong>Parameters:</strong><br><pre>{!! $log['parameters'] !!}</pre>@endif
                                        @if (isset($log['in_file'])) <br />{{{$log['in_file']}}}@endif
                                        @if ($log['stack']) <div class="stack" id="stack{{{$key}}}" style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}</div>@endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                        @if (count($logs))
                        <div>
                            <a href="?dl={{ base64_encode($current_file) }}" class="btn btn-default"><i class="fa fa-download"></i> Download file</a>
                            -
                            <a href="?del={{ base64_encode($current_file) }}" class="btn btn-danger delete-log"><i class="fa fa-trash-o"></i> Delete file</a>
                        </div>
                        @endif

                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php echo HTML::script('assets/js/datatables.min.js') ?>
<?php echo HTML::style('assets/css/datatables.min.css') ?>

<script>
    $(document).ready(function(){
        $('#table-log').DataTable({
            "order": [ 1, 'desc' ],
            "stateSave": true,
            "stateSaveCallback": function (settings, data) {
                window.localStorage.setItem("datatable", JSON.stringify(data));
            },
            "stateLoadCallback": function (settings) {
                var data = JSON.parse(window.localStorage.getItem("datatable"));
                if (data) data.start = 0;
                return data;
            }
        });
        $('.table-container').on('click', '.expand', function(){
            $('#' + $(this).data('display')).toggle();
        });
        $('.delete-log').click(function(){
            return confirm('Are you sure?');
        });
    });
</script>
