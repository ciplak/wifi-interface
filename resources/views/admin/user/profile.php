<user-profile-form inline-template>

    <form class="form-horizontal">
        <div class="panel panel-default panel-actions">

            <div class="panel-heading">
                <h4 class="panel-title"><?php echo trans('user.profile') ?></h4>

                <div class="panel-control">
                    <button :disabled="form.busy" @click="updateProfile" class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>">
                    <span v-if="form.busy">
                        <i class="fa fa-refresh fa-spin"></i> Updating...
                    </span>
                        <i v-else class="fa fa-check"></i>
                    </button>
                </div>

            </div>

            <div class="panel-body">

                <div class="form-group" :class="{'has-error': form.errors.has('name')}">
                    <label for="name" class="control-label col-md-2 required"><?php echo trans('user.columns.name')?></label>
                    <div class="col-md-8">
                        <input type="text" name="name" id="name" class="form-control" autofocus v-model="form.name"/>
                        <span class="help-block">{{ form.errors.get('name') }}</span>
                    </div>
                </div>

                <div class="form-group" :class="{'has-error': form.errors.has('email')}">
                    <label for="email" class="control-label col-md-2 required"><?php echo trans('user.columns.email')?></label>
                    <div class="col-md-8">
                        <input type="text" name="email" id="email" class="form-control" v-model="form.email"/>
                        <span class="help-block">{{ form.errors.get('email') }}</span>
                    </div>
                </div>

                <div v-if="changePassword">
                    <div class="form-group" :class="{'has-error': form.errors.has('password')}">
                        <label for="password" class="control-label col-md-2 required"><?php echo trans('user.password')?></label>
                        <div class="col-md-8">
                            <input type="password" name="password" id="password" class="form-control" v-model="form.password"/>
                            <span class="help-block">{{ form.errors.get('password') }}</span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': form.errors.has('password_confirmation')}">
                        <label for="password_confirmation" class="control-label col-md-2 required"><?php echo trans('user.password_confirmation')?></label>
                        <div class="col-md-8">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" v-model="form.password_confirmation"/>
                            <span class="help-block">{{ form.errors.get('password_confirmation') }}</span>

                        </div>
                    </div>
                </div>
                <div v-else>
                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-2">
                            <a @click="changePassword = true" href="#" class="btn btn-danger"><?php echo trans('user.change_password')?></a>
                        </div>
                    </div>
                </div>

          </div><!-- /panel-body -->

        </div><!-- /panel -->
    </form>

</user-profile-form>


