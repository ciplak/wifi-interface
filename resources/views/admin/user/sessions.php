<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('user.sessions') ?></h4>

    </div>

    <div class="panel-body">
        <?php
        /**
         * @var \App\Models\User $user
         * @var \Illuminate\Support\Collection $sessions
         */
        if ($sessions->count()): ?>
            <table class="table">
                <thead>
                <tr>
                    <th><?php echo trans('user.session.ip_address') ?></th>
                    <th><?php echo trans('user.session.user_agent') ?></th>
                    <th><?php echo trans('user.session.last_activity') ?></th>
                    <th class="col-actions"></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($sessions as $session): ?>
                    <tr>
                        <td><?php echo $session->ip_address ?></td>
                        <td><?php echo $session->user_agent ?></td>
                        <td><?php echo $session->last_activity ?></td>
                        <td class="col-actions"><a href="<?php echo route('admin.user.invalidateSession', [$user, $session->id]) ?>" class="btn btn-danger btn-sm btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p class="alert alert-warning"><?php echo trans('user.session.not_found') ?></p>
        <?php endif ?>
    </div>
</div>
