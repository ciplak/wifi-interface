<?php
/**
 * @var \App\Models\User[]|\Illuminate\Support\MessageBag $models
 */
?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('user.page_title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.user.add') ?>" class="btn btn-success" data-tooltip="true" title="<?php echo trans('messages.add') ?>"><i class="fa fa-plus"></i></a>
        </div>

    </div>

    <div class="panel-body">

        <div class="table-responsive">

            <?php if ($models->isEmpty()): ?>

                <p class="alert alert-warning"><?php echo trans('guest.not_found') ?></p>

            <?php else: ?>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo trans('user.columns.name') ?></th>
                    <th><?php echo trans('user.columns.email') ?></th>
                    <th><?php echo trans('user.columns.role') ?></th>
                    <th><?php echo trans('user.columns.tenant') ?></th>
                    <th><?php echo trans('user.columns.location') ?></th>
                    <th><?php echo trans('user.columns.status') ?></th>
                    <th class="col-actions"></th>
                </tr>
                </thead>
                <tbody id="user-table-body">

                <?php
                foreach ($models as $model): ?>
                    <tr>
                        <td><?php echo HTML::linkRoute('admin.user.edit', $model->name, [$model]) ?></td>
                        <td><?php echo HTML::mailto($model->email);  ?></td>
                        <td><?php echo $model->role_display_name ?></td>
                        <td><?php echo $model->tenant_name ?></td>
                        <td><?php echo $model->location_name ?></td>
                        <td><?php echo $model->status_text ?></td>
                        <td class="col-actions">
                            <?php if (\Entrust::can('users.impersonate') && !$model->hasRole('developer')): ?>
                            <a href="<?php echo route('admin.impersonate', [$model->id]) ?>" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('user.impersonate') ?>">
                                <i class="fa fa-user-secret"></i>
                            </a>
                            <?php endif ?>

                            <?php if (\Entrust::can('user.ban') && !$model->is_banned): ?>
                            <a href="<?php echo route('admin.user.ban', [$model]) ?>" class="btn btn-default btn-sm" data-tooltip="true" title="<?php echo trans('user.ban') ?>">
                                <i class="fa fa-user-times"></i>
                            </a>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach ?>

                </tbody>
            </table>

            <?php pagination($models) ?>

            <?php endif ?>

        </div><!-- /table-responsive -->

    </div><!-- /panel-body -->

</div><!-- /panel -->