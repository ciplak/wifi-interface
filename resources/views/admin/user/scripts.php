<script>
$(function() {
    $('#btn-change-password').click(function() {
        $(this).hide();
        $('#password-fields').removeClass('hidden');
    });

    var groups = $('#role'),
        properties = $('#property-container');

    groups.change(function() {
        var val = $(this).val();
        if (val == 'venue' || val == 'front-desk') {
            properties.removeClass('hidden');
        } else {
            properties.addClass('hidden');
        }
    });

    groups.trigger('change');
});
</script>