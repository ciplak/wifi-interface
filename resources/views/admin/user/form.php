<?php
/**
 * @var \Illuminate\Support\MessageBag $errors
 * @var \App\Models\User|\Illuminate\Database\Eloquent\Collection $model
 * @var array $roles
 * @var array $statuses
 * @var array $properties
 * @var array $localeOptions
 * @var array $statusOptions
 * @var \Illuminate\Support\Collection $sessions
 */
$passwordError = false;
echo Form::model($model, ['class' => 'form-horizontal', 'autocomplete' => 'off', 'route' => ['admin.user.save', $model]]) ?>
<div class="panel panel-default panel-actions">

    <div class="panel-heading">
        <h4 class="panel-title"><?php echo trans('user.title') ?></h4>

        <div class="panel-control">
            <a href="<?php echo route('admin.user.index') ?>" class="btn btn-default" data-tooltip="true" title="<?php echo trans('messages.go_back') ?>"><i class="fa fa-mail-reply"></i> </a>
            <?php if ($model->exists && $model->id != Auth::id()): ?>
                <a href="<?php echo route('admin.user.delete', ['user' => $model]) ?>" class="btn btn-danger btn-delete" data-tooltip="true" title="<?php echo trans('messages.delete') ?>"><i class="fa fa-trash-o"></i></a>
            <?php endif ?>
            <button class="btn btn-primary" type="submit" data-tooltip="true" title="<?php echo trans('messages.save') ?>"><i class="fa fa-check"></i></button>
        </div>

    </div>

    <div class="panel-body">

        <?php if ($errors->count()): ?>
            <?php $passwordError = $errors->has('password') ?>
            <div class="alert alert-danger">
                <?php echo implode('<br>', $errors->all())?>
            </div>
        <?php endif ?>

        <div class="form-group<?php if ($errors->has('name')) echo ' has-error' ?>">
            <?php echo Form::label('name', trans('user.columns.name'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::text('name', null, [
                    'class' => 'form-control',
                    'required',
                    'minlength' => '5',
                    'placeholder' => trans('user.columns.name'),
                    'autofocus'
                ]) ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('email')) echo ' has-error' ?>">
            <?php echo Form::label('email', trans('user.columns.email'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-4">
                <?php echo Form::input('email', 'email', null, ['class' => 'form-control', 'required', 'placeholder' => trans('user.columns.email')]) ?>
            </div>
        </div>

        <div class="form-group<?php if ($errors->has('role')) echo ' has-error' ?>">
            <?php echo Form::label('role', trans('user.columns.role'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::select('role', $roles, null, ['class' => 'form-control', 'placeholder' => trans('messages.select')]) ?>
            </div>

            <div class="col-md-2 hidden" id="property-container">
                <?php echo Form::select('property_id', $properties, null, ['class' => 'form-control', 'placeholder' => trans('venue.select')])?>
            </div>
        </div>

        <?php if (\Entrust::hasRole('developer')): ?>
        <div class="form-group<?php if ($errors->has('locale')) echo ' has-error' ?>">
            <?php echo Form::label('locale', trans('user.columns.locale'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::select('locale', $localeOptions, null, ['class' => 'form-control', 'placeholder' => trans('user.columns.locale')])?>
            </div>
        </div>
        
            <div class="form-group<?php if ($errors->has('status')) echo ' has-error' ?>">
            <?php echo Form::label('status', trans('user.columns.status'), ['class' => 'control-label col-md-2 required']) ?>
            <div class="col-md-2">
                <?php echo Form::select('status', $statusOptions, null, ['class' => 'form-control'])?>
            </div>
        </div>
        <?php endif ?>

        <div <?php if (!$passwordError && $model->exists): ?>class="hidden"<?php endif ?> id="password-fields">
            <div class="form-group">
                <?php echo Form::label('password', trans('user.password'), ['class' => 'control-label col-md-2']) ?>
                <div class="col-md-2">
                    <?php echo Form::password('password', ['class' => 'form-control'])?>
                </div>
            </div>

            <div class="form-group">
                <?php echo Form::label('password_confirmation', trans('user.password_confirmation'), ['class' => 'control-label col-md-2']) ?>
                <div class="col-md-2">
                    <?php echo Form::password('password_confirmation', ['class' => 'form-control'])?>
                </div>
            </div>
        </div>

        <?php if (!$passwordError && $model->exists): ?>
        <div class="form-group">
            <div class="col-md-2 col-md-offset-2">
                <a href="#" class="btn btn-danger" id="btn-change-password"><?php echo trans('user.change_password') ?></a>
            </div>
        </div>
        <?php endif ?>

    </div><!-- /panel-body -->

</div><!-- /panel -->
<?php echo Form::close() ?>

<?php if ($sessions->count()) echo view('admin.user.sessions', ['user' => $model, 'sessions' => $sessions]) ?>