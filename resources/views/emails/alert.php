<?php
/**
 * @var Swift_Message $message
 * @var \App\Models\AlertMessage $alertMessage
 */
if (!isset($includeDetails)) {
    $includeDetails = false;
}
?>
<body>
<p><?php echo $alertMessage->message ?></p>
<p>
    <?php
    if ($includeDetails):
        echo '<strong>Alert messaged id: </strong>', $alertMessage->id, '<hr>';
        $parameters = json_decode($alertMessage->description, true);
        if ($parameters):
            echo '<pre>', jsonPrettify($alertMessage->description), '</pre>';
        endif;
    endif;
    ?>
</p>
<?php /*if ($logo = setting('logo')): ?>
    <hr>
    <img src="<?php echo $message->embed(env('FILESYSTEM_DISK_ROOT') . 'img/' . $logo) ?>" alt="">
<?php endif /**/?>
</body>