<?php

return array (
  'columns' => 
  array (
    'name' => 'İsim',
  ),
  'not_found' => 'Cihaz sağlayıcı bulunamadı',
  'page_title' => 'Cihaz sağlayıcıları ve modeller',
  'saved' => 'Cihaz sağlayıcı kaydedildi',
  'tab_title' => 'Cihaz Sağlayıcıları',
);
