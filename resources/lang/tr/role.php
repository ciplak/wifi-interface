<?php

return array (
  'columns' => 
  array (
    'description' => 'Açıklama',
    'display_name' => 'Görünür isim',
    'name' => 'Adı',
  ),
  'not_found' => 'Rol bulunamadı',
  'page_title' => 'Roller',
  'saved' => 'Rol kaydedildi',
);