<?php

return [
    'all'                       => '(Tüm Cihazlar)',
    'bulk_import'               =>
        [
            'click_to_download_sample_file' => 'Örnek dosyayı indirmek için buraya tıklayın',
            'description'                   => 'CSV, XLS veya XLSX dosyası olmalıdır',
            'page_title'                    => 'Toplu yükleme',
        ],
    'columns'                   =>
        [
            'description' => 'Açıklama',
            'ip'          => 'IP Adresi',
            'location'    => 'Lokasyon',
            'mac'         => 'MAC Adresi',
            'model'       => 'Cihaz Modeli',
            'name'        => 'İsim',
        ],
    'delete'                    =>
        [
            'restore_item' => 'Bu cihazı yeniden yükle',
        ],
    'deleted'                   => 'Silindi',
    'deleted_options'           =>
        [
            'count' => ':count cihaz silindi',
        ],
    'not_deleted'               => 'Cihaz silinemedi',
    'dont_restore'              => 'Yeniden yükleme',
    'file_not_uploaded'         => 'Dosya yüklenemedi',
    'hotspot'                   => 'Kablosuz Bağlantı Noktası',
    'import_file_empty'         => 'Yüklenen dosya boş',
    'imported'                  => ':count cihaz yüklendi',
    'invalid_file'              => 'Geçersiz dosya. CSV, XLS veya XLSX dosyası olmak zorunda',
    'location'                  =>
        [
            'all'    => '(Tüm Lokasyonlar)',
            'select' => '(Lokasyon Seç)',
        ],
    'model'                     =>
        [
            'all' => '(Tüm Cihaz Modelleri)',
        ],
    'not_found'                 => 'Cihaz bulunmadı',
    'not_imported'              => ':count cihaz yüklenemedi',
    'not_restored'              => 'Cihaz yeniden yüklenemedi',
    'restored'                  => 'Cihaz yeniden yüklendi',
    'page_title'                => 'Cihazlar',
    'parent_device'             => 'Üst Cihaz',
    'restore'                   => 'Yeniden Yükle',
    'restored_options'          =>
        [
            'count' => ':count cihaz yeniden yüklendi',
        ],
    'saved'                     => 'Cihaz kaydedildi',
    'select_devices_to_delete'  => 'Silinecek cihazları seçin',
    'select_devices_to_restore' => 'Yeniden yüklenecek cihazları seçin',
    'select_model'              => '(Model Seçin)',
    'status'                    =>
        [
            'active'  => 'Aktif',
            'all'     => '(Tüm Durumlar)',
            'deleted' => 'Silindi',
        ],
    'sync_error'                => 'Senkronize edilemedi. Lütfen sistem yetkilisi ile iletişime geçin.',
];
