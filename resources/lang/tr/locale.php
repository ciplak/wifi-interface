<?php

return [
    'options' => [
        'en' => 'İngilizce',
        'ar' => 'Arapça',
        'tr' => 'Türkçe',
    ],
];