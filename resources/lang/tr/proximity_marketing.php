<?php

return array (
  'columns' => 
  array (
    'age' => 'Yaş Grubu',
    'communication_channel' => 'Haberleşme Kanalı',
    'event_delay' => 'Gecikme',
    'frequency' => 'Frekans',
    'parameter_type' => 'Parametre Türü',
    'status' => 'Durum',
    'title' => 'Başlık',
    'event_type' => 'Olay Türü',
    'date' => 'Çalışma Saati',
    'location_id' => 'Lokasyon',
    'not_found' => 'Kayıt bulunamadı',
  ),
  'not_found' => 'Hiç kayıt bulunamadı',
  'saved' => 'Kaydedildi',
  'status' => 
  array (
    'draft' => 'Taslak',
    'published' => 'Yayınlandı',
  ),
  'page_title' => 'Proximity pazarlama',
);
