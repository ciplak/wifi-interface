<?php

return array (
  'age' => 
  array (
    'group' => 
    array (
      'all' => '(Tüm Yaş Grupları)',
    ),
  ),
  'columns' => 
  array (
    'age' => 'Yaş',
    'birthday' => 'Doğum Günü',
    'connection_status' => 'Bağlantı Durumu',
    'direct_login' => 'Doğrudan Giriş',
    'email' => 'E-Posta',
    'gender' => 'Cinsiyet',
    'last_visit' => 'Son Giriş',
    'location' => 'Lokasyon',
    'name' => 'İsim',
    'nationality' => 'Uyruk',
    'phone' => 'Telefon',
    'total_visits' => 'Toplam Ziyaret',
    'username' => 'Kullanıcı Adı',
    'passport_no' => 'Pasaport No'
  ),
  'connection_status' => 
  array (
    'all' => '(Tüm Bağlantı Durumları)',
    'offline' => 'Çevrimdışı',
    'online' => 'Çevrimiçi',
  ),
  'disconnect' => 'Misafiri çevrimdışı yap',
  'disconnected' => 'Misafir çevrimdışı',
  'full_name' => 'Ad Soyad',
  'gender' => 
  array (
    'all' => '(Tüm Cinsiyetler)',
    'select' => '(Cinsiyet Seçin)',
    'unknown' => 'Bilinmeyen',
    'male' => 'Erkek',
    'female' => 'Kadın',
  ),
  'info' => 'Misafir Bilgisi',
  'nationality' => 
  array (
    'all' => '(Tüm Uyruklar)',
    'select' => '(Uyruk Seçin)',
  ),
  'not_disconnected' => 'Misafir çevrimdışı yapılamadı',
  'not_found' => 'Misafir bulunamadı',
  'page_title' => 'Misafirler',
  'saved' => 'Misafir kaydedildi',
  'session_not_found' => 'Oturum bulunamadı',
  'sessions' => 'Oturumlar',
  'social_network' => 'Sosyal Ağlar',
  'title' => 'Misafir',
  'access_code_generated' => 'Yeni erişim kodu oluşturuldu'
);