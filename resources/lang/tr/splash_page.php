<?php

return array (
  'access_code' => 'Erişim kodu',
  'columns' => 
  array (
    'name' => 'Sayfa Adı',
    'type' => 'Türü',
  ),
  'custom_css' => 'CSS',
  'custom_field_no' => 'Özel alan :no',
  'custom_field_no_title' => 'Özel alan :no başlığı',
  'custom_html' => 'HTML',
  'custom_js' => 'Javascript',
  'deleted' => 'Sayfa silindi',
  'exit_preview' => 'Önizlemeden çık',
  'images' => 'Resimler',
  'in_use' => 'Sayfa kullanımda olduğu için silinemedi',
  'login_form' => 'Giriş Formu',
  'login_options' => 
  array (
    'button_label' => 
    array (
      'required' => ':language dili için :login_type düğmesi yazısı gereklidir',
    ),
  ),
  'not_deleted' => 'Sayfa silinemedi',
  'not_found' => 'Sayfa bulunamadı',
  'not_saved' => 'Sayfa kaydedilemedi',
  'options' => 'Seçenekler',
  'page_title' => 'Karşılama Sayfaları',
  'preview_warning' => 'Sayfa önizleme modunda',
  'publish_settings' => 'Yayınlama ayarları',
  'registration' => 'Kaydolma',
  'saved' => 'Sayfa kaydedildi',
  'send_email' => 'E-posta gönder',
  'send_sms' => 'SMS gönder',
  'social_login_requires_code' => 'Sosyal ağ ile giriş kod gerektirsi',
  'type' => 
  array (
    'offline' => 'Bağlantı öncesi sayfa',
    'online' => 'Bağlantı sonrası sayfa',
  ),
  'publish_settings_description' => 'Sayfayı yayınlamak istediğiniz lokasyonu seçin',
);
