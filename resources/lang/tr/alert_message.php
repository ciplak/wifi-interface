<?php

return array (
  'columns' => 
  array (
    'date' => 'Tarih',
    'description' => 'Açıklama',
    'message' => 'Mesaj',
    'priority' => 'Öncelik',
  ),
  'not_found' => 'Uyarı mesajı bulunamadı',
  'page_title' => 'Uyarı mesajları',
  'priority' => 
  array (
    'all' => '(Tüm Öncelikler)',
    'high' => 'Yüksek',
    'low' => 'Düşük',
    'medium' => 'Orta',
  ),
  'title' => 'Uyarı Mesajı',
  'view_all' => 'Tüm uyarıları göster',
);
