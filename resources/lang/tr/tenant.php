<?php

return array (
  'columns' => 
  array (
    'description' => 'Açıklama',
    'info' => 'Bilgi',
    'name' => 'Adı',
    'id' => 'Id',
  ),
  'not_found' => 'Kayıt bulunamadı',
  'page_title' => 'Tenants',
  'saved' => 'Kaydedildi',
  'title' => 'Tenant',
  'sms_credits' => 
  array (
    'title' => 'SMS Kredisi',
    'ref_no' => 'Ref. No',
    'credits' => 'Kredi',
    'description' => 'Açıklama',
    'date' => 'Tarih',
    'install' => 'Kredi Yükle',
    'installed' => ':count kredi yüklendi',
    'deleted' => 'SMS kredisi silindi',
    'not deleted' => 'SMS kredisi silinemedi',
    'total' => 'Total SMS kredisi',
    'average_daily_sms_usage' => 'Ortalama günlük SMS kullanımı',
    'not_installed' => 'SMS kredisi yüklenemedi',
    'not_found' => 'SMS kredisi bulunamadı',
  ),
);
