<?php

return array (
  'columns' => 
  array (
    'handler' => 'Cihaz İşleyici',
    'name' => 'Model ismi',
    'type' => 'Tür',
    'vendor' => 'Sağlayıcı',
  ),
  'not_found' => 'Cihaz modeli bulunamadı',
  'page_title' => 'Cihaz Modelleri',
  'saved' => 'Cihaz modeli kaydedildi',
  'tab_title' => 'Cihaz Modelleri',
  'type' => 
  array (
    'access_point' => 'Erişim Noktası',
    'select' => '(Tür Seçin)',
    'wlc' => 'WLC',
  ),
);
