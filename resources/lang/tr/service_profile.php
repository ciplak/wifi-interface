<?php

return array (
  'columns' => 
  array (
    'description' => 'Açıkalam',
    'duration' => 'Süre',
    'expiry_date' => 'SKT',
    'max_download_bandwidth' => 'Max. İndirme Hızı',
    'max_upload_bandwidth' => 'Max. Yükleme Hızı',
    'model' => 'Model',
    'name' => 'İsim',
    'price' => 'Fiyat',
    'type' => 'Tür',
  ),
  'deleted' => 'Internet hizmet planı silindi',
  'expired' => 'Süresi dolmuş',
  'model' => 
  array (
    'free_of_charge' => 'Ücretsiz',
    'paid' => 'Ücretli',
  ),
  'not_deleted' => 
  array (
    'in_use' => 'Internet hizmet planı kullanımda olduğu için silinemedi',
  ),
  'not_found' => 'Internet hizmet planı bulunamadı',
  'not_saved' => 'Internet hizmet planı kaydedilemedi',
  'page_title' => 'Internet Hizmet Planları ',
  'saved' => 'Internet hizmet planı kaydedildi',
  'select' => '(Internet Hizmet Planı Seçin)',
  'service_pin' => 
  array (
    'no' => 'Hayır',
    'tooltip' => 'Eğer aktive edildiyse kaydolduktan sonra erişim kodu gerekecektir',
    'yes' => 'Evet',
  ),
  'title' => 'Internet Hizmet Planı ',
  'type' => 
  array (
    'one_time' => 'Tek seferlik',
    'recurring' => 'Tekrar eden',
    'unrestricted' => 'Kısıtlamasız',
  ),
);
