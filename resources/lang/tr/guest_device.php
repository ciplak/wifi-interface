<?php

return array (
  'columns' => 
  array (
    'browser' => 'Tarayıcı',
    'description' => 'Cihaz',
    'platform' => 'Platform',
    'type' => 'Cihaz Türü',
    'wifi_area' => 'WiFi Alanı',
  ),
  'type' => 
  array (
    'desktop' => 'Masaüstü',
    'mobile' => 'Mobil',
    'tablet' => 'Tablet',
    'undefined' => 'Tanımsız',
  ),
);
