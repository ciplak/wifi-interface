<?php

return array (
    'page_title'        => 'Panel',
    'max_online_guests' => 'Max. bağlı',
    'number_of_logins'  => 'Bağlanma sayısı',
    'impressions'       => 'İzlenim',
    'new_logins'        => 'Yeni bağlanma',
    'total_traffic'     => 'Toplam Trafik',
);
