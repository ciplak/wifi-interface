<?php

return array (
  'columns' => 
  array (
    'body' => 'Metin',
    'date' => 'Tarih',
    'end_date' => 'Bitiş Tarihi',
    'image' => 'Resim',
    'start_date' => 'Başlangıç Tarihi',
    'status' => 'Durum',
    'title' => 'Başlık',
    'type' => 'Tür',
    'url' => 'URL',
  ),
  'type' => [
      'text' => 'Metin',
      'image' => 'Resim',
  ],
  'not_found' => 'Reklam bulunamadı',
  'not_saved' => 'Reklam kaydedilemedi',
  'saved' => 'Reklam kaydedildi',
  'status' => 
  array (
    'draft' => 'Taslak',
    'published' => 'Yayınlandı',
  ),
  'select_to_insert' => 'Lütfen eklenecek reklamı seçin',
  'page_title' => 'Karşılama sayfası reklamları',
);
