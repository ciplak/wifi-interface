<?php

return array (
  'columns' => 
  array (
    'description' => 'Açıklama',
    'name' => 'SMS Sağlayıcı Adı',
    'gateway' => 'SMS geçidi',
  ),
  'deleted' => 'SMS sağlayıcı silindi',
  'gateway' => 
  array (
    'accinge' => 
    array (
      'password' => 'Parola',
      'sender_id' => 'Göndern ID',
      'username' => 'Kullanıcı Adı',
    ),
    'clickatell' => 
    array (
      'api_id' => 'API ID',
      'auth_token' => 'Auth Token',
      'password' => 'Parola',
      'username' => 'Kullanıcı adı',
    ),
    'ISmartSms' => 
    array (
      'password' => 'Parola',
      'username' => 'Kullanıcı adı',
    ),
    'twilio' => 
    array (
      'account_sid' => 'Hesap SID',
      'auth_token' => 'Auth Token',
      'from' => 'Kimden',
    ),
  ),
  'not_deleted' => 'Sms sağlayıcı silinemedi',
  'not_found' => 'SMS sağlayıcı bulunamadı',
  'not_saved' => 'SMS sağlayıcı kaydedilemedi',
  'page_title' => 'SMS Sağlayıcılar',
  'saved' => 'SMS sağlayıcı kaydedildi',
  'title' => 'SMS Sağlayıcı',
);
