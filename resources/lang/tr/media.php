<?php

return array (
  'upload_files' => 'Dosya yükle',
  'insert_media' => 'Resim ekle',
  'media_library' => 'Resim kütüphanesi',
  'title' => 'Resimler',
  'media_library_empty' => 'Resim kütüphanesi boş',
  'insert_image_tag'          => 'Resim etiketi ekle',
  'insert_image_url'          => 'Resim URL\'si ekle',
  'select_file'               => 'Dosya seçin',
  'max_upload_file_size'      => 'Maksimum yüklenebilecek dosya boyutu: :size',
  'drop_files_here_to_upload' => 'Dosya yüklemek için buraya sürükleyin',
  'or'                        => 'veya',
);
