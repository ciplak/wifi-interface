<?php

return array (
  'could_not_create' => 'Hiç erişim kodu oluşturulamadı',
  'created' => ':count adet kod oluşturuldu',
  'deleted' => 'Erişim kodu silindi',
  'enter_code' => 'Lütfen erişim kodunu girin',
  'expired' => 'SKT Geçmiş',
  'generate' => 'Üret',
  'not_deleted' => 'Erişim kodu silinemedi',
  'not_expired' => 'Hala geçerli',
  'not_found' => 'Erişim kodu bulunamadı',
  'page_title' => 'Erişim Kodları',
  'quantity' => 'Miktar',
  'select_service_profile' => 'Lütfen internet hizmet planı seçin',
  'select_type' => 'Lütfen kod türü seçin',
  'select_venue' => 'Lütfen lokasyon seçin',
  'status' => 
  array (
    'all' => '(Tüm Durumlar)',
    'available' => 'Uygun',
    'used' => 'Kullanılmış',
  ),
  'type' => 
  array (
    'access_code' => 'Erişim Kodu',
    'all' => '(Tüm Türler)',
    'select' => '(Tür Seçin)',
    'sms' => 'SMS Kodu',
    'shared' => 'Ortak kod',
  ),
  'columns' => 
  array (
    'code' => 'Kod',
    'expiry_date' => 'SKT',
    'status' => 'Durum',
    'type' => 'Tür',
    'use_count' => 'Kullanım Sayısı',
    'location' => 'Lokasyon',
  ),
);
