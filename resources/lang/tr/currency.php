<?php

return array (
  'columns' => 
  array (
    'description' => 'Açıklama',
    'is_prefix' => 'Önek mi',
    'name' => 'İsim',
    'symbol' => 'Sembol',
  ),
  'deleted' => 'Para birimi silindi',
  'not_deleted' => 'Para birimi silinemedi',
  'not_found' => 'Para birimi bulunamadı',
  'not_saved' => 'Para birimi kaydedilemedi',
  'page_title' => 'Para birimleri',
  'saved' => 'Para birimi kaydedilemedi',
);
