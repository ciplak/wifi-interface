<?php

return array (
  'date_format' => 'Tarih biçimi',
  'email' => 
  array (
    'cc' => 'CC',
    'encryption' => 
    array (
      'none' => 'Yok',
      'ssl' => 'SSL',
      'tls' => 'TLS',
      'title' => 'Şifreleme',
    ),
    'from' => 
    array (
      'address' => 'Gönderen adresi',
      'name' => 'Gönderen adı',
    ),
    'host' => 'SMTP Adresi',
    'password' => 'Parola',
    'port' => 'SMTP Portu',
    'to' => 'Kime',
    'username' => 'Kullanıcı adı',
  ),
  'logo' => 'Logo',
  'page_title' => 'Ayarlar',
  'proxy' => 
  array (
    'host' => 'Proxy adresi',
    'password' => 'Proxy parolası',
    'username' => 'Proxy kullanıcı adı',
  ),
  'saved' => 'Ayarlar kaydedildi',
  'shared_device_count' => 'Aynı kodu kullanabilecek cihaz sayısı',
  'sms' => 
  array (
    'daily_limit' => 'Günlük SMS limiti',
    'from' => 'SMS gönderen adı',
    'warning_threshold' => 'SMS uyarı sınırı',
  ),
  'social' => 
  array (
    'facebook' => 
    array (
      'caption' => 'Bağlantı başlığı',
      'link' => 'Bağlantı',
      'message' => 'Mesaj',
      'place_id' => 'Yer ID',
    ),
  ),
  'start_of_week' => 'Hafta başlangıcı',
  'tabs' => 
  array (
    'advanced' => 'Gelişmiş',
    'email' => 'E-posta',
  ),
  'time_format' => 'Saat biçimi',
  'access_code' => 
  array (
    'expiry_period' => 'Son kullanım periyodu',
    'length' => 'Erişim kodu uzunluğu',
    'type' => 'Erişim kodu türü',
    'chars' => 'Erişim kodu karakterleri',
  ),
  'available_country_codes' => 
  array (
    'title' => 'Ülke kodları',
  ),
  'default_country_phone_code' => 
  array (
    'title' => 'Varsayılan ülke kodu',
  ),
  'timezone' => 
  array (
    'title' => 'Saat dilimi',
  ),
);
