<?php

return array (
  'columns' => 
  array (
    'age' => 'Yaş Grubu',
    'birthday' => 'Doğumgünü',
    'email_content' => 'E-posta içeriği',
    'location_id' => 'Lokasyon',
    'parameter_type' => 'Parametre Türü',
    'reengage' => 'Yeniden Ayarla',
    'sms_content' => 'SMS İçeriği',
    'status' => 'Durum',
    'title' => 'Başlık',
    'visit_count' => 'Üst Düzey Müşteriler',
  ),
  'communication_channel' => 'Haberleşme Kanalı',
  'date' => 'Yayınlanma Zamanı',
  'not_found' => 'Kampanya bulunamadı',
  'not_saved' => 'Kampanya kaydedilemedi',
  'page_title' => 'Kampanya',
  'periodic' => 'Dönemsel',
  'saved' => 'Kampanya kaydedildi',
  'status' => 
  array (
    'draft' => 'Taslak',
    'published' => 'Yayınlandı',
  ),
);
