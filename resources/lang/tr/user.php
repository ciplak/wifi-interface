<?php

return array (
  'change_password' => 'Şifre değiştir',
  'columns' => 
  array (
    'email' => 'E-Posta',
    'location' => 'Lokasyon',
    'name' => 'İsim',
    'role' => 'Rol',
      'locale' => 'Dil'
  ),
  'deleted' => 'Kullanıcı silindi',
  'not_deleted' => 'Kullanıcı silinemedi',
  'not_saved' => 'Kullanıcı kaydedilemedi',
  'page_title' => 'Kullanıcılar',
  'password' => 'Şifre',
  'password_confirmation' => 'Şifre doğrulama',
  'profile' => 'Profil',
  'profile_not_updated' => 'Profil güncellenemedi',
  'profile_updated' => 'Profil güncellendi',
  'saved' => 'Kullanıcı kaydedildi',
  'session' => 
  array (
    'ip_address' => 'IP adresi',
    'last_activity' => 'Son Aktiflik',
    'not_found' => 'Hiç oturum bulunamadı',
  ),
  'sessions' => 'Oturumlar',
  'title' => 'Kullanıcı',
  'impersonate' => 'Kullanıcı gibi davran',
  'stop_impersonating' => 'Kullanıcı gibi davranmayı bırak',
);
