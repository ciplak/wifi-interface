<?php

return array (
  'all' => '(Tüm Lokasyonlar)',
  'columns' => 
  array (
    'address' => 'Adres',
    'city' => 'Şehir',
    'country' => 'Ülke',
    'description' => 'Açıklama',
    'name' => 'İsim',
    'post_code' => 'Posta Kodu',
    'state' => 'Eyalet',
  ),
  'created' => 'Lokasyon oluşturuldu',
  'deleted' => 'Lokasyon silindi',
  'not_deleted' => 'Lokasyon silinemedi',
  'not_found' => 'Lokasyon bulunamadı',
  'not_saved' => 'Lokasyon kaydedilemedi',
  'page_title' => 'Lokasyonlar',
  'saved' => 'Lokasyon kaydedildi',
  'select' => '(Lokasyon Seçin)',
  'ssid_name' => 'SSID İsmi',
  'ssids' => 'SSID\'ler',
  'title' => 'Lokasyon',
  'manage_service_profiles' => 'Internet hizmet planlarını yönet',
  'validation'              => [
      'smart_login_frequency' => [
          'required_if' => 'Akıllı giriş aktive edildiğinde geçerli bir frekans belirtilmelidir',
      ],
  ],
);
