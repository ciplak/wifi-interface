<?php

return array (
  'page_title' => 'Device Models',
  'tab_title' => 'Device Models',
  'columns' => 
  array (
    'name' => 'Model name',
    'vendor' => 'Vendor',
    'type' => 'Type',
    'handler' => 'Device Handler',
  ),
  'type' => 
  array (
    'select' => '(Select Type)',
    'wlc' => 'WLC',
    'access_point' => 'Access Point',
  ),
  'saved' => 'Device model saved',
  'not_found' => 'Device model not found',
);
