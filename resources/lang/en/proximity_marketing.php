<?php

return array(
    'page_title' => 'Proximity Marketing',
    'columns'    =>
        array(
            'title'                   => 'Title',
            'weekdays'                => 'Weekdays',
            'location_id'             => 'Location',
            'date'                    => 'Running Time',
            'event_type'              => 'Event Type',
            'event_delay'             => 'Delay',
            'parameter_type'          => 'Parameter Type',
            'frequency'               => 'Frequency',
            'age'                     => 'Age Group',
            'communication_channel'   => 'Communication Channel',
            'not_found'               => 'Proximity Marketing Campaign not found',
            'status'                  => 'Status',
            'twitter'                 => 'Twiter',
            'push_notification'       => 'Push Notification',
            'sms'                     => 'SMS',
            'email'                   => 'E-mail',
            'users_fit_are_performed' => 'Number of end-users match with this campaign',
            'age_parameter'           => 'Age',
            'visit_based'             => 'Visit Based',
        ),
    'tooltip'    => array(
        'event_type'    => 'Detected: When device is detected before connecting to the WiFi Network SSID. This is possible with Cisco CMX or other selected networks;
        Connected: When device is connected to WiFi Network SSID;
        Exit : When device is exited the venue / zone or disconnected;
        ',
        'delay'         => 'In minutes (1 to 300) Number of minutes after the action is triggered. Example: 30 minutes after device is detected',
        'frequency'     => 'Determines the duration between messages pushed to the same end-user regardless of his behaviour and interaction in the venue. It applies to all parameter types (visit based or date-time based)',
        'visit_based_0' => 'Checks end-user’s interaction with Venue from the campaign running time',
        'visit_based_1' => 'Checks the historical number of visits',
    ),
    'status'     =>
        array(
            'published' => 'Published',
            'draft'     => 'Draft',
        ),
    'saved'      => 'Saved',
    'not_found'  => 'No record found',
);
