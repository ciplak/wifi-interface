<?php

return [
    'page_title'          => 'Reports',
    'guests'              => 'Guests',
    'guest_list'          => 'List',
    'guest_sessions'      => 'Sessions',
    'visited_urls'        => 'Visited URLs',
    'impressions'         => 'Impressions',
    'demographics'        => 'Demographics',
    'daily_activity'      => 'Daily Activity',
    'device_usage_shares' => 'Device Usage Shares',
    'device_platforms'    => 'Device Platforms',
    'device_browsers'     => 'Device Browsers',
    'device_vendors'      => 'Device Vendors',
    'online_guests'       => 'Online guests',
    'trend_analysis'      =>
        [
            'type'  =>
                [
                    'connections' => 'Connections',
                    'impressions' => 'Impressions',
                ],
            'title' => 'Trend Analysis',
        ],
    'loyalty'             =>
        [
            'categories' =>
                [
                    'one_time'       => 'One visit',
                    '5_10_visits'    => '5-10 visits',
                    '10_plus_visits' => '10+ visits',
                    '2_5_visits'     => '2-5 visits',
                ],
            'title'      => 'Loyalty',
        ],
    'presence'            =>
        [
            'average_sessions' => 'Average Sessions',
            'y_axis'           =>
                [
                    'title' => 'Duration (H:M:S)',
                ],
            'title'            => 'Presence',
        ],
];
