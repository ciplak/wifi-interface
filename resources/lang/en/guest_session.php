<?php

return [
    'filters' => [
        'uploaded_data'     => 'Uploaded (MB)',
        'downloaded_data'   => 'Downloaded (MB)',
        'duration'          => 'Duration (Minutes)',
        'date'              => 'Date',
        'name'              => 'Guest Name / Username',
        'mac'               => 'Guest MAC',
        'ip'                => 'Guest IP',
        'access_code'       => 'Access Code',
        'connection_status' => 'Connection Status',
    ],
];