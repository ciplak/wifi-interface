<?php

return array (
  'page_title' => 'Roles',
  'columns' => 
  array (
    'name' => 'Name',
    'display_name' => 'Display name',
    'description' => 'Description',
  ),
  'saved' => 'Role saved',
  'not_found' => 'Role not found',
);
