<?php

return array (
  'page_title' => 'Device vendors & models',
  'tab_title' => 'Device Vendors',
  'columns' => 
  array (
    'name' => 'Name',
  ),
  'saved' => 'Device vendor saved',
  'not_found' => 'Device vendor not found',
);
