<?php

return [
    'options' => [
        'en' => 'English',
        'ar' => 'Arabic',
        'tr' => 'Turkish'
    ],
];