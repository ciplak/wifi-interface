<?php
return array(
    'title'                   => 'Guest',
    'page_title'              => 'Guests',
    'columns'                 =>
        array(
            'location'          => 'Location',
            'name'              => 'Name',
            'username'          => 'Username',
            'mac'               => 'Mac',
            'ip'                => 'IP',
            'access_code'       => 'Access Code',
            'time'              => 'Time',
            'duration'          => 'Duration',
            'download'          => 'Download',
            'upload'            => 'Upload',
            'email'             => 'E-mail',
            'birthday'          => 'Birthday',
            'phone'             => 'Phone',
            'age'               => 'Age',
            'gender'            => 'Gender',
            'connection_status' => 'Connection Status',
            'last_visit'        => 'Last Visit',
            'total_visits'      => 'Total Visits',
            'direct_login'      => 'Direct Login',
            'nationality'       => 'Nationality',
            'passport_no'       => 'Passport No',

            'uploaded_data'      => 'Uploaded',
            'downloaded_data'    => 'Downloaded',
        ),
    'not_found'               => 'Guest not found',
    'guest_session_not_found' => 'Guest Session not found',
    'disconnect'              => 'Disconnect the guest',
    'disconnected'            => 'Guest disconnected',
    'not_disconnected'        => 'Could not disconnect the guest',
    'full_name'               => 'Full Name',
    'info'                    => 'Guest Info',
    'saved'                   => 'Guest saved',
    'connection_status'       =>
        array(
            'all'     => '(All Connection Statuses)',
            'offline' => 'Offline',
            'online'  => 'Online',
        ),
    'age'                     =>
        array(
            'group' =>
                array(
                    'all' => '(All Age Groups)',
                ),
        ),
    'gender'                  =>
        array(
            'all'     => '(All Genders)',
            'select'  => '(Select Gender)',
            'unknown' => 'Unknown',
            'male'    => 'Male',
            'female'  => 'Female'
        ),
    'nationality'             =>
        array(
            'all'    => '(All Nationalities)',
            'select' => '(Select Nationality)',
        ),
    'social_network'          => 'Social Network',
    'sessions'                => 'Sessions',
    'session_not_found'       => 'Session not found',
    'access_code_generated'   => 'A new access code has been generated for the user',
);
