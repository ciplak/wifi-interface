<?php

return array (
  'page_title' => 'Campaign',
  'columns' => 
  array (
    'title' => 'Title',
    'location_id' => 'Location',
    'parameter_type' => 'Parameter Type',
    'reengage' => 'Re-engage',
    'age' => 'Age Group',
    'visit_count' => 'Top Customers',
    'birthday' => 'Birthday',
    'sms_content' => 'Sms Content',
    'email_content' => 'Email Content',
    'status' => 'Status',
  ),
  'date' => 'Running Time',
  'communication_channel' => 'Communication Channel',
  'not_found' => 'Campaign not found',
  'saved' => 'Campaign Saved',
  'not_saved' => 'Campaign Not Saved',
  'periodic' => 'Periodic',
  'status' => 
  array (
    'published' => 'Published',
    'draft' => 'Draft',
  ),
);
