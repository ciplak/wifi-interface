<?php

return [
    'page_title'       => 'Splash Ads',
    'columns'          =>
        [
            'title'      => 'Title',
            'type'       => 'Type',
            'body'       => 'Text',
            'url'        => 'URL',
            'image'      => 'Image',
            'status'     => 'Status',
            'date'       => 'Date',
            'start_date' => 'Start Date',
            'end_date'   => 'End Date',
        ],
    'status'           =>
        [
            'published' => 'Published',
            'draft'     => 'Draft',
        ],
    'type' => [
        'text' => 'Text',
        'image' => 'Image',
    ],
    'not_found'        => 'Ad not found',
    'saved'            => 'Advertisement saved',
    'not_saved'        => 'Advertisement not saved',
    'select_to_insert' => 'Please select a splash ad to insert',
];
