<?php

return [
    'page_title'                 => 'Settings',
    'saved'                      => 'Settings saved',
    'logo'                       => 'Logo',
    'available_country_codes'    =>
        [
            'tooltip'      => 'Purpose of this is to define which country codes appear on Splash Portal or Captive Portal (Login Page) for end-user to enter their mobile numbers.',
            'all'          => 'All',
            'only_default' => 'Only default',
            'title'        => 'Available country codes',
        ],
    'default_country_phone_code' =>
        [
            'tooltip' => 'If Available country code set as \'Only Default\' then only the selected country code will appear to end-users on Splash Portal',
            'title'   => 'Default country code',
        ],
    'sms'                        =>
        [
            'title'                     => 'SMS',
            'content'                   => 'Content',
            'content_description'       => 'To include access code in the message use [[code]] snippet',
            'from'                      => 'SMS sender name',
            'daily_limit'               => 'Daily SMS limit',
            'warning_threshold'         => 'SMS warning threshold',
            'warning_threshold_tooltip' => 'When number of remaining SMS credits are below threshold, system sends a warning message',
        ],
    'date_time'                  => 'Date &amp; Time',
    'date_format'                => 'Date format',
    'time_format'                => 'Time format',
    'timezone'                   =>
        [
            'tooltip' => 'Default Time Zone for the Tenant\'s location',
            'title'   => 'Timezone',
        ],
    'start_of_week'              => 'Week starts on',
    'shared_device_count'        => 'No. of devices using same access code',
    'isolate_guests'             => [
        'title'   => 'Isolate guests',
        'tooltip' => 'Isolating Guest will disable multi-venue seamless WiFi experience. Guest in one venue is not recognized in other venue ',

    ],
    'tabs'                       =>
        [
            'email'    => 'E-mail',
            'advanced' => 'Advanced',
        ],
    'email'                      =>
        [
            'to'         => 'To',
            'cc'         => 'CC',
            'from'       =>
                [
                    'name'    => 'From name',
                    'address' => 'From address',
                ],
            'host'       => 'SMTP Host',
            'port'       => 'SMTP Port',
            'encryption' =>
                [
                    'none'  => 'None',
                    'ssl'   => 'SSL',
                    'tls'   => 'TLS',
                    'title' => 'Encryption',
                ],
            'username'   => 'Username',
            'password'   => 'Password',
        ],
    'social'                     =>
        [
            'facebook' =>
                [
                    'message'          => 'Message',
                    'message_tooltip'  => 'Message is posted on End-user’s Facebook timeline (i.e.: I am enjoying fast and free Wi-Fi at the Zen Café New York)',
                    'link'             => 'Link',
                    'link_tooltip'     => 'URL that will be displayed together with message on end-user’s Facebook timeline. You may use your Website URL or Facebook page URL',
                    'caption'          => 'Link caption',
                    'caption_tooltip'  => 'Enter a caption for the link you provided',
                    'place_id'         => 'Facebook Place ID',
                    'place_id_tooltip' => 'Facebook Page ID is used to automatically Check-in end-users to your venue on Facebook once they are successfully connected to the WiFi',
                    'description'      => 'Make your business visible on social media through spreading word-of- mouth. Social message is automatically posted on guest timeline or displayed as check-in message. If you enable this function, it is advised to include a message on Splash Portal to inform users that their social timeline will be updated. Examples; 1) We might post a message on your timeline. 2) You will be automatically checked-in the the Zen Café on Facebook',
                ],
        ],
    'proxy'                      =>
        [
            'host'     => 'Proxy host',
            'username' => 'Proxy username',
            'password' => 'Proxy password',
        ],
    'wifimanager'                =>
        [
            'secret_key'                       => 'WiFi Manager Secret Key',
            'radius_server_type'               => 'Radius Server Type',
            'profile_default_duration_by_mins' => 'Profile default duration by mins',
            'check_session_interval_by_secs'   => 'Check Session Interval by secs',
        ],
    'access_code'                =>
        [
            'title'            => 'Access Code',
            'expiry_period'    => 'Expiry period',
            'type'             => 'Access code type',
            'length'           => 'Access code length',
            'chars'            => 'Access code characters',
            'max_resend_count' => 'Max. resend count',
            'resend_wait_time' => 'Resend wait time',
        ],
    'enable_maintenance_mode'    => 'Enable',
    'maintenance_mode'           => 'Maintenance mode',
    'maintenance_mode_text'      => 'We are working on fixing some bugs. You may encounter error in some pages',
    'licence'                    => [
        'title' => 'Licence',
        'owner' => 'Owner',
        'info'  => 'Info',
    ],

];
