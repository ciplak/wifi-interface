<?php

return array(
    'page_title'            => 'Users',
    'title'                 => 'User',
    'columns'               =>
        array(
            'name'            => 'Name',
            'email'           => 'E-mail',
            'role'            => 'Role',
            'tenant'          => 'Tenant',
            'location'        => 'Location',
            'locale'          => 'Locale',
            'status' => 'Status',
            'banned_at' => 'Banned At'
        ),
    'saved'                 => 'User saved',
    'not_saved'             => 'User not saved',
    'deleted'               => 'User deleted',
    'not_deleted'           => 'User not deleted',
    'change_password'       => 'Change password',
    'password'              => 'Password',
    'password_confirmation' => 'Password confirmation',
    'profile'               => 'Profile',
    'profile_updated'       => 'Profile updated',
    'profile_not_updated'   => 'Profile not updated',
    'sessions'              => 'Sessions',
    'session'               =>
        array(
            'ip_address'    => 'IP Address',
            'user_agent'    => 'User Agent',
            'last_activity' => 'Last Activity',
            'not_found'     => 'No session found',
        ),
    'impersonate' => 'Impersonate',
    'stop_impersonating' => 'Stop Impersonating',
    'ban' => 'Ban',
    'status' => [
        'banned' => 'Banned',
        'active' => 'Active'
    ]
);
