<?php

return array (
  'title' => 'Alert Message',
  'page_title' => 'Alert Messages',
  'columns' => 
  array (
    'priority' => 'Priority',
    'message' => 'Message',
    'description' => 'Description',
    'date' => 'Date',
  ),
  'priority' => 
  array (
    'all' => '(All Priorities)',
    'low' => 'Low',
    'medium' => 'Medium',
    'high' => 'High',
  ),
  'not_found' => 'Alert message not found',
  'view_all' => 'View all alerts',
);
