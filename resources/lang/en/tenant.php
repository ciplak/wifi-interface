<?php

return array (
  'page_title' => 'Tenants',
  'title' => 'Tenant',
  'columns' => 
  array (
    'name' => 'Tenant Name',
    'info' => 'Tenant Info',
    'description' => 'Tenant description',
    'id' => 'Tenant Id',
  ),
  'saved' => 'Tenant saved',
  'not_found' => 'Tenant not found',
  'sms_credits' => 
  array (
    'title' => 'SMS Credits',
    'ref_no' => 'Ref. No',
    'credits' => 'Credits',
    'description' => 'Description',
    'date' => 'Date',
    'install' => 'Install credits',
    'installed' => ':count credits installed',
    'deleted' => 'SMS credits deleted',
    'not deleted' => 'Could not delete SMS credits',
    'total' => 'Total SMS credits',
    'average_daily_sms_usage' => 'Average Daily SMS Usage',
    'not_installed' => 'Could not install SMS credits',
    'not_found' => 'No SMS credits found',
  ),
);
