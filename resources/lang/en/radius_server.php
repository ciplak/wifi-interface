<?php

return array (
  'page_title' => 'Radius Servers',
  'columns' => 
  array (
    'type' => 'Type',
    'auth_port' => 'Authentication Port',
    'acc_port' => 'Accounting Port',
  ),
  'not_found' => 'Radius server not found',
);
