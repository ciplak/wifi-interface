<?php

return [
    'page_title'  => 'Service Profiles',
    'title'       => 'Service Profile',
    'columns'     =>
        [
            'name'                   => 'Name',
            'description'            => 'Description',
            'type'                   => 'Type',
            'model'                  => 'Model',
            'service_pin'            => 'Service PIN',
            'expiry_date'            => 'Expiry Date',
            'max_download_bandwidth' => 'Max Download Bandwidth',
            'max_upload_bandwidth'   => 'Max Upload Bandwidth',
            'qos_level'              => 'Qos Level',
            'duration'               => 'Duration',
            'price'                  => 'Price',
            'status'                 => 'Status',
        ],
    'filters'     =>
        [
            'duration' => 'Duration (Minutes)',
        ],
    'select'      => '(Select Service Profile)',
    'type'        =>
        [
            'all'          => '(All Types)',
            'tooltip'      => '<strong>One Time:</strong> Each device can connect only once for the duration specified in this Service Profile. <br><br><strong>Recurring:</strong> Each device can connect again only after the period of time specified. Recurring User-case: If you want end-users to connect 2 hours daily only, then set Type as \'Recurring\' with 1 day period. <br><br><strong>Unrestricted:</strong> Each device can reconnect again after session period is expired.',
            'one_time'     => 'One Time',
            'recurring'    => 'Recurring',
            'unrestricted' => 'Unrestricted',
        ],
    'model'       =>
        [
            'all'            => '(All Models)',
            'free_of_charge' => 'Free of Charge',
            'paid'           => 'Paid',
        ],
    'expired'     => 'Expired',
    'qos_level'   =>
        [
            'tooltip' => 'Quality of Service. Bandwidth / Speed limit',
        ],
    'service_pin' =>
        [
            'all'     => '(All)',
            'tooltip' => 'If enabled, access code is required after registration process.',
            'no'      => 'No',
            'yes'     => 'Yes',
        ],
    'not_found'   => 'Service profile not found',
    'saved'       => 'Service profile saved',
    'not_saved'   => 'Service profile not saved',
    'deleted'     => 'Service profile deleted',
    'not_deleted' =>
        [
            'in_use' => 'Could not delete service profile because it is in use',
        ],
];
