<?php

return [
    'page_title'            => 'Splash Pages',
    'columns'               =>
        [
            'name' => 'Splash page name',
            'type' => 'Type',
        ],
    'preview_warning'       => 'The page is in preview mode',
    'exit_preview'          => 'Exit Preview',
    'options'               => 'Options',
    'login_options'         =>
        [
            'button_label' =>
                [
                    'required' => ':login_type button label for :language is required',
                ],
        ],
    'custom_html'           => 'Html',
    'custom_css'            => 'CSS',
    'custom_js'             => 'Javascript',
    'images'                => 'Images',
    'registration'          => 'Registration',
    'access_code'           => 'Access code',
    'login_form'            => [
      'title' =>  'Login Form',
    ],
    'custom_field_no'       => 'Custom Field :no',
    'custom_field_no_title' => 'Custom Field :no Title',
    'not_found'             => 'Splash page not found',
    'saved'                 => 'Splash page saved',
    'not_saved'             => 'Splash page not saved',
    'deleted'               => 'Splash page deleted',
    'not_deleted'           => 'Splash page not deleted',
    'in_use'                => 'Splash page not deleted (The page is in use)',
    'publish_settings'      => 'Publish settings',

    'type'                         =>
        [
            'offline' => 'Pre-Login Page',
            'online'  => 'Post-Login Page',
        ],
    'send_email'                   => 'Send e-mail',
    'send_sms'                     => 'Send SMS',
    'social_login_requires_code'   => 'Social login requires code',
    'overwrite_guests'             => 'Overwrite guests',
    'published' => 'Published the splash page',
    'publish_settings_description' => 'Please select the location where you want to publish the splash page.',
    'resend_remaining_time_messages' => 'You need to wait to resend the code. Remaining time: <strong>{{ remainingTime }}</strong>',
];