<?php

return [
    'page_title' => 'Impressions',
    'not_found'  => 'Impression not found',

    'columns' => [
        'name'           => 'Guest Name',
        'username'       => 'Username',
        'description'    => 'Guest Device',
        'platform'       => 'Platform',
        'browser'        => 'Browser',
        'device_type_id' => 'Guest Device Type',
        'ip'             => 'IP',
        'mac'            => 'MAC',
        'created_at'     => 'Date Time',
    ],

    'filters' => [
        'name'           => 'Guest Name / Username',
        'ip'             => 'IP',
        'mac'            => 'MAC',
        'description'    => 'Guest Device',
        'platform'       => 'Platform',
        'browser'        => 'Browser',
        'device_type_id' => 'Guest Device Type',
        'created_at'     => 'Created At',
    ],
];