<?php

return array (
  'columns' => 
  array (
    'description' => 'Device',
    'platform' => 'Platform',
    'browser' => 'Browser',
    'type' => 'Device Type',
    'wifi_area' => 'WiFi Area',
  ),
  'type' => 
  array (
      'all'       => '(All Types)',
      'undefined' => 'Undefined',
      'mobile'    => 'Mobile',
      'tablet'    => 'Tablet',
      'desktop'   => 'Desktop',
  ),
);
