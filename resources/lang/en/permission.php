<?php

return [
    'page_title' => 'Permissions',
    'saved'      => 'Permission saved',
    'not_found'  => 'Permission not found',
];
