<?php

return array (
  'page_title' => 'Zones',
  'add' => 'Create zone',
  'name' => 'Name',
  'description' => 'Description',
  'override_terms_and_conditions' => 'Use custom text',
  'service_profile_inherited' => 'If no service profile is selected, service models selected in venue will be used',
  'not_found' => 'Zone not found',
  'created' => 'Zone created',
  'saved' => 'Zone saved',
  'deleted' => 'Zone deleted',
  'not_deleted' => 'Zone not deleted',
);
