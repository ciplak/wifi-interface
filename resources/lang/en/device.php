<?php

return [
    'page_title'                => 'Devices',
    'columns'                   =>
        [
            'location'    => 'Location',
            'name'        => 'Name',
            'description' => 'Description',
            'ip'          => 'IP Address',
            'mac'         => 'MAC Address',
            'model'       => 'Device Model',
        ],
    'all'                       => '(All Devices)',
    'bulk_import'               =>
        [
            'page_title'                    => 'Bulk Import',
            'description'                   => 'Must be a CSV, XLS or XLSX file',
            'click_to_download_sample_file' => 'Click here to download sample file',
        ],
    'location'                  =>
        [
            'select' => '(Select Locations)',
            'all'    => '(All Locations)',
        ],
    'model'                     =>
        [
            'all' => '(All Device Models)',
        ],
    'parent_device'             => 'Parent Device',
    'select_model'              => '(Select model)',
    'hotspot'                   => 'Hotspot',
    'status'                    =>
        [
            'all'     => '(All Statuses)',
            'deleted' => 'Deleted',
            'active'  => 'Active',
        ],
    'restored_options'          =>
        [
            'count' => 'Restored :count devices',
        ],
    'not_restored'              => 'Could not restore device',
    'select_devices_to_restore' => 'Select devices to restore',
    'delete'                    =>
        [
            'restore_item' => 'Restore this device?',
        ],
    'dont_restore'              => 'Do not restore',
    'restore'                   => 'Restore',
    'file_not_uploaded'         => 'Could not upload file',
    'invalid_file'              => 'Invalid file. File must be a CSV, XLS or XLSX file',
    'imported'                  => 'Imported :count device(s)',
    'not_imported'              => 'Could not import :count device(s)',
    'select_devices_to_delete'  => 'Select devices you want to delete',
    'not_found'                 => 'Device not found',
    'saved'                     => 'Device saved',
    'deleted'                   => 'Deleted',
    'deleted_options'           =>
        [
            'count' => 'Deleted :count devices',
        ],
    'not_deleted'               => 'Device not deleted',
    'sync_error'                => 'Could not synchronize the  Please contact with system admin.',
    'import_file_empty'         => 'Import file is empty',
    'restored'                  => 'Restored',

];
