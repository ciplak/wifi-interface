<?php

return array (
  'page_title' => 'Access Codes',
  'columns' => 
  array (
    'location' => 'Location',
    'code' => 'Code',
    'type' => 'Type',
    'expiry_date' => 'Expiry Date',
    'use_count' => 'Use Count',
    'status' => 'Status',
  ),
  'type' => 
  array (
    'select' => '(Select Type)',
    'all' => '(All Types)',
    'shared' => 'Shared',
    'access_code' => 'Access Code',
    'sms' => 'SMS Code',
  ),
  'select_type' => 'Please select code type',
  'select_venue' => 'Please select location',
  'select_service_profile' => 'Please select service profile',
  'enter_code' => 'Please enter access code',
  'status' => 
  array (
    'all' => '(All Statuses)',
    'available' => 'Available',
    'used' => 'Used',
  ),
  'created' => 'Created :count code|Created :count codes',
  'could_not_create' => 'Could not create any access codes',
  'expired' => 'Expired',
  'not_expired' => 'Not Expired',
  'generate' => 'Generate',
  'quantity' => 'Quantity',
  'not_found' => 'Access code not found',
  'deleted' => 'Access code deleted',
  'not_deleted' => 'Access code not deleted',
);
