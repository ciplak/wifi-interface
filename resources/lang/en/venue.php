<?php

return [
    'title'                   => 'Location',
    'page_title'              => 'Locations',
    'columns'                 =>
        [
            'name'           => 'Name',
            'description'    => 'Description',
            'address'        => 'Address',
            'post_code'      => 'Post Code',
            'city'           => 'City',
            'state'          => 'State',
            'country'        => 'Country',
            'smart_login'    => 'Smart Login',
            'post_login_url' => 'Post Login Url',
        ],
    'manage_service_profiles' => 'Manage service profiles',
    'select'                  => '(Select Location)',
    'all'                     => '(All Locations)',
    'ssids'                   => 'SSIDs',
    'ssid_name'               => 'SSID Name',
    'ssid_description'        => 'WLAN networks that require multiple SSIDs (e.g.: Guest, Exhibitors, Corporate, Vendors, etc.) and multiple Welcome Experience &amp; Authentication methods.',
    'not_found'               => 'Location not found',
    'created'                 => 'Location created',
    'saved'                   => 'Location saved',
    'not_saved'               => 'Location not saved',
    'deleted'                 => 'Location deleted',
    'not_deleted'             => 'Location not deleted',
    'validation'              => [
        'smart_login_frequency' => [
            'required_if' => 'You need enter a valid number for frequency when smart login is enabled',
        ],
    ],
];
