<?php

return array (
  'page_title' => 'Currencies',
  'columns' => 
  array (
    'name' => 'Name',
    'description' => 'Description',
    'symbol' => 'Symbol',
    'is_prefix' => 'Is Prefix',
  ),
  'not_found' => 'Currency not found',
  'saved' => 'Currency saved',
  'not_saved' => 'Currency not saved',
  'deleted' => 'Currency deleted',
  'not_deleted' => 'Currency not deleted',
);
