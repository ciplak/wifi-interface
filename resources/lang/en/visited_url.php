<?php

return [
    'page_title' => 'Visited URLs',

    'columns' => [
        'source_ip'        => 'Source IP',
        'source_port'      => 'Source Port',
        'destination_ip'   => 'Destination IP',
        'destination_port' => 'Destination Port',
        'ap_mac'           => 'Access Point MAC',
        'client_mac'       => 'Client MAC',
        'method'           => 'Method',
        'url'              => 'URL',
        'visited_at'       => 'Visited At',
    ],

    'not_found' => 'No record found',
];