<?php

return [
    'title'                     => 'Media',
    'insert_media'              => 'Insert media',
    'upload_files'              => 'Upload files',
    'media_library'             => 'Media library',
    'media_library_empty'       => 'Media library is empty',
    'insert_image_tag'          => 'Insert Image Tag',
    'insert_image_url'          => 'Insert Image Url',
    'select_file'               => 'Select file',
    'max_upload_file_size'      => 'Maximum upload file size: :size',
    'drop_files_here_to_upload' => 'Drop files here to upload',
    'or'                        => 'or',
];
