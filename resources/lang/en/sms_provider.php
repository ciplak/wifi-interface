<?php

return array (
  'page_title' => 'SMS Providers',
  'title' => 'SMS Provider',
  'columns' => 
  array (
    'name' => 'SMS Provider Name',
    'description' => 'Description',
    'gateway' => 'Gateway',
  ),
  'gateway' => 
  array (
    'clickatell' => 
    array (
      'api_id' => 'API ID',
      'username' => 'Username',
      'password' => 'Password',
      'auth_token' => 'Auth Token',
    ),
    'twilio' => 
    array (
      'account_sid' => 'Account SID',
      'auth_token' => 'Auth Token',
      'from' => 'From',
    ),
    'ISmartSms' => 
    array (
      'username' => 'Username',
      'password' => 'Password',
    ),
    'accinge' => 
    array (
      'username' => 'Username',
      'password' => 'Password',
      'sender_id' => 'Sender ID',
    ),
    'mobilpark' =>
    array (
      'username' => 'Username',
      'password' => 'Password',
      'from' => 'From',
    ),
    'mobildev' =>
    array (
      'key' => 'Key',
      'secret' => 'Secret',
      'msisdn' => 'MSISDN',
      'originator' => 'Sender ID',
    ),
    'mayermobile' =>
    array (
      'username' => 'Username',
      'password' => 'Password',
      'sender' => 'Sender'
    ),
  ),
  'not_found' => 'SMS provider not found',
  'saved' => 'SMS provider saved',
  'not_saved' => 'SMS provider not saved',
  'deleted' => 'SMS provider deleted',
  'not_deleted' => 'SMS provider not deleted',
);
