<?php

return array (
    'page_title'        => 'Dashboard',
    'max_online_guests' => 'Max. online',
    'number_of_logins'  => 'Number of logins',
    'impressions'       => 'Impressions',
    'new_logins'        => 'New logins',
    'total_traffic'     => 'Total Traffic',
);
