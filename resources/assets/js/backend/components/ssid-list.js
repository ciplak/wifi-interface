Vue.component('ssid-list', {
    data: () => ({
        newItem: {
            name: "",
            login_page_id: "",
            post_login_page_id: ""
        },
        itemList: []
    }),

    props: ['items', 'loginPages', 'postLoginPages'],

    methods: {
        add: function () {
            this.itemList.push(this.newItem);

            this.newItem = {
                name: "",
                login_page_id: "",
                post_login_page_id: ""
            };

        },

        remove: function (item) {
            console.log(this.itemList, item);
            this.items.splice(this.itemList.indexOf(item), 1);
        }
    },

    created() {
        this.itemList = this.items;
    }
});