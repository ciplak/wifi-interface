Vue.component('notifications', {
    data: () => ({
        items: [],
        count: 0
    }),

    methods: {
        loadData() {
            this.$http.get('notifications/recent').then(response => {
                this.count = response.data.count;
                this.items = response.data.items;
            });
        }
    },

    mounted() {
        this.loadData()
    },
});