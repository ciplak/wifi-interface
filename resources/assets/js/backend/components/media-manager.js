import Dropzone from './dropzone.vue';

Vue.component('media-manager', {
    data: () => ({
        errors: '',
        showProgress: false,
        progress: 0,
        items: [],
        selectedItem: null,
        busy: false
    }),

    props: {
        url: {
            type: String,
            default: 'media'
        },

        showSnippets: {
            type: Boolean,
            default: false
        },

        modelId: {
            default: null
        }
    },

    components: {
        Dropzone
    },

    created() {
        eventHub.$on('vdropzone-success', this.onSuccess);
        eventHub.$on('vdropzone-error', this.onError);
        eventHub.$on('vdropzone-sending', this.onSending);
        eventHub.$on('vdropzone-totaluploadprogress', this.onProgress);
        eventHub.$on('vdropzone-complete', this.onComplete);

        this.getAll();
    },

    methods: {

        getAll() {
            this.$http.get(this.url).then(response => this.items = response.data);
        },

        selectItem(item) {
            this.selectedItem = this.selectedItem == item ? null : item;

            eventHub.$emit('item-selected', this.selectedItem);
        },

        remove(item) {
            this.busy = true;

            this.$http.delete(this.url + '/' + item.id).then(() => {
                this.items.splice(this.items.indexOf(item), 1);
                this.selectedItem = null;
            }).finally(() => {
                this.busy = false;
            });
        },

        onSuccess(file, response) {
            this.items.push(response);
        },

        onError(file, response) {
            this.errors = response;
        },

        onSending() {
            this.showProgress = true;
            this.errors = '';
            $('a[href="#tab-media-library"]').tab('show');
        },

        onProgress(progress) {
            this.progress = progress;
        },

        onComplete() {
            this.showProgress = false;
            this.progress = 0;
        },

    }
});