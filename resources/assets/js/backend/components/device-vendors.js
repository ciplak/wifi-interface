Vue.component('device-vendors', {
    data: () => ({
        updatingItem: null,
        updating: false,
        items: [],
        form: new SparkForm({
            id: 0,
            name: ""
        })
    }),

    methods: {
        newItem() {
            return {
                id: 0,
                name: ""
            }
        },

        getAll() {
            this.$http.get('device-vendors').then(response => {
                this.items = response.data;
                eventHub.$emit('device-vendor-list-updated', this.items);
            });
        },

        showForm(item = null) {
            this.updating = true;

            this.updatingItem = item;

            if (!item) {
                this.updating = false;
                item = this.newItem()
            }

            _.assign(this.form, item);
            $('#modal-device-vendor-form').modal();
        },

        save() {
            return this.updating ? this.update() : this.store();
        },

        store() {
            Spark.post('device-vendors', this.form).then(() => {
                this.closeForm();
                this.getAll();
            });
        },

        update() {
            Spark.put('device-vendors/' + this.form.id, this.form).then(() => {
                this.closeForm();
                this.getAll();
            });
        },

        remove(item) {
            this.$http.delete('device-vendors/' + item.id).then(() => this.items.splice(this.items.indexOf(item), 1));
        },

        closeForm() {
            this.form.resetStatus();
            $('#modal-device-vendor-form').modal('hide');
        }

    },

    created() {
        this.getAll();
    }

});