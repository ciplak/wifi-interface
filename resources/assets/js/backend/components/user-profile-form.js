Vue.component('user-profile-form', {
    data: () => ({
        changePassword: false,
        form: new SparkForm({
            name: "",
            email: ""
        })
    }),

    mounted() {
        this.getInfo()
    },

    methods: {
        getInfo() {
            this.$http.get('users/profile').then(response => _.assign(this.form, response.data));
        },

        updateProfile() {
            Spark.put('users/profile', this.form).then(response => {
                if (response.status) {
                    toastr.success('User profile updated');
                } else {
                    toastr.error('Error on updating user profile');
                }

                this.changePassword = false;
                this.form.password = '';
                this.form.password_confirmation = '';
            });
        }
    }
});