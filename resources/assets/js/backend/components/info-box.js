export default {
    template: '<div class="card">' +
    '<div class="card-block">' +
    '<h4 class="card-title">{{ text }}</h4>' +
    '<div class="card-text">' +
    '<i class="fa" :class="[\'fa-\' + icon ]"></i>' +
    '<div class="counter">{{ data }}</div>' +
    '</div>' +
    '</div>' +
    '</div>',

    props: ['data', 'text', 'icon']
};