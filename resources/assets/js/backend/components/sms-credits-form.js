Vue.component('sms-credits-form', {
    data: () => ({
        form: new SparkForm({
            credit: "",
            description: "",
        })
    }),

    props: ['tenantId'],

    methods: {
        submit() {
            Spark.post('sms-credits/' + this.tenantId, this.form).then(response => {
                if (response.success) {
                    this.closeForm();
                    $('#sms-history-container').html(response.html);
                    $('#total-sms-credits').html(response.total_sms_credits);
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
            });
        },

        closeForm() {
            this.form.resetStatus();
            $('#modal-sms-credit-form').modal('hide');
        }
    },
});