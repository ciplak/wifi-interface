Vue.component('facebook-preview', {
    data: () => ({
        facebook: {
            message: '',
            link: '',
            caption: '',
            place: ''
        }
    }),

    created() {
        this.getData();
    },

    computed: {
        showPreview() {
            return this.facebook.message.trim() || this.facebook.link.trim() || this.facebook.place.trim();
        }
    },

    methods: {
        getData() {
            this.$http.get('settings/facebook').then(response => {
                this.facebook = response.data;
            });
        }
    }
});