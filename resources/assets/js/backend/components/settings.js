Vue.component('settings', {
    data: () => ({
        updating: false,
        form: new SparkForm({
            maintenance_mode: 0,
            email: {
                host: "",
                port: "",
                encryption: "",
                username: "",
                password: "",
                from: {
                    address: "",
                    name: ""
                }
            },
        })
    }),

    methods: {
        getAll() {
            this.$http.get('settings').then(response => _.assign(this.form, response.data));
        },

        update() {
            Spark.put('settings', this.form).then(() => toastr.success('Settings saved'));
        }
    },

    mounted() {
        this.getAll();
        //this.isLoading = false;
    }
});