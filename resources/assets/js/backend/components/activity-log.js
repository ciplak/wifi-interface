import pagination from "./pagination";

Vue.component('activity-log', {
    data: () => ({
        isLoading: false,
        items: [],
        pagination: {
            total: 0,
            per_page: 20,    // required
            current_page: 1, // required
            last_page: 0,    // required
            from: 1,
            to: 0            // required
        },
        paginationOptions: {
            offset: 4,
            previousText: 'Prev',
            nextText: 'Next',
            alwaysShowPrevNext: true
        }
    }),

    methods: {
        loadData() {
            this.isLoading = true;
            let data = {
                paginate: this.pagination.per_page,
                page: this.pagination.current_page
            };

            this.$http.get('activity-logs', {params: data}).then(response => {
                this.items = response.data.data;

                this.pagination = {
                    total: response.data.total,
                    per_page: response.data.per_page,
                    from: response.data.from,
                    to: response.data.to,
                    current_page: response.data.current_page,
                    last_page: response.data.last_page
                };
                this.isLoading = false;
            });
        }
    },

    components: {
       pagination
    },

    mounted() {
        this.loadData()
    },
});