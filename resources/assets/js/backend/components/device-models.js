Vue.component('device-models', {
    data: () => ({
        updating: false,
        items: [],
        deviceVendorOptions: null,
        typeOptions: null,
        handlerOptions: null,
        form: new SparkForm({
            id: 0,
            name: "",
            device_vendor_id: "",
            device_type_id: "",
            device_handler_id: ""
        })
    }),

    methods: {
        newItem() {
            return {
                id: 0,
                name: "",
                device_vendor_id: "",
                device_type_id: "",
                device_handler_id: ""
            };
        },

        getAll() {
            this.$http.get('device-models').then(response => this.items = response.data);
        },

        getTypeOptions() {
            this.$http.get('device-models/types').then(response => this.typeOptions =response.data);
        },

        getHandlerOptions() {
            this.$http.get('device-models/handlers').then(response => this.handlerOptions =response.data);
        },

        showForm(item = null) {
            this.updating = true;

            if (!item) {
                this.updating = false;
                item = this.newItem();
            }

            _.assign(this.form, item);
            $('#modal-device-model-form').modal();
        },

        save() {
            return this.updating ? this.update() : this.store();
        },

        store() {
            Spark.post('device-models', this.form).then(() => {
                this.closeForm();
                this.getAll();

                toastr.success('Device model saved');
            });
        },

        update() {
            Spark.put('device-models/' + this.form.id, this.form).then(() => {
                this.closeForm();
                this.getAll();

                toastr.success('Device model saved');
            });
        },

        remove(item) {
            this.$http.delete('device-models/' + item.id).then(() => this.items.splice(this.items.indexOf(item), 1));
        },

        closeForm() {
            this.form.resetStatus(0);
            $('#modal-device-model-form').modal('hide');
        },

        updateVendorList(data) {
            this.deviceVendorOptions = data;
            this.getAll();
        }
    },

    created() {
        eventHub.$on('device-vendor-list-updated', this.updateVendorList);

        this.getTypeOptions();
        this.getHandlerOptions();
        this.isLoading = false;
    }
});