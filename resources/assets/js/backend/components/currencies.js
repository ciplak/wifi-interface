Vue.component('currencies', {
    data: () => ({
        updating: false,
        items: [],
        form: new SparkForm({
            id: 0,
            name: "",
            description: "",
            symbol: "",
            is_prefix: 0
        })
    }),

    methods: {
        newItem() {
            return {
                id: 0,
                name: "",
                description: "",
                symbol: "",
                is_prefix: 0
            };
        },

        getAll() {
            this.$http.get('currencies').then(response => this.items = response.data);
        },

        showForm(item = null) {
            this.updating = true;

            if (!item) {
                this.updating = false;
                item = this.newItem();
            }

            _.assign(this.form, item);
            $('#modal-currency-form').modal();
        },

        save() {
            return this.updating ? this.update() : this.store();
        },

        store() {
            Spark.post('currencies', this.form).then(() => {
                this.closeForm();
                this.getAll();

                toastr.success('Currency saved');
            });
        },

        update() {
            Spark.put('currencies/' + this.form.id, this.form).then(() => {
                this.closeForm();
                this.getAll();

                toastr.success('Currency saved');
            });
        },

        remove(item) {
            this.$http.delete('currencies/' + item.id).then(() => this.items.splice(this.items.indexOf(item), 1));
        },

        closeForm() {
            this.form.resetStatus();
            $('#modal-currency-form').modal('hide');
        }
    },

    created() {
        this.getAll();
        this.isLoading = false;
    }
});