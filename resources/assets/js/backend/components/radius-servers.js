Vue.component('radius-servers', {
    data: () => ({
        updating: false,
        items: [],
        form: new SparkForm({
            id: 0,
            type: 0,
            ip: "",
            secret: "",
            auth_port: 1812,
            acc_port: 1813
        }),
        serverTypes: [
            {text: 'FreeRadius', value: 0},
            {text: 'TekRadius', value: 1}
        ]
    }),

    methods: {
        newItem() {
            return {
                id:0,
                type: 0,
                ip: "",
                secret: "",
                auth_port: 1812,
                acc_port: 1813
            };
        },

        getAll() {
            this.$http.get('radius-servers').then(response => this.items = response.data);
        },

        showForm(item = null) {
            this.updating = true;

            if (!item) {
                this.updating = false;
                item = this.newItem();
            }

            _.assign(this.form, item);
            $('#modal-radius-server-form').modal();
        },

        save() {
            return this.updating ? this.update() : this.store();
        },

        store() {
            Spark.post('radius-servers', this.form).then(() => {
                this.closeForm();
                this.getAll();

                toastr.success('Radius Server saved');
            });
        },

        update() {
            Spark.put('radius-servers/' + this.form.id, this.form).then(() => {
                this.closeForm();
                this.getAll();

                toastr.success('Radius Server saved');
            });
        },

        remove(item) {
            this.$http.delete('radius-servers/' + item.id).then(() => this.items.splice(this.items.indexOf(item), 1));
        },

        closeForm() {
            this.form.resetStatus();
            $('#modal-radius-server-form').modal('hide');
        }
    },

    created() {
        this.getAll();
        this.isLoading = false;
    }
});