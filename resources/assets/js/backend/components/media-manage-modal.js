Vue.component('media-manager-modal', {
    data: () => ({
        selectedItem: null
    }),

    props: {
        showSnippets: {
            type: Boolean,
            default: false
        },
    },

    created() {
        eventHub.$on('item-selected', this.selectItem);
    },

    methods: {
        embedImageTag() {
            embedSnippet('Image file=' +  this.selectedItem.file);
        },

        embedImageUrl() {
            embedSnippet('ImageUrl file=' +  this.selectedItem.file);
        },

        selectItem(item) {
            this.selectedItem = item;
        }
    }
});