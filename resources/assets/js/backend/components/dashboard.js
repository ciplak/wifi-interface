require('bootstrap-daterangepicker');
require('moment');

import InfoBox from "./info-box";
import OnlineUsersChart from "./charts/online-users-chart";
import DeviceUsageSharesChart from "./charts/device-usage-shares-chart";

Vue.component('dashboard', {
    components: {
        InfoBox,
        OnlineUsersChart,
        DeviceUsageSharesChart
    },

    data: () => ({
        data: {
            "numberOfOnlineGuests": 0,
            "numberOfMaxOnlineGuests": 0,
            "deviceUsageShares": [],
            "maxOnlineUsers": 0,
            "numberOfNewLogins": 0,
            "numberOfImpressions": 0,
            "numberOfLogins": 0,
            "onlineUsers": [],
            "totalTrafficOfDay": "00.00 B"
        },
        location: 0,
        datesEnabled: true,
        startDate: moment(),
        endDate: moment()
    }),

    props: ['locations'],

    methods: {
        loadData() {
            var location = this.location ? this.location : 0;

            this.$parent.loading = true;

            let postData = this.datesEnabled ? {start_date: this.startDate.format('YYYY-MM-DD'), end_date: this.endDate.format('YYYY-MM-DD')} : {};

            this.$http.post('reports/stats/' + location, postData).then(response => {
                this.data = response.data;

                eventHub.$emit('update-online-users-chart', this.data.onlineUsers);

                eventHub.$emit('update-device-usage-shares-chart', this.data.deviceUsageShares);
            }).finally(() => {
                this.$parent.loading = false;
            });
        },

        callback(start, end) {
            this.startDate = start;
            this.endDate = end;
        }
    },

    computed: {
        startDateFormatted() {
            return this.startDate.format('MMMM D, YYYY');
        },

        endDateFormatted() {
            return this.endDate.format('MMMM D, YYYY');
        }
    },

    mounted() {
        this.startDate = moment();
        this.endDate = moment();

        const target = $('#dashboard-report-date-range');

        target.daterangepicker({
            opens: "left",
            applyClass: "btn-primary pull-right",
            maxDate: moment().add(1, 'days'),
            locale: {
                format: 'YYYY-MM-DD'
            },
            dateLimit: {
                months: 1
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }

        }, this.callback);

        target.on('apply.daterangepicker', (ev, picker) => {
            this.loadData();
        });

        this.loadData();
    }
});