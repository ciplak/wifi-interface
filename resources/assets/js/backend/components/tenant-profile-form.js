Vue.component('tenant-profile-form', {
    data: () => ({
        form: new SparkForm({
            name: '',
            description: '',
            address: '',
            city: '',
            post_code: '',
            state: '',
            country_code: null,
            login_page_id: null,
            post_login_page_id: null,
            sms_provider_id: null,
            post_login_url: '',
            total_sms_credits: 0,
            average_daily_sms_usage: 0,
            sms_credits: []
        })
    }),

    mounted() {
        this.getInfo()
    },

    methods: {
        getInfo() {
            this.$http.get('tenants/profile').then(response => {
                _.assign(this.form, response.data);
                eventHub.$emit('sms-credits-history-updated', {
                    total_sms_credits: this.form.total_sms_credits,
                    average_daily_sms_usage: this.form.average_daily_sms_usage,
                    history: this.form.sms_credits
                });
            });
        },

        updateProfile() {
            Spark.put('tenants/profile', this.form).then(response => {
                if (response.status) {
                    toastr.success('Tenant profile updated');
                } else {
                    toastr.error('Error on updating tenant profile');
                }
            });
        }
    }

});