Vue.component('roles', {
    data: () => ({
        updating: false,
        items: [],
        form: new SparkForm({
            id: 0,
            name: "",
            description: "",
            display_name: ""
        })
    }),

    methods: {
        newItem() {
            return {
                id: 0,
                name: "",
                description: "",
                display_name: ""
            }
        },

        getAll() {
            this.$http.get('roles').then(response => this.items = response.data);
        },

        showForm(item = null) {
            this.updating = true;

            if (!item) {
                this.updating = false;
                item = this.newItem();
            }

            _.assign(this.form, item);
            $('#modal-role-form').modal();
        },

        save() {
            return this.updating ? this.update() : this.store();
        },

        store() {
            Spark.post('roles', this.form).then(() => {
                this.closeForm();
                this.getAll();
            });
        },

        update() {
            Spark.put('roles/' + this.form.id, this.form).then(() => {
                this.closeForm();
                this.getAll();
            });
        },

        remove(item) {
            this.$http.delete('roles/' + item.id).then(() => this.items.splice(this.items.indexOf(item), 1));
        },


        closeForm() {
            this.form.resetStatus();
            $('#modal-role-form').modal('hide');
        }
    },

    created() {
        this.getAll();
    }

});