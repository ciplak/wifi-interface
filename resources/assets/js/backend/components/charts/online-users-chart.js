export default {
    template: '<div></div>',

    data: () => ({
        chart: null,

        options: {
            chart: {
                renderTo: 'online-users',
                type: 'area'
            },
            title: {
                text: 'Online Users'
            },
            xAxis: {
                categories: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
                title: {
                    text: 'Hours'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Users'
                }

            },
            plotOptions: {
                area: {
                    fillOpacity: 0.5
                }
            },
            series: [
                {
                    name: 'Online Users',
                    //showInLegend: false,
                    data: []
                }

            ]
            }
    }),

    mounted() {
        eventHub.$on('update-online-users-chart', this.updateData);

        this.chart = new Highcharts.Chart(this.$el, this.options);
    },

    beforeDestroy() {
        this.chart.destroy();
    },

    methods: {
        updateData(data) {
            this.chart.series[0].setData(data, false);
            this.chart.redraw();
            this.chart.reflow();
        }
    }
};