export default {
    template: '<div></div>',

    data() {
        return {
            chart: null,

            options: {
                chart: {
                    renderTo: 'device-usage-shares',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: 'Device usage shares'
                },

                tooltip: {
                    pointFormat: '<b>{point.y}</b>'
                },

                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false

                        }
                    },
                    series: {
                        dataLabels: {
                            style: {
                                textShadow: false
                            },
                            enabled: true,
                            formatter: function () {
                                return Math.round(this.percentage * 100) / 100 + ' %';
                            },
                            distance: -30,
                            color: 'white'
                        }

                    }
                },
                legend: {
                    enabled: true
                },

                series: [{
                    type: 'pie',
                    data: [
                        
                    ]

                }]

            }
        }
    },

    mounted() {
        eventHub.$on('update-device-usage-shares-chart', this.updateData);

        this.chart = new Highcharts.Chart(this.$el, this.options);
    },

    beforeDestroy() {
        this.chart.destroy();
    },

    methods: {
        updateData(data) {
            this.chart.series[0].setData(data, false);
            this.chart.redraw();
            this.chart.reflow();
        }
    }
};