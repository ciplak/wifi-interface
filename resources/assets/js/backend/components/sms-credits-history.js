Vue.component('sms-credits-history', {
    data: () => ({
        total_sms_credits: 0,
        average_daily_sms_usage: 0,
        items: []
    }),

    created() {
        eventHub.$on('sms-credits-history-updated', this.update);
    },

    methods: {
        update(data) {
            this.items = data.history;
            this.total_sms_credits = parseInt(data.total_sms_credits);
            this.average_daily_sms_usage = parseInt(data.average_daily_sms_usage);
        }
    }
});