Vue.component('access-code-form', {

    data: () => ({
        busy: false,

        form: new SparkForm({
            venue_id: null,
            service_profile_id: null,
            type: 2,
            code: "",
            quantity: 1,
            expiry_date: null
        })
    }),

    props: ['types', 'serviceProfiles', 'venues'],

    methods: {
        submit() {
            this.busy = true;

            Spark.post('access-codes', this.form).then(response => {
                if (response.success) {
                    location.reload();
                } else {
                    toastr.error(response.message);
                }
            }).finally(() => {
                this.busy = false;
            });
        },

        closeForm() {
            this.form.resetStatus(0);
            this.venue_id = null;
            this.service_profile_id = null;
            this.type = null;
            this.code = "";
            this.quantity = 1;
            this.expiry_date = null;

            $('#modal-access-code-form').modal('hide');
        }

    },
});