require('../common/spark');

require('./components/navbar');
require('./components/notifications');

require('./components/activity-log');
require('./components/radius-servers');
require('./components/currencies');

require('./components/device-vendors');
require('./components/device-models');

require('./components/tenant-profile-form');
require('./components/user-profile-form');
require('./components/roles');
require('./components/facebook-preview');

require('./components/dashboard');

require('./components/media-manager');
require('./components/media-manage-modal');

require('./components/settings');

require('./components/access-code-form');
require('./components/sms-credits-form');
require('./components/sms-credits-history');

require('./components/ssid-list');