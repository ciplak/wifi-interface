const eventHub = new Vue();

const app = new Vue({
    el: '#app',

    /**
     * Holds the timestamp for the last time we updated the API token.
     */
    lastRefreshedApiTokenAt: null,

    data: {
        loading: false
    },

    /**
     * Prepare the application.
     */
    ready() {
        console.log('Application Ready.');

        this.whenReady();
        this.listen();
    },

    /**
     * The component has been created by Vue.
     */
    created() {
        //this.refreshApiTokenEveryFewMinutes();
    },

    methods: {
        /**
         * Finish bootstrapping the application.
         */

        whenReady() {
            //
        },

        listen() {
            if (App.echo.enabled) {
                Echo.private('guest.' + App.user.tenantId)
                    .listen('Guest.Registered', function (event) {
                        toastr.info('A new guest has registered: ' + event.guest_name);
                    });
            }
        },

        /**
         * Refresh the current API token every few minutes.
         */
        refreshApiTokenEveryFewMinutes() {
            var _this = this;

            this.lastRefreshedApiTokenAt = moment();

            setInterval(function () {
                _this.refreshApiToken();
            }, 240000);

            setInterval(function () {
                if (_this.lastRefreshedApiTokenAt.diff(moment(), 'minutes') >= 5) {
                    _this.refreshApiToken();
                }
            }, 5000);
        },

        /**
         * Refresh the current API token.
         */
        refreshApiToken() {
            this.lastRefreshedApiTokenAt = moment();

            this.$http.put('token').then(function (response) {
                console.log('API Token Refreshed.');
            });
        },

        /*
         * Get the current user of the application.
         */
        getUser() {
            this.$http.get('users/current').then(function (response) {
                App.user = response.data;
            });
        },
    }
});