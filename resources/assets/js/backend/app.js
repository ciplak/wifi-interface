require('./bootstrap');
require('./scripts');

Vue.directive('future-datepicker', {
    bind: function(el, binding, vnode) {
        $(el).datepicker({
            startDate: "+1d",
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            todayHighlight: true,
            clearBtn: true,
            autoclose: true
        }).on('changeDate', e => {
            var e = new Event('input', { bubbles: true });
            el.dispatchEvent(e);
        });
    }
});

Vue.directive('datepicker', {
    bind: function(el, binding, vnode) {
        $(el).datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            todayHighlight: true,
            clearBtn: true,
            autoclose: true
        }).on('changeDate', e => {
            var e = new Event('input', { bubbles: true });
            el.dispatchEvent(e);
        });
    }
});