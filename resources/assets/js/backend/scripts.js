function toggleFullScreen() {
    document.fullScreenElement && null !== document.fullScreenElement || !document.mozFullScreen && !document.webkitIsFullScreen ? document.documentElement.requestFullScreen ? document.documentElement.requestFullScreen() : document.documentElement.mozRequestFullScreen ? document.documentElement.mozRequestFullScreen() : document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT) : document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen && document.webkitCancelFullScreen()
}

var encodeHtmlEntity = function(str) {
    var buf = [];
    for (var i = str.length - 1; i >= 0; i--) {
        buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
    }
    return buf.join('');
};

window.resizeCharts = function() {
    $('[data-highcharts-chart]').each(function(i, el) {
        var c = $(el).highcharts();
        c.reflow();
    });
};

$(function () {
    var body = $('body');

    if (parseInt(localStorage.getItem('ipera-wifi-sidebar-open'))) {
        body.removeClass('sidebar-collapse');
    }

    // keep state of side bar
    $(document).on('click', '[data-toggle=offcanvas]', function (e) {
        localStorage.setItem('ipera-wifi-sidebar-open', body.hasClass('sidebar-collapse') ? 0 : 1);
        resizeCharts();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.toggle-full-screen').on('click', function() {
        toggleFullScreen();
    });

    // select 2
    $('.select').select2({
        theme: "bootstrap"
    });

    // input mask
    $('[data-mask]').inputmask();

    $('input.date').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        todayHighlight: true,
        clearBtn: true,
        autoclose: true
    });

    $('input.future-date').datepicker({
        startDate: "+1d",
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        todayHighlight: true,
        clearBtn: true,
        autoclose: true
    });

    $('.input-daterange').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        todayHighlight: true,
        clearBtn: true,
        //autoclose: true
    });

    $('.date-range-select').change(function() {
        var target;

        if ($(this).data('picker')) {
            target = $(this).data('picker');
        } else {
            target = '#date-range-picker';
        }

        var $target = $(target);
        if (parseInt($(this).val()) == -1) {
            $target.removeClass('hidden');
        } else {
            $target.addClass('hidden');
        }
    });

    // reset button
    $('.btn-reset').click(function() {
        var form = $(this).closest('form');

        $(':input', form)
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');

        $('.select', form).val(null).trigger('change');
        return false;
    });


    // active tab by hash
    function showTabByHash() {
        var hash = window.location.hash;

        if (hash) {
            var tab = $('a[href="'+ hash +'"]');
            if (tab.length) tab.tab('show');
        }
    }
    showTabByHash();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var scrollmem = body.scrollTop();
        location.hash = $(e.target).attr('href').substr(1);
        $('html,body').scrollTop(scrollmem);
    });

    $(window).bind('hashchange', function() {
        showTabByHash();
    });

    // file input
    $(".file-input").fileinput({
        showUpload: false,
        theme: 'fa',
        allowedFileTypes: ['image'],
        previewFileType: "image",
        removeIcon: '<i class="fa fa-times"></i> ',
        maxImageWidth: 1920,
        maxImageHeight: 1080,
        maxFileSize: 1048
    });

    // delete button confirmation
    body.on('click', '.btn-delete', function () {
        var $this = $(this),
            isAjax = $this.hasClass('ajax-delete'),
            message = 'Delete this item?';

        if ($.trim($this.attr('title'))) {
            message = $this.attr('title');
        }

        bootbox.confirm({
            message: message,
            buttons: {
                'cancel': {
                    label: "Don't delete",
                    className: "btn-default"
                },
                'confirm': {
                    label: "Delete",
                    className: "btn-danger"
                }
            },
            callback: function(result){
                if (!result) return ;

                if (isAjax) {

                    $.getJSON($this.attr('href'), function (data) {
                        if (data.status == true) {
                            $this.closest('tr').remove();
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    });

                } else {

                    location.href = $this.attr('href');

                }

            }
        });

        return false;
    });

    // tooltips
    $('[data-tooltip=true]').tooltip();

    // check box
    $('.check-all').change(function() {
        $(this).closest('table').find('tbody :checkbox').prop('checked', $(this).prop('checked'));
    });

    // maxlength
    $('[maxlength]').maxlength();

    $('.btn-export-chart').click(function () {
        var $this = $(this),
            chartContainer = $('#' + $this.data('chart-container')),
            chart = chartContainer.highcharts(),
            svg = chart.getSVG({
                chart: {
                    width: 1200,
                    height: 700
                }
            });

        svg = encodeHtmlEntity(svg);

        var form = $('<form method="POST" action="' + $this.data('url') + '">' +
            '<input type="hidden" name="_token" value="' + $('#csrf-token').attr('content') + '">' +
            '<input type="hidden" name="svg" value="' + svg + '">' +
            '</form>');

        form.appendTo(body);
        form.submit().remove();

        return false;
    });

    // property smart login options
    var smartLoginOptions = $('.smart-login-options');

    if (smartLoginOptions.length) {
        var smartLoginEnabled = $('#smart_login');

        smartLoginEnabled.change(function () {
            if ($(this).val() == 1) {
                smartLoginOptions.removeClass('hidden');
            } else {
                smartLoginOptions.addClass('hidden');
            }
        });

        smartLoginEnabled.trigger('change');
    }

    $('.affix-header').affix({
        offset: {
            top: 15
        }
    });

});