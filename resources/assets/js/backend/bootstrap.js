/*
 * Load Underscore.js, used for map / reduce on arrays.
 */
if (window._ === undefined) {
    window._ = require('underscore');
}

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

window.moment = require('moment');

require('../common/vue');

axios.interceptors.request.use(function (config) {
    let token = App.user.token;

    if (token) {
        config.headers['Authorization'] = 'Bearer ' + token;
    }

    return config;
}, function (err) {
    return Promise.reject(err);
});

require('./components');
require('jquery-qrcode');
require('bootstrap-maxlength');
require('bootstrap-datepicker');
require('bootstrap-fileinput');
require('bootstrap-fileinput/themes/fa/theme.js');
require('bootstrap-toggle');
require('select2');

require('jquery.inputmask/dist/jquery.inputmask.bundle');

require('./codemirror');
require('../AdminLTE');

require('../jquery-ui.min');
window.bootbox = require('bootbox');

window.toastr = require('toastr');

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

window.Highcharts = require('highcharts');

require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/no-data-to-display')(Highcharts);

Highcharts.theme = {
    colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
    exporting: {enabled: false},
    credits: false
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);

import Echo from "laravel-echo";

if (App.echo.enabled) {
    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: App.pusher.key,
        cluster: 'eu',
        encrypted: true
    });
}
