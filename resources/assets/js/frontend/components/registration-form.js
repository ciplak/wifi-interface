Vue.component('registration-form', {
    data() {
        return {
            require_code: false,
            resend_enabled: false,

            can_resend: false,
            counter: 0,
            countdownIntervalFunc: null,
            resend_counter: 0,

            message: "",
            error_message: "",

            redirecting: false,
            form: new SparkForm({
                code: "",
                name: "",
                email: "",
                birthday: "",
                country_code: "",
                phone: "",
                passport_no: "",
                gender: "",
                nationality: "",
                accept_terms_and_conditions: ""
            })
        }
    },

    props: ["counterStart", "maxResend", "countryCode"],

    created() {
        this.form.country_code = this.countryCode;
    },

    methods: {
        clearMessages() {
            this.message = "";
            this.error_message = "";
        },

        redirect(response) {
            this.redirecting = true;
            this.clearMessages();

            $(response.form).appendTo('body').submit();
        },

        submit() {
            Spark.post('register', this.form).then(response => {
                this.require_code = response.require_code;
                this.resend_enabled = response.resend_enabled;
                this.message = response.message;
                this.error_message = response.error_message;

                this.startCountdown();

                if (response.redirect) {
                    this.redirect(response);
                }
            });
        },

        checkCode() {
            Spark.post('register/check-code', this.form).then(response => {
                this.error_message = response.error_message;

                if (response.redirect) {
                    this.redirect(response);
                }
            });
        },

        resend() {
            this.clearMessages();

            this.form.busy = true;
            this.$http.get('register/resend-code', { params: { resend_counter: this.resend_counter} }).then(response => {
                this.message = response.data.message;
                this.error_message = response.data.error_message;
                this.startCountdown();
            }).finally(() => this.form.busy = false);
        },

        startCountdown() {
            if (this.resend_counter >= this.maxResend) {
                this.resend_enabled = false;
                return false;
            }
            this.counter = this.counterStart;
            this.can_resend = false;

            this.countdownIntervalFunc = setInterval(() => { this.countdown(); }, 1000);
        },

        countdown() {
            this.counter--;

            if (this.counter < 0) {
                clearInterval(this.countdownIntervalFunc);
                this.can_resend = true;
                this.resend_counter++;
            }
        }
    },

    computed: {
        remainingTime() {
            let minutes = parseInt(this.counter / 60) % 60;
            let seconds = this.counter % 60;

            return minutes + ":" + (seconds  < 10 ? "0" + seconds : seconds);
        }
    }
});