Vue.component('login-form', {
    data() {
        return {
            redirecting: false,
            message: "",
            error_message: "",
            form: new SparkForm({
                code: "",
                email: "",
                country_code: "",
                phone: "",
                passport_no: "",
                custom_field_1: "",
                custom_field_2: "",
                custom_field_3: "",
            })
        }
    },

    props: ['url', 'countryCode'],

    created() {
        this.form.country_code = this.countryCode;
    },

    methods: {
        redirect(response) {
            this.message = "";
            this.error_message = "";
            this.redirecting = true;
            $(response.form).appendTo('body').submit();
        },

        submit() {
            this.error_message = "";
            Spark.post("login", this.form).then(response => {
                this.error_message = response.error_message;
                this.message = response.message;

                if (response.redirect) {
                    this.redirect(response);
                }
            });
        }
    }
});