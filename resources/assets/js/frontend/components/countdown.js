Vue.component('countdown', {
    template: '<span class="remaining-time-countdown">{{ value }}</span>',

    data() {
        return {
            value: "00:00",
            intervalFn: null,
            start: null,
        }
    },

    props: ['duration'],

    created() {
        this.timer();
        this.intervalFn = setInterval(this.timer, 1000);
        this.start = Date.now() + 1000;
    },

    methods: {
        timer() {
            var diff = this.duration - (((Date.now() - this.start) / 1000) | 0);

            if (diff < 0) {
                clearInterval(this.intervalFn);
                return false;
            }

            var days        = Math.floor(diff/24/60/60);
            var hoursLeft   = Math.floor((diff) - (days*86400));
            var hours       = Math.floor(hoursLeft/3600);
            var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
            var minutes     = Math.floor(minutesLeft/60);
            var seconds = diff % 60;

            days = days < 10 ? "0" + days : days;
            hours = hours < 10 ? "0" + hours : hours;
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            var time = minutes + ":" + seconds;

            if (parseInt(hours) > 0) time = hours + ":" + time;
            if (parseInt(days) > 0) time = days + ":" + time;

            this.value = time;
        }
    }
});