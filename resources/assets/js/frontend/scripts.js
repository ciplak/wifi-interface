if (typeof isPreviewMode == "undefined") {
    window.isPreviewMode = false;
    window.previewWarningText = "";
}

$(function() {
    if (isPreviewMode) {
        $('a').click(function() {
            alert(previewWarningText);
            return false;
        });

        $('form').submit(function() {
            alert(previewWarningText);
            return false;
        });
    }

    var languageSelector = document.getElementById("language-selector");

    if (languageSelector) {
        var languageForm = document.getElementById("language-form");

        languageSelector.addEventListener("change", function () {
            languageForm.submit();
        });
    }

    $('[data-auth-provider]').click(function() {
        var provider = $(this).data('auth-provider');

        switch (provider) {
            case 'form':
                $('#register-form-modal').modal();
                break;

            default:
                window.location = '/auth/' + provider;
        }

        return false;
    });

    function hideOverlay() {
        $('.ipera-overlay-container').hide();
    }

    $('.ipera-overlay-close-button').click(function() {
        hideOverlay();
        return false;
    });

});