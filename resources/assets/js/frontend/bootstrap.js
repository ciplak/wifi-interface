if (!window.$) {
    window.$ = window.jQuery = require('jquery');
}

/*
 * Load Underscore.js, used for map / reduce on arrays.
 */
if (window._ === undefined) {
    window._ = require('underscore');
}


require('../common/spark.js');
require('../common/vue.js');

require('./components/registration-form');
require('./components/login-form');
// require('./components/access-code-form');
require('./components/countdown');