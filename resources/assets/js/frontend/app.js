require('./bootstrap');
require('./scripts');

import AccessCodeForm from "./components/access-code-form.vue";

require('jquery.inputmask/dist/jquery.inputmask.bundle');

delete Inputmask.prototype.defaults.definitions[9];

Inputmask.prototype.defaults.definitions["#"] = {
    validator: "[0-9]",
    cardinality: 1,
    definitionSymbol: "*"
};

Vue.directive('mask', {
    bind: function(el, binding, vnode) {
        $(el).inputmask({
            definitions: {},
            mask: binding.value,
            oncomplete: () => {
                  var e = new Event('input', { bubbles: true });
                 el.dispatchEvent(e);
            },
            onincomplete: () => {
                 var e = new Event('input', { bubbles: true });
                 el.dispatchEvent(e);
            }
        });
    }
});

const app = new Vue({
    el: '#ipera-wifi-app',
    components: {
        AccessCodeForm
    }
});