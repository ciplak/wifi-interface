/**
 * Spark form error collection class.
 */
window.SparkFormErrors = function () {
    this.errors = {};

    /**
     * Determine if the collection has any errors.
     */
    this.hasErrors = function () {
        return ! _.isEmpty(this.errors);
    };


    /**
     * Determine if the collection has errors for a given field.
     */
    this.has = function (field) {
        return _.indexOf(_.keys(this.errors), field) > -1;
    };


    /**
     * Get all of the raw errors for the collection.
     */
    this.all = function () {
        return this.errors;
    };


    /**
     * Get all of the errors for the collection in a flat array.
     */
    this.flatten = function () {
        return _.flatten(_.toArray(this.errors));
    };


    /**
     * Get the first error message for a given field.
     */
    this.get = function (field) {
        if (this.has(field)) {
            return this.errors[field][0];
        }
    };


    /**
     * Set the raw errors for the collection.
     */
    this.set = function (errors) {
        if (typeof errors === 'object') {
            this.errors = errors;
        } else {
            this.errors = {'field': ['Something went wrong. Please try again.']};
        }
    };


    /**
     * Forget all of the errors currently in the collection.
     */
    this.forget = function () {
        this.errors = {};
    };
};

/**
 * SparkForm helper class. Used to set common properties on all forms.
 */
window.SparkForm = function (data) {
    var form = this;

    $.extend(this, data);

    this.errors = new SparkFormErrors();
    this.busy = false;
    this.successful = false;

    this.startProcessing = function () {
        form.errors.forget();
        form.busy = true;
        form.successful = false;
    };

    this.finishProcessing = function () {
        form.busy = false;
        form.successful = true;
    };

    this.resetStatus = function() {
        form.errors.forget();
        form.busy = false;
        form.successful = false;
    };

    this.setErrors = function(data) {
        form.busy = false;
        form.errors.set(data);
    };
};

window.Spark = {};

var http = {
    /**
     * A few helper methods for making HTTP requests and doing common form actions.
     */
    post(uri, form) {
        return Spark.sendForm('post', uri, form);
    },


    put(uri, form) {
        return Spark.sendForm('put', uri, form);
    },


    delete(uri, form) {
        return Spark.sendForm('delete', uri, form);
    },

    /**
     * Send the form to the back-end server. Perform common form tasks.
     *
     * This function will automatically clear old errors, update "busy" status, etc.
     */
    sendForm: function (method, uri, form) {
        return new Promise((resolve, reject) => {
            form.startProcessing();

            axios[method](uri, form)
                .then(response => {
                    form.finishProcessing();
                    resolve(response.data);
                }).catch(error => {
                    form.errors.set(error.response.data);
                    form.busy = false;

                    reject(error.response.data);
                });
            

        });
    }
};

/**
 * Add additional form helpers to the Spark object.
 */
$.extend(Spark, http);