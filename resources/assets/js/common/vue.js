/*
 * Load Vue & Vue-Resource.
 *
 * Vue is the JavaScript framework used by Spark.
 */
if (window.Vue === undefined) {
    window.Vue = require('vue');
}

window.Promise = require('promise');

// This is the event hub we'll use in every
// component to communicate between them.
window.eventHub = new Vue();

window.axios = require('axios');
window.axios.defaults.baseURL = window.App ? App.apiRoot : '/';

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};

Vue.prototype.$http = axios;

Vue.filter('nl2br', function (value) {
    return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
});