<?php

namespace App\Listeners;


use App\Events\Venue\Created;
use App\Events\Venue\Updated;
use App\Models\ActivityLog;

class VenueEventListener
{
    /**
     * @var ActivityLog
     */
    private $logger;

    public function __construct(ActivityLog $activityLog)
    {
        $this->logger = $activityLog;
    }

    /**
     * Handle user create events
     *
     * @param Created $event
     */
    public function onCreated(Created $event)
    {
        $venue = $event->getVenue();
        $this->logger->setUserId(\Auth::id());
        $this->logger->log(sprintf("Tenant #%d '%s' is created", $venue->id, $venue->name));
    }

    /**
     * Handle user update by admin events
     *
     * @param Updated $event
     */
    public function onUpdated(Updated $event)
    {
        $venue = $event->getVenue();
        $this->logger->setUserId(\Auth::id());
        $this->logger->log(sprintf("Tenant #%d '%s' is updated", $venue->id, $venue->name));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher $events
     * @return array
     */
    public function subscribe($events)
    {
        $class = static::class;

        $events->listen(Created::class, "{$class}@onCreated");
        $events->listen(Updated::class, "{$class}@onUpdated");

    }
}
