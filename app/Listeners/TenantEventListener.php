<?php

namespace App\Listeners;


use App\Events\Tenant\Created;
use App\Events\Tenant\Updated;
use App\Models\ActivityLog;

class TenantEventListener
{
    /**
     * @var ActivityLog
     */
    private $logger;

    public function __construct(ActivityLog $activityLog)
    {
        $this->logger = $activityLog;
    }

    /**
     * Handle user create events
     *
     * @param Created $event
     */
    public function onCreated(Created $event)
    {
        $tenant = $event->getTenant();
        $this->logger->setUserId(\Auth::id());
        $this->logger->log(sprintf("Tenant #%d '%s' is created", $tenant->id, $tenant->name));
    }

    /**
     * Handle user update by admin events
     *
     * @param Updated $event
     */
    public function onUpdated(Updated $event)
    {
        $tenant = $event->getTenant();
        $this->logger->setUserId(\Auth::id());
        $this->logger->log(sprintf("Tenant #%d '%s' is updated", $tenant->id, $tenant->name));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher $events
     * @return array
     */
    public function subscribe($events)
    {
        $class = static::class;

        $events->listen(Created::class, "{$class}@onCreated");
        $events->listen(Updated::class, "{$class}@onUpdated");

    }
}
