<?php

namespace App\Listeners;


use App\Events\User\Created;
use App\Events\User\LoggedIn;
use App\Events\User\PasswordChanged;
use App\Events\User\UpdatedByAdmin;
use App\Events\User\UpdatedProfile;
use App\Models\ActivityLog;
use Illuminate\Auth\Events\Logout;

class UserEventListener
{
    /**
     * @var ActivityLog
     */
    private $logger;

    public function __construct(ActivityLog $activityLog)
    {
        $this->logger = $activityLog;
    }

    /**
     * Handle user login events
     *
     * @param Login $event
     */
    public function onLoggedIn($event)
    {
        $this->logger->setUserId($event->user->getAuthIdentifier());
        $this->logger->log('User logged in');
    }

    /**
     * Handle user logout events
     *
     * @param Logout $event
     */
    public function onLoggedOut($event)
    {
        $this->logger->setUserId($event->user->getAuthIdentifier());
        $this->logger->log('User logged out');
    }

    /**
     * Handle user create events
     *
     * @param Created $event
     */
    public function onCreated(Created $event)
    {
        $user = $event->getUser();
        $this->logger->setUserId($user->id);
        $this->logger->log(sprintf("User #%d '%s' is created", $user->id, $user->name));
    }

    /**
     * Handle user update by admin events
     *
     * @param UpdatedByAdmin $event
     */
    public function onUpdatedByAdmin(UpdatedByAdmin $event)
    {
        $user = $event->getUser();
        $this->logger->setUserId($user->id);
        $this->logger->log(sprintf("User #%d '%s' is updated", $user->id, $user->name));
    }

    /**
     * Handle user update by admin events
     *
     * @param UpdatedProfile $event
     */
    public function onUpdatedProfile(UpdatedProfile $event)
    {
        $this->logger->setUserId(\Auth::id());
        $this->logger->log('Updated own profile');
    }

    /**
     * Handle user update by admin events
     *
     * @param PasswordChanged $event
     */
    public function onPasswordChanged(PasswordChanged $event)
    {
        $this->logger->setUserId(\Auth::id());
        $this->logger->log('Changed password');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     *
     * @return array
     */
    public function subscribe($events)
    {
        $class = static::class;

        $events->listen(LoggedIn::class, "{$class}@onLoggedIn");
        $events->listen(Logout::class, "{$class}@onLoggedOut");

        $events->listen(Created::class, "{$class}@onCreated");
        $events->listen(UpdatedByAdmin::class, "{$class}@onUpdatedByAdmin");
        $events->listen(UpdatedProfile::class, "{$class}@onUpdatedProfile");
        $events->listen(PasswordChanged::class, "{$class}@onPasswordChanged");
    }
}
