<?php

namespace App\Listeners;

use App\Events\Sms\SmsSent;
use App\Mail\SmsWarningThreshold;
use App\Models\SmsLog;
use App\Repositories\SettingRepository;
use App\Repositories\SmsLogRepository;
use App\Repositories\UserRepository;

class SmsLoggerListener
{
    /**
     * @var SmsLogRepository
     */
    private $smsLogs;

    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var SettingRepository
     */
    private $settings;

    /**
     * Create the event listener.
     *
     * @param SmsLogRepository  $smsLogs
     * @param UserRepository    $users
     * @param SettingRepository $settings
     */
    public function __construct(SmsLogRepository $smsLogs, UserRepository $users, SettingRepository $settings)
    {
        $this->smsLogs = $smsLogs;
        $this->users = $users;
        $this->settings = $settings;
    }

    /**
     * Handle the event.
     *
     * @param SmsSent $event
     *
     * @return void
     */
    public function handle(SmsSent $event)
    {
        $this->createLog($event);

        $this->settings->decreaseSmsCredits($event->tenantId);

        if ($this->settings->checkThresholdWarning($event->tenantId)) {
            $emails = $this->users->getTenantAdminEmails($event->tenantId);
            $emails[] = $this->settings->getToEmailAddress();

            $emails = $emails->filter(function($item) {
                return trim($item);
            });

            if ($emails) {
                \Mail::to($emails)->queue(new SmsWarningThreshold());
            }
        }
    }

    /**
     * Create SMS log
     *
     * @param SmsSent $event
     */
    private function createLog(SmsSent $event)
    {
        SmsLog::create(
            [
            'tenant_id'       => $event->tenantId,
            'sms_provider_id' => $event->smsProvider->id,
            'type'            => $event->type,
            //            'event_type'      => $event->eventType,
            'to'              => $event->to,
            'message'         => $event->message,
            'queue_id'        => $event->queueId,
            'data'            => $event->data,
            'response'        => $event->response,
            'status'          => $event->status,
            ]
        );
    }
}
