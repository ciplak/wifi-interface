<?php

namespace App\Listeners;


use App\Events\RadiusServer\Created;
use App\Events\RadiusServer\Updated;
use App\Models\ActivityLog;

class RadiusServerListener
{
    /**
     * @var ActivityLog
     */
    private $logger;

    public function __construct(ActivityLog $activityLog)
    {
        $this->logger = $activityLog;
    }

    /**
     * Handle user create events
     *
     * @param Created $event
     */
    public function onCreated(Created $event)
    {
        $user = \Auth::user();
        $this->logger->setUserId($user->id);
        $this->logger->log(sprintf("Radius server #%d '%s' is created", $user->id, $user->name));
    }

    /**
     * Handle user update by admin events
     *
     * @param Updated $event
     */
    public function onUpdated(Updated $event)
    {
        $this->logger->setUserId(\Auth::id());
        $this->logger->log('Radius server updated #' . $event->getRadiusServerId());
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     *
     * @return array
     */
    public function subscribe($events)
    {
        $class = static::class;

        $events->listen(Created::class, "{$class}@onCreated");
        $events->listen(Updated::class, "{$class}@onUpdated");
    }
}
