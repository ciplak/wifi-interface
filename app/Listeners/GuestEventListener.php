<?php

namespace App\Listeners;


use App\Events\Guest\Connected;
use App\Events\Guest\Created;
use App\Events\Guest\DeviceConnected;
use App\Events\Guest\DeviceCreated;
use App\Events\Guest\Registered;
use App\Models\GuestActivity;
use Carbon\Carbon;

class GuestEventListener
{
    /**
     * Handle guest create events
     *
     * @param Created $event
     */
    public function onCreated(Created $event)
    {

    }

    /**
     * Handle guest create events
     *
     * @param Registered $event
     */
    public function onRegistered(Registered $event)
    {
        $guestDevice = $event->guestDevice;

        GuestActivity::create([
            'tenant_id'       => $event->guest->tenant_id,
            'property_id'     => $event->guest->venue_id,
            'guest_id'        => $event->guest->id,
            'guest_device_id' => $guestDevice ? $guestDevice->id : null,
            'action_id'       => GuestActivity::ACTION_REGISTERED,
            'ip'              => request()->ip(),
            'mac'             => $guestDevice ? $guestDevice->mac : session('client_mac'),
        ]);
    }

    /**
     * Handle guest login events
     *
     * @param Connected $event
     */
    public function onConnected(Connected $event)
    {
        $event->guest->update(
            [
                'status'     => true,
                'login_time' => Carbon::now(),
                //'last_visit_time' => Carbon::now(),
                //'total_visits' => $event->guest->total_visits + 1
            ]
        );

        $guestDevice = $event->guestDevice;

        GuestActivity::create([
            'tenant_id'       => $event->guest->tenant_id,
            'property_id'     => $event->guest->venue_id,
            'guest_id'        => $event->guest->id,
            'guest_device_id' => $guestDevice ? $guestDevice->id : null,
            'action_id'       => GuestActivity::ACTION_CONNECT_TO_WIFI,
            'ip'              => request()->ip(),
            'mac'             => $guestDevice ? $guestDevice->mac : session('client_mac'),
        ]);
    }

    /**
     * Handle guest device create events
     *
     * @param DeviceCreated $event
     */
    public function onDeviceCreated(DeviceCreated $event)
    {

    }

    /**
     * Handle guest device connect events
     *
     * @param DeviceConnected $event
     */
    public function onDeviceConnected(DeviceConnected $event)
    {

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     *
     * @return array
     */
    public function subscribe($events)
    {
        $class = 'App\Listeners\GuestEventListener';

        $events->listen(Created::class, "{$class}@onCreated");
        $events->listen(Registered::class, "{$class}@onRegistered");
        $events->listen(Connected::class, "{$class}@onConnected");
        $events->listen(DeviceCreated::class, "{$class}@onDeviceCreated");
        $events->listen(DeviceConnected::class, "{$class}@onDeviceConnected");

    }
}
