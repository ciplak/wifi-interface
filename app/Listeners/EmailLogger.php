<?php

namespace App\Listeners;


use App\Models\EmailLog;

class EmailLogger
{
    /**
     * Handle the event.
     *
     * @param  \Swift_Message $message
     * @return void
     */
    public function handle(\Swift_Message $message)
    {
        $cc = $message->getHeaders()->get('Cc');
        $bcc = $message->getHeaders()->get('Bcc');
        $subject = $message->getHeaders()->get('Subject');

        EmailLog::create(
            [
            'to' => $message->getHeaders()->get('To')->getFieldBody(),
            'cc' => $cc ? $cc->getFieldBody() : '',
            'bcc' => $bcc ? $bcc->getFieldBody() : '',
            'subject' => $subject ? $subject->getFieldBody(): '',
            'body' => $this->getMimeEntityString($message),
            ]
        );
    }

    /**
     * Get a loggable string out of a Swiftmailer entity.
     *
     * @param  \Swift_Mime_MimeEntity $entity
     * @return string
     */
    protected function getMimeEntityString(\Swift_Mime_MimeEntity $entity)
    {
        $string = (string)$entity->getHeaders() . PHP_EOL . $entity->getBody();
        foreach ($entity->getChildren() as $children) {
            $string .= PHP_EOL . PHP_EOL . $this->getMimeEntityString($children);
        }
        return $string;
    }
}
