<?php

namespace App\Listeners;


use App\Events\Zone\Created;
use App\Events\Zone\Updated;
use App\Models\ActivityLog;

class ZoneEventListener
{
    /**
     * @var ActivityLog
     */
    private $logger;

    public function __construct(ActivityLog $activityLog)
    {
        $this->logger = $activityLog;
    }

    /**
     * Handle user create events
     *
     * @param Created $event
     */
    public function onCreated(Created $event)
    {
        $zone = $event->getZone();
        $this->logger->setUserId(\Auth::id());
        $this->logger->log(sprintf("Zone #%d '%s' is created", $zone->id, $zone->name));
    }

    /**
     * Handle user update by admin events
     *
     * @param Updated $event
     */
    public function onUpdated(Updated $event)
    {
        $zone = $event->getZone();
        $this->logger->setUserId(\Auth::id());
        $this->logger->log(sprintf("Zone #%d '%s' is updated", $zone->id, $zone->name));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher $events
     * @return array
     */
    public function subscribe($events)
    {
        $class = static::class;

        $events->listen(Created::class, "{$class}@onCreated");
        $events->listen(Updated::class, "{$class}@onUpdated");

    }
}
