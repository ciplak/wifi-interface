<?php

namespace App\Jobs;

use App\Models\VisitedUrl;
use App\Services\SyslogParser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessSysLogs implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \stdClass
     */
    private $logs;

    /**
     * @var array
     */
    private $accessPoints;

    /**
     * Create a new job instance.
     *
     * @param $logs
     */
    public function __construct($logs)
    {
        $this->logs = $logs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $lastLogId = null;

        foreach ($this->logs as $log) {
            $data = SyslogParser::parseAvayaLog($log->Message);

            if ($data !== false) {
                $accessPoint = $this->getAccessPoint($data['ap_mac']);

                $data['tenant_id'] = $accessPoint->tenant_id;
                $data['device_id'] = $accessPoint->id;
                $data['visited_at'] = $log->ReceivedAt;
                $data['url'] = substr($data['url'], 0, 255);

                if (VisitedUrl::create($data)) {
                    $lastLogId = $log->ID;
                }
            }
        }

        \DB::table('settings')
            ->updateOrInsert([
                'tenant_id' => null,
                'name'      => 'syslog.last_processed_id',
            ], [
                'value' => $lastLogId,
            ]);
    }

    /**
     * @param string $mac
     *
     * @return \stdClass
     */
    private function getAccessPoint($mac)
    {
        if (!isset($this->accessPoints[$mac])) {
            $this->accessPoints[$mac] = $this->findAccessPoint($mac);
        }

        return $this->accessPoints[$mac];
    }

    /**
     * @param string $mac
     *
     * @return \stdClass
     */
    private function findAccessPoint($mac)
    {
        $accessPoint = \DB::table('devices')
            ->where('mac', $mac)
            ->first(['id', 'tenant_id']);

        if (!$accessPoint) {
            $accessPoint = new \stdClass();
            $accessPoint->tenant_id = null;
            $accessPoint->id = null;
        }

        return $accessPoint;
    }
}
