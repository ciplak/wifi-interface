<?php

namespace App\Reports;

use App\ConnectionStatus;

class Guest
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]|\App\Models\Guest
     */
    public static function getData()
    {
        return static::filter()->get();
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public static function filter()
    {
        $request = trimmed_request();

        $location = null;
        if (!\Entrust::can('venues.manage-global')) {
            $location = \Auth::user()->property_id;
        } else {
            $location = hashids_decode($request->input('location'));
        }

        $connectionStatus = (int)$request->connection_status ? ConnectionStatus::CONNECTED : ConnectionStatus::DISCONNECTED;

        /**
         * @var \Illuminate\Database\Query\Builder $query
         */
        $query = \App\Models\Guest::orderBy('id', 'DESC')
            ->select([
                'id',
                'name',
                'email',
                'phone',
                'nationality',
                'passport_no',
                'birthday',
                'gender',
                'last_visit_time',
                'total_visits',
            ])
            ->whereIn('id', function($query) use ($request, $location) {
                /**
                 * @var \Illuminate\Database\Query\Builder $query
                 */
                $query->select('guest_id')
                    ->from('wifi_sessions')
                    ->whereNull('parent_session_id')
                    ->whereRaw('tenant_id = guests.tenant_id');

                if ($request->has('date')) {
                    $query->whereBetween('start_time', date_ranges($request->input('date')));
                }

                if ($location) {
                    $query->where('venue_id', $location);
                }
            })
            ->when($request->has('name'), function($query) use ($request) {
                return $query->where(function($query) use ($request) {
                    $query->where('name', 'LIKE', '%' . $request->name . '%');
                        //->orWhere('wifi_sessions.username', 'LIKE', "%{$request->name}%");
                });
            })
            ->if($request->has('email'), 'email', 'LIKE', "%{$request->email}%")
            ->if($request->has('phone'), 'phone', 'LIKE', "%{$request->phone}%")
            ->if($request->has('nationality'), 'nationality', $request->nationality)
            ->if($request->has('passport_no'), 'passport_no', 'LIKE', "%{$request->passport_no}%")
            ->if($request->has('gender'), 'gender', $request->gender)
            ->if($request->has('connection_status'), 'connection_status', $connectionStatus);

        return $query;
    }
}