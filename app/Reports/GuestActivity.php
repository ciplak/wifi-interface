<?php

namespace App\Reports;

class GuestActivity
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]|\App\Models\GuestActivity
     */
    public static function getData()
    {
        return static::filter()->get();
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public static function filter()
    {
        $request = request();

        /**
         * @var \Illuminate\Database\Query\Builder $query
         */
        $query = \App\Models\GuestActivity::orderBy('guest_activities.id', 'DESC')
            ->select([
                'guests.name',
                'guests.username',
                'guest_activities.guest_device_id',
                'guest_devices.device_type_id',
                'guest_devices.description',
                'guest_devices.platform',
                'guest_devices.browser',
                'guest_activities.ip',
                'guest_activities.mac',
                'guest_activities.created_at',
            ])
            ->leftJoin('guests', 'guests.id', '=', 'guest_activities.guest_id')
            ->leftJoin('guest_devices', 'guest_devices.id', '=', 'guest_activities.guest_device_id')
            ->when($request->has('date'), function ($query) use ($request) {
                return $query->whereBetween('guest_activities.created_at', date_ranges($request->input('date')));
            })
            ->when($request->has('name'), function ($query) use ($request) {
                return $query->where(function ($query) use ($request) {
                    $query->where('guests.name', 'LIKE', "%{$request->name}%")
                        ->orWhere('guests.username', 'LIKE', "%{$request->name}%");
                });
            })
            ->where('guest_activities.action_id', \App\Models\GuestActivity::ACTION_VIEW_LOGIN_PAGE)
            ->where('guest_activities.tenant_id', activeTenantId())
            ->if($request->has('description'), 'guest_devices.description', 'LIKE', "%{$request->description}%")
            ->if($request->has('platform'), 'guest_devices.platform', 'LIKE', "%{$request->platform}%")
            ->if($request->has('browser'), 'guest_devices.browser', 'LIKE', "%{$request->browser}%")
            ->if($request->has('ip'), 'guest_activities.ip', 'LIKE', "%{$request->ip}%")
            ->if($request->has('mac'), 'guest_activities.mac', 'LIKE', "%{$request->mac}%")
            ->if($request->has('device_type_id'), 'guest_devices.device_type_id', $request->device_type_id);

        return $query;
    }
}