<?php

namespace App\Reports;

use App\ConnectionStatus;

class GuestSessions
{
    const OPERATOR_EQUALS = 0;
    const OPERATOR_LESS_THAN = 1;
    const OPERATOR_GREATER_THAN = 2;
    const OPERATOR_LESS_THAN_OR_EQUAL = 3;
    const OPERATOR_GREATER_THAN_OR_EQUAL = 4;

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]|\App\Models\Guest
     */
    public static function getData()
    {
        return static::filter()->get();
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public static function filter()
    {
        $request = trimmed_request();

        $connectionStatus = (int)$request->connection_status ? ConnectionStatus::CONNECTED : ConnectionStatus::DISCONNECTED;
        $operators = self::getOperators();

        /**
         * @var \Illuminate\Database\Query\Builder $query
         */
        $query = \App\Models\WifiSession::orderBy('wifi_sessions.id', 'DESC')
            ->select([
                'guests.name',
                'wifi_sessions.guest_id',

                'wifi_sessions.username',
                'wifi_sessions.start_time',
                'wifi_sessions.downloaded_data',
                'wifi_sessions.uploaded_data',
                'wifi_sessions.duration',
                'wifi_sessions.guest_device_ip',
                'wifi_sessions.guest_device_mac',
                'wifi_sessions.connection_status',
                'wifi_sessions.access_code',
            ])
            ->join('guests', 'guests.id', '=', 'wifi_sessions.guest_id')
            ->when($request->has('name'), function ($query) use ($request) {
                return $query->where(function ($query) use ($request) {
                    $query->where('name', 'LIKE', '%' . $request->name . '%')
                        ->orWhere('wifi_sessions.username', 'LIKE', "%{$request->name}%");
                });
            })
            ->when($request->has('duration'), function ($query) use ($request, $operators) {
                $operator = $operators[$request->duration_operator] ?? '=';

                return $query->where('wifi_sessions.duration', $operator, (int)($request->duration * 60));     // duration in minutes
            })
            ->when($request->has('downloaded_data'), function ($query) use ($request, $operators) {
                $operator = $operators[$request->download_operator] ?? '=';

                return $query->where('wifi_sessions.downloaded_data', $operator, (int)($request->downloaded_data * 1024 * 1024));  // data in MB
            })
            ->when($request->has('uploaded_data'), function ($query) use ($request, $operators) {
                $operator = $operators[$request->upload_operator] ?? '=';

                return $query->where('wifi_sessions.uploaded_data', $operator, (int)($request->uploaded_data * 1024 * 1024));      // data in MB
            })
            ->when($request->has('date'), function ($query) use ($request) {
                return $query->whereBetween('wifi_sessions.start_time', date_ranges($request->input('date')));
            })
            ->if($request->has('guest_device_mac'), 'guest_device_mac', 'LIKE', "%{$request->guest_device_mac}%")
            ->if($request->has('guest_device_ip'), 'guest_device_ip', 'LIKE', "%{$request->guest_device_ip}%")
            ->if($request->has('access_code'), 'wifi_sessions.access_code', 'LIKE', "%{$request->access_code}%")
            ->if($request->has('connection_status'), 'wifi_sessions.connection_status', $connectionStatus);

        return $query;
    }

    /**
     * @todo create enumeration class
     *
     * @param bool $includeEquals
     * @return array
     */
    public static function getOperators($includeEquals = false)
    {
        $operators = [];

        if ($includeEquals) {
            $operators[self::OPERATOR_EQUALS] = '=';
        }

        $operators[self::OPERATOR_LESS_THAN_OR_EQUAL] = '<=';
        $operators[self::OPERATOR_GREATER_THAN_OR_EQUAL] = '>=';

        return $operators;
    }
}