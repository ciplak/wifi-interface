<?php

namespace App;

use Illuminate\Support\Arr;

/**
 * Class Setting
 *
 * @package App
 */
class Setting
{
    /**
     * Setting data
     *
     * @var array
     */
    protected $data = [];

    /**
     * Whether the settings data are loaded
     *
     * @var bool
     */
    protected $loaded = false;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Get setting value
     *
     * @param  string $key
     * @param  null   $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $this->checkLoaded();

        return Arr::get($this->data, $key, $default);
    }

    /**
     * Check if data loaded from db
     */
    protected function checkLoaded()
    {
        if (!$this->loaded) {
            $this->read();
            $this->loaded = true;
        }
    }

    /**
     * Read settings from db
     */
    public function read()
    {
        $this->data = $this->parseReadData($this->query()->get(['name', 'value']));
    }

    /**
     * Parse dotted string and convert to arrays
     *
     * @param  $data
     *
     * @return array
     */
    public function parseReadData($data)
    {
        $results = [];
        foreach ($data as $row) {
            if (is_array($row)) {
                $key = $row['name'];
                $value = $row['value'];
            } elseif (is_object($row)) {
                $key = $row->name;
                $value = $row->value;
            } else {
                $msg = 'Expected array or object, got ' . gettype($row);
                throw new \UnexpectedValueException($msg);
            }
            Arr::set($results, $key, $value);
        }

        return $results;
    }

    /**
     * Create a new query builder instnace
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function query()
    {
        $query = \DB::table($this->table)->where('tenant_id', activeTenantId());

        return $query;
    }

    /**
     * Put a key / value pair or array of key / value pairs in settings
     *
     * @param  string|array $key
     * @param  mixed|null   $value
     *
     * @return $this
     */
    public function put($key, $value = null)
    {
        if (!is_array($key)) {
            $key = [$key => $value];
        }

        foreach ($key as $arrayKey => $arrayValue) {
            $this->set($arrayKey, (string)$arrayValue);
        }

        return $this;
    }

    /**
     * Set setting
     *
     * @param  $key
     * @param  $value
     *
     * @return $this
     */
    public function set($key, $value)
    {
        $this->checkLoaded();
        Arr::set($this->data, $key, $value);

        return $this;
    }

    /**
     * Remove one or many array items using "dot" notation.
     *
     * @param  array|string $key
     *
     * @return void
     */
    public function forget($key)
    {
        Arr::forget($this->data, $key);
    }

    /**
     * Check if a key is set
     *
     * @param  $key
     *
     * @return bool
     */
    public function has($key)
    {
        $this->checkLoaded();

        return Arr::has($this->data, $key);
    }

    /**
     * Save settings into the db
     */
    public function save()
    {
        $keys = $this->query()
            ->pluck('name');

        $insertData = Arr::dot($this->data);
        $updateData = $deleteKeys = [];

        foreach ($keys as $key) {
            if (isset($insertData[$key])) {
                $updateData[$key] = $insertData[$key];
            } else {
                $deleteKeys[] = $key;
            }

            unset($insertData[$key]);
        }

        foreach ($updateData as $key => $value) {
            $this->query()
                ->where('name', $key)
                ->update(['value' => $value]);
        }

        if ($insertData) {
            $data = [];
            foreach ($insertData as $name => $value) {
                $data[] = ['tenant_id' => activeTenantId(), 'name' => $name, 'value' => $value];
            }

            $this->query()
                ->insert($data);
        }

        if ($deleteKeys) {
            $this->query()
                ->whereIn('key', $deleteKeys)
                ->delete();
        }
    }

    /**
     * Return settings data
     *
     * @return array
     */
    public function all()
    {
        $this->checkLoaded();

        return $this->data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function raw()
    {
        return $this->query()->get();
    }

    /**
     * Get global settings
     *
     * @return \Illuminate\Support\Collection
     */
    public function getGlobal()
    {
        $data = cache()->store('file')->remember('global-settings', 60, function () {
            return \DB::table('settings')->whereNull('tenant_id')->pluck('value', 'name');
        });

        return $data;
    }
}