<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app['setting'] = $this->app->share(
            function ($app) {
                return new Setting;
            }
        );

        $this->app->alias('setting', Setting::class);

    }

    public function provides()
    {
        return array(
            'setting',
        );
    }
}
