<?php

namespace App\Providers;

use App\Events\Sms\SmsSent;
use App\Listeners\GuestEventListener;
use App\Listeners\RadiusServerListener;
use App\Listeners\TenantEventListener;
use App\Listeners\UserEventListener;
use App\Listeners\VenueEventListener;
use App\Listeners\ZoneEventListener;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'mailer.sending' => [
            \App\Listeners\EmailLogger::class
        ],
        SmsSent::class => [
            \App\Listeners\SmsLoggerListener::class
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\Instagram\InstagramExtendSocialite@handle',
            'SocialiteProviders\Foursquare\FoursquareExtendSocialite@handle',
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        UserEventListener::class,
        GuestEventListener::class,
        TenantEventListener::class,
        VenueEventListener::class,
        ZoneEventListener::class,
        RadiusServerListener::class,
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
