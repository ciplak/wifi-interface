<?php

namespace App\Providers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->formMacros();
        $this->collectionMacros();
        $this->queryBuilderMacros();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Add form macros
     */
    private function formMacros()
    {
        \Form::macro(
            'phone', function ($name, $value = null, $options = []) {
                $defaultCountryCode = config('front_page_settings.default_country_phone_code');

                $singleValue = isset($options['phone-country-code-text']) && $options['phone-country-code-text']
                    ? $options['phone-country-code-text']
                    : config('countries.phone_code_names.' . $defaultCountryCode);

                $output =
                (config('front_page_settings.country_phone_code_type') == 0
                    ? \Form::select('country_code', config('countries.phone_code_names'), null, ['class' => 'form-control country-code-list', 'v-model' => 'form.country_code'])
                    //: '<span class="form-control phone-country-code">' . $singleValue . '</span>'
                    : \Form::text('country_code', $singleValue, ['class' => 'form-control', 'disabled'])
                ) .
                \Form::input('tel', $name, $value, $options);

                return $output;
            }
        );
    }

    /**
     * Add collection macros
     */
    private function collectionMacros()
    {
        Collection::macro(
            'toVueArray', function ($prependValue = null) {
                $items = collect($this->items)
                ->transform(
                    function ($name, $value) {
                        return ['value' => $value, 'name' => $name];
                    }
                );

                if ($prependValue) {
                    $items = $items->prepend(['value' => null, 'name' => $prependValue]);
                }

                return $items->values();
            }
        );

        Collection::macro(
            'toVueArrayJson', function ($prependValue = null) {
                return collect($this->items)
                ->toVueArray($prependValue)
                ->toJson();
            }
        );
    }

    /**
     * Add query builder macros
     */
    private function queryBuilderMacros()
    {
        Builder::macro('if', function ($condition, $column, $operator, $value = null) {
            if ($condition) {
                return is_null($value)
                    ? $this->where($column, $operator)
                    : $this->where($column, $operator, $value);
            }

            return $this;
        });
    }
}
