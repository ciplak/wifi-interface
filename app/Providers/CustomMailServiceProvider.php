<?php

namespace App\Providers;

use Illuminate\Mail\MailServiceProvider;
use Illuminate\Mail\TransportManager;
use Illuminate\Support\Arr;

class CustomMailServiceProvider extends MailServiceProvider
{
    /**
     * Register the Swift Transport instance.
     *
     * @return void
     */
    public function registerSwiftTransport()
    {
        $this->app['swift.transport'] = $this->app->share(
            function ($app) {
                return new CustomTransportManager($app);
            }
        );
    }
}

class CustomTransportManager extends TransportManager
{

    /**
     * Create a new manager instance.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;

        $settings = setting()->getGlobal();

        if ($settings) {

            $this->app['config']['mail'] = [
                'driver'     => env('MAIL_DRIVER', 'smtp'),

                'host'       => Arr::get($settings, 'email.host'),
                'port'       => Arr::get($settings, 'email.port'),
                'from'       => [
                    'name'    => Arr::get($settings, 'email.from.name'),
                    'address' => Arr::get($settings, 'email.from.address'),
                ],
                'encryption' => Arr::get($settings, 'email.encryption'),
                'username'   => Arr::get($settings, 'email.username'),
                'password'   => Arr::get($settings, 'email.password'),

                //'sendmail'   => '',
                //'pretend'    => '',
            ];
        }
    }
}