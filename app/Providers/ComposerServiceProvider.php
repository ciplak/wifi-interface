<?php

namespace App\Providers;

use App\Composers\AccessCodeFilterComposer;
use App\Composers\AdminComposer;
use App\Composers\AdminNavigationComposer;
use App\Composers\AdvertisementComposer;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider
     *
     * @param Factory $factory
     */
    public function boot(Factory $factory)
    {
        $factory->composer('layouts.admin', AdminComposer::class);
        $factory->composer('layouts.admin._navigation', AdminNavigationComposer::class);
        $factory->composer('admin.advertisement.modal', AdvertisementComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
