<?php

namespace App\Providers;

use App\Services\WifiEngine\AccountingChecker;
use App\Services\WifiEngine\Engine;
use App\Services\WifiEngine\SessionChecker;
use App\Services\WifiEngine\SessionSync;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        \Validator::extend(
            'mac', function ($attribute, $value, $parameters, $validator) {
                return !empty($value) && preg_match('/^(([0-9a-fA-F]{2}-){5}|([0-9a-fA-F]{2}:){5})[0-9a-fA-F]{2}$/', $value);
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment() == 'local') {
            $this->app->register(\Laracasts\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);

            $loader = AliasLoader::getInstance();
            $loader->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
        }

        $this->app->singleton(Engine::class, function($app) {
            /** @var \Illuminate\Contracts\Foundation\Application $app */
            return new Engine($app->make(SessionSync::class), $app->make(SessionChecker::class), $app->make(AccountingChecker::class));
        });
    }
}
