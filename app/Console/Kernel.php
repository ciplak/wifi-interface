<?php

namespace App\Console;

use App\Console\Commands\Loop;
use App\Console\Commands\RunLoop;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The bootstrap classes for the application.
     *
     * @var array
     */
    protected $bootstrappers = [
        'Illuminate\Foundation\Bootstrap\DetectEnvironment',
        'Illuminate\Foundation\Bootstrap\LoadConfiguration',
        //'Illuminate\Foundation\Bootstrap\ConfigureLogging',
        \App\Services\ConfigureLogging::class,
        'Illuminate\Foundation\Bootstrap\HandleExceptions',
        'Illuminate\Foundation\Bootstrap\RegisterFacades',
        'Illuminate\Foundation\Bootstrap\SetRequestForConsole',
        'Illuminate\Foundation\Bootstrap\RegisterProviders',
        'Illuminate\Foundation\Bootstrap\BootProviders',
    ];

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //\App\Console\Commands\Inspire::class,
        \App\Console\Commands\CodeClear::class,
        \App\Console\Commands\Install::class,
        \App\Console\Commands\AlertSend::class,
        \App\Console\Commands\CodeUpdate::class,
        \App\Console\Commands\Crud::class,

        \App\Console\Commands\EngineRunCommand::class,
        \App\Console\Commands\EngineAlwaysUpCommand::class,
        \App\Console\Commands\UpdateConnectionStatus::class,
        \App\Console\Commands\SyslogProcess::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')
        //         ->hourly();

        //$schedule->command('alert:send')
        //    ->everyMinute();

        $schedule->command('engine:alwaysup')
            ->everyFiveMinutes()
            ->withoutOverlapping();

        $schedule->command('engine:update-connection-status')
            ->everyTenMinutes()
            ->withoutOverlapping();
    }
}
