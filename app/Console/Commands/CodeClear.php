<?php

namespace App\Console\Commands;

use App\Models\AccessCode;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CodeClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'code:clear {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear used or expired codes that were created older than 2 months';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = AccessCode
            ::where(
                function ($query) {
                    $query->where('status', AccessCode::STATUS_USED)
                        ->orWhere('expiry_date', '<=', Carbon::today());
                }
            );

        if (!$this->option('all')) {
            $query->where('created_at', '<=', Carbon::today()->subMonth(2));
        }

        echo 'Codes cleaned: ', $query->delete();
    }
}
