<?php

namespace App\Console\Commands;

use App\Services\CrudGenerator;
use Illuminate\Console\AppNamespaceDetectorTrait;
use Illuminate\Console\Command;

class Crud extends Command
{
    use AppNamespaceDetectorTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud {name} {--api} {--schema=} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simple CRUD generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = str_singular($this->argument('name'));

        $crud = new CrudGenerator();

        $config = [
            'app_namespace'         => $this->getAppNamespace(),
            'controller_namespace'  => $this->getAppNamespace() . 'Http\Controllers',
            'model_namespace'       => $this->getAppNamespace() . 'Models',
            'model_namespaced_name' => $this->getAppNamespace() . 'Models\\' . studly_case($name),
            'request_namespace'     => $this->getAppNamespace() . 'Http\Requests',

            'name'                  => $name,
            'name_plural'           => str_plural($name),
            'table_name'            => strtolower(str_plural(snake_case($name))),
            'model_name'            => studly_case($name),
            'name_camel_case'       => studly_case($name),
            'name_snake_case'       => snake_case($name),
            'model_fillable_fields' => '',
            'migration_fields'      => '',
        ];

        $columns = [];
        if ($this->option('schema')) {
            $fields = explode(',', $this->option('schema'));

            foreach ($fields as $field) {
                $segments = explode(':', $field);

                $name = array_shift($segments);
                $type = array_shift($segments);
                $arguments = $segments;

                // Do we have arguments being used here?
                // Like: string(100)
                if (preg_match('/(.+?)\(([^)]+)\)/', $type, $matches)) {
                    $type = $matches[1];
                    $arguments = explode(',', $matches[2]);
                }

                $columns[] = compact('name', 'type', 'arguments');
            }

            $migrationFields = '';
            foreach ($columns as $column) {
                $migrationFields .= sprintf(
                    "\t\t\t\$table->%s('%s')%s;\n",
                    $column['type'] ? $column['type'] : 'unsignedInteger',
                    $column['name'],
                    $column['arguments'] ? '->' . implode('()->', $column['arguments']) . '()' : ''
                );
            }

            $config['model_fillable_fields'] = "'" . implode("', '", array_column($columns, 'name')) . "'";
            $config['migration_fields'] = $migrationFields;
            //            $this->line(print_r($str, 1));
            //            return;
        }

        // Controller
        $this->line('Creating controller');
        $file = app_path('Http/Controllers') . '/' . $config['name_camel_case'] . 'Controller.php';

        if ($this->checkOverwrite($file, 'Controller exists, Overwrite?')) {
            $this->controller($config, $crud);
        } else {
            $this->info('Skipping controller');
        }

        // Model
        $this->line('Creating model');
        $file = app_path('Models') . '/' . $config['name_camel_case'] . '.php';

        if ($this->checkOverwrite($file, 'Model exists, Overwrite?')) {
            $this->model($config, $crud);
        } else {
            $this->info('Skipping model');
        }

        // Views
        $this->line('Creating views');

        $views = ['index', 'grid', 'form'];

        foreach ($views as $view) {
            $file = resource_path('views/admin') . '/' . $config['name_snake_case'] . '/' . $view . '.php';

            if ($this->checkOverwrite($file, "View '{$view}' exists, Overwrite?")) {
                $this->view($config, $crud, $view);
            } else {
                $this->info("Skipping view '{$view}'");
            }
        }

        // API Controller
        if ($this->option('api')) {
            $this->line('Creating API controller');
            $file = app_path('Http/Controllers/Api') . '/' . $config['name_camel_case'] . 'Controller.php';

            if ($this->checkOverwrite($file, 'Api Controller exists, Overwrite?')) {
                $this->apiController($config, $crud);
            } else {
                $this->info('Skipping controller');
            }
        }
    }

    /**
     * @param string $file
     * @param string $message
     *
     * @return bool
     */
    private function checkOverwrite($file, $message)
    {
        if ($this->option('force')) {
            return true;
        }

        return !file_exists($file) || (file_exists($file) && $this->confirm($message));
    }

    /**
     * @param array         $config
     * @param CrudGenerator $crud
     */
    private function controller($config, $crud)
    {
        $crud->controller($config);
        $crud->form_request($config);
        $this->comment('Done.');
    }

    /**
     * @param array         $config
     * @param CrudGenerator $crud
     */
    private function model($config, $crud)
    {
        $crud->model($config);
        $crud->migration($config);
        $this->comment('Done.');
    }

    /**
     * @param array         $config
     * @param CrudGenerator $crud
     */
    private function apiController($config, $crud)
    {
        $crud->api_controller($config);
        $this->comment('Done.');
    }

    /**
     * @param array         $config
     * @param CrudGenerator $crud
     * @param string        $view
     */
    private function view($config, $crud, $view)
    {
        $crud->view($config, $view);
        $this->comment("'{$view}' Done.");
    }
}
