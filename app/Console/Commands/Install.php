<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->copyAssets();

        $this->settings();
    }

    private function copyAssets()
    {
        $basePath = app_path() . '/../resources/files/img/';

        $files = ['admin-logo.jpg', 'logo.jpg', 'background.jpg'];

        foreach ($files as $file) {
            $stream = fopen($basePath . $file, 'r+');
            \Storage::putStream('img/' . $file, $stream);
            fclose($stream);
        }
    }

    private function settings()
    {
        setting(
            [
            'wifimanager.secret_key' => 'xl/3aSrueVKr9OIMQoJa2g==',
            'wifimanager.radius_server_type' => 1,
            'wifimanager.profile_default_duration_by_mins' => 30,
            'wifimanager.check_session_interval_by_secs' => 30,
            'logo' => 'admin-logo.jpg',
            'licence_owner' => 'IPera Solutions',
            'service_profile.check_type' => 1
            ]
        );

        setting()->save();
    }
}
