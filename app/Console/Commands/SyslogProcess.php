<?php

namespace App\Console\Commands;

use App\Jobs\ProcessSysLogs;
use Illuminate\Console\Command;

class SyslogProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syslog:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);

        $lastProcessedId = (int)\DB::table('settings')->where(['tenant_id' => null, 'name' => 'syslog.last_processed_id'])->value('value');

        \DB::connection(env('SYSLOG_CONNECTION'))
            ->table('SystemEvents')
            ->select(['ID', 'Message', 'ReceivedAt'])
            ->where('id', '>', $lastProcessedId)
            ->chunk(300, function($logs) {
                dispatch(new ProcessSysLogs($logs));
            });

        return 0;
    }
}
