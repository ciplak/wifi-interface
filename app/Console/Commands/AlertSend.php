<?php

namespace App\Console\Commands;

use App\Mail\AlertMessageMail;
use App\Models\AlertMessage;
use Illuminate\Console\Command;
use Illuminate\Mail\Message;

class AlertSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /**
         * @var AlertMessage[] $alertMessages
         */
        $alertMessages = AlertMessage::where('status', AlertMessage::STATUS_PENDING)->get();

        $cc = trim(setting('email.cc'));

        if ($cc) {
            $cc = str_replace(';', ',', $cc);
            $cc = explode(',', $cc);
            $cc = array_map('trim', $cc);
        }

        $alertTo = explode(',', setting('email.alert_to'));

        foreach ($alertMessages as $alertMessage) {
            if ($alertTo) {
                \Mail::to($alertTo)->cc($cc)->send(new AlertMessageMail($alertMessage));

                if (count(\Mail::failures())) {
                    \Log::error('Failed to send alert message email', ['alert message id' => $alertMessage->id, 'recipients' => \Mail::failures()]);
                } else {
                    $alertMessage->update(['status' => AlertMessage::STATUS_EMAIL_SENT]);
                }
            }
        }

    }
}
