<?php

namespace App\Console\Commands;

use App\ConnectionStatus;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;

class UpdateConnectionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:update-connection-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates guest sessions as disconnected if logout time exceeded';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $now = \Carbon\Carbon::now();
            $now->subMinutes(3);

            \DB::table('wifi_sessions_origin')
                ->whereIn('id', function($query) use ($now) {
                    /** @var Builder $query */
                    $query->from('wifi_sessions')
                        ->select('wifi_sessions_origin_id')
                        ->where('guest_device_logout_time', '<', $now)
                        ->where('connection_status', ConnectionStatus::CONNECTED);
                })
                ->update(['session_status' => 9]);
        } catch (\Exception $e) {
            return -1;
        }

        return 0;
    }
}
