<?php

namespace App\Console\Commands;

use App\Models\AccessCode;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CodeUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'code:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update codes as used if logout time is expired';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AccessCode::where('logout_time', '<=', Carbon::now())->update(['status' => AccessCode::STATUS_USED]);
    }
}
