<?php

namespace App\Console\Commands;

use App\Services\WifiEngine\Engine;
use Carbon\Carbon;
use Illuminate\Console\Command;

class EngineRunCommand extends Command
{
    /**
     * @var Engine
     */
    protected $engine;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:run 
                           {--once : Only process the next job on the queue}
                           {--memory=512 : The memory limit in megabytes}
                           {--sleep=30 : Number of seconds to sleep when no job is available}
                           {--timeout=60 : The number of seconds a child process can run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'php artisan engine:run init value cron but --daemon parameter set daemon process ';

    /**
     * Create a new command instance.
     *
     * @param Engine $engine
     */
    public function __construct(Engine $engine)
    {
        parent::__construct();
        $this->engine = $engine;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('once')) {
            $this->runEngine();
        } else {
            $this->daemon();
        }

        return 0;
    }

    /**
     * @return int
     */
    public function runEngine()
    {
        try {
            //$this->updateOrInsertIsWorking();

            //if ($this->getEngineIsWorking() === false) {
                //$this->updateOrInsertIsWorking(true);

            $this->engine->runSyncSessions();
            $this->engine->runSessionChecker();
            $this->engine->runAccountingChecker();

            //$this->updateOrInsertIsWorking();
            $this->updateOrInsertEngineLastWorkingTime(Carbon::now());
            //}
        } catch (\Exception $e) {
            $this->log($e->getMessage());

            return 1;
        }
    }

    /**
     *
     */
    public function daemon()
    {
        while (true) {
            $this->registerTimeoutHandler();
            $this->runEngine();
            $this->sleep($this->option('sleep'));

            if ($this->memoryExceeded($this->option('memory'))) { // ||  $this->shouldRestart($lastRestart))
                $this->stop();
            }
        }
    }

    /**
     * Determine if the memory limit has been exceeded.
     *
     * @param  int $memoryLimit
     *
     * @return bool
     */
    public function memoryExceeded($memoryLimit)
    {
        return (memory_get_usage() / 1024 / 1024) >= $memoryLimit;
    }

    /**
     * Stop listening and bail out of the script.
     *
     * @return void
     */
    public function stop()
    {
        die;
    }

    /**
     * Register the worker timeout handler (PHP 7.1+).
     *
     * @return void
     */
    protected function registerTimeoutHandler()
    {
        if ($this->option('timeout') == 0 || version_compare(PHP_VERSION, '7.1.0') < 0 || !extension_loaded('pcntl')) {
            return;
        }

        pcntl_async_signals(true);

        pcntl_signal(SIGALRM, function () {
            if (extension_loaded('posix')) {
                posix_kill(getmypid(), SIGKILL);
            }

            exit(1);
        });

        pcntl_alarm($this->option('timeout') + $this->option('sleep'));
    }

    /**
     * Sleep the script for a given number of seconds.
     *
     * @param  int $seconds
     *
     * @return void
     */
    public function sleep($seconds)
    {
        sleep($seconds);
    }

    /**
     * @param bool $isWorking
     *
     * @return bool
     */
    public function updateOrInsertIsWorking($isWorking = false)
    {
        return \DB::table('settings')
            ->updateOrInsert(
                ['name' => 'wifimanager.wifiengine_is_working'],
                ['value' => $isWorking]
            );
    }

    /**
     * @param $date
     *
     * @return bool
     */
    private function updateOrInsertEngineLastWorkingTime($date)
    {
        return \DB::table('settings')
            ->updateOrInsert(
                ['name' => 'wifimanager.wifiengine_last_working_time'],
                ['value' => $date]
            );
    }

    /**
     * @return bool
     */
    public function getEngineIsWorking()
    {
        return (bool)\DB::table('settings')
            ->where('name', 'wifimanager.wifiengine_is_working')
            ->value('value');
    }

    /**
     * Log message
     *
     * @param $message
     *
     * @return int
     */
    private function log($message)
    {
        return file_put_contents(storage_path('logs/') . '/engine-' . date('Y-m-d') . '.log', "\n" . date("[d-m-Y H:i:s] ") . $message, FILE_APPEND);
    }
}