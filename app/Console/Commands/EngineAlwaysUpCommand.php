<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Symfony\Component\Process\Process;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class EngineAlwaysUpCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:alwaysup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run engine if it is not working';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $engineLastWorkingTime = $this->getEngineLastWorkingTime();

            $runEngine = ($engineLastWorkingTime === false) || ($engineLastWorkingTime->diffInMinutes() > 3);

            if ($runEngine) {
                $process = new Process('php artisan engine:run');
                $process->start();
            }

        } catch (FileNotFoundException $e) {
            $this->log($e->getMessage());

            return -1;
        }

        return 0;
    }

    /**
     * Get last working time of the engine
     *
     * @return bool|Carbon
     */
    public function getEngineLastWorkingTime()
    {
        $result = false;

        try {
            $lastWorkingTime = \DB::table('settings')
                ->where('name', 'wifimanager.wifiengine_last_working_time')
                ->value('value');

            $result = $lastWorkingTime ? Carbon::parse($lastWorkingTime) : false;
        } catch (\Exception $e) {
            $this->log($e->getMessage());
        }

        return $result;
    }

    /**
     * Log message
     *
     * @param $message
     *
     * @return int
     */
    private function log($message)
    {
        return file_put_contents(storage_path('logs/') . '/engine-always-up-' . date('Y-m-d') . '.log', "\n" . date("[d-m-Y H:i:s] ") . $message, FILE_APPEND);
    }
}