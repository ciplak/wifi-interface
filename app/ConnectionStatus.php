<?php

namespace App;

final class ConnectionStatus
{
    const DISCONNECTED = 0;
    const CONNECTED = 1;

    /**
     * Return label list
     *
     * @return array
     */
    public static function labels()
    {
        return [
            self::DISCONNECTED => trans('guest.connection_status.offline'),
            self::CONNECTED    => trans('guest.connection_status.online'),
        ];
    }

    /**
     * Return label of the given value
     *
     * @param $value
     *
     * @return mixed|string
     */
    public static function label($value)
    {
        $values = self::labels();

        return $values[$value] ?? '';
    }

    /**
     * Return values
     *
     * @return array
     */
    public static function values()
    {
        return array_keys(self::labels());
    }
}
