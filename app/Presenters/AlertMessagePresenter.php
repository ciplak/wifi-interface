<?php

namespace App\Presenters;


use App\Models\AlertMessage;
use App\Repositories\AlertMessageRepository;
use McCool\LaravelAutoPresenter\BasePresenter;

/**
 * Class AlertMessagePresenter
 */
class AlertMessagePresenter extends BasePresenter
{

    /**
     * @var AlertMessageRepository
     */
    private $alertMessages;

    /**
     * AlertMessagePresenter constructor.
     *
     * @param AlertMessageRepository $alertMessages
     * @param AlertMessage           $resource
     */
    public function __construct(AlertMessageRepository $alertMessages, AlertMessage $resource)
    {
        $this->alertMessages = $alertMessages;
        $this->wrappedObject = $resource;
    }

    /**
     * Get priority value as string
     *
     * @return string|int
     */
    public function priority_value()
    {
        $priorityOptions = $this->alertMessages->getPriorityOptions();
        $priority = $this->wrappedObject->priority;

        return $priorityOptions[$priority] ?? $priority;
    }

    /**
     * Get bootstrap label value for priority
     *
     * @return string
     */
    public function priority_label_type()
    {
        $labelTypes = [
            AlertMessage::PRIORITY_LOW    => 'success',
            AlertMessage::PRIORITY_MEDIUM => 'warning',
            AlertMessage::PRIORITY_HIGH   => 'danger',
        ];

        $priority = $this->wrappedObject->priority;

        return $labelTypes[$priority] ?? 'default';
    }

}