<?php

namespace App\Presenters;


use App\Models\GuestDevice;
use App\Repositories\GuestRepository;
use McCool\LaravelAutoPresenter\BasePresenter;

class GuestDevicePresenter extends BasePresenter
{
    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * GuestDevicePresenter constructor.
     *
     * @param GuestDevice     $resource
     * @param GuestRepository $guests
     */
    public function __construct(GuestDevice $resource, GuestRepository $guests)
    {
        $this->wrappedObject = $resource;
        $this->guests = $guests;
    }

    /**
     * Get connection status icon for the guest device
     *
     * @return string
     */
    public function connection_status_icon()
    {
        $statuses = \App\ConnectionStatus::labels();

        $isOnline = $this->wrappedObject->connection_status == \App\ConnectionStatus::CONNECTED;

        $statusText = $statuses[$this->wrappedObject->connection_status];

        return $isOnline
            ? '<span class="label label-success">' . $statusText .  '</span>'
            : '<span class="label label-danger">' . $statusText . '</span>';
    }
}