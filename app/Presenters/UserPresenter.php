<?php

namespace App\Presenters;


use App\Models\Role;
use App\Models\User;
use McCool\LaravelAutoPresenter\BasePresenter;

class UserPresenter extends BasePresenter
{

    public function __construct(User $resource)
    {
        $this->wrappedObject = $resource;
    }

    /**
     * Get the user's role
     *
     * @return Role|null
     */
    private function findRole()
    {
        $roles = $this->wrappedObject->roles;
        return  $roles->count() ? $roles->first() : null;
    }

    /**
     * Get the id of the user's role
     *
     * @return int|null
     */
    public function role_id()
    {
        $role = $this->findRole();
        return $role ? $role->id : null;
    }

    /**
     * Get the name of the user's role
     *
     * @return string|null
     */
    public function role()
    {
        $role = $this->findRole();
        return $role ? $role->name : null;
    }

    /**
     * Get the display name of the user's role
     *
     * @return string|null
     */
    public function role_display_name()
    {
        $role = $this->findRole();
        return $role ? $role->display_name : null;
    }

    /**
     * Get tenant name of the user
     *
     * @return string
     */
    public function tenant_name()
    {
        return $this->wrappedObject->tenant_id ? $this->wrappedObject->tenant->name : '';
    }

    /**
     * Get status text
     *
     * @return string
     */
    public function status_text()
    {
        if ($this->wrappedObject->status == User::STATUS_BANNED) {
            $class = 'danger';
            $text = trans('user.status.banned');
        } else {
            $class = 'success';
            $text = trans('user.status.active');
        }

        return '<span class="label label-' . $class . '">' . $text . '</span>';
    }

    /**
     * Get user location name
     *
     * @return string
     */
    public function location_name()
    {
        return $this->wrappedObject->property_id ? $this->wrappedObject->property->name : '';
    }
}