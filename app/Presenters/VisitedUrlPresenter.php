<?php

namespace App\Presenters;


use App\Models\GuestDevice;
use App\Models\VisitedUrl;
use Carbon\Carbon;
use McCool\LaravelAutoPresenter\BasePresenter;

class VisitedUrlPresenter extends BasePresenter
{
    /**
     * VisitedUrlLogPresenter constructor.
     *
     * @param VisitedUrl $resource
     */
    public function __construct(VisitedUrl $resource)
    {
        $this->wrappedObject = $resource;
    }

    /**
     * Get source IP and port
     *
     * @return string
     */
    public function source_with_port()
    {
        return $this->wrappedObject->source_ip . ($this->wrappedObject->source_port ? ':' . $this->wrappedObject->source_port : '');
    }

    /**
     * Get destination IP and port
     *
     * @return string
     */
    public function destination_with_port()
    {
        return $this->wrappedObject->destination_ip . ($this->wrappedObject->destination_port ? ':' . $this->wrappedObject->destination_port : '');
    }

    /**
     * Get visit date
     *
     * @return string
     */
    public function visited_at_formatted()
    {
        return $this->wrappedObject->visited_at ? Carbon::parse($this->wrappedObject->visited_at)->tz(setting('timezone'))->format(setting('datetime_format')) : '';
    }

    /**
     * Get guest device type name
     *
     * @return string
     */
    public function guest_device_type_name()
    {
        $deviceTypes = GuestDevice::getDeviceTypes();

        return is_null($this->wrappedObject->device_type_id) ? null  : ($deviceTypes[$this->wrappedObject->device_type_id] ?? '');
    }
}