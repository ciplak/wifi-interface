<?php

namespace App\Presenters;


use App\ConnectionStatus;
use App\Models\Guest;
use App\Repositories\GuestRepository;
use Carbon\Carbon;
use McCool\LaravelAutoPresenter\BasePresenter;

class GuestPresenter extends BasePresenter
{
    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * GuestPresenter constructor.
     *
     * @param Guest           $resource
     * @param GuestRepository $guests
     */
    public function __construct(Guest $resource, GuestRepository $guests)
    {
        $this->wrappedObject = $resource;
        $this->guests = $guests;
    }

    /**
     * Get guest's gender
     *
     * @return string
     */
    public function gender_name()
    {
        $options = Guest::getGenderOptions();

        $gender = $this->wrappedObject->gender;

        return $options[$gender] ?? '';
    }

    /**
     * Get icon for guest's gender
     *
     * @return string
     */
    public function gender_icon()
    {
        $icons = [
            Guest::GENDER_UNKNOWN => 'question',
            Guest::GENDER_MALE    => 'male',
            Guest::GENDER_FEMALE  => 'female',
        ];

        $gender = $this->wrappedObject->gender;
        if (!isset($icons[$gender])) { $gender = Guest::GENDER_UNKNOWN;
        }

        $icon = $icons[$gender];

        return '<i class="icon-circle fa fa-' . $icon . '"></i>';
    }

    /**
     * Get guest age
     *
     * @return int
     */
    public function age()
    {
        $birthday = $this->wrappedObject->birthday;

        return $birthday ? Carbon::createFromFormat('Y-m-d', $birthday)->age : '';
    }

    /**
     * Get social network icon
     *
     * @return string
     */
    public function social_icon()
    {

        $icon = '';
        if ($this->wrappedObject->facebook_id) {
            $icon = 'facebook';
        } else if ($this->wrappedObject->twitter_id) {
            $icon = 'twitter';
        }

        return $icon ? '<i class="icon-circle fa fa-' . $icon . '"></i>' : '';
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function avatar_picture()
    {
        $avatar = $this->wrappedObject->avatar;

        return $avatar ? \HTML::image($avatar, $this->wrappedObject->name, ['class' => 'avatar']) : '';
    }

    /**
     * Get last visit date of the guest
     *
     * @return string
     */
    public function last_visit_time_formatted()
    {
        return $this->wrappedObject->last_visit_time ? $this->wrappedObject->last_visit_time->tz(setting('timezone'))->format(setting('datetime_format')) : '';
    }

    /**
     * Get create date of the guest
     *
     * @return string
     */
    public function created_at_formatted()
    {
        return $this->wrappedObject->created_at ? $this->wrappedObject->created_at->tz(setting('timezone'))->format(setting('datetime_format')) : '';
    }

    /**
     * Get birthday of the guest
     *
     * @return string
     */
    public function birthday_formatted()
    {
        $birthday = $this->wrappedObject->birthday;

        return $birthday ? Carbon::createFromFormat('Y-m-d', $birthday)->format(setting('date_format')) : '';
    }

    /**
     * Get birthday of the guest
     *
     * @return string
     */
    public function birthday_picker_formatted()
    {
        $birthday = $this->wrappedObject->birthday;

        return $birthday ? Carbon::createFromFormat('Y-m-d', $birthday)->format('d/m/Y') : '';
    }

    /**
     * Get CSS class for the connection status of the guest
     *
     * @return string
     */
    public function connection_status_class()
    {
        return $this->is_online() ? 'success' : 'danger';
    }

    /**
     * Check if the guest is online
     *
     * @return bool
     */
    public function is_online()
    {
        return $this->wrappedObject->connection_status == \App\ConnectionStatus::CONNECTED;
    }

    /**
     * Get connection status text of the guest
     *
     * @return string
     */
    public function connection_status_value()
    {
        return ConnectionStatus::label($this->wrappedObject->connection_status);
    }

    /**
     * Get nationality of the guest as string
     *
     * @return string
     */
    public function nationality_name()
    {
        $nationalities = $this->guests->getNationalityOptions();
        $nationality = $this->wrappedObject->nationality;

        return $nationalities[$nationality] ?? '';
    }

    /**
     * Get guest venue name
     *
     * @return string
     */
    public function venue_name()
    {
        return $this->wrappedObject->venue_id ? $this->wrappedObject->venue->name : '';
    }

    /**
     * Get link for the guest's email
     *
     * @return string
     */
    public function email_link()
    {
        return $this->wrappedObject->email ? \HTML::email($this->wrappedObject->email) : '';
    }

    /**
     * Get the guest's display name
     *
     * @return string
     */
    public function display_name()
    {
        if ($this->wrappedObject->name) {
            return $this->wrappedObject->name;
        } else if ($this->wrappedObject->email) {
            return $this->wrappedObject->email;
        } else if ($this->wrappedObject->phone) {
            return $this->wrappedObject->phone;
        }

        return $this->wrappedObject->username;
    }

    /**
     * Get downloaded data
     *
     * @return string
     */
    public function downloaded_data_human_readable()
    {
        return human_filesize($this->wrappedObject->downloaded_data);
    }

    /**
     * Get downloaded data
     *
     * @return string
     */
    public function uploaded_data_human_readable()
    {
        return human_filesize($this->wrappedObject->uploaded_data);
    }


}