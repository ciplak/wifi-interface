<?php

namespace App\Presenters;


use App\Models\Campaign;
use App\Repositories\CampaignRepository;
use McCool\LaravelAutoPresenter\BasePresenter;

class CampaignPresenter extends BasePresenter
{
    /**
     * @var CampaignRepository
     */
    private $campaign;

    /**
     * CampaignPresenter constructor.
     *
     * @param Campaign           $resource
     * @param CampaignRepository $campaign
     */
    public function __construct(Campaign $resource, CampaignRepository $campaign)
    {
        $this->wrappedObject = $resource;
        $this->campaign = $campaign;
    }

    /**
     * Get status icon html
     *
     * @return string
     */
    public function status_icon()
    {
        if ($this->wrappedObject->status == Campaign::STATUS_PUBLISHED) {
            return '<span class="label label-success">' . trans('campaign.status.published') . '</span>';
        } else {
            return '<span class="label label-danger">' . trans('campaign.status.draft') . '</span>';
        }
    }

    /**
     * Get start date value
     *
     * @return string|null
     */
    public function start_date_value()
    {
        return $this->wrappedObject->start_date ? $this->wrappedObject->start_date->toDateString() : null;
    }

    /**
     * Get end date value
     *
     * @return string|null
     */
    public function end_date_value()
    {
        return $this->wrappedObject->end_date ? $this->wrappedObject->end_date->toDateString() : null;
    }

    /**
     * @return mixed
     */
    public function location_name()
    {
        return $this->wrappedObject->location_id ? $this->wrappedObject->location->name : trans('venue.all');
    }

    /**
     * @return array
     */
    public function parameter_value()
    {
        return $this->wrappedObject->parameter_type ? $this->campaign->getParameterTypeOptions()[$this->wrappedObject->parameter_type] : '-';
    }


    /**
     * @return mixed|string
     */
    public function paremeter_type_time()
    {
        $paremeter_type_time = substr($this->wrappedObject->parameter_type_time, 0, 5);
        return array_search($paremeter_type_time, config('times.time'));
    }
}