<?php

namespace App\Presenters;


use App\Models\SmsLog;
use McCool\LaravelAutoPresenter\BasePresenter;

/**
 * Class SmsLogPresenter
 */
class SmsLogPresenter extends BasePresenter
{
    /**
     * SmsLogPresenter constructor.
     *
     * @param SmsLog $resource
     */
    public function __construct(SmsLog $resource)
    {
        $this->wrappedObject = $resource;
    }

    /**
     * Get creation datetime
     *
     * @return string
     */
    public function created_at_formatted()
    {
        return $this->wrappedObject->created_at->tz(setting('timezone'))->format(setting('datetime_format'));
    }

    public function ref_no()
    {
        return $this->wrappedObject->data->get('ref_no');
    }

    public function credit()
    {
        return $this->wrappedObject->data->get('credit');
    }

    public function description()
    {
        return $this->wrappedObject->data->get('description');
    }
}