<?php

namespace App\Presenters;


use App\ConnectionStatus;
use App\Models\WifiSession;
use App\Repositories\GuestRepository;
use McCool\LaravelAutoPresenter\BasePresenter;

class WifiSessionPresenter extends BasePresenter
{
    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * WifiSessionPresenter constructor.
     *
     * @param WifiSession     $resource
     * @param GuestRepository $guests
     */
    public function __construct(WifiSession $resource, GuestRepository $guests)
    {
        $this->wrappedObject = $resource;
        $this->guests = $guests;
    }

    /**
     * Formatted total duration
     * 
     * @return string
     */
    public function total_duration()
    {
        return gmdate('H:i:s', (int)$this->wrappedObject->duration);
    }

    /**
     * Get device name
     *
     * @return string
     */
    public function device_name()
    {
        return $this->wrappedObject->device ? $this->wrappedObject->device->name : '';
    }

    /**
     * Get property name
     *
     * @return string
     */
    public function property_name()
    {
        return $this->wrappedObject->device ? $this->wrappedObject->device->property->name : '';
    }

    /**
     * Get downloaded data
     *
     * @return string
     */
    public function downloaded_data_human_readable()
    {
        return human_filesize($this->wrappedObject->downloaded_data);
    }

    /**
     * Get uploaded data
     *
     * @return string
     */
    public function uploaded_data_human_readable()
    {
        return human_filesize($this->wrappedObject->uploaded_data);
    }

    /**
     * Get connection status icon
     *
     * @return string
     */
    public function connection_status_icon()
    {
        $cssClass = ($this->wrappedObject->connection_status == ConnectionStatus::CONNECTED  ? 'success' : 'danger');

        return '<span class="label label-' . $cssClass . '">' . ConnectionStatus::label($this->wrappedObject->connection_status) . '</span>';
    }

    /**
     * Get start time of the session
     *
     * @return string
     */
    public function start_time_formatted()
    {
        return $this->wrappedObject->start_time ? $this->wrappedObject->start_time->tz(setting('timezone'))->format(setting('datetime_format')) : '';
    }

    /**
     * Get CSS class for the connection status of the guest
     *
     * @return string
     */
    public function connection_status_class()
    {
        return $this->is_online() ? 'success' : 'danger';
    }

    /**
     * Get connection status text of the guest
     *
     * @return string
     */
    public function connection_status_value()
    {
        return ConnectionStatus::label($this->wrappedObject->connection_status);
    }

    /**
     * Check if the guest is online
     *
     * @return bool
     */
    public function is_online()
    {
        return $this->wrappedObject->connection_status == ConnectionStatus::CONNECTED;
    }
}