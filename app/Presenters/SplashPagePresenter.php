<?php

namespace App\Presenters;


use App\Models\SplashPage;
use McCool\LaravelAutoPresenter\BasePresenter;

class SplashPagePresenter extends BasePresenter
{

    public function __construct(SplashPage $resource)
    {
        $this->wrappedObject = $resource;
    }

    /**
     * Return the name of the user created the login page
     *
     * @return string
     */
    public function created_by_name()
    {
        return $this->wrappedObject->created_by ? $this->wrappedObject->createdBy->name : '';
    }

    /**
     * Return the name of the user updated the login page
     *
     * @return string
     */
    public function updated_by_name()
    {
        return $this->wrappedObject->updated_by ? $this->wrappedObject->updatedBy->name : '';
    }

    /**
     * Get the login page type
     *
     * @return mixed
     */
    public function type_name()
    {
        $types = SplashPage::getTypeOptions();
        
        return $types[$this->wrappedObject->type];
    }

    /**
     * Get the login page icon
     *
     * @return mixed
     */
    public function type_icon()
    {
        return $this->wrappedObject->type == SplashPage::TYPE_OFFLINE
            ? '<span class="label label-danger">' . $this->type_name() . '</span>'
            : '<span class="label label-success">' . $this->type_name() . '</span>';
    }

    /**
     * Get creation datetime
     *
     * @return string
     */
    public function created_at_formatted()
    {
        return $this->wrappedObject->created_at->tz(setting('timezone'))->format(setting('datetime_format'));
    }
}