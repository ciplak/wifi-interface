<?php

namespace App\Presenters;


use App\Models\ProximityMarketingCampaign;
use App\Repositories\ProximityMarketingCampaignRepository;
use McCool\LaravelAutoPresenter\BasePresenter;

class ProximityMarketingCampaignPresenter extends BasePresenter
{
    /**
     * @var ProximityMarketingCampaignRepository
     */
    private $proximityMarketingCampaings;

    /**
     * ProximityMarketingCampaign constructor.
     *
     * @param ProximityMarketingCampaign $resource
     * @param ProximityMarketingCampaignRepository $proximityMarketingCampaings
     */
    public function __construct(ProximityMarketingCampaign $resource, ProximityMarketingCampaignRepository $proximityMarketingCampaings)
    {
        $this->wrappedObject = $resource;
        $this->proximityMarketingCampaings = $proximityMarketingCampaings;
    }

    /**
     * Get status icon html
     *
     * @return string
     */
    public function status_icon()
    {
        if ($this->wrappedObject->status == ProximityMarketingCampaign::STATUS_PUBLISHED) {
            return '<span class="label label-success">' . trans('campaign.status.published') . '</span>';
        } else {
            return '<span class="label label-danger">' . trans('campaign.status.draft') . '</span>';
        }
    }

    /**
     * Get start date value
     *
     * @return string|null
     */
    public function start_date_value()
    {
        return $this->wrappedObject->start_date ? $this->wrappedObject->start_date->toDateString() : null;
    }

    /**
     * Get end date value
     *
     * @return string|null
     */
    public function end_date_value()
    {
        return $this->wrappedObject->end_date ? $this->wrappedObject->end_date->toDateString() : null;
    }

    /**
     * @return string
     */
    public function event_type_value()
    {
        return $this->wrappedObject->event_type ? $this->proximityMarketingCampaings->eventTypeOptions()[$this->wrappedObject->event_type] : '-';
    }

    /**
     * @return array
     */
    public function weekdays_value()
    {
        foreach ($this->proximityMarketingCampaings->weekDays() as $key => $val) {
            $weekDays = explode(",", $this->wrappedObject->parameter_type_weekdays);
            $desiredWeekDays[$key]['value'] = $val;
            $desiredWeekDays[$key]['isChecked'] = false;
            $desiredWeekDays[$key]['key'] = $key;

            if (isset($this->wrappedObject->parameter_type_weekdays)) {
                foreach ($weekDays as $weekId) {
                    if ($key == $weekId) {
                        $desiredWeekDays[$weekId]['value'] = $val;
                        $desiredWeekDays[$weekId]['isChecked'] = true;
                        $desiredWeekDays[$key]['key'] = $key;
                    }
                }
            }
        }

        return $desiredWeekDays;
    }

    /**
     * @param bool $hasSingle
     *
     * @return array
     */
    public function communication_channel_value($hasSingle = false)
    {

        if (isset($this->wrappedObject->communication_channel)) {
            $virtualCommunicationChannels = explode(",", $this->wrappedObject->communication_channel);
            foreach ($virtualCommunicationChannels as $channelId) {
                $sortedCommunicationChannels[$channelId] = $this->proximityMarketingCampaings->communicationChannelTypeOptions()[$channelId];
                if ($hasSingle) {
                    return $this->proximityMarketingCampaings->communicationChannelTypeOptions()[$channelId];
                }
            }
        } else {
            $sortedCommunicationChannels = $this->proximityMarketingCampaings->communicationChannelTypeOptions();
        }

        return $sortedCommunicationChannels;
    }


}