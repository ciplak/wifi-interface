<?php

namespace App\Presenters;


use App\Models\Advertisement;
use App\Repositories\AdvertisementRepository;
use McCool\LaravelAutoPresenter\BasePresenter;

class AdvertisementPresenter extends BasePresenter
{
    /**
     * @var AdvertisementRepository
     */
    private $advertisements;

    /**
     * AdvertisementPresenter constructor.
     *
     * @param Advertisement           $resource
     * @param AdvertisementRepository $advertisements
     */
    public function __construct(Advertisement $resource, AdvertisementRepository $advertisements)
    {
        $this->wrappedObject = $resource;
        $this->advertisements = $advertisements;
    }

    /**
     * Get status icon html
     *
     * @return string
     */
    public function status_icon()
    {
        if ($this->wrappedObject->status == Advertisement::STATUS_PUBLISHED) {
            return '<span class="label label-success">' . trans('advertisement.status.published') . '</span>';
        } else {
            return '<span class="label label-danger">' . trans('advertisement.status.draft') . '</span>';
        }
    }

    /**
     * Get start date value
     *
     * @return string|null
     */
    public function start_date_value()
    {
        return $this->wrappedObject->start_date ? $this->wrappedObject->start_date->toDateString() : null;
    }

    /**
     * Get end date value
     *
     * @return string|null
     */
    public function end_date_value()
    {
        return $this->wrappedObject->end_date ? $this->wrappedObject->end_date->toDateString() : null;
    }

    /**
     * Get image url
     *
     * @return string
     */
    public function image_url()
    {
        return asset_url('img/ads/' . $this->wrappedObject->image);
    }

    /**
     * Get type value
     *
     * @return string
     */
    public function type_value()
    {
        $types = $this->advertisements->getTypeOptions();

        return $types[$this->wrappedObject->type] ?? '';
    }
}