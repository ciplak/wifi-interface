<?php

namespace App\Presenters;


use App\Models\ServiceProfile;
use App\Repositories\ServiceProfileRepository;
use Carbon\Carbon;
use McCool\LaravelAutoPresenter\BasePresenter;

class ServiceProfilePresenter extends BasePresenter
{

    /**
     * @var ServiceProfileRepository
     */
    private $serviceProfiles;

    /**
     * ServiceProfilePresenter constructor.
     *
     * @param ServiceProfile           $resource
     * @param ServiceProfileRepository $serviceProfiles
     */
    public function __construct(ServiceProfile $resource, ServiceProfileRepository $serviceProfiles)
    {
        $this->wrappedObject = $resource;
        $this->serviceProfiles = $serviceProfiles;
    }

    /**
     * Return whether the service profile is expired
     *
     * @return bool
     */
    public function is_expired()
    {
        $expiry_date = $this->wrappedObject->expiry_date;

        return $expiry_date && $expiry_date <= Carbon::today();
    }

    /**
     * Get duration value
     *
     * @return string
     */
    public function duration_value()
    {
        $durations = $this->serviceProfiles->getDurationTypeOptions();

        return $this->wrappedObject->duration . ' ' . $durations[$this->wrappedObject->duration_type];
    }

    /**
     * Get type value
     *
     * @return string
     */
    public function type_value()
    {
        $types = $this->serviceProfiles->getTypeOptions();
        $type = $this->wrappedObject->type;
        $result = $types[$type];

        if ($type == ServiceProfile::TYPE_RECURRING) {
            $frequencies = $this->serviceProfiles->getRenewalFrequencyTypeOptions();
            $result .= sprintf(" - %d %s", $this->wrappedObject->renewal_frequency, $frequencies[$this->wrappedObject->renewal_frequency_type]);
        }

        return $result;
    }

    /**
     * Get service pin value
     *
     * @return string
     */
    public function service_pin_value()
    {
        $types = $this->serviceProfiles->getServicePinTypeOptions();
        $service_pin = $this->wrappedObject->service_pin;

        return $types[$service_pin] ?? '';
    }

    /**
     * Get service profile status icon
     *
     * @return string
     */
    public function status_icon()
    {
        if ($this->wrappedObject->status == ServiceProfile::STATUS_ACTIVE) {
            return '<span class="label label-success"><i class="fa fa-check" ></i></span>';
        } else {
            return '<span class="label label-danger"><i class="fa fa-times" ></i></span>';
        }
    }

    /**
     * Get service profile price
     *
     * @return string
     */
    public function price_html()
    {
        return $this->wrappedObject->model == ServiceProfile::MODEL_PAID ? " <strong>({$this->wrappedObject->price})</strong>" : '';
    }

    /**
     * Get service profile model name
     *
     * @return mixed
     */
    public function model_name()
    {
        $models = $this->serviceProfiles->getModelOptions();

        return $models[$this->wrappedObject->model];
    }

    /**
     * Get service pin status html
     *
     * @return string
     */
    public function service_pin_html()
    {
        return $this->wrappedObject->service_pin
            ? '<span class="label label-success">' . $this->service_pin_value() . '</span>'
            : '<span class="label label-danger">' . $this->service_pin_value() . '</span>';
    }

    /**
     * Get service profile name and duration
     *
     * @return string
     */
    public function name_with_duration()
    {
        return $this->wrappedObject->name . ' - ' . $this->duration_value();
    }
}