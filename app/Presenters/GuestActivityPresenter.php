<?php

namespace App\Presenters;

use App\Models\GuestActivity;
use App\Models\GuestDevice;
use McCool\LaravelAutoPresenter\BasePresenter;

class GuestActivityPresenter extends BasePresenter
{
    /**
     * GuestActivityPresenter constructor.
     *
     * @param GuestActivity $resource
     */
    public function __construct(GuestActivity $resource)
    {
        $this->wrappedObject = $resource;
    }

    /**
     * Get create date of the guest activity
     *
     * @return string
     */
    public function created_at_formatted()
    {
        return $this->wrappedObject->created_at ? $this->wrappedObject->created_at->tz(setting('timezone'))->format(setting('datetime_format')) : '';
    }

    /**
     * Get guest device type name
     *
     * @return string
     */
    public function guest_device_type_name()
    {
        $deviceTypes = GuestDevice::getDeviceTypes();

        return $deviceTypes[$this->wrappedObject->device_type_id] ?? '';
    }
}