<?php

namespace App\Presenters;


use App\Models\AccessCode;
use App\Repositories\AccessCodeRepository;
use Carbon\Carbon;
use McCool\LaravelAutoPresenter\BasePresenter;

/**
 * Class AccessCodePresenter
 */
class AccessCodePresenter extends BasePresenter
{
    /**
     * @var AccessCodeRepository
     */
    private $accessCodes;

    /**
     * AccessCodePresenter constructor.
     *
     * @param AccessCodeRepository $accessCodes
     * @param AccessCode           $resource
     */
    public function __construct(AccessCodeRepository $accessCodes, AccessCode $resource)
    {
        $this->accessCodes = $accessCodes;
        $this->wrappedObject = $resource;
    }

    /**
     * Return whether the access code is expired
     *
     * @return bool
     */
    public function is_expired()
    {
        $expiry_date = $this->wrappedObject->expiry_date;

        return $expiry_date && $expiry_date <= Carbon::today();
    }

    /**
     * Get access code expiry date
     *
     * @return string
     */
    public function expiry_date_formatted()
    {
        $expiry_date = $this->wrappedObject->expiry_date;

        return $expiry_date ? $expiry_date->tz(setting('timezone'))->format(setting('date_format')) : '';
    }

    /**
     * Get status text
     */
    public function status_value()
    {
        $status = $this->wrappedObject->status;
        if ($status == AccessCode::STATUS_USED) {
            return trans('access_code.status.used');
        } elseif ($this->is_expired()) {
            return trans('access_code.expired');
        } else {
            return trans('access_code.status.available');
        }
    }

    /**
     * Get status html
     */
    public function status_value_html()
    {
        $status = $this->wrappedObject->status;
        if ($status == AccessCode::STATUS_USED) {
            return '<span class="label label-danger">' . trans('access_code.status.used') . '</span>';
        } elseif ($this->is_expired()) {
            return '<span class="label label-danger">' . trans('access_code.expired') . '</span>';
        } else {
            return '<span class="label label-success">' . trans('access_code.status.available') . '</span>';
        }
    }

    /**
     * Get access code type
     *
     * @return mixed
     */
    public function type_name()
    {
        $types = $this->accessCodes->getTypes();

        return $types[$this->wrappedObject->type];
    }

    /**
     * Return the name of the user created the access code
     *
     * @return string
     */
    public function created_by_name()
    {
        return $this->wrappedObject->created_by ? $this->wrappedObject->createdBy->name : '';
    }

    /**
     * Return the name of the owner of the access code
     *
     * @return string
     */
    public function guest_display_name()
    {
        return $this->wrappedObject->guest_id ?  $this->wrappedObject->guest->display_name : '';
    }

    /**
     * Return the name of venue the access code belongs to
     *
     * @return string
     */
    public function venue_name()
    {
        return $this->wrappedObject->venue_id ? $this->wrappedObject->venue->name : '';
    }

    /**
     * Get service profile name of the access code
     *
     * @return string
     */
    public function service_profile_name()
    {
        return $this->wrappedObject->service_profile_id ? $this->wrappedObject->serviceProfile->name : '';
    }

    /**
     * Get creation datetime
     *
     * @return string
     */
    public function created_at_formatted()
    {
        return $this->wrappedObject->created_at->tz(setting('timezone'))->format(setting('datetime_format'));
    }
}