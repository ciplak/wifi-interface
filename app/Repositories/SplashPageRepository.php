<?php

namespace App\Repositories;


use App\Models\SplashPage;

/**
 * Class SplashPageRepository
 *
 * @property SplashPage $model
 */
class SplashPageRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return SplashPage::class;
    }

    /**
     * @return SplashPage[]
     */
    public function getAllPaginated()
    {
        $query = $this->model
            ->with(['createdBy', 'updatedBy', 'translations'])
            ->orderBy('type')
            ->orderBy('name')
            ->whereNull('parent_id')
            ->where(
                function($query) {

                    if (!\Entrust::can('login-pages.manage-global')) {
                        $query->where(
                            function($query) {
                                $query->whereNull('venue_id')
                                    ->orWhere('venue_id', \Auth::user()->property_id);
                            }
                        );
                    }
                }
            );

        return $query->paginate(config('main.per_page'));
    }

    /**
     * Get list of published login pages
     *
     * @param int $type
     *
     * @return mixed
     */
    public function getPublishedParentList($type = SplashPage::TYPE_OFFLINE, $tenantId = null)
    {
        $canManageGlobal = $this->model->canManageGlobal();

        if (is_null($tenantId)) {
            $query = $this->model->newQuery();
        } else {
            $query = $this->model
                ->newQueryWithoutScope(\HipsterJazzbo\Landlord\Landlord::class)
                ->where('tenant_id', $tenantId);
        }

        $query = $query->orderBy('name')
            ->where('type', $type)
            ->whereNull('parent_id')
            ->where(
                function($query) use ($canManageGlobal) {
                    $query->whereNull('venue_id');

                    if (!$canManageGlobal) {
                        $query->orWhere('venue_id', \Auth::user()->property_id);
                    }
                }
            );

        return $query->pluck('name', 'id')->all();
    }

    /**
     * Create a copy of the login page
     *
     * @param string $value
     *
     * @return SplashPage
     */
    public function replicateByHash($value)
    {
        /**
         * @var SplashPage $model
         */
        $model = $this->findByHash($value);

        /**
         * @var SplashPage $newModel
         */
        $newModel = $model->replicate();

        $newModel->venue_id = \Auth::user()->property_id;
        $newModel->status = SplashPage::STATUS_DRAFT;
        $newModel->name .= ' - Clone';

        return $newModel;
    }

    /**
     * Delete model by hash
     *
     * @param string $value
     *
     * @return mixed
     */
    public function deleteByHash($value)
    {
        $id = hashids_decode($value);

        /**
         * @var SplashPage $model
         */
        $model = $this->find($id);

        if (!$model) {
            return false;
        }
        if ($model->isInUse()) {
            return false;
        }

        $query = $this->model->where(
            function($query) use ($id) {
                $query->where('parent_id', $id)
                    ->orWhere('id', $id);
            }
        );

        if (!\Entrust::can('login-pages.manage-global')) {
            return $this->model->where('venue_id', \Auth::user()->property_id)->destroy($id);
        }

        return $query->delete();
    }

    /**
     * Save model by hash
     *
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value = null)
    {
        /** @var SplashPage $model */
        if ($value) {
            $id = hashids_decode($value);
            $model = $this->find($id);
        } else {
            $model = $this->makeModel();
        }

        $request = request();

        if ($model->is_global && !$model->canManageGlobal()) {
            abort(403);
        }

        $settings = $request->get('settings', []);

        // registration form fields
        $selectedRegistrationFormFields = $request->get('fields', []);

        $fieldOrder = null;

        // get fieldOrder
        parse_str($request->get('field-list'));

        $defaultFields = $model->getRegistrationFields();

        $list = [];
        foreach ($fieldOrder as $fieldName) {
            if (!isset($defaultFields[$fieldName])) {
                continue;
            }

            $item = $defaultFields[$fieldName];

            if (isset($defaultFields[$fieldName]['custom']) && $defaultFields[$fieldName]['custom']) {
                $item['title'] = $request->input("fields.{$fieldName}.title");
            }

            $item['enabled'] = array_get($selectedRegistrationFormFields, "{$fieldName}.enabled", 0);
            $isRequired = array_get($selectedRegistrationFormFields, "{$fieldName}.required", 0);

            $item['required'] = $isRequired;

            if ($isRequired) {
                $item['rules'][] = 'required';
            }

            $list[$fieldName] = $item;
        }

        $settings['registration_fields'] = $list;


        // login form fields
        $selectedLoginFormFields = $request->get('loginFields', []);

        $loginFieldOrder = null;

        // get fieldOrder
        parse_str($request->get('login-field-list'));

        $defaultFields = $model->getLoginFields();

        $list = [];
        foreach ($loginFieldOrder as $fieldName) {
            if (!isset($defaultFields[$fieldName])) {
                continue;
            }

            $item = $defaultFields[$fieldName];

            if (isset($defaultFields[$fieldName]['custom']) && $defaultFields[$fieldName]['custom']) {
                $item['title'] = $request->input("fields.{$fieldName}.title");
            }

            $item['enabled'] = isset($selectedLoginFormFields[$fieldName]) && array_get($selectedLoginFormFields, "{$fieldName}.enabled", 0);
            $item['required'] = $isRequired = true;
            if ($isRequired) {
                $item['rules'][] = 'required';
            }

            $list[$fieldName] = $item;
        }

        $settings['login_fields'] = $list;

        if ($model->exists) {
            $settings = array_merge($model->settings, $settings);
        }

        $model->fill($request->all());
        $model->send_sms = $request->input('send_sms', 0);
        $model->send_email = $request->input('send_email', 0);
        $model->social_login_requires_code = $request->input('social_login_requires_code', 0);
        $model->overwrite_guests = $request->input('overwrite_guests', 0);

        $model->settings = $settings;

        if (\Entrust::can('login-pages.manage')) {
            $model->venue_id = \Auth::user()->property_id;
        }

        $model->save();

        return $model;
    }

    public function getTypeOptions()
    {
        return [
            SplashPage::TYPE_OFFLINE => trans('splash_page.type.offline'),
            SplashPage::TYPE_ONLINE  => trans('splash_page.type.online'),
        ];
    }
}