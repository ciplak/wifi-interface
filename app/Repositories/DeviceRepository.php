<?php

namespace App\Repositories;


use App\Exceptions\DeviceNotFoundException;
use App\Models\Device;
use App\Models\Venue;
use App\Repositories\Criteria\WithTrashed;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * Class DeviceRepository
 *
 * @property Device $model
 */
class DeviceRepository extends BaseRepository
{
    public function findByHash($value, $columns = ['*'])
    {
        $id = hashids_decode($value);
        $this->applyCriteria();
        $result = $this->model->withTrashed()->find($id, $columns);

        if (!is_null($result)) {
            return $result;
        }

        throw (new ModelNotFoundException)->setModel(get_class($this->model));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Device::class;
    }

    /**
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value)
    {
        if (isset($attributes['property_id'])) {
            $attributes['property_id'] = hashids_decode($attributes['property_id']);
        }

        $this->pushCriteria(new WithTrashed());

        return parent::saveByHash($attributes, $value);
    }

    /**
     * Filter devices
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function getAllPaginated(Request $request)
    {
        $query = $this->model->with('property', 'model', 'model.vendor')
            ->orderBy('deleted_at')
            ->orderBy('id', 'DESC')
            ->when($request->has('name'), function ($query) use ($request) {
                return $query->where(function ($query) use ($request) {
                    $query->where('name', 'LIKE', "%{$request->name}%")
                        ->orWhere('description', 'LIKE', "%{$request->name}%");
                });
            })
            ->when($request->has('mac'), function ($query) use ($request) {
                $mac = str_replace(['-', '.'], ':', $request->input('mac'));

                return $query->where('mac', 'LIKE', "%{$mac}%");
            })
            ->if($request->has('ip'), 'ip', 'LIKE', '%' . $request->input('ip') . '%')
            ->if($request->has('device_model'), 'device_model_id', $request->input('device_model'));

        if ($request->has('status')) {
            switch ($request->input('status')) {
                case Device::STATUS_DELETED:
                    $query->onlyTrashed();
                    break;

                case Device::STATUS_ACTIVE:
                    break;

                default:
                    $query->withTrashed();
            }

        } else {
            $query->withTrashed();
        }

        if ($request->has('location')) {
            $location = hashids_decode($request->input('location'));

            $query->where('property_id', $location);

            $venue = Venue::where('id', $location)->first();

            if ($venue) {
                $query->whereIn('property_id', $venue->zones()->pluck('id'), 'or');
            }
        }

        $query = $this->applyPermissionCriteria($query);

        return $query->paginate(config('main.per_page'));
    }

    /**
     * Apply venue role permission
     *
     * @param $query
     *
     * @return mixed
     */
    private function applyPermissionCriteria($query)
    {
        // @todo use criteria

        // permissions

        if (!\Entrust::can('devices.manage-global')) {
            $venue = \Auth::user()->property;
            $query->whereIn('property_id', $venue->zones()->pluck('id'));
        }

        return $query;
    }

    /**
     * Restore a deleted device
     *
     * @param string $value
     *
     * @return mixed
     */
    public function restoreByHash($value)
    {
        $id = hashids_decode($value);

        $query = $this->model->withTrashed();

        $query = $this->applyPermissionCriteria($query);

        $model = $query->find($id);

        if (is_null($model)) {
            throw (new ModelNotFoundException)->setModel(get_class($this->model));
        }

        return $model->restore();
    }

    /**
     * Destroy the devices for the given IDs.
     *
     * @param array|int $values
     *
     * @return int
     */
    public function destroy($values)
    {
        $ids = is_array($values) ? $this->decodeList($values) : hashids_decode($values);

        $query = $this->model->whereIn('id', $ids);

        $query = $this->applyPermissionCriteria($query);

        return $query->delete();
    }

    /**
     * Decode list of hashes
     *
     * @param array $values
     *
     * @return array
     */
    private function decodeList($values)
    {
        return array_map(
            function ($value) {
                return hashids_decode($value);
            }, $values
        );
    }

    /**
     * Restore the devices for the given IDs.
     *
     * @param array|int $values
     *
     * @return mixed
     */
    public function restoreSelected($values)
    {
        $ids = $this->decodeList($values);

        $query = $this->model->whereIn('id', $ids);

        $query = $this->applyPermissionCriteria($query);

        return $query->restore();
    }

    /**
     * Get device status options
     *
     * @return array
     */
    public function getStatusOptions()
    {
        return $this->model->getStatusOptions();
    }

    /**
     * Get current access point
     *
     * @param $deviceMac
     *
     * @return Device
     * @throws DeviceNotFoundException
     */
    public function getCurrentDeviceByMac($deviceMac)
    {
        $deviceMac = str_replace('-', ':', $deviceMac);

        log_info('Trying to find access point by mac address', ['mac' => $deviceMac]);

        $device = $this->model
            ->allTenants()
            ->select('id', 'property_id', 'device_model_id', 'tenant_id')
            ->with('property')
            ->where('mac', $deviceMac)
            ->first();

        if ($device) {
            log_info('Found access point by mac address', ['mac' => $deviceMac]);
        } else {
            log_error('Could not found access point by mac address', ['mac' => $deviceMac]);
            throw new DeviceNotFoundException($deviceMac);
        }

        return $device;
    }

    /**
     * Get default device
     *
     * @return Device
     */
    public function getDefaultDevice()
    {
        return $this->model->first();
    }

}