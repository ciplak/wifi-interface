<?php
namespace App\Repositories\Criteria;

use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Criteria\Criteria;

class WithTrashed extends Criteria
{
    /**
     * @param string     $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        /**
 * @var \Illuminate\Database\Eloquent\Model $model 
*/
        return $model->newQueryWithoutScope(\Illuminate\Database\Eloquent\SoftDeletingScope::class);
    }
}