<?php
namespace App\Repositories\Criteria;

use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Criteria\Criteria;

class OrderBy extends Criteria
{
    /**
     * @var string
     */
    private $orderField;

    /**
     * OrderBy constructor.
     *
     * @param $orderField
     */
    public function __construct($orderField)
    {
        $this->orderField = $orderField;
    }

    /**
     * @param string     $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->orderBy($this->orderField);
    }
}