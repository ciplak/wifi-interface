<?php
namespace App\Repositories\Criteria;

use App\Models\Advertisement;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Criteria\Criteria;
use Carbon\Carbon;

class OnlyPublishedAds extends Criteria
{
    /**
     * @param Advertisement $model
     * @param Repository    $repository
     *
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $now = Carbon::today();

        $model = $model->where('status', Advertisement::STATUS_PUBLISHED)
            ->where('start_date', '<=', $now)
            ->where('end_date', '>=', $now);

        return $model;
    }
}