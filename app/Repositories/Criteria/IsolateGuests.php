<?php
namespace App\Repositories\Criteria;

use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Criteria\Criteria;

class IsolateGuests extends Criteria
{
    /**
     * @param            $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if (setting('isolate_guests') && !\Entrust::can('guests.manage-global')) {
            $model = $model->where('venue_id', \Auth::user()->property_id);
        }

        return $model;
    }
}