<?php

namespace App\Repositories;


use App\Models\AccessCode;
use App\Models\Guest;
use App\Repositories\Criteria\IsolateGuests;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class GuestRepository
 *
 * @property Guest $model
 */
class GuestRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Guest::class;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllPaginated(Request $request)
    {
        $this->pushCriteria(new IsolateGuests())
            ->applyCriteria();

        $query = $this
            ->model
            ->with('venue')
            ->select('guests.*')
            ->orderBy('guests.connection_status', 'DESC')
            ->orderBy('guests.id', 'DESC')
            ->when($request->has('name'), function ($query) use ($request) {
                return $query->where(function ($query) use ($request) {
                    $query->where('name', 'LIKE', "%{$request->name}%")
                        ->orWhere('username', 'LIKE', "%{$request->name}%");
                });
            })
            ->if($request->has('email'), 'email', 'LIKE', '%' . $request->input('email') . '%')
            ->if($request->has('phone'), 'phone', 'LIKE', '%' . $request->input('phone') . '%')
            ->if($request->has('connection_status'), 'guests.connection_status', $request->input('connection_status'));

        if ($request->has('mac')) {
            $query->join('guest_devices', 'guest_devices.guest_id', '=', 'guests.id')
                ->where('guest_devices.mac', 'LIKE', '%' . $request->input('mac') . '%');
        }

        if ($request->has('venue') && \Entrust::can('guests.manage-global')) {
            $venueId = hashids_decode($request->input('venue'));

            $query->where('venue_id', $venueId);
        }

        return $query->paginate(config('main.per_page'));
    }

    /**
     * @inheritdoc
     */
    public function findByHash($value, $columns = ['*'])
    {
        $this->pushCriteria(new IsolateGuests())
            ->applyCriteria();

        return parent::findByHash($value, $columns);
    }

    /**
     * @param string $value
     *
     * @return mixed
     */
    public function getDetailsByHash($value)
    {
        $id = hashids_decode($value);

        return $this->getDetails($id);
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getDetails($id)
    {
        $this->pushCriteria(new IsolateGuests())
            ->applyCriteria();

        $model = $this->model->with('devices.serviceProfile')->find($id);

        if ($model) {
            $model->load(['accessCodes.serviceProfile']);
        }

        return $model;
    }

    /**
     * Save model by hash
     *
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value)
    {
        if (\Entrust::can('guests.manage-global')) {
            $attributes['venue_id'] = hashids_decode($attributes['venue_id']);
        } else {
            $attributes['venue_id'] = \Auth::user()->property_id;
        }

        if (isset($attributes['birthday']) && trim($attributes['birthday'])) {
            $attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $attributes['birthday']);
        } else {
            $attributes['birthday'] = null;
        }

        return parent::saveByHash($attributes, $value);
    }

    /**
     * Get total online users count
     *
     * @param int|null $venueId
     * @param int|null $tenantId
     *
     * @return int
     */
    public function getOnlineCount($venueId = null, $tenantId = null)
    {
        $query = $this->model->where('connection_status', \App\ConnectionStatus::CONNECTED);

        if ($venueId) {
            $query->where('venue_id', $venueId);
        }

        if ($tenantId) {
            $query->where('tenant_id', $tenantId);
        }

        return (int)$query->count();
    }

    /**
     * Get demographics data for the guests
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDemographicsData(Request $request)
    {
        if (!\Entrust::can('venues.manage-global')) {
            $location = \Auth::user()->property_id;
        } else {
            $location = hashids_decode($request->input('location'));
        }

        $query = $this->model
            ->select('gender', 'age_range', \DB::raw('count(gender) AS total'))
            ->groupBy('gender')
            ->groupBy('age_range')
            ->whereIn(
                'id', function ($query) use ($request, $location) {
                    $query->select('guest_id')
                    //                    ->groupBy('guest_id')
                        ->distinct()
                        ->from('wifi_sessions')
                        ->whereNull('parent_session_id')
                        ->when(
                            $request->has('date'), function ($query) use ($request) {
                                $query->whereBetween('start_time', date_ranges($request->input('date')));
                            }
                        );

                    if ($location) {
                        $query->where('venue_id', $location);
                    }

                }
            );

        return $query->get();
    }

    /**
     * Get list of predefined age ranges
     *
     * @return array
     */
    public function getAgeRanges()
    {
        return [e('<20'), '21-25', '36-40', '41-55', e('>55'), trans('messages.unknown')];
    }

    /**
     * Get gender options
     *
     * @return array
     */
    public function getGenderOptions()
    {
        return $this->model->getGenderOptions();
    }

    /**
     * Get nationality options
     *
     * @return array
     */
    public function getNationalityOptions()
    {
        return $this->model->getNationalityOptions();
    }

    /**
     * @param Guest $model
     * @param int   $count
     *
     * @return mixed
     */
    public function getConnectionSessions($model, $count = 50)
    {
        if (!$model) return null;

        return $model->sessions()
            ->with('device.property', 'serviceProfile', 'guestDevice')
            ->orderBy('start_time', 'DESC')
            ->orderBy('connection_status', 'DESC')
            ->take($count)
            ->get();
    }

    /**
     * Get the first guest with the given attributes
     *
     * @param array $attributes
     *
     * @return Guest|null|
     */
    public function findByAttributes($attributes)
    {
        return $this->model->where($attributes)->first();
    }

    /**
     * @param int             $guestId
     * @param AccessCode|null $accessCode
     *
     * @return bool
     */
    public function setAccessCode(int $guestId, AccessCode $accessCode = null)
    {
        $code = $accessCode ? $accessCode->code : '';

        return $this->model->where('id', $guestId)->update(['access_code' => $code]);
    }

    /**
     * Update or create a guest
     *
     * @param array $attributes
     * @param array $values
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateOrCreate(array $attributes, array $values = [])
    {
        return $this->model->updateOrCreate($attributes, $values);
    }
}