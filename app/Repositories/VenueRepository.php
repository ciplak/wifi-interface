<?php

namespace App\Repositories;


use App\Events\Venue\Created;
use App\Events\Venue\Updated;
use App\Models\SplashPage;
use App\Models\Property;
use App\Models\Scopes\VenueScope;
use App\Models\Venue;

/**
 * Class VenueRepository
 *
 * @property Venue $model
 */
class VenueRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Venue::class;
    }

    /**
     * Return all venues with zones
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $request = request();

        $query = $this->model
            ->orderBy('name')
            ->with(
                [
                    'loginPage',
                    'postLoginPage',
                    'zones.loginPage',
                ]
            );

        if ($request->has('name')) {
            $name = $request->input('name');

            $query->where('name', 'like', '%' . $name . '%')
                ->orWhereHas(
                    'zones', function ($query) use ($name) {
                    /**
                     * @var \Illuminate\Database\Eloquent\Builder $query
                     */
                    $query->where('name', 'like', '%' . $name . '%');
                }
                );
        }

        $query->where('parent_id', activeTenantId());

        if (!\Entrust::can('venues.manage-global')) {
            $query->where('id', \Auth::user()->property_id);
        }

        return $query->get();
    }

    /**
     * Get the venues for the current tenant
     *
     * @param bool $hashIds
     *
     * @return array
     */
    public function getNameList($hashIds = true)
    {
        $query = $this->model
            ->orderBy('name')
            ->where('parent_id', \Auth::user()->tenant_id);

        if (!\Entrust::can('venues.manage-global')) {
            $query->where('id', \Auth::user()->property_id);
        }

        $items = $query->pluck('name', 'id')->toArray();

        if ($hashIds) {
            $data = [];

            foreach ($items as $key => $value) {
                $data[hashids_encode($key)] = $value;
            }

            $items = $data;
        }

        return $items;
    }

    /**
     * Get the venues for the current tenant
     *
     * @param bool $hashIds
     *
     * @return \Illuminate\Support\Collection
     */
    public function getNameListCollection($hashIds = true)
    {
        // @todo merge with getNameList method
        $query = $this->model
            ->orderBy('name')
            ->where('parent_id', \Auth::user()->tenant_id);

        if (!\Entrust::can('venues.manage-global')) {
            $query->where('id', \Auth::user()->property_id);
        }

        $items = $query->pluck('name', 'id');

        $items = $items->map(
            function ($name, $id) {
                return [$id, $name];
            }
        )->reduce(
            function ($list, $values) use ($hashIds) {
                list($id, $name) = $values;
                $key = $hashIds ? hashids_encode($id) : $id;
                $list[$key] = $name;

                return $list;
            }
        );

        return collect($items);
    }

    /**
     * @param bool $group
     *
     * @return array
     */
    public function getList($group = false)
    {
        $locations = [];

        $query = $this->model
            ->with('zones')
            ->orderBy('name')
            ->where('parent_id', \Auth::user()->tenant_id);

        if (!\Entrust::can('venues.manage-global')) {
            $query->where('id', \Auth::user()->property_id);
        }

        /**
         * @var Venue[] $venues
         */
        $venues = $query->get();

        foreach ($venues as $venue) {

            if ($group) {
                $key = $venue->name;
                $locations[$key] = [];
            } else {
                $key = $venue->getRouteKey();
                $locations[$key] = $venue->name;
            }

            $zones = $venue->zones;
            if ($zones) {

                foreach ($zones as $zone) {
                    if ($group) {
                        $locations[$key][$zone->getRouteKey()] = $zone->name;
                    } else {
                        $locations[$zone->getRouteKey()] = str_repeat('&nbsp;', 10) . $zone->name;
                    }
                }
            }

        }

        return $locations;
    }

    /**
     * Delete model by hash
     *
     * @param string $value
     *
     * @return mixed
     */
    public function deleteByHash($value)
    {
        $id = hashids_decode($value);

        \Auth::user()->canManageVenue($id);

        $query = $this->model
            ->where('is_default', '<>', 1)
            ->where('id', $id);

        if (\Entrust::hasRole('venue') && \Auth::user()->property_id == $id) {
            $query->where('id', '<>', \Auth::user()->property_id == $id);
        }

        $model = $query->first();

        return $model ? $model->delete() : false;
    }

    /**
     * Save model by hash
     *
     * @param array $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value)
    {
        $request = request();

        $id = hashids_decode($value);
        /**
         * @var Venue $model
         */
        $model = $this->model->findOrNew($id);

        $isNew = $model && !$model->exists;

        if (!$isNew) {
            \Auth::user()->canManageVenue($model);
        }

        $model->fill($request->all());

        if (!$request->has('login_page_id')) {
            $model->login_page_id = null;
        }

        if (!$request->has('sms_provider_id')) {
            $model->sms_provider_id = null;
        }

        $model->parent_id = \Auth::user()->tenant_id;

        unset($model->default_service_profile);

        $contents = $model->contents;

        $contents['terms_and_conditions'] = $request->input('terms_and_conditions');
        $model->contents = $contents;

        if (!$request->has('ssids')) {
            $model->ssids = [];
        }

        if ($status = $model->save()) {

            if ($request->has('is_default')) {
                $this->model->where('parent_id', $model->parent_id)->where('id', '<>', $model->id)->update(['is_default' => 0]);
            }

            //            $model->serviceProfiles()->sync($request->input('service_profiles', []));
            $model->serviceProfiles()->sync([$request->input('default_service_profile')]);

            $model->setDefaultServiceProvider($request->input('default_service_profile'));

            event($isNew ? new Created($model) : new Updated($model));
        }

        return $model;
    }

    /**
     * Set login page for the given locations
     *
     * @param SplashPage $loginPage
     * @param int $locationId
     *
     * @return bool|int
     */
    public function setLoginPage($loginPage, $locationId)
    {
        $fieldName = $loginPage->type == SplashPage::TYPE_ONLINE ? 'post_login_page_id' : 'login_page_id';

        /**
         * @var \Illuminate\Database\Eloquent\Builder $query
         */
        $query = $this->model->newQueryWithoutScope(VenueScope::class);

        if ($locationId) {
            $query->where('id', $locationId);
        } else {

            $idList = \DB::table('properties')
                ->where(
                    function ($q) {
                        $q->select('id')
                            ->from('properties')
                            ->where('parent_id', activeTenantId())
                            ->where('property_type_id', Property::TYPE_VENUE);
                    }
                )
                ->orWhereIn(
                    'parent_id', function ($q) {
                    $q->select('id')
                        ->from('properties')
                        ->where('parent_id', activeTenantId())
                        ->where('property_type_id', Property::TYPE_VENUE);
                }
                )
                ->pluck('id');

            $query->whereIn('id', $idList);
        }

        return $query->update([$fieldName => $loginPage->id]);
    }

}