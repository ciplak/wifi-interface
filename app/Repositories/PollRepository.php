<?php

namespace App\Repositories;


use App\Models\Poll;
use Carbon\Carbon;

/**
 * Class PollRepository
 *
 * @property Poll $model
 */
class PollRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Poll::class;
    }

    /**
     * Get poll by id
     *
     * @param int $id
     *
     * @return Poll
     */
    public function findActiveById($id)
    {
        return $this->model
            ->where('status', 1)
            ->whereNull('parent_id')
            ->find($id);
    }

    /**
     * Vote vor the given poll option
     *
     * @param $optionId
     */
    public function vote($optionId)
    {
        // @todo check if already voted

        $pollId = $this->model->where('id', $optionId)->value('parent_id');

        $this->model->where('id', $optionId)->increment('votes');

        \DB::table('poll_votes')->insert(
            [
            'poll_id'    => $pollId,
            'option_id'  => $optionId,
            'ip_address' => request()->ip(),
            'user_agent' => request()->header('User-Agent'),
            'created_at' => Carbon::now()
            ]
        );
    }

    public function checkIfVoted()
    {

    }
}