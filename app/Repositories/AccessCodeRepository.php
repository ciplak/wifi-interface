<?php

namespace App\Repositories;


use App\Models\AccessCode;
use App\Models\Guest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Class AccessCodeRepository
 *
 * @property AccessCode $model
 */
class AccessCodeRepository extends BaseRepository
{
    /**
     * Access code types
     *
     * @param bool $forCreate
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getTypes($forCreate = false)
    {
        $types = [
            AccessCode::TYPE_ACCESS_CODE => trans('access_code.type.access_code'),
            AccessCode::TYPE_SHARED      => trans('access_code.type.shared'),
        ];

        if (!$forCreate) {
            $types[AccessCode::TYPE_SMS] = trans('access_code.type.sms');
        }

        return collect($types);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return AccessCode::class;
    }

    /**
     * Filter access codes
     *
     * @param Request $request
     *
     * @return Collection|AccessCode[]
     */
    public function getAll(Request $request)
    {
        $query = $this->filter($request);

        return $query->get();
    }

    /**
     * Create query for filtered results
     *
     * @param Request $request
     *
     * @return mixed
     */
    private function filter(Request $request)
    {
        $query = $this->model->with('serviceProfile', 'createdBy', 'venue', 'guest')
            ->orderBy('status', 'desc')
            ->orderBy('id', 'DESC');

        if ($request->has('code')) {
            $query->where('code', 'LIKE', '%' . $request->input('code') . '%');
        }

        if ($request->has('type')) {
            $query->where('type', $request->input('type'));
        }
        if ($request->has('expired')) {
            $expired = $request->input('expired');
            $expired = (int)$expired;

            if ($expired === 0) {
                $query->notExpired();
            } else {
                if ($expired == 1) {
                    $query->expired();
                }
            }
        }

        if ($request->has('service_profile')) {
            $query->where('service_profile_id', $request->input('service_profile'));
        }

        if ($request->has('status')) {
            $query->where('status', $request->input('status'));
        }

        // permissions

        if (\Entrust::can('access-codes.manage-global')) {
            if ($request->has('venue')) {
                $venueId = hashids_decode($request->input('venue'));

                $query->where('venue_id', $venueId);
            }
        } else {
            $query->where('venue_id', \Auth::user()->property_id);
        }

        return $query;
    }

    /**
     * Get all access codes
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function getAllPaginated(Request $request)
    {
        $query = $this->filter($request);

        return $query->paginate(config('main.per_page'));
    }

    /**
     * Delete model by hash
     *
     * @param string $value
     *
     * @return mixed
     */
    public function deleteByHash($value)
    {
        $id = hashids_decode($value);

        $query = $this->model->where('id', $id);

        if (!\Entrust::can('access-codes.manage-global')) {
            $query->where('venue_id', \Auth::user()->property_id);
        }

        return $query->delete();
    }

    /**
     * Generate and return one or more access codes
     *
     * @param array $attributes
     * @param int   $accessCodeLength
     * @param int   $accessCodeChars
     *
     * @return AccessCode|\Illuminate\Support\Collection
     */
    public function generateCode($attributes = [], $accessCodeLength = 8, $accessCodeChars = AccessCode::CHARS_ALPHANUMERIC)
    {
        if (!$attributes) {
            $attributes = \Request::all();
        }

        $quantity = (int)Arr::get($attributes, 'quantity', 1);
        $serviceProfileId = (int)Arr::get($attributes, 'service_profile_id');
        $expiryDate = Arr::get($attributes, 'expiry_date');
        $type = (int)Arr::get($attributes, 'type', AccessCode::TYPE_ACCESS_CODE);
        $staticCode = Arr::get($attributes, 'code');
        $guestId = Arr::get($attributes, 'guest_id');

        if (!$expiryDate) {
            $expiryDate = null;
        }
        if ($quantity < 1 || $type == AccessCode::TYPE_SHARED) {
            $quantity = 1;
        }

        $models = [];

        for ($i = 0; $i < $quantity; $i++) {
            $code = $type == AccessCode::TYPE_SHARED ? $staticCode : AccessCode::generate($accessCodeLength, $accessCodeChars);

            $model = AccessCode::create(
                [
                'service_profile_id' => $serviceProfileId,
                'type'               => $type,
                'code'               => $code,
                'expiry_date'        => $expiryDate,
                'status'             => AccessCode::STATUS_ACTIVE,
                'created_by'         => \Auth::id(),
                'guest_id'           => $guestId,
                'venue_id'           => $attributes['venue_id'],
                'tenant_id'          => $attributes['tenant_id'] ?? activeTenantId(),
                ]
            );

            if ($model) {
                $models[] = $model;
            }
        }

        $result = collect($models);

        return $quantity == 1 ? $result->first() : $result;
    }

    /**
     * Generate an access code
     *
     * @param array $attributes
     * @param int   $accessCodeLength
     * @param int   $accessCodeChars
     *
     * @return AccessCode
     */
    public function generateSingleCode($attributes = [], $accessCodeLength = 8, $accessCodeChars = AccessCode::CHARS_ALPHANUMERIC)
    {
        $attributes['quantity'] = 1;

        return $this->generateCode($attributes, $accessCodeLength, $accessCodeChars);
    }

    /**
     * @param string $code
     * @param bool   $isSMSCode
     * @param int    $tenantId
     * @param int    $venueId
     *
     * @return AccessCode|bool
     */
    public function checkCode($code, $isSMSCode = false, $tenantId, $venueId)
    {
        log_info(
            'Checking access code', [
            'code' => $code,
            'SMS'  => $isSMSCode,
            ]
        );

        /**
         * @var AccessCode $accessCode
         */
        $query = $this->model
            ->where('code', $code)
            ->where('tenant_id', $tenantId)
            ->where('venue_id', $venueId)
            ->active()
            ->notExpired();

        if ($isSMSCode) {
            $query->where('type', AccessCode::TYPE_SMS);
        } else {
            $query->whereIn('type', [AccessCode::TYPE_SHARED, AccessCode::TYPE_ACCESS_CODE]);
        }

        $accessCode = $query->first();

        if (!$accessCode) {
            return false;
        }

        if ($accessCode->type != AccessCode::TYPE_SHARED) {
            $sharedDeviceCount = (int)config('front_page_settings.shared_device_count', 0);

            // exceeded use limit
            if ($accessCode->use_count > $sharedDeviceCount) {
                log_info('Use count limit is reached', ['access code' => $accessCode->code, 'limit' => $sharedDeviceCount]);

                return false;
            }

            // exceeded logout time
            // @todo update for reusable codes
            if (!is_null($accessCode->logout_time) && ($accessCode->logout_time < Carbon::now())) {
                return false;
            }
        }

        return $accessCode;
    }

    /**
     * Return first available access code for the given guest
     *
     * @param Guest $guest
     *
     * @return mixed
     */
    public function getFirstAvailableForGuest(Guest $guest)
    {
        return $this->model
            ->allTenants()
            ->where('guest_id', $guest->id)
            ->where('tenant_id', $guest->tenant_id)
            ->notExpired()
            ->active()
            ->where(function($query) {
                /** @var \Illuminate\Database\Query\Builder $query */
                $query->whereNull('logout_time')
                    ->orWhere('logout_time', '>', Carbon::now());
            })
            ->first();
    }

    /**
     * Returns list of statuses for access codes
     *
     * @return array
     */
    public function getStatusOptions()
    {
        return [
            AccessCode::STATUS_USED   => trans('access_code.status.used'),
            AccessCode::STATUS_ACTIVE => trans('access_code.status.available'),
        ];
    }

    /**
     * Get options for the expiry status
     *
     * @return array
     */
    public function getExpiredOptions()
    {
        return [
            0 => trans('access_code.not_expired'),
            1 => trans('access_code.expired'),
        ];
    }

    /**
     * Reactive the given access code
     *
     * @param AccessCode $accessCode
     *
     * @return bool
     */
    public function reactivate(AccessCode $accessCode)
    {
        return $accessCode->update(
            [
            'status'      => AccessCode::STATUS_ACTIVE,
            'logout_time' => null,
            'use_count'   => max(0, $accessCode->use_count - 1),
            ]
        );
    }

    /**
     * Mark the given access code as used and set logout time
     *
     * @param AccessCode $accessCode
     *
     * @return AccessCode
     */
    public function useCode(AccessCode $accessCode)
    {
        if ($accessCode->type == AccessCode::TYPE_SHARED) {
            return $accessCode;
        }

        $attributes = [];
        if ($accessCode->use_count == 0) {
            $attributes['logout_time'] = Carbon::now()->addMinutes($accessCode->serviceProfile->duration_total_minutes);

            log_info(
                'Access code is used for the first time. Logout time is set', [
                'access code' => $accessCode->code,
                'logout time' => $attributes['logout_time'],
                ]
            );
        }

        $attributes['use_count'] = ++$accessCode->use_count;

        $sharedDeviceCount = (int)config('front_page_settings.shared_device_count', 0);

        if ((config('front_page_settings.access_code.type') == AccessCode::USAGE_TYPE_ONE_TIME)
            && ($accessCode->use_count >= $sharedDeviceCount)
        ) {
            $attributes['status'] = AccessCode::STATUS_USED;
        }

        $accessCode->update($attributes);

        return $accessCode;
    }
}