<?php

namespace App\Repositories;

use App\Models\Setting;
use App\Models\SmsLog;


/**
 * Class Setting
 *
 * @property SmsLog $model
 */
class SettingRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Setting::class;
    }

    /**
     * Get value for single item
     *
     * @param string   $name
     * @param null|int $tenantId
     *
     * @return mixed
     */
    public function get($name, $tenantId = null)
    {
        $query = $this->model
            ->where('name', $name)
            ->take(1);

        $query = $this->applyTenantScope($query, $tenantId);

        return $query->value('value');
    }

    /**
     * Get all settings
     *
     * @param null|int $tenantId
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAll($tenantId = null)
    {
        $query = $this->model->newQuery();
        $query = $this->applyTenantScope($query, $tenantId);

        return $query->pluck('value', 'name');
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @param null   $tenantId
     *
     * @return mixed
     */
    public function set($name, $value, $tenantId = null)
    {
        $query = $this->model->where('name', $name);
        $query = $this->applyTenantScope($query, $tenantId);

        dd($query->toSql());

        return $query->update(['value', $value]);
    }

    /**
     * @param null $tenantId
     *
     * @return mixed
     */
    public function getKeys($tenantId = null)
    {
        $query = $this->model->newQuery();
        $query = $this->applyTenantScope($query, $tenantId);

        return $query->pluck('name');
    }

    /**
     * @param          $query
     * @param null|int $tenantId
     *
     * @return mixed
     */
    private function applyTenantScope($query, $tenantId = null)
    {
        return is_null($tenantId) ? $query->global() : $query->forTenant($tenantId);
    }

    /**
     * Get sent sms warning threshold
     *
     * @param int $tenantId
     *
     * @return int
     */
    public function getSentSmsWarningThreshold(int $tenantId)
    {
        return (int)$this->get('sms.warning_threshold', $tenantId);
    }

    /**
     * Get sms credits
     *
     * @param int $tenantId
     *
     * @return int
     */
    public function getSmsCredits(int $tenantId)
    {
        return (int)$this->get('sms.total_credits', $tenantId);
    }

    /**
     * Decrease sms credits
     *
     * @param int $tenantId
     *
     * @return int
     */
    public function decreaseSmsCredits(int $tenantId)
    {
        return $this->model
            ->forTenant($tenantId)
            ->where('name', 'sms.total_credits')
            ->decrement('value');
    }

    /**
     * Check sms threshold
     *
     * @param int $tenantId
     *
     * @return bool
     */
    public function checkThresholdWarning(int $tenantId)
    {
        // TODO optimize query
        return $this->getSentSmsWarningThreshold($tenantId) > $this->getSmsCredits($tenantId);
    }

    /**
     * Get to email address
     *
     * @return \Illuminate\Support\Collection
     */
    public function getToEmailAddress()
    {
        return $this->get('email.to');
    }
}