<?php

namespace App\Repositories;


use App\Models\SmsProvider;
use HipsterJazzbo\Landlord\Landlord;

/**
 * Class SmsProviderRepository
 *
 * @property SmsProvider $model
 */
class SmsProviderRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return SmsProvider::class;
    }

    /**
     * Get list of sms providers
     *
     * @return mixed
     */
    public function getList($tenantId = null)
    {
        if(is_null($tenantId)) {
            $query = $this->model->newQuery();
        }  else {
            $query = $this->model
                ->newQueryWithoutScope(\HipsterJazzbo\Landlord\Landlord::class)
                ->where('tenant_id', $tenantId);
        }
        return $query->pluck('name', 'id')->all();
    }

    /**
     * Gate gate way name list
     *
     * @return array
     */
    public static function getGatewayOptions()
    {
        return array_column(config('sms.gateways'), 'name', 'id');
    }

    /**
     * Save model
     *
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value)
    {
        $gateway_id = $attributes['gateway_id'];
        $parameters = array_get($attributes, 'parameters', []);
        $attributes['parameters'] = isset($parameters[$gateway_id]) ? $parameters[$gateway_id] : [];

        return parent::saveByHash($attributes, $value);
    }

}