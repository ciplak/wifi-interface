<?php

namespace App\Repositories;


use App\Models\Currency;

/**
 * Class CurrencyRepository
 *
 * @property Currency $model
 */
class CurrencyRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Currency::class;
    }

    /**
     * Update or create currency
     *
     * @param array $attributes
     * @param null  $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateOrCreate($attributes, $id = null)
    {
        return $this->model->updateOrCreate(['id' => $id], $attributes);
    }

}