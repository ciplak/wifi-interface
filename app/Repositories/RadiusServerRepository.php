<?php

namespace App\Repositories;


use App\Models\RadiusServer;

/**
 * Class RadiusServerRepository
 *
 * @property RadiusServer $model
 */
class RadiusServerRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return RadiusServer::class;
    }

    /**
     * Update or create radius server
     *
     * @param array $attributes
     * @param null  $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateOrCreate($attributes, $id = null)
    {
        $attributes['secret'] = \Crypt::encrypt($attributes['secret']);

        return $this->model->updateOrCreate(['id' => $id], $attributes);
    }

}