<?php

namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class BaseRepository
 */
abstract class BaseRepository extends Repository
{

    /**
     * @var Model
     */
    protected $model;

    /**
     * Return a new instance of the model
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function makeNew()
    {
        return $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return parent::model();
    }

    /**
     * Delete model by hash
     *
     * @param string $value
     *
     * @return mixed
     */
    public function deleteByHash($value)
    {
        $id = hashids_decode($value);

        return parent::delete($id);
    }

    /**
     * Find a model by its hashed primary key or throw an exception
     *
     * @param string $value
     * @param array  $columns
     *
     * @return mixed
     */
    public function findByHash($value, $columns = ['*'])
    {
        $id = hashids_decode($value);
        $result = parent::find($id, $columns);

        if (!is_null($result)) {
            return $result;
        }

        throw (new ModelNotFoundException)->setModel(get_class($this->model));
    }

    /**
     * Save model by hash
     *
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value)
    {
        $model = null;

        if ($value) {
            $id = hashids_decode($value);
            $model = $this->find($id);
        }

        if (is_null($model)) {
            return $this->create($attributes);
        } else {
            $this->updateRich($attributes, $id);

            return $model;
        }
    }

    /**
     * @param array  $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id") 
    {
        $data = $this->fillableFromArray($data);
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * Get the fillable attributes of a given array.
     *
     * @param  array $attributes
     * @return array
     */
    protected function fillableFromArray(array $attributes)
    {
        if (count($this->model->getFillable())) {
            return array_intersect_key($attributes, array_flip($this->model->getFillable()));
        }

        return $attributes;
    }

}