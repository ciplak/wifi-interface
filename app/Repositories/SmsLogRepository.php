<?php

namespace App\Repositories;

use App\Date;
use App\Models\SmsLog;
use Carbon\Carbon;
use Illuminate\Http\Request;


/**
 * Class SmsRepository
 *
 * @property SmsLog $model
 */
class SmsLogRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return SmsLog::class;
    }

    /**
     * Get SMS Count
     *
     * @param int $tenantId
     *
     * @return mixed
     */
    public function getSentSmsCount(int $tenantId)
    {
        return $this->model
            ->where('status', SmsLog::STATUS_SENT)
            ->where('tenant_id', $tenantId)
            ->whereBetween('created_at', Date::getRanges(Date::RANGE_TODAY))
            ->count();
    }

    /**
     *
     * @param int $tenantId
     *
     * @return \Illuminate\Support\Collection
     */
    public function getDailySmsUsageForLastMonth(int $tenantId)
    {
        $dateField = isSqlServer()
            ? 'CONVERT(DATE, created_at)'
            : 'DATE(created_at)';

        return \DB::table('sms_logs')
            ->select(
                \DB::raw('COUNT(id) AS total'),
                \DB::raw($dateField . ' AS date')
            )
            ->where('tenant_id', $tenantId)
            ->where('status', SmsLog::STATUS_SENT)
            ->where('created_at', '>=', Date::getRanges(Date::RANGE_LAST_30_DAYS))
            ->orderBy('date')
            ->groupBy(\DB::raw($dateField))
            ->pluck('total', 'date');

    }


    /**
     * @param int     $tenantId
     * @param Request $request
     *
     * @return int
     */
    public function installCredit($tenantId, Request $request)
    {
        $result = $this->model
            ->create(
                [
                'tenant_id' => $tenantId,
                'type'      => SmsLog::TYPE_INSTALL_CREDIT,
                'data'      => [
                    'ref_no'      => \date('Ymd') . str_random(8) . \date('Hi'),
                    'credit'      => $request->input('credit'),
                    'description' => $request->input('description'),
                ],
                ]
            );

        return $result ? $request->input('credit') : 0;
    }

    /**
     * @param $tenantId
     *
     * @return \Illuminate\Support\Collection
     */
    public function getInstalledSmsCreditList($tenantId)
    {
        return $this->model
            ->where('tenant_id', $tenantId)
            ->where('type', SmsLog::TYPE_INSTALL_CREDIT)
            ->latest()
            ->take(15)
            ->get();
    }

    /**
     * @param $tenantId
     *
     * @return float|int
     */
    public function getAverageDailySmsUsage($tenantId)
    {
        $monthlySmsUsage = $this->getDailySmsUsageForLastMonth($tenantId);
        $monthlySmsCount = $monthlySmsUsage->sum();
        $monthlySmsFirst = $monthlySmsUsage->keys()->first();
        $totalDateCount = isset($monthlySmsFirst) ? Carbon::now()->diffInDays(Carbon::createFromFormat('Y-m-d', $monthlySmsFirst)) + 1 : 1;
        $average_daily_sms_usage = isset($totalDateCount) ? ceil($monthlySmsCount / $totalDateCount) : 0;

        return $average_daily_sms_usage;
    }

    public function deleteCredit($tenantId, $id)
    {
        return $this->model
            ->where('tenant_id', $tenantId)
            ->where('id', $id)
            ->delete();
    }

    /**
     * @param $tenantId
     * @param $id
     *
     * @return SmsLog
     */
    public function findHistoryRecord($tenantId, $id)
    {
        return $this->model
            ->where('tenant_id', $tenantId)
            ->where('id', $id)
            ->first();
    }

    /**
     * @param int $tenantId
     * @param int $credits
     *
     * @return int
     */
    public function updateTotalSmsCredits(int $tenantId, int $credits)
    {
        $model = \DB::table('settings')->where('name', 'sms.total_credits')
            ->where('tenant_id', $tenantId)
            ->first();

        if ($model) {
            \DB::table('settings')
                ->where('name', 'sms.total_credits')
                ->increment('value', $credits);
        } else {
            \DB::table('settings')->insert(
                [
                'tenant_id' => $tenantId,
                'name'      => 'sms.total_credits',
                'value'     => $credits,
                ]
            );
        }
        return $credits;
    }
}