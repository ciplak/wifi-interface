<?php

namespace App\Repositories;


use App\Models\AlertMessage;
use Illuminate\Http\Request;

/**
 * Class AlertMessageRepository
 *
 * @property AlertMessage $model
 */
class AlertMessageRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return AlertMessage::class;
    }

    /**
     * Get number of alert messages
     *
     * @return int
     */
    public function getCount()
    {
        return $this->model->count();
    }

    /**
     * Get latest alert messages
     *
     * @param $count
     *
     * @return mixed
     */
    public function getLatest($count)
    {
        return $this->model->orderBy('id', 'DESC')->take($count)->get();
    }

    /**
     * Filter alert messages
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function getAllPaginated(Request $request)
    {
        $query = $this->model->orderBy('created_at', 'DESC');

        if ($request->has('priority')) {
            $query->where('priority', $request->input('priority'));
        }

        if ($request->has('message')) {
            $query->where('message', 'LIKE', '%' . $request->input('message') . '%');
        }

        return $query->paginate(config('main.per_page'));
    }

    /**
     * Get list of priorities
     *
     * @return array
     */
    public static function getPriorityOptions()
    {
        return [
            AlertMessage::PRIORITY_LOW    => trans('alert_message.priority.low'),
            AlertMessage::PRIORITY_MEDIUM => trans('alert_message.priority.medium'),
            AlertMessage::PRIORITY_HIGH   => trans('alert_message.priority.high'),
        ];
    }

}