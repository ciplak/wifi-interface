<?php

namespace App\Repositories;


use App\Date;
use App\Events\Guest\DeviceCreated;
use App\Models\Guest;
use App\Models\GuestDevice;

/**
 * Class GuestDeviceRepository
 *
 * @property GuestDevice $model
 */
class GuestDeviceRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return GuestDevice::class;
    }

    /**
     * Get guest device usage shares for the given interval
     *
     * @param int   $tenantId
     * @param array $parameters
     *
     * @return array
     */
    public function getUsageShares($tenantId, array $parameters = [])
    {
        $query = $this->model
            ->select('device_type_id', \DB::raw('count(*) as total'))
            ->leftJoin('wifi_sessions', 'guest_devices.id', '=', 'wifi_sessions.guest_device_id')
            ->whereNull('wifi_sessions.parent_session_id')
            ->where('wifi_sessions.tenant_id', $tenantId)
            ->groupBy('device_type_id');

        if (isset($parameters['date']) && $parameters['date']) {
            $query->whereBetween('wifi_sessions.start_time', Date::getRanges($parameters['date']));
        }

        if (isset($parameters['location'])) {
            $query->where('wifi_sessions.venue_id', $parameters['location']);
        }
        $data = $query->pluck('total', 'device_type_id')->toArray();

        return $data;
    }

    /**
     * Get guest device platform usage shares for the given interval
     *
     * @param int   $tenantId
     * @param array $parameters
     *
     * @return array
     */
    public function getPlatforms($tenantId, array $parameters = [])
    {
        $query = $this->model
            ->select('platform', \DB::raw('count(*) as total'))
            ->leftJoin('wifi_sessions', 'guest_devices.id', '=', 'wifi_sessions.guest_device_id')
            ->whereNull('wifi_sessions.parent_session_id')
            ->where('wifi_sessions.tenant_id', $tenantId)
            ->groupBy('platform');

        if (isset($parameters['date']) && $parameters['date']) {
            $query->whereBetween('wifi_sessions.start_time', Date::getRanges($parameters['date']));
        }

        if (isset($parameters['location'])) {
            $query->where('wifi_sessions.venue_id', $parameters['location']);
        }
        $data = $query->pluck('total', 'platform')->toArray();

        return $data;
    }

    /**
     * Get guest device browser usage shares for the given interval
     *
     * @param int   $tenantId
     * @param array $parameters
     *
     * @return array
     */
    public function getBrowsers($tenantId, array $parameters = [])
    {
        $query = $this->model
            ->select('browser', \DB::raw('count(*) as total'))
            ->leftJoin('wifi_sessions', 'guest_devices.id', '=', 'wifi_sessions.guest_device_id')
            ->whereNull('wifi_sessions.parent_session_id')
            ->where('wifi_sessions.tenant_id', $tenantId)
            ->groupBy('browser');

        if (isset($parameters['date']) && $parameters['date']) {
            $query->whereBetween('wifi_sessions.start_time', Date::getRanges($parameters['date']));
        }

        if (isset($parameters['location'])) {
            $query->where('wifi_sessions.venue_id', $parameters['location']);
        }
        $data = $query->pluck('total', 'browser')->toArray();

        return $data;
    }
    /**
     * Get guest device browser usage shares for the given interval
     *
     * @param int   $tenantId
     * @param array $parameters
     *
     * @return array
     */
    public function getDescriptions($tenantId, array $parameters = [])
    {
        $query = $this->model
            ->select('description', \DB::raw('count(*) as total'))
            ->leftJoin('wifi_sessions', 'guest_devices.id', '=', 'wifi_sessions.guest_device_id')
            ->whereNull('wifi_sessions.parent_session_id')
            ->where('wifi_sessions.tenant_id', $tenantId)
            ->groupBy('description');

        if (isset($parameters['date']) && $parameters['date']) {
            $query->whereBetween('wifi_sessions.start_time', Date::getRanges($parameters['date']));
        }

        if (isset($parameters['location'])) {
            $query->where('wifi_sessions.venue_id', $parameters['location']);
        }
        $data = $query->pluck('total', 'description')

            ->toArray();

        return $data;
    }

    /**
     * Get the guest device by mac address
     *
     * @param string $mac
     *
     * @return GuestDevice
     */
    public function findByMac($mac)
    {
        $guestDevice = $this->model->whereMac($mac)->first();

        if ($guestDevice) {
            log_info('Guest device found by mac', ['mac' => $mac]);
        } else {
            log_info('Guest device not found by mac', ['mac' => $mac]);
        }

        return $guestDevice;
    }

    /**
     * Find or create a guest device by mac
     *
     * @param string $mac
     *
     * @return GuestDevice
     */
    public function findOrCreateByMac($mac)
    {
        if (!$mac) {
            $mac = 'UNDEFINED';
        }

        $guestDevice = $this->findByMac($mac);

        if (!$guestDevice) {
            $guestDevice = $this->createForGuest(null, $mac);
        }

        return $guestDevice;
    }

    /**
     * Create a guest device
     *
     * @param int  $guestId
     * @param null $mac
     *
     * @return GuestDevice
     */
    public function createForGuest($guestId, $mac = null)
    {
        // find guest device type by browser user agent
        $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_UNDEFINED;

        if (\Agent::isMobile()) {
            $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_MOBILE;
        } else {
            if (\Agent::isTablet()) {
                $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_TABLET;
            } else {
                if (\Agent::isDesktop()) {
                    $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_DESKTOP;
                }
            }
        }

        $mac = str_replace('-', ':', trim($mac));

        $platform = \Agent::platform();
        $browser = \Agent::browser();

        /**
         * @var GuestDevice $guestDevice
         */
        $guestDevice = $this->model->create(
            [
                'guest_id'         => $guestId,
                'device_type_id'   => $guestDeviceTypeId,
                'description'      => \Agent::device(),
                'platform'         => $platform,
                'platform_version' => \Agent::version($platform),
                'browser'          => $browser,
                'browser_version'  => \Agent::version($browser),
                'mac'              => $mac ?: null,
                'ip'               => session('client_ip') ? : request()->ip(),
            ]
        );

        if ($guestDevice) {
            log_info('Guest device created', ['mac' => $mac, 'guest id' => $guestId]);

            event(new DeviceCreated($guestDevice));
        }

        return $guestDevice;
    }

    /**
     * Get guest device mac address by ip
     *
     * @param string $ip
     *
     * @return mixed
     */
    public function getMacByIp($ip = null)
    {
        if (!$ip) {
            $ip = request()->ip();
        }

        log_info('Trying to find guest device by ip', ['ip' => $ip]);

        $model = $this->model->where('ip', $ip)
            //->connected()
            ->value('mac');

        if ($model) {
            log_info('Found guest device by IP', ['ip' => $ip]);
        } else {
            log_error('Could not find guest device by IP', ['ip' => $ip]);
        }

        return $model;
    }

    /**
     * Transform device usage shares data
     *
     * @param $data
     *
     * @return array
     */
    public function transformDeviceUsageSharesData($data)
    {
        $transformedData = [];
        $deviceTypes = GuestDevice::getDeviceTypes();

        foreach ($data as $deviceTypeId => $total) {
            $transformedData[] = [$deviceTypes[$deviceTypeId], (int)$total];
        }

        return $transformedData;
    }

    /**
     * Transform device platform data
     *
     * @param $data
     *
     * @return array
     */
    public function transformChartData($data)
    {
        $transformedData = [];

        foreach ($data as $value => $total) {
            $transformedData[] = [$value, (int)$total];
        }

        return $transformedData;
    }


    /**
     * Set owner of the given guest device
     *
     * @param GuestDevice $guestDevice
     * @param Guest       $guest
     *
     * @return GuestDevice
     */
    public function setOwner(GuestDevice $guestDevice, Guest $guest)
    {
        if ($guestDevice->guest_id != $guest->id) {
            $guestDevice->update(['guest_id' => $guest->id]);
        }

        return $guestDevice;
    }

    /**
     * Update device type, OS and browser info for the guest device
     *
     * @param GuestDevice $guestDevice
     *
     * @return bool|int
     */
    public function updateDeviceInfo(GuestDevice $guestDevice)
    {
        if ($guestDevice->browser) {
            return false;
        }

        // find guest device type by browser user agent
        $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_UNDEFINED;

        if (\Agent::isMobile()) {
            $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_MOBILE;
        } else {
            if (\Agent::isTablet()) {
                $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_TABLET;
            } else {
                if (\Agent::isDesktop()) {
                    $guestDeviceTypeId = GuestDevice::DEVICE_TYPE_DESKTOP;
                }
            }
        }

        $platform = \Agent::platform();
        $browser = \Agent::browser();

        return $this->update([
            'device_type_id'   => $guestDeviceTypeId,
            'description'      => \Agent::device(),
            'platform'         => $platform,
            'platform_version' => \Agent::version($platform),
            'browser'          => $browser,
            'browser_version'  => \Agent::version($browser),
            'ip'               => session('client_ip') ? : request()->ip(),
        ], $guestDevice->id);
    }
}