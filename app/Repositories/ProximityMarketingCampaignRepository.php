<?php

namespace App\Repositories;


use App\Models\ProximityMarketingCampaign;
use Carbon\Carbon;

/**
 * Class ProximityMarketingCampaignRepository
 *
 * @package App\Repositories
 */
class ProximityMarketingCampaignRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return ProximityMarketingCampaign::class;
    }

    /**
     * @param array $attributes
     * @param null $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value = null)
    {
        $id = hashids_decode($value);

        $attributes['location_id'] = isset($attributes['location_id']) && $attributes['location_id']
            ? hashids_decode($attributes['location_id'])
            : null;
        $castArray = ['event_delay', 'frequency'];

        if (isset($attributes['parameter_type_weekdays']) && $attributes['parameter_type_weekdays']) {
            foreach ($attributes['parameter_type_weekdays'] AS $weekDayKey => $weekDay) {
                $virtualArray[] = $weekDay === "on" ? $weekDayKey : null;
            }
            $attributes['parameter_type_weekdays'] = implode(",", $virtualArray);
        }



        if (isset($attributes['parameter_type_weekdays2']) && $attributes['parameter_type_weekdays2']) {
            $attributes['parameter_type_weekdays'] = $attributes['parameter_type_weekdays2'];
        }
        unset($attributes['parameter_type_weekdays2']);



        foreach ($castArray AS $key => $val) {
            $attributes[$val] = (int)$attributes[$val];
        }

        return $this->save($attributes, $id);
    }

    /**
     * @param $attributes
     * @param $id
     *
     * @return mixed
     */
    public function save($attributes, $id)
    {
        $request = request();

        $model = $this->find($id);

        if (is_null($model)) {
            return $this->create($attributes);
        } else {
            return $this->updateRich($attributes, $id);
        }
    }

    /**
     * Get paginated list of proximity marketing campaigns
     *
     * @return mixed
     */
    public function getAllPaginated()
    {
        return $this->getListQuery()->paginate(config('main.per_page'));
    }

    /**
     * Get query for list of the proximity marketing campaigns
     *
     * @return ProximityMarketingCampaign[]
     */
    public function getListQuery()
    {
        return $this->model
            ->orderBy('status', 'DESC')
            ->orderBy('id', 'DESC');
    }

    /**
     * Get status options
     *
     * @return array
     */
    public function getStatusOptions()
    {
        return [
            ProximityMarketingCampaign::STATUS_DRAFT     => trans('proximity_marketing.status.draft'),
            ProximityMarketingCampaign::STATUS_PUBLISHED => trans('proximity_marketing.status.published'),
        ];
    }

    /**
     * @return array
     */
    public function eventTypeOptions()
    {
        return [
            ProximityMarketingCampaign::EVENT_TYPE_DETECTED  => 'Detected',
            ProximityMarketingCampaign::EVENT_TYPE_CONNECTED => 'Connected',
            ProximityMarketingCampaign::EVENT_TYPE_EXIT      => 'Exit',
        ];
    }

    /**
     * @return array
     */
    public function parameterTypeOptions()
    {
        return [
            ProximityMarketingCampaign::PARAMETER_TYPE_VISIT     => 'Visit Based',
            ProximityMarketingCampaign::PARAMETER_TYPE_DATE_TIME => 'Date / Time Based',
        ];
    }

    /**
     * @return array
     */
    public function parameterTypeVisitOptions()
    {
        return [
            ProximityMarketingCampaign::PARAMETER_TYPE_VISIT_INCLUDE_VISITORS => 'Include Visitors',
            ProximityMarketingCampaign::PARAMETER_TYPE_VISIT_NUMBER_OF_VISITS => 'Number of Visits',
        ];
    }

    /**
     * @return array
     */
    public function parameterTypeNumberOfVisits()
    {
        return [
            ProximityMarketingCampaign::PARAMETER_TYPE_NUMBER_OF_VISITS_IS            => 'Is',
            ProximityMarketingCampaign::PARAMETER_TYPE_NUMBER_OF_VISITS_MORE_THAN     => 'More than',
            ProximityMarketingCampaign::PARAMETER_TYPE_NUMBER_OF_VISITS_EVERY_X_VISIT => 'Every [N] Visits ',
        ];
    }

    /**
     * @return array
     */
    public function weekDays()
    {
        return [
            Carbon::MONDAY    => 'M',
            Carbon::TUESDAY   => 'T',
            Carbon::WEDNESDAY => 'W',
            Carbon::THURSDAY  => 'T',
            Carbon::FRIDAY    => 'F',
            Carbon::SATURDAY  => 'S',
            Carbon::SUNDAY    => 'S',
        ];
    }

    /**
     * @return array
     */
    public function frequenceTypeOptions()
    {
        return [
            ProximityMarketingCampaign::FREQUENCY_TYPE_NONE  => 'Does not apply',
            ProximityMarketingCampaign::FREQUENCY_TYPE_DAY   => 'Once a day',
            ProximityMarketingCampaign::FREQUENCY_TYPE_WEEK  => 'Once a week',
            ProximityMarketingCampaign::FREQUENCY_TYPE_MONTH => 'Once a month',
            ProximityMarketingCampaign::FREQUENCY_TYPE_YEAR  => 'Once a year',
        ];
    }

    /**
     * @return array
     */
    public function ageTypeOptions()
    {
        return [
            ProximityMarketingCampaign::AGE_TYPE_UNKNOWN_AGE => 'unknown age',
            ProximityMarketingCampaign::AGE_TYPE_LESS_THAN   => 'less than',
            ProximityMarketingCampaign::AGE_TYPE_MORE_THAN   => 'more than',
            ProximityMarketingCampaign::AGE_TYPE_RANGE       => 'range',
        ];
    }

    /**
     * @return array
     */
    public function communicationChannelTypeOptions()
    {
        return [
            'SMS',
            'Push Notification',
            'Twitter',
            'E-mail',
        ];
    }

    /**
     * @param $model
     *
     * @return mixed
     */
    public function renderAudience($model)
    {
        $gender = (int)$model->gender;
        $group = (int)$model->age;
        $startAge = (int)$model->start_age;
        $endAge = (int)$model->end_age;
        return $this->model->calculateAudience($gender, $group, $startAge, $endAge);
    }

}