<?php

namespace App\Repositories;


use App\Date;
use App\Models\ServiceProfile;
use App\Models\WifiSession;
use App\Reports\Presence;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class WifiSessionRepository
 *
 * @property WifiSession $model
 */
class WifiSessionRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return WifiSession::class;
    }

    /**
     * Get total downloaded data for the given interval
     *
     * @param null|int $location
     * @param null|int $range
     *
     * @return mixed
     */
    public function getTotalTraffic($location = null, $range = null)
    {
        if (!$range) {
            $range = Date::RANGE_TODAY;
        }

        list($startTime, $endTime) = Date::getRanges($range);

        $query = $this->model
            ->byLocation($location)
            ->where('start_time', '>=', $startTime)
            ->where(function($query) use ($endTime) {
                $query->where('end_time', '<=', $endTime)
                    ->orWhereNull('end_time');
            });

        return $query->sum('downloaded_data');
    }

    /**
     * Get total number of login for the given interval
     *
     * @param null     $location
     * @param null|int $range Default interval is today
     *
     * @return mixed
     */
    public function getTotalLoginCount($location = null, $range = null)
    {
        if (!$range) {
            $range = Date::RANGE_TODAY;
        }

        $query = $this->model
            ->byLocation($location)
            ->select('guest_id')
            ->whereBetween('start_time', Date::getRanges($range));

        return count($query->get());
    }

    /**
     * @param Carbon[] $controlInterval
     * @param Carbon[] $startDateInterval
     * @param int|null $location
     *
     * @return mixed
     */
    public function getNewGuestCount($controlInterval, $startDateInterval, $location = null)
    {
        $query = $this->model
            ->byLocation($location)
            ->whereBetween('start_time', $startDateInterval)
            ->whereNotIn(
                'guest_id', function ($query) use ($controlInterval) {
                $query->select('guest_id')
                    ->from('wifi_sessions')
                    ->whereNull('parent_session_id')
                    ->whereBetween('start_time', $controlInterval)
                    ->groupBy('guest_id');
            });

        return $query->groupBy('guest_id')->count();
    }

    /**
     * Get loyalty data
     *
     * @param Request $request
     *
     * @return array mixed
     */
    public function getLoyaltyData(Request $request)
    {
        $location = $this->getLocationFromHash($request->input('location'));

        $query = $this->model
            ->byLocation($location)
            ->select(\DB::raw('COUNT(guest_id) as total'))
            ->groupBy('guest_id');

        if ($request->has('date')) {
            $query->whereBetween('start_time', date_ranges($request->input('date')));
        }

        return $query->pluck('total')->toArray();
    }

    private function getLocation($input)
    {
        $location = $input;
        if (!\Entrust::can('venues.manage-global')) {
            $location = \Auth::user()->property_id;
        }

        \Log::warning(
            "getLocation", [
                'input'    => $input,
                'location' => $location,
            ]
        );

        return $location;
    }

    private function getLocationFromHash($input)
    {
        return $this->getLocation(hashids_decode($input));
    }

    /**
     * @param int   $tenantId
     * @param int   $location
     * @param array $dateRange
     *
     * @return array
     */
    public function getDailyActivity(int $tenantId, $location, array $dateRange)
    {
        $location = $this->getLocation($location);
        $timezone = setting('timezone');

        $query = \DB::table($this->model->getTable())
            ->select(
                'guest_id',
                'start_time',
                \DB::raw('COUNT(guest_id) AS total')
            )
            ->whereBetween('start_time', $dateRange)
            ->whereNull('parent_session_id')
            ->where('tenant_id', $tenantId)
            ->groupBy('guest_id')
            ->groupBy('start_time');

        if ($location) {
            $query->where('venue_id', $location);
        }

        $result = $query->get();

        $data = array_fill(0, 24, 0);

        $result->map(
            function ($item) use ($timezone) {
                $item->hour = Carbon::parse($item->start_time)->tz($timezone)->hour;

                return $item;
            }
        )
            ->groupBy('hour')
            ->each(
                function ($item, $key) use (&$data) {
                    $data[$key] = $item->count();
                }
            );

        return $data;
    }

    /**
     * Get presence report data
     *
     * @param Request $request
     * @param int     $type
     *
     * @return \Illuminate\Support\Collection
     */
    public function getPresenceData(Request $request, $type)
    {
        $location = $this->getLocationFromHash($request->input('location'));

        $dateField = isSqlServer()
            ? 'CONVERT(DATE, start_time)'
            : 'DATE(start_time)';

        // 0 = Weekly, 1 = Monthly
        $range = $type == 0 ? Date::RANGE_LAST_7_DAYS : Date::RANGE_LAST_30_DAYS;

        $query = $this->model
            ->select(
                \DB::raw($dateField . ' AS date'),
                \DB::raw('AVG(duration) * 1000 AS duration')
            )
            ->byLocation($location)
            ->orderBy('date')
            ->whereBetween('start_time', date_ranges($range))
            ->groupBy(\DB::raw($dateField));

        $data = $query->pluck('duration', 'date')
            ->map(
                function ($item) {
                    return (int)$item;
                }
            );

        return $data;
    }

    public function getTrendAnalysisConnectionData(Request $request, $dateType = 0)
    {
        $location = $this->getLocationFromHash($request->input('location'));

        $dateField = isSqlServer()
            ? 'DATEPART(WEEK, start_time)'
            : 'WEEK(start_time)';

        switch ($dateType) {
            case Date::RANGE_LAST_MONTH:
            case Date::RANGE_INTERVAL:
                $dateRanges = Date::getRanges($dateType);
                break;

            default:
                $dateRanges = Date::getRanges(Date::RANGE_LAST_WEEK);
                $dateField = isSqlServer()
                    ? 'CONVERT(DATE, start_time)'
                    : 'DATE(start_time)';
        }

        $query = $this->model
            ->select(
                \DB::raw('COUNT(guest_id) AS total'),
                \DB::raw($dateField . ' AS date')
            )
            ->byLocation($location)
            ->whereBetween('start_time', $dateRanges)
            ->orderBy('date')
            ->groupBy(\DB::raw($dateField));

        return $query->pluck('total', 'date')->toArray();
    }

    /**
     * @param int            $guestId
     * @param ServiceProfile $serviceProfile
     *
     * @return bool
     */
    public function canGuestConnectWithServiceProfile($guestId, ServiceProfile $serviceProfile)
    {
        $query = $this->model
            ->orderBy('id', 'DESC')
            ->where('guest_id', $guestId)
            ->where('service_profile_id', $serviceProfile->id);

        if ($serviceProfile->type == ServiceProfile::TYPE_RECURRING) {
            $query->where('start_time', '>', Carbon::now()->subHour($serviceProfile->calculateRenewalFrequencyHours()));
        }

        return (int)$query->value('id') == 0;
    }

    /**
     * @param int      $interval
     * @param int|null $venueId
     * @param int|null $tenantId
     *
     * @return int
     */
    public function getOnlineGuestCount($interval = Date::RANGE_TODAY, $venueId = null, $tenantId = null)
    {
        $query = $this->model
            ->whereBetween('start_time', Date::getRanges($interval))
            ->groupBy('guest_id');

        if ($venueId) {
            $query->where('venue_id', $venueId);
        }

        if ($tenantId) {
            $query->where('tenant_id', $tenantId);
        }

        return (int)$query->count();
    }
}