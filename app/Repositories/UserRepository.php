<?php

namespace App\Repositories;


use App\Events\User\Created;
use App\Events\User\PasswordChanged;
use App\Events\User\UpdatedByAdmin;
use App\Events\User\UpdatedProfile;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ServiceProfileRepository
 *
 * @property User $model
 */
class UserRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get paginated list of users
     *
     * @return mixed
     */
    public function getAllPaginated()
    {
        $query = $this->model
            ->with(['tenant', 'property', 'roles'])
            ->orderBy('users.name');

        $disabledRoles = [];

        if (!\Entrust::can('developers.manage')) {
            $disabledRoles[] = 'developer';
        }

        if (!\Entrust::can('support.manage')) {
            $disabledRoles[] = 'support';
        }

        if ($disabledRoles) {
            $query->whereHas('roles', function ($query) use ($disabledRoles) {
                $query->whereNotIn('roles.name', $disabledRoles);
            });
        }

        return $query->paginate(config('main.per_page'));
    }

    /**
     * Save model by hash
     *
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    function saveByHash($attributes, $value)
    {
        if (isset($attributes['password']) && !trim($attributes['password'])) {
            unset($attributes['password']);
            unset($attributes['password_confirmation']);
        }

        if (isset($attributes['password'])) {
            $attributes['password'] = \Hash::make($attributes['password']);
        }

        if (\Entrust::hasRole('developer')) {
            if ($attributes['status'] == User::STATUS_ACTIVE) {
                $attributes['banned_at'] = null;
            }
        }

        if (!array_has($attributes, 'property_id') || !trim($attributes['property_id'])) {
            $attributes['property_id'] = null;
        }

        $id = hashids_decode($value);
        $model = $this->model->find($id);

        $isNew = false;

        if (is_null($model)) {
            $isNew = true;
            $model = $this->create($attributes);
        } else {
            $model->fill($attributes)->save();
        }

        if ($model) {
            $role = Role::where('name', $attributes['role'])->first();
            $model->roles()->sync([$role->id]);
            event($isNew ? new Created($model) : new UpdatedByAdmin($model));
        }

        return $model;
    }

    /**
     * Delete model by hash
     *
     * @param string $value
     *
     * @return mixed
     */
    public function delete($value)
    {
        $status = false;
        $id = hashids_decode($value);

        try {
            $status = $this->model->where('id', '<>', \Auth::id())->where('id', $id)->delete($id);
        } catch (\Illuminate\Database\QueryException $e) {
            \Log::error('Query error on deleting user: ', ['user id' => $id, 'message' => $e->getMessage()]);
        } catch (\Exception $e) {
            \Log::error('Error on deleting user: ', ['message' => $e->getMessage()]);
        }

        return $status;
    }

    /**
     * Invalidate specified session for the user
     *
     * @param string $id
     * @param string $sessionId
     */
    public function invalidateSessions($id, $sessionId)
    {
        $id = hashids_decode($id);

        \DB::table('sessions')
            ->where('user_id', $id)
            ->where('id', $sessionId)
            ->delete();

        $this->model->where('id', $id)->update(['remember_token' => null]);
    }

    /**
     * Update the current users profile
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function updateProfile($attributes)
    {
        if (isset($attributes['password']) && !trim($attributes['password'])) {
            unset($attributes['password']);
            unset($attributes['password_confirmation']);
        }

        if (isset($attributes['password'])) {
            $attributes['password'] = \Hash::make($attributes['password']);
        }

        $status = $this->updateRich($attributes, \Auth::id());

        if ($status) {
            if (isset($data['password'])) {
                event(new PasswordChanged);
            }

            event(new UpdatedProfile);
        }

        return $status;
    }

    /**
     * Get the list of active sessions for the user
     *
     * @param int $userId
     *
     * @return \Illuminate\Support\Collection
     */
    public function getSessions($userId)
    {
        if (!$userId) {
            return collect([]);
        }

        /**
         * @var \Illuminate\Support\Collection $data
         */
        $data = \DB::table('sessions')
            ->where('user_id', $userId)
            ->get(['id', 'ip_address', 'user_agent', 'last_activity']);

        $sessions = $data->map(
            function ($item) {

                $item->last_activity = Carbon::createFromTimestamp($item->last_activity)
                    ->tz(setting('timezone'))
                    ->format(setting('datetime_format'));

                return $item;
            }
        );

        return $sessions;
    }

    /**
     * Find a model by its hashed primary key or throw an exception
     *
     * @param string $value
     * @param array  $columns
     *
     * @return mixed
     */
    public function findByHash($value, $columns = ['*'])
    {
        $id = hashids_decode($value);
        $result = $this->model->find($id, $columns);

        if (!is_null($result)) {
            return $result;
        }

        throw (new ModelNotFoundException)->setModel(get_class($this->model));
    }

    /**
     * Get emails of tenant users
     *
     * @param int $tenantId
     *
     * @return \Illuminate\Support\Collection
     */
    public function getTenantAdminEmails(int $tenantId)
    {
        return $this->model
            ->where('tenant_id', $tenantId)
            ->whereHas('roles', function ($q) {
                $q->whereIn('name', ['developer', 'support', 'tenant']);
            })
            ->pluck('email');
    }

    /**
     * Ban the given user
     *
     * @param int $id
     *
     * @return bool
     */
    public function ban($id)
    {
        if ($id == \Auth::id()) {
            return false;
        }

        return $this->model
            ->where('id', $id)
            ->update([
                'status'    => User::STATUS_BANNED,
                'banned_at' => Carbon::now(),
            ]);
    }

    /**
     * Bant a user by hash
     *
     * @param string $id
     *
     * @return bool
     */
    public function banByHash($id)
    {
        return $this->ban(hashids_decode($id));
    }

    /**
     * Get status options
     */
    public function getStatusOptions()
    {
        return [
            User::STATUS_ACTIVE => trans('user.status.active'),
            User::STATUS_BANNED => trans('user.status.banned'),
        ];
    }
}