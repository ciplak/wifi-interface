<?php

namespace App\Repositories;


use App\Models\DeviceVendor;

/**
 * Class DeviceVendorRepository
 *
 * @property DeviceVendor $model
 */
class DeviceVendorRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return DeviceVendor::class;
    }

    /**
     * Save the model
     *
     * @param array    $attributes
     * @param int|null $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function save($attributes, $id = null)
    {
        return $this->model->updateOrCreate(['id' => $id], $attributes);
    }

    /**
     * Get list of device vendors
     *
     * @return array
     */
    public function getList()
    {
        return $this->model->orderBy('name')->pluck('name', 'id')->toArray();
    }

}