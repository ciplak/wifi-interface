<?php

namespace App\Repositories;


use App\Date;
use App\Models\ServiceProfile;

/**
 * Class ServiceProfileRepository
 *
 * @property ServiceProfile $model
 */
class ServiceProfileRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return ServiceProfile::class;
    }

    /**
     * Save service profile
     *
     * @param int   $id
     * @param array $attributes
     *
     * @return bool
     */
    public function save($id, array $attributes)
    {
        $model = $this->model->findOrNew($id);

        foreach (getLocales() as $locale => $name) {
            $attributes[$locale] = [
                'name'        => array_get($attributes, 'name.' . $locale),
                'description' => array_get($attributes, 'description.' . $locale),
            ];
        }

        $attributes = array_except($attributes, ['name', 'description']);

        $attributes['price'] = str_replace(',', '.', $attributes['price']);
        $attributes['price'] = preg_replace('/[^0-9.]/', '', $attributes['price']);
        if (!trim($attributes['price'])) {
            $attributes['price'] = 0;
        }

        $attributes['renewal_frequency'] = str_replace(',', '.', $attributes['renewal_frequency']);
        $attributes['renewal_frequency'] = preg_replace('/[^0-9.]/', '', $attributes['renewal_frequency']);
        if (!trim($attributes['renewal_frequency'])) { $attributes['renewal_frequency'] = 0;
        }

        if (!trim($attributes['expiry_date'])) {
            $attributes['expiry_date'] = null;
        }

        $model->fill($attributes);

        return $model->save();
    }

    /**
     * Get list of service profiles
     *
     * @return ServiceProfile[]
     */
    public function getList()
    {
        return $this->getListQuery()->get();
    }

    /**
     * Get query for list of the service profiles
     *
     * @return ServiceProfile[]
     */
    public function getListQuery()
    {
        return $this->model
            ->orderBy('name')
            ->orderBy('status', 'DESC')
            ->orderBy('id', 'DESC');
    }

    /**
     * Get active service profile list
     *
     * @return \Illuminate\Support\Collection
     */
    public function getActiveServiceProfilesWithPinEnabled()
    {
        return $this->model
            ->where('service_pin', '!=', ServiceProfile::SERVICE_PIN_DISABLED)
            ->active()
            ->pluck('name', 'id');
    }

    /**
     * Get paginated list of service profiles
     *
     * @return mixed
     */
    public function getAllPaginated()
    {
        return $this->getListQuery()->paginate(config('main.per_page'));
    }

    /**
     * Save model by hash
     *
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value)
    {
        $request = request();

        if (!$request->has('qos_level')) {
            $attributes['qos_level'] = null;
        }

        $attributes['max_download_bandwidth'] = (int)$attributes['max_download_bandwidth'];
        $attributes['max_upload_bandwidth'] = (int)$attributes['max_upload_bandwidth'];

        return parent::saveByHash($attributes, $value);
    }

    /**
     * Get duration types
     *
     * @return array
     */
    public static function getDurationTypeOptions()
    {
        return [
            Date::DURATION_TYPE_MINUTES => trans('messages.duration.minutes'),
            Date::DURATION_TYPE_HOURS   => trans('messages.duration.hours'),
            Date::DURATION_TYPE_DAYS    => trans('messages.duration.days'),
        ];
    }

    /**
     * Get types
     *
     * @return array
     */
    public static function getTypeOptions()
    {
        return [
            ServiceProfile::TYPE_ONE_TIME     => trans('service_profile.type.one_time'),
            ServiceProfile::TYPE_RECURRING    => trans('service_profile.type.recurring'),
            ServiceProfile::TYPE_UNRESTRICTED => trans('service_profile.type.unrestricted'),
        ];
    }

    /**
     * Get renewal frequency type
     *
     * @return array
     */
    public static function getRenewalFrequencyTypeOptions()
    {
        return [
            Date::DURATION_TYPE_HOURS => trans('messages.duration.hours'),
            Date::DURATION_TYPE_DAYS  => trans('messages.duration.days'),
            Date::DURATION_TYPE_WEEKS => trans('messages.duration.weeks'),
        ];
    }

    /**
     * Get service profile models
     *
     * @return array
     */
    public static function getModelOptions()
    {
        return [
            ServiceProfile::MODEL_FREE => trans('service_profile.model.free_of_charge'),
            ServiceProfile::MODEL_PAID => trans('service_profile.model.paid'),
        ];
    }

    /**
     * Get status list
     *
     * @return array
     */
    public static function getStatusOptions()
    {
        return [
            ServiceProfile::STATUS_PASSIVE => trans('messages.passive'),
            ServiceProfile::STATUS_ACTIVE  => trans('messages.active'),
        ];
    }

    /**
     * Get service pin types
     *
     * @return array
     */
    public static function getServicePinTypeOptions()
    {
        return [
            ServiceProfile::SERVICE_PIN_DISABLED => trans('service_profile.service_pin.no'),
            ServiceProfile::SERVICE_PIN_ENABLED  => trans('service_profile.service_pin.yes'),
        ];
    }
}