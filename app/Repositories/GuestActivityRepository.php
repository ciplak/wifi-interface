<?php

namespace App\Repositories;


use App\Date;
use App\Models\GuestActivity;
use Illuminate\Http\Request;

/**
 * Class GuestActivityRepository
 *
 * @property GuestActivity $model
 */
class GuestActivityRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return GuestActivity::class;
    }

    /**
     * @param Request $request
     * @param int     $dateType
     *
     * @return array
     */
    public function getImpressions(Request $request, $dateType = 0)
    {
        $location = null;
        if (!\Entrust::can('venues.manage-global')) {
            $location = \Auth::user()->property_id;
        } else {
            $location = hashids_decode($request->input('location'));
        }

        $dateField = isSqlServer()
            ? 'DATEPART(WEEK, guest_activities.created_at)'
            : 'WEEK(guest_activities.created_at)';

        switch ($dateType) {
        case Date::RANGE_LAST_MONTH:
        case Date::RANGE_INTERVAL:
            $dateRanges = Date::getRanges($dateType);
            break;

        default:
            $dateRanges = Date::getRanges(Date::RANGE_LAST_WEEK);
        }

        $query = $this->model
            ->select(
                \DB::raw('COUNT(guest_id) AS total'),
                \DB::raw($dateField . ' AS date')
            )
            ->whereBetween('guest_activities.created_at', $dateRanges)
            ->join('guests', 'guests.id', '=', 'guest_activities.guest_id')
            ->where('guests.tenant_id', activeTenantId())
            ->orderBy('date')
            ->groupBy(\DB::raw($dateField));

        if ($location) {
            $query->where(
                function ($query) use ($location) {
                    $query->where('property_id', $location)
                        ->orWhereIn(
                            'property_id', function ($query) use ($location) {
                                $query->select('id')
                                    ->from('properties')
                                    ->where('parent_id', $location);
                            }
                        );
                }
            );
        }

        return $query->pluck('total', 'date')->toArray();
    }
}