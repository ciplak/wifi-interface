<?php

namespace App\Repositories;


use App\Models\Advertisement;

/**
 * Class AdvertisementRepository
 *
 * @property Advertisement $model
 */
class AdvertisementRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Advertisement::class;
    }

    /**
     * @inheritdoc
     */
    public function saveByHash($attributes, $value = null)
    {
        $id = hashids_decode($value);

        return $this->save($attributes, $id);
    }

    /**
     * @inheritdoc
     */
    public function save($attributes, $id)
    {
        $request = request();

        $model = $this->find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            if ($image->isValid()) {

                // getting file extension
                $filename = $image->getFilename() . uniqid() . '.' . $image->getClientOriginalExtension();

                \Storage::disk()->put(assets_base('img/ads/' . $filename), \File::get($image));

                $attributes['image'] = $filename;
            } else {
                session()->flash('error', 'invalid file');
            }
        }

        if (is_null($model)) {
            return $this->create($attributes);
        } else {
            return $this->updateRich($attributes, $id);
        }
    }

    /**
     * Return list of ads with hashed ids
     *
     * @return \Illuminate\Support\Collection
     */
    public function getList()
    {
        $items = $this->model
            ->orderBy('title')
            ->pluck('title', 'id');

        $data = $items->mapWithKeys(
            function ($title, $key) {
                return [hashids_encode($key) => $title];
            }
        );

        return $data;
    }

    /**
     * Get status options
     *
     * @return array
     */
    public function getStatusOptions()
    {
        return [
            Advertisement::STATUS_DRAFT => trans('advertisement.status.draft'),
            Advertisement::STATUS_PUBLISHED => trans('advertisement.status.published'),
        ];
    }

    /**
     * Get type options
     *
     * @return array
     */
    public function getTypeOptions()
    {
        return [
            Advertisement::TYPE_TEXT  => trans('advertisement.type.text'),
            Advertisement::TYPE_IMAGE => trans('advertisement.type.image'),
        ];
    }
}