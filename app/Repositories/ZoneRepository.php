<?php

namespace App\Repositories;


use App\Events\Zone\Created;
use App\Events\Zone\Updated;
use App\Models\Zone;

/**
 * Class VenueRepository
 *
 * @property Zone $model
 */
class ZoneRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Zone::class;
    }

    /**
     * @inheritdoc
     */
    public function deleteByHash($value)
    {
        $id = hashids_decode($value);
        $model = $this->model->where('id', $id)->firstOrFail();

        \Auth::user()->canManageVenue($model->parent_id);

        if (\Entrust::hasRole('venue')) {
            if ($model->parent_id != \Auth::user()->property_id) {
                abort(404);
            }
        }

        return $this->delete($id);
    }

    /**
     * Save model by hash
     *
     * @param array  $attributes
     * @param string $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value)
    {
        $request = request();

        $id = hashids_decode($value);
        /**
 * @var Zone $model 
*/
        $model = $this->model->findOrNew($id);

        $isNew = $model && !$model->exists;

        if (!$isNew) {
            \Auth::user()->canManageVenue($model->parent_id);
        }

        $model->fill($request->all());

        if (\Entrust::hasRole('venue')) {
            $model->parent_id = \Auth::user()->property_id;
        } else {
            $venueId = hashids_decode($request->input('venue_id'));
            $model->parent_id = $venueId;
        }

        unset($model->default_service_profile);

        $contents = $model->contents;

        $contents['terms_and_conditions'] = $request->input('terms_and_conditions');
        $model->contents = $contents;

        if (!$request->has('smart_login_frequency')) {
            $model->smart_login_frequency = 0;
        }

        if (!$request->has('use_custom_text')) {
            $model->use_custom_text = 0;
        }

        if (!$request->has('ssids')) {
            $model->ssids = [];
        }

        if ($status = $model->save()) {
            $model->is_new = $isNew;

            if ($request->has('is_default')) {
                $tenantId = $model->parent->parent_id;

                $venueIdList = \DB::table('properties')->where('parent_id', $tenantId)->pluck('id');

                $this->model
                    ->whereIn('parent_id', $venueIdList)
                    ->where('id', '<>', $model->id)
                    ->update(['is_default' => 0]);
            }

            //            $model->serviceProfiles()->sync($request->input('service_profiles', []));

            $serviceProfiles = $request->input('default_service_profile') ? [$request->input('default_service_profile')] : [];

            $model->serviceProfiles()->sync($serviceProfiles);

            $model->setDefaultServiceProvider($request->input('default_service_profile'));

            event($isNew ? new Created($model) : new Updated($model));
        }

        return $model;
    }

}