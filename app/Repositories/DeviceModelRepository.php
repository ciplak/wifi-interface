<?php

namespace App\Repositories;


use App\Models\DeviceModel;

/**
 * Class DeviceModelRepository
 *
 * @property DeviceModel $model
 */
class DeviceModelRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return DeviceModel::class;
    }

    /**
     * Get device models with device vendor prefix
     *
     * @return array
     */
    public function getAllModels()
    {
        $query = $this->model
            ->join('device_vendors', 'device_models.device_vendor_id', '=', 'device_vendors.id')
            ->orderBy('device_vendors.name', 'ASC')
            ->orderBy('device_models.name', 'ASC');

        if (isSqlServer()) {
            $query->selectRaw("(device_vendors.name + ' ' + device_models.name) AS name, device_models.id");
        } else {
            $query->selectRaw('CONCAT(device_vendors.name, " ", device_models.name) AS name, device_models.id');
        }

        return $query->pluck('name', 'id')->toArray();
    }

    /**
     * Get type options
     *
     * @return array
     */
    public function getTypeOptions()
    {
        return $this->model->getTypeOptions();
    }

    /**
     * Get type options
     *
     * @return array
     */
    public function getHandlerOptions()
    {
        return $this->model->getHandlerOptions();
    }

    /**
     * Save the model
     *
     * @param array    $attributes
     * @param int|null $id
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function save($attributes, $id = null)
    {
        return $this->model->updateOrCreate(['id' => $id], $attributes);
    }

    /**
     * Get all device models
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->with('vendor')->get();
    }

}