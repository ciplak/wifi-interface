<?php

namespace App\Repositories;


use App\Models\Campaign;

/**
 * Class CampaignRepository
 *
 * @package App\Repositories
 */
class CampaignRepository extends BaseRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Campaign::class;
    }

    /**
     * @param array $attributes
     * @param null  $value
     *
     * @return mixed
     */
    public function saveByHash($attributes, $value = null)
    {
        $id = hashids_decode($value);

        $attributes['parameter_type_time'] = config('times.time.' . $attributes['parameter_type_time']);

        $attributes['birthday'] = $attributes['birthday'] ?? 0;
        $attributes['location_id'] = isset($attributes['location_id']) && $attributes['location_id']
            ? hashids_decode($attributes['location_id'])
            : null;

        $castArray = ['reengage', 'gender', 'age', 'visit_count'];

        foreach ($castArray AS $key => $val) {
            $attributes[$val] = (int)$attributes[$val];
        }

        return $this->save($attributes, $id);
    }

    /**
     * @param $attributes
     * @param $id
     *
     * @return mixed
     */
    public function save($attributes, $id)
    {
        $request = request();

        $model = $this->find($id);

        return is_null($model)
            ? $this->create($attributes)
            : $this->updateRich($attributes, $id);
    }

    /**
     * Get status options
     *
     * @return array
     */
    public function getStatusOptions()
    {
        return [
            Campaign::STATUS_DRAFT     => trans('campaign.status.draft'),
            Campaign::STATUS_PUBLISHED => trans('campaign.status.published'),
        ];
    }

    /**
     * @return array
     */
    public function getParameterTypeOptions()
    {
        return [
            Campaign::PARAMETER_TYPE_DATE_TIME => 'Date / Time Based',
            Campaign::PARAMETER_TYPE_PERIODIC  => 'Periodic',
        ];
    }

    /**
     * @return array
     */
    public function getPeriodicTypeOptions()
    {
        return [
            Campaign::PERIODIC_TYPE_WEEKLY   => 'Weekly',
            Campaign::PERIODIC_TYPE_MONTHLY  => 'Monthly',
            Campaign::PERIODIC_TYPE_ANNUALLY => 'Annually',
        ];
    }

    /**
     * @return array
     */
    public function getAgeGroupOptions()
    {
        return [
            null                           => trans('guest.age.group.all'),
            Campaign::AGE_TYPE_UNKNOWN_AGE => 'unknown age',
            Campaign::AGE_TYPE_LESS_THAN   => 'less than',
            Campaign::AGE_TYPE_MORE_THAN   => 'more than',
            Campaign::AGE_TYPE_RANGE       => 'range',
        ];
    }

    /**
     * @return array
     */
    public function getCommunicationChannelTypeOptions()
    {
        return [
            'SMS',
            'E-mail',
        ];
    }
}