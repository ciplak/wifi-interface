<?php

namespace App\Services;


use PHPExcel_Cell;

class ListReportExporter
{
    /**
     * File format
     *
     * @var string
     */
    private $format = 'xlsx';

    /**
     * Data to export
     *
     * @var array
     */
    private $data = [];

    /**
     * Column titles
     *
     * @var array
     */
    private $titles = [];

    /**
     * Excel sheet title
     *
     * @var string
     */
    private $sheetTitle = '';

    /**
     * Column formats
     *
     * @var array
     */
    private $columnFormat = [];

    /**
     * Set file format
     *
     * @param string $format
     *
     * @return ListReportExporter
     */
    public function format(string $format): ListReportExporter
    {
        $format = strtolower(trim($format));

        if (!in_array($format, ['csv', 'xslx', 'pdf'])) {
            $format = 'xlsx';
        }

        $this->format = $format;

        return $this;
    }

    /**
     * Export data to a file
     *
     * @param string $filename
     * @param array  $data
     * @param array  $titles
     * @param string $sheetTitle
     */
    public function export($filename, $data = [], $titles = [], $sheetTitle = '')
    {
        if ($data) {
            $this->data($data);
        }

        if ($titles) {
            $this->titles($titles);
        }

        if (trim($sheetTitle)) {
            $this->sheetTitle($sheetTitle);
        }

        if (!trim($this->sheetTitle)) {
            $this->sheetTitle = $filename;
        }

        \Excel::create($filename, function($excel) {
            /** @var \Maatwebsite\Excel\Writers\LaravelExcelWriter $excel */

            $excel->sheet($this->sheetTitle, function($sheet) {
                $maxColumn = PHPExcel_Cell::stringFromColumnIndex(count($this->titles) - 1);

                /** @var \Maatwebsite\Excel\Classes\LaravelExcelWorksheet $sheet */
                $sheet->_parent->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);
                $sheet->freezeFirstRow();
                $sheet->setAutoFilter("A1:{$maxColumn}1");
                $sheet->setAutoSize(true);

                if ($this->columnFormat) {
                    $sheet->setColumnFormat($this->columnFormat);
                }

                $sheet->row(1, $this->titles);

                $maxRow = count($this->data) + 1;
                $sheet->cell("A1:{$maxColumn}1", function($cells) {
                    $cells->setBorder('none', 'none', 'solid', 'none');
                });

                // clear all borders
                $sheet->setBorder("A2:{$maxColumn}" . $maxRow, 'none');

                for ($i = 2; $i <= $maxRow; $i += 2) {
                    $sheet->row($i, function($row) {
                        $row->setBackground('#f4f4f4');
                    });
                }

                $sheet->fromArray($this->data, null, 'A2', false, false);
            });
        })->export($this->format);
    }

    /**
     * Set data to export
     *
     * @param array $data
     *
     * @return ListReportExporter
     */
    public function data(array $data): ListReportExporter
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set column titles
     *
     * @param array $titles
     *
     * @return ListReportExporter
     */
    public function titles(array $titles): ListReportExporter
    {
        $this->titles = $titles;

        return $this;
    }

    /**
     * Set Excel sheet title
     *
     * @param string $sheetTitle
     *
     * @return ListReportExporter
     */
    public function sheetTitle($sheetTitle)
    {
        $this->sheetTitle = trim($sheetTitle);

        return $this;
    }

    /**
     * Set column format
     *
     * @param array $columnFormat
     *
     * @return ListReportExporter
     */
    public function columnFormat(array $columnFormat): ListReportExporter
    {
        $this->columnFormat = $columnFormat;

        return $this;
    }
}