<?php

if (!function_exists('isRTL')) {
    /**
     * Check if locale is right to left
     *
     * @param null $locale
     *
     * @return bool
     */
    function isRTL($locale = null)
    {
        if (!$locale) {
            $locale = App::getLocale();
        }

        return in_array($locale, ['ar']);
    }

}

if (!function_exists('human_filesize')) {
    /**
     * Return sizes readable by humans
     *
     * @param int $bytes
     * @param int $decimals
     *
     * @return string
     */
    function human_filesize($bytes, $decimals = 2)
    {
        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f %s", $bytes / pow(1024, $factor), @$size[$factor]);
    }
}

if (!function_exists('copyright')) {
    /**
     * Return copyright text
     *
     * @return mixed|string
     */
    function copyright()
    {
        $copyright = config('main.copyright_text');

        $copyright = strtr(
            $copyright, [
                '%year%' => date('Y'),
            ]
        );

        return $copyright;
    }
}

if (!function_exists('sortArrayByArray')) {
    /**
     * Reorder array by another array
     *
     * @param array $array
     * @param array $sort
     *
     * @return mixed|string
     */
    function sortArrayByArray(array $array, array $sort)
    {

        $keys = array_keys($array);

        $intersect = array_intersect($sort, $keys);
        $diff = array_diff($keys, $intersect);
        $orderedKeys = array_merge($intersect, $diff);

        $result = [];
        foreach ($orderedKeys as $key) {
            $result[$key] = $array[$key];
        }

        return $result;
    }
}

if (!function_exists('setting')) {
    /**
     * Get / set the specified setting value.
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param array|string $key
     * @param mixed        $default
     *
     * @return mixed
     */
    function setting($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('setting');
        }

        if (is_array($key)) {
            return app('setting')->put($key);
        }

        return app('setting')->get($key, $default);
    }
}

if (!function_exists('redirect_url')) {
    function redirect_url(\App\Models\Property $property)
    {
        return $property->post_login_url_value ?: setting('post_login_url', session('redirect'));
    }
}

if (!function_exists('date_options')) {
    function date_options()
    {
        return \App\Date::getOptions();
    }
}

if (!function_exists('date_ranges')) {
    function date_ranges($value)
    {
        return \App\Date::getRanges($value);
    }
}

if (!function_exists('isSqlServer')) {
    function isSqlServer()
    {
        return env('DB_CONNECTION') == 'sqlsrv';
    }
}

if (!function_exists('pagination')) {
    function pagination(\Illuminate\Contracts\Pagination\Paginator $paginator)
    {
        echo $paginator->appends(request()->except(['page']))->render('vendor.pagination.pagination');
    }
}

if (!function_exists('asset_url')) {
    function asset_url($path = '')
    {
        $base = assets_base($path);

        return config('filesystems.default') == 'network'
            ? route('asset' , ['path' => $base])
            : '//static.iperawifi.com/' . $base;
    }
}

if (!function_exists('logo_url')) {
    function logo_url()
    {
        return asset_url('img/' . setting('logo'));
    }
}

if (!function_exists('assets_base')) {
    function assets_base($path = null)
    {
        $activeTenantId = \Auth::check() ? (int)\Auth::user()->tenant_id : session('front_page_tenant_id');

        $base = "a/{$activeTenantId}_" . hashids_encode($activeTenantId) . '/';

        if ($path) {
            $base .= $path;
        }

        return $base;
    }
}

if (!function_exists('activeTenantId')) {
    function activeTenantId()
    {
        return \Auth::check() ? (int)\Auth::user()->tenant_id : 0;
    }
}

if (!function_exists('jsonPrettify')) {
    /**
     * Indents a flat JSON string to make it more human-readable.
     *
     * @param string $json The original JSON string to process.
     *
     * @return string Indented version of the original JSON string.
     */
    function jsonPrettify($json)
    {

        $result = '';
        $pos = 0;
        $strLen = strlen($json);
        $indentStr = '  ';
        $newLine = "\n";
        $prevChar = '';
        $outOfQuotes = true;

        for ($i = 0; $i <= $strLen; $i++) {

            // Grab the next character in the string.
            $char = substr($json, $i, 1);

            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\') {
                $outOfQuotes = !$outOfQuotes;

                // If this character is the end of an element,
                // output a new line and indent the next line.
            } else {
                if (($char == '}' || $char == ']') && $outOfQuotes) {
                    $result .= $newLine;
                    $pos--;
                    for ($j = 0; $j < $pos; $j++) {
                        $result .= $indentStr;
                    }
                }
            }

            // Add the character to the result string.
            $result .= $char;

            // If the last character was the beginning of an element,
            // output a new line and indent the next line.
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                $result .= $newLine;
                if ($char == '{' || $char == '[') {
                    $pos++;
                }

                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            $prevChar = $char;
        }

        return $result;
    }
}

if (!function_exists('hashids_encode')) {
    /**
     * @param string $value
     *
     * @return string
     *
     */
    function hashids_encode($value)
    {
        return \Hashids::encode($value);
    }
}

if (!function_exists('hashids_decode')) {
    /**
     * @param string $hash
     * @param null   $default
     *
     * @return int|null
     */
    function hashids_decode($hash, $default = null)
    {
        $value = \Hashids::decode($hash);

        return $value ? $value[0] : $default;
    }
}

if (!function_exists('log_info')) {
    /**
     * @param string $message
     * @param array  $parameters
     * @param string $type
     */
    function log_info($message, $parameters = [], $type = 'info')
    {
        $defaults = [
            'tenant id'            => session('front_page_tenant_id'),
            'client_mac (session)' => session('client_mac'),
            'current url'          => request()->url(),
            'guest device ip'      => request()->ip(),
            'request parameters'   => request()->all(),
            'session'              => session()->all(),
        ];

        $parameters['_info'] = $defaults;

        if ($type == 'error') {
            \Log::error($message, $parameters);
        } else {
            \Log::info($message, $parameters);
        }
    }
}

if (!function_exists('log_error')) {
    /**
     * @param string $message
     * @param array  $parameters
     */
    function log_error($message, $parameters = [])
    {
        log_info($message, $parameters, 'error');
    }
}

if (!function_exists('build_phone_number')) {
    /**
     * Build phone number from request
     *
     * @return string
     */
    function build_phone_number()
    {
        $request = request();

        // add country code to phone field
        $isAllCountryCodes = config('front_page_settings.country_phone_code_type') == 0;

        $countryPhoneCode = $isAllCountryCodes
            ? $request->input('country_code')
            : config('front_page_settings.default_country_phone_code');

        // remove non numeric chars
        $phoneNumber = preg_replace('/\D/', '', $request->input('phone'));

        $phone = config('countries.phone_codes.' . $countryPhoneCode) . $phoneNumber;

        return $phone;
    }
}

if (!function_exists('load_settings')) {
    /**
     * Load front page settings
     *
     * @param int $tenantId
     */
    function load_settings($tenantId)
    {
        try {
            // @todo move into controller
            /** @var \App\Repositories\SettingRepository $repository */
            $repository = app(\App\Repositories\SettingRepository::class);
            $settings = $repository->getAll($tenantId);

            $settings->each(
                function ($item, $key) {
                    config(['front_page_settings.' . $key => $item]);
                }
            );

            \Log::info('loaded front page settings', ['settings' => $settings]);
        } catch (\Exception $e) {
            \Log::error('Error on loading settings from DB', ['message' => $e->getMessage()]);
        }
    }
}

if (!function_exists('getLocales')) {
    /**
     * Get locale list
     */
    function getLocales()
    {
        // @todo get locales from settings table for each tenant
        return config('app.locales');
    }
}

if (!function_exists('trimmed_request')) {
    /**
     * Return a request object with all inputs trimmed
     *
     * @return \Illuminate\Http\Request
     */
    function trimmed_request()
    {
        $request = request();
        $input = array_map(function($var) {
            return is_string($var) ? trim($var) : $var;
        }, $request->all());
        $request->replace($input);

        return $request;
    }
}