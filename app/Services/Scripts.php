<?php

namespace App\Services;


class Scripts
{
    private static $scripts = [];

    public static function view($view)
    {
        static::$scripts[] = ['view' => $view, 'type' => 'view'];
    }

    public static function url($url)
    {
        static::$scripts[] = ['url' => $url, 'type' => 'url'];
    }

    public static function render()
    {
        $scripts = collect(static::$scripts);

        $scripts->where('type', 'url')->each(
            function ($item) {
                echo \HTML::script($item['url']);
            }
        );

        $scripts->where('type', 'view')->each(
            function ($item) {
                echo view($item['view'])->render();
            }
        );

        static::$scripts = [];
    }
}