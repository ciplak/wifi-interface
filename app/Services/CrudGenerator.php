<?php

namespace App\Services;


class CrudGenerator
{

    /**
     * @param array $config
     *
     * @return int
     */
    public function controller($config)
    {
        $content = file_get_contents(base_path('resources/templates') . '/controller.stub');

        $content = $this->replaceVars($config, $content);

        $result = file_put_contents(app_path('Http/Controllers') . '/' . $config['name_camel_case'] . 'Controller.php', $content);

        return $result;
    }

    /**
     * @param array  $config
     * @param string $content
     *
     * @return mixed
     */
    private function replaceVars($config, $content)
    {
        foreach ($config as $key => $value) {
            $content = str_replace('{' . $key . '}', $value, $content);
        }

        return $content;
    }

    /**
     * @param array $config
     *
     * @return int
     */
    public function api_controller($config)
    {
        $content = file_get_contents(base_path('resources/templates') . '/api_controller.stub');

        $content = $this->replaceVars($config, $content);

        $result = file_put_contents(app_path('Http/Controllers') . '/Api/' . $config['name_camel_case'] . 'Controller.php', $content);

        return $result;
    }

    /**
     * @param array $config
     *
     * @return int
     */
    public function form_request($config)
    {
        $content = file_get_contents(base_path('resources/templates') . '/form_request.stub');

        $content = $this->replaceVars($config, $content);

        $result = file_put_contents(app_path('Http/Requests') . '/' . $config['name_camel_case'] . 'Request.php', $content);

        return $result;
    }

    /**
     * @param array $config
     *
     * @return bool
     */
    public function model($config)
    {
        $model = file_get_contents(base_path('resources/templates') . '/model.stub');
        $model = $this->replaceVars($config, $model);
        $model = file_put_contents(app_path('Models') . '/' . $config['name_camel_case'] . '.php', $model);


        $repository = file_get_contents(base_path('resources/templates') . '/repository.stub');
        $repository = $this->replaceVars($config, $repository);
        $repository = file_put_contents(app_path('Repositories') . '/' . $config['name_camel_case'] . 'Repository.php', $repository);

        return $model && $repository;
    }

    /**
     * @param array $config
     *
     * @return int|mixed|string
     */
    public function migration($config)
    {
        $migration = file_get_contents(base_path('resources/templates') . '/migration.stub');
        $migration = $this->replaceVars($config, $migration);
        $migration = file_put_contents(base_path('database/migrations') . '/' . date('Y_m_d_His') . '_create_' . $config['table_name'] . '_table.php', $migration);

        return $migration;
    }

    /**
     * @param array  $config
     * @param string $view
     *
     * @return int
     */
    public function view($config, $view)
    {
        $path = resource_path('views/admin') . '/' . $config['name_snake_case'] . '/';

        if (!file_exists($path)) {
            mkdir($path);
        }

        $content = file_get_contents(base_path('resources/templates/views') . '/' . $view . '.stub');
        $content = $this->replaceVars($config, $content);

        $result = file_put_contents($path . $view . '.php', $content);

        return $result;
    }

}