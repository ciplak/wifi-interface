<?php

namespace App\Services;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Bootstrap\ConfigureLogging as IlluminateConfigureLogging;
use Illuminate\Log\Writer;

class ConfigureLogging extends IlluminateConfigureLogging
{

    /**
     * Configure the Monolog handlers for the application.
     *
     * @param  \Illuminate\Contracts\Foundation\Application $app
     * @param  \Illuminate\Log\Writer                       $log
     * @return void
     */
    protected function configureHandlers(Application $app, Writer $log)
    {

        //$bubble = false;

        // Stream Handlers
        //$infoStreamHandler = new StreamHandler( storage_path("/logs/app_info.log"), Monolog::INFO, $bubble);
        //$errorStreamHandler = new StreamHandler(storage_path("/logs/error.log"), Monolog::ERROR, $bubble);

        // Get monolog instance and push handlers
        //$monolog = $log->getMonolog();
        //$monolog->pushHandler($infoStreamHandler);
        //$monolog->pushHandler($errorStreamHandler);

        $log->useDailyFiles($app->storagePath() . '/logs/app.log', config('app.log_max_files', 7), config('app.log_level', 'debug'));
    }

}