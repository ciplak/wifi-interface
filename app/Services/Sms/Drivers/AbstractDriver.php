<?php

namespace App\Services\Sms\Drivers;


use GuzzleHttp\Client;

abstract class AbstractDriver
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

}