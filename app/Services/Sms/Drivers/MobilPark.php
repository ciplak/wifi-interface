<?php

namespace App\Services\Sms\Drivers;


class MobilPark extends AbstractDriver implements DriverInterface
{
    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return mixed
     */
    public function send($to, $message, $parameters = [])
    {
        $result = false;

        try {
            $username = $parameters['username'] ?? '';
            $password = $parameters['password'] ?? '';
            $from = $parameters['from'] ?? 'MOBILPARK';
            $message = trim($message);
            $to = trim($to);

            $data = "<packet version=\"1.0\">
	<header>
		<auth userName=\"$username\" password=\"$password\" />
	</header>
	<body>
		<sendMessage useGrouping=\"1\">
			<type>Sms</type>
			<senderid>$from</senderid>
			<recipients>$to</recipients>
			<sendDate></sendDate>
			<content>$message</content>
		</sendMessage>
	</body>
</packet>";

            $response = $this->client->post('http://service.mobilpark.biz/xml/default.aspx', [
                'headers' => [
                    'Content-Type' => 'text/xml',
                    'Accept'       => 'application/xml',
                ],

                'body' => $data,
            ]);

            $strResponse = (string)$response->getBody();

            $xmlResponse = simplexml_load_string($strResponse);

            if ($xmlResponse['type'] == 'error') {
                \Log::error('Could not send SMS via MobilPark', [
                    'error code' => (string)$xmlResponse->error->code,
                    'error'      => (string)$xmlResponse->error->description,
                    'to'         => $to,
                    'message'    => $message,
                    'parameters' => $parameters,
                    'response'   => $strResponse,
                ]);
            } else {
                \Log::info('SMS sent via MobilPark', [
                    'to'         => $to,
                    'message'    => $message,
                    'parameters' => $parameters,
                ]);

                $result = true;
            }
        } catch (\Exception $e) {
            \Log::error('Could not send SMS via MobilPark', [
                'error'      => $e->getMessage(),
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
            ]);
        }

        return $result;
    }
}