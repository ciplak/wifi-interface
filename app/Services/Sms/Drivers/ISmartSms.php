<?php

namespace App\Services\Sms\Drivers;


class ISmartSms extends AbstractDriver implements DriverInterface
{

    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return mixed
     */
    public function send($to, $message, $parameters = [])
    {
        $baseUrl = 'https://www.ismartsms.net/iBulkSMS/HttpWS/SMSDynamicAPI.aspx';

        $languageCode = 0;

        $languageCodes = [
            'en' => 0,
            'ar' => 64,
        ];

        if (isset($parameters['locale']) && isset($languageCodes[$parameters['locale']])) {
            $languageCode = $languageCodes[$parameters['locale']];
        }

        $formParameters = [
            'UserId'   => $parameters['username'],
            'Password' => $parameters['password'],
            'MobileNo' => $to,
            'Message'  => $message,
            'Lang'     => $languageCode,
        ];

        $result = false;

        try {
            $response = $this->client->get($baseUrl . '?' . http_build_query($formParameters));

            $returnCodes = [
                1  => 'Message Pushed successfully.',
                2  => 'Company Not Exits. Please check the company.',
                3  => 'User or Password is wrong.',
                4  => 'Credit is Low.',
                5  => 'Message is blank.',
                6  => 'Message Length Exceeded.',
                7  => 'Account is Inactive.',
                8  => 'Mobile No length is empty.',
                9  => 'Invalid Mobile No.',
                10 => 'Invalid Language.',
                11 => 'Un Known Error.',
                12 => 'Account is Blocked by administrator, concurrent failure of login.',
                13 => 'Account Expired.',
                14 => 'Credit Expired.',
                15 => 'Invalid Http request or Parameter fields are wrong.',
                16 => 'Invalid date time parameter.',
                18 => 'Web Service User not registered.',
                20 => 'Client IP Address has been blocked.',
                21 => 'Client IP is outside Oman, Outside Oman IP is not allowed to access web service',
            ];

            $returnCode = (int)$response->getBody()->getContents();

            \Log::info(
                'iSmartSms response', [
                'response'       => $returnCodes[$returnCode],
                'to'             => $to,
                'message'        => $message,
                'parameters'     => $parameters,
                'api parameters' => $formParameters,
                ]
            );

            $result = $returnCode == 1;

        } catch (\Exception $e) {
            \Log::error(
                'Could not send SMS via iSmartSms', [
                'error'          => $e->getMessage(),
                'to'             => $to,
                'message'        => $message,
                'parameters'     => $parameters,
                'api parameters' => $formParameters,
                ]
            );
        }

        return $result;
    }

}