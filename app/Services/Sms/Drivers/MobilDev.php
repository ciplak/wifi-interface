<?php

namespace App\Services\Sms\Drivers;


class MobilDev extends AbstractDriver implements DriverInterface
{
    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return bool
     */
    public function send($to, $message, $parameters = [])
    {
        $result = false;

        try {
            $formParameters = [
                'key'        => $parameters['key'] ?? '',
                'secret'     => $parameters['secret'] ?? '',
                'originator' => $parameters['originator'] ?? 'MDEV DEMO',
                'msisdn'     => $parameters['msisdn'] ?? '',
                'sendType'   => 2,
                'serviceid'  => 'otp',
                'blacklist'  => 0,
                'content'    => [
                    'message' => trim($message),
                    trim($to),
                ],
            ];

            $baseUrl = 'https://api.mobildev.com/sms';
            $url = $baseUrl . '?' . http_build_query($formParameters);

            $response = $this->client->get($url);

            $response = json_decode($response->getBody());

            if (isset($response->error)) {
                throw new \Exception("Error {$response->error}: {$response->Message}", $response->error);
            } else {
                $result = true;

                \Log::info('MobileDev response', [
                    'response'       => $response,
                    'to'             => $to,
                    'message'        => $message,
                    'parameters'     => $parameters,
                    'api parameters' => $formParameters,
                ]);
            }
        } catch (\Exception $e) {
            \Log::error('Could not send SMS via MobilDev', [
                'error'      => $e->getMessage(),
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
            ]);
        }

        return $result;
    }
}