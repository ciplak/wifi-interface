<?php

namespace App\Services\Sms\Drivers;


class MayerMobile extends AbstractDriver implements DriverInterface
{
    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return bool
     */
    public function send($to, $message, $parameters = [])
    {
        $result = false;

        try {
            $formParameters = [
                'username' => $parameters['username'] ?? '',
                'password' => $parameters['password'] ?? '',
                'sender'   => $parameters['sender'] ?? '',
                'GSM'      => $to,
                'SMSText'  => $message,
            ];

            $baseUrl = 'http://185.46.34.249:8080/smsgw/endpoint/v1/sendsms/plain';
            $url = $baseUrl . '?' . http_build_query($formParameters);

            $response = $this->client->get($url);

            $response = json_decode($response->getBody());

            if ((int)$response->status !== 0) {
                $message = $this->getMessage($response->status);

                throw new \Exception("Error {$response->status}: {$message}", $response->status);
            }

            $result = true;

            \Log::info('SMS sent via Mayer Mobile', [
                'response'       => $response,
                'to'             => $to,
                'message'        => $message,
                'parameters'     => $parameters,
                'api parameters' => $formParameters,
            ]);
        } catch (\Exception $e) {
            \Log::error('Could not send SMS via Mayer Mobile', [
                'error'      => $e->getMessage(),
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
            ]);
        }

        return $result;
    }

    /**
     * Get response status messages
     *
     * @param int $status
     *
     * @return array
     */
    private function getMessage($status)
    {
        $status = (int)$status;

        $messages = [
            0   => 'Request was successful (all recipients)',
            -1  => 'Error in processing the request',
            -2  => 'Not enough credits on a specific account',
            -3  => 'Targeted network is not covered on specific account',
            -5  => 'Username or password is invalid',
            -6  => 'Destination address is missing in the request',
            -10 => 'Username is missing in the request',
            -11 => 'Password is missing in the request',
            -13 => 'Number is not recognized by SMSGW platform',
            -22 => 'Incorrect XML format, caused by syntax error',
            -23 => 'General error, reasons may vary',
            -26 => 'General API error, reasons may vary',
            -27 => 'Invalid scheduling parametar',
            -28 => 'Invalid PushURL in the request',
            -30 => 'Invalid APPID in the request',
            -33 => 'Duplicated MessageID in the request',
            -34 => 'Sender name is not allowed',
            -99 => 'Error in processing request, reasons may vary',
        ];

        return $messages[$status] ?? 'Unknown error: ' . $status ;
    }
}