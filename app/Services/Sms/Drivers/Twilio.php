<?php

namespace App\Services\Sms\Drivers;


class Twilio extends AbstractDriver implements DriverInterface
{

    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return mixed
     */
    public function send($to, $message, $parameters = [])
    {
        $result = false;
        $to = '+' . $to;

        try {
            $from = $parameters['from'];
            $AccountSid = $parameters['account_sid'];
            $token = $parameters['auth_token'];

            $baseUrl = "https://api.twilio.com/2010-04-01/Accounts/{$AccountSid}/SMS/Messages.json";

            $response = $this->client->post(
                $baseUrl, [
                'auth'        => [
                    $AccountSid,
                    $token,
                ],
                'form_params' => [
                    'To'   => $to,
                    'From' => $from,
                    'Body' => $message,
                ],
                ]
            );

            \Log::info(
                'SMS sent via Twilio', [
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
                ]
            );

            $result = true;

        } catch (\Exception $e) {
            \Log::error(
                'Could not send SMS via Twilio', [
                'error'      => $e->getMessage(),
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
                ]
            );
        }

        return $result;
    }
}