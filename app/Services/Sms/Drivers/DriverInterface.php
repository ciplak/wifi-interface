<?php

namespace App\Services\Sms\Drivers;


interface DriverInterface
{

    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return bool
     */
    public function send($to, $message, $parameters = []);

}