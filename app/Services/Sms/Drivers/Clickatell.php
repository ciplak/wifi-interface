<?php

namespace App\Services\Sms\Drivers;


class Clickatell extends AbstractDriver implements DriverInterface
{
    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return mixed
     */
    public function send($to, $message, $parameters = [])
    {
        $returnCodes = [
            1  => 'Message unknown',
            2  => 'Message queued',
            3  => 'Delivered to gateway',
            4  => 'Received by recipient',
            5  => 'Error with message',
            6  => 'User cancelled message delivery',
            7  => 'Error delivering message',
            9  => 'Routing error',
            10 => 'Message expired',
            11 => 'Message scheduled for later delivery',
            12 => 'Out of credit',
            13 => 'Clickatell cancelled message delivery',
            14 => 'Maximum MT limit exceeded',
        ];

        $result = false;

        try {
            // HTTP request
            //            http://api.clickatell.com/http/sendmsg?user=USERNAME&password=PASSWORD&api_id=3609882&to=905055814759&text=Message

            $token = $parameters['auth_token'];

            $response = $this->client->post(
                'https://api.clickatell.com/rest/message', [
                'headers' => [
                    'X-Version'     => 1,
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],

                'json' => [
                    'text' => $message,
                    'to'   => [$to],
                    //'from' => 'from' // @todo add sender id support  - https://www.clickatell.com/help/apidocs/Message.htm#SourceAddress
                ],
                ]
            );

            \Log::info(
                'SMS sent via Clickatell', [
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
                ]
            );

            $result = true;

        } catch (\Exception $e) {
            \Log::error(
                'Could not send SMS via Clickatell', [
                'error'      => $e->getMessage(),
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
                ]
            );
        }

        return $result;
    }
}