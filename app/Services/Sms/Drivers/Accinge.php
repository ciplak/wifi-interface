<?php

namespace App\Services\Sms\Drivers;


class Accinge extends AbstractDriver implements DriverInterface
{
    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return mixed
     */
    public function send($to, $message, $parameters = [])
    {
        $returnCodes = [
             0 => 'Unknown',
            -1 => 'Invalid connection',
            -2 => 'Unauthorized',
            -3 => 'Invalid credit',
        ];

        $result = false;

        try {
            $data = [
                'UserName'    => $parameters['username'] ?? '',
                'Password'    => $parameters['password'] ?? '',
                'Header'      => $parameters['sender_id'] ?? '',
                'Message'     => trim($message),
                'ContactNo'   => trim($to),
                'OriginSMSId' => $parameters['from'] ?? -1,
            ];

            $response = $this->client->post(
                'http://accingesms.com/smswebapi/sms/SendMessage', [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                ],

                'json' => $data
                ]
            );

            $returnCode = json_decode($response->getBody());

            if ($returnCode <= 0) {
                throw new \Exception($returnCodes[$returnCode]);
            }

            \Log::info(
                'SMS sent via Accinge', [
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
                ]
            );

            $result = true;

        } catch (\Exception $e) {
            \Log::error(
                'Could not send SMS via Accinge', [
                'error'      => $e->getMessage(),
                'to'         => $to,
                'message'    => $message,
                'parameters' => $parameters,
                ]
            );
        }

        return $result;
    }
}