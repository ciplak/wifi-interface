w<?php

namespace App\Services\Sms;


use App\Services\Sms\Drivers\Accinge;
use App\Services\Sms\Drivers\Clickatell;
use App\Services\Sms\Drivers\ISmartSms;
use App\Services\Sms\Drivers\MayerMobile;
use App\Services\Sms\Drivers\MobilDev;
use App\Services\Sms\Drivers\MobilPark;
use App\Services\Sms\Drivers\Twilio;
use GuzzleHttp\Client;
use Illuminate\Support\Manager;

class DriverManager extends Manager
{
    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->createClickatellDriver();
    }

    /**
     * Create an instance of the Clickatell driver
     *
     * @return Clickatell
     */
    protected function createClickatellDriver()
    {
        $provider = new Clickatell(new Client());

        return $provider;
    }

    /**
     * Create an instance of the Twilio driver
     *
     * @return Twilio
     */
    protected function createTwilioDriver()
    {
        $provider = new Twilio(new Client());

        return $provider;
    }

    /**
     * Create an instance of the ISmart driver
     *
     * @return ISmartSms
     */
    protected function createISmartSmsDriver()
    {
        $provider = new ISmartSms(new Client());

        return $provider;
    }

    /**
     * Create an instance of the Accinge driver
     *
     * @return Accinge
     */
    protected function createAccingeDriver()
    {
        $provider = new Accinge(new Client());

        return $provider;
    }

    /**
     * Create an instance of the MobilPark driver
     *
     * @return MobilPark
     */
    protected function createMobilParkDriver()
    {
        $provider = new MobilPark(new Client());

        return $provider;
    }

    /**
     * Create an instance of the MobilDev driver
     *
     * @return MobilDev
     */
    protected function createMobilDevDriver()
    {
        $provider = new MobilDev(new Client());

        return $provider;
    }

    /**
     * Create an instance of the MayerMobile driver
     *
     * @return MayerMobile
     */
    protected function createMayerMobileDriver()
    {
        $provider = new MayerMobile(new Client());

        return $provider;
    }
}