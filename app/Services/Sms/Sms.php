<?php

namespace App\Services\Sms;


class Sms
{
    /**
     * The Driver Interface instance.
     *
     * @var Drivers\DriverInterface
     */
    protected $driver;

    /**
     * Sms constructor.
     *
     * @param string $driver
     */
    public function __construct($driver = null)
    {
        if ($driver) {
            $this->driver = $this->driver($driver);
        }
    }

    public function driver($driver)
    {
        $manager = new DriverManager(app());
        $this->driver = $manager->driver($driver);

        return $this->driver;
    }

    /**
     * @param string $to
     * @param string $message
     * @param array  $parameters
     *
     * @return bool
     */
    public function send($to, $message, $parameters = [])
    {
        return $this->driver->send($to, $message, $parameters);
    }
}