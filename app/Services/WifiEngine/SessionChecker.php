<?php

namespace App\Services\WifiEngine;


use App\ConnectionStatus;
use App\Services\RadiusCommunicator\RadiusCommunicator;
use Carbon\Carbon;

class SessionChecker
{
    const STATUS_ONLINE_UNPROCESSED = 0;
    const STATUS_ONLINE_PROCESSED = 1;
    const STATUS_OFFLINE_UNPROCESSED = 9;
    const STATUS_OFFLINE_PROCESSED = 10;
    const STATUS_ELEVEN = 11;
    const CONNECTION_STATUS_ZERO = 0;
    private $radiusDriver;


    /**
     * SessionChecker constructor.
     */
    public function __construct()
    {
        // todo add radius class constructor parameter
        $this->radiusDriver = (new RadiusCommunicator)->driver(config('main.radius_server'));
    }


    /**
     * Check wifi sessions
     */
    public function run()
    {
        $sessions = $this->getSessions();
    }

    /**
     * @param int $count
     */
    private function getSessions($count = 10000)
    {
        $statusOptions = [self::STATUS_ONLINE_UNPROCESSED, self::STATUS_OFFLINE_UNPROCESSED];

        $items = \DB::table('wifi_sessions_origin')
            ->select([
                    'id',
                    'session_id',
                    'calling_station_id as guest_device_mac',
                    'called_station_id as device_mac',
                    'username as username',
                    'start_time as start_time',
                    'end_time',
                    'internal_session_id',
                    'guest_device_logout_time',
                    'session_status as status',
                    'framed_ip_addr',
                    'guest_device_id',
                    'guest_id',
                    'tenant_id',
                    'venue_id',
                    'service_profile_id',
                    'login_type',
                    'last_updated_time',
                    'timestamp',
                ])
            ->whereIn('session_status', $statusOptions)
            ->take($count)
            ->orderBy('timestamp')
            ->get();

        foreach ($items AS $session) {
            $session->guest_device_mac = $this->formatMac($session->guest_device_mac);
            $session->device_mac = $this->formatMac($session->device_mac);
            $session->internal_session_id = trim($session->internal_session_id);
            $guestData = $this->getGuest($session);

            if (!$guestData) {
                $this->updateSessionStatus($session);
                continue;
            }

            $deviceId = $this->getDeviceId($session->device_mac);

            if ($session->status == self::STATUS_ONLINE_UNPROCESSED) {
                $session->connectionStatus = self::STATUS_ONLINE_PROCESSED;
            } else {
                $session->connectionStatus = self::STATUS_ONLINE_UNPROCESSED;
            }

            $wifiSessionsOrigin = $this->checkWifiSessionsOnlineOfflineRecords($session);

            if (!($session->guest_device_id > 0)) {
                if ($session->status == self::STATUS_OFFLINE_UNPROCESSED) {
                    $session->status = self::STATUS_ELEVEN;
                    $this->updateWifiSessionOriginStatusBySession($session);
                    continue;
                }
            }

            if ($wifiSessionsOrigin) {
                $this->updateWifiSessionOriginInternalSessionId($wifiSessionsOrigin, $session);
            }

            $parentSessionId = null;
            $hasParentSession = null;
            $wifiSessions = $this->checkWifiSessions($session);

            if (!$wifiSessions->isEmpty()) {
                $parentSessionId = $this->createParentSessionId($session);

                $hasParentSession = $this->hasWifiSessionParentId($session, $parentSessionId);
                if (!$hasParentSession && $session->status == self::STATUS_ONLINE_UNPROCESSED) {
                    $this->insertNewWifiSessionParentSession($wifiSessions, $parentSessionId, $guestData, $session, $deviceId);
                    $this->updateParentSessionId($parentSessionId, $wifiSessions);
                }
            }

            $wifiSession = $this->checkWifiSessionsById($session->id);

            if (!$wifiSession) {
                $this->insertWifiSessions($guestData, $session, $deviceId, $parentSessionId, $wifiSessionsOrigin);
            }

            if (isset($parentSessionId)) {
                $this->updateConnectionStatusBySessionId($session, $parentSessionId);
            }

            if($session->status == self::STATUS_ONLINE_UNPROCESSED){
                $guestDeviceData = $this->getGuestDeviceIdByMac($session->guest_device_mac);
                $this->updateGuestForVisit($guestData, $guestDeviceData, $session);
            }

            $this->updateGuest($session);
            $this->updateGuestDeviceConnectionStatus($session);
            $this->updateWifiSessionsConnectionStatus($session);
            $this->updateSessionAsInternal($session);

            if ($session->status == self::STATUS_OFFLINE_UNPROCESSED) {
//                $this->deleteRadiusUsers($guestData->username);
            }
        }
    }

    /**
     * @param string $mac
     *
     * @return string
     */
    private function formatMac($mac)
    {
        $newMac = str_replace(['-', ':', '.'], '', trim($mac));
        $newMac = substr($newMac, 0, 12);
        $newMac = sprintf(
            '%s:%s:%s:%s:%s:%s',
            substr($newMac, 0, 2),
            substr($newMac, 2, 2),
            substr($newMac, 4, 2),
            substr($newMac, 6, 2),
            substr($newMac, 8, 2),
            substr($newMac, 10, 2)
        );

        return $newMac;
    }

    /**
     * @param $session
     *
     * @return int
     */
    public function updateWifiSessionOriginStatusBySession($session)
    {

        if ($session->status != self::STATUS_ELEVEN) {
            $sessionStatus = $session->status + 1;
        } else {
            $sessionStatus = self::STATUS_ELEVEN;
        }
        return \DB::table('wifi_sessions_origin')
            ->where('session_id', $session->session_id)
            ->update([
                'session_status'    => $sessionStatus,
                'connection_status' => self::CONNECTION_STATUS_ZERO,
            ]);
    }


    /**
     * @param $session
     *
     * @return array|null|\stdClass
     */
    private function getGuest($session)
    {

        if ($session->guest_id > 0) {
            return \DB::table('guests')
                ->select([
                    'guests.id as guest_id',
                    'guests.username',
                    'guests.total_visits',
                    'guests.connection_status',
                    'guests.total_duration',
                    'guests.total_downloaded_data',
                    'guests.total_uploaded_data',
                    'guests.access_code',
                ])
                ->where('guests.id', $session->guest_id)
                ->first();
        }

        return \DB::table('guests')
            ->select([
                'guests.id as guest_id',
                'guests.username',
                'guests.total_visits',
                'guests.connection_status',
                'guests.total_downloaded_data',
                'guests.total_uploaded_data',
                'guests.access_code',
            ])
            ->where('guests.username', $session->username)
            ->first();
    }

    /**
     * @param $session
     *
     * @return int
     */
    private function updateSessionAsInternal($session)
    {
        return \DB::table('wifi_sessions_origin')
            ->where('id', $session->id)
            ->update(
                [
                    'session_status'    => $session->status + 1,
                    'connection_status' => $session->connectionStatus,
                ]
            );
    }


    /**
     * @param $session
     *
     * @return array|null|\stdClass
     */
    private function checkWifiSessionsOnlineOfflineRecords($session)
    {
        return \DB::table('wifi_sessions_origin')
            ->select([
                'id',
                'session_id',
                'internal_session_id',
            ])
            ->where('calling_station_id', $session->guest_device_mac)
            ->where('username', '<>', $session->username)
            ->where('timestamp', '<', $session->timestamp)
            ->whereIn('session_status', [self::STATUS_ONLINE_UNPROCESSED, self::STATUS_ONLINE_PROCESSED])
            ->orderBy('id', 'DESC')
            ->first();
    }

    /**
     * @param $wifiSessionsOrigin
     * @param $session
     *
     * @return int
     */
    private function updateWifiSessionOriginInternalSessionId($wifiSessionsOrigin, $session)
    {
        return \DB::table('wifi_sessions_origin')
            ->where('id', '=', $session->id)
            ->update([
                'internal_session_id' => $wifiSessionsOrigin->session_id,
            ]);
    }


    /**
     * @param $session
     *
     * @return mixed
     */
    private function checkWifiSessions($session)
    {
        return \DB::table('wifi_sessions')
            ->where('username', $session->username)
            ->where('guest_device_logout_time', $session->guest_device_logout_time)
            ->where('venue_id', $session->venue_id)
            ->where('tenant_id', $session->tenant_id)
            ->where('guest_device_id', $session->guest_device_id)
            ->orderBy('guest_device_logout_time', 'ASC')
            ->get();
    }

    /**
     * @param $session
     *
     * @return string
     */
    private function createParentSessionId($session)
    {
        $guestDeviceLogoutTime = str_replace([":", "-", " "], "", $session->guest_device_logout_time);
        return 'ps_' . $session->guest_device_id . '_' . $guestDeviceLogoutTime;
    }


    /**
     * @param $session
     * @param $parentSessionId
     *
     * @return bool
     */
    private function hasWifiSessionParentId($session, $parentSessionId)
    {
        return (bool)\DB::table('wifi_sessions')
            ->where('session_id', $parentSessionId)
            ->where('venue_id', $session->venue_id)
            ->where('tenant_id', $session->tenant_id)
            ->where('guest_device_id', $session->guest_device_id)
            ->orderBy('start_time', 'ASC')
            ->first();
    }


    /**
     * @param $wifiSessions
     * @param $parentSessionId
     * @param $guestData
     * @param $session
     * @param $deviceId
     *
     * @return bool
     */
    private function insertNewWifiSessionParentSession($wifiSessions, $parentSessionId, $guestData, $session, $deviceId = null)
    {
        $duration = $downloaded_data = $uploaded_data = null;

        if (isset($wifiSessions)) {
            foreach ($wifiSessions as $key => $wifiSession) {
                $duration += $wifiSession->duration;
                $downloaded_data += $wifiSession->downloaded_data;
                $uploaded_data += $wifiSession->uploaded_data;
            }
        }

        return \DB::table('wifi_sessions')
            ->insert([
                'tenant_id'                => $session->tenant_id,
                'venue_id'                 => $session->venue_id,
                'guest_id'                 => $session->guest_id,
                'guest_device_ip'          => $session->framed_ip_addr,
                'start_time'               => $session->start_time ?? Carbon::now(),
                'service_profile_id'       => $session->service_profile_id,
                'login_type'               => $session->login_type,
                'access_code'              => $guestData->access_code,
                'guest_device_id'          => $session->guest_device_id,
                'device_id'                => $deviceId,
                'end_time'                 => $session->end_time ?? Carbon::now(),
                'guest_device_mac'         => $session->guest_device_mac,
                'username'                 => $session->username,
                'guest_device_logout_time' => $session->guest_device_logout_time,
                'session_id'               => $parentSessionId,
                'last_updated_time'        => $session->last_updated_time,
                'timestamp'                => $session->timestamp,
                'duration'                 => $duration,
                'downloaded_data'          => $downloaded_data,
                'uploaded_data'            => $uploaded_data,
            ]);
    }


    /**
     * @param $id
     *
     * @return bool
     */
    private function checkWifiSessionsById($id)
    {
        return (bool)\DB::table('wifi_sessions')
            ->where('wifi_sessions_origin_id', $id)
            ->first();
    }


    /**
     * @param $guestData
     * @param $session
     * @param $deviceId
     * @param $parentSessionId
     * @param $wifiSessionsOrigin
     *
     * @return bool
     */
    private function insertWifiSessions($guestData, $session, $deviceId = null, $parentSessionId, $wifiSessionsOrigin)
    {
        return \DB::table('wifi_sessions')
            ->insert([
                'tenant_id'                => $session->tenant_id,
                'venue_id'                 => $session->venue_id,
                'guest_device_ip'          => $session->framed_ip_addr,
                'start_time'               => $session->start_time ?? Carbon::now(),
                'end_time'                 => $session->end_time ?? Carbon::now(),
                'service_profile_id'       => $session->service_profile_id,
                'login_type'               => $session->login_type,
                'wifi_sessions_origin_id'  => $session->id,
                'guest_device_id'          => $session->guest_device_id,
                'device_id'                => $deviceId,
                'guest_device_mac'         => $session->guest_device_mac,
                'username'                 => $session->username,
                'guest_device_logout_time' => $session->guest_device_logout_time,
                'session_id'               => $session->session_id,
                'parent_session_id'        => $parentSessionId,
                'guest_id'                 => $guestData->guest_id,
                'access_code'              => $guestData->access_code,
                'last_updated_time'        => $session->last_updated_time,
                'timestamp'                => $session->timestamp,
                'internal_session_id'      => $wifiSessionsOrigin->session_id ?? null,
            ]);
    }

    /**
     * @param $parentSessionId
     * @param $wifiSessions
     *
     * @return int
     */
    private function updateParentSessionId($parentSessionId, $wifiSessions)
    {
        $desiredIds = [];
        foreach ($wifiSessions AS $sameDeviceRecord) {
            $desiredIds[] = $sameDeviceRecord->id;
        }

        return \DB::table('wifi_sessions')
            ->whereIn('id', $desiredIds)
            ->update([
                    'parent_session_id' => $parentSessionId,
                ]);
    }

    /**
     * @param $guestDeviceMac
     *
     * @return array|null|\stdClass
     */
    private function getGuestDeviceIdByMac($guestDeviceMac)
    {
        return \DB::table('guest_devices')
            ->where('mac', $guestDeviceMac)
            ->orderBy('created_at', 'DESC')
            ->first(['login_time']);
    }


    /**
     * @param $guestData
     * @param $guestDeviceData
     * @param $session
     *
     * @return int
     */
    private function updateGuestForVisit($guestData, $guestDeviceData, $session)
    {
        return \DB::table('guests')
            ->where('username', $session->username)
            ->update([
                'total_visits'    => \DB::raw($guestData->total_visits + 1),
                'login_time'      => $guestDeviceData->login_time ?? null,
                'last_visit_time' => $session->start_time,
            ]);
    }

    /**
     * @param $session
     *
     * @return int
     */
    private function updateGuest($session)
    {
        $attributes = [
            'connection_status' => $session->connectionStatus,
            'updated_at'        => $session->last_updated_time,
        ];

        if ($session->connectionStatus == ConnectionStatus::DISCONNECTED) {
            $attributes['access_code'] = null;
        }

        return \DB::table('guests')
            ->where('username', $session->username)
            ->update($attributes);
    }

    /**
     * @param $username
     *
     * @return int
     */
    private function deleteRadiusUsers($username)
    {
        return $this->radiusDriver->deleteWiFiUser($username);
    }

    /**
     * @param $session
     *
     * @return mixed
     */
    private function updateGuestDeviceConnectionStatus($session)
    {
        return \DB::table('guest_devices')
            ->where('mac', $session->guest_device_mac)
            ->update(['connection_status' => $session->connectionStatus]);
    }

    /**
     * @param $session
     *
     * @return mixed
     */
    private function updateWifiSessionsConnectionStatus($session)
    {
        return \DB::table('wifi_sessions')
            ->where('session_id', $session->session_id)
            ->update([
                'session_status'    => $session->status,
                'connection_status' => $session->connectionStatus,
            ]);
    }

    /**
     * @param $deviceMac
     *
     * @return mixed
     */
    private function getDeviceId($deviceMac)
    {
        return \DB::table('devices')
            ->where('mac', $deviceMac)
            ->value('id');
    }

    /**
     * @param $session
     * @param $parentSessionId
     *
     * @return int
     */
    private function updateConnectionStatusBySessionId($session, $parentSessionId)
    {
        return \DB::table('wifi_sessions')
            ->where('session_id', $parentSessionId)
            ->update([
                'session_status'    => $session->status,
                'connection_status' => $session->connectionStatus,
            ]);
    }

    /**
     * @param $session
     *
     * @return int
     */
    private function updateSessionStatus($session)
    {
        return \DB::table('wifi_sessions_origin')
            ->where('session_id', $session->session_id)
            ->update([
                'session_status' => self::STATUS_ONLINE_PROCESSED,
            ]);
    }

}