<?php

namespace App\Services\WifiEngine;


/**
 * Class Engine
 *
 * @package App\Services\WifiEngine
 */
class Engine
{
    /**
     * @var SessionSync
     */
    private $sessionSync;

    /**
     * @var SessionChecker
     */
    private $sessionChecker;

    /**
     * @var AccountingChecker
     */
    private $accountingChecker;

    /**
     * Engine constructor.
     *
     * @param SessionSync       $sessionSync
     * @param SessionChecker    $sessionChecker
     * @param AccountingChecker $accountingChecker
     */
    public function __construct(SessionSync $sessionSync, SessionChecker $sessionChecker, AccountingChecker $accountingChecker)
    {
        $this->sessionSync = $sessionSync;
        $this->sessionChecker = $sessionChecker;
        $this->accountingChecker = $accountingChecker;
    }

    /**
     * Sync wifi sessions
     */
    public function runSyncSessions()
    {
        $this->sessionSync->run();
    }

    /**
     * Session Checker
     */
    public function runSessionChecker()
    {
        $this->sessionChecker->run();
    }

    /**
     * Accounting Checker
     */
    public function runAccountingChecker()
    {
        $this->accountingChecker->run();
    }
}