<?php

namespace App\Services\WifiEngine;


/**
 * Class AccountingChecker
 *
 * @package App\Services\WifiEngine
 */
class AccountingChecker
{
    const RECORD_STATUS_UNFINISHED = 0;
    const RECORD_STATUS_FINISHED = 1;


    /**
     * Check wifi sessions
     */
    public function run()
    {
        $records = $this->getAccountingRecords();
    }

    /**
     * @param int $count
     */
    public function getAccountingRecords($count = 10000)
    {
        $records = \DB::table('wifi_accounting_records')
            ->select([
                'id',
                'session_id',
                'username',
                'calling_station_id',
                'called_station_id',
                'status_type',
                'framed_ip_addr',
                'timestamp',
                'input_octs',
                'out_octs',
                'acct_sess_time',
            ])
            ->where('record_status', self::RECORD_STATUS_UNFINISHED)
            ->take($count)
            ->orderBy('timestamp')
            ->get();


        foreach ($records as $key => $record) {
            $apMac = $record->called_station_id;
            $guestDeviceMac = $record->calling_station_id;
            $username = $record->username;

            $guestDevice = $this->getGuestDevice($guestDeviceMac);
            $wifiSession = $this->getWifiSessions($guestDeviceMac, $username, $record);
            if (!$guestDevice && !$wifiSession) {
                $this->updateGuestDeviceRecordStatusByMac($apMac, $guestDeviceMac);
                continue;
            }

            if ($guestDevice) {
                $isGuest = $this->getGuest($record->username);
                if ($isGuest) {

                    if ($wifiSession) {
                        if ($record->status_type === 'Start') {
                            $this->updateGuestDeviceRecordStatusByMac($apMac, $guestDeviceMac);
                            continue;
                        } else {
                            // current before validate record
                            $recordsNewData = $this->getSameSessionsIdPrevRecord($record->session_id, $record->timestamp);

                            if ($recordsNewData) {
                                $totalUploadedData = $record->input_octs - $recordsNewData->input_octs;
                                $totalDownloadedData = $record->out_octs - $recordsNewData->out_octs;
                                $totalDuration = $record->acct_sess_time - $recordsNewData->acct_sess_time;
                                $guest = $this->getGuestData($record->username);

                                // guests
                                if ($guest) {
                                    $totalDownloadedDataGuest = $guest->total_downloaded_data;
                                    $totalUploadedDataGuest = $guest->total_uploaded_data;
                                    $totalDurationDataGuest = $guest->total_duration;
                                    $totalDownloadedDataForGuest = $totalDownloadedData + $totalDownloadedDataGuest;
                                    $totalUploadedDataForGuest = $totalUploadedData + $totalUploadedDataGuest;
                                    $totalDurationForGuest = $totalDuration + $totalDurationDataGuest;
                                } else {
                                    $totalDownloadedDataForGuest = $totalDownloadedData;
                                    $totalUploadedDataForGuest = $totalUploadedData;
                                    $totalDurationForGuest = $totalDuration;
                                }

                                // guest devices
                                $this->updateDownloadUploadAndDurationDataForGuestDevice($guestDeviceMac, $record->input_octs, $record->out_octs, $record->acct_sess_time);
                                $totalDownloadedDataForGuestDevices = $totalDownloadedData + $guestDevice->total_downloaded_data;
                                $totalUploadedDataForGuestDevices = $totalUploadedData + $guestDevice->total_uploaded_data;
                                $totalDurationForGuestDevices = $totalDuration + $guestDevice->total_duration;

                                // wifi sessions
                                $totalDownloadedDataForWifiSessions = $totalDownloadedData + $wifiSession->downloaded_data;
                                $totalUploadedDataForWifiSessions = $totalUploadedData + $wifiSession->uploaded_data;
                                $totalDurationForWifiSessions = $totalDuration + $wifiSession->duration;

                                // update total download upload and duration values
                                $this->updateDownloadUploadAndDurationData($record, $type = 'guests', $guestDeviceMac, $apMac, $username, $totalDownloadedDataForGuest, $totalUploadedDataForGuest, $totalDurationForGuest);
                                $this->updateDownloadUploadAndDurationData($record, $type = 'wifiSession', $guestDeviceMac, $apMac, $username, $totalDownloadedDataForWifiSessions, $totalUploadedDataForWifiSessions, $totalDurationForWifiSessions);
                                $this->updateDownloadUploadAndDurationData($record, $type = 'guestDevices', $guestDeviceMac, $apMac, $username, $totalDownloadedDataForGuestDevices, $totalUploadedDataForGuestDevices, $totalDurationForGuestDevices);
                                $this->updateGuestDeviceRecordStatusByMac($apMac, $guestDeviceMac);
                            }
                        }
                    }

                    $this->updateNotRealInternal($record);
                    $this->updateGuestDeviceRecordStatusByMac($apMac, $guestDeviceMac);
                } else {
                    if ($record->status_type === 'Stop') {
                        $internalSession = $this->hasWifiSessionInternalSessionId($record);
                        $username = $internalSession->username;
                        // guest internal session
                        $guest = $this->getGuestData($username);

                        // current before validate record
                        $recordsNewData = $this->getSameSessionsIdPrevRecord($record->session_id, $record->timestamp);

                        if ($recordsNewData) {
                            $totalUploadedData = $record->input_octs - $recordsNewData->input_octs;
                            $totalDownloadedData = $record->out_octs - $recordsNewData->out_octs;
                            $totalDuration = $record->acct_sess_time - $recordsNewData->acct_sess_time;
                            if ($guest) {
                                $totalDownloadedDataGuest = $guest->total_downloaded_data;
                                $totalUploadedDataGuest = $guest->total_uploaded_data;
                                $totalDurationDataGuest = $guest->total_duration;
                                $totalDownloadedDataForGuest = $totalDownloadedData + $totalDownloadedDataGuest;
                                $totalUploadedDataForGuest = $totalUploadedData + $totalUploadedDataGuest;
                                $totalDurationForGuest = $totalDuration + $totalDurationDataGuest;
                            }
                            // wifi sessions internal session
                            $totalDownloadedDataForWifiSessions = $totalDownloadedData + $internalSession->downloaded_data;
                            $totalUploadedDataForWifiSessions = $totalUploadedData + $internalSession->uploaded_data;
                            $totalDurationForWifiSessions = $totalDuration + $internalSession->duration;

                            // guest devices internal session
                            $this->updateDownloadUploadAndDurationDataForGuestDevice($guestDeviceMac, $record->input_octs, $record->out_octs, $record->acct_sess_time);
                            $totalDownloadedDataForGuestDevices = $totalDownloadedData + $guestDevice->total_downloaded_data;
                            $totalUploadedDataForGuestDevices = $totalUploadedData + $guestDevice->total_uploaded_data;
                            $totalDurationForGuestDevices = $totalDuration + $guestDevice->total_duration;

                            // update total download upload and duration values
                            $this->updateDownloadUploadAndDurationData($record, $type = 'guests', $guestDeviceMac, $apMac, $username, $totalDownloadedDataForGuest, $totalUploadedDataForGuest, $totalDurationForGuest);
                            $this->updateWifiSessionInternalSessionId($record, $totalDownloadedDataForWifiSessions, $totalUploadedDataForWifiSessions, $totalDurationForWifiSessions);
                            $this->updateDownloadUploadAndDurationData($record, $type = 'guestDevices', $guestDeviceMac, $apMac, $username, $totalDownloadedDataForGuestDevices, $totalUploadedDataForGuestDevices, $totalDurationForGuestDevices);
                            $this->updateGuestDeviceRecordStatusByMac($apMac, $guestDeviceMac);
                        }
                    }
                }
            }
        }

        if (isset($record)) {
            $parentSessions = $this->getParentSessions();
            foreach ($parentSessions as $k => $val) {
                $virArr[$val->parent_session_id]['duration'][] = $val->duration;
                $virArr[$val->parent_session_id]['uploaded_data'][] = $val->uploaded_data;
                $virArr[$val->parent_session_id]['downloaded_data'][] = $val->downloaded_data;

            }
            if (isset($virArr)) {
                foreach ($virArr as $parenSessionId => $virtualTotal) {
                    $total = [
                        'parent_session_id' => $parenSessionId,
                        'duration'          => array_sum($virtualTotal['duration']),
                        'uploaded_data'     => array_sum($virtualTotal['uploaded_data']),
                        'downloaded_data'   => array_sum($virtualTotal['downloaded_data']),
                    ];
                    $this->updateWifiSessionBySessionId($total);
                }
            }
        }
    }

    /**
     * @param $total
     *
     * @return mixed
     */
    private
    function updateWifiSessionBySessionId($total)
    {
        return \DB::table('wifi_sessions')
            ->where('session_id', $total['parent_session_id'])
            ->update([
                'duration'        => $total['duration'],
                'uploaded_data'   => $total['uploaded_data'],
                'downloaded_data' => $total['downloaded_data'],
            ]);

    }

    /**
     * @return mixed
     */
    public
    function getParentSessions()
    {
        return \DB::table('wifi_sessions')
            ->whereNotNull('parent_session_id')
            ->get();
    }

    /**
     * @param string $mac
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public
    function getDevices(string $mac)
    {
        return \DB::table('devices')->where('mac', $mac)->get();
    }

    /**
     * @param string $mac
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public
    function getGuestDevice(string $mac)
    {
        return \DB::table('guest_devices')
            ->where('mac', $mac)
            ->first([
                'id',
                'mac',
                'total_downloaded_data',
                'downloaded_data',
                'total_uploaded_data',
                'uploaded_data',
                'total_duration',
                'duration',
            ]);
    }


    /**
     * @param string $guestDeviceMac
     * @param string $username
     * @param $record
     *
     * @return mixed
     */
    public
    function getWifiSessions(string $guestDeviceMac, string $username, $record)
    {
        return \DB::table('wifi_sessions')
            ->where('session_id', $record->session_id)
            ->where('guest_device_mac', $guestDeviceMac)
            ->where('username', $username)
            ->first(['downloaded_data', 'uploaded_data', 'duration']);
    }


    /**
     * @param string $apMac
     * @param string $guestDeviceMac
     *
     * @return mixed
     */
    public
    function updateGuestDeviceRecordStatusByMac(string $apMac, string $guestDeviceMac)
    {
        return \DB::table('wifi_accounting_records')
            ->where('called_station_id', $apMac)
            ->where('calling_station_id', $guestDeviceMac)
            ->update(['record_status' => self::RECORD_STATUS_FINISHED]);
    }


    /**
     * @param int $sessionId
     * @param $createdDate
     *
     * @return mixed
     */
    public
    function getSameSessionsIdPrevRecord($sessionId, $createdDate)
    {
        return \DB::table('wifi_accounting_records')
            ->select(
                'input_octs',
                'out_octs',
                'acct_sess_time'
            )
            ->where('session_id', $sessionId)
            ->where('timestamp', '<', $createdDate)
            ->orderBy('timestamp', 'desc')
            ->first();

    }


    /**
     * @param $record
     * @param $type
     * @param $guestDeviceMac
     * @param null $apMac
     * @param $totalDownloadedData
     * @param $totalUploadedData
     * @param $totalDuration
     *
     * @return int
     */
    public
    function updateDownloadUploadAndDurationData($record, $type, $guestDeviceMac, $apMac = null, $username = null, $totalDownloadedData, $totalUploadedData, $totalDuration)
    {

//        dump($isInternal, $record);
        if ($type === 'guests') {
            return \DB::table('guests')
                ->where('username', $username)
                ->update(
                    [
                        'total_downloaded_data' => $totalDownloadedData,
                        'total_uploaded_data'   => $totalUploadedData,
                        'total_duration'        => $totalDuration,
                    ]
                );
        }

        if ($type === 'guestDevices') {
            return \DB::table('guest_devices')
                ->where('mac', $guestDeviceMac)
                ->update(
                    [
                        'total_downloaded_data' => $totalDownloadedData,
                        'downloaded_data'       => $record->out_octs,
                        'total_uploaded_data'   => $totalUploadedData,
                        'uploaded_data'         => $record->input_octs,
                        'total_duration'        => $totalDuration,
                        'duration'              => $record->acct_sess_time,
                    ]
                );
        }

        if ($type === 'wifiSession') {
            return \DB::table('wifi_sessions')
                ->where('guest_device_mac', $guestDeviceMac)
                ->where('session_id', $record->session_id)
                ->update(
                    [
                        'downloaded_data' => $totalDownloadedData,
                        'uploaded_data'   => $totalUploadedData,
                        'duration'        => $totalDuration,
                    ]
                );

        }
    }

    /**
     * @param string $username
     *
     * @return array|null|\stdClass
     */
    public
    function getGuest(string $username)
    {
        return \DB::table('guests')
            ->where('username', $username)
            ->first(['id']);
    }

    /**
     * @param $guestDeviceMac
     * @param $input_octs
     * @param $out_octs
     * @param $acct_sess_time
     *
     * @return mixed
     */
    public
    function updateDownloadUploadAndDurationDataForGuestDevice($guestDeviceMac, $input_octs, $out_octs, $acct_sess_time)
    {
        return \DB::table('guest_devices')
            ->where('mac', $guestDeviceMac)
            ->update(
                [
                    'downloaded_data' => $out_octs,
                    'uploaded_data'   => $input_octs,
                    'duration'        => $acct_sess_time,
                ]
            );
    }

    /**
     * @param $username
     *
     * @return array|null|\stdClass
     */
    public
    function getGuestData($username)
    {
        return \DB::table('guests')
            ->where('username', $username)
            ->first(['total_downloaded_data', 'total_uploaded_data', 'total_duration']);
    }


    /**
     * @param $record
     *
     * @return array|null|\stdClass
     */
    private function hasWifiSessionInternalSessionId($record)
    {
        return \DB::table('wifi_sessions')
            ->where('internal_session_id', $record->session_id)
            ->first();
    }

    /**
     * @param $record
     * @param $download
     * @param $upload
     * @param $duration
     *
     * @return int
     */
    private function updateWifiSessionInternalSessionId($record, $download, $upload, $duration)
    {
        return \DB::table('wifi_sessions')
            ->where('internal_session_id', $record->session_id)
            ->update([
                'duration'        => $duration,
                'uploaded_data'   => $upload,
                'downloaded_data' => $download,
            ]);
    }

    /**
     * @param $record
     *
     * @return int
     */
    private function updateNotRealInternal($record)
    {
        \DB::table('wifi_sessions')
            ->where('session_id', $record->session_id)
            ->update([
                'internal_session_id' => null,
            ]);

        return \DB::table('wifi_sessions_origin')
            ->where('session_id', $record->session_id)
            ->update([
                'internal_session_id' => null,
            ]);

    }
}