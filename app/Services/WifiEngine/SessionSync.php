<?php

namespace App\Services\WifiEngine;


use App\Services\RadiusCommunicator\RadiusCommunicator;

class SessionSync
{
    const STATUS_DISCONNECTED = 0;
    const STATUS_CONNECTED = 1;
    private $radiusDriver;

    /**
     * SessionChecker constructor.
     */
    public function __construct()
    {
        // todo add radius class constructor parameter
        $this->radiusDriver = (new RadiusCommunicator)->driver(config('main.radius_server'));
    }

    /**
     * Sync wifi sessions
     */
    public function run()
    {
        // get online guest devices
        $onlineGuestDevices = $this->getAllOnlineGuestDevices();
        $onlineGuestDevices->each(
            function ($guestDevice) {
                $guestId = $guestDevice->guest_id;
                $result = false;

                // if the device's owner has no other online devices, disconnect him
                if (!$this->guestDeviceHasSession($guestDevice)) {
                    $result = $this->disconnectGuestDevice($guestDevice);
                }

                if ($result && $this->guestHasOnlineDevices($guestId, $guestDevice->id)) {
                    $this->disconnectGuest($guestId);
                    $this->disconnectWifiSession($guestDevice);
                    $this->deleteRadiusUser($guestDevice->username);
                }
            }
        );
    }

    /**
     * Get list of online guest devices
     *
     * @return \Illuminate\Support\Collection
     */
    private function getAllOnlineGuestDevices()
    {
        return \DB::table('guest_devices')
            ->select('guest_devices.id', 'guest_devices.guest_id', 'guest_devices.mac', 'guests.username')
            ->join('guests', 'guest_devices.guest_id', '=', 'guests.id')
            ->where('guest_devices.connection_status', self::STATUS_CONNECTED)
            ->get();
    }

    /**
     * Check if the given guest has online devices
     *
     * @param int $guestId
     * @param int $guestDeviceId
     *
     * @return bool
     */
    private function guestHasOnlineDevices($guestId, int $guestDeviceId)
    {
        if (!$guestId) {
            return false;
        }

        return (bool)\DB::table('guest_devices')
            ->where('guest_id', $guestId)
            ->where('connection_status', self::STATUS_CONNECTED)
            ->where('id', '<>', $guestDeviceId)
            ->value('id');
    }

    /**
     * Update the given guest as disconnected
     *
     * @param int $guestId
     *
     * @return int
     */
    private function disconnectGuest($guestId)
    {
        if (!$guestId) {
            return false;
        }

        return \DB::table('guests')
            ->where('id', $guestId)
            ->take(1)
            ->update(['connection_status' => self::STATUS_DISCONNECTED]);
    }

    /**
     * Delete the given radius user
     *
     * @param $username
     *
     * @return bool
     */
    private function deleteRadiusUser($username)
    {
        return $this->radiusDriver->deleteWiFiUser($username);
    }

    /**
     * check if the given guest device has online sessions
     *
     * @param $guestDevice
     *
     * @return bool
     */
    private function guestDeviceHasSession($guestDevice)
    {
        return (bool)\DB::table('wifi_sessions_origin')
            ->where('connection_status', self::STATUS_CONNECTED)
            ->where('calling_station_id', $guestDevice->mac)
            ->first(['id']);
    }

    /**
     * Update the given guest device as disconnected
     *
     * @param $guestDevice
     *
     * @return int
     */
    private function disconnectGuestDevice($guestDevice)
    {
        return (bool)\DB::table('guest_devices')
            ->where('id', $guestDevice->id)
            ->take(1)
            ->update(['connection_status' => self::STATUS_DISCONNECTED]);
    }

    private function disconnectWifiSession($guestDevice)
    {
        return \DB::table('wifi_sessions')
            ->where('id', $guestDevice->mac)
            ->take(1)
            ->update(['connection_status' => self::STATUS_DISCONNECTED]);
    }
}