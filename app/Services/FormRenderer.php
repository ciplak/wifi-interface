<?php

namespace App\Services;


use App\Models\SplashPage;

class FormRenderer
{
    /**
     * Render form fields
     *
     * @param array  $fields
     * @param string $prefix
     * @param array  $masks
     * @param array  $options
     */
    public static function render(array $fields, $prefix = '', $masks = [], $options = [])
    {
        $locale = \App::getLocale();

        collect($fields)->each(
            function ($field, $fieldName) use ($locale, $prefix, $masks, $options) {

                $title = isset($field['custom']) && $field['custom']
                    ? array_get($field, "title.$locale")
                    : trans("login.form.$fieldName");

                $title = e($title);

                $inputId = $prefix . $fieldName;

                echo '<div class="form-group" id="' . $inputId . '-form-group" :class="{\'has-error\': form.errors.has(\'' . $fieldName . '\')}">';

                $htmlAttributes = ['id' => $inputId, 'class' => 'form-control', 'v-model' => 'form.' . $fieldName];
                $isRequired = isset($field['required']) && $field['required'];

                if ($isRequired) {
                    $htmlAttributes['required'] = true;
                }

                if (isset($masks[$fieldName]) && trim($masks[$fieldName])) {
                    $htmlAttributes['v-mask'] = "'" . trim($masks[$fieldName]) . "'";
                }

                $labelOptions = [
                    'id' => $inputId . '-label',
                ];

                if ($isRequired) {
                    $labelOptions['class'] = 'required';
                }

                echo \Form::label($inputId, $title, $labelOptions);

                $value = null;

                $type = isset($field['type']) ? $field['type'] : 'text';

                switch ($type) {
                    case \App\Models\SplashPage::FIELD_TYPE_SELECT:
                        $list = SplashPage::getOptionsForField($fieldName);

                        echo \Form::select($fieldName, $list,  null, $htmlAttributes);
                        break;

                    default:
                        $htmlAttributes['placeholder'] = $fieldName == 'birthday' ? 'D/M/Y' : $title;

                        $type = 'text';
                        if ($fieldName == 'email') {
                            $type = 'email';
                        }
                        if ($fieldName == 'phone') {
                            $type = 'tel';
                        }
                        if ($fieldName == 'birthday') {
                            $htmlAttributes['id'] = $prefix . 'birthday';
                            $htmlAttributes['dir'] = 'ltr';
                            if (isRTL()) {
                                $htmlAttributes['class'] .= ' text-right';
                            }
                        }

                        if ($type == 'tel') {
                            if (isset($options['phone-country-code-text'])) {
                                $htmlAttributes['phone-country-code-text'] = $options['phone-country-code-text'];
                            }

                            echo \Form::phone($fieldName, $value, $htmlAttributes);
                        } else {
                            echo \Form::input($type, $fieldName, $value, $htmlAttributes);
                        }
                }

                echo '<span class="help-block">{{ form.errors.get(\'' . $fieldName . '\') }}</span>';
                echo '</div>';
            }
        );
    }

    public static function languageSelector()
    {
        $output = '<div id="language-selector">'
            . \Form::open(['id' => 'language-form', 'method' => 'get'])
            . '<div class="form-group">'
            . \Form::select('lang', getLocales(), \App::getLocale(), ['id' => 'language-selector', 'class' => 'form-control'])
            . '</div>'
            . \Form::close()
            . '</div>';

        return $output;
    }
}