<?php

namespace App\Services;


class SyslogParser
{
    public static function parseAvayaLog($log)
    {
        // debug    : Station 7c:01:91:6c:33:20: HTTP GET request received, gig1 MAC: 64:a7:dd:01:38:f6, Src IP: 192.168.168.114, Dst IP: 192.168.168.61, Dst Port: 80, URL: http://192.168.168.61/
        $pattern = '/Station\s*(?P<client_mac>([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})).*HTTP\s*(?P<method>GET|POST|DELETE|PUT|PATCH)\s*request(.*)gig1 MAC:\s*(?P<ap_mac>([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})).*, Src IP:\s*(?P<source_ip>.*), Dst IP:\s*(?P<destination_ip>.*), Dst Port:\s*(?P<destination_port>[0-9]{2,5}), URL: (?P<url>.*)/';

        return static::parse($pattern, $log);
    }

    public static function parseMerakiLog($log)
    {
        //May 05 13:54:44 24.136.237.228 logger:  <134>1 0.0 pavan urls src=10.111.191.59:61289 dst=23.1.61.15:80 mac=E4:CE:8F:29:6E:5A request: GET http://www.apple.com/library/test/success.html
        $pattern = '/src=(?P<source_ip>[0-9\.]{1,})\s*:\s*(?P<source_port>[0-9]{2,5})\s*dst=(?P<destination_ip>[0-9\.]{1,})\s*:\s*(?P<destination_port>[0-9]{2,5})\s*mac=(?P<ap_mac>([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})).*\s*(?P<method>GET|POST|DELETE|PUT|PATCH)\s*(?P<url>.*)/';

        return static::parse($pattern, $log);
    }

    /**
     * @param string $pattern
     * @param \stdClass $log
     *
     * @return array|bool
     */
    protected static function parse($pattern, $log)
    {
        preg_match($pattern, $log, $matches);

        return isset($matches[0]) ? $matches : false;
    }
}