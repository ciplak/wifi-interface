<?php

namespace App\Services;


use Illuminate\Http\Request;

class MacEstimator
{
    private function getMacKeys()
    {
        return [
            'ap_mac',   // Cisco, Meraki
            'apmac',    // Xirrus, Avaya
            'acmac',    // Other,
            'apname',   // Aruba
        ];
    }

    public function getMac(Request $request)
    {
        $mac = null;

        $macKeys = $this->getMacKeys();

        foreach ($macKeys as $key) {
            if ($request->has($key)) {
                $mac = $request->input($key);
                break;
            }
        }

        return $mac;
    }
}