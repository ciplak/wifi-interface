<?php

namespace App\Services\DeviceHandlers;


use App\Services\DeviceHandlers\Vendors\ArubaControllerDeviceVendor;
use App\Services\DeviceHandlers\Vendors\ArubaDeviceVendor;
use App\Services\DeviceHandlers\Vendors\AvayaDeviceVendor;
use App\Services\DeviceHandlers\Vendors\CiscoDeviceVendor;
use App\Services\DeviceHandlers\Vendors\FortinetDeviceVendor;
use App\Services\DeviceHandlers\Vendors\MerakiDeviceVendor;
use App\Services\DeviceHandlers\Vendors\XirrusDeviceVendor;
use Illuminate\Support\Manager;

class DriverManager extends Manager
{
    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return null;
    }

    /**
     * Create an instance of the Aruba driver
     *
     * @return DeviceVendorInterface
     */
    protected function createArubaDriver()
    {
        $provider = new ArubaDeviceVendor;

        return $provider;
    }

    /**
     * Create an instance of the Aruba controller driver
     *
     * @return DeviceVendorInterface
     */
    protected function createArubaControllerDriver()
    {
        $provider = new ArubaControllerDeviceVendor;

        return $provider;
    }

    /**
     * Create an instance of the Avaya driver
     *
     * @return DeviceVendorInterface
     */
    protected function createAvayaDriver()
    {
        $provider = new AvayaDeviceVendor;

        return $provider;
    }

    /**
     * Create an instance of the Cisco driver
     *
     * @return DeviceVendorInterface
     */
    protected function createCiscoDriver()
    {
        $provider = new CiscoDeviceVendor;

        return $provider;
    }

    /**
     * Create an instance of the Fortinet driver
     *
     * @return DeviceVendorInterface
     */
    protected function createFortinetDriver()
    {
        $provider = new FortinetDeviceVendor;

        return $provider;
    }

    /**
     * Create an instance of the Meraki driver
     *
     * @return DeviceVendorInterface
     */
    protected function createMerakiDriver()
    {
        $provider = new MerakiDeviceVendor;

        return $provider;
    }

    /**
     * Create an instance of the Xirrus driver
     *
     * @return DeviceVendorInterface
     */
    protected function createXirrusDriver()
    {
        $provider = new XirrusDeviceVendor;

        return $provider;
    }
}