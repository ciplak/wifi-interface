<?php
namespace App\Services\DeviceHandlers\Vendors;

use App\Models\Guest;
use App\Services\DeviceHandlers\AbstractDeviceVendor;

class CiscoDeviceVendor extends AbstractDeviceVendor
{
    /**
     * Set session variables from device parameters
     */
    public function setSessionVariables()
    {
        session([
            'device_login_page_url' => request('switch_url'),
            'ap_mac'                => request('ap_mac'),
            'client_mac'            => request('client_mac'),
            'client_ip'             => request()->ip(),
            'ssid_name'             => request('wlan'),
            'redirect'              => request('redirect'),
        ]);
    }

    /**
     * Check if client mac address is set in parameters
     *
     * @return mixed
     */
    public function hasClientMac()
    {
        return request()->has('client_mac');
    }

    /**
     * Get device status messages
     *
     * @return string
     */
    public function getMessages()
    {
        return [
            1 => 'You are already logged in. No further action is required on your part.',
            2 => 'You are not configured to authenticate against web portal. No further action is required on your part.',
            3 => 'The username specified cannot be used at this time. Perhaps the username is already logged into the system?',
            4 => 'You have been excluded.',
            5 => 'The User Name and Password combination you have entered is invalid. Please try again.',
        ];
    }

    /**
     * Check if device responded with an error
     *
     * @return mixed
     */
    public function hasErrors()
    {
        if (request()->has('statusCode')) {
            $statusCode = (int)request('statusCode', 1);

            if ($statusCode > 1) {
                $messages = $this->getMessages();
                $error = $messages[$statusCode] ?? "Status code: $statusCode";

                return $error;
            }
        }

        return false;
    }

    /**
     * @param Guest  $guest
     * @param string $redirectUrl
     *
     * @return array
     */
    public function getFormParameters(Guest $guest, $redirectUrl = '')
    {
        return [
            'method' => 'POST',
            'fields' => [
                'buttonClicked' => 4,
                'err_flag'      => 0,
                'redirect_url'  => $redirectUrl,
                'username'      => $guest->username,
                'password'      => $guest->password,
            ],
        ];
    }

    /**
     * Check if device responded with successful connection (includes already connected status)
     *
     * @return bool
     */
    public function isConnected()
    {
        return
            // already connected
            (request()->has('statusCode') && request('statusCode') == 1)
            ||
            // params not set
            !(request()->has('ap_mac') || request()->has('client_mac'));
    }

}