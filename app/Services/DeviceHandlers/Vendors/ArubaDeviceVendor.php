<?php
namespace App\Services\DeviceHandlers\Vendors;

use App\Models\Guest;
use App\Services\DeviceHandlers\AbstractDeviceVendor;

class ArubaDeviceVendor extends AbstractDeviceVendor
{
    /**
     * Set session variables from device parameters
     */
    public function setSessionVariables()
    {
        session([
            'device_login_page_url' => request()->has('switchip') ? sprintf('https://%s/swarm.cgi', request('switchip')) : 'https://securelogin.arubanetworks.com/cgi-bin/login',
            'ap_mac'                => request('apname'),
            'client_mac'            => request('mac'),
            'client_ip'             => request()->ip(),
            'ssid_name'             => request('essid'),
            'redirect'              => request('url'),
        ]);
    }

    /**
     * Check if client mac address is set in parameters
     *
     * @return mixed
     */
    public function hasClientMac()
    {
        return request()->has('mac');
    }

    /**
     * Get device status messages
     *
     * @return string
     */
    public function getMessages()
    {
        return [
            'notyet'  => 'Not yet',
            'success' => 'Success',
            'already' => 'Already connected',
            'failed'  => 'Failed to connect',
        ];
    }

    /**
     * Check if device responded with an error
     *
     * @return mixed
     */
    public function hasErrors()
    {
        if (request()->has('errmsg')) {
            return request('errmsg');
        }

        return false;
    }

    /**
     * @param Guest  $guest
     * @param string $redirectUrl
     *
     * @return array
     */
    public function getFormParameters(Guest $guest, $redirectUrl = '')
    {
        return [
            'method' => 'POST',
            'fields' => [
                'user'     => $guest->username,
                'password' => $guest->password,
                'cmd'      => 'authenticate',
            ],
        ];
    }

    /**
     * Check if device responded with successful connection (includes already connected status)
     *
     * @return bool
     */
    public function isConnected()
    {
        return !request()->has('errmsg') && (request('res') == 'success');
    }
}