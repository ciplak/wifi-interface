<?php
namespace App\Services\DeviceHandlers\Vendors;

class ArubaControllerDeviceVendor extends ArubaDeviceVendor
{
    /**
     * Set session variables from device parameters
     */
    public function setSessionVariables()
    {
        session([
            'device_login_page_url' => sprintf('https://%s/cgi-bin/login', 'securelogin.arubanetworks.com'),
            'ap_mac'                => request('apname'),
            'client_mac'            => request('mac'),
            'client_ip'             => request('ip'),
            'ssid_name'             => request('essid'),
            'redirect'              => request('url'),
        ]);
    }
}