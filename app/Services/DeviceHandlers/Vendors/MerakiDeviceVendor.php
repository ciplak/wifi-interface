<?php
namespace App\Services\DeviceHandlers\Vendors;

use App\Models\Guest;
use App\Services\DeviceHandlers\AbstractDeviceVendor;

class MerakiDeviceVendor extends AbstractDeviceVendor
{
    /**
     * Set session variables from device parameters
     */
    public function setSessionVariables()
    {
        session([
            'device_login_page_url' => request('login_url'),
            'ap_mac'                => request('ap_mac'),
            'client_mac'            => request('client_mac'),
            'client_ip'             => request()->ip(),
            'redirect'              => request('continue_url'),
        ]);
    }

    /**
     * Check if client mac address is set in parameters
     *
     * @return mixed
     */
    public function hasClientMac()
    {
        return request()->has('client_mac');
    }

    /**
     * Check if device responded with an error
     *
     * @return mixed
     */
    public function hasErrors()
    {
        if (request()->has('error_message')) {
            return request('error_message');
        }

        return false;
    }

    /**
     * @param Guest  $guest
     * @param string $redirectUrl
     *
     * @return array
     */
    public function getFormParameters(Guest $guest, $redirectUrl = '')
    {
        return [
            'method' => 'POST',
            'fields' => [
                'success_url' => $redirectUrl,
                'username'    => $guest->username,
                'password'    => $guest->password,
            ],
        ];
    }

    /**
     * Check if device responded with successful connection (includes already connected status)
     *
     * @return bool
     */
    public function isConnected()
    {
        return !request()->has('error_message') && request('res') == 'success';
    }
}