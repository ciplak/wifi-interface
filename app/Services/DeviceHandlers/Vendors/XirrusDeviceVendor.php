<?php
namespace App\Services\DeviceHandlers\Vendors;

use App\Models\Guest;
use App\Services\DeviceHandlers\AbstractDeviceVendor;

class XirrusDeviceVendor extends AbstractDeviceVendor
{
    /**
     * Set session variables from device parameters
     */
    public function setSessionVariables()
    {
        session([
            'device_login_page_url' => sprintf('http://%s:%s/logon', request('uamip'), request('uamport')),
            'ap_mac'                => str_replace('-', ':', request('apmac')),
            'client_mac'            => str_replace('-', ':', request('mac')),
            'client_ip'             => request()->ip(),
            'ssid_name'             => request('ssid'),
            'redirect'              => request('userurl'),
        ]);
    }

    /**
     * Check if client mac address is set in parameters
     *
     * @return mixed
     */
    public function hasClientMac()
    {
        return request()->has('mac');
    }

    /**
     * Get device status messages
     *
     * @return string
     */
    public function getMessages()
    {
        return [
            'notyet'  => 'Not yet',
            'success' => 'Success',
            'already' => 'Already connected',
            'failed'  => 'Failed to connect',
        ];
    }

    /**
     * Check if device responded with an error
     *
     * @return mixed
     */
    public function hasErrors()
    {
        if (request()->has('res')) {
            $response = request('res');

            if (!in_array($response, ['notyet', 'already', 'success'])) {
                $messages = $this->getMessages();
                $error = $messages[$response] ?? "Response code: $response";

                return $error;
            }
        }

        return false;
    }

    /**
     * @param Guest  $guest
     * @param string $redirectUrl
     *
     * @return array
     */
    public function getFormParameters(Guest $guest, $redirectUrl = '')
    {
        return [
            'method' => 'GET',
            'fields' => [
                'userurl'  => $redirectUrl,
                'username' => $guest->username,
                'response' => '9600bd63483e192d0f612930b3f1e248',
            ],
        ];
    }

    /**
     * Check if device responded with successful connection (includes already connected status)
     *
     * @return bool
     */
    public function isConnected()
    {
        $response = request('res');

        return in_array($response, ['already', 'success']);
    }
}