<?php
namespace App\Services\DeviceHandlers\Vendors;

use App\Models\Guest;
use App\Services\DeviceHandlers\AbstractDeviceVendor;

class FortinetDeviceVendor extends AbstractDeviceVendor
{
    /**
     * Set session variables from device parameters
     */
    public function setSessionVariables()
    {
        session([
            'device_login_page_url' => str_replace('fgtauth', '', request('post')),
            'ap_mac'                => request('apmac'),
            'client_mac'            => request('usermac'),
            'client_ip'             => request()->ip(),
            'ssid_name'             => request('ssid'),
            'forti_magic'           => request('magic')
        ]);
    }

    /**
     * Check if client mac address is set in parameters
     *
     * @return mixed
     */
    public function hasClientMac()
    {
        return request()->has('usermac');
    }

    /**
     * Get device status messages
     *
     * @return string
     */
    public function getMessages()
    {
        return [
            'success' => 'Success',
            'failed'  => 'Failed to connect',
        ];
    }

    /**
     * Check if device responded with an error
     *
     * @return mixed
     */
    public function hasErrors()
    {
        if (request('auth') == 'failed') {
            return 'Failed to connect';
        }

        return false;
    }

    /**
     * @param Guest  $guest
     * @param string $redirectUrl
     *
     * @return array
     */
    public function getFormParameters(Guest $guest, $redirectUrl = '')
    {
        return [
            'method' => 'POST',
            'fields' => [
                'magic'    => session('forti_magic'),
                'username' => $guest->username,
                'password' => $guest->password,

            ],
        ];
    }

    /**
     * Check if device responded with successful connection (includes already connected status)
     *
     * @return bool
     */
    public function isConnected()
    {
        return request('auth') == 'success';
    }
}