<?php

namespace App\Services\DeviceHandlers;


class Device
{
    /**
     * The Driver Interface instance.
     *
     * @var DeviceVendorInterface
     */
    protected $driver;

    /**
     * Device constructor.
     *
     * @param string $driver
     */
    public function __construct($driver = null)
    {
        if ($driver) {
            $this->driver = $this->driver($driver);
        }

        return $this->driver;
    }

    /**
     * @param $driver
     *
     * @return DeviceVendorInterface
     */
    public function driver($driver)
    {
        $manager = new DriverManager(app());
        $this->driver = $manager->driver($driver);

        return $this->driver;
    }
}