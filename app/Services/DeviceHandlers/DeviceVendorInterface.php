<?php

namespace App\Services\DeviceHandlers;


interface DeviceVendorInterface
{
    /**
     * Set session variables from device parameters
     */
    public function setSessionVariables();

    /**
     * Get device status messages
     *
     * @return string
     */
    public function getMessages();

    /**
     * Check if device responded with successful connection (includes already connected status)
     *
     * @return bool
     */
    public function isConnected();

    /**
     * Check if device responded with an error
     *
     * @return mixed
     */
    public function hasErrors();

    /**
     * @param \App\Models\Guest $guest
     * @param string            $redirectUrl
     * @return array
     */
    public function getFormParameters(\App\Models\Guest $guest, $redirectUrl = '');

    /**
     * Check if client mac address is set in parameters
     *
     * @return mixed
     */
    public function hasClientMac();
}