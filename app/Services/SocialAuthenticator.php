<?php

namespace App\Services;


use App\Models\Guest;
use App\Models\SplashPage;
use Carbon\Carbon;
use Facebook\Exceptions\FacebookSDKException;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class SocialAuthenticator
{
    /**
     * Handle social login authentication callback
     *
     * @param $provider
     * @param $user
     *
     * @return mixed
     */
    public function handle($provider, $user)
    {
        if (method_exists($this, $provider)) {
            return call_user_func([$this, $provider], $user);
        }

        return false;
    }

    /**
     * Handle facebook authentication callback
     *
     * @param $user
     */
    public function facebook($user)
    {
        session(['guest_login_type' => SplashPage::LOGIN_TYPE_FACEBOOK]);

        $facebook = app(LaravelFacebookSdk::class);

        $facebook->setDefaultAccessToken($user->token);
        // Get basic info on the user from Facebook.

        $linkData = [];

        if (setting('front_page.social.facebook.message')) {
            $linkData['message'] = setting('front_page.social.facebook.message');
        }

        if (setting('front_page.social.facebook.caption')) {
            $linkData['caption'] = setting('front_page.social.facebook.caption');
        }

        if (setting('front_page.social.facebook.place_id')) {
            $linkData['place'] = setting('front_page.social.facebook.place_id');
        }

        if (setting('front_page.social.facebook.link')) {
            $linkData['link'] = setting('front_page.social.facebook.link');
        }

        if ($linkData) {
            try {
                $response = $facebook->post('/me/feed', $linkData);
                log_info('Post to guest Facebook page', ['response' => $response]);
            } catch (FacebookSDKException $e) {
                log_error('Error on guest facebook login', ['message' => $e->getMessage(), 'code' => $e->getCode()]);
            }
        }
    }

    /**
     * @param $provider
     * @param $user
     *
     * @return array|mixed
     */
    public function prepareAttributes($provider, $user)
    {
        if (method_exists($this, "{$provider}Attributes")) {
            return call_user_func([$this, "{$provider}Attributes"], $user);
        }

        return [];
    }

    /**
     * @param $user
     *
     * @return array
     */
    public function facebookAttributes($user)
    {
        $attributes = [
            'birthday' => isset($user->user['birthday']) ? Carbon::createFromFormat('m/d/Y', $user->user['birthday'])->format('Y-m-d') : null,
            'avatar'   => $user->avatar,
            'name'     => $user->name ?: ($user->user['first_name'] . ' ' . $user->user['last_name']),
        ];

        switch ($user->user['gender']) {
        case 'male':
            $attributes['gender'] = Guest::GENDER_MALE;
            break;

        case 'female':
            $attributes['gender'] = Guest::GENDER_FEMALE;
            break;

        default:
            $attributes['gender'] = Guest::GENDER_UNKNOWN;
        }

        return $attributes;
    }

    /**
     * @param $user
     *
     * @return array
     */
    public function twitterAttributes($user)
    {
        $attributes = [
            'avatar' => $user->avatar,
            'name'   => $user->name,
        ];

        return $attributes;
    }

    /**
     * @param $user
     *
     * @return array
     */
    public function instagramAttributes($user)
    {
        $attributes = [
            'avatar' => $user->avatar,
            'name'   => $user->name,
        ];

        return $attributes;
    }

}