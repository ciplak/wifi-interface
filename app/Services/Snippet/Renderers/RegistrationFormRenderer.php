<?php

namespace App\Services\Snippet\Renderers;


use App\Services\Snippet\Snippet;

class RegistrationFormRenderer extends AbstractSnippetRenderer
{
    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return string
     */
    public function render(Snippet $snippet, $content)
    {
        $attributes = $snippet->attrs->only(['id', 'class', 'register-text', 'continue-text', 'login-text', 'mask-phone', 'phone-country-code-text']);

        $htmlAttributes = [
            'id'    => $attributes->get('id', 'registration-form-container'),
            'class' => $attributes->get('class', 'form-container'),
        ];

        $translations = [
            'register' => $attributes->get('register-text', trans('login.register')),
            'continue' => $attributes->get('continue-text', trans('login.continue')),
            'login'    => $attributes->get('login-text', trans('login.login')),
        ];

        $masks = [
            'phone' => $attributes->get('mask-phone', ''),
        ];

        $html = view('splash_page.forms.registration.form', [
            'htmlAttributes' => $htmlAttributes,
            'translations'   => $translations,
            'masks'          => $masks,
            'options'        => [
                'phone-country-code-text' => $attributes->get('phone-country-code-text', false),
            ],
        ])->render();

        return str_replace($snippet->string, $html, $content);
    }
}