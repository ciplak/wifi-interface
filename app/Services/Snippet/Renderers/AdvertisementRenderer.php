<?php

namespace App\Services\Snippet\Renderers;


use App\Repositories\AdvertisementRepository;
use App\Repositories\Criteria\OnlyPublishedAds;
use App\Services\Snippet\Snippet;

class AdvertisementRenderer extends AbstractSnippetRenderer
{
    /**
     * @var AdvertisementRepository
     */
    private $advertisements;

    /**
     * PollRenderer constructor.
     */
    public function __construct()
    {
        $this->advertisements = app(AdvertisementRepository::class);
    }

    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return string
     */
    public function render(Snippet $snippet, $content)
    {
        $attributes = $snippet->attrs->only(['class', 'id', 'ad']);

        $hash = $attributes->get('ad');
        $html = '';

        if ($hash) {

            try {
                $this->advertisements->pushCriteria(new OnlyPublishedAds());

                $advertisement = $this->advertisements->findByHash($hash);

                $htmlAttributes = [
                    'class' => 'advertisement-container ' . $attributes->get('class'),
                    'id' => $attributes->get('id')
                ];

                $html = view('splash_page.snippets.advertisement', ['advertisement' => $advertisement, 'htmlAttributes' => $htmlAttributes]);
            } catch (\Exception $e) {

            }
        }

        return str_replace($snippet->string, $html, $content);
    }
}