<?php

namespace App\Services\Snippet\Renderers;


use App\Repositories\PollRepository;
use App\Services\Snippet\Snippet;

class PollRenderer extends AbstractSnippetRenderer
{
    /**
     * @var PollRepository
     */
    private $polls;

    /**
     * PollRenderer constructor.
     */
    public function __construct()
    {
        $this->polls = app(PollRepository::class);
    }

    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return string
     */
    public function render(Snippet $snippet, $content)
    {
        $attributes = $snippet->attrs->only(['class', 'id']);

        $id = (int)$attributes->get('id');
        $html = '';

        if ($id) {
            $poll = $this->polls->findActiveById($id);

            $html = $poll ? view('splash_page.snippets.poll', ['poll' => $poll]) : '';
        }

        return str_replace($snippet->string, $html, $content);
    }
}