<?php

namespace App\Services\Snippet\Renderers;


use App\Services\Snippet\Snippet;

abstract class AbstractSnippetRenderer
{
    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return string
     */
    abstract public function render(Snippet $snippet, $content);
}