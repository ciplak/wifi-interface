<?php

namespace App\Services\Snippet\Renderers;


use App\Services\Snippet\Snippet;
use Illuminate\Support\HtmlString;

class AccessCodeFormComponentRenderer extends AbstractSnippetRenderer
{
    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return string
     */
    public function render(Snippet $snippet, $content)
    {
        $text = [
            'please_wait'        => trans('login.please_wait'),
            'login'              => trans('login.login'),
            'access_code'        => trans('login.access_code'),
            'resend_access_code' => trans('login.resend_access_code'),
        ];

        $html = session()->has('social_login_requires_code')
            ? new HtmlString("<access-code-form :text='" . json_encode($text) . "'></access-code-form>")
            : '';

        return str_replace($snippet->string, $html, $content);
    }
}