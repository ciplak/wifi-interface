<?php

namespace App\Services\Snippet\Renderers;


use App\Services\Snippet\Snippet;

class SnippetRenderer
{
    /**
     * @var array
     */
    private $renderers = [
        'errormessages'           => 'ErrorMessage',
        'poll'                    => 'Poll',
        'advertisement'           => 'Advertisement',
        'registrationform'        => 'RegistrationForm',
        'loginform'               => 'LoginForm',
        'accesscodeformcomponent' => 'AccessCodeFormComponent',
    ];

    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return mixed
     */
    public function render(Snippet $snippet, $content)
    {
        $name = strtolower($snippet->name);

        if (!isset($this->renderers[$name])) {
            return $content;
        }

        $renderer = $this->factory($name);

        return $renderer->render($snippet, $content);
    }

    /**
     * Create a new renderer instance fot the given snippet
     *
     * @param string $name
     *
     * @return AbstractSnippetRenderer
     */
    private function factory($name)
    {
        $rendererClassName = __NAMESPACE__ . '\\' . $this->renderers[$name] . 'Renderer';

        return new $rendererClassName;
    }
}