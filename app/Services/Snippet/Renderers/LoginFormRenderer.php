<?php

namespace App\Services\Snippet\Renderers;


use App\Services\Snippet\Snippet;

class LoginFormRenderer extends AbstractSnippetRenderer
{
    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return string
     */
    public function render(Snippet $snippet, $content)
    {
        $attributes = $snippet->attrs->only(['id', 'class', 'login-text', 'mask-phone', 'phone-country-code-text']);

        $htmlAttributes = [
            'id'    => $attributes->get('id', 'login-form-container'),
            'class' => $attributes->get('class', 'form-container'),
        ];

        $translations = [
            'login' => $attributes->get('login-text', trans('login.login')),
        ];

        $masks = [
            'phone' => $attributes->get('mask-phone', ''),
        ];

        $html = view('splash_page.forms.login.form', [
            'htmlAttributes' => $htmlAttributes,
            'translations'   => $translations,
            'masks'          => $masks,
            'options'        => [
                'phone-country-code-text' => $attributes->get('phone-country-code-text', false),
            ],
        ])->render();

        return str_replace($snippet->string, $html, $content);
    }
}