<?php

namespace App\Services\Snippet\Renderers;


use App\Services\Snippet\Snippet;
use Illuminate\Support\HtmlString;

class ErrorMessageRenderer extends AbstractSnippetRenderer
{
    /**
     * Render the snippet
     *
     * @param Snippet $snippet
     * @param string  $content
     *
     * @return string
     */
    public function render(Snippet $snippet, $content)
    {
        $attributes = $snippet->attrs->only(['class', 'text']);

        $error = session('error');

        $htmlAttributes = [
            'class' => $attributes->get('class', 'alert alert-danger error-message')
        ];

        $html = $error ? new HtmlString('<div' . \HTML::attributes($htmlAttributes) . '>' . $attributes->get('text', $error) . '</div>') : '';

        return str_replace($snippet->string, $html, $content);
    }
}