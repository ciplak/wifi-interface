<?php

namespace App\Services\Snippet;


use Illuminate\Support\Collection;

class Parser
{
    /**
     * Parse snippets with parameters
     *
     * @param string $string
     *
     * @return Collection|Snippet[]
     */
    public function getSnippets($string)
    {
        // parse snippets with parameters
        $pattern = "/(?P<snippet>(?:\s?\[\[)(?P<name>[\w\-]{3,})(?:\s(?P<attrs>[\w\d,\s=\\\"\'\(\)\-\+\#\%\!\~\`\&\.\s\:\/\?\|]+))?(?:\]\]))/u";
        preg_match_all($pattern, $string, $matches, PREG_SET_ORDER);

        $result = collect($matches)->map(
            function ($item) {
                $attributes = isset($item['attrs']) ? $this->getAttributes($item['attrs']) : [];

                $snippet = new Snippet($item['snippet'], $item['name'], $attributes);

                return $snippet;
            }
        );

        return $result;
    }

    /**
     * Parse attributes
     *
     * @param $attr
     *
     * @return array
     */
    public function getAttributes($attr)
    {
        preg_match_all("/(?<name>\\S+)=[\"']?(?P<value>(?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?/u", $attr, $matches, PREG_SET_ORDER);
        $attributes = [];

        foreach ($matches as $i => $value) {
            $key = $value['name'];
            $attributes[$key] = $value['value'];
        }

        return $attributes;
    }
}