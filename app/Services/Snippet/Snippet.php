<?php

namespace App\Services\Snippet;


use Illuminate\Support\Collection;

class Snippet
{
    /**
     * @var string
     */
    public $string;

    /**
     * @var string
     */
    public $name;

    /**
     * @var Collection
     */
    public $attrs;

    /**
     * Snippet constructor.
     *
     * @param string $string
     * @param string $name
     * @param array  $attrs
     */
    public function __construct($string, $name, array $attrs = [])
    {
        $this->string = $string;
        $this->name = $name;
        $this->attrs = collect($attrs);
    }
}
