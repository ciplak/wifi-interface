<?php

namespace App\Services\RadiusCommunicator;

interface VendorInterface
{
    /**
     * Create a wifi user
     *
     * @param \App\Models\GuestDevice        $guestDevice
     * @param int|\App\Models\ServiceProfile $serviceProfile
     *
     * @return bool
     */
    public function createWiFiUser(\App\Models\GuestDevice $guestDevice, $serviceProfile);

    /**
     * Delete the given wifi user
     *
     * @param string $username
     *
     * @return bool
     */
    public function deleteWiFiUser($username);

    /**
     * Sync one or all devices
     *
     * @param null|int|\App\Models\Device $device
     *
     * @return bool|mixed
     */
    public function updateDevice($device = null);

    /**
     * Sync one or all service profiles
     *
     * @param null|int|\App\Models\ServiceProfile $serviceProfile
     *
     * @return bool|mixed
     */
    public function updateServiceProfile($serviceProfile = null);

    /**
     * Disconnect user
     *
     * @param \App\Models\Guest $guest
     *
     * @return bool|mixed
     */
    public function logoutWifiUser(\App\Models\Guest $guest);
}