<?php

namespace App\Services\RadiusCommunicator;


class RadiusCommunicator
{
    /**
     * The Driver Interface instance.
     *
     * @var VendorInterface
     */
    protected $driver;

    /**
     * Device constructor.
     *
     * @param string $driver

     * @return VendorInterface
     */
    public function __construct($driver = null)
    {
        $this->driver = $this->driver($driver);

        return $this->driver;
    }

    /**
     * @param string $driver
     *
     * @return VendorInterface
     */
    public function driver($driver)
    {
        $manager = new DriverManager(app());

        $this->driver = $manager->driver($driver);

        return $this->driver;
    }
}