<?php

namespace App\Services\RadiusCommunicator\Vendors;


use App\Models\Device;
use App\Models\Nas;

class FreeRadius extends WebService
{
    /**
     * Make a request to create wifi user
     *
     * @param \App\Models\GuestDevice    $guestDevice
     * @param \App\Models\ServiceProfile $serviceProfile
     *
     * @return bool
     */
    public function createWiFiUser(\App\Models\GuestDevice $guestDevice, $serviceProfile)
    {
        $guest = $guestDevice->owner;

        $connection = \DB::connection('radius');

        $this->deleteWiFiUser($guest->username);

        $remainingTimeInSeconds = $guestDevice->remaining_time;

        if ($remainingTimeInSeconds < 0) {
            $remainingTimeInSeconds = $serviceProfile->duration_total_seconds;
        }

        $result = $connection->table('radcheck')->insert([
            ['username' => $guest->username, 'attribute' => 'Password', 'op' => ':=', 'value' => $guest->password],
            ['username' => $guest->username, 'attribute' => 'Auth-Type', 'op' => ':=', 'value' => 'Accept'],
        ]);

        // rad reply

        $radReplyConnection = $connection->table('radreply');

        $radReplyConnection->insert([
            ['username' => $guest->username, 'attribute' => 'Session-Timeout', 'op' => ':=', 'value' => $remainingTimeInSeconds],      // remaining time
            ['username' => $guest->username, 'attribute' => 'Acct-Interim-Interval', 'op' => ':=', 'value' => 300]                     // Meraki Acct-Interim-Interval
        ]);

        // Qos level
        if (!is_null($serviceProfile->qos_level)) {
            $radReplyConnection->insert([
                'username'  => $guest->username,
                'attribute' => 'Airespace-QOS-Level',
                'op'        => ':=',
                'value'     => $serviceProfile->qos_level,
            ]);
        }

        if (!$result) {
            \Log::error('Could not create Radius user', ['username' => $guest->username]);
        }

        return $result;
    }


    /**
     * Sync one or all devices
     *
     * @param null|int|\App\Models\Device $device
     *
     * @return bool|mixed
     */
    public function updateDevice($device = null)
    {
        $devices = $device ? collect([$device]) : Device::all();

        $devices->each(function ($device) {
            /**
             * @var Device $device
             */
            $this->updateSingleDevice($device);
        });

        $response = new \stdClass();
        $response->Success = true;

        return $response;
    }

    /**
     * Sync the given device
     *
     * @param \App\Models\Device $device
     */
    private function updateSingleDevice($device)
    {
        $nas = Nas::updateOrCreate(
            ['nasname' => $device->ip],
            [
                'shortname'   => $device->name,
                'description' => $device->description,
            ]
        );

        if ($device) {
            \Log::info('Nas updated', ['device id' => $device->id, 'nas id' => $nas->id]);
        } else {
            \Log::error('Could not update nas', ['device id' => $device->id]);
        }
    }

    /**
     * Delete the given wifi user
     *
     * @param string $username
     *
     * @return bool
     */
    public function deleteWiFiUser($username)
    {
        $connection = \DB::connection('radius');

        if (!$connection->table('radcheck')->where('username', $username)->delete()) {
            \Log::warning('Could not delete Radius user from rad check table', ['username' => $username]);
        };

        if (!$connection->table('radreply')->where('username', $username)->delete()) {
            \Log::warning('Could not delete Radius user from rad reply table', ['username' => $username]);
        };

        return true;
    }
}