<?php

namespace App\Services\RadiusCommunicator\Vendors;

use App\Models;
use App\Models\AlertMessage;
use App\Models\Guest;
use App\Models\GuestDevice;
use App\Models\ServiceProfile;
use App\Services\RadiusCommunicator\VendorInterface;
use GuzzleHttp\Client;

/**
 * Class RadiusCommunicator
 *
 * @package App
 */
class WebService implements VendorInterface
{
    /**
     * Make a request to create wifi user
     *
     * @param GuestDevice        $guestDevice
     * @param int|ServiceProfile $serviceProfile
     *
     * @return bool
     */
    public function createWiFiUser(GuestDevice $guestDevice, $serviceProfile)
    {
        $url = $this->getBaseUrl() . 'WifiCommunicator/RestService/CreateGuest';
        $guest = $guestDevice->owner;

        $serviceProfileId = $serviceProfile instanceof ServiceProfile ? $serviceProfile->id : (int)$serviceProfile;
        $guestInfo = [
            'ServiceProfileID' => $serviceProfileId,
            'Username'         => $guest->username,
            'Password'         => $guest->password,
        ];

        \Log::info('Make request to create wifi user', ['guest info' => $guestInfo]);
        $response = $this->sendRequest('post', $url, ['guest' => json_encode($guestInfo)]);

        $result = isset($response->Success) && $response->Success;

        return $result;
    }

    /**
     * Get web service base url
     *
     * @return mixed
     */
    private function getBaseUrl()
    {
        return config('main.wifi_communicator_url');
    }

    /**
     * Make a request
     *
     * @param $method
     * @param $url
     * @param array  $formParams
     *
     * @return bool|mixed
     */
    private function sendRequest($method, $url, $formParams = [])
    {
        if (env('DEMO_MODE')) {
            $result = new \stdClass();
            $result->Success = true;

            return $result;
        }

        $result = false;
        try {
            $client = new Client();
            $options = [];

            if ($formParams) {
                $options['form_params'] = $formParams;
            }

            $response = $client->request($method, $url, $options);

            $result = json_decode($response->getBody());
            \Log::info(
                'Request successfully sent to wifi communicator', [
                'client_mac'      => session('client_mac'),
                'url'             => $url,
                'method'          => $method,
                'form parameters' => $formParams,
                'response'        => $result,
                ]
            );

        } catch (\Exception $e) {
            AlertMessage::high(
                'Error on sending request to wifi communicator', [
                'client_mac'      => session('client_mac'),
                'url'             => $url,
                'method'          => $method,
                'form parameters' => $formParams,
                'message'         => $e->getMessage(),
                ]
            );
        }

        return $result;
    }

    /**
     * Sync one or all service profiles
     *
     * @param null|int|\App\Models\ServiceProfile $serviceProfile
     *
     * @return bool|mixed
     */
    public function updateServiceProfile($serviceProfile = null)
    {
        $url = $this->getBaseUrl() . 'WifiCommunicator/RestService/syncProfile';

        if ($serviceProfile) {
            $serviceProfileId = $serviceProfile instanceof Models\ServiceProfile ? $serviceProfile->id : (int)$serviceProfile;

            $url .= '/' . $serviceProfileId;
        }

        \Log::info('Make request to sync service profile', ['service profile' => $serviceProfile]);
        $result = $this->sendRequest('get', $url);

        return $result;
    }

    /**
     * Sync one or all devices
     *
     * @param null|int|\App\Models\Device $device
     *
     * @return bool|mixed
     */
    public function updateDevice($device = null)
    {
        $url = $this->getBaseUrl() . 'WifiCommunicator/RestService/syncDevice';

        if ($device) {
            $deviceId = $device instanceof Models\Device ? $device->id : (int)$device;

            $url .= '/' . $deviceId;
        }

        \Log::info('Make request to sync device', ['device' => $device]);
        $result = $this->sendRequest('get', $url);

        return $result;
    }

    /**
     * Send SMS to given number
     *
     * @param $smsProviderId
     * @param $phoneNo
     * @param $message
     *
     * @return bool|mixed
     */
    public function sendSMS($smsProviderId, $phoneNo, $message, $langCode = 'en')
    {
        $url = $this->getBaseUrl() . 'SmsService/RestService/SendSMS';

        $smsInfo = [
            'ProviderID' => $smsProviderId,
            'To'         => $phoneNo,
            'Text'       => $message,
            'LangCode'   => $langCode,
        ];

        \Log::info('Make request to send SMS', ['sms' => $smsInfo]);

        $result = $this->sendRequest('post', $url, ['guest' => json_encode($smsInfo)]);

        return $result;
    }

    /**
     * Disconnect user
     *
     * @param Guest $guest
     *
     * @return bool|mixed
     */
    public function logoutWifiUser(Guest $guest)
    {
        $url = $this->getBaseUrl() . 'WifiCommunicator/RestService/LogoutGuest/' . $guest->username;

        \Log::info('Make request to logout wifi user', ['guest id' => $guest->id, 'guest username' => $guest->username]);
        $result = $this->sendRequest('get', $url);

        return $result;
    }

    /**
     * Delete the given wifi user
     *
     * @param string $username
     *
     * @return bool
     */
    public function deleteWiFiUser($username)
    {
        // TODO: Implement deleteWiFiUser() method.
        return false;
    }
}