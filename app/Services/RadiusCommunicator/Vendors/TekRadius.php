<?php

namespace App\Services\RadiusCommunicator\Vendors;


class TekRadius extends WebService
{
    /**
     * Make a request to create wifi user
     *
     * @param \App\Models\GuestDevice    $guestDevice
     * @param \App\Models\ServiceProfile $serviceProfile
     *
     * @return bool|mixed
     */
    public function createWiFiUser(\App\Models\GuestDevice $guestDevice, $serviceProfile)
    {
        $guest = $guestDevice->owner;

        $connection = \DB::table('tekradius_users');

        $this->deleteWiFiUser($guest->username);

        $remainingTimeInSeconds = $guestDevice->remaining_time;

        if ($remainingTimeInSeconds < 0) {
            $remainingTimeInSeconds = $serviceProfile->duration_total_seconds;
        }

        $result = $connection->insert([
            ['UserName' => (string)$guest->username, 'Attribute' => 'ietf|1', 'AttrType' => 0, 'Val' => (string)$guest->username],                                            // username
            ['UserName' => (string)$guest->username, 'Attribute' => 'ietf|0', 'AttrType' => 0, 'Val' => (string)($serviceProfile->id)],                                       // group
            ['UserName' => (string)$guest->username, 'Attribute' => 'ietf|27', 'AttrType' => 1, 'Val' => (string)$remainingTimeInSeconds],                                    // remaining time
            ['UserName' => (string)$guest->username, 'Attribute' => 'ietf|85', 'AttrType' => 1, 'Val' => '300'],                                                              // Meraki Acct-Interim-Interval
            ['UserName' => (string)$guest->username, 'Attribute' => 'adsl-forum|135', 'AttrType' => 1, 'Val' => (string)($serviceProfile->max_upload_bandwidth * 1000)],      // Meraki Maximum-Data-Rate-Upstream (convert kbps to bps)
            ['UserName' => (string)$guest->username, 'Attribute' => 'adsl-forum|136', 'AttrType' => 1, 'Val' => (string)($serviceProfile->max_download_bandwidth * 1000)],    // Meraki Maximum-Data-Rate-Downstream (convert kbps to bps)
        ]);

        // Qos level
        if (!is_null($serviceProfile->qos_level)) {
            $result = $connection->insert(
                [
                    'UserName'  => (string)$guest->username,
                    'Attribute' => 'airespace|2',
                    'AttrType'  => 1,
                    'Val'       => (string)$serviceProfile->qos_level,
                ]
            );
        }

        if ($result) {
            \Log::info(
                'Created Radius user', [
                    'username'            => $guest->username,
                    'remaining time'      => $remainingTimeInSeconds,
                    'service profile id ' => $serviceProfile->id,
                    'result'              => $result,
                ]
            );
        } else {
            \Log::error(
                'Could not create Radius user', [
                    'username'            => $guest->username,
                    'remaining time'      => $remainingTimeInSeconds,
                    'service profile id ' => $serviceProfile->id,
                    'result'              => $result,
                ]
            );
        }

        return $result;
    }

    /**
     * Delete the given wifi user
     *
     * @param string $username
     *
     * @return bool
     */
    public function deleteWiFiUser($username)
    {
        $result = \DB::table('tekradius_users')
            ->where('UserName', $username)
            ->delete();

        if ($result) {
            \Log::info('Deleted Radius user', ['username' => $username]);
        } else {
            \Log::warning('Could not delete Radius user', ['username' => $username]);
        }

        return $result;
    }
}