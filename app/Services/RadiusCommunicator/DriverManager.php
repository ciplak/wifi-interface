<?php

namespace App\Services\RadiusCommunicator;


use App\Services\RadiusCommunicator\Vendors\FreeRadius;
use App\Services\RadiusCommunicator\Vendors\TekRadius;
use Illuminate\Support\Manager;

class DriverManager extends Manager
{
    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return 'FreeRadius';
    }

    /**
     * Create an instance of the Aruba driver
     *
     * @return VendorInterface
     */
    protected function createFreeRadiusDriver()
    {
        $driver = new FreeRadius();

        return $driver;
    }

    /**
     * Create an instance of the Aruba controller driver
     *
     * @return VendorInterface
     */
    protected function createTekRadiusDriver()
    {
        $driver = new TekRadius();

        return $driver;
    }
}