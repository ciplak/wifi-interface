<?php

namespace App\Services;


use App\Models\GuestDevice;
use App\Models\SplashPage;
use App\Services\Snippet\Parser;
use App\Services\Snippet\Renderers\SnippetRenderer;
use App\Services\Snippet\Snippet;
use Illuminate\Support\HtmlString;

class SplashPageRenderer
{
    /**
     * @var SplashPage
     */
    private $splashPage;

    /**
     * @var bool
     */
    private $isPreviewMode;

    /**
     * SplashPageRenderer constructor.
     *
     * @param SplashPage $splashPage
     * @param bool       $isPreviewMode
     */
    public function __construct(SplashPage $splashPage, $isPreviewMode = false)
    {
        $this->splashPage = $splashPage;
        $this->isPreviewMode = $isPreviewMode;
    }

    /**
     * Compile splash page custom_html
     *
     * @return string
     */
    public function compile()
    {
        $output = $this->splashPage->custom_html;

        $output = preg_replace('/(<body.*?>)/is', '$1<div id="ipera-wifi-app" v-cloak>', $output, 1);
        $output = preg_replace('/(<\\/body\s*>)/is', '<###end###>$1', $output, 1);
        $output = preg_replace('/(<\\/head\s*>)/is', '<###head###>$1', $output, 1);

        $head = '<meta id="csrf-token" name="csrf-token" content="[[CsrfToken]]">';
        $end = '';

        if ($this->splashPage->type == SplashPage::TYPE_OFFLINE) {
            $end .= '[[AccessCodeFormComponent]]';
        }

        $end .= '</div><!-- /ipera-wifi-app --><script>window.App = { apiRoot: "[[BaseUrl]]" }; </script><script src="[[App:js]]"></script>';

        if ($this->splashPage->custom_js) {
            $end .= '<script>' . Minifier::js($this->splashPage->custom_js) . '</script>';
        }

        $head .= '<link media="all" type="text/css" rel="stylesheet" href="[[App:css]]">';

        if ($this->splashPage->custom_css) {
            $head .= '<style type="text/css">' . Minifier::css($this->splashPage->custom_css) . '</style>';
        }

//        $head .= '<script src="[[BaseUrl]]/assets/dist/app.js"></script>';

        $output = str_replace('<###head###>', $head, $output);
        $output = str_replace('<###end###>', $end, $output);

        $output = str_replace('<html', '<html [[language_attributes]]', $output);

        // comments
        $output = preg_replace('/\[\[--(.*?)--\]\]/s', '<!-- $1 -->', $output);

        $output = strtr($output, [
            '[[AuthLink:Facebook]]'   => '[[BaseUrl]]/auth/facebook',
            '[[AuthLink:Twitter]]'    => '[[BaseUrl]]/auth/twitter',
            '[[AuthLink:Linkedin]]'   => '[[BaseUrl]]/auth/linkedin',
            '[[AuthLink:Instagram]]'  => '[[BaseUrl]]/auth/instagram',
            '[[AuthLink:Google]]'     => '[[BaseUrl]]/auth/google',
            '[[AuthLink:Foursquare]]' => '[[BaseUrl]]/auth/foursquare',

            '[[AuthAttr:Form]]'       => 'data-auth-provider="form"',
            '[[AuthAttr:Facebook]]'   => 'data-auth-provider="facebook"',
            '[[AuthAttr:Twitter]]'    => 'data-auth-provider="twitter"',
            '[[AuthAttr:Instagram]]'  => 'data-auth-provider="instagram"',
            '[[AuthAttr:Google]]'     => 'data-auth-provider="google"',
            '[[AuthAttr:Foursquare]]' => 'data-auth-provider="foursquare"',
            '[[Loading]]'             => '<div id="page-loading-indicator" class="loading-indicator" data-loading-indicator></div>',

            '[[Link:Disconnect]]' => '[[BaseUrl]]/disconnect',
        ]);

        $output = $this->compileSnippets($output);

        return $output;
    }

    /**
     * Compile snippets
     *
     * @param string $output
     *
     * @return string
     */
    private function compileSnippets($output)
    {
        $replaceList = [];

        $snippets = (new Parser())->getSnippets($output);

        $snippets->filter(function($snippet) {
            return in_array($snippet->name, ['Loading', 'Image', 'ImageUrl']);
        })->each(function($snippet) use (&$replaceList) {
            $replaceValue = '';

            /** @var Snippet $snippet */
            switch (strtolower($snippet->name)) {
                case 'loading':
                    $attributes = $snippet->attrs->only(['class', 'id'])->toArray();
                    $replaceValue = '<div id="loading-indicator"' . \HTML::attributes($attributes) . '></div>';
                    break;

                case 'image':
                    $allowedAttributes = $snippet->attrs->only(['class', 'id', 'file']);

                    $file = $allowedAttributes->get('file', '');

                    $imageAttributes = $allowedAttributes->only(['id', 'class'])->toArray();

                    $replaceValue = new HtmlString('<img src="[[BaseAssetUrl]]img/' . $file . '"' . \HTML::attributes($imageAttributes) . '>');
                    break;

                case 'imageurl':
                    $file = $snippet->attrs->get('file', '');
                    $replaceValue = '[[BaseAssetUrl]]img/' . $file;
                    break;
            }

            if ($replaceValue) {
                $replaceList[$snippet->string] = $replaceValue;
            }
        });

        return strtr($output, $replaceList);
    }

    /**
     * Render the splash page
     *
     * @param GuestDevice $currentGuestDevice
     * @param string      $termsAndConditionsText
     *
     * @return string
     */
    public function render(GuestDevice $currentGuestDevice = null, $termsAndConditionsText = '')
    {
        $locale = \App::getLocale();

        $commonSnippets = [
            '[[BaseUrl]]'      => url('/'),
            '[[BaseAssetUrl]]' => asset_url(),
            '[[CsrfToken]]'    => csrf_token(),

            '[[App:css]]' => url('/') . elixir('assets/dist/app.css', ''),
            '[[App:js]]'  => url('/') . elixir('assets/dist/app.js', ''),

            '[[LanguageSelector]]'    => FormRenderer::languageSelector(),
            '[[language_attributes]]' => sprintf('lang="%s" dir="%s"', $locale, isRTL() ? 'rtl' : 'ltr'),
            '[[CurrentLanguage]]'     => config('app.locales.' . $locale),
        ];

        if ($this->splashPage->type == SplashPage::TYPE_ONLINE) {

            $remainingTime = $this->isPreviewMode ? 0
                : (($currentGuestDevice && $currentGuestDevice->is_connected) ? $currentGuestDevice->remaining_time : 0);

            $guestName = ($currentGuestDevice && $currentGuestDevice->owner) ? $currentGuestDevice->owner->name : '';

            $snippets = [
                '[[CountdownTimer]]' => '<countdown :duration="' . $remainingTime . '"></countdown>',
                '[[Guest:Name]]'     => $this->isPreviewMode ? 'Guest Name' : $guestName,
            ];
        } else {
            $snippets = [
                '[[TermsAndConditionsText]]' => $termsAndConditionsText,
            ];
        }

        $snippets = array_merge($commonSnippets, $snippets);
        $output = strtr($this->splashPage->compiled_html, $snippets);

        $output = $this->renderSnippets($output);

        if ($this->isPreviewMode) {
            $previewScript = '<script>window.isPreviewMode = true; window.previewWarningText = "' . trans('splash_page.preview_warning') . '";</script></head>';
            $output = str_replace('</head>', $previewScript, $output);
        }

        return $output;
    }

    /**
     * Render snippets
     *
     * @param string $content
     *
     * @return string
     */
    private function renderSnippets($content)
    {
        $snippets = (new Parser())->getSnippets($content);
        $renderer = new SnippetRenderer();

        $snippets->each(function($snippet) use ($renderer, &$content) {
            $content = $renderer->render($snippet, $content);
        });

        return $content;
    }
}