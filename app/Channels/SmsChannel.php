<?php

namespace App\Channels;

use App\Channels\Messages\SmsMessage;
use App\Events\Sms\SmsSent;
use App\Models\SmsLog;
use App\Repositories\SmsLogRepository;
use App\Services\Sms\Sms;
use Illuminate\Notifications\Notification;

class SmsChannel
{
    /**
     * @var SmsLogRepository
     */
    private $smsLogs;

    /**
     * SmsChannel constructor.
     *
     * @param SmsLogRepository $smsLogs
     */
    public function __construct(SmsLogRepository $smsLogs)
    {
        $this->smsLogs = $smsLogs;
    }

    /**
     * Send the given notification.
     *
     * @param mixed                                  $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$to = $notifiable->routeNotificationFor('sms')) {
            if (!$to = $notifiable->phone) {
                return;
            }
        }

        try {
            $message = $notification->toSms($notifiable);

            $this->sendMessage($message, $to);
        } catch (\Exception $e) {
            \Log::error('Could not send SMS (SMS channel)', [
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Send message
     *
     * @param SmsMessage $message
     * @param string     $to
     *
     * @return bool
     * @throws \Exception
     */
    protected function sendMessage(SmsMessage $message, $to)
    {
        if (!env('SMS_SANDBOX', false)) {
            try {
                $parameters = $message->smsProvider->parameters;
                $parameters['locale'] = \App::getLocale();

                $isSent = (new Sms())
                    ->driver($message->smsProvider->gateway_key)
                    ->send(
                        $to,
                        $message->content,
                        $parameters
                    );
            } catch (\Exception $e) {
                throw new \Exception('Could not send sms notification');
            }
        } else {
            $isSent = true;

            \Log::info('Faking sending SMS message', [
                'to'           => $to,
                'message'      => $message->content,
                'sms provider' => [
                    'name'       => $message->smsProvider->name,
                    'parameters' => $message->smsProvider->parameters,
                ],
            ]);
        }

        if ($isSent) {
            event(
                new SmsSent(
                    $message->smsProvider->tenant_id,
                    $message->smsProvider,
                    $to,
                    SmsLog::TYPE_ACCESS_CODE,
                    $message->content,
                    null,
                    [],
                    '',
                    SmsLog::STATUS_SENT
                )
            );
        }

        return $isSent;
    }
}
