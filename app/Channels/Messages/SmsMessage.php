<?php

namespace App\Channels\Messages;

use App\Models\SmsProvider;

class SmsMessage
{
    /**
     * The message content.
     *
     * @var string
     */
    public $content;

    /**
     * The phone number the message should be sent from.
     *
     * @var string
     */
    public $from;

    /**
     * @var SmsProvider
     */
    public $smsProvider;

    /**
     * Create a new message instance.
     *
     * @param SmsProvider $smsProvider
     * @param string      $content
     */
    public function __construct(SmsProvider $smsProvider, $content = '')
    {
        $this->smsProvider = $smsProvider;
        $this->content = $content;
    }

    /**
     * Set the message content.
     *
     * @param string $content
     *
     * @return $this
     */
    public function content($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Set the phone number the message should be sent from.
     *
     * @param string $from
     *
     * @return $this
     */
    public function from($from)
    {
        $this->from = $from;

        return $this;
    }
}
