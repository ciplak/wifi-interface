<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SmsWarningThreshold extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // TODO Update email contents And implement Notifications classes please.
        $data = [
            'introLines' => [
                'Threshold Information !',
            ],
            'outroLines' => [
                'See You. Code Happy!',
            ],
            'level' => 'default',
            'actionText' => 'Visit Us!',
            'actionUrl' => route('admin.index')
        ];
        
        return $this->view('emails.sms_warning_threshold', $data);
    }
}
