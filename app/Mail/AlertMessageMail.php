<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertMessageMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $alertMessage;

    /**
     * @var bool
     */
    public $includeDetails;

    /**
     * Create a new message instance.
     *
     * @param string $alertMessage
     * @param bool   $includeDetails
     */
    public function __construct(string $alertMessage, bool $includeDetails = true)
    {
        $this->alertMessage = $alertMessage;
        $this->includeDetails = $includeDetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.alert')
            ->from(config('mail.from.address', config('mail.username')), config('mail.from.name'))
            ->subject('IPERA Guest WiFi System - ' . trans('alert_message.title'));
    }
}
