<?php

namespace App\Events\Sms;

use App\Models\SmsProvider;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Queue\SerializesModels;

class SmsSent
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var SmsProvider
     */
    public $smsProvider;

    /**
     * @var int
     */
    public $tenantId;

    /**
     * @var string
     */
    public $to;

    /**
     * @var int
     */
    public $type;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $queueId;

    /**
     * @var string
     */
    public $data;

    /**
     * @var string
     */
    public $response;

    /**
     * @var int
     */
    public $status;

    /**
     * @var int
     */
    public $eventType;

    /**
     * Create a new event instance.
     *
     * @param int         $tenantId
     * @param SmsProvider $smsProvider
     * @param string      $to
     * @param             $type
     * @param string      $message
     * @param             $queueId
     * @param string[]    $data
     * @param string      $response
     * @param int         $status
     */

    public function __construct(int $tenantId, SmsProvider $smsProvider, $to, $type, /*$eventType,*/$message, $queueId, $data, $response, $status)
    {
        $this->smsProvider = $smsProvider;
        $this->tenantId = $tenantId;
        $this->to = $to;
        //$this->eventType = $eventType;
        $this->type = $type;
        $this->message = $message;
        $this->queueId = $queueId;
        $this->data = $data;
        $this->response = $response;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return [];
    }
}
