<?php

namespace App\Events\RadiusServer;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class Updated extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    private $radiusServerId;

    /**
     * Create a new event instance.
     *
     * @param $radiusServerId
     */
    public function __construct($radiusServerId)
    {
        $this->radiusServerId = $radiusServerId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getRadiusServerId()
    {
        return $this->radiusServerId;
    }
}
