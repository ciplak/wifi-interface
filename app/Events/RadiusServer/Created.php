<?php

namespace App\Events\RadiusServer;

use App\Events\Event;
use App\Models\RadiusServer;
use Illuminate\Queue\SerializesModels;

class Created extends Event
{
    use SerializesModels;

    /**
     * @var RadiusServer
     */
    protected $radiusServer;

    /**
     * Create a new event instance.
     *
     * @param RadiusServer $RadiusServer
     */
    public function __construct(RadiusServer $RadiusServer)
    {
        $this->radiusServer = $RadiusServer;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getRadiusServer()
    {
        return $this->radiusServer;
    }
}
