<?php

namespace App\Events\Venue;

use App\Events\Event;
use App\Models\Venue;
use Illuminate\Queue\SerializesModels;

class Updated extends Event
{
    use SerializesModels;

    /**
     * @var Venue
     */
    private $venue;

    /**
     * Create a new event instance.
     *
     * @param Venue $venue
     */
    public function __construct(Venue $venue)
    {
        $this->venue = $venue;
    }

    /**
     * @return Venue
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
