<?php

namespace App\Events\Guest;

use App\Events\Event;
use App\Models\GuestDevice;
use Illuminate\Queue\SerializesModels;

class DeviceCreated extends Event
{
    use SerializesModels;

    public $guestDevice;

    /**
     * Create a new event instance.
     *
     * @param GuestDevice $guestDevice
     */
    public function __construct(GuestDevice $guestDevice)
    {
        $this->guestDevice = $guestDevice;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
