<?php

namespace App\Events\Guest;

use App\Models\Guest;
use App\Models\GuestDevice;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class Registered /* implements ShouldBroadcast /**/
{
    use /*InteractsWithSockets, /**/SerializesModels;

    /**
     * @var Guest
     */
    public $guest;

    /**
     * @var \App\Models\GuestDevice
     */
    public $guestDevice;

    /**
     * Create a new event instance.
     *
     * @param Guest                   $guest
     * @param \App\Models\GuestDevice $guestDevice
     */
    public function __construct(Guest $guest, GuestDevice $guestDevice)
    {
        $this->guest = $guest;
        $this->guestDevice = $guestDevice;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('guest.' . $this->guest->tenant_id);
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        if ($this->guest->name) {
            $displayName = $this->guest->name;
        } else if ($this->guest->email) {
            $displayName = $this->guest->email;
        } else if ($this->guest->phone) {
            $displayName = $this->guest->phone;
        } else {
            $displayName = $this->guest->username;
        }

        return [
            'guest_name' => $displayName
        ];
    }
}
