<?php

namespace App\Events\Guest;

use App\Events\Event;
use App\Models\GuestDevice;
use Illuminate\Queue\SerializesModels;

class DeviceConnected extends Event
{
    use SerializesModels;

    public $guest;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\GuestDevice $guestDevice
     */
    public function __construct(GuestDevice $guestDevice)
    {
        $this->guest = $guestDevice;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
