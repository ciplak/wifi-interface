<?php

namespace App\Events\Guest;

use App\Events\Event;
use App\Models\Guest;
use App\Models\GuestDevice;
use Illuminate\Queue\SerializesModels;

/**
 * Class Connected
 *
 * @package App\Events
 */
class Connected extends Event
{
    use SerializesModels;

    /**
     * @var Guest
     */
    public $guest;

    /**
     * @var \App\Models\GuestDevice
     */
    public $guestDevice;

    /**
     * Create a new event instance.
     *
     * @param Guest                   $guest
     * @param \App\Models\GuestDevice $guestDevice
     */
    public function __construct(Guest $guest, GuestDevice $guestDevice)
    {
        $this->guest = $guest;
        $this->guestDevice = $guestDevice;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
