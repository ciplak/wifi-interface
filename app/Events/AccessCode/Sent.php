<?php

namespace App\Events\AccessCode;

use App\Events\Event;
use App\Models\AccessCode;
use App\Models\Guest;
use Illuminate\Queue\SerializesModels;

class Sent extends Event
{
    use SerializesModels;

    /**
     * @var Guest
     */
    public $guest;

    /**
     * @var AccessCode
     */
    public $accessCode;

    /**
     * @var string
     */
    private $type;

    /**
     * Create a new event instance.
     *
     * @param Guest      $guest
     * @param AccessCode $accessCode
     * @param string     $type
     */
    public function __construct(Guest $guest, AccessCode $accessCode, $type)
    {
        $this->guest = $guest;
        $this->accessCode = $accessCode;
        $this->type = $type;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
