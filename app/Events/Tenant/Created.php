<?php

namespace App\Events\Tenant;

use App\Events\Event;
use App\Models\ServiceProfile;
use App\Models\SplashPage;
use App\Models\Tenant;
use App\Models\User;
use App\Models\Venue;
use App\Models\Zone;
use Illuminate\Queue\SerializesModels;

class Created extends Event
{
    use SerializesModels;

    /**
     * @var Tenant
     */
    protected $tenant;

    /**
     * Create a new event instance.
     *
     * @param Tenant $tenant
     */
    public function __construct(Tenant $tenant)
    {
        // @todo optimize
        $this->tenant = $tenant;

        User::where('id', \Auth::id())->update(['tenant_id' => $tenant->id]);
        \Auth::user()->tenant_id = $tenant->id;

        $data = [
            'tenant_id'     => $tenant->id,
            'duration'      => 15,
            'duration_type' => 1,
            'name'          => 'Default service profile for ' . $tenant->name
        ];

        $serviceProfile = ServiceProfile::create($data);

        $serviceProfile->update(['tenant_id' => $tenant->id]);

        $venue = Venue::create([
            'parent_id'  => $tenant->id,
            'name'       => 'Default venue for ' . $tenant->name,
            'is_default' => 1,
        ]);

        $venue->serviceProfiles()->sync([$serviceProfile->id]);
        $venue->setDefaultServiceProvider($serviceProfile->id);

        Zone::create([
            'parent_id'  => $venue->id,
            'name'       => 'Default Zone for ' . $venue->name,
            'is_default' => 1,
        ]);


        // Login pages
        $loginPageSettings = [
            'registration_fields' => [
                'email' => [
                    'rules'    => ['email'],
                    'enabled'  => 1,
                    'required' => 1,
                ],
            ],

            'login_fields' => [
                'email' => [
                    'rules'    => ['email', 'required'],
                    'enabled'  => 1,
                    'required' => 1,
                ],
            ],
        ];

        // set login page fields
        $loginPage = new SplashPage();

        $fields = [];
        foreach ($loginPage->getRegistrationFields() as $key => $item) {
            $item['enabled'] = (int)($key == 'email');
            $item['required'] = (int)($key == 'email');
            $fields[$key] = $item;
        }

        $settings = [
            'registration_fields' => $fields,
        ];

        $fields = [];
        foreach ($loginPage->getLoginFields() as $key => $item) {
            $item['enabled'] = (int)($key == 'email');
            $item['required'] = (int)($key == 'email');
            $fields[$key] = $item;
        }
        $settings['login_fields'] = $fields;

        $offlinePage = SplashPage::create(
            [
                'name'     => 'Default Pre-Login page for ' . $tenant->name,
                'type'     => SplashPage::TYPE_OFFLINE,
                'settings' => $settings,
            ]
        );

        $onlinePage = SplashPage::create(
            [
                'name'     => 'Default Post-Login page for ' . $tenant->name,
                'type'     => SplashPage::TYPE_ONLINE,
                'settings' => $settings,
            ]
        );

        $offlinePage->update(['tenant_id' => $tenant->id]);
        $onlinePage->update(['tenant_id' => $tenant->id]);

        $tenant->update(
            [
                'login_page_id'      => $offlinePage->id,
                'post_login_page_id' => $onlinePage->id,
            ]
        );
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }
}
