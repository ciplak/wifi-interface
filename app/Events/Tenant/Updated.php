<?php

namespace App\Events\Tenant;

use App\Events\Event;
use App\Models\Tenant;
use Illuminate\Queue\SerializesModels;

class Updated extends Event
{
    use SerializesModels;

    /**
     * @var Tenant
     */
    protected $tenant;

    /**
     * Create a new event instance.
     *
     * @param Tenant $tenant
     */
    public function __construct(Tenant $tenant)
    {
        $this->tenant = $tenant;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
     * @return Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }
}
