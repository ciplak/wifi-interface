<?php

namespace App\Transformers\Api;

use App\Models\Guest;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class GuestTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param Guest $guestModel
     *
     * @return array
     */
    public function transform(Guest $guestModel)
    {
        /**
         * @var Guest $model
         */
        $model = AutoPresenter::decorate($guestModel);

        return [
            'id'         => $model->getRouteKey(),
            'name'       => $model->name,
            'username'   => $model->username ?? '',
            'email'      => $model->email ?? '',
            'phone'      => $model->phone ?? '',
            'created_at' => $model->created_at,
        ];
    }
}