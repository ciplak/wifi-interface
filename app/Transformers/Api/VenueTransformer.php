<?php

namespace App\Transformers\Api;

use App\Models\Venue;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class VenueTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param Venue $venueModel
     *
     * @return array
     */
    public function transform(Venue $venueModel)
    {
        /**
         * @var Venue $model
         */
        $model = AutoPresenter::decorate($venueModel);

        return [
            'id'                 => $model->getRouteKey(),
            'name'               => $model->name,
            'description'        => $model->description ?? '',
            'login_page_id'      => $model->login_page_id ?? '',
            'post_login_page_id' => $model->post_login_page_id ?? '',
            'created_at'         => $model->created_at,
        ];
    }
}