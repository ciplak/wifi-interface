<?php

namespace App\Transformers\Api;

use App\Models\GuestDevice;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class GuestDeviceTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param GuestDevice $guestDeviceModel
     *
     * @return array
     */
    public function transform(GuestDevice $guestDeviceModel)
    {
        /**
         * @var GuestDevice $model
         */
        $model = AutoPresenter::decorate($guestDeviceModel);

        return [
            'id' => $model->getRouteKey(),
        ];
    }
}