<?php

namespace App\Transformers\Api;

use App\Models\Device;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class DeviceTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param Device $deviceModel
     *
     * @return array
     */
    public function transform(Device $deviceModel)
    {
        /**
         * @var Device $model
         */
        $model = AutoPresenter::decorate($deviceModel);

        return [
            'id'              => $model->getRouteKey(),
            'name'            => $model->name,
            'description'     => $model->description,
            'device_model_id' => $model->device_model_id ?? '',
            'mac'             => $model->mac ?? '',
            'ip'              => $model->ip ?? '',
            'created_at'      => $model->created_at,
        ];
    }
}