<?php

namespace App\Transformers;

use App\Models\WifiSession;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class GuestSessionsReportTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param WifiSession $wifiSessionModel
     *
     * @return array
     */
    public function transform(WifiSession $wifiSessionModel)
    {
        /**
         * @var WifiSession $model
         */
        $model = AutoPresenter::decorate($wifiSessionModel);

        return [
            'name'                           => e($model->name ?? $model->username),
            'username'                       => $model->username,
            'guest_device_mac'               => $model->guest_device_mac ?: ' ',
            'guest_device_ip'                => $model->guest_device_ip ?: ' ',
            'access_code'                    => $model->access_code,
            'total_duration'                 => $model->total_duration,
            'downloaded_data_human_readable' => $model->downloaded_data_human_readable,
            'uploaded_data_human_readable'   => $model->uploaded_data_human_readable,
            'connection_status_value'        => $model->connection_status_value,
            'start_time_formatted'           => $model->start_time_formatted,
        ];
    }
}