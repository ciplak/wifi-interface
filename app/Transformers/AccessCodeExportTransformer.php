<?php

namespace App\Transformers;

use App\Models\AccessCode;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class AccessCodeExportTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param AccessCode $accessCodeModel
     *
     * @return array
     */
    public function transform(AccessCode $accessCodeModel)
    {
        /**
         * @var AccessCode $accessCode
         */
        $accessCode = AutoPresenter::decorate($accessCodeModel);

        return [
            'venue'              => $accessCode->venue_name,
            'type'               => $accessCode->type_name,
            'code'               => $accessCode->code,
            'service_profile_id' => $accessCode->service_profile_name,
            'expiry_date'        => $accessCode->expiry_date_formatted,
            'status'             => $accessCode->status_value,
        ];
    }
}