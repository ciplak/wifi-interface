<?php

namespace App\Transformers;

use App\Models\VisitedUrl;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class VisitedUrlLogReportTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param VisitedUrl $visitedUrlLog
     *
     * @return array
     */
    public function transform(VisitedUrl $visitedUrlLog)
    {
        /**
         * @var VisitedUrl $model
         */
        $model = AutoPresenter::decorate($visitedUrlLog);

        return [
            'ap_mac'         => $model->ap_mac,
            'client_mac'     => $model->client_mac,
            'source_ip'      => $model->source_with_port,
            'destination_ip' => $model->destination_with_port,
            'device_type_id' => $model->guest_device_type_name,
            'description'    => $model->description,
            'platform'       => $model->platform,
            'browser'        => $model->browser,
            'url'            => $model->url,
            'visited_at'     => $model->visited_at_formatted,
        ];
    }
}