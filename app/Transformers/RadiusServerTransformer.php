<?php

namespace App\Transformers;


use App\Models\RadiusServer;
use League\Fractal\TransformerAbstract;

class RadiusServerTransformer extends TransformerAbstract
{
    public function transform(RadiusServer $item)
    {
        $types = RadiusServer::getTypeOptions();

        return [
            'id'        => $item->id,
            'type'      => $item->type,
            'type_name' => $types[$item->type],
            'secret'    => $item->secret ? \Crypt::decrypt($item->secret) : '',
            'ip'        => $item->ip,
            'auth_port' => $item->auth_port,
            'acc_port'  => $item->acc_port,
        ];
    }
}