<?php

namespace App\Transformers;


use App\Models\Media;
use League\Fractal\TransformerAbstract;

class MediaTransformer extends TransformerAbstract
{
    public function transform(Media $item)
    {
        return [
            'id'              => hashids_encode($item->id),
            'url'             => $item->getUrl(),
            'file'            => $item['file_name'],
            'size'            => $item['size'],
            'size_for_humans' => human_filesize($item['size']),
            'dimensions'      => $item->hasCustomProperty('dimensions') ? $item->getCustomProperty('dimensions') : '',
            'uploaded_at'     => $item->created_at->format('F j, Y')
        ];
    }
}