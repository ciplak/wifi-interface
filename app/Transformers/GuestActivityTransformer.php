<?php

namespace App\Transformers;

use App\Models\GuestActivity;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class GuestActivityTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param GuestActivity $guestActivityModel
     *
     * @return array
     */
    public function transform(GuestActivity $guestActivityModel)
    {
        /**
         * @var GuestActivity $model
         */
        $model = AutoPresenter::decorate($guestActivityModel);

        return [
            'name'           => $model->name ?: ' ',
            'username'       => $model->username ?: ' ',
            'device_type_id' => $model->guest_device_type_name,
            'description'    => $model->description,
            'platform'       => $model->platform,
            'browser'        => $model->browser,
            'ip'             => $model->ip,
            'mac'            => $model->mac,
            'created_at'     => $model->created_at_formatted,
        ];
    }
}