<?php

namespace App\Transformers;

use App\Models\Guest;
use League\Fractal\TransformerAbstract;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class GuestReportTransformer extends TransformerAbstract
{
    /**
     * Transform the given model
     *
     * @param Guest $guestModel
     *
     * @return array
     */
    public function transform(Guest $guestModel)
    {
        /**
         * @var Guest $model
         */
        $model = AutoPresenter::decorate($guestModel);

        return [
            'name'         => $model->display_name,
            'email'        => $model->email ?: ' ',
            'phone'        => $model->phone ?: ' ',
            'nationality'  => $model->nationality_name,
            'passport_no'  => $model->passport_no,
            'birthday'     => $model->birthday_formatted,
            'gender'       => $model->gender_name,
            'last_visit'   => $model->last_visit_time_formatted,
            'total_visits' => (int)$model->total_visits ?: '0',
        ];
    }
}