<?php

namespace App;

use Carbon\Carbon;

class Date
{
    const RANGE_INTERVAL = -1;

    const RANGE_ALL = null;

    const RANGE_TODAY = 1;

    const RANGE_YESTERDAY = 2;

    const RANGE_THIS_WEEK = 3;

    const RANGE_LAST_WEEK = 4;

    const RANGE_THIS_MONTH = 5;

    const RANGE_LAST_MONTH = 6;

    const RANGE_THIS_YEAR = 7;

    const RANGE_LAST_7_DAYS = 8;

    const RANGE_LAST_30_DAYS = 9;

    const DURATION_TYPE_MINUTES = 1;

    const DURATION_TYPE_HOURS = 2;

    const DURATION_TYPE_DAYS = 3;

    const DURATION_TYPE_WEEKS = 4;

    const DATE_FORMAT_YMD = 1;

    const DATE_FORMAT_DMY = 2;

    const DATE_FORMAT_MDY = 3;

    const TIME_FORMAT_12_HOURS = 1;

    const TIME_FORMAT_24_HOURS = 2;

    /**
     * Get list of date range options for report filters
     *
     * @return array
     */
    public static function getOptions()
    {
        return [
            null                     => trans('messages.date.options.all'),
            static::RANGE_INTERVAL   => trans('messages.date.options.interval'),
            static::RANGE_TODAY      => trans('messages.date.options.today'),
            static::RANGE_YESTERDAY  => trans('messages.date.options.yesterday'),
            static::RANGE_THIS_WEEK  => trans('messages.date.options.this_week'),
            static::RANGE_LAST_WEEK  => trans('messages.date.options.last_week'),
            static::RANGE_THIS_MONTH => trans('messages.date.options.this_month'),
            static::RANGE_LAST_MONTH => trans('messages.date.options.last_month'),
            static::RANGE_THIS_YEAR  => trans('messages.date.options.this_year'),
        ];
    }

    /**
     * Get start date and end date for the given range
     *
     * @param int         $interval
     * @param null|string $start
     * @param null|string $end
     *
     * @return Carbon[]|bool
     */
    public static function getRanges($interval, $start = null, $end = null)
    {
        switch ($interval) {
            case static::RANGE_INTERVAL:
                $startDate = is_null($start) ? request('start_date') : $start;
                $endDate = is_null($end) ? request('end_date') : $end;

                return [
                    Carbon::createFromFormat('Y-m-d', $startDate)->startOfDay(),
                    Carbon::createFromFormat('Y-m-d', $endDate)->endOfDay(),
                ];
                break;

            case static::RANGE_TODAY:
                return [
                    Carbon::today()->startOfDay(),
                    Carbon::tomorrow()->startOfDay(),
                ];
                break;

            case static::RANGE_YESTERDAY:
                return [
                    Carbon::yesterday()->startOfDay(),
                    Carbon::today()->startOfDay(),
                ];
                break;

            case static::RANGE_THIS_WEEK:
                return [
                    Carbon::now()->startOfWeek(),
                    Carbon::now(),
                ];
                break;

            case static::RANGE_LAST_WEEK:
                return [
                    Carbon::now()->subWeek()->startOfWeek(),
                    Carbon::now()->startOfWeek(),
                ];
                break;

            case static::RANGE_THIS_MONTH:
                return [
                    Carbon::now()->startOfMonth(),
                    Carbon::now(),
                ];
                break;

            case static::RANGE_LAST_MONTH:
                return [
                    Carbon::now()->subMonth()->startOfMonth(),
                    Carbon::now()->startOfMonth(),
                ];
                break;

            case static::RANGE_THIS_YEAR:
                return [
                    Carbon::now()->startOfYear(),
                    Carbon::now(),
                ];
                break;

            case static::RANGE_LAST_7_DAYS:
                return [
                    Carbon::now()->subWeek()->startOfDay(),
                    Carbon::now(),
                ];
                break;

            case static::RANGE_LAST_30_DAYS:
                return [
                    Carbon::now()->subMonth()->startOfDay(),
                    Carbon::now(),
                ];
                break;
        }

        return false;
    }

    /**
     * Get date range for week number
     *
     * @param $week
     *
     * @return array
     */
    public static function getWeekDateRange($week)
    {
        $dto = Carbon::now()->firstOfYear()->addWeek($week - 1);

        return [
            $dto->startOfWeek()->toDateString(),
            $dto->endOfWeek()->toDateString(),
        ];
    }

    /**
     * Get day list
     *
     * @return array
     */
    public static function getDays()
    {
        return [
            Carbon::SUNDAY    => trans('messages.days.sunday'),
            Carbon::MONDAY    => trans('messages.days.monday'),
            Carbon::TUESDAY   => trans('messages.days.tuesday'),
            Carbon::WEDNESDAY => trans('messages.days.wednesday'),
            Carbon::THURSDAY  => trans('messages.days.thursday'),
            Carbon::FRIDAY    => trans('messages.days.friday'),
            Carbon::SATURDAY  => trans('messages.days.saturday'),
        ];
    }

    /**
     * Get date format options
     *
     * @return array
     */
    public static function getDateFormatOptions()
    {
        return [
            static::DATE_FORMAT_YMD => 'Y-m-d',
            static::DATE_FORMAT_DMY => 'd-m-Y',
            static::DATE_FORMAT_MDY => 'm-d-Y',
        ];
    }

    /**
     * Get time format options
     *
     * @return array
     */
    public static function getTimeFormatOptions()
    {
        return [
            static::TIME_FORMAT_12_HOURS => 'g:i A',
            static::TIME_FORMAT_24_HOURS => 'H:i',
        ];
    }

    /**
     * @param integer $timestamp
     * @param string  $timezone
     *
     * @return Carbon
     */
    public static function toUTC($timestamp, $timezone)
    {
        $date = new Carbon($timestamp, $timezone);
        $date->setTimezone('UTC');

        return $date;
    }

    /**
     * @param integer $timestamp
     * @param string  $timezone
     *
     * @return Carbon
     */
    public static function fromUTC($timestamp, $timezone)
    {
        $date = new Carbon($timestamp, 'UTC');
        $date->setTimezone($timezone);

        return $date;
    }
}
