<?php

namespace App\Composers;

use App\Repositories\AdvertisementRepository;
use Illuminate\Contracts\View\View;

class AdvertisementComposer
{
    /**
     * @var AdvertisementRepository
     */
    private $advertisements;

    /**
     * AccessCodeFilterComposer constructor.
     *
     * @param AdvertisementRepository $advertisements
     */
    public function __construct(AdvertisementRepository $advertisements)
    {
        $this->advertisements = $advertisements;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(
            [
            'advertisementList' => $this->advertisements->getList()
            ]
        );
    }
}
