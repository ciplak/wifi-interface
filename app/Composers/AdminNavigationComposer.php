<?php

namespace App\Composers;

use App\Models\Tenant;
use Illuminate\Contracts\View\View;

class AdminNavigationComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        if (\Entrust::can('tenants.switch')) {
            $view->with('tenants', Tenant::orderBy('name')->pluck('name', 'id'));
        }
    }
}
