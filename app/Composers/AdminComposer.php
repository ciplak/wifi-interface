<?php

namespace App\Composers;

use App\Repositories\SettingRepository;
use Illuminate\Contracts\View\View;

class AdminComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $settings = app(SettingRepository::class);

        $view->with('maintenanceMode', (int)$settings->get('maintenance_mode'));
    }
}
