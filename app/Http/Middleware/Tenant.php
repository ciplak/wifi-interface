<?php

namespace App\Http\Middleware;

use Closure;

class Tenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Landlord::addTenant('tenant_id', \Auth::check() ? (int)\Auth::user()->tenant_id : 0);

        return $next($request);
    }
}
