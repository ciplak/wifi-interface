<?php

namespace App\Http\Middleware;

use Closure;

class LoadSettings
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // load  settings from DB
        try {

            // seet week start and end days
            $weekStartDay = (int)setting('start_of_week', 0);
            $weekEndDay = $weekStartDay - 1;

            if ($weekEndDay < 0) {
                $weekEndDay = 6;
            }

            \Carbon\Carbon::setWeekStartsAt($weekStartDay);
            \Carbon\Carbon::setWeekEndsAt($weekEndDay);

        } catch (\Exception $e) {
            \Log::error('Error on loading settings from DB', ['message' => $e->getMessage()]);
        }

        $response = $next($request);

        return $response;
    }
}
