<?php

namespace App\Http\Middleware;

use App\Models\GuestDevice;
use Closure;

class RedirectIfConnected
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (GuestDevice::checkIfConnected(session('client_mac'))) {
            \Log::info('Guest device is connected. Redirecting to index', ['from url' => $request->url(), 'client mac' => session('client_mac')]);
            return redirect()->route('login.index');
        }

        return $next($request);
    }

}
