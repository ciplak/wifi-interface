<?php

namespace App\Http\Middleware;

use Closure;

class FrontPageLanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('lang')) {
            $lang = $request->get('lang', '');

            $languages = array_keys(config('app.locales'));

            if (!in_array($lang, $languages)) {
                $lang = config('app.locale');
            }

            session(['front_page_lang' => $lang]);
        }

        $lang = session('front_page_lang', config('app.locale'));
        \App::setLocale($lang);

        return $next($request);
    }
}
