<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class UserProfileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'     => 'required|min:5',
            'email'    => [
                'required',
                'email',
                Rule::unique('users')->ignore(\Auth::id()),
            ],
            'password' => 'min:6|confirmed',
        ];

        return $rules;
    }
}
