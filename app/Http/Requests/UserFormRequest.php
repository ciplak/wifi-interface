<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class UserFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //@todo check with permission
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = hashids_decode($this->route('id'));

        $rules = [
            'name'  => 'required|min:5',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($id),
            ],
            'role'  => 'required',
        ];

        if ($id) {
            $rules['password'] = 'min:6|confirmed';
        } else {
            $rules['password'] = 'required|min:6|confirmed';
        }

        // @todo check with permissions
        if ($this->input('role') == 'venue' || $this->input('role') == 'front-desk') {
            $rules['property_id'] = 'required';
        }

        return $rules;
    }
}
