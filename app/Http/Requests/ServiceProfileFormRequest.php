<?php

namespace App\Http\Requests;

use App\Models\ServiceProfile;
use Illuminate\Validation\Rule;

class ServiceProfileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = hashids_decode($this->route('id'));

        $rules = [
            'duration'          => 'required|numeric|min:1',
            'renewal_frequency' => 'required_if:type,' . ServiceProfile::TYPE_RECURRING,
            'price'             => 'required_if:model,' . ServiceProfile::MODEL_PAID,
            'name'              => [
                'required',
                Rule::unique('service_profiles')->ignore($id)->where('tenant_id', activeTenantId()),
            ],
        ];

        return $rules;
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->sometimes(
            'renewal_frequency', 'numeric|min:1', function ($input) {
                return $input->type == ServiceProfile::TYPE_RECURRING;
            }
        );

        $validator->sometimes(
            'price', 'numeric|min:1', function ($input) {
                return $input->model == ServiceProfile::MODEL_PAID;
            }
        );

        return $validator;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $input = $this->all();

        $input['price'] = str_replace(',', '.', $input['price']);
        $input['price'] = preg_replace('/[^0-9.]/', '', $input['price']);
        if (!trim($input['price'])) {
            $input['price'] = 0;
        }

        $input['renewal_frequency'] = str_replace(',', '.', $input['renewal_frequency']);
        $input['renewal_frequency'] = preg_replace('/[^0-9.]/', '', $input['renewal_frequency']);
        if (!trim($input['renewal_frequency'])) {
            $input['renewal_frequency'] = 0;
        }

        if (!trim($input['expiry_date'])) {
            $input['expiry_date'] = null;
        }

        $this->replace($input);
    }
}
