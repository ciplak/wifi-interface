<?php

namespace App\Http\Requests;

class DeviceModelFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_vendor_id'  => 'required',
            'name'              => 'required',
            'device_type_id'    => 'required',
            'device_handler_id' => 'required',
        ];
    }
}
