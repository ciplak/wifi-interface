<?php

namespace App\Http\Requests;

use App\Models\Property;

class ZoneFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'           => 'required',
            'post_login_url' => 'url',
        ];

        if (\Entrust::can('venues.manage-global')) {
            $rules['venue_id'] = 'required';
        }

        if ($this->input('smart_login') == Property::SMART_LOGIN_ENABLED) {
            $rules['smart_login_frequency'] = 'numeric|min:1|required_if:smart_login,' . Property::SMART_LOGIN_ENABLED;
        }

        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'smart_login_frequency.required_if' => trans('venue.validation.smart_login_frequency.required_if')
        ];
    }
}