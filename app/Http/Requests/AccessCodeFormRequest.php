<?php

namespace App\Http\Requests;

use App\Models\AccessCode;
use Illuminate\Foundation\Http\FormRequest;

class AccessCodeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'service_profile_id' => 'required',
            'type'               => 'required',
            'code'               => 'required_if:type,' . AccessCode::TYPE_SHARED,
        ];

        if ($this->user()->can('access-codes.manage-global')) {
            $rules['venue_id'] = 'required';
        }

        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.required_if' => trans('validation.required'),
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->sometimes(
            'quantity', 'integer|max:1000|min:1', function ($input) {
                return $input->type == AccessCode::TYPE_ACCESS_CODE;
            }
        );

        return $validator;
    }
}
