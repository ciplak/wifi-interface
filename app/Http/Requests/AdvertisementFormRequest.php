<?php

namespace App\Http\Requests;

use App\Models\Advertisement;

class AdvertisementFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'      => 'required',
            'url'        => 'url',
            'body'       => 'required_if:type,' . Advertisement::TYPE_TEXT,
            'start_date' => 'required|date',
            'end_date'   => 'required|date',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'body.required_if' => trans('validation.required', ['attribute' => 'text']),
        ];
    }
}