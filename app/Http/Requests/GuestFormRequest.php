<?php

namespace App\Http\Requests;

class GuestFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'     => 'min:5',
            'email'    => 'email',
            'birthday' => 'date_format:d/m/Y',
        ];

        if ($this->user()->can('guests.manage-global')) {
            $rules['venue_id'] = 'required';
        }

        return $rules;
    }
}
