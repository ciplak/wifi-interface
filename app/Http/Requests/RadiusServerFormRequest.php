<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class RadiusServerFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'type'      => 'required',
            'ip'        => [
                'required',
                'ip',
                Rule::unique('radius_servers')->ignore($id),
            ],
            'secret'    => 'required',
            'auth_port' => 'required|integer|between:1,65535',
            'acc_port'  => 'required|integer|between:1,65535',
        ];
    }
}
