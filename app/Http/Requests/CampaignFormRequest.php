<?php

namespace App\Http\Requests;

use App\Models\Campaign;
use Illuminate\Foundation\Http\FormRequest;

class CampaignFormRequest extends FormRequest
{
    /**
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'               => 'required',
            'parameter_type_date' => 'required_if:parameter_type,' . Campaign::PARAMETER_TYPE_DATE_TIME,
            'reengage'            => 'integer',
            'visit_count'         => 'integer',
        ];

        return $rules;
    }
}
