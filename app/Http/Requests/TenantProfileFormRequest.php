<?php

namespace App\Http\Requests;

use App\Models\Property;
use Illuminate\Validation\Rule;

class TenantProfileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = \Auth::user()->tenant_id;

        $rules = [
            'name'    => [
                'required',
                Rule::unique('properties')->ignore($id)->where('property_type_id', Property::TYPE_TENANT),
            ],
            'address' => 'required',
        ];

        if ($this->input('smart_login') == Property::SMART_LOGIN_ENABLED) {
            $rules['smart_login_frequency'] = 'numeric|min:1|required_if:smart_login,' . Property::SMART_LOGIN_ENABLED;
        }

        return $rules;
    }
}
