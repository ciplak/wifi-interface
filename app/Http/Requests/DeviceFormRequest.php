<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class DeviceFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = hashids_decode($this->route('id'), 0);

        $tenantId = activeTenantId();

        return [
            'property_id'     => 'required',
            'name'            => 'required',
            'mac'             => [
                'required',
                'mac',
                Rule::unique('devices')->ignore($id),
            ],
            'ip'              => [
                'required',
                'ip',
                Rule::unique('devices')->ignore($id)->where('tenant_id', $tenantId),
            ],
            'device_model_id' => 'required',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $input = $this->all();

        if (isset($input['ip'])) {
            $input['ip'] = str_replace('_', '', $input['ip']);
        }

        $this->replace($input);
    }
}
