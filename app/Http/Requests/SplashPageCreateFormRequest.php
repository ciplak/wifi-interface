<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class SplashPageCreateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = hashids_decode($this->route('id'));

        $rules = [
            'name' => [
                'required',
                Rule::unique('login_pages')->ignore($id)->where('tenant_id', activeTenantId()),
            ],
        ];

        return $rules;
    }
}
