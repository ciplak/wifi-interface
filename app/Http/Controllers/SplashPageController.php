<?php

namespace App\Http\Controllers;

use App\ConnectionStatus;
use App\Events\Guest\Registered;
use App\Models\AccessCode;
use App\Models\GuestActivity;
use App\Models\ServiceProfile;
use App\Models\SplashPage;
use App\Repositories\SmsLogRepository;
use App\Services\RadiusCommunicator\RadiusCommunicator;
use App\Services\SplashPageRenderer;
use App\Traits\RedirectsToAccessPoint;
use App\Traits\RegistersGuests;
use App\Traits\SendsAccessCodes;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class SplashPageController
 *
 * @package App\Http\Controllers
 */
class SplashPageController extends FrontController
{
    use RegistersGuests, SendsAccessCodes, RedirectsToAccessPoint;

    /**
     * @var SmsLogRepository
     */
    public $smsLogs;

    /**
     * Show splash page
     *
     * @return \Illuminate\Http\Response|string
     * @throws \Exception
     */
    public function index()
    {
        // reset login type
        session()->forget('guest_login_type');

        if ($this->currentGuestDevice->is_connected) {
            log_info('Guest device is connected. Showing status page');

            if ($this->currentGuestDevice->remaining_time <= 0) {
                log_info('Disconnecting guest device with no remaining time', ['guest device id' => $this->currentGuestDevice->id, 'guest id' => $this->currentGuestDevice->guest_id]);
                $this->currentGuestDevice->setConnectionStatus(ConnectionStatus::DISCONNECTED);
            } else {
                log_info('Showing status page');

                return $this->render();
            }
        }

        // guest did see login page
        GuestActivity::create([
            'tenant_id'       => $this->tenantId,
            'property_id'     => $this->venue->id,
            'guest_id'        => $this->currentGuestDevice->owner ? $this->currentGuestDevice->owner->id : null,
            'guest_device_id' => $this->currentGuestDevice->id,
            'action_id'       => GuestActivity::ACTION_VIEW_LOGIN_PAGE,
            'device_id'       => $this->currentAccessPoint->id,
            'ip'              => request()->ip(),
            'mac'             => session('client_mac'),
        ]);

        return $this->render();
    }

    /**
     * Handle post request for registration form
     *
     * @param Request $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postRegisterForm(Request $request)
    {
        log_info('Validating registration form');

        $fields = $this->splashPage->getEnabledRegistrationFields();

        // build attributes for creating guest
        $attributes = $request->only(array_keys($fields));
        $attributes['venue_id'] = $this->venue->id;
        $attributes['tenant_id'] = $this->tenantId;

        // no field is selected, click through

        if (empty($fields) && $this->currentGuestDevice->guest_id) {
            $guest = $this->currentGuestDevice->owner;
            session(['guest_login_type' => SplashPage::LOGIN_TYPE_CLICK_THROUGH]);

            // @todo handle isolated guests
            /*
            if ($guest->venue_id != $this->venue->id) {
                if (config('front_page_settings.isolate_guests')) {
                    $guest = $this->createGuest($request, $attributes, $fields);
                    event(new Registered($guest, $this->currentGuestDevice));
                }
            }/**/

            $guest->update($attributes);
        } else {
            $this->validateRegistrationForm($request, $fields, $this->splashPage->overwrite_guests);

            $guest = $this->createGuest($request, $attributes, $fields, $this->splashPage->overwrite_guests);
            event(new Registered($guest, $this->currentGuestDevice));
        }

        $this->guestDevices->setOwner($this->currentGuestDevice, $guest);

        session(['guest_login_type' => SplashPage::LOGIN_TYPE_REGISTRATION]);

        // check if pin code enabled
        $defaultServiceProfile = $this->location->getDefaultServiceProfile();
        if ($defaultServiceProfile->service_pin) {

            $accessCode = $this->accessCodes->getFirstAvailableForGuest($guest);

            if (!$accessCode) {
                $accessCode = $this->accessCodes->generateSingleCode(
                    [
                        'service_profile_id' => $this->location->getDefaultServiceProfile()->id,
                        'guest_id'           => $guest->id,
                        'tenant_id'          => $this->tenantId,
                        'venue_id'           => $this->venue->id,
                        'expiry_date'        => Carbon::now()->addDay(7),
                    ],
                    config('front_page_settings.access_code.length', 8),
                    config('front_page_settings.access_code.chars', AccessCode::CHARS_ALPHANUMERIC)
                );
            }

            $sendAccessCode = ($this->splashPage->send_sms && $guest->phone) || ($this->splashPage->send_email && $guest->email);
            $messages = '';

            if ($sendAccessCode) {
                $result = $this->sendAccessCode($accessCode, $guest);
                $messages = $result['message'];
            }

            return response()->json(['require_code' => 1, 'resend_enabled' => $sendAccessCode, 'message' => $messages]);
        }

        log_info('Registration form validated. Redirecting to access point / WLC login page after registration');

        if ($this->createWiFiUser($this->currentGuestDevice, $this->location->getDefaultServiceProfile())) {
            return $this->redirectToDeviceLoginPage($guest, $this->currentAccessPoint);
        } else {
            return response()->json(['error_message' => trans('login.error_occurred')]);
        }
    }

    /**
     * @param Request $request
     *
     * @return SplashPageController
     */
    public function postRegisterAccessCodeForm(Request $request)
    {
        log_info('Validating login form after registration');

        $this->validateAccessCodeForm($request);

        $attributes = ['id' => $this->currentGuestDevice->guest_id];

        return $this->loginGuest($request, $attributes);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postSocialAccessCodeForm(Request $request)
    {
        log_info('Validating login form after social registration');

        $this->validateAccessCodeForm($request);

        $attributes = ['id' => $this->currentGuestDevice->guest_id];

        return $this->loginGuest($request, $attributes);
    }

    /**
     * Handle post request for login form
     *
     * @param Request $request
     *
     * @return $this|\Illuminate\Http\Response
     */
    public function postLoginForm(Request $request)
    {
        log_info('Validating login form');

        $fields = $this->splashPage->getEnabledLoginFields();
        $this->validateLoginForm($request, $fields);

        $attributes = $request->only(array_keys($fields));

        return $this->loginGuest($request, $attributes);
    }

    /**
     * Render login page
     *
     * @return string
     */
    public function render()
    {
        return (new SplashPageRenderer($this->splashPage))
            ->render(
                $this->currentGuestDevice,
                $this->location->getTermsAndConditionsText()
            );
    }

    /**
     * Disconnect guest
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function disconnectGuest()
    {
        // disconnect only if the current guest device is connected
        if ($this->currentGuestDevice->connection_status == ConnectionStatus::CONNECTED) {

            $guest = $this->currentGuestDevice->owner;
            $status = false;

            \Log::info('Disconnecting guest from post-login page');

            try {
                $logoutStatus = (new RadiusCommunicator)
                    ->driver(config('main.radius_server'))
                    ->logoutWifiUser($guest);

                if ($logoutStatus) {
                    $this->guestDevices->update(['connection_status' => ConnectionStatus::DISCONNECTED], $this->currentGuestDevice->id);
                    $this->guests->update(['connection_status' => ConnectionStatus::DISCONNECTED], $guest->id);
                    $status = true;

                    log_info(
                        'Disconnected guest from post-login page', [
                            'guest id' => $guest->id,
                        ]
                    );
                }
            } catch (\Exception $e) {
                log_error(
                    'Error on disconnecting disconnecting guest in post-login page', [
                        'guest id' => $guest->id,
                    ]
                );
            }

            if (!$status) {
                session()->flash('error', trans('login.error_occurred'));
            }
        }

        return redirect('/');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                                                                        //
    // not used methods                                                                                                                       //
    //                                                                                                                                        //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Handle specified service profile
     *
     * @param \App\Models\ServiceProfile $serviceProfile
     *
     * @return \Illuminate\Http\Response
     */
    private function handleServiceProfile(ServiceProfile $serviceProfile)
    {

        if ($serviceProfile->service_pin == ServiceProfile::SERVICE_PIN_DISABLED) {

            $guest = $this->currentGuestDevice->owner;
            $this->guestDevices->setOwner($this->currentGuestDevice, $guest);

            // check service profile type
            if (!$guest->canConnectWithServiceProfile($serviceProfile)) {

                $errorMessage = $serviceProfile->type == ServiceProfile::TYPE_ONE_TIME
                    ? trans('login.service_profile.type.one_time_error')
                    : trans('login.service_profile.type_recurring_error', ['frequency' => $serviceProfile->calculateRenewalFrequencyHours()]);

                return redirect()->route('login.index')->with('error', $errorMessage);
            }

            if ($this->createWiFiUser($this->currentGuestDevice, $serviceProfile, session('guest_login_type'))) {
                return $this->redirectToDeviceLoginPage($guest, $this->currentAccessPoint);
            }
        } else {
            return redirect()->route('login.accessCode')->with('serviceProfileId', $serviceProfile->id);
        }

        return redirect('/');
    }
}