<?php

namespace App\Http\Controllers;

use App\Repositories\SplashPageRepository;
use App\Services\SplashPageRenderer;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class PreviewController
 *
 * @package App\Http\Controllers
 */
class PreviewController extends BaseController
{
    /**
     * @var SplashPageRepository
     */
    private $splashPages;

    /**
     * LoginPageController constructor.
     *
     * @param SplashPageRepository $splashPages
     */
    function __construct(SplashPageRepository $splashPages)
    {
        $this->splashPages = $splashPages;
    }

    /**
     * Preview login page
     *
     * @param string $value
     * @param int    $inside
     *
     * @return \Illuminate\Http\Response|string
     */
    public function preview($value = null, $inside = 0)
    {
        if (!$value) {
            return redirect()->route('login.index');
        }

        /**
         * @var \App\Models\SplashPage $splashPage
         */
        $splashPage = $this->splashPages->findByHash($value);

        view()->share(['splashPage' => $splashPage]);

        load_settings($splashPage->tenant_id);

        session(['front_page_tenant_id' => $splashPage->tenant_id]);

        if (!$inside) {
            return view('layouts.preview');
        }

        if (!$splashPage->exists) {
            return redirect()->route('login.index');
        }

        $venue = $splashPage->tenant->defaultVenue;
        /**
         * @var \App\Models\Zone $zone
         */
        $zone = $venue->defaultZone ?: $venue->zones()->first();

        return (new SplashPageRenderer($splashPage, true))
            ->render(
                null,
                $zone ? $zone->getTermsAndConditionsText() : ''
            );
    }

    /**
     * Exit preview mode
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exitPreview()
    {
        return redirect()->route('login.index');
    }
}