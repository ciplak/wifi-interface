<?php

namespace App\Http\Controllers;

use App\Exceptions\DeviceNotFoundException;
use App\Models\AlertMessage;
use App\Models\SplashPage;
use App\Repositories\DeviceRepository;
use App\Repositories\GuestDeviceRepository;
use App\Repositories\GuestRepository;
use App\Services\DeviceHandlers\Device;
use App\Services\MacEstimator;
use App\Traits\RedirectsToAccessPoint;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class AccessController
 *
 * @package App\Http\Controllers
 */
class AccessController extends BaseController
{
    use RedirectsToAccessPoint;

    /**
     * @var DeviceRepository
     */
    private $devices;

    /**
     * @var GuestDeviceRepository
     */
    private $guestDevices;

    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * LoginPageController constructor.
     *
     * @param DeviceRepository      $devices
     * @param GuestDeviceRepository $guestDevices
     * @param GuestRepository       $guests
     */
    public function __construct(DeviceRepository $devices, GuestDeviceRepository $guestDevices, GuestRepository $guests)
    {
        $this->devices = $devices;
        $this->guestDevices = $guestDevices;
        $this->guests = $guests;
    }

    /**
     * Handle request sent by Access point
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws DeviceNotFoundException
     * @throws \Exception
     */
    public function access(Request $request)
    {
        log_info('Access point / WLC sent guest to access page');

        if ($mac = (new MacEstimator())->getMac($request)) {
            session(['ap_mac' => $mac]);
        }

        $currentAccessPoint = $this->devices->getCurrentDeviceByMac(session('ap_mac'));

        $location = $currentAccessPoint->property;

        $vendor = $currentAccessPoint->getDeviceHandlerClassName();

        $deviceDriver = (new Device)->driver($vendor);

        session(['vendor' => $vendor]);

        if ($deviceDriver->hasClientMac()) {
            log_info('Setting up variables for device vendor', ['vendor' => $vendor]);
            $deviceDriver->setSessionVariables();
        }

        $currentGuestDevice = $this->guestDevices->findOrCreateByMac(session('client_mac'));

        log_info('Access point sent guest to login page');

        if ($request->input('res') == 'success' || $deviceDriver->isConnected()) {
            log_info('Access point response: connected');

            $currentGuestDevice->setConnectionStatus(\App\ConnectionStatus::CONNECTED);
            if ($location->post_login_url_value) {
                log_info('Redirecting to post login URL', ['post login url' => $location->post_login_url_value]);

                return redirect($location->post_login_url_value);
            }
        } else {
            log_info('Access point response: not connected');

            if ($error = $deviceDriver->hasErrors()) {
                session()->flash('error', trans('login.error_occurred'));

                log_error('Access point error (1)', ['error message' => $error]);

                AlertMessage::high('Access point error (2)', [
                    'client mac' => session('client_mac'),
                    'error'      => $error,
                    'parameters' => $request->all(),
                ]);
            } else {
                log_info('Access point has no errors');

                $guest = $currentGuestDevice->owner;

                // if the guest is coming from the current tenant
                if ($guest && ($guest->tenant_id == $currentAccessPoint->tenant_id)) {

                    if ($currentGuestDevice->isReturningWithRemainingTime()) {
                        log_info('Returning guest with remaining time. Making direct reconnect.', ['remaining time' => $currentGuestDevice->remaining_time]);

                        if ($this->createWiFiUser($currentGuestDevice)) {
                            return $this->redirectToDeviceLoginPage($guest, $currentAccessPoint, false);
                        }
                    }

                    // check if direct login is enabled for the current guest
                    $isSmartLoginEnabled = $location->is_smart_login_enabled;

                    if (($isSmartLoginEnabled && $location->canSmartLogin($guest, $currentAccessPoint->tenant_id)) || $currentGuestDevice->canDirectLogin()) {

                        log_info('Direct login for guest', ['guest id' => $guest->id]);

                        $loginType = $isSmartLoginEnabled ? SplashPage::LOGIN_TYPE_SMART_LOGIN : SplashPage::LOGIN_TYPE_DIRECT_LOGIN;

                        if ($this->createWiFiUser($currentGuestDevice, $location->getDefaultServiceProfile(), $loginType)) {

                            // set smart login time
                            if ($loginType == SplashPage::LOGIN_TYPE_SMART_LOGIN) {
                                $this->guests->update(['smart_login_time' => Carbon::now()], $guest->id);
                            }

                            return $this->redirectToDeviceLoginPage($guest, $currentAccessPoint, false);
                        }
                    }
                }

            }
        }

        return redirect()->route('login.index');
    }
}
