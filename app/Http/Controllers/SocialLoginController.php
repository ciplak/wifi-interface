<?php

namespace App\Http\Controllers;

use App\Models\ServiceProfile;
use App\Services\SocialAuthenticator;
use App\Traits\RedirectsToAccessPoint;

class SocialLoginController extends FrontController
{
    use RedirectsToAccessPoint;

    /**
     * Redirect the user to the social login provider authentication page
     *
     * @param $provider
     *
     * @return \Response
     */
    public function redirectToAuthProvider($provider)
    {
        $socialite = \Socialite::driver($provider);

        $scopes = config('services.' . $provider . '.scopes');
        if ($scopes) {
            $socialite->scopes($scopes);
        }

        return $socialite->redirect();
    }

    /**
     * Obtain the user information from the social login provider
     *
     * @param string $provider
     *
     * @return \Response
     */
    public function handleAuthProviderCallback($provider)
    {
        try {
            $driver = \Socialite::driver($provider);

            if ($provider == 'facebook') {
                $driver->fields(
                    [
                    'first_name',
                    'last_name',
                    'email',
                    'gender',
                    'birthday',
                    'age_range',
                    ]
                );
            }

            $user = $driver->user();
        } catch (\Exception $e) {
            // user denied
            log_error('Error or social login', ['provider' => $provider, 'messages' => $e->getMessage()]);

            return redirect()->route('login.index');
        }

        $authenticator = new SocialAuthenticator();
        $attributes = $authenticator->prepareAttributes($provider, $user);
        $attributes["{$provider}_id"] = $user->id;

        $authenticator->handle($provider, $user);

        $searchAttributes = [
            'tenant_id'      => $this->tenantId,
            'venue_id'       => $this->venue->id,
        ];

        if ($user->email) {
            $attributes['email'] = $user->email;
            $searchAttributes['email'] = $user->email;
        } else {
            $searchAttributes["{$provider}_id"] = $user->id;
        }

        /**
 * @var \App\Models\Guest $guest 
*/
        $guest = $this->guests->findByAttributes($searchAttributes);

        $attributes['tenant_id'] = $this->tenantId;
        $attributes['venue_id'] = $this->venue->id;

        if ($guest) {
            $this->guests->update($attributes, $guest->id);
        } else {
            $guest = $this->guests->create($attributes);
        }

        $this->guestDevices->setOwner($this->currentGuestDevice, $guest);

        log_info('Redirecting to service profiles page after social login');

        $serviceProfile = $this->location->getDefaultServiceProfile();

        if ($guest->canConnectWithServiceProfile($serviceProfile)) {

            if ($serviceProfile->service_pin && $this->splashPage->social_login_requires_code) {
                session()->flash('social_login_requires_code', true);
            } else {
                if ($this->createWiFiUser($this->currentGuestDevice, $serviceProfile)) {
                    return $this->redirectToDeviceLoginPage($guest, $this->currentAccessPoint, false);
                }
            }

        } else {
            $message = $serviceProfile->type == ServiceProfile::TYPE_ONE_TIME
                ? trans('login.service_profile.type.one_time_error')
                : trans(
                    'login.service_profile.type_recurring_error', [
                    'frequency' => $serviceProfile->calculateRenewalFrequencyHours(),
                    ]
                );

            session()->flash('error', $message);
        }

        return redirect()->route('login.index');
    }
}