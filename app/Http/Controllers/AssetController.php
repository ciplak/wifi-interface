<?php

namespace App\Http\Controllers;

class AssetController extends Controller
{
    public function asset()
    {
        $path = request('path');

        $contents = \Storage::read($path);

        $headers = [
            'Content-Type' => \Storage::getMimetype($path),
            'Content-Length' => \Storage::getSize($path),
            'Cache-Control' => 'public',
            'Pragma' => 'public',
            'Sender' => 'IPera'
        ];

        $response = \Response::make($contents, 200, $headers);
        return $response;
    }
}