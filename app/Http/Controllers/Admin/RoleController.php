<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends AdminController
{

    /**
     * Show all role permissions
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('admin.role.index');
    }

    /**
     * Show permissions page
     *
     * @return \Illuminate\Http\Response
     */
    public function permissions()
    {
        return $this->render('admin.role.permissions', [
            'roles'       => Role::get(),
            'permissions' => Permission::orderBy('name')->get(),
        ]);
    }

    /**
     * Update role permissions
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePermissions(Request $request)
    {
        $roles = $request->input('roles');

        foreach ($roles as $roleName => $permissions) {
            $role = Role::where('name', $roleName)->first();

            if (!$role) {
                continue;
            }

            $role->perms()->sync($permissions);

        }

        session()->flash('success', trans('permission.saved'));

        return redirect()->back();
    }

}
