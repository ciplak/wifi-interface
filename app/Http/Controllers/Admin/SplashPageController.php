<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SplashPageCreateFormRequest;
use App\Http\Requests\SplashPageUpdateFormRequest;
use App\Models\SplashPage;
use App\Repositories\SplashPageRepository;
use App\Repositories\VenueRepository;
use App\Services\Scripts;
use Illuminate\Http\Request;

/**
 * Class SplashPageController
 */
class SplashPageController extends AdminController
{
    /**
     * @var SplashPageRepository
     */
    private $splashPages;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * SplashPageController constructor.
     *
     * @param SplashPageRepository $splashPages
     * @param VenueRepository      $venues
     */
    public function __construct(SplashPageRepository $splashPages, VenueRepository $venues)
    {
        $this->splashPages = $splashPages;
        $this->venues = $venues;
    }

    /**
     * Display a listing of login pages
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share('locations', $this->venues->getList());

        return $this->render('admin.splash_page.index', [
            'models' => $this->splashPages->getAllPaginated(),
        ]);
    }

    /**
     * Show the login page add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->splashPages->makeNew());
    }

    /**
     * Show login page form
     *
     * @param SplashPage $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(SplashPage $model)
    {
        if ($model->is_global && !$model->canManageGlobal()) {
            abort(403);
        }

        view()->share([
            'model'        => $model,
            'translations' => $model->getTranslationList(),
            'locations'    => $this->venues->getList(),
            'typeOptions'  => $this->splashPages->getTypeOptions(),
            'locales'      => getLocales(),
            'model_id'     => $model ? $model->getRouteKey() : '',
        ]);

        Scripts::view('admin.splash_page.scripts');

        return $this->render('admin.splash_page.form');
    }

    /**
     * Show the edit page for the given login page
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->form($this->splashPages->findByHash($id));
    }

    /**
     * Create or update a login page
     *
     * @param SplashPageCreateFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function post(SplashPageCreateFormRequest $request)
    {
        $model = $this->splashPages->saveByHash($request->all());

        if ($model) {
            session()->flash('success', trans('splash_page.saved'));

            return redirect()->route('admin.splash_page.edit', [$model]);
        } else {
            session()->flash('error', trans('splash_page.not_saved'));
        }
    }

    /**
     * Update the given login page
     *
     * @param SplashPageUpdateFormRequest $request
     * @param string                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SplashPageUpdateFormRequest $request, $id)
    {
        $model = $this->splashPages->saveByHash($request->all(), $id);

        return response()->json(['status' => $model ? true : false]);
    }

    /**
     * Delete the given login page
     *
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        try {
            $result = $this->splashPages->deleteByHash($id);

            if ($result) {
                session()->flash('success', trans('splash_page.deleted'));
            } else {
                session()->flash('error', trans('splash_page.in_use'));
            }

            return redirect()->route('admin.splash_page.index');
        } catch (\Exception $e) {
            session()->flash('error', trans('splash_page.not_deleted'));
        }

        return redirect()->back();
    }

    /**
     * Create a copy of the given login page
     *
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createClone($id)
    {
        $model = $this->splashPages->replicateByHash($id);

        return $this->form($model);
    }

    /**
     * Publish the login page to given locations
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(Request $request)
    {
        $locationId = hashids_decode($request->input('publish_location'));

        $loginPage = $this->splashPages->findByHash($request->input('splash_page'), ['id', 'type']);

        $status = $this->venues->setLoginPage($loginPage, $locationId);

        return response()->json(['status' => $status ? 1 : 0]);
    }

    /**
     * Create a translation for the specified login page
     *
     * @param Request $request
     * @param string  $id
     * @param string  $locale
     *
     * @return mixed
     */
    public function createTranslation(Request $request, $id, $locale)
    {
        /** @var SplashPage $loginPage */
        $loginPage = $this->splashPages->findByHash($id);

        /** @var SplashPage $translation */
        $translation = $loginPage->replicate();

        $translation->parent_id = $loginPage->parent_id ?? $loginPage->id;
        $translation->locale = $locale;
        $translation->name .= ' - Translation for ' . trans('locale.options.' . $locale);

        $translation->save();

        return redirect()->route('admin.splash_page.edit', $translation);
    }
}