<?php
namespace App\Http\Controllers\Admin;

class MediaController extends AdminController
{
    /**
     * Show the media manager
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('admin.media.index');
    }

}