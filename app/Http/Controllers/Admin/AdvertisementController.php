<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdvertisementFormRequest;
use App\Models\Advertisement;
use App\Repositories\AdvertisementRepository;
use App\Services\Scripts;

/**
 * Class AdvertisementController
 */
class AdvertisementController extends AdminController
{
    /**
     * @var AdvertisementRepository
     */
    private $advertisements;

    /**
     * AdvertisementController constructor.
     *
     * @param AdvertisementRepository $advertisements
     */
    public function __construct(AdvertisementRepository $advertisements)
    {
        $this->advertisements = $advertisements;
    }

    /**
     * Show list of advertisements
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render(
            'admin.advertisement.index', [
            'models' => $this->advertisements->paginate(config('main.per_page')),
            ]
        );
    }

    /**
     * Show the advertisement add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->advertisements->makeNew());
    }

    /**
     * Show the edit page for the given advertisement
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->advertisements->findByHash($id);

        return $this->form($model);
    }

    /**
     * Show advertisement form
     *
     * @param Advertisement $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(Advertisement $model)
    {
        Scripts::view('admin.advertisement.scripts');

        return $this->render(
            'admin.advertisement.form', [
            'model'         => $model,
            'typeOptions'   => $this->advertisements->getTypeOptions(),
            'statusOptions' => $this->advertisements->getStatusOptions(),
            ]
        );
    }

    /**
     * Create or update an advertisement
     *
     * @param AdvertisementFormRequest $request
     * @param string|null              $id
     *
     * @return \Illuminate\Http\Response
     */
    public function save(AdvertisementFormRequest $request, $id = null)
    {
        if ($this->advertisements->saveByHash($request->all(), $id)) {
            session()->flash('success', trans('advertisement.saved'));

            return redirect()->route('admin.advertisement.index');
        }
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->advertisements->deleteByHash($id);

        return redirect()->route('admin.advertisement.index');
    }
}
