<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\AlertMessageRepository;
use Illuminate\Http\Request;

/**
 * Class AlertMessageController
 */
class AlertMessageController extends AdminController
{

    /**
     * @var AlertMessageRepository
     */
    private $alertMessages;

    /**
     * AlertMessageController constructor.
     *
     * @param AlertMessageRepository $alertMessages
     */
    public function __construct(AlertMessageRepository $alertMessages)
    {
        $this->alertMessages = $alertMessages;
    }

    /**
     * Show list of alert messages
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $models = $this->alertMessages->getAllPaginated($request);
        $priorityOptions = $this->alertMessages->getPriorityOptions();

        view()->share(compact('models', 'priorityOptions'));

        return $this->render('admin.alert_message.index');
    }

}
