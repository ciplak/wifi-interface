<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\AccessCodeRepository;
use App\Repositories\ServiceProfileRepository;
use App\Repositories\VenueRepository;
use App\Services\Scripts;
use Illuminate\Http\Request;

/**
 * Class AccessCodeController
 */
class AccessCodeController extends AdminController
{
    /**
     * @var AccessCodeRepository
     */
    private $accessCodes;

    /**
     * @var ServiceProfileRepository
     */
    private $serviceProfiles;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * AccessCodeController constructor.
     *
     * @param AccessCodeRepository     $accessCodes
     * @param ServiceProfileRepository $serviceProfiles
     * @param VenueRepository          $venues
     */
    public function __construct(AccessCodeRepository $accessCodes, ServiceProfileRepository $serviceProfiles, VenueRepository $venues)
    {
        $this->accessCodes = $accessCodes;
        $this->serviceProfiles = $serviceProfiles;
        $this->venues = $venues;
    }

    /**
     * Show list of access codes
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formTypeOptions = $this->accessCodes->getTypes(true)
            ->toVueArrayJson();

        $venues = $this->venues
            ->getNameList();

        $serviceProfiles = $this->serviceProfiles
            ->getActiveServiceProfilesWithPinEnabled();

        $formLocations = $this->venues->getNameListCollection()
            ->toVueArrayJson(trans('access_code.type.select'));

        $formServiceProfileOptions = $serviceProfiles
            ->toVueArrayJson(trans('service_profile.select'));

        view()->share(
            [
            'models'                    => $this->accessCodes->getAllPaginated($request),
            'venueOptions'              => $venues,
            'serviceProfileOptions'     => $serviceProfiles,
            'expiredOptions'            => $this->accessCodes->getExpiredOptions(),
            'statusOptions'             => $this->accessCodes->getStatusOptions(),
            'typeOptions'               => $this->accessCodes->getTypes(),
            'formTypeOptions'           => $formTypeOptions,
            'formServiceProfileOptions' => $formServiceProfileOptions,
            'formLocationOptions'       => $formLocations,
            ]
        );

        Scripts::view('admin.access_code.scripts');

        return $this->render('admin.access_code.index');
    }

    /**
     * Delete the given access code
     *
     * @param string $value
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($value)
    {
        if ($this->accessCodes->deleteByHash($value)) {
            session()->flash('success', trans('access_code.deleted'));
        } else {
            session()->flash('error', trans('access_code.not_deleted'));
        }

        return redirect()->route('admin.access_code.index');
    }
}
