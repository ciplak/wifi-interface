<?php
namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\ViewController;

abstract class AdminController extends ViewController
{
    public $layout = 'layouts.admin';


}