<?php
namespace App\Http\Controllers\Admin;

/**
 * Class DeviceVendorController
 */
class DeviceVendorController extends AdminController
{

    /**
     * Show list of device vendors
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('admin.device_vendor.index');
    }

}
