<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserFormRequest;
use App\Models\Role;
use App\Models\User;
use App\Models\Venue;
use App\Repositories\UserRepository;
use App\Services\Scripts;

class UserController extends AdminController
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * UserController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Show list of users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('admin.user.index', [
            'models' => $this->users->getAllPaginated(),
        ]);
    }

    /**
     * Show the user add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->users->makeNew());
    }

    /**
     * Show user form page
     *
     * @param User $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(User $model)
    {
        if (!\Entrust::can('developers.manage') && $model->hasRole('developer')) {
            return redirect()->route('admin.user.index');
        }

        if (!\Entrust::can('support.manage') && $model->hasRole('support')) {
            return redirect()->route('admin.user.index');
        }

        $roleQuery = Role::query();
        $disabledRoles = [];

        if (!\Entrust::can('developers.manage')) {
            $disabledRoles[] = 'developer';
        }

        if (!\Entrust::can('support.manage')) {
            $disabledRoles[] = 'support';
        }

        if ($disabledRoles) {
            $roleQuery->whereNotIn('name', $disabledRoles);
        }

        $roles = $roleQuery->pluck('display_name', 'name')->toArray();
        $properties = Venue::orderBy('name')->pluck('name', 'id')->toArray();
        $sessions = $this->users->getSessions($model->id);
        $localeOptions = getLocales();
        $statusOptions = $this->users->getStatusOptions();

        Scripts::view('admin.user.scripts');

        return $this->render('admin.user.form', compact('model', 'roles', 'statusOptions', 'properties', 'sessions', 'localeOptions'));
    }

    /**
     * Show the edit page for the given user
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->form($this->users->findByHash($id));
    }

    /**
     * Create or update a user
     *
     * @param UserFormRequest $request
     * @param string|null     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function save(UserFormRequest $request, $id = null)
    {
        if ($this->users->saveByHash($request->all(), $id)) {
            session()->flash('success', trans('user.saved'));

            return redirect()->route('admin.user.index');
        } else {
            session()->flash('error', trans('user.not_saved'));
        }
    }

    /**
     * Delete the given user
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $status = $this->users->delete($id);

        $message = $status ? trans('user.deleted') : trans('user.not_deleted');
        session()->flash($status ? 'success' : 'error', $message);

        return $status ? redirect()->route('admin.user.index') : redirect()->back();
    }

    /**
     * Show the user's profile information
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return $this->render('admin.user.profile');
    }

    /**
     * Invalidate specified session for the user
     *
     * @param string $id
     * @param string $sessionId
     *
     * @return mixed
     */
    public function invalidateSessions($id, $sessionId)
    {
        $this->users->invalidateSessions($id, $sessionId);

        session()->flash('success', trans('user.profile_updated'));

        return redirect()->back();
    }

    /**
     * Switch tehantn of the current user
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function switchTenant($id)
    {
        if (\Entrust::can('tenants.switch')) {
            $this->users->update(['tenant_id' => $id], \Auth::id());
        }

        return redirect()->back();
    }

    /**
     * Impersonate another user
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function impersonate($id)
    {
        $user = User::find($id);

        if (!$user->hasRole('developer')) {
            \Auth::user()->setImpersonating($user->id);

            // if the user impersonated can not see users page, redirect to dashboard
            if (!$user->can('users.manage')) {
                return redirect()->route('admin.index');
            }
        }

        return redirect()->back();
    }

    /**
     * Stop impersonating a user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function stopImpersonate()
    {
        \Auth::user()->stopImpersonating();

        session()->flash('success', 'Welcome back!');

        return redirect()->back();
    }

    /**
     * Ban a user
     *
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function banUser($id)
    {
        if ($this->users->banByHash($id)) {
            session()->flash('success', 'User is banned');
        }

        return back();
    }
}
