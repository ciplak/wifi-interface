<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProximityMarketingCampaignFormRequest;
use App\Models\Guest;
use App\Models\ProximityMarketingCampaign;
use App\Repositories\ProximityMarketingCampaignRepository;
use App\Repositories\VenueRepository;
use App\Services\Scripts;

class ProximityMarketingController extends AdminController
{
    /**
     * @var ProximityMarketingCampaignRepository
     */
    private $proximityMarketingCampaigns;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * ProximityMarketingController constructor.
     *
     * @param ProximityMarketingCampaignRepository $proximityMarketingCampaigns
     * @param VenueRepository $venues
     */
    public function __construct(ProximityMarketingCampaignRepository $proximityMarketingCampaigns, VenueRepository $venues)
    {
        $this->proximityMarketingCampaigns = $proximityMarketingCampaigns;
        $this->venues = $venues;
    }

    /**
     * Show list of proximityMarketingCampaigns
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = $this->proximityMarketingCampaigns->getAllPaginated();

        return $this->render(
            'admin.proximity_marketing.index', [
                'models' => $models,
            ]
        );
    }

    /**
     * Show the proximity_marketing add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->proximityMarketingCampaigns->makeNew());
    }

    /**
     * Show the edit page for the given proximity_marketing
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->proximityMarketingCampaigns->findByHash($id);

        return $this->form($model);
    }

    /**
     * Show proximity_marketing form
     *
     * @param ProximityMarketingCampaign $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(ProximityMarketingCampaign $model)
    {


        view()->share('model', $model);

        Scripts::view('admin.proximity_marketing.scripts');
        Scripts::url('assets/js/ckeditor/ckeditor.js');



        return $this->render(
            'admin.proximity_marketing.form', [
                'statusOptions'                   => $this->proximityMarketingCampaigns->getStatusOptions(),
                'locations'                       => $this->venues->getList(),
                'eventTypeOptions'                => $this->proximityMarketingCampaigns->eventTypeOptions(),
                'parameterTypeOptions'            => $this->proximityMarketingCampaigns->parameterTypeOptions(),
                'parameterTypeVisitOptions'       => $this->proximityMarketingCampaigns->parameterTypeVisitOptions(),
                'parameterTypeNumberOfVisits'     => $this->proximityMarketingCampaigns->parameterTypeNumberOfVisits(),
                'frequenceTypeOptions'            => $this->proximityMarketingCampaigns->frequenceTypeOptions(),
                'renderAudience'                  => $this->proximityMarketingCampaigns->renderAudience($model),
                'weekDays'                        => $this->proximityMarketingCampaigns->weekDays(),
                'ageTypeOptions'                  => $this->proximityMarketingCampaigns->ageTypeOptions(),
                'communicationChannelTypeOptions' => $this->proximityMarketingCampaigns->communicationChannelTypeOptions(),
                'genderTypeOptions'               => Guest::getGenderOptions(),

            ]
        );
    }

    /**
     * @param ProximityMarketingCampaignFormRequest $request
     * @param null $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(ProximityMarketingCampaignFormRequest $request, $id = null)
    {
        if ($this->proximityMarketingCampaigns->saveByHash($request->all(), $id)) {
            session()->flash('success', trans('proximity_marketing.saved'));

            return redirect()->route('admin.proximity_marketing.index');
        }
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->proximityMarketingCampaigns->deleteByHash($id);

        return redirect()->route('admin.proximity_marketing.index');
    }

    public function estimate()
    {
        $data['msg'] = [
           'success'
        ];

        $model = (object) request()->get('data')[0];

        $value = $this->proximityMarketingCampaigns->renderAudience($model);
        $data['value'] = $value;
        return response()->json($data);

    }
}
