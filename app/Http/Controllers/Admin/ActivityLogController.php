<?php
namespace App\Http\Controllers\Admin;

/**
 * Class ActivityLogController
 */
class ActivityLogController extends AdminController
{

    /**
     * Show the activity logs
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('admin.activity_log.index');
    }

}