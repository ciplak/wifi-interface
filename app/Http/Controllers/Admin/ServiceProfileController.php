<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ServiceProfileFormRequest;
use App\Models\ServiceProfile;
use App\Repositories\ServiceProfileRepository;
use App\Services\Scripts;
use Illuminate\Http\Request;

/**
 * Class ServiceProfileController
 *
 * @package App\Http\Controllers\Admin
 */
class ServiceProfileController extends AdminController
{
    /**
     * @var ServiceProfileRepository
     */
    private $serviceProfiles;

    /**
     * ServiceProfileController constructor.
     *
     * @param ServiceProfileRepository $serviceProfiles
     */
    public function __construct(ServiceProfileRepository $serviceProfiles)
    {
        $this->serviceProfiles = $serviceProfiles;
    }

    /**
     * Show list of service profiles
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        view()->share([
            'durationOperators' => ServiceProfile::getOperators(true),
            'types'             => $this->serviceProfiles->getTypeOptions(),
            'modelOptions'      => $this->serviceProfiles->getModelOptions(),
            'servicePinOptions' => $this->serviceProfiles->getServicePinTypeOptions(),
            'statusOptions'     => $this->serviceProfiles->getStatusOptions(),
        ]);

        return $this->render(
            'admin.service_profile.index', [
                'models' => \App\Models\ServiceProfile::filter($request)->paginate(config('main.per_page')),
            ]
        );
    }

    /**
     * Show the service profile add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $model = $this->serviceProfiles->makeNew();

        return $this->form($model);
    }

    /**
     * Show service profile form
     *
     * @param ServiceProfile $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(ServiceProfile $model)
    {
        view()->share(
            [
                'durationTypes'         => $this->serviceProfiles->getDurationTypeOptions(),
                'types'                 => $this->serviceProfiles->getTypeOptions(),
                'renewalFrequencyTypes' => $this->serviceProfiles->getRenewalFrequencyTypeOptions(),
                'modelOptions'          => $this->serviceProfiles->getModelOptions(),
                'servicePinOptions'     => $this->serviceProfiles->getServicePinTypeOptions(),
                'statusOptions'         => $this->serviceProfiles->getStatusOptions(),
                'qosLevels'             => [3 => 'Bronze', 0 => 'Silver', 1 => 'Gold', 2 => 'Platinum'],
            ]
        );

        Scripts::view('admin.service_profile.scripts');

        return $this->render(
            'admin.service_profile.form', [
                'model' => $model,
            ]
        );
    }

    /**
     * Show the edit page for the given service profile
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->serviceProfiles->findByHash($id);

        return $this->form($model);
    }

    /**
     * Create or update a service profile
     *
     * @param ServiceProfileFormRequest $request
     * @param int                       $id
     *
     * @return mixed
     */
    public function save(ServiceProfileFormRequest $request, $id = null)
    {
        if ($this->serviceProfiles->saveByHash($request->all(), $id)) {
            session()->flash('success', trans('service_profile.saved'));

            return redirect()->route('admin.service_profile.index');
        } else {
            session()->flash('error', trans('service_profile.not_saved'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Delete the given service profile
     *
     * @param int $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        $status = false;

        try {
            $status = $this->serviceProfiles->deleteByHash($id);
            $message = $status ? trans('service_profile.deleted') : trans('service_profile.not_deleted');
        } catch (\Illuminate\Database\QueryException $e) {
            \Log::error('Query error on deleting service profile: ', ['message' => $e->getMessage(), 'id' => $id]);
            $message = trans('service_profile.not_deleted.in_use');
        } catch (\Exception $e) {
            \Log::error('Error on deleting service profile: ', ['message' => $e->getMessage()]);
            $message = trans('service_profile.not_deleted');
        }

        session()->flash($status ? 'success' : 'error', $message);

        return $status ? redirect()->route('admin.service_profile.index') : redirect()->back();
    }
}