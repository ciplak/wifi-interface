<?php

namespace App\Http\Controllers\Admin;

/**
 * Class CurrencyController
 */
class CurrencyController extends AdminController
{
    /**
     * Show list of currencies
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('admin.currency.index');
    }
}
