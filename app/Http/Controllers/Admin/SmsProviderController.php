<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SmsProviderFormRequest;
use App\Repositories\SmsProviderRepository;
use App\Services\Scripts;

class SmsProviderController extends AdminController
{

    /**
     * @var SmsProviderRepository
     */
    private $smsProviders;

    /**
     * SmsProviderController constructor.
     *
     * @param SmsProviderRepository $smsProviders
     */
    public function __construct(SmsProviderRepository $smsProviders)
    {
        $this->smsProviders = $smsProviders;
    }

    /**
     * Show list of SMS providers
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render(
            'admin.sms_provider.index', [
            'models'   => $this->smsProviders->all(['id', 'name', 'description', 'gateway_id']),
            'gateways' => $this->smsProviders->getGatewayOptions(),
            ]
        );
    }

    /**
     * Show the add page for SMS provider
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->smsProviders->makeNew());
    }

    /**
     * Show the edit page for the given SMS provider
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->smsProviders->findByHash($id);

        return $this->form($model);
    }

    /**
     * Show SMS provider form
     *
     * @param \App\Models\SmsProvider $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form($model)
    {
        Scripts::view('admin.sms_provider.scripts');

        return $this->render(
            'admin.sms_provider.form', [
            'model'             => $model,
            'smsGatewayOptions' => $this->smsProviders->getGatewayOptions(),
            'smsGateways'       => config('sms.gateways'),
            ]
        );
    }

    /**
     * Create or update an SMS provider
     *
     * @param SmsProviderFormRequest $request
     * @param string                 $id
     *
     * @return \Illuminate\Http\Response
     */
    public function save(SmsProviderFormRequest $request, $id = null)
    {
        if ($this->smsProviders->saveByHash($request->all(), $id)) {
            session()->flash('success', trans('sms_provider.saved'));

            return redirect()->route('admin.sms_provider.index');
        }
    }

    /**
     * Delete the given SMS provider
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $status = $this->smsProviders->deleteByHash($id);
        } catch (\Exception $e) {
            $status = false;
        }
        $message = $status ? trans('sms_provider.deleted') : trans('sms_provider.not_deleted');

        if (\Request::ajax()) {
            return response()->json(['status' => $status, 'message' => $message]);
        } else {
            session()->flash($status ? 'success' : 'error', $message);

            return redirect()->route('admin.sms_provider.index');
        }
    }

}
