<?php
namespace App\Http\Controllers\Admin;

use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;
use Illuminate\Http\Request;

class LogViewerController extends AdminController
{
    /**
     * Show system logs
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->input('l')) {
            LaravelLogViewer::setFile(base64_decode($request->input('l')));
        }

        if ($request->input('dl')) {
            return response()->download(LaravelLogViewer::pathToLogFile(base64_decode($request->input('dl'))));
        } elseif ($request->has('del')) {
            \File::delete(LaravelLogViewer::pathToLogFile(base64_decode($request->input('del'))));

            return redirect($request->url());
        }

        $logs = LaravelLogViewer::all();

        $logs = collect($logs)->map(
            function ($log) {
                $log['parameters'] = null;

                if (str_contains($log['text'], '{')) {
                    list($text, $parameters) = explode('{', $log['text'], 2);
                    $parameters = '{' . $parameters;

                    $log['text'] = $text;
                    $log['parameters'] = jsonPrettify($parameters);
                }

                return $log;
            }
        );

        return $this->render(
            'admin.log_viewer.log', [
            'logs'         => $logs,
            'files'        => LaravelLogViewer::getFiles(true),
            'current_file' => LaravelLogViewer::getFileName(),
            ]
        );
    }

}