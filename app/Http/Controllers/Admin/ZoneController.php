<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ZoneFormRequest;
use App\Models\Content;
use App\Models\Property;
use App\Models\SplashPage;
use App\Models\Zone;
use App\Repositories\ServiceProfileRepository;
use App\Repositories\SmsProviderRepository;
use App\Repositories\SplashPageRepository;
use App\Repositories\VenueRepository;
use App\Repositories\ZoneRepository;
use App\Services\Scripts;

class ZoneController extends AdminController
{
    /**
     * @var ServiceProfileRepository
     */
    private $serviceProfiles;

    /**
     * @var SmsProviderRepository
     */
    private $smsProviders;

    /**
     * @var SplashPageRepository
     */
    private $splashPages;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * @var ZoneRepository
     */
    private $zones;

    /**
     * VenueController constructor.
     *
     * @param ServiceProfileRepository $serviceProfiles
     * @param SmsProviderRepository    $smsProviders
     * @param SplashPageRepository     $splashPages
     * @param VenueRepository          $venues
     * @param ZoneRepository           $zones
     */
    public function __construct(ServiceProfileRepository $serviceProfiles, SmsProviderRepository $smsProviders, SplashPageRepository $splashPages, VenueRepository $venues, ZoneRepository $zones)
    {
        $this->serviceProfiles = $serviceProfiles;
        $this->smsProviders = $smsProviders;
        $this->splashPages = $splashPages;
        $this->venues = $venues;
        $this->zones = $zones;
    }

    /**
     * Show the zone add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->zones->makeNew());
    }

    /**
     * Show zone form
     *
     * @param Zone $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(Zone $model)
    {
        if ($model && $model->exists) {
            \Auth::user()->canManageVenue($model->parent_id);
        }

        view()->share([
            'model'                      => $model,
            'serviceProfiles'            => $model->serviceProfiles,
            'venues'                     => $this->venues->getNameList(),
            'loginPages'                 => $this->splashPages->getPublishedParentList(),
            'postLoginPages'             => $this->splashPages->getPublishedParentList(SplashPage::TYPE_ONLINE),
            'smsProviders'               => $this->smsProviders->getList(),
            'smartLoginOptions'          => Property::getSmartLoginOptions(),
            'smartLoginFrequencyOptions' => Property::getSmartLoginFrequencyTypeOptions(),
            'allServiceProfiles'         => $this->serviceProfiles->getList(),
        ]);

        Scripts::view('admin.venue.scripts');
        Scripts::url('assets/js/ckeditor/ckeditor.js');

        return $this->render('admin.zone.form');
    }

    /**
     * Show the edit page for the given zone
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->form($this->zones->findByHash($id));
    }

    /**
     * Create or update a zone
     *
     * @param ZoneFormRequest $request
     * @param string|null     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function save(ZoneFormRequest $request, $id = null)
    {
        if ($model = $this->zones->saveByHash($request->all(), $id)) {

            if ($model->is_new) {
                session()->flash('success', trans('zone.created'));
            } else {
                session()->flash('success', trans('zone.saved'));
            }

            return redirect()->route('admin.venue.index');

        }
    }

    /**
     * Delete the given zone
     *
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $status = false;

        try {
            $status = $this->zones->deleteByHash($id);
        } catch (\Exception $e) {
            \Log::error('Error on deleting zone: ', ['message' => $e->getMessage()]);
        }

        $message = $status ? trans('zone.deleted') : trans('zone.not_deleted');
        session()->flash($status ? 'success' : 'error', $message);

        return redirect()->route('admin.venue.index');
    }

}
