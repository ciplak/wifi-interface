<?php
namespace App\Http\Controllers\Admin;

use App\Models\CmxData;
use App\Repositories\VenueRepository;

class DashboardController extends AdminController
{
    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * DashboardController constructor.
     *
     * @param VenueRepository $venues
     */
    public function __construct(VenueRepository $venues)
    {
        $this->venues = $venues;
    }

    /**
     * Show the admin dashboard
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = [];

        if (\Entrust::can('reports.view-global')) {
            $locations = [trans('venue.all')] + $this->venues->getNameList();
        }

        view()->share('locations', json_encode($locations));

        return $this->render('admin.dashboard.index');
    }

    /**
     * Set admin page language
     *
     * @param string $lang
     *
     * @return \Illuminate\Http\Response
     */
    public function lang($lang)
    {
        $languages = array_keys(getLocales());

        if (!in_array($lang, $languages)) {
            $lang = config('app.locale');
        }

        session()->put('lang', $lang);
        \App::setLocale($lang);

        return redirect(\URL::previous());
    }

    /**
     * Show about page
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return $this->render('admin.dashboard.about');
    }

    public function map()
    {
        $radius = 20;

        return $this->render(
            'map', [
            'data'   => $this->getMapData($radius),
            'radius' => $radius,
            ]
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getMapData($radius = 100)
    {
        $data = CmxData::where('tenant_id', activeTenantId())
            ->latest('id')
            ->take(1)
            ->value('data');

        $item = json_decode($data);

        $markers = collect($item->data->observations)->map(
            function ($item) use ($radius) {

                $r = $radius / 111300;
                $y0 = $item->location->lat;
                $x0 = $item->location->lng;

                $u = mt_rand() / mt_getrandmax();
                $v = mt_rand() / mt_getrandmax();

                $w = $r * sqrt($u);
                $t = 2 * M_PI * $v;
                $x = $w * cos($t);
                $y = $w * sin($t);

                return [
                'mac'     => $item->clientMac,
                'seen_at' => $item->seenTime,
                'lat'     => $item->location->lat + $y,
                'lng'     => $item->location->lng + $x,
                ];
            }
        );

        return $markers;
    }
}