<?php

namespace App\Http\Controllers\Admin;

class RadiusServerController extends AdminController
{
    /**
     * Show list of currencies
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render(
            'admin.radius_server.index', [
            ]
        );
    }
}
