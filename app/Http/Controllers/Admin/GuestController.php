<?php

namespace App\Http\Controllers\Admin;

use App\ConnectionStatus;
use App\Http\Requests\GuestFormRequest;
use App\Models\AccessCode;
use App\Models\Guest;
use App\Repositories\AccessCodeRepository;
use App\Repositories\GuestRepository;
use App\Repositories\VenueRepository;
use App\Services\RadiusCommunicator\RadiusCommunicator;
use App\Services\Scripts;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GuestController extends AdminController
{
    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * @var AccessCodeRepository
     */
    private $accessCodes;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * GuestController constructor.
     *
     * @param GuestRepository      $guests
     * @param AccessCodeRepository $accessCodes
     * @param VenueRepository      $venues
     */
    public function __construct(GuestRepository $guests, AccessCodeRepository $accessCodes, VenueRepository $venues)
    {
        $this->guests = $guests;
        $this->accessCodes = $accessCodes;
        $this->venues = $venues;
    }

    /**
     * Show list of guests
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        view()->share([
            'models'   => $this->guests->getAllPaginated($request),
            'venues'   => $this->venues->getNameList(),
            'statuses' => ConnectionStatus::labels(),
        ]);

        return $this->render('admin.guest.index');
    }

    /**
     * Show the guests add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->guests->makeNew());
    }

    /**
     * Show guest form
     *
     * @param Guest $model
     *
     * @return \Illuminate\Http\Response
     */
    private function form(Guest $model)
    {
        return $this->render(
            'admin.guest.form', [
                'model'              => $model,
                'genderOptions'      => $this->guests->getGenderOptions(),
                'nationalityOptions' => $this->guests->getNationalityOptions(),
                'directLoginOptions' => [trans('messages.no'), trans('messages.yes')],
                'venues'             => $this->venues->getNameList(),
            ]
        );
    }

    /**
     * Show the edit page for the given guest
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->form($this->guests->findByHash($id));
    }

    /**
     * Show details of the given user
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $model = $this->guests->getDetailsByHash($id);

        view()->share([
            'model'    => $model,
            'statuses' => ConnectionStatus::labels(),
            'devices'  => $model->devices,
        ]);

        Scripts::view('admin.guest.scripts');

        return $this->render('admin.guest.details', [
            'sessions' => $this->guests->getConnectionSessions($model, 50),
        ]);
    }

    /**
     * Create or update a guest
     *
     * @param GuestFormRequest $request
     * @param string           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function save(GuestFormRequest $request, $id = null)
    {
        if ($this->guests->saveByHash($request->all(), $id)) {
            session()->flash('success', trans('guest.saved'));

            return redirect()->route('admin.guest.details', [$id]);
        }

        return redirect()->route('admin.guest.index');
    }

    /**
     * Generate an access code for the given guest
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function generateAccessCode($id)
    {
        $guest = $this->guests->findByHash($id);

        $accessCode = $this->accessCodes->generateSingleCode(
            [
                'service_profile_id' => $guest->venue->getDefaultServiceProfile()->id,
                'guest_id'           => $guest->id,
                'venue_id'           => $guest->venue_id,
                'expiry_date'        => Carbon::tomorrow(),
            ], setting('access_code.length', 8),
            setting('access_code.chars', AccessCode::CHARS_ALPHANUMERIC)
        );

        $guest->load('accessCodes.serviceProfile');

        return response()->json(
            [
                'status'  => $accessCode ? true : false,
                'code'    => $accessCode ? $accessCode->code : '',
                'message' => trans('guest.access_code_generated'),
                'html'    => view(
                    'admin.access_code.grid', [
                        'models'  => $guest->accessCodes,
                        'partial' => true,
                    ]
                )->render(),
            ]
        );
    }

    /**
     * Drop connection for the given guest
     *
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function disconnect($id)
    {
        $guest = $this->guests->findByHash($id);

        if ($guest) {
            $logoutStatus = (new RadiusCommunicator)
                ->driver(config('main.radius_server'))
                ->logoutWifiUser($guest);

            if ($logoutStatus) {
                $this->guests->update(['connection_status' => \App\ConnectionStatus::DISCONNECTED], hashids_decode($id));

                session()->flash('success', trans('guest.disconnected'));
            } else {
                session()->flash('error', trans('guest.not_disconnected'));
            }
        }

        return redirect()->back();
    }
}