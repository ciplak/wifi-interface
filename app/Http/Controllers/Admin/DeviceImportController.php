<?php

namespace App\Http\Controllers\Admin;

use App\Models\Device;
use App\Models\DeviceModel;
use App\Models\Venue;
use App\Models\Zone;
use App\Repositories\DeviceModelRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\VenueRepository;
use App\Services\RadiusCommunicator\RadiusCommunicator;
use App\Services\Scripts;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Readers\LaravelExcelReader;
use Maatwebsite\Excel\Writers\CellWriter;
use Illuminate\Http\Request;

/**
 * Class DeviceImportController
 */
class DeviceImportController extends AdminController
{
    /**
     * @var DeviceRepository
     */
    private $devices;
    /**
     * @var VenueRepository
     */
    private $venues;
    /**
     * @var DeviceModelRepository
     */
    private $deviceModels;

    /**
     * DeviceController constructor.
     *
     * @param DeviceRepository      $devices
     * @param VenueRepository       $venues
     * @param DeviceModelRepository $deviceModels
     */
    public function __construct(DeviceRepository $devices, VenueRepository $venues, DeviceModelRepository $deviceModels)
    {
        $this->devices = $devices;
        $this->venues = $venues;
        $this->deviceModels = $deviceModels;
    }

    /**
     * Show list of devices
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        view()->share(
            [
                'deviceModels'        => $this->deviceModels->getAllModels(),
                'locations'           => $this->venues->getList(),
                'importLocations'     => $this->venues->getList(true),
                'deviceStatusOptions' => $this->devices->getStatusOptions(),
                'models'              => $this->devices->getAllPaginated($request),
            ]
        );

        Scripts::view('admin.device.scripts');

        return $this->render('admin.device.index');
    }

    /**
     * Import devices from a file
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $validator = validator(
            $request->all(), [
                'device-file' => 'required',
            ]
        );

        if ($validator->fails()) {
            $errors = implode('<br>', $validator->errors()->all());
            $errors .= '<br>' . trans('device.invalid_file');

            return redirect()->route('admin.device.index')->with('error', $errors);
        }

        $importResult = $this->importFile($request->file('device-file'));
        if (!isset($importResult['errors']) || (!count($importResult['errors']))) {
            return redirect()->route('admin.device.index');
        }

        Scripts::view('admin.device.scripts');

        return $this->render(
            'admin.device.import_results', [
                'results' => $importResult,
            ]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile|array|null $file
     *
     * @return array
     */
    private function importFile($file)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');

        config(['excel.import.heading' => 'numeric']);

        $info = [
            'results' => [
                'success' => 0,
                'errors'  => 0,
            ],

            'errors' => [],
        ];

        \Excel::selectSheetsByIndex(0)->load(
            $file->getRealPath(), function ($reader) use (&$info) {

            /**
             * @var LaravelExcelReader $reader
             */

            // Getting all results
            $rows = $reader->toArray();

            if (count($rows) > 1) {
                // remove headers
                $info['headers'] = $rows[0];

                unset($rows[0]);

                $defaults = array_fill(0, 8, '');

                $rules = [
                    'property_id'     => 'required',
                    'name'            => 'required',
                    'mac'             => 'required|mac',
                    'ip'              => 'required',
                    'device_model_id' => 'required',
                ];

                $messages = [
                    'device_model_id.required' => 'Device model not found',
                    'property_id.required'     => 'Venue / zone not found',
                ];

                $propertyList = [
                    'venues' => [],
                    'zones'  => [],
                ];

                $i = 1;
                foreach ($rows as $row) {
                    $i++;
                    $row = array_map('trim', $row);

                    list($vendor, $model, $ip, $mac, $name, $venueName, $zoneName, $description) = array_merge($row, $defaults);

                    try {

                        $venue = $zone = null;

                        $venueName = strtolower($venueName);
                        $zoneName = strtolower($zoneName);

                        if ($venueName) {
                            if (!isset($propertyList['venues'][$venueName])) {
                                $venue = Venue::where('parent_id', activeTenantId())->where('name', $venueName)->first();
                                $propertyList['venues'][$venueName] = $venue;
                            }

                            $venue = $propertyList['venues'][$venueName];

                            if ($zoneName) {
                                if (!isset($propertyList['zones'][$zoneName])) {
                                    $zone = Zone::where('parent_id', activeTenantId())->where('name', $zoneName)->first();
                                    $propertyList['zones'][$zoneName] = $zone;
                                }

                                $zone = $propertyList['zones'][$zoneName];
                            }
                        }

                        $property = $zone ?: $venue;

                        /**
                         * @var \App\Models\DeviceModel $deviceModel
                         */
                        $deviceModel = DeviceModel::where('name', $model)->first();

                        /**
                         * @var Device $device
                         */
                        $device = Device::whereMac($mac)->withTrashed()->first() ?: new Device();

                        $data = [
                            'name'            => $name ?: ($deviceModel ? $deviceModel->name : null),
                            'description'     => $description,
                            'property_id'     => $property ? $property->id : null,
                            'mac'             => $mac,
                            'ip'              => $ip,
                            'device_model_id' => $deviceModel ? $deviceModel->id : null,
                            'deleted_at'      => null,
                        ];

                        $validator = validator($data, $rules, $messages);
                        if ($validator->fails()) {
                            $info['errors'][$i] = [
                                'data'   => $row,
                                'errors' => $validator->errors()->all(),
                            ];

                            $info['results']['errors']++;

                            \Log::error(
                                'Error on bulk device import', [
                                    'errors' => $validator->errors()->all(),
                                    'row'    => $row,
                                    'data'   => $data,
                                ]
                            );
                        } else {

                            $device->fill($data);

                            if ($device->save()) {
                                $info['results']['success']++;
                                \Log::info('Created device by bulk insert', ['device' => $device]);
                            } else {
                                $info['results']['errors']++;
                            }
                        }
                    } catch (\Exception $e) {
                        \Log::error('Error on bulk device import ' . $e->getMessage(), [
                            'e'   => get_class($e),
                            'row' => $row,
                        ]);
                    }
                }

                (new RadiusCommunicator)
                    ->driver(config('main.radius_server'))
                    ->updateDevice();

                \Log::info('Total number of device created by bulk insert', [
                    'success' => $info['results']['success'],
                    'error'   => $info['results']['errors'],
                ]);
            } else {
                session()->flash('error', trans('device.import_file_empty'));
            }
        }
        );

        return $info;
    }

    /**
     * Download sample import file
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadImportFile()
    {
        if (!\File::exists(storage_path('app/files/devices.xlsx'))) {

            \Excel::create(
                'devices', function ($excel) {
                $excel->setTitle('Devices');

                $excel->sheet(
                    'Device list', function ($sheet) {

                    /**
                     * @var LaravelExcelWorksheet $sheet
                     */
                    $sheet->row(1, ['Vendor', 'Model', 'IP', 'Mac Address', 'Name', 'Venue', 'Zone', 'Description']);

                    $sheet->row(
                        1, function ($row) {
                        /**
                         * @var CellWriter $row
                         */
                        $row->setFontWeight('bold')
                            ->setBorder('none', 'none', 'solid', 'none');
                    }
                    );

                    $sheet->setAutoSize(true);
                }
                );
            }
            )->store('xlsx', storage_path('app/files'));
        }


        return response()->download(storage_path('app/files/devices.xlsx'));
    }
}
