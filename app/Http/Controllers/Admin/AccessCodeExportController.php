<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\AccessCodeRepository;
use App\Transformers\AccessCodeExportTransformer;
use Illuminate\Http\Request;

class AccessCodeExportController extends AdminController
{
    /**
     * @var AccessCodeRepository
     */
    private $accessCodes;

    /**
     * AccessCodeController constructor.
     *
     * @param AccessCodeRepository $accessCodes
     */
    public function __construct(AccessCodeRepository $accessCodes)
    {
        $this->accessCodes = $accessCodes;
    }

    /**
     * Export access codes
     *
     * @param Request $request
     */
    public function export(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $codes = $this->accessCodes->getAll($request);

        $results = fractal()
            ->collection($codes, new AccessCodeExportTransformer($this->accessCodes))
            ->toArray();

        $format = strtolower($request->input('format'));

        if (!in_array($format, ['csv', 'xslx', 'pdf'])) {
            $format = 'xlsx';
        }

        \Excel::create('access_codes', function($excel) use ($results) {
            /**
             * @var \Maatwebsite\Excel\Writers\LaravelExcelWriter $excel
             */
            $excel->sheet(trans('access_code.page_title'), function($sheet) use ($results) {
                /**
                 * @var \Maatwebsite\Excel\Classes\LaravelExcelWorksheet $sheet
                 */
                $sheet->_parent->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);
                $sheet->freezeFirstRow();
                $sheet->setAutoFilter('A1:E1');
                $sheet->setAutoSize(true);
                $sheet->setAllBorders();

                $sheet->setColumnFormat([
                    'B' => '@',
                    'E' => 'dd/mm/yy',
                ]);

                $sheet->row(1, [
                    trans('access_code.columns.location'),
                    trans('access_code.columns.type'),
                    trans('access_code.columns.code'),
                    trans('service_profile.title'),
                    trans('access_code.columns.expiry_date'),
                    trans('messages.available'),
                ]);

                $maxRow = count($results) + 1;
                // clear all borders
                $sheet->setBorder('A1:F' . $maxRow, 'none');

                $sheet->cell('A1:F1', function($cells) {
                    $cells->setBorder('none', 'none', 'solid', 'none');
                });

                for ($i = 2; $i <= $maxRow; $i += 2) {
                    $sheet->row($i, function($row) {
                        $row->setBackground('#f4f4f4');
                    });
                }

                $sheet->fromArray($results, null, 'A2', false, false);
            });
        })->export($format);
    }
}
