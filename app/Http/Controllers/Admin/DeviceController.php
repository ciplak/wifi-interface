<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DeviceFormRequest;
use App\Models\AlertMessage;
use App\Models\Device;
use App\Repositories\DeviceModelRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\VenueRepository;
use App\Services\RadiusCommunicator\RadiusCommunicator;
use App\Services\Scripts;
use Illuminate\Http\Request;

/**
 * Class DeviceController
 */
class DeviceController extends AdminController
{
    /**
     * @var DeviceRepository
     */
    private $devices;
    /**
     * @var VenueRepository
     */
    private $venues;
    /**
     * @var DeviceModelRepository
     */
    private $deviceModels;

    /**
     * DeviceController constructor.
     *
     * @param DeviceRepository      $devices
     * @param VenueRepository       $venues
     * @param DeviceModelRepository $deviceModels
     */
    public function __construct(DeviceRepository $devices, VenueRepository $venues, DeviceModelRepository $deviceModels)
    {
        $this->devices = $devices;
        $this->venues = $venues;
        $this->deviceModels = $deviceModels;
    }

    /**
     * Show list of devices
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = trimmed_request();

        if ($request->isMethod('post')) {
            if ($request->input('device_action') == 'delete') {
                return $this->deleteSelected($request);
            } else {
                return $this->restoreSelected($request);
            }
        }

        view()->share([
            'models'              => $this->devices->getAllPaginated($request),
            'deviceModels'        => $this->deviceModels->getAllModels(),
            'locations'           => $this->venues->getList(),
            'deviceStatusOptions' => $this->devices->getStatusOptions(),
        ]);

        Scripts::view('admin.device.scripts');

        return $this->render('admin.device.index');
    }

    /**
     * Show the device add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->devices->makeNew());
    }

    /**
     * Show the edit page for the given device
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->form($this->devices->findByHash($id));
    }

    /**
     * Show device form
     *
     * @param \App\Models\Device $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(Device $model)
    {
        $deviceTypes = config('main.device_types');
        $venues = $this->venues->getNameList();
        $properties = $this->venues->getList($venues, true);
        $deviceModels = $this->deviceModels->getAllModels();
        $model->property_id = hashids_encode($model->property_id);

        return $this->render('admin.device.form', compact('model', 'deviceTypes', 'properties', 'deviceModels'));
    }

    /**
     * Create or update a device
     *
     * @param DeviceFormRequest $request
     * @param null|string       $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function save(DeviceFormRequest $request, $id = null)
    {
        if ($model = $this->devices->saveByHash($request->all(), $id)) {

            $result = (new RadiusCommunicator)
                ->driver(config('main.radius_server'))
                ->updateDevice($model);

            if (isset($result->Success) && $result->Success) {
                session()->flash('success', trans('device.saved'));
                \Log::info('Device synchronization is successful', ['response' => $result, 'device' => $model]);
            } else {
                session()->flash('error', trans('device.sync_error'));
                AlertMessage::high('Device synchronization failed', ['response' => $result, 'device' => $model]);

                return redirect()->route('admin.device.edit', $model);
            }
        }

        return redirect()->route('admin.device.index');
    }

    /**
     * Delete the given device
     *
     * @param string $value
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function delete($value)
    {
        if ($this->devices->deleteByHash($value)) {
            session()->flash('success', trans('device.deleted'));
        } else {
            session()->flash('error', trans('device.not_deleted'));
        }

        return redirect()->back();
    }

    /**
     * Restore a deleted device
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if ($this->devices->restoreByHash($id)) {
            session()->flash('success', trans('device.restored'));
        } else {
            session()->flash('error', trans('device.not_restored'));
        }

        return redirect()->back();
    }

    /**
     * Delete the selected devices
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSelected(Request $request)
    {
        if ($request->has('devices')) {
            try {
                $count = $this->devices->destroy($request->input('devices'));
                session()->flash('success', trans('device.deleted_options.count', ['count' => $count]));
            } catch (\Exception $e) {
                session()->flash('error', trans('device.not_deleted'));
                \Log::error('Error on deleting devices: ', ['message' => $e->getMessage()]);
            }
        }

        return redirect()->back();
    }

    /**
     * Restore the selected devices
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function restoreSelected(Request $request)
    {
        if ($request->has('devices')) {
            try {
                $count = $this->devices->restoreSelected($request->input('devices'));
                session()->flash('success', trans('device.restored_options.count', ['count' => $count]));
            } catch (\Exception $e) {
                session()->flash('error', trans('device.not_restored'));
                \Log::error('Error on restoring devices: ', ['message' => $e->getMessage()]);
            }
        }

        return redirect()->back();
    }
}
