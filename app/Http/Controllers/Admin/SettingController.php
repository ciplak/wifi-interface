<?php
namespace App\Http\Controllers\Admin;

use App\Date;
use App\Models\AccessCode;
use App\Repositories\SettingRepository;
use App\Tab;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SettingController extends AdminController
{
    /**
     * @var SettingRepository
     */
    private $settings;

    /**
     * SettingController constructor.
     *
     * @param SettingRepository $settings
     */
    public function __construct(SettingRepository $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Show setting page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabs = [
            new Tab('General', 'general', 'info-circle'),
            new Tab('E-mail', 'email', 'envelope'),
            new Tab('Social', 'social', 'facebook'),
            new Tab('SMS', 'sms', 'commenting'),
        ];

        if (\Entrust::can('settings.advanced')) {
            $tabs[] = new Tab('Advanced', 'advanced', 'cogs');
        }

        $timezones = \DateTimeZone::listIdentifiers();

        view()->share(
            [
                'settings'                => \Setting::raw(),
                'timezones'               => array_combine($timezones, $timezones),
                'dateFormats'             => [
                    Date::DATE_FORMAT_YMD => Carbon::now()->format('Y-m-d') . ' (Y-M-D)',
                    Date::DATE_FORMAT_DMY => Carbon::now()->format('d-m-Y') . ' (D-M-Y)',
                    Date::DATE_FORMAT_MDY => Carbon::now()->format('m-d-Y') . ' (M-D-Y)',
                ],
                'timeFormats'             => [
                    Date::TIME_FORMAT_12_HOURS => Carbon::now(setting('timezone'))->format('g:i A'),
                    Date::TIME_FORMAT_24_HOURS => Carbon::now(setting('timezone'))->format('H:i'),
                ],
                'days'                    => Date::getDays(),
                'accessCodeTypes'         => [
                    AccessCode::USAGE_TYPE_ONE_TIME => 'One time',
                    AccessCode::USAGE_TYPE_REUSABLE => 'Reusable',
                ],
                'accessCodeChars'         => [
                    AccessCode::CHARS_NUMERIC      => 'Numeric',
                    AccessCode::CHARS_ALPHANUMERIC => 'Alphanumeric',
                ],
                'accessCodeExpiryOptions' => AccessCode::getDefaultExpiredPeriodOptions(),
            ]
        );

        $tab = request('tab', 'general');

        if ($tab == 'advanced' && !\Entrust::can('settings.advanced')) {
            return redirect()->route('admin.setting.index');
        }

        return $this->render('admin.setting.index', [
            'tabs'         => $tabs,
            'activeTab'    => $tabs[0],
            'activeTabKey' => $tab,
        ]);
    }

    /**
     * Save settings
     *
     * @return \Illuminate\Http\Response
     */
    public function save()
    {
        $request = trimmed_request();
        $tab = $request->input('tab', 'general');

        if ($tab == 'advanced' && !\Entrust::can('settings.advanced')) {
            return redirect()->route('admin.setting.index');
        }

        $tab = studly_case($tab);

        if (method_exists($this, 'tab' . $tab) && is_callable([$this, 'tab' . $tab])) {
            call_user_func([$this, 'tab' . $tab]);
        } else {
            setting(request('settings'))->save();
        }

        session()->flash('success', trans('settings.saved'));

        return redirect()->back()->withInput();
    }

    /**
     * Handle general tab settings
     */
    public function tabGeneral()
    {
        $request = trimmed_request();

        $data = $request->input('settings');

        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');

            if ($logo->isValid()) {
                $miniLogo = \Image::make($logo)
                    ->resize(
                        null, 40, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    }
                    )->encode('png');

                $filename = $logo->getFilename() . '.jpg';

                \Storage::disk()->put(assets_base('img/' . $filename), $miniLogo->__toString());

                $data['logo'] = $filename;
            } else {
                session()->flash('error', 'invalid file');
            }
        }

        $dateFormatOptions = Date::getDateFormatOptions();
        $dateFormat = $data['date_format'];

        $timeFormatOptions = Date::getTimeFormatOptions();
        $timeFormat = $data['time_format'];

        $data['date_format'] = $dateFormatOptions[$dateFormat] ?? 'Y-m-d';
        $data['time_format'] = $timeFormatOptions[$timeFormat] ?? 'H:i';
        $data['datetime_format'] = $data['date_format'] . ' ' . $data['time_format'];
        $data['shared_device_count'] = (int)$data['shared_device_count'];

        setting($data)->save();
    }

    public function globalSettings()
    {
        $settings = $this->settings->getAll();

        return $this->render(
            'admin.setting.tabs.global', [
                'settings' => $settings,
            ]
        );
    }

    public function saveGlobalSettings(Request $request)
    {
        $keys = $this->settings->getKeys();

        $insertList = $request->input('settings');

        $updateList = [];
        foreach ($keys as $key) {
            if (isset($insertList[$key])) {
                $updateList[$key] = $insertList[$key];
            }

            unset($insertList[$key]);
        }

        foreach ($updateList as $name => $value) {
            $this->settings->set($name, $value);
        }

        if ($insertList) {
            $data = [];
            foreach ($insertList as $name => $value) {
                $data[] = ['tenant_id' => null, 'name' => $name, 'value' => $value];
            }

            \DB::table('settings')->insert($data);
        }

        // clear cache for global setting
        cache()->store('file')->forget('global-settings');

        return redirect()->route('admin.setting.global');
    }
}