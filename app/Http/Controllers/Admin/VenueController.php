<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VenueFormRequest;
use App\Models\Content;
use App\Models\SplashPage;
use App\Models\Property;
use App\Models\Venue;
use App\Repositories\SplashPageRepository;
use App\Repositories\ServiceProfileRepository;
use App\Repositories\SmsProviderRepository;
use App\Repositories\VenueRepository;
use App\Services\Scripts;

class VenueController extends AdminController
{
    /**
     * @var ServiceProfileRepository
     */
    private $serviceProfiles;

    /**
     * @var SmsProviderRepository
     */
    private $smsProviders;

    /**
     * @var SplashPageRepository
     */
    private $splashPages;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * VenueController constructor.
     *
     * @param VenueRepository          $venues
     * @param ServiceProfileRepository $serviceProfiles
     * @param SmsProviderRepository    $smsProviders
     * @param SplashPageRepository     $splashPages
     */
    public function __construct(VenueRepository $venues, ServiceProfileRepository $serviceProfiles, SmsProviderRepository $smsProviders, SplashPageRepository $splashPages)
    {
        $this->venues = $venues;
        $this->serviceProfiles = $serviceProfiles;
        $this->smsProviders = $smsProviders;
        $this->splashPages = $splashPages;
    }

    /**
     * Show list of venues
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render(
            'admin.venue.index', [
            'models' => $this->venues->getAll(),
            ]
        );
    }

    /**
     * Show the venue add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->venues->makeNew());
    }

    /**
     * Show venue form
     *
     * @param Venue $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(Venue $model)
    {
        \Auth::user()->canManageVenue($model);

        $serviceProfiles = $model->serviceProfiles;

        view()->share(
            [
            'serviceProfiles'                => $serviceProfiles,
            'smartLoginOptions'              => Property::getSmartLoginOptions(),
            'smartLoginFrequencyTypeOptions' => Property::getSmartLoginFrequencyTypeOptions(),
            'countries'                      => Property::getCountries(),

            'model'              => $model,
            'smsProviders'       => $this->smsProviders->getList(),
            'loginPages'         => $this->splashPages->getPublishedParentList(),
            'postLoginPages'     => $this->splashPages->getPublishedParentList(SplashPage::TYPE_ONLINE),
            'allServiceProfiles' => $this->serviceProfiles->getList(),
            ]
        );

        Scripts::view('admin.venue.scripts');
        Scripts::url('assets/js/ckeditor/ckeditor.js');

        return $this->render('admin.venue.form');
    }

    /**
     * Show the edit page for the given venue
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->venues->findByHash($id);

        return $this->form($model);
    }

    /**
     * Delete the given venue
     *
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $status = false;

        try {
            $status = $this->venues->deleteByHash($id);
        } catch (\Exception $e) {
            \Log::error('Error on deleting venue: ', ['message' => $e->getMessage()]);
        }

        $message = $status ? trans('venue.deleted') : trans('venue.not_deleted');

        session()->flash($status ? 'success' : 'error', $message);

        return $status ? redirect()->route('admin.venue.index') : redirect()->back();
    }

    /**
     * Create or update a venue
     *
     * @param VenueFormRequest $request
     * @param string|null      $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(VenueFormRequest $request, $id = null)
    {
        /**
 * @var Venue $model 
*/
        if ($model = $this->venues->saveByHash($request->all(), $id)) {

            if ($model->wasRecentlyCreated) {

                /// create default zone for the venue
                \App\Models\Zone::create(
                    [
                    'property_type_id' => Property::TYPE_ZONE,
                    'parent_id'        => $model->id,
                    'name'             => 'Default Zone for ' . $model->name,
                    ]
                );

                session()->flash('success', trans('venue.created'));
            } else {
                session()->flash('success', trans('venue.saved'));
            }

            return redirect()->route('admin.venue.index');

        } else {
            session()->flash('error', trans('venue.not_saved'));
        }

        return redirect()->back();
    }

}
