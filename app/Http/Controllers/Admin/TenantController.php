<?php

namespace App\Http\Controllers\Admin;

use App\Events\Tenant\Created;
use App\Events\Tenant\Updated;
use App\Http\Requests\TenantFormRequest;
use App\Models\Property;
use App\Models\SplashPage;
use App\Models\Tenant;
use App\Repositories\ServiceProfileRepository;
use App\Repositories\SettingRepository;
use App\Repositories\SmsLogRepository;
use App\Repositories\SmsProviderRepository;
use App\Repositories\SplashPageRepository;
use Carbon\Carbon;

/**
 * Class TenantController
 */
class TenantController extends AdminController
{
    /**
     * @var ServiceProfileRepository
     */
    private $serviceProfiles;

    /**
     * @var SmsProviderRepository
     */
    private $smsProviders;

    /**
     * @var SplashPageRepository
     */
    private $splashPages;

    /**
     * @var SmsLogRepository
     */
    private $smsLogs;

    /**
     * @var SettingRepository
     */
    private $settings;

    /**
     * TenantController constructor.
     *
     * @param ServiceProfileRepository $serviceProfiles
     * @param SmsProviderRepository    $smsProviders
     * @param SplashPageRepository     $splashPages
     * @param SmsLogRepository         $smsLogs
     * @param SettingRepository        $settings
     */
    public function __construct(ServiceProfileRepository $serviceProfiles, SmsProviderRepository $smsProviders, SplashPageRepository $splashPages, SmsLogRepository $smsLogs, SettingRepository $settings)
    {
        $this->serviceProfiles = $serviceProfiles;
        $this->smsProviders = $smsProviders;
        $this->splashPages = $splashPages;
        $this->smsLogs = $smsLogs;
        $this->settings = $settings;
    }

    /**
     * Show list of tenants
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('admin.tenant.index', [
            'models' => Tenant::orderBy('name')->paginate(config('main.per_page')),
        ]);
    }

    /**
     * Show the tenant add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form(new Tenant);
    }

    /**
     * Show the edit page for the given tenant
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
         * @var Tenant $model
         */
        $model = Tenant::findOrFail($id);

        if ($model) {
            $model->total_sms_credits = $this->settings->getSmsCredits($model->id);
            $model->average_daily_sms_usage = $this->smsLogs->getAverageDailySmsUsage($model->id);

            $smsUsageData = $this->smsLogs->getDailySmsUsageForLastMonth($model->id);

            if ($smsUsageData) {
                $smsUsageDates = $smsUsageData->keys();

                $begin = $smsUsageDates->isEmpty()
                    ? Carbon::today()
                    : Carbon::createFromFormat('Y-m-d', $smsUsageDates->get(0));

                $end = Carbon::today()->addDay(1);
                $interval = new \DateInterval('P1D');
                $dateRange = new \DatePeriod($begin, $interval, $end);
                $dateArray = [];

                foreach ($dateRange as $date) {
                    $dateArray[$date->tz(setting('timezone'))->format(setting('date_format'))] = 0;
                }

                $smsUsageData->each(
                    function ($value, $key) use (&$dateArray) {
                        $date = Carbon::createFromFormat('Y-m-d', $key)->tz(setting('timezone'))->format(setting('date_format'));
                        $dateArray[$date] = (int)$value;
                    }
                );

                $smsUsageData = collect($dateArray);

                $model->smsUsageChartData = $smsUsageData->values()->toJson();
                $model->smsUsageChartCategories = $smsUsageData->keys()->toJson();
            }
        }


        return $this->form($model);
    }

    /**
     * Show tenant form
     *
     * @param Tenant $model
     *
     * @return \Illuminate\Http\Response
     */
    private function form(Tenant $model)
    {
        $credits = $this->smsLogs->getInstalledSmsCreditList($model->id);

        view()->share([
            'model'                          => $model,
            'serviceProfileModels'           => $this->serviceProfiles->getModelOptions(),
            'serviceProfiles'                => $model->serviceProfiles,
            'smartLoginOptions'              => Property::getSmartLoginOptions(),
            'smartLoginFrequencyTypeOptions' => Property::getSmartLoginFrequencyTypeOptions(),
            'countries'                      => Property::getCountries(),
            'smsProviders'                   => $this->smsProviders->getList(),
            'preLoginPages'                  => $this->splashPages->getPublishedParentList(),
            'postLoginPages'                 => $this->splashPages->getPublishedParentList(SplashPage::TYPE_ONLINE),
            'allServiceProfiles'             => $this->serviceProfiles->getList(),
            'credits'                        => $credits,
        ]);

        return $this->render('admin.tenant.form');
    }

    /**
     * Create or update a tenant
     *
     * @param TenantFormRequest $request
     * @param int|null          $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(TenantFormRequest $request, $id = null)
    {
        /**
         * @var Tenant $model
         */
        $model = Tenant::findOrNew($id);

        $model->fill($request->all());

        if ($model->save()) {
            event($model->wasRecentlyCreated ? new Created($model) : new Updated($model));

            session()->flash('success', trans('tenant.saved'));

            return redirect()->route('admin.tenant.index');
        }
    }

    /**
     * Show tenant profile form
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $model = \Auth::user()->tenant;
        view()->share([
            'model'                          => $model,
            'serviceProfileModels'           => $this->serviceProfiles->getModelOptions(),
            'serviceProfiles'                => $model->serviceProfiles,
            'smartLoginOptions'              => Property::getSmartLoginOptions(),
            'smartLoginFrequencyTypeOptions' => Property::getSmartLoginFrequencyTypeOptions(),
            'countries'                      => Property::getCountries(),
            'smsProviders'                   => $this->smsProviders->getList(),
            'preLoginPages'                  => $this->splashPages->getPublishedParentList(),
            'postLoginPages'                 => $this->splashPages->getPublishedParentList(SplashPage::TYPE_ONLINE),
            'allServiceProfiles'             => $this->serviceProfiles->getList(),
        ]);

        return $this->render('admin.tenant.profile');
    }

    /**
     * @param $tenantId
     * @param $id
     *
     * @return string
     */
    public function deleteCreditHistory($tenantId, $id)
    {
        $data = ['status' => false];

        $record = $this->smsLogs->findHistoryRecord($tenantId, $id);

        if ($record) {
            $credits = $record->data->get('credit');

            $this->smsLogs->updateTotalSmsCredits($tenantId, -$credits);
            $this->smsLogs->deleteCredit($tenantId, $id);

            $totalSmsCredits = $this->settings->getSmsCredits($tenantId);

            $data['status'] = true;
            $data['message'] = trans('tenant.sms_credits.deleted');
            $data['totalSmsCredits'] = $totalSmsCredits;
        } else {
            $data['message'] = trans('tenant.sms_credits.not_deleted');
        }

        return response()->json($data);
    }
}