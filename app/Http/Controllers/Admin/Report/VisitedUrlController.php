<?php
namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Admin\AdminController;
use App\Models\GuestDevice;
use App\Models\VisitedUrl;
use App\Services\ListReportExporter;
use App\Transformers\VisitedUrlLogReportTransformer;
use Illuminate\Http\Request;

class VisitedUrlController extends AdminController
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        view()->share([
            'deviceTypes' => GuestDevice::getDeviceTypes(),
        ]);

        return $this->render('admin.report.visited_urls.index', [
            'models' => VisitedUrl::filter()->paginate(config('main.per_page')),
        ]);
    }

    /**
     * Export data
     */
    public function export()
    {
        set_time_limit(0);
        ini_set('memory_limit', '3G');

        $models = VisitedUrl::filter()->get();

        $data = fractal()
            ->collection($models, new VisitedUrlLogReportTransformer)
            ->toArray();

        $exporter = new ListReportExporter;
        $exporter->format(request('format'))
            ->data($data)
            ->titles([
                trans('visited_url.columns.ap_mac'),
                trans('visited_url.columns.client_mac'),
                trans('visited_url.columns.source_ip'),
                trans('visited_url.columns.destination_ip'),
                trans('impression.columns.device_type_id'),
                trans('impression.columns.description'),
                trans('impression.columns.platform'),
                trans('impression.columns.browser'),
                trans('visited_url.columns.url'),
                trans('visited_url.columns.visited_at'),
            ])
            ->sheetTitle(trans('visited_url.page_title'))
            ->export('visited urls');
    }
}