<?php
namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Admin\AdminController;
use App\Repositories\VenueRepository;
use App\Repositories\WifiSessionRepository;
use Illuminate\Http\Request;

class LoyaltyController extends AdminController
{
    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * @var WifiSessionRepository
     */
    private $wifiSessions;

    /**
     * LoyaltyController constructor.
     *
     * @param VenueRepository       $venues
     * @param WifiSessionRepository $wifiSessions
     */
    public function __construct(VenueRepository $venues, WifiSessionRepository $wifiSessions)
    {
        $this->venues = $venues;
        $this->wifiSessions = $wifiSessions;
    }

    /**
     * Show visitor loyalty chart
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->wifiSessions->getLoyaltyData($request);

        view()->share(
            [
            'locations' => $this->venues->getNameList(),
            'data'      => $this->transformData($data)
            ]
        );

        return $this->render('admin.report.loyalty.index');
    }

    /**
     * Transform data for the chart
     *
     * @param array $items
     *
     * @return array
     */
    private function transformData($items)
    {
        $totals = array_fill(0, 4, 0);

        foreach ($items as $item) {

            switch (true) {
            case $item >= 2 && $item < 5:
                $range = 1;
                break;

            case $item >= 5 && $item < 10:
                $range = 2;
                break;

            case $item >= 10:
                $range = 3;
                break;

            default:
                $range = 0;
            }

            $totals[$range] += $item;
        }

        $categories = [
            trans('report.loyalty.categories.one_time'),
            trans('report.loyalty.categories.2_5_visits'),
            trans('report.loyalty.categories.5_10_visits'),
            trans('report.loyalty.categories.10_plus_visits'),
        ];

        $data = [];
        foreach ($categories as $range => $title) {
            $data[] = [$title, (int)($totals[$range] ?? 0)];
        }

        return $data;
    }
}