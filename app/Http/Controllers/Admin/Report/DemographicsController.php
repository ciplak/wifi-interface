<?php
namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Guest;
use App\Repositories\GuestRepository;
use App\Repositories\VenueRepository;
use Illuminate\Http\Request;

class DemographicsController extends AdminController
{
    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * DemographicsController constructor.
     *
     * @param VenueRepository $venues
     * @param GuestRepository $guests
     */
    public function __construct(VenueRepository $venues, GuestRepository $guests)
    {
        $this->venues = $venues;
        $this->guests = $guests;
    }

    /**
     * Show guest demographic chart
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = $this->guests->getDemographicsData($request);

        $data = $this->transformData($items);

        view()->share([
            'locations' => $this->venues->getNameList(),
        ]);

        return $this->render('admin.report.demographics.index', [
            'data'      => $data,
            'ageRanges' => $this->guests->getAgeRanges(),
        ]);
    }

    /**
     * Transform data for the chart
     *
     * @param array $items
     *
     * @return array
     */
    private function transformData($items)
    {
        $data = [];

        $ageRangeCount = count($this->guests->getAgeRanges());
        $genderOptions = $this->guests->getGenderOptions();

        foreach ($items as $item) {
            $values = isset($data[$item->gender]['data'])
                ? $data[$item->gender]['data']
                : array_fill(0, $ageRangeCount, 0);

            $values[is_null($item->age_range) ? Guest::AGE_RANGE_UNKNOWN : $item->age_range] = (int)$item->total;

            $data[$item->gender] = [
                'name' => $genderOptions[$item->gender] ?? trans('guest.gender.unknown'),
                'data' => $values,
            ];
        }

        if (isset($data[Guest::GENDER_UNKNOWN])) {
            $data[Guest::GENDER_UNKNOWN]['color'] = '#888';
        }

        if (isset($data[Guest::GENDER_MALE])) {
            $data[Guest::GENDER_MALE]['color'] = '#3278fe';
        }

        if (isset($data[Guest::GENDER_FEMALE])) {
            $data[Guest::GENDER_FEMALE]['color'] = '#e03078';
        }

        return $data;
    }

}