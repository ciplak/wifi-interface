<?php
namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Admin\AdminController;
use App\Repositories\VenueRepository;
use App\Repositories\WifiSessionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PresenceController extends AdminController
{
    const TYPE_WEEK = 0;
    const TYPE_MONTH = 1;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * @var WifiSessionRepository
     */
    private $wifiSessions;

    /**
     * PresenceController constructor.
     *
     * @param VenueRepository       $venues
     * @param WifiSessionRepository $wifiSessions
     */
    public function __construct(VenueRepository $venues, WifiSessionRepository $wifiSessions)
    {
        $this->venues = $venues;
        $this->wifiSessions = $wifiSessions;
    }

    /**
     * Show presence report
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->input('type', static::TYPE_WEEK);

        $data = $this->wifiSessions->getPresenceData($request, $type);

        view()->share(
            [
            'locations' => $this->venues->getNameList(),
            'values'    => $data->values()->implode(','),
            'xAxis'     => $this->generateXAxis($type, $data),
            ]
        );

        return $this->render('admin.report.presence.index');
    }

    /**
     * @param int                            $type
     * @param \Illuminate\Support\Collection $data
     *
     * @return \Illuminate\Support\Collection
     */
    private function generateXAxis($type, $data)
    {
        $format = $type == static::TYPE_WEEK ? 'l, F jS' : 'F jS';

        $xAxis = $data->keys()->map(
            function ($item) use ($format) {
                return Carbon::createFromFormat('Y-m-d', $item)->format($format);
            }
        );

        return $xAxis;
    }
}