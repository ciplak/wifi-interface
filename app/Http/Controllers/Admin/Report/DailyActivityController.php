<?php
namespace App\Http\Controllers\Admin\Report;

use App\Date;
use App\Http\Controllers\Admin\AdminController;
use App\Repositories\VenueRepository;
use App\Repositories\WifiSessionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DailyActivityController extends AdminController
{
    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * @var WifiSessionRepository
     */
    private $wifiSessions;

    /**
     * PresenceController constructor.
     *
     * @param VenueRepository       $venues
     * @param WifiSessionRepository $wifiSessions
     */
    public function __construct(VenueRepository $venues, WifiSessionRepository $wifiSessions)
    {
        $this->venues = $venues;
        $this->wifiSessions = $wifiSessions;
    }

    /**
     * Show daily activity chart
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('date')) {
            $start = Carbon::createFromFormat('Y-m-d', $request->input('date'))->startOfDay();
        } else {
            $start = Carbon::today();
        }

        $interval = Date::getRanges(Date::RANGE_INTERVAL, $start->format('Y-m-d'), $start->format('Y-m-d'));

        $data = $this->wifiSessions->getDailyActivity(activeTenantId(), $request->input('location'), $interval);

        view()->share([
            'locations' => $this->venues->getNameList(),
            'data'      => json_encode($data),
        ]);

        return $this->render('admin.report.daily_activity.index');
    }
}