<?php
namespace App\Http\Controllers\Admin\Report;

use App\ConnectionStatus;
use App\Http\Controllers\Admin\AdminController;
use App\Reports\GuestSessions;
use App\Repositories\GuestDeviceRepository;
use App\Repositories\GuestRepository;
use App\Repositories\VenueRepository;
use App\Services\ListReportExporter;
use App\Transformers\GuestSessionsReportTransformer;
use Illuminate\Http\Request;

class GuestSessionsController extends AdminController
{
    /**
     * @var GuestDeviceRepository
     */
    private $guestDevices;

    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * @var GuestSessions
     */
    private $guestSessions;

    /**
     * ReportController constructor.
     *
     * @param GuestRepository       $guests
     * @param GuestDeviceRepository $guestDevices
     * @param VenueRepository       $venues
     */
    public function __construct(GuestRepository $guests, GuestDeviceRepository $guestDevices, VenueRepository $venues, GuestSessions $guestSessions)
    {
        $this->guests = $guests;
        $this->guestDevices = $guestDevices;
        $this->venues = $venues;
        $this->guestSessions = $guestSessions;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        view()->share([
            'connectionStatuses' => ConnectionStatus::labels(),
            'operators'          => $this->guestSessions->getOperators(),
            'durationOperators'  => $this->guestSessions->getOperators(true),
        ]);

        return $this->render('admin.report.guest_sessions.index', [
            'models'   => GuestSessions::filter($request)->paginate(config('main.per_page')),
            'statuses' => ConnectionStatus::labels(),
        ]);
    }

    public function export()
    {
        set_time_limit(0);
        ini_set('memory_limit', '3G');

        $models = GuestSessions::getData();

        $data = fractal()
            ->collection($models, new GuestSessionsReportTransformer)
            ->toArray();

        $exporter = new ListReportExporter;
        $exporter->format(request('format'))
            ->data($data)
            ->titles([
                trans('guest.columns.name'),
                trans('guest.columns.username'),
                trans('guest.columns.mac'),
                trans('guest.columns.ip'),
                trans('guest.columns.access_code'),
                trans('guest.columns.duration'),
                trans('guest.columns.download'),
                trans('guest.columns.upload'),
                trans('guest.columns.connection_status'),
                trans('guest.columns.time'),
            ])
            ->sheetTitle(trans('guest.page_title'))
            ->export('guests_sessions');
    }
}