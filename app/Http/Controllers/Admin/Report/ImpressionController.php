<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Admin\AdminController;
use App\Models\GuestDevice;
use App\Repositories\GuestActivity;
use App\Repositories\GuestActivityRepository;
use App\Repositories\GuestDeviceRepository;
use App\Repositories\GuestRepository;
use App\Services\ListReportExporter;
use App\Transformers\GuestActivityTransformer;
use Illuminate\Http\Request;

class ImpressionController extends AdminController
{
    /**
     * @var GuestDeviceRepository
     */
    private $guestDevices;

    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * @var GuestActivityRepository
     */
    private $guestActivities;

    /**
     * ReportController constructor.
     *
     * @param GuestRepository         $guests
     * @param GuestDeviceRepository   $guestDevices
     * @param GuestActivityRepository $guestActivities
     */
    public function __construct(
        GuestRepository $guests,
        GuestDeviceRepository $guestDevices,
        GuestActivityRepository $guestActivities
    ) {
        $this->guests = $guests;
        $this->guestDevices = $guestDevices;
        $this->guestActivities = $guestActivities;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        view()->share([
            'deviceTypes' => GuestDevice::getDeviceTypes(),
        ]);

        return $this->render('admin.report.impression.index', [
            'models' => \App\Reports\GuestActivity::filter($request)->paginate(config('main.per_page')),
        ]);
    }

    public function export()
    {
        set_time_limit(0);
        ini_set('memory_limit', '3G');

        $models = \App\Reports\GuestActivity::getData();

        $data = fractal()
            ->collection($models, new GuestActivityTransformer())
            ->toArray();

        $exporter = new ListReportExporter;
        $exporter->format(request('format'))
            ->data($data)
            ->titles([
                trans('impression.columns.name'),
                trans('impression.columns.username'),
                trans('impression.columns.device_type_id'),
                trans('impression.columns.description'),
                trans('impression.columns.platform'),
                trans('impression.columns.browser'),
                trans('impression.columns.ip'),
                trans('impression.columns.mac'),
                trans('impression.columns.created_at'),
            ])
            ->sheetTitle(trans('impression.page_title'))
            ->export('impressions');
    }
}