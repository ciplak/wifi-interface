<?php
namespace App\Http\Controllers\Admin\Report;

use App\ConnectionStatus;
use App\Http\Controllers\Admin\AdminController;
use App\Repositories\GuestDeviceRepository;
use App\Repositories\GuestRepository;
use App\Repositories\VenueRepository;
use App\Services\ListReportExporter;
use App\Transformers\GuestReportTransformer;
use Illuminate\Http\Request;

class GuestController extends AdminController
{
    /**
     * @var GuestDeviceRepository
     */
    private $guestDevices;

    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * ReportController constructor.
     *
     * @param GuestRepository       $guests
     * @param GuestDeviceRepository $guestDevices
     * @param VenueRepository       $venues
     */
    public function __construct(GuestRepository $guests, GuestDeviceRepository $guestDevices, VenueRepository $venues)
    {
        $this->guests = $guests;
        $this->guestDevices = $guestDevices;
        $this->venues = $venues;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        view()->share([
            'locations'          => $this->venues->getNameList(),
            'nationalityOptions' => $this->guests->getNationalityOptions(),
            'genderOptions'      => $this->guests->getGenderOptions(),
            'connectionStatuses' => ConnectionStatus::labels(),
        ]);

        return $this->render('admin.report.guest.index', [
            'models'   => \App\Reports\Guest::filter($request)->paginate(config('main.per_page')),
            'statuses' => ConnectionStatus::labels(),
        ]);
    }

    public function export()
    {
        set_time_limit(0);
        ini_set('memory_limit', '3G');

        $models = \App\Reports\Guest::getData();

        $data = fractal()
            ->collection($models, new GuestReportTransformer)
            ->toArray();

        $exporter = new ListReportExporter;
        $exporter->format(request('format'))
            ->data($data)
            ->titles([
                trans('guest.columns.name'),
                trans('guest.columns.email'),
                trans('guest.columns.phone'),
                trans('guest.columns.nationality'),
                trans('guest.columns.passport_no'),
                trans('guest.columns.birthday'),
                trans('guest.columns.gender'),
                trans('guest.columns.last_visit'),
                trans('guest.columns.total_visits'),
            ])
            ->columnFormat([
                'F' => 'dd/mm/yy',
                'J' => '0',
            ])
            ->sheetTitle(trans('guest.page_title'))
            ->export('guests');
    }
}