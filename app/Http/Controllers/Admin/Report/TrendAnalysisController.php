<?php
namespace App\Http\Controllers\Admin\Report;

use App\Date;
use App\Http\Controllers\Admin\AdminController;
use App\Repositories\GuestActivityRepository;
use App\Repositories\VenueRepository;
use App\Repositories\WifiSessionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrendAnalysisController extends AdminController
{
    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * @var WifiSessionRepository
     */
    private $wifiSessions;

    /**
     * @var GuestActivityRepository
     */
    private $guestActivities;

    /**
     * TrendAnalysisController constructor.
     *
     * @param VenueRepository $venues
     * @param WifiSessionRepository $wifiSessions
     * @param GuestActivityRepository $guestActivities
     */
    public function __construct(VenueRepository $venues, WifiSessionRepository $wifiSessions, GuestActivityRepository $guestActivities)
    {
        $this->venues = $venues;
        $this->wifiSessions = $wifiSessions;
        $this->guestActivities = $guestActivities;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $selectedInterval = $request->input('date', Date::RANGE_LAST_WEEK);
        $connections = $this->wifiSessions->getTrendAnalysisConnectionData($request, $selectedInterval);
        $impressions = $this->guestActivities->getImpressions($request, $selectedInterval);

        $dates = array_unique(array_merge(array_keys($connections), array_keys($impressions)));
        asort($dates);
        $data = $this->transformData($connections, $impressions, $dates, $selectedInterval);

        if ($selectedInterval != Date::RANGE_LAST_WEEK) {
            $dates = collect($dates)->map(
                function ($value) use ($request) {
                    $weekNo = (int)$value;
                    $date = explode("-", $request->start_date ?? Carbon::now()->format('Y-m-d'));
                    $year = (int)$date[0];
                    $week_start = new \DateTime();
                    $week_start->setISODate($year, $weekNo);
                    $weekStart = $week_start->format('Y-m-d');
                    $week_start->modify('+6 days');
                    $weekEnd = $week_start->format('Y-m-d');
                    $desiredDates = $weekStart . " - " . $weekEnd;
                    return $desiredDates;
                }
            )->toArray();
        } else {
            if (isset($dates)) {
                $date = $dates[0];
                $date = Carbon::parse($date);
                $dates[max(array_keys($dates))] = $date->addDay(-1)->format('Y-m-d');
            }
        }

        view()->share(
            [
                'locations' => $this->venues->getNameList(),
                'dates'     => implode('", "', $dates),
                'data'      => json_encode($data),
            ]
        );

        return $this->render('admin.report.trend_analysis.index');
    }

    /**
     * @param $connections
     * @param $impressions
     * @param $dates
     * @param $selectedInterval
     *
     * @return array
     */
    private function transformData($connections, $impressions, $dates, $selectedInterval)
    {
        $data = [
            [
                'name' => trans('report.trend_analysis.type.connections'),
                'data' => [],
            ],
            [
                'name' => trans('report.trend_analysis.type.impressions'),
                'data' => [],
            ],
        ];


        foreach ($dates as $date) {
            $data[0]['data'][] = (int)($connections[$date] ?? 0);
            $data[1]['data'][] = (int)($impressions[$date] ?? 0);
        }

        /**/
        if ($selectedInterval != Date::RANGE_LAST_WEEK) {
            $newData = [];

            $month = date('n');

            foreach ($dates as $date) {
                $dateRange = Date::getWeekDateRange($date);

                list($startDate, $endDate) = $dateRange;

                $startDate = Carbon::createFromFormat('Y-m-d', $startDate);
                $endDate = Carbon::createFromFormat('Y-m-d', $endDate);

                if ($endDate->month == $month) {
                    $endDate = $startDate->copy()->endOfMonth();
                } else {
                    if ($startDate->month != $endDate->month) {
                        $startDate = $endDate->copy()->startOfMonth();
                    }
                }

                $key = $startDate->format('F jS');

                if ($startDate->toDateString() != $endDate->toDateString()) {
                    $key .= ' - ' . $endDate->format('F jS');
                }
                $newData[] = $key;
            }
        }/**/

        return $data;
    }
}