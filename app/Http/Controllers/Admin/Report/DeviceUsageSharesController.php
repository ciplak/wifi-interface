<?php
namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Admin\AdminController;
use App\Repositories\GuestDeviceRepository;
use App\Repositories\VenueRepository;
use Illuminate\Http\Request;

class DeviceUsageSharesController extends AdminController
{
    /**
     * @var GuestDeviceRepository
     */
    private $guestDevices;
    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * PresenceController constructor.
     *
     * @param GuestDeviceRepository $guestDevices
     * @param VenueRepository       $venues
     */
    public function __construct(GuestDeviceRepository $guestDevices, VenueRepository $venues)
    {
        $this->guestDevices = $guestDevices;
        $this->venues = $venues;
    }

    /**
     * Show daily activity chart
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $parameters = $request->only(['date']);

        $location = null;
        if (!\Entrust::can('venues.manage-global')) {
            $location = \Auth::auth()->user()->property_id;
        } else {
            $location = hashids_decode($request->input('location'));
        }

        $parameters['location'] = $location;


        $data = $this->guestDevices->getUsageShares(\Auth::user()->tenant_id, $parameters);
        $platformData = $this->guestDevices->getPlatforms(\Auth::user()->tenant_id, $parameters);
        $browserData = $this->guestDevices->getBrowsers(\Auth::user()->tenant_id, $parameters);
        $descriptionData = $this->guestDevices->getDescriptions(\Auth::user()->tenant_id, $parameters);

        view()->share([
            'locations'    => $this->venues->getNameList(),
            'data'         => $this->guestDevices->transformDeviceUsageSharesData($data),
            'platformData' => $this->guestDevices->transformChartData($platformData),
            'browserData'  => $this->guestDevices->transformChartData($browserData),
            'descriptionData' => $this->guestDevices->transformChartData($descriptionData),
        ]);

        return $this->render('admin.report.device_usage_shares.index');
    }
}