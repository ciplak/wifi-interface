<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CampaignFormRequest;
use App\Models\Campaign;
use App\Models\Guest;
use App\Repositories\CampaignRepository;
use App\Repositories\VenueRepository;
use App\Services\Scripts;
use App\Traits\HashId;

class CampaignController extends AdminController
{
    use HashId;
    /**
     * @var CampaignRepository
     */
    private $campaigns;

    /**
     * @var VenueRepository
     */
    private $venues;

    /**
     * CampaignController constructor.
     *
     * @param CampaignRepository $campaigns
     * @param VenueRepository    $venues
     */
    public function __construct(CampaignRepository $campaigns, VenueRepository $venues)
    {
        $this->campaigns = $campaigns;
        $this->venues = $venues;
    }

    /**
     * Show list of campaignRepository
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render(
            'admin.campaign.index', [
            'models'     => $this->campaigns->paginate(config('main.per_page')),
            'repository' => $this->campaigns,
            'locations'  => $this->venues->getList(),
            ]
        );
    }

    /**
     * Show the campaign add page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return $this->form($this->campaigns->makeNew());
    }

    /**
     * Show the edit page for the given campaign
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->campaigns->findByHash($id);

        return $this->form($model);
    }

    /**
     * Show campaign form
     *
     * @param Campaign $model
     *
     * @return \Illuminate\Http\Response
     */
    public function form(Campaign $model)
    {
        Scripts::view('admin.campaign.scripts');

        return $this->render(
            'admin.campaign.form', [
            'model'         => $model,
            'statusOptions' => $this->campaigns->getStatusOptions(),
            'locations'     => $this->venues->getList(),

            'parameterTypeOptions'            => $this->campaigns->getParameterTypeOptions(),
            'periodicTypeOptions'             => $this->campaigns->getPeriodicTypeOptions(),
            'genderTypeOptions'               => [null => trans('guest.gender.all')] + Guest::getGenderOptions(),
            'ageTypeOptions'                  => $this->campaigns->getAgeGroupOptions(),
            'communicationChannelTypeOptions' => $this->campaigns->getCommunicationChannelTypeOptions(),
            ]
        );
    }

    /**
     * @param CampaignFormRequest $request
     * @param null                $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(CampaignFormRequest $request, $id = null)
    {
        if ($this->campaigns->saveByHash($request->all(), $id)) {
            session()->flash('success', trans('campaign.saved'));

            return redirect()->route('admin.campaign.index');
        }
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->campaigns->deleteByHash($id);

        return redirect()->route('admin.campaign.index');
    }
}
