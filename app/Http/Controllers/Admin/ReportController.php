<?php

namespace App\Http\Controllers\Admin;

class ReportController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Response
     */
    public function index()
    {
        return $this->render();
    }

    public function export()
    {
        $svg = request('svg');

        $logoImage = '';

        if ($logo = logo_url()) {
            $logoImage = \HTML::image($logo, null, ['height' => 40]);
        }

        $mpdf = new \mPDF('', 'A4-L');
        $mpdf->SetHTMLFooter(
            '
<table width="100%" style="border-top: 1px solid #000; padding-top: 3mm; vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000">
<tr>
<td width="33%">' . $logoImage . '</td>
<td valign="middle" width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
<td valign="middle" width="33%" style="text-align: right; ">{DATE d-m-Y}</td>
</tr>
</table>'
        );


        $html = <<<html
<html>
<body>
$svg
</body>
</html>
html;

        $mpdf->WriteHTML($html);
        $mpdf->Output('Report.pdf', 'D');
    }
}
