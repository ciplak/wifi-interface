<?php
namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

/**
 * Class ViewController
 *
 * @author Burak ŞAHİN <buraksh@gmail.com>
 */
abstract class ViewController extends Controller
{
    /**
     * The layout that should be used for responses.
     */
    public $layout = 'layouts.default';

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = view($this->layout);
        }
    }

    /**
     * Execute an action on the controller.
     *
     * @param  string $method
     * @param  array  $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        $this->setupLayout();

        return parent::callAction($method, $parameters);
    }


    /**
     * Renders view
     *
     * @param  string $view
     * @param  array  $data
     * @return \Illuminate\Http\Response
     */
    protected function render($view = '', $data = [], $partial = false)
    {

        $content = view($view, $data)->render();
        if ($partial) { return $content;
        }

        $this->layout->layout_content = $content;

        return $this->layout;
    }
}