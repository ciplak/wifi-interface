<?php

namespace App\Http\Controllers;

use App\Exceptions\DeviceNotFoundException;
use App\Models\Property;
use App\Models\SplashPage;
use App\Repositories\AccessCodeRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\GuestDeviceRepository;
use App\Repositories\GuestRepository;
use App\Repositories\SplashPageRepository;
use App\Repositories\WifiSessionRepository;

/**
 * Class FrontController
 *
 * @package App\Http\Controllers
 */
abstract class FrontController extends Controller
{
    /**
     * @var SplashPageRepository
     */
    protected $splashPages;

    /**
     * @var GuestDeviceRepository
     */
    protected $guestDevices;

    /**
     * @var GuestRepository
     */
    protected $guests;

    /**
     * @var WifiSessionRepository
     */
    protected $wifiSessions;

    /**
     * @var DeviceRepository
     */
    protected $devices;

    /**
     * @var AccessCodeRepository
     */
    protected $accessCodes;

    /**
     * @var \App\Models\GuestDevice
     */
    protected $currentGuestDevice;

    /**
     * @var \App\Models\Device
     */
    protected $currentAccessPoint;

    /**
     * @var \App\Models\Property
     */
    protected $location;

    /**
     * @var SplashPage
     */
    protected $splashPage;

    /**
     * @var int
     */
    protected $tenantId;

    /**
     * @var \App\Models\Venue
     */
    protected $venue;

    /**
     * FrontController constructor.
     *
     * @param SplashPageRepository  $splashPages
     * @param GuestRepository       $guests
     * @param GuestDeviceRepository $guestDevices
     * @param WifiSessionRepository $wifiSessions
     * @param DeviceRepository      $devices
     * @param AccessCodeRepository  $accessCodes
     */
    function __construct(SplashPageRepository $splashPages, GuestRepository $guests, GuestDeviceRepository $guestDevices, WifiSessionRepository $wifiSessions, DeviceRepository $devices, AccessCodeRepository $accessCodes)
    {
        // repositories
        $this->splashPages = $splashPages;
        $this->guestDevices = $guestDevices;
        $this->guests = $guests;
        $this->wifiSessions = $wifiSessions;
        $this->devices = $devices;
        $this->accessCodes = $accessCodes;

        // to access session info we need to use a closure based middleware
        $this->middleware(
            function($request, $next) {
                $this->bootstrap();

                return $next($request);
            }
        );
    }

    private function bootstrap()
    {
        try {
            $this->currentAccessPoint = $this->devices->getCurrentDeviceByMac(session('ap_mac'));
        } catch (DeviceNotFoundException $e) {

            // if single location enabled, get mac address of the first device
            if (env('SINGLE_LOCATION', false)) {
                $this->currentAccessPoint = $this->devices->getDefaultDevice();

                if ($this->currentAccessPoint) {
                    session(['ap_mac' => $this->currentAccessPoint->mac]);
                }
            }

            if (!$this->currentAccessPoint) {
                throw new DeviceNotFoundException('');
            }
        }

        $this->tenantId = (int)$this->currentAccessPoint->tenant_id;
        $this->location = $this->currentAccessPoint->property;
        $this->venue = $this->location->property_type_id == Property::TYPE_ZONE ? $this->location->parent : $this->location;

        session([
            'front_page_tenant_id'   => $this->tenantId,
            'front_page_venue_id'    => $this->venue->id,
            'front_page_location_id' => $this->location->id,
        ]);

        log_info(
            'front page tenant_id', [
                'tenant id'         => $this->tenantId,
                'current device id' => $this->currentAccessPoint->id,
            ]
        );

        load_settings(session('front_page_tenant_id'));

        $this->currentGuestDevice = $this->guestDevices->findOrCreateByMac(session('client_mac'));
        $this->guestDevices->updateDeviceInfo($this->currentGuestDevice);

        $this->splashPage = $this->location->getSplashPage(
            \App::getLocale(),
            $this->currentGuestDevice->is_connected,
            session('ssid_name')
        );

        view()->share('splashPage', $this->splashPage);
    }
}