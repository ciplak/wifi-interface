<?php
namespace App\Http\Controllers\Api;

use App\Repositories\AlertMessageRepository;
use Illuminate\Http\Request;

/**
 * Class NotificationController
 */
class NotificationController extends ApiController
{
    /**
     * @var AlertMessageRepository
     */
    private $alertMessages;

    public function __construct(AlertMessageRepository $alertMessages)
    {
        $this->alertMessages = $alertMessages;
    }

    /**
     * Get list of all activity log
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function recent(Request $request)
    {
        $alertData = \Cache::remember(
            'alertData', 5, function () {
                $alertCount = $this->alertMessages->getCount();;
                $latestAlerts = $this->alertMessages->getLatest(5);

                return compact('alertCount', 'latestAlerts');
            }
        );

        return response()->json(
            [
            'count'   => $alertData['alertCount'],
            'items' => $alertData['latestAlerts'],
            ]
        );
    }

}

