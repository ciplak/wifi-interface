<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\TenantProfileFormRequest;
use App\Models\SmsLog;
use App\Models\Tenant;
use App\Repositories\SettingRepository;
use App\Repositories\SmsLogRepository;
use Carbon\Carbon;

/**
 * Class TenantController
 */
class TenantController extends ApiController
{
    /**
     * @var SmsLogRepository
     */
    private $smsLogs;

    /**
     * @var SettingRepository
     */
    private $settings;

    /**
     * TenantController constructor.
     *
     * @param SmsLogRepository  $smsLogs
     * @param SettingRepository $settings
     */
    public function __construct(SmsLogRepository $smsLogs, SettingRepository $settings)
    {
        $this->smsLogs = $smsLogs;
        $this->settings = $settings;
    }

    /**
     * Get tenant profile
     *
     * @return \Illuminate\Http\Response
     */
    public function getProfile()
    {
        $tenantId = (int)$this->user()->tenant_id;

        $monthlySmsUsage = $this->smsLogs->getDailySmsUsageForLastMonth($tenantId);
        $monthlySmsCount = $monthlySmsUsage->sum();
        $monthlySmsFirst = $monthlySmsUsage->keys()->first();
        $totalDateCount = $monthlySmsFirst ? Carbon::now()->diffInDays(Carbon::createFromFormat('Y-m-d', $monthlySmsFirst)) : 0;

        $tenant = Tenant::where('id', $tenantId)->first();
        $tenant->total_sms_credits = $this->settings->getSmsCredits($tenantId);
        $tenant->average_daily_sms_usage = $totalDateCount ? ceil($monthlySmsCount / $totalDateCount) : 0;

        // todo optimize
        $smsCredits = $this->smsLogs->getInstalledSmsCreditList($tenant->id);
        $tenant->sms_credits = $smsCredits->isEmpty() ? [] : $smsCredits->map(
            function (SmsLog $item) {
                $data = $item->data;
                $data['created_at'] = $item->created_at->tz(setting('timezone'))->format(setting('datetime_format'));

                return $data;
            }
        );

        return $tenant;
    }

    /**
     * Update tenant profile
     *
     * @param TenantProfileFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(TenantProfileFormRequest $request)
    {
        // @todo use repository
        $model = $this->user()->tenant;

        $model->fill($request->all());

        $status = $model->save();

        return response()->json(['status' => $status], $status ? 200 : 422);
    }
}

