<?php
namespace App\Http\Controllers\Api;

use App\Models\ActivityLog;
use Illuminate\Http\Request;

/**
 * Class ActivityLogController
 */
class ActivityLogController extends ApiController
{

    /**
     * Get list of all activity log
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(Request $request)
    {
        $query = \Entrust::can('activity-logs.view-global')
            ? ActivityLog::allTenants()
            : ActivityLog::query();

        return $query->with(['tenant', 'user'])
            ->orderBy('id', 'DESC')
            ->paginate($request->input('paginate'));
    }

}

