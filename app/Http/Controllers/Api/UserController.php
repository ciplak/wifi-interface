<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\UserProfileFormRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;

/**
 * Class UserController
 */
class UserController extends ApiController
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * UserController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Get User profile
     *
     * @return JsonResponse
     */
    public function profile()
    {
        return \Auth::user();
    }

    /**
     * Update the user's profile information
     *
     * @param UserProfileFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(UserProfileFormRequest $request)
    {
        $status = $this->users->updateProfile($request->all());

        return response()->json(['status' => $status], $status ? 200 : 422);
    }
}

