<?php
namespace App\Http\Controllers\Api;

use App\Models\Device;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CmxController extends ApiController
{
    /**
     * Validate
     *
     * @param Request $request
     *
     * @return string
     */
    public function index(Request $request)
    {
        if (\App::isLocal()) {
            \Debugbar::disable();
        }

        return $request->input('validator');
    }

    /**
     * Store CMX data
     *
     * @param Request $request
     *
     * @return array
     */
    public function store(Request $request)
    {
        if (\App::isLocal()) {
            \Debugbar::disable();
        }

        $now = Carbon::now();
        $item = json_decode($request->getContent());

        $mac = $item->data->apMac;
        $device = Device::where('mac', $mac)->first();

        \DB::table('cmx_data')->insert(
            [
            'tenant_id' => $device ? $device->tenant_id : null,
            'device_id' => $device ? $device->id : null,
            'mac' => $mac,
            'type' => $item->type,
            'data' => json_encode($request->all()),
            'created_at' => $now,
            'updated_at' => $now
            ]
        );

        return response()->json(['status' => 'ok']);
    }
}

