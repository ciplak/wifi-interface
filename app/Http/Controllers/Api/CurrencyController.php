<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\CurrencyFormRequest;
use App\Repositories\CurrencyRepository;

/**
 * Class CurrencyController
 */
class CurrencyController extends ApiController
{

    /**
     * @var CurrencyRepository
     */
    private $currencies;

    /**
     * CurrencyController constructor.
     *
     * @param CurrencyRepository $currencies
     */
    public function __construct(CurrencyRepository $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * Get list of all device vendors
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return $this->currencies->all();
    }

    /**
     * Create a currency
     *
     * @param CurrencyFormRequest $request
     *
     * @return mixed
     */
    public function store(CurrencyFormRequest $request)
    {
        return $this->currencies->create($request->all());
    }

    /**
     * Update the given currency
     *
     * @param CurrencyFormRequest $request
     * @param $id
     *
     * @return mixed
     */
    public function update(CurrencyFormRequest $request, $id)
    {
        return $this->currencies->update($request->only(['name', 'description', 'symbol', 'is_prefix']), $id);
    }

    /**
     * Delete the given device vendor
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $status = $this->currencies->delete($id);

        return response()->json(['status' => $status]);
    }

}

