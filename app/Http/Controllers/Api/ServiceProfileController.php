<?php
namespace App\Http\Controllers\Api;

use App\Models\ServiceProfile;
use App\Repositories\ServiceProfileRepository;

/**
 * Class ServiceProfileController
 */
class ServiceProfileController extends ApiController
{
    /**
     * @var ServiceProfileRepository
     */
    private $serviceProfiles;

    /**
     * ServiceProfileController constructor.
     *
     * @param ServiceProfileRepository $serviceProfiles
     */
    public function __construct(ServiceProfileRepository $serviceProfiles)
    {
        $this->serviceProfiles = $serviceProfiles;
    }

    /**
     * Get list of all device models
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        /**
 * @var ServiceProfile[] $items 
*/
        $items = $this->serviceProfiles->all();

        return $items;
    }

}

