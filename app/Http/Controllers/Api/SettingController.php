<?php
namespace App\Http\Controllers\Api;

use App\Repositories\SettingRepository;
use Illuminate\Http\Request;

/**
 * Class SettingController
 */
class SettingController extends ApiController
{
    /**
     * @var SettingRepository
     */
    private $settings;

    /**
     * SettingController constructor.
     *
     * @param SettingRepository $settings
     */
    public function __construct(SettingRepository $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Get list of all settings
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        $settings = $this->settings->getAll();

        $array = [];
        foreach ($settings as $key => $value) {
            array_set($array, $key, $value);
        }

        return $array;
    }

    /**
     * Update the given currency
     *
     * @return mixed
     */
    public function update()
    {
        $request = trimmed_request();
        $settings = $request->except(['errors', 'busy', 'successful']);
        $insertList = array_dot($settings);

        $keys = $this->settings->getKeys();

        $updateList = [];
        foreach ($keys as $key) {
            if (isset($insertList[$key])) {
                $updateList[$key] = $insertList[$key];
            }

            unset($insertList[$key]);
        }

        foreach ($updateList as $name => $value) {
            \DB::table('settings')
                ->whereNull('tenant_id')
                ->where('name', $name)
                ->update(['value' => $value]);
        }

        if ($insertList) {
            $data = [];
            foreach ($insertList as $name => $value) {
                $data[] = ['tenant_id' => null, 'name' => $name, 'value' => $value];
            }

            \DB::table('settings')->insert($data);
        }
    }

    /**
     * Get Facebook sharing settings
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function facebook()
    {
        return response()->json([
            'message' => setting('social.facebook.message', ''),
            'link'    => setting('social.facebook.link', ''),
            'caption' => setting('social.facebook.caption', ''),
            'place'   => setting('social.facebook.place_id', ''),
        ]);
    }
}