<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\RoleFormRequest;
use App\Models\Role;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class RoleController
 */
class RoleController extends ApiController
{
    use ValidatesRequests;

    /**
     * Get list of all roles
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Role::where('name', '<>', 'developer')->get();
    }

    /**
     * Create or update a role
     *
     * @param RoleFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RoleFormRequest $request)
    {
        return Role::create($request->only(['name', 'display_name', 'description']));
    }

    /**
     * Create or update a role
     *
     * @param RoleFormRequest $request
     * @param int             $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RoleFormRequest $request, $id)
    {
        return Role::where('id', $id)->update($request->only(['name', 'display_name', 'description']));
    }

    /**
     * Delete the given role
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $status = Role::where('id', $id)->where('name', '<>', 'developer')->delete();

        return response()->json(['status' => $status]);
    }

}

