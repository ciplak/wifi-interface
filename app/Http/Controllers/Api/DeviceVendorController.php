<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\DeviceVendorFormRequest;
use App\Repositories\Criteria\OrderBy;
use App\Repositories\DeviceVendorRepository;

/**
 * Class DeviceVendorController
 */
class DeviceVendorController extends ApiController
{
    /**
     * @var DeviceVendorRepository
     */
    private $deviceVendors;

    /**
     * DeviceVendorController constructor.
     *
     * @param DeviceVendorRepository $deviceVendors
     */
    public function __construct(DeviceVendorRepository $deviceVendors)
    {
        $this->deviceVendors = $deviceVendors;
    }

    /**
     * Get list of all device vendors
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        $this->deviceVendors->pushCriteria(new OrderBy('name'));
        return $this->deviceVendors->all();
    }

    /**
     * Create a device vendor
     * 
     * @param DeviceVendorFormRequest $request
     *
     * @return mixed
     */
    public function store(DeviceVendorFormRequest $request)
    {
        return $this->deviceVendors->create($request->all());
    }

    /**
     * Update the given device vendor
     * 
     * @param DeviceVendorFormRequest $request
     * @param int                     $id
     *
     * @return mixed
     */
    public function update(DeviceVendorFormRequest $request, $id)
    {
        return $this->deviceVendors->update($request->all(), $id);
    }

    /**
     * Delete the given device vendor
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $status = $this->deviceVendors->delete($id);

        return response()->json(['status' => $status]);
    }

    /**
     * Get list of device vendors
     *
     * @return array
     */
    public function getList()
    {
        return $this->deviceVendors->getList();
    }

}

