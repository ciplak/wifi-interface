<?php
namespace App\Http\Controllers\Api;

use App\ApiSerializer;
use App\Models\Venue;
use App\Transformers\Api\VenueTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class LocationController extends ApiController
{
    /**
     * Get list of venues
     *
     * @param Request $request
     *
     * @return string
     */
    public function index(Request $request)
    {
        $paginator = $this->getPaginator($request);
        $data = $paginator->getCollection();

        $json = fractal($data, new VenueTransformer, new ApiSerializer)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toJson();

        return $this->response($json);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPaginator(Request $request)
    {
        /** @var Builder $query */
        $query = Venue::query()
            ->select([
                'id',
                'name',
                'description',
                'login_page_id',
                'post_login_page_id',
                'created_at',
            ])
            ->if($request->has('name') && trim($request->name), 'name', 'LIKE', "%{$request->name}%")
            ->if($request->has('description') && trim($request->description), 'description', 'LIKE', "%{$request->description}%");

        return $query->paginate();
    }
}

