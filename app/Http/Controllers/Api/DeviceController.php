<?php
namespace App\Http\Controllers\Api;

use App\ApiSerializer;
use App\Models\Device;
use App\Transformers\Api\DeviceTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class DeviceController extends ApiController
{
    /**
     * Get list of devices
     *
     * @param Request $request
     *
     * @return string
     */
    public function index(Request $request)
    {
        $paginator = $this->getPaginator($request);
        $data = $paginator->getCollection();

        $json = fractal($data, new DeviceTransformer, new ApiSerializer)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toJson();

        return $this->response($json);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPaginator(Request $request)
    {
        /** @var Builder $query */
        $query = Device::query()
            ->select([
                'id',
                'name',
                'description',
                'device_model_id',
                'mac',
                'ip',
                'created_at',
            ])
            ->if($request->has('name') && trim($request->name), 'name', 'LIKE', "%{$request->name}%")
            ->if($request->has('mac') && trim($request->mac), 'mac', 'LIKE', "%{$request->mac}%");

        return $query->paginate();
    }
}

