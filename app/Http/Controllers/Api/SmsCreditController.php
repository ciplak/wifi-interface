<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\SmsCreditInstallFormRequest;
use App\Repositories\SettingRepository;
use App\Repositories\SmsLogRepository;
use Illuminate\Http\Request;

class SmsCreditController extends ApiController
{
    /**
     * @var SmsLogRepository
     */
    private $smsLogs;
    /**
     * @var SettingRepository
     */
    private $settings;

    /**
     * SmsCreditController constructor.
     *
     * @param SmsLogRepository $smsLogs
     * @param SettingRepository $settings
     */
    public function __construct(SmsLogRepository $smsLogs, SettingRepository $settings)
    {
        $this->smsLogs = $smsLogs;
        $this->settings = $settings;
    }

    /**
     * Install SMS credits
     *
     * @param SmsCreditInstallFormRequest $request
     * @param int $tenantId
     *
     * @return array
     */
    public function store(SmsCreditInstallFormRequest $request, $tenantId)
    {
        $installedCredits = $this->smsLogs->installCredit($tenantId, $request);

        $credits = $this->smsLogs->getInstalledSmsCreditList($tenantId);

        $this->smsLogs->updateTotalSmsCredits($tenantId, $installedCredits);


        if ($installedCredits) {
            $data = [
                'success'           => true,
                'message'           => trans('tenant.sms_credits.installed', ['count' => $request->input('credit')]),
                'html'              => view('admin.tenant.partials.sms_credits.history', ['credits' => $credits])->render(),
                'total_sms_credits' => $this->settings->getSmsCredits($tenantId),
            ];
        } else {
            $data = [
                'success' => false,
                'message' => trans('tenant.sms_credits.not_installed'),
                'html'    => view('admin.tenant.partials.sms_credits.history', ['credits' => $credits])->render(),
            ];
        }

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @param int $tenantId
     * @param int $id
     */
    public function delete(Request $request, int $tenantId, int $id)
    {

    }
}

