<?php
namespace App\Http\Controllers\Api;

use App\Events\RadiusServer\Created;
use App\Events\RadiusServer\Updated;
use App\Http\Requests\RadiusServerFormRequest;
use App\Repositories\RadiusServerRepository;
use App\Transformers\RadiusServerTransformer;

class RadiusServerController extends ApiController
{
    /**
     * @var RadiusServerRepository
     */
    private $radiusServers;

    /**
     * RadiusServerController constructor.
     *
     * @param RadiusServerRepository $radiusServers
     */
    public function __construct(RadiusServerRepository $radiusServers)
    {
        $this->radiusServers = $radiusServers;
    }

    /**
     * Get list of all device vendors
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return fractal()
            ->collection($this->radiusServers->all(), new RadiusServerTransformer)
            ->toArray();
    }

    /**
     * Create a radius server
     *
     * @param RadiusServerFormRequest $request
     *
     * @return mixed
     */
    public function store(RadiusServerFormRequest $request)
    {
        $attributes = $request->all();
        $attributes['secret'] = \Crypt::encrypt($attributes['secret']);

        if ($model = $this->radiusServers->create($attributes)) {
            event(new Created($model));
        };

        return $model;
    }

    /**
     * Update the given radius server
     *
     * @param RadiusServerFormRequest $request
     * @param $id
     *
     * @return mixed
     */
    public function update(RadiusServerFormRequest $request, $id)
    {
        $attributes = $request->all();
        $attributes['secret'] = \Crypt::encrypt($attributes['secret']);

        if ($status = $this->radiusServers->update($attributes, $id)) {
            event(new Updated($id));
        }

        return $status;
    }

    /**
     * Delete the given device vendor
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $status = $this->radiusServers->delete($id);

        return response()->json(['status' => $status]);
    }
}