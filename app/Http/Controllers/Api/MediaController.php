<?php

namespace App\Http\Controllers\Api;

use App\Models\Media;
use App\Models\SplashPage;
use App\Repositories\SplashPageRepository;
use App\Transformers\MediaTransformer;
use Illuminate\Http\Request;

/**
 * Class MediaController
 */
class MediaController extends ApiController
{
    /**
     * @var SplashPageRepository
     */
    private $splashPages;

    /**
     * MediaController constructor.
     *
     * @param SplashPageRepository $splashPages
     */
    public function __construct(SplashPageRepository $splashPages)
    {
        $this->splashPages = $splashPages;
    }

    /**
     * Get list of uploaded images for the venue
     *
     * @return \Illuminate\Http\Response|mixed
     */
    public function getAll()
    {
        $data = Media::where('tenant_id', activeTenantId())
            ->where(function($query) {
                /**
                 * @var \Illuminate\Database\Query\Builder $query
                 */
                $query->where('venue_id', $this->user()->property_id)
                    ->orWhereNull('venue_id');
            })->get();

        return fractal()
            ->collection($data, new MediaTransformer)
            ->toArray();
    }

    /**
     * Upload a file to the media library
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $input = $request->all();

        $rules = [
            'file' => 'image|max:1024',
        ];

        /**
         * @var \Illuminate\Validation\Validator $validation
         */
        $validation = validator($input, $rules);

        if ($validation->fails()) {
            return response()->json($validation->errors()->first(), 400);
        }

        /**
         * @var \Illuminate\Http\UploadedFile $file
         */
        $file = $request->file('file');
        $fileSize = $file->getSize();

        list($width, $height) = getimagesize($file->getRealPath());

        $filename = $file->store(assets_base('img'));

        if ($filename) {

            $loginPage = null;
            if ($request->has('model_id')) {
                $id = $request->input('model_id');

                /** @var SplashPage $loginPage */
                $loginPage = $this->splashPages->find(hashids_decode($id), ['id']);
            }

            $media = Media::create([
                'tenant_id'         => activeTenantId(),
                'venue_id'          => $this->user()->property_id,
                'collection_name'   => 'images',
                'model_id'          => $loginPage ? $loginPage->id : null,
                'model_type'        => $loginPage ? SplashPage::class : null,
                'name'              => $file->getClientOriginalName(),
                'file_name'         => basename($filename),
                'size'              => $fileSize,
                'disk'              => config('filesystems.default'),
                'custom_properties' => [
                    'width'      => $width,
                    'height'     => $height,
                    'dimensions' => "{$width}x{$height}",
                ],
            ]);

            return response()->json(fractal()->item($media, new MediaTransformer)->toArray(), 200);
        } else {
            return response()->json(['status' => false], 400);
        }
    }

    /**
     * Delete the given file from the media library
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Request $request, $id)
    {
        $mediaId = hashids_decode($id);

        $result = false;

        /**
         * @var Media $media
         */
        $media = Media::find($mediaId, ['id', 'file_name']);

        if ($media) {
            \Storage::disk($media->disk)->delete(assets_base('img/' . $media->file_name));
            $media->delete();
            $result = true;
        }

        return response()->json(['status' => $result], 200);
    }
}
