<?php
/**
 * Created by PhpStorm.
 * User: Burak
 * Date: 25.04.2016
 * Time: 17:16
 */

namespace App\Http\Controllers\Api;

use App\Http\Requests\AccessCodeFormRequest;
use App\Models\AccessCode;
use App\Models\Guest;
use App\Models\Zone;
use App\Repositories\AccessCodeRepository;
use Carbon\Carbon;

class AccessCodeController extends ApiController
{
    /**
     * @var AccessCodeRepository
     */
    private $accessCodes;

    /**
     * AccessCodeController constructor.
     *
     * @param AccessCodeRepository $accessCodes
     */
    public function __construct(AccessCodeRepository $accessCodes)
    {
        $this->accessCodes = $accessCodes;
    }

    public function getAccessCode()
    {
        $accessCode = null;
        $resultCode = ResultCode::INTERNAL_SERVER_ERROR;

        $result = $this->checkData(\Request::all());
        if (is_array($result)) {
            $resultCode = $result[0];
            $customMessage = $result[1];
        } else {
            $resultCode = $result;
        }

        if ($resultCode == ResultCode::SUCCESS) {
            $accessCode = AccessCode::generate();

            try {
                $genderList = ['m' => Guest::GENDER_MALE, 'f' => Guest::GENDER_FEMALE];

                $guest = Guest::firstOrNew(['passport_no' => request('passportNumber')]);

                $gender = strtolower(request('gender'));
                $guest->fill([
                    'name'        => request('firstName') . ' ' . request('lastName'),
                    'passport_no' => request('passportNumber'),
                    'nationality' => request('countryCode'),
                    'birthday'    => Carbon::createFromFormat('Y-m-d', request('dateOfBirth')),
                    'gender'      => $genderList[$gender] ?? Guest::GENDER_UNKNOWN,
                ]);

                $guest->save();

                /**
                 * @var Zone $zone
                 */
                $zone = Zone::where('is_default', 1)->first();

                AccessCode::create([
                    'guest_id'           => $guest->id,
                    'service_profile_id' => $zone->getDefaultServiceProfile()->id,
                    'type'               => AccessCode::TYPE_ACCESS_CODE,
                    'code'               => $accessCode,
                    'expiry_date'        => Carbon::tomorrow(),
                    'status'             => AccessCode::STATUS_ACTIVE,
                    'created_by'         => $this->userId(),
                ]);

            } catch (\Exception $e) {
                \Log::error('Error on creating access code by api', ['message' => $e->getMessage()]);
            }
        }

        $resultMessage = ResultCode::message($resultCode);

        if (isset($customMessage)) {
            $resultMessage .= ': ' . $customMessage;
        }

        $response = [
            'resultCode'    => $resultCode,
            'resultMessage' => $resultMessage,
        ];

        if ($accessCode) {
            $response['accessCode'] = $accessCode;
        }

        return \Response::json(
            $response, 200, [
                'access-control-allow-origin' => '*',
                'Access-Control-Max-Age'      => 1000,
            ]
        )->setCallback(request('callback'));
    }

    private function checkData($requestData)
    {

        if (empty($requestData)) {
            return ResultCode::INVALID_REQUEST_OBJECT;
        }

        // todo add validation
        $fields = [
            'firstName',
            'lastName',
            'countryCode',
            'dateOfBirth',
            'gender',
            'passportNumber',
            'ip',
            'mac',
        ];

        foreach ($fields as $field) {
            if (!isset($requestData[$field]) || !trim($requestData[$field])) {
                $code = ResultCode::INVALID_REQUEST_PARAMETER;

                return [$code, "{$field} is required"];
            }
        }

        return ResultCode::SUCCESS;
    }

    /**
     * Generate new access code(s)
     *
     * @param AccessCodeFormRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function generate(AccessCodeFormRequest $request)
    {
        $attributes = $request->all();

        if (\Entrust::can('access-codes.manage-global')) {
            $attributes['venue_id'] = hashids_decode($request->input('venue_id'));
        } else {
            $attributes['venue_id'] = $this->user()->property_id;
        }

        $accessCodes = $this->accessCodes->generateCode($attributes, setting('access_code.length', 8), setting('access_code.chars', AccessCode::CHARS_ALPHANUMERIC));
        $count = $accessCodes->count();

        if ($count) {
            $message = trans_choice('access_code.created', $count, ['count' => $count]);
            session()->flash('success', $message);
        } else {
            $message = trans('access_code.could_not_create', $count);
        }

        return response()->json(
            [
                'success'  => (bool)$count,
                'messages' => $message,
            ]
        );
    }
}


class ResultCode
{
    const SUCCESS = 200;
    const INTERNAL_SERVER_ERROR = 100;
    const INVALID_REQUEST_OBJECT = 101;
    const INVALID_REQUEST_PARAMETER = 102;
    const BAD_REQUEST = 404;

    private static $resultMessages = [
        ResultCode::SUCCESS                   => 'Success',
        ResultCode::INTERNAL_SERVER_ERROR     => 'Internal server error',
        ResultCode::INVALID_REQUEST_OBJECT    => 'Invalid request object',
        ResultCode::INVALID_REQUEST_PARAMETER => 'Invalid request parameter',
        ResultCode::BAD_REQUEST               => 'Bad Request',
    ];

    public static function message($resultCode)
    {
        return self::$resultMessages[$resultCode];
    }
}
