<?php
namespace App\Http\Controllers\Api;

use App\Date;
use App\Models\GuestActivity;
use App\Repositories\GuestDeviceRepository;
use App\Repositories\GuestRepository;
use App\Repositories\WifiSessionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class ReportController
 */
class ReportController extends ApiController
{
    /**
     * @var GuestRepository
     */
    private $guests;

    /**
     * @var WifiSessionRepository
     */
    private $wifiSessions;

    /**
     * @var GuestDeviceRepository
     */
    private $guestDevices;

    /**
     * ReportController constructor.
     *
     * @param GuestRepository       $guests
     * @param WifiSessionRepository $wifiSessions
     * @param GuestDeviceRepository $guestDevices
     */
    public function __construct(GuestRepository $guests, WifiSessionRepository $wifiSessions, GuestDeviceRepository $guestDevices)
    {
        $this->guests = $guests;
        $this->wifiSessions = $wifiSessions;
        $this->guestDevices = $guestDevices;
    }

    /**
     * @param Request     $request
     * @param string|null $venueId
     *
     * @return mixed
     */
    public function stats(Request $request, $venueId = null)
    {
        $venueId = $venueId ? hashids_decode($venueId) : null;

        $today = Carbon::today()->format('Y-m-d');

        if ($request->has('start_date')) {
            $interval = ($request->start_date == $today) && ($request->end_date == $today)
                ? Date::RANGE_TODAY : Date::RANGE_INTERVAL;
        } else {
            $interval = Date::RANGE_TODAY;
        }

        $data = $this->getData($venueId, $interval);
        //$data = $this->getDemoData();

        return $data;
    }

    private function getDemoData()
    {
        $totalTrafficOfDay = rand(0, 100 * 1024) * 1024 * 1024;

        $onlineUsers = array_fill(0, 24, 0);
        for ($i = 0; $i < date('G') + 1; $i++) {
            $onlineUsers[$i] = rand(0, 120);
        }

        $numberOfOnlineGuests = $onlineUsers[date('G')];
        $numberOfMaxOnlineGuests = (int)max($onlineUsers);
        $numberOfLogins = array_sum($onlineUsers);
        $numberOfNewLogins = (int)($numberOfLogins / (1 + rand(10, 65) /100 ));
        $numberOfImpressions = (int)($numberOfLogins * (1 + rand(10, 50) / 100));

        $totalTrafficOfDay = human_filesize($totalTrafficOfDay);

        $deviceUsageShares = [['Desktop', rand(253, 1205)], ['Mobile', rand(253, 1205)], ['Tablet', rand(253, 1205)]];

        return compact(
            'numberOfOnlineGuests',
            'numberOfMaxOnlineGuests',
            'numberOfLogins',
            'numberOfImpressions',
            'numberOfNewLogins',
            'totalTrafficOfDay',
            'onlineUsers',
            'deviceUsageShares'
        );
    }

    private function getData($venueId = null, $interval = Date::RANGE_TODAY)
    {
        // if interval is today get current online guest count
        $numberOfOnlineGuests = $interval == Date::RANGE_TODAY
            ? $this->guests->getOnlineCount($venueId)
            : $this->wifiSessions->getOnlineGuestCount($interval, $venueId);

        $totalTrafficOfDay = human_filesize($this->wifiSessions->getTotalTraffic($venueId, $interval));
        $numberOfLogins = $this->wifiSessions->getTotalLoginCount($venueId, $interval);
        $numberOfImpressions = GuestActivity::getTotalImpressions($interval, $venueId);

        $deviceUsageShares = $this->guestDevices->getUsageShares($this->user()->tenant_id, ['location' => $venueId, 'date' => $interval]);
        $deviceUsageShares = $this->guestDevices->transformDeviceUsageSharesData($deviceUsageShares);

        // new customers %
        $diffInDays = config('main.new_users_range');


        $selectedDates = Date::getRanges($interval);

        $newUserControlInterval = [$selectedDates[0]->copy()->subDay($diffInDays), $selectedDates[0]];
        $numberOfNewLogins = $this->wifiSessions->getNewGuestCount($newUserControlInterval, $selectedDates, $venueId);

        // online users
        $onlineUsers = $this->wifiSessions->getDailyActivity(activeTenantId(), $venueId, Date::getRanges($interval));

        $numberOfMaxOnlineGuests = (int)max($onlineUsers);

        return compact(
            'numberOfOnlineGuests',
            'totalTrafficOfDay',
            'numberOfLogins',
            'numberOfImpressions',
            'deviceUsageShares',
            'numberOfMaxOnlineGuests',
            'numberOfNewLogins',
            'onlineUsers'
        );
    }
}

