<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\DeviceModelFormRequest;
use App\Models\DeviceModel;
use App\Repositories\DeviceModelRepository;

/**
 * Class DeviceModelController
 */
class DeviceModelController extends ApiController
{
    /**
     * @var DeviceModelRepository
     */
    private $deviceModels;

    /**
     * DeviceModelController constructor.
     *
     * @param DeviceModelRepository $deviceModels
     */
    public function __construct(DeviceModelRepository $deviceModels)
    {
        $this->deviceModels = $deviceModels;
    }

    /**
     * Get list of all device models
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        /**
 * @var DeviceModel[] $items 
*/
        $items = $this->deviceModels->getAll();

        return $items;

    }

    /**
     * Create a device model
     *
     * @param DeviceModelFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DeviceModelFormRequest $request)
    {
        return $this->deviceModels->create($request->all());
    }

    /**
     * Update the given device model
     *
     * @param DeviceModelFormRequest $request
     * @param int                    $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(DeviceModelFormRequest $request, $id)
    {
        return $this->deviceModels->update($request->all(), $id);
    }

    /**
     * Delete the given device model
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $status = $this->deviceModels->delete($id);

        return response()->json(['status' => $status]);
    }

    /**
     * Get type options
     *
     * @return array
     */
    public function types()
    {
        return $this->deviceModels->getTypeOptions();
    }

    /**
     * Get device handler options
     *
     * @return array
     */
    public function handlers()
    {
        return $this->deviceModels->getHandlerOptions();
    }
}

