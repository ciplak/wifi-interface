<?php
namespace App\Http\Controllers\Api;

use App\ApiSerializer;
use App\Models\Guest;
use App\Transformers\Api\GuestTransformer;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class GuestController extends ApiController
{
    /**
     * Get list of guests
     *
     * @param Request $request
     *
     * @return string
     */
    public function index(Request $request)
    {
        $paginator = $this->getPaginator($request);
        $data = $paginator->getCollection();

        $json = fractal($data, new GuestTransformer, new ApiSerializer)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toJson();

        return $this->response($json);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPaginator(Request $request)
    {
        /** @var Builder $query */
        $query = Guest::query()
            ->select([
                'id',
                'name',
                'email',
                'phone',
                'created_at'
            ])
            ->if($request->has('name') && trim($request->name), 'name', 'LIKE', "%{$request->name}%")
            ->if($request->has('email') && trim($request->email), 'email', 'LIKE', "%{$request->email}%");

        return $query->paginate();
    }
}

