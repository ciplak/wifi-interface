<?php
namespace App\Http\Controllers\Api;

use App\ApiSerializer;
use App\Models\GuestDevice;
use App\Transformers\Api\GuestDeviceTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class GuestDeviceController extends ApiController
{
    /**
     * Get list of guestdevices
     *
     * @param Request $request
     *
     * @return string
     */
    public function index(Request $request)
    {
        $paginator = $this->getPaginator($request);
        $data = $paginator->getCollection();

        $json = fractal($data, new GuestDeviceTransformer, new ApiSerializer)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toJson();

        return $this->response($json);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPaginator(Request $request)
    {
        /** @var Builder $query */
        $query = GuestDevice::query()
            ->select([
                'id',
            ]);

        return $query->paginate();
    }
}

