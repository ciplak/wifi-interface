<?php
namespace App\Http\Controllers\Api;


use Illuminate\Routing\Controller;

abstract class ApiController extends Controller
{
    /**
     * Get api user id
     *
     * @return int|null
     */
    public function userId()
    {
        return $this->user()->id;
    }

    /**
     * Get api user
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|\App\Models\User
     */
    public function user()
    {
        return \Auth::user();
    }

    /**
     * Return response
     *
     * @param string $json
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function response($json)
    {
        if (request()->has('prettify') && request('prettify')) {
            $json = jsonPrettify($json);
        }

        return response($json, 200, [
            'content-type' => 'application/json'
        ]);
    }
}