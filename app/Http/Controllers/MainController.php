<?php

namespace App\Http\Controllers;


use App\Models\MongoLog;
use App\Services\Sms\Sms;

use App\Services\WifiEngine\Engine;

class MainController extends Controller
{
    public function index()
    {
        return 'OK';
    }

    public function test()
    {


        $params = [
            'username' => '5010-test',
            'password' => 'test'
        ];
        $r = (new Sms)->driver('MobilPark')->send('5320000000', 'This is a test message', $params);

        dd($r);
    }

    public function mid()
    {

        $engine = new Engine();

        $x = $engine->syncSessions();
        $y = $engine->sessionChecker();
        $z = $engine->accountingChecker();


//        dd($x, $y, $z);
    }
}