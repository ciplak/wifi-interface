<?php

namespace App\Traits;

use App\Models\AlertMessage;
use App\Models\Device;
use App\Models\Guest;
use App\Models\GuestDevice;
use App\Models\ServiceProfile;
use App\Services\RadiusCommunicator\RadiusCommunicator;

trait RedirectsToAccessPoint
{
    /**
     * Connect guest to WiFi
     *
     * @param GuestDevice    $guestDevice
     * @param ServiceProfile $serviceProfile
     * @param int|null       $loginType
     * @param null           $logoutTime
     *
     * @return bool|int
     * @throws \Exception
     */
    public function createWiFiUser(GuestDevice $guestDevice, ServiceProfile $serviceProfile = null, $loginType = null, $logoutTime = null)
    {
        try {
            $guest = $guestDevice->owner;

            if ($serviceProfile) {
                $guestDevice->updateConnectionInfo($guest, $serviceProfile, $loginType, $logoutTime);
            }

            $connectionStatus = (new RadiusCommunicator)
                ->driver(config('main.radius_server'))
                ->createWiFiUser($guestDevice, $serviceProfile ?: $guestDevice->serviceProfile);

        } catch (\Exception $e) {
            $connectionStatus = false;
            log_error('Error on creating wifi user on radius server', [
                'message' => $e->getMessage(),
            ]);
        }

        if (!$connectionStatus) {
            AlertMessage::high('Could not create wifi user', [
                'serviceProfileId' => $serviceProfile ? $serviceProfile->id : null,
                'guestId'          => $guestDevice->guest_id,
            ]);
        }

        return $connectionStatus;
    }

    /**
     * Redirect guest to access point
     *
     * @param Guest  $guest
     * @param Device $accessPoint
     * @param bool   $onlyForm
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|string
     */
    private function redirectToDeviceLoginPage(Guest $guest, Device $accessPoint, $onlyForm = true)
    {
        $vendor = $accessPoint->getDeviceHandlerClassName();

        $deviceDriver = (new \App\Services\DeviceHandlers\Device)->driver($vendor);

        $parameters = $deviceDriver->getFormParameters($guest, url('access?res=success'));

        log_info('Redirecting to Access point login page', ['Access point url' => session('device_login_page_url'), 'parameters' => $parameters]);

        $form = $this->buildRedirectForm(session('device_login_page_url'), $parameters['fields'], $parameters['method']);

        if ($onlyForm) {
            return ['redirect' => true, 'form' => $form];
        } else {
            return view('splash_page.redirect_page_layout', ['form' => $form])->render();
        }
    }

    /**
     * Build redirect form
     *
     * @param string $url
     * @param array  $fields
     * @param string $method
     *
     * @return string
     */
    public function buildRedirectForm(string $url, array $fields, string $method = 'POST'): string
    {
        $form = \Form::open(['id' => 'redirect-form', 'url' => $url, 'method' => $method]);

        if (isset($fields)) {
            foreach ($fields as $name => $value) {
                $form .= \Form::hidden($name, $value);
            }
        }

        $form .= \Form::close();

        return $form;
    }
}
