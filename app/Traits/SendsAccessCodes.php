<?php

namespace App\Traits;


use App\Models\AccessCode;
use App\Models\Guest;
use App\Notifications\AccessCodeNotification;

trait SendsAccessCodes
{

    /**
     * Resend access code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendAccessCode()
    {
        $guest = $this->currentGuestDevice->owner;

        $accessCode = $this->accessCodes->getFirstAvailableForGuest($guest);

        if ($accessCode) {
            $result = $this->sendAccessCode($accessCode, $guest);

            return response()->json(['message' => $result['message']]);
        } else {
            return response()->json(['message' => trans('login.could_not_send_access_code')]);
        }
    }

    /**
     * @param AccessCode $accessCode
     * @param Guest      $guest
     *
     * @return array
     */
    public function sendAccessCode($accessCode, $guest)
    {
        log_info('Sending access code', ['guest_id' => $guest->id, 'username' => $guest->username, 'code' => $accessCode->code]);

        $message = '';

        $emailSent = $smsSent = false;

        $noSmsCredits = config('front_page_settings.sms.total_credits') < 1;

        try {
            $smsContent = trim(config('front_page_settings.sms.content.' . $this->splashPage->locale));

            if (!$smsContent) {
                $smsContent = trans('login.sms_content');
            }

            $guest->notify(
                new AccessCodeNotification(
                    $accessCode->code,
                    $this->splashPage,
                    !$noSmsCredits,
                    $this->location->getSmsProvider(),
                    'WiFi Access at ' . $this->venue->name,
                    $smsContent
                )
            );
        } catch (\Exception $e) {
            \Log::error('Error on sending access code notification', [
                'error' => $e->getMessage()
            ]);
        }

        if ($this->splashPage->send_email) {
            $message = trans('login.access_code_sent', ['email' => $guest->email]);
        }

        if ($noSmsCredits) {
            $message = trans('login.no_sms_credit');
        } else {

            if ($this->splashPage->send_sms) {
                $message = trans('login.sms_code_sent', ['phone' => $guest->phone]);
                session(['smsCode' => $accessCode]);
            }

            if ($this->splashPage->send_email && $this->splashPage->send_sms) {
                $message = trans('login.access_code_and_sms_sent', ['email' => $guest->email, 'phone' => $guest->phone]);
            }

        }

        return ['status' => $emailSent || $smsSent, 'message' => $message];
    }
}