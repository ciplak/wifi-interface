<?php

namespace App\Traits;


use App\Models\AccessCode;
use App\Models\AlertMessage;
use App\Models\Guest;
use App\Models\ServiceProfile;
use App\Models\SplashPage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class RegistersGuests
 *
 * @property \App\Repositories\GuestRepository $guests
 */
trait RegistersGuests
{
    /**
     * Create a guest
     *
     * @param Request $request
     * @param array   $attributes
     * @param array   $fields
     * @param bool    $overwriteGuests
     *
     * @return Guest
     */
    private function createGuest(Request $request, array $attributes, array $fields, $overwriteGuests = false)
    {
        log_info('Creating guest', ['data' => $attributes]);

        $attributes = $this->prepareAttributesForRegistration($request, $attributes, $fields);

        if (isset($attributes['phone'])) {
            $attributes['phone'] = build_phone_number();
        }

        $guest = $this->runCreateQueries($attributes, $overwriteGuests);

        log_info('Guest created', ['guest id' => $guest->id, 'data' => $attributes]);

        return $guest;
    }

    /**
     * Handle register
     *
     * @param array $attributes
     * @param bool  $overwriteGuests
     *
     * @return Guest|mixed|null
     */
    private function runCreateQueries($attributes, $overwriteGuests = false)
    {
        $update = false;
        $checkAttributes = [
            'tenant_id' => $attributes['tenant_id'],
            'venue_id'  => $attributes['venue_id'],
        ];

        if ($overwriteGuests) {

            if (isset($attributes['phone']) && trim($attributes['phone'])) {
                $checkAttributes['phone'] = $attributes['phone'];

                $update = true;
            } else {
                if (isset($attributes['email']) && trim($attributes['email'])) {
                    $checkAttributes['email'] = $attributes['email'];

                    $update = true;
                }
            }
        }

        if ($update) {
            $guest = $this->guests->updateOrCreate($checkAttributes, $attributes);
        } else {
            $guest = $this->guests->create($attributes);
        }

        return $guest;
    }

    /**
     * @param Request $request
     * @param array   $attributes
     * @param array   $fields
     *
     * @return mixed
     */
    private function prepareAttributesForRegistration(Request $request, $attributes, $fields)
    {
        if (isset($attributes['birthday']) && trim($attributes['birthday'])) {
            $attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $attributes['birthday'])->toDateString();
        } else {
            $attributes['birthday'] = null;
        }

        // set custom data
        if (!isset($attributes['custom_data'])) {

            $attributes['custom_data'] = [];

            for ($i = 1; $i <= config('main.custom_field_count'); $i++) {
                $key = "custom_field_{$i}";

                $customItem = ['value' => $request->input($key)];
                foreach (getLocales() as $locale => $name) {
                    $customItem['title'][$locale] = array_get($fields, "registration_fields.custom_field_{$i}.title.$locale");
                }

                $attributes['custom_data'][$key] = $customItem;
            }
        }

        return $attributes;
    }

    /**
     * Handle post request for access code form
     *
     * @param Request $request
     * @param array   $attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function loginGuest(Request $request, $attributes = [])
    {
        $code = $request->input('code');
        $accessCode = $this->accessCodes->checkCode($code, false, $this->tenantId, $this->venue->id);

        // invalid access code
        if ($accessCode == false) {
            log_info('Invalid access code', ['access code' => $code]);

            return response()->json([
                'error_message' => trans('login.invalid_access_code', ['code' => $code]),
            ]);
        }

        if (isset($attributes['phone'])) {
            $attributes['phone'] = build_phone_number();
        }

        $guest = null;

        // find guest by attributes
        if ($attributes) {
            $guest = $this->guests->findByAttributes($attributes);
        } else {
            // create a new guest for log in with only access code
            $attributes['tenant_id'] = $this->tenantId;
            $attributes['venue_id'] = $this->venue->id;

            $guest = $this->createGuest($request, $attributes, []);
        }

        $accessCodeGuestDoesNotMatch = ($accessCode->guest_id && $guest && ($accessCode->guest_id != $guest->id));

        if (!$guest || $accessCodeGuestDoesNotMatch) {
            log_info('Access code valid but guest not found', ['access code' => $code, 'guest' => $attributes]);

            return response()->json(
                [
                    'error_message' => trans('login.form.access_code_and_info_do_not_match'),
                ]
            );
        }

        $this->accessCodes->useCode($accessCode);

        $this->guests->setAccessCode($guest->id, $accessCode);

        $this->guestDevices->setOwner($this->currentGuestDevice, $guest);

        if (!$guest->canConnectWithServiceProfile($accessCode->serviceProfile)) {
            return response()->json(
                [
                    'error_message' => $accessCode->serviceProfile->type == ServiceProfile::TYPE_ONE_TIME
                        ? trans('login.service_profile.type.one_time_error')
                        : trans(
                            'login.service_profile.type_recurring_error', [
                                'frequency' => $accessCode->serviceProfile->calculateRenewalFrequencyHours(),
                            ]
                        ),
                ]
            );
        }

        if (!session()->has('guest_login_type')) {
            session(['guest_login_type' => SplashPage::LOGIN_TYPE_ACCESS_CODE]);
        }

        $connectionStatus = $this->createWiFiUser($this->currentGuestDevice, $accessCode->serviceProfile, session('guest_login_type'), $accessCode->logout_time);

        if ($connectionStatus) {
            log_info('Valid access code', ['access code' => $code]);

            return $this->redirectToDeviceLoginPage($guest, $this->currentAccessPoint);
        } else {
            // if can't connect, update access code as available
            if ($accessCode->type != AccessCode::TYPE_SHARED) {
                // if can't connect, update access code as available
                $accessCode->update(
                    [
                        'use_count' => $accessCode->use_count - 1,
                        'status'    => true,
                    ]
                );
            }

            AlertMessage::medium('Access code is valid but could not connect', ['client mac' => session('client_mac')]);

            return response()->json(
                [
                    'error_message' => trans('login.error_occurred'),
                ]
            );
        }
    }

    /**
     * Validate form fields
     *
     * @param Request $request
     * @param array   $rules
     * @param array   $messages
     *
     * @return \Illuminate\Validation\Validator
     */
    private function validateFields(Request $request, $rules = [], $messages = [])
    {
        // set validation messages for custom fields
        $locale = \App::getLocale();

        $messages['accept_terms_and_conditions.required'] = trans('login.accept_terms_and_conditions_warning', ['termsAndConditions' => trans('login.terms_and_conditions')]);
        $messages['code.required'] = trans('login.errors.access_code.required');

        for ($i = 1; $i <= config('main.custom_field_count'); $i++) {
            $messages["custom_field_{$i}.required"] = trans('validation.required', ['attribute' => e(array_get($this->splashPage->settings, "registration_fields.custom_field_{$i}.title.$locale"))]);
        }

        $attributes = $request->all();

        if (isset($attributes['phone'])) {
            $attributes['phone'] = build_phone_number();
        }

        $validator = validator($attributes, $rules, $messages);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
    }

    /**
     * Validate registration form
     *
     * @param Request $request
     * @param array   $fields
     * @param bool    $overwriteGuests
     *
     * @return \Illuminate\Validation\Validator
     */
    public function validateRegistrationForm(Request $request, array $fields, $overwriteGuests = false)
    {
        $tenantId = session('front_page_tenant_id');
        $isolateGuests = config('front_page_settings.isolate_guests');

        // build validation rules
        $rules = collect($fields)->map(
            function ($item, $key) use ($tenantId, $isolateGuests, $overwriteGuests) {
                $rules = [];
                $rule = null;

                if (!$overwriteGuests) {
                    switch ($key) {
                        case 'email':
                            $rule = Rule::unique('guests', 'email')->where('tenant_id', $tenantId);
                            break;

                        case 'phone':
                            $rule = Rule::unique('guests', 'phone')->where('tenant_id', $tenantId);
                            break;

                        case 'passport_no':
                            $rule = Rule::unique('guests', 'passport_no')->where('tenant_id', $tenantId);
                            break;
                    }
                }

                if ($rule) {
                    if ($isolateGuests) {
                        $rule->where('venue_id', session('front_page_venue_id'));
                    }

                    $rules[] = $rule;
                }

                return isset($item['rules'])
                    ? array_merge($item['rules'], $rules)
                    : $rules;
            }
        )->merge(['accept_terms_and_conditions' => 'required']);

        return $this->validateFields($request, $rules->toArray());
    }

    /**
     * Validate login form
     *
     * @param Request $request
     * @param array   $fields
     *
     * @return \Illuminate\Validation\Validator
     */
    public function validateLoginForm(Request $request, array $fields)
    {
        $rules = collect($fields)->map(
            function ($item) {
                $rules = [];

                if (isset($item['rules'])) {
                    return array_merge($item['rules'], $rules);
                }

                return [];
            }
        )->merge(['code' => 'required']);

        return $this->validateFields($request, $rules->toArray());
    }

    /**
     * Validate login form
     *
     * @param Request $request
     *
     * @return \Illuminate\Validation\Validator
     */
    public function validateAccessCodeForm(Request $request)
    {
        $rules = ['code' => 'required'];

        return $this->validateFields($request, $rules);
    }
}