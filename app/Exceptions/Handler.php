<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $e
     *
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof MethodNotAllowedHttpException) {
            \Log::error('Method not allowed ', ['url' => $request->url(), 'method' => $request->method(), 'message' => $e->getMessage()]);

            return redirect('/');
        }

        if ($e instanceof TokenMismatchException) {
            \Log::error('Token mismatch ', ['url' => $request->url(), 'method' => $request->method(), 'message' => $e->getMessage()]);

            return redirect()->back();
        }

        if ($e instanceof DeviceNotFoundException) {
            return response()->view('errors.device_not_found', ['error' => $e]);
        }
        if ($e instanceof LoginPageNotFoundException) {
            return response()->view('errors.login_page_not_found', ['error' => $e]);
        }

        if ($this->isHttpException($e) && $e->getStatusCode() == 403) {
            \Log::error('Permission error ', [
                'url'      => $request->url(),
                'user _id' => \Auth::id() ?: '-',
                'method'   => $request->method(),
                'message'  => $e->getMessage(),
            ]);

            if ($request->ajax() && $request->wantsJson()) {
                return response()->json(['You\'re not allowed to access this page'], 403);
            }

            return view('errors.403');
        }

        return parent::render($request, $e);
    }

    /**
     * Create a Symfony response for the given exception.
     *
     * @param  \Exception $e
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertExceptionToResponse(Exception $e)
    {
        $debug = config('app.debug', false);

        if (app()->environment() == 'local' && $debug) {
            return parent::convertExceptionToResponse($e);
        }

        return response()->view('errors.error', ['exception' => $e]);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request                 $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     *
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('admin/login');
    }
}
