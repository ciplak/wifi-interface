<?php

namespace App\Exceptions;

use App\Models\Property;

class LoginPageNotFoundException extends \Exception
{
    protected $message = 'Login page not found';

    /**
     * @var Property
     */
    private $property;

    /**
     * LoginPageNotFoundException constructor.
     *
     * @param Property $property
     */
    public function __construct(Property $property)
    {
        $this->property = $property;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }
}
