<?php

namespace App\Exceptions;

class DeviceNotFoundException extends \Exception
{
    protected $message = 'Access point not found';

    /**
     * @var string
     */
    private $mac;

    /**
     * DeviceNotFoundException constructor.
     *
     * @param string $mac
     */
    public function __construct($mac)
    {
        $this->mac = $mac;
    }

    /**
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }
}
