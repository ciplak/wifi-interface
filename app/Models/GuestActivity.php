<?php

namespace App\Models;

use App\Date;
use App\Models;
use App\Presenters\GuestActivityPresenter;
use McCool\LaravelAutoPresenter\HasPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\GuestActivity
 *
 * @property      integer            $id
 * @property      integer            $property_id
 * @property      integer            $device_id
 * @property      integer            $guest_id
 * @property      integer            $guest_device_id
 * @property      integer            $action_id
 * @property      string             $name
 * @property      string             $username
 * @property      string             $browser
 * @property      string             $platform
 * @property      string             $description
 * @property      string             $mac
 * @property      string             $ip
 * @property-read Guest              $guest
 * @property-read GuestDevice        $guestDevice
 * @property-read Device             $device
 * @method        static Builder|GuestActivity whereId($value)
 * @method        static Builder|GuestActivity whereGuestId($value)
 * @method        static Builder|GuestActivity whereGuestDeviceId($value)
 * @method        static Builder|GuestActivity whereDeviceId($value)
 * @method        static Builder|GuestActivity whereActionId($value)
 * @method        static Builder|GuestActivity whereMac($value)
 * @property      \Carbon\Carbon     $created_at
 * @property-read string             $created_at_formatted
 * @property-read string             $guest_device_type_name
 * @property      \Carbon\Carbon     $updated_at
 * @property-read \App\Models\Device $Device
 * @property-read int                $device_type_id
 * @method        static \Illuminate\Database\Query\Builder|GuestActivity wherePropertyId($value)
 * @method        static \Illuminate\Database\Query\Builder|GuestActivity whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|GuestActivity whereUpdatedAt($value)
 * @mixin         \Eloquent
 */
class GuestActivity extends Model implements HasPresenter
{
    const ACTION_VIEW_LOGIN_PAGE = 1;
    const ACTION_CONNECT_TO_WIFI = 2;
    const ACTION_REGISTERED = 3;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'property_id',
        'device_id',
        'guest_id',
        'guest_device_id',
        'action_id',
        'mac',
        'ip',
    ];

    /**
     * Get number of total impression in the given interval.
     *
     * @param null|int $interval default interval is today
     * @param null     $propertyId
     *
     * @return mixed
     */
    public static function getTotalImpressions($interval = null, $propertyId = null)
    {
        if (!$interval) {
            $interval = Date::RANGE_TODAY;
        }

        $query = static
            ::where('action_id', GuestActivity::ACTION_VIEW_LOGIN_PAGE)
            ->where('tenant_id', activeTenantId())
            ->whereBetween('created_at', Date::getRanges($interval))
            ->groupBy('guest_id');

        if ($propertyId) {
            $query->where('property_id', $propertyId);
        }

        return $query->count();
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return GuestActivityPresenter::class;
    }

    /**
     * Get guest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guest()
    {
        return $this->belongsTo(Models\Guest::class);
    }

    /**
     * Get guest device
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guestDevice()
    {
        return $this->belongsTo(Models\GuestDevice::class);
    }

    /**
     * Get Device
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Models\Device::class);
    }
}
