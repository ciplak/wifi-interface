<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RadiusServer
 *
 * @mixin    \Eloquent
 * @property integer        $id
 * @property integer        $type
 * @property string         $ip
 * @property string         $secret
 * @property integer        $auth_port
 * @property integer        $acc_port
 * @property integer        $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereId($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereType($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereIp($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereAuthPort($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereAccPort($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereStatus($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereCreatedAt($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereUpdatedAt($value)
 * @method   static \Illuminate\Database\Query\Builder|RadiusServer whereSecret($value)
 */
class RadiusServer extends Model
{
    const TYPE_FREERADIUS = 0;
    const TYPE_TEKRADIUS = 1;

    /**
     * The attributes that aren mass assignable
     *
     * @var array
     */
    protected $fillable = ['type', 'ip', 'secret', 'auth_port', 'acc_port', 'status'];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'status' => 1
    ];

    /**
     * Get radius server type options
     *
     * @return array
     */
    public static function getTypeOptions()
    {
        return [
            RadiusServer::TYPE_FREERADIUS => 'FreeRadius',
            RadiusServer::TYPE_TEKRADIUS  => 'TekRadius',
        ];
    }
}