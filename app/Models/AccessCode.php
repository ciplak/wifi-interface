<?php

namespace App\Models;

use App\Presenters\AccessCodePresenter;
use App\Traits\HashId;
use Carbon\Carbon;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * App\Models\AccessCode
 *
 * @property       integer           $id
 * @property       integer           $tenant_id
 * @property       integer           $venue_id
 * @property       integer           $guest_id
 * @property       integer           $service_profile_id
 * @property       integer           $type
 * @property       string            $code
 * @property       Carbon            $expiry_date
 * @property       boolean           $status
 * @property       integer           $use_count
 * @property       Carbon            $logout_time
 * @property       integer           $created_by
 * @property       Carbon            $created_at
 * @property       Carbon            $updated_at
 * @property-read  bool              $is_expired
 * @property-read  string            $expiry_date_formatted
 * @property-read  \App\Models\Guest $guest
 * @property-read  string            $type_name
 * @property-read  ServiceProfile    $serviceProfile
 * @property-read  User              $createdBy
 * @property-read  string            $status_value
 * @property-read  string            $status_value_html
 * @property-read  string            $guest_display_name
 * @property-read  string            $venue_name
 * @property-read  string            service_profile_name
 * @property       string            $end_time
 * @property       string            $created_by_name
 * @property       string            $created_at_formatted
 * @property-read  mixed             $encrypted
 * @property-write mixed             $encrypting
 * @property-write mixed             $hashing
 * @method         static Builder|AccessCode whereId($value)
 * @method         static Builder|AccessCode whereServiceProfileId($value)
 * @method         static Builder|AccessCode whereCode($value)
 * @method         static Builder|AccessCode whereExpiryDate($value)
 * @method         static Builder|AccessCode whereStatus($value)
 * @method         static Builder|AccessCode whereCreatedBy($value)
 * @method         static Builder|AccessCode whereCreatedAt($value)
 * @method         static Builder|AccessCode whereUpdatedAt($value)
 * @method         static \Illuminate\Database\Query\Builder|AccessCode active()
 * @method         static \Illuminate\Database\Query\Builder|AccessCode expired()
 * @method         static \Illuminate\Database\Query\Builder|AccessCode notExpired()
 * @method         static \Illuminate\Database\Query\Builder|Model without($relations)
 * @method         static \Illuminate\Database\Query\Builder|AccessCode whereGuestId($value)
 * @method         static \Illuminate\Database\Query\Builder|AccessCode whereType($value)
 * @method         static \Illuminate\Database\Query\Builder|AccessCode whereUseCount($value)
 * @method         static \Illuminate\Database\Query\Builder|AccessCode whereLogoutTime($value)
 * @method         static \Illuminate\Database\Query\Builder|AccessCode whereTenantId($value)
 * @mixin          \Eloquent
 * @property-read  \App\Models\Venue $venue
 * @method         static \Illuminate\Database\Query\Builder|AccessCode whereEndTime($value)
 * @method         static \Illuminate\Database\Query\Builder|AccessCode whereVenueId($value)
 */
class AccessCode extends Model implements HasPresenter
{
    use BelongsToTenant, HashId;

    // type constants
    const TYPE_SHARED = 1;

    const TYPE_ACCESS_CODE = 2;

    const TYPE_SMS = 3;

    // status constants
    const STATUS_ACTIVE = 1;

    const STATUS_USED = 0;

    // usage types
    const USAGE_TYPE_ONE_TIME = 1;

    const USAGE_TYPE_REUSABLE = 2;

    // access code characters
    const CHARS_NUMERIC = 1;

    const CHARS_ALPHANUMERIC = 2;

    /**
     * The attributes that aren't mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['expiry_date'];

    /**
     * The attributes that should be casted to native types
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * Generate a random code
     *
     * @param int $length
     * @param int $chars
     *
     * @return string
     */
    public static function generate($length = 8, $chars = AccessCode::CHARS_ALPHANUMERIC)
    {

        if ($chars == AccessCode::CHARS_NUMERIC) {
            $final_rand = '';
            for ($i = 0; $i < $length; $i++) {
                $final_rand .= rand(0, 9);
            }

            return $final_rand;
        }

        return strtoupper(substr(md5(time() . rand(10000, 99999) . uniqid(mt_rand())), 0, $length));
    }

    // @todo optimize list
    public static function getDefaultExpiredPeriodOptions()
    {
        return [
            'No expiry date',
            'Next Day',
            'Next week',
            '15 days',
            'Next month',
        ];
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return AccessCodePresenter::class;
    }

    /**
     * Get the venue the access code belongs to
     *
     * @return mixed
     */
    public function venue()
    {
        return $this->belongsTo(Venue::class)
            ->withTrashed();
    }

    /**
     * Get the service profile that owns the access code
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function serviceProfile()
    {
        return $this->belongsTo(ServiceProfile::class)
            ->withTrashed();
    }

    /**
     * Get the user that created the access code
     *
     * @return User|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')
            ->withoutGlobalScope(\HipsterJazzbo\Landlord\Landlord::class)
            ->withTrashed()
            ->select('id', 'name');
    }

    /**
     * Get the guest that owns the access code
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guest()
    {
        return $this->belongsTo(Guest::class)
            ->withoutGlobalScope(\HipsterJazzbo\Landlord\Landlord::class);
    }

    /**
     * Scope a query to only include active (not used) access codes
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', static::STATUS_ACTIVE);
    }

    /**
     * Scope a query to only include expired access codes
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeExpired($query)
    {
        return $query->where(
            function($query) {
                $query->whereNotNull('expiry_date')
                    ->where('expiry_date', '<=', Carbon::today()->toDateString());
            }
        );
    }

    /**
     * Scope a query to only include not expired access codes
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNotExpired($query)
    {
        return $query->where(
            function($query) {
                $query->whereNull('expiry_date')
                    ->orWhere('expiry_date', '>', Carbon::today()->toDateString());
            }
        );
    }
}
