<?php

namespace App\Models;

use App\Presenters\SmsLogPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * \App\Models\SmsLog
 *
 * @property      integer             $id
 * @property      integer             $tenant_id
 * @property      integer             $sms_provider_id
 * @property      boolean             $type
 * @property      boolean             $event_type
 * @property      string              $to
 * @property      string              $message
 * @property      string              $queue_id
 * @property      Collection          $data
 * @property      string              $response
 * @property      boolean             $status
 * @property      \Carbon\Carbon      $created_at
 * @property      \Carbon\Carbon      $updated_at
 * @property-read \Carbon\Carbon $created_at_formatted
 * @property-read int            $credit
 * @property-read string         $ref_no
 * @property-read string         $description
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereTenantId($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereSmsProviderId($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereType($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereEventType($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereTo($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereMessage($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereQueueId($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereData($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereResponse($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereStatus($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|SmsLog whereUpdatedAt($value)
 * @mixin         \Eloquent
 */
class SmsLog extends Model implements HasPresenter
{
    const TYPE_ACCESS_CODE = 1;
    const TYPE_INSTALL_CREDIT = 2;

    const STATUS_NOT_SENT = 0;
    const STATUS_SENT = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'sms_provider_id',
        'type',
        'event_type',
        'to',
        'message',
        'queue_id',
        'data',
        'response',
        'status',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'collection',
    ];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return SmsLogPresenter::class;
    }
}
