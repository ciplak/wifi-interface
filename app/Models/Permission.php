<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

/**
 * App\Models\Permission
 *
 * @property      integer $id
 * @property      string $name
 * @property      string $display_name
 * @property      string $description
 * @property      \Carbon\Carbon $created_at
 * @property      \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @method        static \Illuminate\Database\Query\Builder|Permission whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|Permission whereName($value)
 * @method        static \Illuminate\Database\Query\Builder|Permission whereDisplayName($value)
 * @method        static \Illuminate\Database\Query\Builder|Permission whereDescription($value)
 * @method        static \Illuminate\Database\Query\Builder|Permission whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|Permission whereUpdatedAt($value)
 * @mixin         \Eloquent
 */
class Permission extends EntrustPermission
{
    protected $guarded = ['id'];
}
