<?php

namespace App\Models;

use App\Traits\HashId;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\SmsProvider
 *
 * @property       integer        $id
 * @property       integer        $tenant_id
 * @property       string         $name
 * @property       string         $description
 * @property       integer        $gateway_id
 * @property       array          $parameters
 * @property       \Carbon\Carbon $created_at
 * @property       \Carbon\Carbon $updated_at
 * @property-read  Property  $properties
 * @property-read  string    $gateway_key
 * @property-read  mixed     $encrypted
 * @property-write mixed    $encrypting
 * @property-write mixed    $hashing
 * @method         static Builder|SmsProvider whereId($value)
 * @method         static Builder|SmsProvider whereName($value)
 * @method         static Builder|SmsProvider whereDescription($value)
 * @method         static Builder|SmsProvider whereCreatedAt($value)
 * @method         static Builder|SmsProvider whereUpdatedAt($value)
 * @method         static Builder|SmsProvider whereGatewayId($value)
 * @method         static Builder|SmsProvider whereParameters($value)
 * @method         static Builder|Model without($relations)
 */
class SmsProvider extends Model
{
    use BelongsToTenant, HashId;

    const GATEWAY_CLICKATELL = 1;
    const GATEWAY_TWILIO = 2;
    const GATEWAY_ISMART = 3;
    const GATEWAY_ACCINGE = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'name',
        'description',
        'gateway_id',
        'parameters',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'parameters' => 'array',
    ];

    /**
     * Get properties
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function properties()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Return value of a given parameter of an sms gateway
     *
     * @param string $name
     *
     * @return mixed
     */
    public function parameter($name)
    {
        return $this->parameters[$name] ?? null;
    }

    /**
     * Get gateway key attribute
     *
     * @return string
     */
    public function getGatewayKeyAttribute()
    {
        return config('sms.gateways.' . $this->gateway_id . '.key');
    }
}
