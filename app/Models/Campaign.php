<?php

namespace App\Models;

use App\Presenters\CampaignPresenter;
use App\Repositories\CampaignRepository;
use App\Traits\HashId;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * \App\Models\Campaign
 *
 * @property      integer                 $id
 * @property      string                  $title
 * @property      boolean                 $location_id
 * @property      boolean                 $status
 * @property      boolean                 $parameter_type
 * @property      string                  $parameter_type_date
 * @property      string                  $parameter_type_time
 * @property      integer                 $day_time
 * @property      boolean                 $periodic
 * @property      integer                 $reengage
 * @property      boolean                 $gender
 * @property      boolean                 $age
 * @property      boolean                 $birthday
 * @property      integer                 $visit_count
 * @property      string                  $sms_content
 * @property      string                  $email_content
 * @property      \Carbon\Carbon          $created_at
 * @property      \Carbon\Carbon          $updated_at
 * @property-read Property           $location
 * @property-read string             $location_name
 * @property-read mixed              $hash_id
 * @property-read CampaignRepository $campaign
 * @property-read string             $parameter_name
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereTitle($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereLocationId($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereStatus($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereParameterType($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereParameterTypeDate($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereParameterTypeTime($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereDayTime($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign wherePeriodic($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereReengage($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereGender($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereAge($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereBirthday($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereVisitCount($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereSmsContent($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereEmailContent($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\Campaign whereUpdatedAt($value)
 * @mixin         \Eloquent
 */
class Campaign extends Model implements HasPresenter
{
    use HashId, SoftDeletes, BelongsToTenant;

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    const PERIODIC_TYPE_WEEKLY = 1;
    const PERIODIC_TYPE_MONTHLY = 2;
    const PERIODIC_TYPE_ANNUALLY = 3;

    const PARAMETER_TYPE_DATE_TIME = 1;
    const PARAMETER_TYPE_PERIODIC = 2;

    const GENDER_TYPE_UNKNOWN = 0;
    const GENDER_TYPE_MALE = 1;
    const GENDER_TYPE_FEMALE = 2;

    const AGE_TYPE_UNKNOWN_AGE = 0;
    const AGE_TYPE_LESS_THAN = 1;
    const AGE_TYPE_MORE_THAN = 2;
    const AGE_TYPE_RANGE = 3;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['start_date', 'end_date'];

    /**
     * @var array
     */
    protected $casts = ['reengage' => 'integer'];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return CampaignPresenter::class;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Property::class);
    }
}
