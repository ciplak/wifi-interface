<?php

namespace App\Models;

use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Poll
 *
 * @property      integer                                              $id
 * @property      integer                                              $parent_id
 * @property      string                                               $title
 * @property      integer                                              $votes
 * @property      boolean                                              $order_no
 * @property      boolean                                              $status
 * @property      \Carbon\Carbon                                       $created_at
 * @property      \Carbon\Carbon                                       $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Poll[] $options
 * @method        static \Illuminate\Database\Query\Builder|Poll whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|Poll whereParentId($value)
 * @method        static \Illuminate\Database\Query\Builder|Poll whereTitle($value)
 * @method        static \Illuminate\Database\Query\Builder|Poll whereVotes($value)
 * @method        static \Illuminate\Database\Query\Builder|Poll whereOrderNo($value)
 * @method        static \Illuminate\Database\Query\Builder|Poll whereStatus($value)
 * @method        static \Illuminate\Database\Query\Builder|Poll whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|Poll whereUpdatedAt($value)
 * @mixin         \Eloquent
 */
class Poll extends Model
{
    use BelongsToTenant;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'title',
        'votes',
        'order_bo',
        'status',
    ];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'parent_id' => null,
        'votes'     => 0,
        'order_no'  => 0,
        'status'    => 1,
    ];

    /**
     * Ger poll options
     *
     * @return mixed
     */
    public function options()
    {
        return $this->hasMany(Poll::class, 'parent_id')->orderBy('order_no')->orderBy('title');
    }

    /**
     * Get total vote count for the poll
     *
     * @return int
     */
    public function getTotalVotes()
    {
        return (int)$this->options->sum('votes');
    }
}
