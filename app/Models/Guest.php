<?php

namespace App\Models;

use App\ConnectionStatus;
use App\Presenters\GuestPresenter;
use App\Traits\HashId;
use Carbon\Carbon;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\Notifiable;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * App\Models\Guest
 *
 * @property       integer                                                              $id
 * @property       integer                                                              $tenant_id
 * @property       integer                                                              $venue_id
 * @property       string                                                               $name
 * @property       string                                                               $username
 * @property       string                                                               $email
 * @property       string                                                               $birthday
 * @property       integer                                                              $age_range
 * @property       boolean                                                              $gender
 * @property       integer                                                              $facebook_id
 * @property       integer                                                              $twitter_id
 * @property       integer                                                              $instagram_id
 * @property       integer                                                              $google_id
 * @property       integer                                                              $linkedin_id
 * @property       integer                                                              $foursquare_id
 * @property       string                                                               $passport_no
 * @property       string                                                               $phone
 * @property       string                                                               $nationality
 * @property       string                                                               $custom_data
 * @property       string                                                               $custom_field_1
 * @property       string                                                               $custom_field_2
 * @property       string                                                               $custom_field_3
 * @property       \Carbon\Carbon                                                       $smart_login_time
 * @property       \Carbon\Carbon                                                       $login_time
 * @property       \Carbon\Carbon                                                       $last_visit_time
 * @property       int                                                                  $total_visits
 * @property       int                                                                  $total_duration
 * @property       int                                                                  $total_downloaded_data
 * @property       int                                                                  $total_uploaded_data
 * @property       boolean                                                              $connection_status
 * @property       boolean                                                              $direct_login
 * @property       boolean                                                              $status
 * @property       string                                                               $avatar
 * @property       string                                                               $access_code
 * @property       \Carbon\Carbon                                                       $created_at
 * @property       \Carbon\Carbon                                                       $updated_at
 * @property-read  string                                                               $display_name
 * @property-read  string                                                               $guest_device_ip
 * @property-read  string                                                               $duration
 * @property-read  string                                                               $downloaded_data_human_readable
 * @property-read  string                                                               $uploaded_data_human_readable
 * @property-read  string                                                               $birthday_formatted
 * @property-read  string                                                               $birthday_picker_formatted
 * @property-read  string                                                               $age
 * @property-read  string                                                               $gender_name
 * @property-read  string                                                               $gender_icon
 * @property-read  string                                                               $social_icon
 * @property-read  string                                                               $avatar_picture
 * @property-read  string                                                               $email_link
 * @property-read  string                                                               $connection_status_class
 * @property-read  string                                                               $connection_status_value
 * @property-read  bool                                                                 $direct_login_enabled
 * @property-read  bool                                                                 $is_online
 * @property-read  AccessCode[]                                                         $accessCodes
 * @property-read  Venue                                                                $venue
 * @property-read  string                                                               $venue_name
 * @property-read  string                                                               $password
 * @property-read  mixed                                                                $nationality_name
 * @property-read  \Carbon\Carbon                                                       $last_visit_time_formatted
 * @property-read  \Carbon\Carbon                                                       $created_at_formatted
 * @property-read  GuestDevice[]                                                        $devices
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\WifiSession[]   $sessions
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\GuestActivity[] $actions
 * @property-read  mixed                                                                $encrypted
 * @property-write mixed                                                                $encrypting
 * @property-write mixed                                                                $hashing
 * @method         static Builder|Guest whereId($value)
 * @method         static Builder|Guest whereName($value)
 * @method         static Builder|Guest whereLastName($value)
 * @method         static Builder|Guest whereUsername($value)
 * @method         static Builder|Guest whereEmail($value)
 * @method         static Builder|Guest whereBirthday($value)
 * @method         static Builder|Guest whereGender($value)
 * @method         static Builder|Guest whereIsAnonymous($value)
 * @method         static Builder|Guest whereFacebookId($value)
 * @method         static Builder|Guest whereTwitterId($value)
 * @method         static Builder|Guest wherePassportNo($value)
 * @method         static Builder|Guest wherePhone($value)
 * @method         static Builder|Guest whereLoginType($value)
 * @method         static Builder|Guest whereLoginTime($value)
 * @method         static Builder|Guest whereConnectionStatus($value)
 * @method         static Builder|Guest whereStatus($value)
 * @method         static Builder|Guest whereAvatar($value)
 * @method         static Builder|Guest whereCreatedAt($value)
 * @method         static Builder|Guest whereUpdatedAt($value)
 * @method         static Builder|Model without($relations)
 * @method         static Builder|Guest whereAgeRange($value)
 * @method         static Builder|Guest whereNationality($value)
 * @method         static Builder|Guest whereCustomData($value)
 * @method         static Builder|Guest whereLastVisitTime($value)
 * @method         static Builder|Guest whereTotalVisits($value)
 * @method         static Builder|Guest whereTotalDuration($value)
 * @method         static Builder|Guest whereTotalDownloadedData($value)
 * @method         static Builder|Guest whereTotalUploadedData($value)
 * @method         static Builder|Guest whereDirectLogin($value)
 * @method         static Builder|Guest whereTenantId($value)
 * @mixin          \Eloquent
 * @method         static \Illuminate\Database\Query\Builder|Guest whereCustomField1($value)
 * @method         static \Illuminate\Database\Query\Builder|Guest whereCustomField2($value)
 * @method         static \Illuminate\Database\Query\Builder|Guest whereCustomField3($value)
 * @method         static \Illuminate\Database\Query\Builder|Guest whereVenueId($value)
 */
class Guest extends Model implements HasPresenter
{
    use Notifiable, BelongsToTenant, HashId;

    const GENDER_UNKNOWN = 0;

    const GENDER_MALE = 1;

    const GENDER_FEMALE = 2;

    const AGE_RANGE_UNKNOWN = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'venue_id',
        'name',
        'email',
        'facebook_id',
        'twitter_id',
        'instagram_id',
        'google_id',
        'linkedin_id',
        'foursquare_id',
        'avatar',
        'username',
        'birthday',
        'age_range',
        'passport_no',
        'phone',
        'gender',
        'nationality',
        'custom_data',
        'custom_field_1',
        'custom_field_2',
        'custom_field_3',
        'direct_login',
        'smart_login_time',
        'access_code',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['smart_login_time', 'login_time', 'last_visit_time'];

    /**
     * Default values for model attributes
     *
     * @var array
     */
    protected $attributes = [
        'status'            => 1,
        'gender'            => Guest::GENDER_UNKNOWN,
        'connection_status' => ConnectionStatus::DISCONNECTED,
    ];

    /**
     * The attributes that should be casted to native types
     *
     * @var array
     */
    protected $casts = [
        'custom_data'  => 'array',
        'direct_login' => 'integer',
    ];

    /**
     * Get gender options for guests
     *
     * @return array
     */
    public static function getGenderOptions()
    {
        $options = [
            static::GENDER_UNKNOWN => trans('guest.gender.unknown'),
            static::GENDER_MALE    => trans('guest.gender.male'),
            static::GENDER_FEMALE  => trans('guest.gender.female'),
        ];

        return $options;
    }

    /**
     * Get nationality options
     *
     * @return array
     */
    public static function getNationalityOptions()
    {
        $codes = array_keys(config('countries.names'));

        $options = collect($codes)->reduce(
            function($nationalities, $key) {
                $nationalities[$key] = trans('login.form.nationality_options.' . $key);

                return $nationalities;
            }
        );

        return $options;
    }

    /**
     * @inheritdoc
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(
            function(Guest $model) {

                $model->gender = (int)$model->gender;

                // calculate age range if birthday is given
                if (!is_null($model->birthday)) {
                    $model->age_range = static::getAgeRange($model->birthday);
                }

                if (!$model->exists) {
                    $model->username = static::generateUserName();
                }
            }
        );
    }

    /**
     * Get age range from birthday
     *
     * @param $birthday
     *
     * @return int
     */
    public static function getAgeRange($birthday)
    {
        $age = 0;

        if ($birthday) {
            if ($birthday instanceof Carbon) {
                $age = $birthday->age;
            } else {
                if ($birthday instanceof \DateTime) {
                    $birthday = new Carbon($birthday->format(DATE_ISO8601));
                    $age = $birthday->age;
                } else {
                    $age = Carbon::createFromFormat('Y-m-d', $birthday)->age;
                }
            }
        }

        switch (true) {
            case $age >= 21 && $age < 36:
                $range = 1;
                break;

            case $age >= 36 && $age < 40:
                $range = 2;
                break;

            case $age >= 40 && $age <= 55:
                $range = 3;
                break;

            case $age > 55:
                $range = 4;
                break;

            default:
                $range = 0;
        }

        return $range;
    }

    /**
     * Generate a random user name
     *
     * @param int $length
     *
     * @return string
     */
    public static function generateUserName($length = 8)
    {
        return strtoupper(substr(md5(time() . rand(10000, 99999) . uniqid(mt_rand())), 0, $length));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function venue()
    {
        return $this->belongsTo(Venue::class)
            ->withTrashed();
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return GuestPresenter::class;
    }

    /**
     * Get guest devices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(GuestDevice::class);
    }

    /**
     * Get guest actions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function actions()
    {
        return $this->hasMany(GuestActivity::class);
    }

    /**
     * Get guest sessions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany(WifiSession::class);
    }

    /**
     * Get guest's password
     *
     * @return string
     */
    public function getPasswordAttribute()
    {
        return $this->username;
    }

    /**
     * Get direct login status
     *
     * @return bool
     */
    public function getDirectLoginEnabledAttribute()
    {
        return $this->direct_login;
    }

    /**
     * Get guest access codes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accessCodes()
    {
        return $this->hasMany(AccessCode::class);
    }

    /**
     * Check service profile type for the guest and return whether the guest can connect with the same service profile or not
     *
     * @param ServiceProfile $serviceProfile
     *
     * @return bool
     */
    public function canConnectWithServiceProfile(ServiceProfile $serviceProfile)
    {
        if ($serviceProfile->type == ServiceProfile::TYPE_UNRESTRICTED) {
            return true;
        }

        log_info(
            'Checking service profile type', [
                'guest id'           => $this->id,
                'service profile id' => $serviceProfile->id,
                'type'               => $serviceProfile->type_value,
            ]
        );

        $query = $this->sessions()
            ->orderBy('id', 'DESC')
            ->where('service_profile_id', $serviceProfile->id);

        if ($serviceProfile->type == ServiceProfile::TYPE_RECURRING) {
            $query->where('start_time', '>', Carbon::now()->subHour($serviceProfile->calculateRenewalFrequencyHours()));
        }

        return (int)$query->value('id') == 0;
    }
}
