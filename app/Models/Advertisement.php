<?php

namespace App\Models;

use App\Presenters\AdvertisementPresenter;
use App\Traits\HashId;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * App\Models\Advertisement
 *
 * @property      integer        $id
 * @property      string         $title
 * @property      integer        $type
 * @property      string         $body
 * @property      string         $image
 * @property      string         $url
 * @property      string         $position
 * @property      \Carbon\Carbon $start_date
 * @property      \Carbon\Carbon $end_date
 * @property      integer        $status
 * @property      \Carbon\Carbon $created_at
 * @property      \Carbon\Carbon $updated_at
 * @property-read string    $type_value
 * @property-read string    $image_url
 * @property-read string    $status_icon
 * @property-read string    $start_date_value
 * @property-read string    $end_date_value
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereTitle($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereType($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereBody($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereImage($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereUrl($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement wherePosition($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereStatus($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|Advertisement whereUpdatedAt($value)
 * @mixin         \Eloquent
 */
class Advertisement extends Model implements HasPresenter
{
    use BelongsToTenant, HashId;

    const TYPE_TEXT = 0;
    const TYPE_IMAGE = 1;

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'end_date'];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'status' => self::STATUS_DRAFT,
        'type'   => self::TYPE_TEXT,
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(
            function (Advertisement $model) {

                if ($model->type == Advertisement::TYPE_IMAGE) {
                    $model->body = null;
                } else {
                    $model->image = null;
                }
            }
        );
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return AdvertisementPresenter::class;
    }
}