<?php

namespace App\Models;

use App\Presenters\AlertMessagePresenter;
use HipsterJazzbo\Landlord\BelongsToTenant;
use McCool\LaravelAutoPresenter\HasPresenter;
use Illuminate\Database\Eloquent\Model;

/**
 * \App\Models\AlertMessage
 *
 * @property      integer        $id
 * @property      integer        $tenant_id
 * @property      integer        $priority
 * @property      string         $message
 * @property      string         $description
 * @property      integer        $status
 * @property      \Carbon\Carbon $created_at
 * @property      \Carbon\Carbon $updated_at
 * @property-read string    $priority_value
 * @property-read string    $priority_label_type
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage wherePriority($value)
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage whereMessage($value)
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage whereDescription($value)
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage whereUpdatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage whereStatus($value)
 * @method        static \Illuminate\Database\Query\Builder|AlertMessage whereTenantId($value)
 * @mixin         \Eloquent
 */
class AlertMessage extends Model implements HasPresenter
{

    use BelongsToTenant;

    const PRIORITY_LOW = 1;
    const PRIORITY_MEDIUM = 30;
    const PRIORITY_HIGH = 60;

    const STATUS_PENDING = 0;
    const STATUS_EMAIL_SENT = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'priority',
        'message',
        'description',
        'status',
    ];

    /**
     * The model's default attribute values
     *
     * @var array
     */
    protected $attributes = [
        'status' => AlertMessage::STATUS_PENDING,
    ];

    /**
     * Create an alert message record with low priority
     *
     * @param string $message
     * @param array  $parameters
     *
     * @return AlertMessage
     */
    public static function low($message, $parameters = [])
    {
        return static::set(AlertMessage::PRIORITY_LOW, $message, $parameters);
    }

    /**
     * Create an alert message record
     *
     * @param int    $priority
     * @param string $messages
     * @param array  $parameters
     *
     * @return static
     */
    public static function set($priority, $messages, $parameters = [])
    {
        \Log::error($messages, $parameters);

        return AlertMessage::create(
            [
            'priority'    => $priority,
            'message'     => $messages,
            'description' => $parameters ? json_encode($parameters) : '',
            ]
        );
    }

    /**
     * Create an alert message record with medium priority
     *
     * @param string $message
     * @param array  $parameters
     *
     * @return AlertMessage
     */
    public static function medium($message, $parameters = [])
    {
        return static::set(AlertMessage::PRIORITY_MEDIUM, $message, $parameters);
    }

    /**
     * Create an alert message record with high priority
     *
     * @param string $message
     * @param array  $parameters
     *
     * @return AlertMessage
     */
    public static function high($message, $parameters = [])
    {
        return static::set(AlertMessage::PRIORITY_HIGH, $message, $parameters);
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return AlertMessagePresenter::class;
    }

}
