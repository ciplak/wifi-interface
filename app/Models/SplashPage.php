<?php

namespace App\Models;

use App\Presenters\SplashPagePresenter;
use App\Services\SplashPageRenderer;
use App\Traits\HashId;
use HipsterJazzbo\Landlord\BelongsToTenant;
use McCool\LaravelAutoPresenter\HasPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\MessageBag;

/**
 * App\Models\SplashPage
 *
 * @property       integer                                                      $id
 * @property       integer                                                      $tenant_id
 * @property       integer                                                      $venue_id
 * @property       integer                                                      $parent_id
 * @property       string                                                       $locale
 * @property       string                                                       $name
 * @property       string                                                       $description
 * @property       string                                                       $settings
 * @property       boolean                                                      $status
 * @property       integer                                                      $type
 * @property       integer                                                      $login_field
 * @property       bool                                                         $show_sms_code
 * @property       \Carbon\Carbon                                               $created_at
 * @property       \Carbon\Carbon                                               $updated_at
 * @property       integer                                                      $created_by
 * @property       integer                                                      $updated_by
 * @property       string                                                       $title
 * @property       string                                                       $logo
 * @property       string                                                       $custom_html
 * @property       string                                                       $custom_css
 * @property       string                                                       $custom_js
 * @property       string                                                       $compiled_html
 * @property-read  mixed                                                        $encrypted
 * @property-read  bool                                                         $is_global
 * @property-read  Tenant                                                       $tenant
 * @property-read  User                                                         $createdBy
 * @property-read  string                                                       $type_name
 * @property-read  string                                                       $created_by_name
 * @property-read  string                                                       $updated_by_name
 * @property-read  User                                                         $updatedBy
 * @property-read  string                                                       $login_field_label
 * @property-read  string                                                       $created_at_formatted
 * @property       boolean                                                      $send_sms
 * @property       boolean                                                      $send_email
 * @property       boolean                                                      $social_login_requires_code
 * @property       boolean                                                      $overwrite_guests
 * @property-write mixed                                                        $encrypting
 * @property-write mixed                                                        $hashing
 * @property-read  string                                                       $hashed_id
 * @method         static Builder|SplashPage whereId($value)
 * @method         static Builder|SplashPage whereName($value)
 * @method         static Builder|SplashPage whereDescription($value)
 * @method         static Builder|SplashPage whereSettings($value)
 * @method         static Builder|SplashPage whereStatus($value)
 * @method         static Builder|SplashPage whereLoginType($value)
 * @method         static Builder|SplashPage wherePostLoginType($value)
 * @method         static Builder|SplashPage whereCreatedAt($value)
 * @method         static Builder|SplashPage whereUpdatedAt($value)
 * @method         static Builder|Model without($relations)
 * @method         static Builder|SplashPage whereTitle($value)
 * @method         static Builder|SplashPage whereLogo($value)
 * @method         static Builder|SplashPage whereLoginField($value)
 * @method         static Builder|SplashPage whereShowSmsCode($value)
 * @method         static Builder|SplashPage whereCustomCss($value)
 * @method         static Builder|SplashPage whereCreatedBy($value)
 * @method         static Builder|SplashPage whereUpdatedBy($value)
 * @method         static Builder|SplashPage whereCustomHtml($value)
 * @method         static Builder|SplashPage whereTenantId($value)
 * @mixin          \Eloquent
 * @method         static Builder|SplashPage whereParentId($value)
 * @method         static Builder|SplashPage whereVenueId($value)
 * @method         static Builder|SplashPage whereCustomJs($value)
 * @method         static Builder|SplashPage whereCompiledHtml($value)
 * @method         static Builder|SplashPage whereType($value)
 * @method         static Builder|SplashPage whereSendSms($value)
 * @method         static Builder|SplashPage whereSendEmail($value)
 * @method         static Builder|SplashPage whereLanguageCode($value)
 * @method         static Builder|SplashPage whereLocale($value)
 * @property-read  SplashPage                                                   $parent
 * @property-read  string                                                       $type_icon
 * @property-read  \Illuminate\Database\Eloquent\Collection|SplashPage[]        $translations
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 */
class SplashPage extends Model implements HasPresenter
{
    use BelongsToTenant, HashId;

    const TYPE_OFFLINE = 0;

    const TYPE_ONLINE = 1;

    const LOGIN_TYPE_DIRECT_LOGIN = 1;

    const LOGIN_TYPE_SMS = 2;

    const LOGIN_TYPE_REGISTRATION = 3;

    const LOGIN_TYPE_ACCESS_CODE = 4;

    const LOGIN_TYPE_NO_SERVICE_PIN = 5;

    const LOGIN_TYPE_SMART_LOGIN = 6;

    const LOGIN_TYPE_CLICK_THROUGH = 7;

    const LOGIN_TYPE_SOCIAL = 100;

    const LOGIN_TYPE_FACEBOOK = 101;

    const LOGIN_TYPE_TWITTER = 102;

    const LOGIN_TYPE_LOGIN_FORM = 255;

    const STATUS_PUBLISHED = 1;

    const STATUS_DRAFT = 2;

    const STATUS_AUTO_DRAFT = 3;

    const FIELD_TYPE_TEXT = 1;

    const FIELD_TYPE_SELECT = 2;

    const LOGIN_FIELD_EMAIL = 0;

    const LOGIN_FIELD_PHONE = 1;

    const LOGIN_FIELD_USERNAME = 2;

    const LOGIN_FIELD_PASSPORT_NO = 3;

    public $table = 'login_pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'settings',
        'login_field',
        'show_sms_code',
        'send_sms',
        'send_email',
        'social_login_requires_code',
        'overwrite_guests',
        'title',
        'tenant_id',
        'venue_id',
        'custom_html',
        'custom_css',
        'custom_js',
        'compiled_html',
        'type',
        'parent_id',
        'locale',
    ];

    /**
     * Default values for model attributes
     *
     * @var array
     */
    protected $attributes = [
        'status'                     => SplashPage::STATUS_PUBLISHED,
        'locale'                     => 'en',
        'social_login_requires_code' => 1,
        'custom_html'                => '<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
</head>
<body>

</body>
</html>',
    ];

    /**
     * The attributes that should be casted to native types
     *
     * @var array
     */
    protected $casts = [
        'settings'      => 'array',
        'title'         => 'array',
        'show_sms_code' => 'boolean',
    ];

    /**
     * @var \Illuminate\Support\MessageBag
     */
    protected $validationErrors = null;

    /**
     * Get tenant
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    /**
     * Get parent login page
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(SplashPage::class, 'parent_id');
    }

    /**
     * Get translations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(SplashPage::class, 'parent_id');
    }

    /**
     * Set the polymorphic relation.
     *
     * @return mixed
     */
    public function media()
    {
        return $this->morphMany(Media::class, 'model');
    }

    /**
     * Get login page type options
     *
     * @return array
     */
    public static function getTypeOptions()
    {
        return [
            static::TYPE_OFFLINE => trans('splash_page.type.offline'),
            static::TYPE_ONLINE  => trans('splash_page.type.online'),
        ];
    }

    /**
     * @inheritdoc
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(
            function (SplashPage $model) {
                $model->created_by = \Auth::id();
            }
        );

        static::saving(
            function (SplashPage $model) {
                $model->compiled_html = (new SplashPageRenderer($model))->compile();

                $model->updated_by = \Auth::id();
            }
        );
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return SplashPagePresenter::class;
    }

    /**
     * Get the user created the login page
     *
     * @return mixed
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')
            ->withoutGlobalScope(\HipsterJazzbo\Landlord\Landlord::class)
            ->withTrashed()
            ->select('id', 'name');
    }

    /**
     * Get the user updated the login page
     *
     * @return mixed
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by')
            ->withoutGlobalScope(\HipsterJazzbo\Landlord\Landlord::class)
            ->withTrashed()
            ->select('id', 'name');
    }

    /**
     * @return array
     */
    public function getRegistrationFields()
    {
        $fields = [
            'name'        => [
                'rules' => ['min:5'],
            ],
            'email'       => [
                'rules' => ['email'],
            ],
            'birthday'    => [
                'type'  => 'date',
                'rules' => ['date_format:d/m/Y'],
            ],
            'phone'       => [
                'rules' => ['numeric'],
            ],
            'passport_no' => [
                'rules' => ['alpha_num'],
            ],
            'gender'      => [
                'type' => SplashPage::FIELD_TYPE_SELECT,
            ],

            'nationality' => [
                'type' => SplashPage::FIELD_TYPE_SELECT,
            ],
        ];

        for ($i = 1; $i <= config('main.custom_field_count'); $i++) {
            $fields["custom_field_{$i}"] = ['custom' => true];
        }

        return $fields;
    }

    /**
     * Return options for the given field's select box
     *
     * @param string $fieldName
     *
     * @return array
     */
    public static function getOptionsForField($fieldName)
    {
        $options = [
            'gender'      => [null => trans('guest.gender.select')] + Guest::getGenderOptions(),
            'nationality' => [null => trans('login.form.select_nationality')] + Guest::getNationalityOptions(),
        ];

        return $options[$fieldName] ?? [];
    }

    /**
     * Get a page setting value
     *
     * @param string $name
     * @param string $default
     *
     * @return null
     */
    public function setting($name, $default = null)
    {
        return $this->settings[$name] ?? $default;
    }

    /**
     * Get fields available in login form
     *
     * @return array
     */
    public function getLoginFields()
    {
        $fields = [
            'email'       => [
                'rules' => ['email'],
            ],
            'phone'       => [
                'rules' => ['numeric'],
            ],
            'passport_no' => [
                'rules' => ['alpha_num'],
            ],
        ];

        for ($i = 1; $i <= config('main.custom_field_count'); $i++) {
            $fields["custom_field_{$i}"] = ['custom' => true];
        }

        return $fields;
    }

    /**
     * Get validation errors
     *
     * @return MessageBag
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * Check if the splash page is global
     *
     * @return bool
     */
    public function getIsGlobalAttribute()
    {
        return $this->exists && is_null($this->venue_id);
    }

    /**
     * @return bool
     */
    public function canManageGlobal()
    {
        return $this->is_global && \Entrust::can('login-pages.manage-global');
    }

    /**
     * @return array
     */
    public function getTranslationList()
    {
        return $this->where('parent_id', $this->id)
            ->pluck('id', 'locale')
            ->toArray();
    }

    /**
     * Get translations for the splash page
     *
     * @param string $locale
     *
     * @return $this|SplashPage
     */
    public function getTranslation($locale)
    {
        if ($this->locale == $locale) {
            return $this;
        }

        $translation = $this->where('parent_id', $this->id)
            ->where('locale', $locale)
            ->first();

        return $translation ?: $this;
    }

    /**
     * Return if the splash page is used by a property
     *
     * @return bool
     */
    public function isInUse()
    {
        $count = \DB::table('properties')
            ->where('login_page_id', $this->id)
            ->orWhere('post_login_page_id', $this->id)
            ->count();

        return $count > 0;
    }

    /**
     * Get enabled fields of registration fields
     *
     * @return array
     */
    public function getEnabledRegistrationFields()
    {
        $fields = collect($this->settings['registration_fields'])
            ->filter(function ($item) {
                return $item['enabled'];
            });

        return $fields->toArray();
    }

    /**
     * Get enabled fields of login fields
     *
     * @return array
     */
    public function getEnabledLoginFields()
    {
        $fields = collect($this->settings['login_fields'])
            ->filter(function ($item) {
                return $item['enabled'] ?? false;
            });

        return $fields->toArray();
    }

    /**
     * Get list of ids of translations of the splash page
     *
     * @param bool $encode
     *
     * @return \Illuminate\Support\Collection|array
     */
    public function getTranslationsIdList($encode = true)
    {
        $result = $this->translations->pluck('id', 'locale');

        if ($encode) {
            $result = $result->map(function ($item) {
                return hashids_encode($item);
            });
        }

        return $result;
    }
}