<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'name',
        'value',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'collection',
    ];

    public $timestamps = false;

    /**
     * Scope a query to only include global settings
     *
     * @param $query
     *
     * @return Builder
     */
    public function scopeGlobal(Builder $query)
    {
        return $query->whereNull('tenant_id');
    }

    /**
     * Scope a query to only include global settings
     *
     * @param Builder $query
     * @param int     $tenantId
     *
     * @return Builder
     */
    public function scopeForTenant(Builder $query, int $tenantId)
    {
        return $query->where('tenant_id', $tenantId);
    }
}
