<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * App\Models\DeviceModel
 *
 * @property      int                                               $id
 * @property      int                                               $device_type_id
 * @property      int                                               $device_vendor_id
 * @property      string                                            $name
 * @property      int                                               $device_handler_id
 * @property-read string                                            $type_value
 * @property-read string                                            $handler_display_name
 * @property-read string                                            $handler_class_name
 * @property-read DeviceVendor                                      $vendor
 * @property-read \Illuminate\Database\Eloquent\Collection|Device[] $devices
 * @property-read mixed                                             $full_name
 * @method        static \Illuminate\Database\Query\Builder|DeviceModel whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|DeviceModel whereDeviceTypeId($value)
 * @method        static \Illuminate\Database\Query\Builder|DeviceModel whereDeviceVendorId($value)
 * @method        static \Illuminate\Database\Query\Builder|DeviceModel whereName($value)
 * @mixin         \Eloquent
 */
class DeviceModel extends Model
{
    const DEVICE_TYPE_WLC = 1;
    const DEVICE_TYPE_ACCESS_POINT = 2;

    const VENDOR_CISCO_WLC = 1;
    const VENDOR_XIRRUS = 2;
    const VENDOR_AVAYA = 3;
    const VENDOR_ARUBA = 4;
    const VENDOR_MERAKI = 5;
    const VENDOR_FORTINET = 6;
    const VENDOR_ARUBA_CONTROLLER = 7;

    private $deviceHandlers = [
        self::VENDOR_CISCO_WLC        => ['title' => 'Cisco WLC', 'class' => 'Cisco'],
        self::VENDOR_MERAKI           => ['title' => 'Meraki', 'class' => 'Meraki'],
        self::VENDOR_XIRRUS           => ['title' => 'Xirrus/Avaya AP', 'class' => 'Xirrus'],
        self::VENDOR_ARUBA            => ['title' => 'Aruba (IAP)', 'class' => 'Aruba'],
        self::VENDOR_ARUBA_CONTROLLER => ['title' => 'Aruba Controller', 'class' => 'ArubaController'],
        self::VENDOR_FORTINET         => ['title' => 'Fortinet', 'class' => 'Fortinet'],
    ];

    public $timestamps = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'device_type_id',
        'device_vendor_id',
        'name',
        'device_type_id',
        'device_handler_id',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'vendor_name',
        'type_value',
        'handler_display_name',
    ];

    /**
     * Get vendor of the device model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(DeviceVendor::class, 'device_vendor_id');
    }

    /**
     * Get devices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    /**
     * Get vendor and model name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->vendor->name . ' ' . $this->name;
    }

    /**
     * Get model type value
     *
     * @return string
     */
    public function getTypeValueAttribute()
    {
        $types = static::getTypeOptions();

        return $types[$this->device_type_id] ?? '';
    }

    /**
     * Get model type options
     *
     * @return array
     */
    public static function getTypeOptions()
    {
        return [
            static::DEVICE_TYPE_WLC          => trans('device_model.type.wlc'),
            static::DEVICE_TYPE_ACCESS_POINT => trans('device_model.type.access_point'),
        ];
    }

    /**
     * Get device handler options
     *
     * @return \Illuminate\Support\Collection
     */
    public function getHandlerOptions()
    {
        return collect($this->deviceHandlers)->map(
            function ($item) {
                return $item['title'];
            }
        );
    }

    /**
     * Get device handler display name
     *
     * @return string
     */
    public function getHandlerDisplayNameAttribute()
    {
        return Arr::get($this->deviceHandlers, "{$this->device_handler_id}.title");
    }

    /**
     * Get device handler class name
     *
     * @return string
     */
    public function getHandlerClassNameAttribute()
    {
        return Arr::get($this->deviceHandlers, "{$this->device_handler_id}.class");
    }

    /**
     * Get vendor name
     *
     * @return string
     */
    public function getVendorNameAttribute()
    {
        return $this->vendor->name;
    }
}