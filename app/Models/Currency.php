<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;


/**
 * App\Models\Currency
 *
 * @property       integer     $id
 * @property       string      $name
 * @property       string      $description
 * @property       string      $symbol
 * @property       boolean     $is_prefix
 * @method         static Builder|Currency whereId($value)
 * @method         static Builder|Currency whereName($value)
 * @method         static Builder|Currency whereDescription($value)
 * @method         static Builder|Currency whereSymbol($value)
 * @method         static Builder|Currency whereIsPrefix($value)
 * @property-read  mixed  $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method         static \Illuminate\Database\Query\Builder|Model without($relations)
 */
class Currency extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'symbol', 'is_prefix'];
}
