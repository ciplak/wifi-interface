<?php

namespace App\Models;

use App\Traits\HashId;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Device
 *
 * @mixin          \Eloquent
 * @property       integer                         $id
 * @property       integer                         $tenant_id
 * @property       string                          $name
 * @property       string                          $description
 * @property       string                          $mac
 * @property       string                          $ip
 * @property       integer                         $property_id
 * @property       integer                         $device_model_id
 * @property       \Carbon\Carbon                  $created_at
 * @property       \Carbon\Carbon                  $updated_at
 * @property-read  Property                   $property
 * @property-read  DeviceModel                $model
 * @method         static Builder|Device whereId($value)
 * @method         static Builder|Device whereName($value)
 * @method         static Builder|Device whereDescription($value)
 * @method         static Builder|Device whereMac($value)
 * @method         static Builder|Device whereIp($value)
 * @method         static Builder|Device wherePropertyId($value)
 * @method         static Builder|Device whereCreatedAt($value)
 * @method         static Builder|Device whereUpdatedAt($value)
 * @property       \Carbon\Carbon                  $deleted_at
 * @property-read  \App\Models\ServiceProfile $serviceProfile
 * @property-read  mixed                      $encrypted
 * @property-write mixed                     $encrypting
 * @property-write mixed                     $hashing
 * @method         static \Illuminate\Database\Query\Builder|Model without($relations)
 * @method         static \Illuminate\Database\Query\Builder|Device whereDeviceModelId($value)
 * @method         static \Illuminate\Database\Query\Builder|Device whereDeletedAt($value)
 * @method         static \Illuminate\Database\Query\Builder|Device whereTenantId($value)
 * @property       integer                         $parent_id
 * @method         static \Illuminate\Database\Query\Builder|\App\Models\Device whereParentId($value)
 */
class Device extends Model
{
    use SoftDeletes, BelongsToTenant, HashId;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'name',
        'description',
        'mac',
        'ip',
        'property_id',
        'device_model_id',
    ];

    /**
     * Get device status options
     *
     * @return array
     */
    public static function getStatusOptions()
    {
        return [
            static::STATUS_DELETED => trans('device.status.deleted'),
            static::STATUS_ACTIVE  => trans('device.status.active'),
        ];
    }

    /**
     * Get property
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class)->withTrashed();
    }

    /**
     * Get device model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(DeviceModel::class, 'device_model_id');
    }

    /**
     * Set property
     *
     * @param int $value
     */
    public function setPropertyIdAttribute($value)
    {
        if (!$value) {
            $value = null;
        }
        $this->attributes['property_id'] = $value;
    }

    /**
     * Clean mask chars and set IP
     *
     * @param $value
     */
    public function setIpAttribute($value)
    {
        $this->attributes['ip'] = strtr($value, ['_' => '']);
    }

    /**
     * Get device handler name
     *
     * @return string
     */
    public function getDeviceHandlerClassName()
    {
        return $this->model->handler_class_name;
    }
}
