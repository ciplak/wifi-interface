<?php

namespace App\Models;

use App\Presenters\VisitedUrlPresenter;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * App\Models\VisitedUrlLog
 *
 * @property int            $id
 * @property string         $source_ip
 * @property int            $source_port
 * @property string         $destination_ip
 * @property int            $destination_port
 * @property string         $ap_mac
 * @property string         $client_mac
 * @property string         $method
 * @property string         $url
 * @property \Carbon\Carbon $visited_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string    $source_with_port
 * @property-read string    $destination_with_port
 * @property-read string    $visited_at_formatted
 * @property-read string    $device_type_id
 * @property-read string    $guest_device_type_name
 * @property-read string    $description
 * @property-read string    $platform
 * @property-read string    $browser
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereId($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereSourceIp($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereSourcePort($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereDestinationIp($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereDestinationPort($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereApMac($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereClientMac($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereMethod($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereTimestamp($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|VisitedUrl whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VisitedUrl extends Model implements HasPresenter
{
    use BelongsToTenant;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'source_ip',
        'source_port',
        'destination_ip',
        'destination_port',
        'ap_mac',
        'device_id',
        'client_mac',
        'method',
        'url',
        'host',
        'visited_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['visited_at'];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return VisitedUrlPresenter::class;
    }

    /**
     * Filter query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public static function filter()
    {
        $request = trimmed_request();

        /** @var \Illuminate\Database\Query\Builder $query */
        $query = \Entrust::can('tenants.manage')
            ? static::allTenants()
            : static::query();

        $query->orderBy('visited_urls.id', 'DESC')
            ->select([
                'visited_urls.*',
                'guest_devices.device_type_id',
                'guest_devices.description',
                'guest_devices.platform',
                'guest_devices.browser',
            ])
            ->leftJoin('guest_devices', 'guest_devices.mac', '=', 'visited_urls.client_mac')
            ->when($request->has('date'), function($query) use ($request) {
                return $query->whereBetween('visited_at', date_ranges($request->date));
            })
            ->if($request->has('description'), 'guest_devices.description', 'LIKE', "%{$request->description}%")
            ->if($request->has('platform'), 'guest_devices.platform', 'LIKE', "%{$request->platform}%")
            ->if($request->has('browser'), 'guest_devices.browser', 'LIKE', "%{$request->browser}%")
            ->if($request->has('device_type_id'), 'guest_devices.device_type_id', $request->device_type_id)
            ->if($request->has('ap_mac'), 'ap_mac', 'LIKE', "%{$request->ap_mac}%")
            ->if($request->has('client_mac'), 'client_mac', 'LIKE', "%{$request->client_mac}%")
            ->if($request->has('source_ip'), 'source_ip', 'LIKE', "%{$request->source_ip}%")
            ->if($request->has('destination_ip'), 'destination_ip', 'LIKE', "%{$request->destination_ip}%")
            ->if($request->has('url'), 'url', 'LIKE', "%{$request->url}%");


        return $query;
    }
}
