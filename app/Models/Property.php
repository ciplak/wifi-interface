<?php

namespace App\Models;

use App\Date;
use App\Exceptions\LoginPageNotFoundException;
use App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Property
 *
 * @property       integer                        $id
 * @property       integer                        $parent_id
 * @property       boolean                        $property_type_id
 * @property       string                         $name
 * @property       string                         $description
 * @property       string                         $address
 * @property       string                         $city
 * @property       string                         $state
 * @property       string                         $post_code
 * @property       integer                        $country_code
 * @property       string                         $contact_name
 * @property       string                         $contact_email
 * @property       string                         $contact_phone
 * @property       bool                           $use_custom_text
 * @property       string                         $contents
 * @property       string                         $post_login_url
 * @property       integer                        $sms_provider_id
 * @property       integer                        $login_page_id
 * @property       integer                        $post_login_page_id
 * @property       \Carbon\Carbon                 $created_at
 * @property       \Carbon\Carbon                 $updated_at
 * @property-read  Property                       $parent
 * @property-read  SmsProvider                    $smsProvider
 * @property-read  Property[]                     $serviceProfiles
 * @property-read  Device[]                       $devices
 * @property-read  SplashPage                     $loginPage
 * @property-read  SplashPage                     $postLoginPage
 * @property-read  string                         $post_login_url_value
 * @property-read  string                         $login_page_name
 * @property-read  string                         $post_login_page_name
 * @method         static Builder|Property whereId($value)
 * @method         static Builder|Property whereParentId($value)
 * @method         static Builder|Property wherePropertyTypeId($value)
 * @method         static Builder|Property whereName($value)
 * @method         static Builder|Property whereDescription($value)
 * @method         static Builder|Property whereAddress($value)
 * @method         static Builder|Property whereCity($value)
 * @method         static Builder|Property whereState($value)
 * @method         static Builder|Property whereZipCode($value)
 * @method         static Builder|Property whereCountryCode($value)
 * @method         static Builder|Property whereContactName($value)
 * @method         static Builder|Property whereContactEmail($value)
 * @method         static Builder|Property whereContactPhone($value)
 * @method         static Builder|Property wherePostLoginUrl($value)
 * @method         static Builder|Property whereLoginPageId($value)
 * @method         static Builder|Property whereCreatedAt($value)
 * @method         static Builder|Property whereUpdatedAt($value)
 * @property-read  string                         $country_name
 * @property-read  array                          $sms_provider_list
 * @property       boolean                        $is_default
 * @property       \Carbon\Carbon                 $deleted_at
 * @property-read  mixed                          $encrypted
 * @property-write mixed                          $encrypting
 * @property-write mixed                          $hashing
 * @method         static \Illuminate\Database\Query\Builder|Property tenants()
 * @method         static \Illuminate\Database\Query\Builder|Property venues()
 * @method         static \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model without($relations)
 * @property       integer                        $smart_login
 * @property       integer                        $smart_login_frequency
 * @property       integer                        $smart_login_frequency_type
 * @property       string                         $hashed_id
 * @property       \Illuminate\Support\Collection $ssids
 * @property-read  integer                        $smart_login_frequency_hours
 * @property-read  bool                           $is_smart_login_enabled
 * @method         static \Illuminate\Database\Query\Builder|Property wherePostCode($value)
 * @method         static \Illuminate\Database\Query\Builder|Property whereUseCustomText($value)
 * @method         static \Illuminate\Database\Query\Builder|Property whereSmsProviderId($value)
 * @method         static \Illuminate\Database\Query\Builder|Property whereIsDefault($value)
 * @method         static \Illuminate\Database\Query\Builder|Property whereDeletedAt($value)
 * @method         static \Illuminate\Database\Query\Builder|Property whereSmartLogin($value)
 * @method         static \Illuminate\Database\Query\Builder|Property whereSmartLoginFrequency($value)
 * @method         static \Illuminate\Database\Query\Builder|Property whereSmartLoginFrequencyType($value)
 * @mixin          \Eloquent
 * @property-read  string                         $terms_and_conditions
 * @property-read  string                         $ssid_name_list
 * @method         static \Illuminate\Database\Query\Builder|Property whereContents($value)
 * @method         static \Illuminate\Database\Query\Builder|Property wherePostLoginPageId($value)
 * @method         static \Illuminate\Database\Query\Builder|Property tenantScoped()
 */
class Property extends Model
{
    use SoftDeletes;

    const TYPE_ENTERPRISE = 1;

    const TYPE_TENANT = 2;

    const TYPE_VENUE = 3;

    const TYPE_ZONE = 4;

    const SMART_LOGIN_INHERIT = null;

    const SMART_LOGIN_DISABLED = 0;

    const SMART_LOGIN_ENABLED = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'address',
        'city',
        'state',
        'post_code',
        'country_code',
        'login_page_id',
        'post_login_page_id',
        'sms_provider_id',
        'smart_login',
        'post_login_url',
        'is_default',
        'ssids',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'contents' => 'array',
        'ssids'    => 'collection',
    ];

    /**
     * Get smart login options
     *
     * @return array
     */
    public static function getSmartLoginOptions()
    {
        return [
            static::SMART_LOGIN_DISABLED => trans('messages.disabled'),
            static::SMART_LOGIN_ENABLED  => trans('messages.enabled'),
        ];
    }

    /**
     * Get smart login frequency type options
     *
     * @return array
     */
    public static function getSmartLoginFrequencyTypeOptions()
    {
        return [
            Date::DURATION_TYPE_HOURS => trans('messages.duration.hours'),
            Date::DURATION_TYPE_DAYS  => trans('messages.duration.days'),
        ];
    }

    /**
     * Get parent property
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Get the SMS provider
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function smsProvider()
    {
        return $this->belongsTo(SmsProvider::class);
    }

    /**
     * Get devices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(Device::class, 'property_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function loginPage()
    {
        return $this->hasOne(SplashPage::class, 'id', 'login_page_id')->where('type', SplashPage::TYPE_OFFLINE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function postLoginPage()
    {
        return $this->hasOne(SplashPage::class, 'id', 'post_login_page_id')->where('type', SplashPage::TYPE_ONLINE);
    }

    /**
     * Scope a query to only include tenants
     *
     * @param   Builder $query
     * @@return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTenants(Builder $query)
    {
        return $query->where('property_type_id', static::TYPE_TENANT);
    }

    /**
     * Scope a query to only include tenants
     *
     * @param Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVenues(Builder $query)
    {
        return $query->where('property_type_id', static::TYPE_VENUE);
    }

    /**
     * Scope a query to only include zones
     *
     * @param Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeZones(Builder $query)
    {
        return $query->where('property_type_id', static::TYPE_ZONE);
    }

    /**
     * Get country name
     *
     * @return string
     */
    public function getCountryNameAttribute()
    {
        $countries = $this->getCountries();

        return $countries[$this->country_code] ?? '';
    }

    /**
     * Get contry list
     *
     * @return array
     */
    public static function getCountries()
    {
        return [trans('messages.select_country')] + config('countries.names');
    }

    /**
     * Get sms providers
     *
     * @return string
     */
    public function getSmsProviderListAttribute()
    {
        return $this->smsProviders->pluck('id')->toArray();
    }

    /**
     * Get terms and conditions text for the property
     *
     * @return string
     */
    public function getTermsAndConditionsAttribute()
    {
        return $this->contents['terms_and_conditions'];
    }

    /**
     * Get inherited terms and conditions text for the property
     *
     * @param string|null $locale
     *
     * @return mixed
     */
    public function getTermsAndConditionsText($locale = null)
    {
        if (!$locale) {
            $locale = \App::getLocale();
        }

        if ($this->use_custom_text) {
            return array_get($this->contents, "terms_and_conditions.$locale");
        } else {
            return $this->parent->getTermsAndConditionsText($locale);
        }
    }

    /**
     * Get list of active services profiles of the property
     *
     * @return null|ServiceProfile[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getActiveServiceProfiles()
    {
        $serviceProfiles = $this->serviceProfiles()
            ->where('status', true)
            ->where(
                function($query) {
                    $query->whereNull('expiry_date')
                        ->orWhere('expiry_date', '>', \Carbon\Carbon::today()->toDateString());
                }
            )->get();

        return $serviceProfiles->count() ? $serviceProfiles : $this->parent->getActiveServiceProfiles();
    }

    /**
     * Get service profiles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function serviceProfiles()
    {
        return $this->belongsToMany(ServiceProfile::class, 'property_service_profiles', 'property_id')
            ->withTrashed()
            ->withPivot(['is_default']);
    }

    public function getSelectedDefaultServiceProfileId()
    {
        return $this->serviceProfiles->filter(
            function($item) {
                return $item->pivot->is_default;
            }
        )->pluck('id')->first();
    }

    /**
     * Get default service profile of the property
     *
     * @return ServiceProfile
     */
    public function getDefaultServiceProfile()
    {
        $serviceProfile = $this->serviceProfiles()->where('is_default', 1)->first();

        if (!$serviceProfile) {
            $serviceProfile = $this->parent->getDefaultServiceProfile();
        }

        return $serviceProfile;
    }

    /**
     * Get login page for the property
     *
     * @param string $locale
     * @param bool   $isConnected
     * @param string $ssidName
     *
     * @return SplashPage|null
     * @throws LoginPageNotFoundException
     */
    public function getSplashPage($locale = 'en', $isConnected = false, $ssidName = '')
    {
        // @todo optimize & implement repository
        $splashPage = null;

        if (trim($ssidName) && $this->ssids) {
            $ssidSettings = $this->ssids->where('name', $ssidName)->first();

            $key = $isConnected ? 'post_login_page_id' : 'login_page_id';

            if (isset($ssidSettings[$key])) {
                $splashPage = SplashPage::find($ssidSettings[$key]);
            }
        }

        if (!$splashPage) {
            $splashPage = $isConnected ? $this->getPostLoginPage() : $this->getPreLoginPage();
        }

        if (!$splashPage) {
            throw new LoginPageNotFoundException($this);
        }

        return $splashPage->getTranslation($locale);
    }

    /**
     * Get post login page for the property
     *
     * @return SplashPage|null
     */
    public function getPostLoginPage()
    {
        if ($this->post_login_page_id) {
            return $this->postLoginPage;
        } else {
            if ($this->parent_id && $this->parent->postLoginPage) {
                return $this->parent->postLoginPage;
            }
        }

        return null;
    }

    /**
     * Get pre login page for the property
     *
     * @return SplashPage|null
     */
    public function getPreLoginPage()
    {
        if ($this->login_page_id) {
            return $this->loginPage;
        } else {
            if ($this->parent_id && $this->parent->loginPage) {
                return $this->parent->loginPage;
            }
        }

        return null;
    }

    /**
     * Get if smart login is enabled for the property
     *
     * @return bool
     */
    public function getIsSmartLoginEnabledAttribute()
    {
        return $this->smart_login || ($this->isSmartLoginInherited() && $this->parent->is_smart_login_enabled);
    }

    private function isSmartLoginInherited()
    {
        return is_null($this->smart_login) && $this->parent_id;
    }

    /**
     * Check if the guest can smart login
     *
     * @param Guest|null $guest
     * @param int|null   $tenantId
     *
     * @return bool
     */
    public function canSmartLogin(Guest $guest = null, $tenantId = null)
    {
        if (!$guest) {
            return false;
        }

        return ($guest->tenant_id == $tenantId) && is_null($guest->smart_login_time) || ($guest->smart_login_time > Carbon::now()->subHours($this->smart_login_frequency_hours));
    }

    /**
     * Get post login url for the property
     *
     * @return string
     */
    public function getPostLoginUrlValueAttribute()
    {
        if (trim($this->attributes['post_login_url'])) {
            return trim($this->attributes['post_login_url']);
        } else {
            if ($this->parent_id && trim($this->parent->attributes['post_login_url'])) {
                return trim($this->parent->attributes['post_login_url']);
            }
        }

        return '';
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(
            function($model) {
                $model->update(
                    [
                        'login_page_id'      => null,
                        'post_login_page_id' => null,
                        'sms_provider_id'    => null,
                    ]
                );
            }
        );
    }

    /**
     * Get sms provider of the property
     *
     * @return Models\SmsProvider|null
     */
    public function getSmsProvider()
    {
        if ($this->smsProvider) {
            return $this->smsProvider;
        } else {
            if ($this->parent_id && $this->parent->smsProvider) {
                return $this->parent->smsProvider;
            }
        }

        return null;
    }

    /**
     * Set default service profile for the property
     *
     * @param int $serviceProfileId
     */
    public function setDefaultServiceProvider($serviceProfileId)
    {
        \DB::table('property_service_profiles')
            ->where('property_id', $this->id)
            ->where('is_default', 1)
            ->update(['is_default' => 0]);

        \DB::table('property_service_profiles')
            ->where('property_id', $this->id)
            ->where('service_profile_id', $serviceProfileId)
            ->update(['is_default' => 1]);
    }

    /**
     * Get smart login frequency in hours
     *
     * @return int
     */
    public function getSmartLoginFrequencyHoursAttribute()
    {
        if ($this->isSmartLoginInherited()) {
            return $this->parent->smart_login_frequency_hours;
        }

        $frequency = $this->smart_login_frequency;

        if ($this->smart_login_frequency_type == Date::DURATION_TYPE_DAYS) {
            $frequency *= 24;
        }

        return $frequency;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder,
     */
    public function scopeTenantScoped($query)
    {
        return $query->where('tenant_id', activeTenantId());
    }

    /**
     * Get login page name
     *
     * @return string
     */
    public function getLoginPageNameAttribute()
    {
        return $this->login_page_id ? $this->loginPage->name : '';
    }

    /**
     * Get post login page name
     *
     * @return string
     */
    public function getPostLoginPageNameAttribute()
    {
        return $this->post_login_page_id ? $this->postLoginPage->name : '';
    }

    /**
     * Get list of defined Ssids
     *
     * @return string
     */
    public function getSsidNameListAttribute()
    {
        return $this->ssids ? $this->ssids->pluck('name')->implode('; ') : '';
    }
}
