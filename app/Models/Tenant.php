<?php

namespace App\Models;


use App\Models\Scopes\TenantScope;

/**
 * App\Models\Venue
 *
 * @property       integer                                                          $id
 * @property       integer                                                          $parent_id
 * @property       integer                                                          $property_type_id
 * @property       string                                                           $name
 * @property       string                                                           $description
 * @property       string                                                           $address
 * @property       string                                                           $city
 * @property       string                                                           $state
 * @property       string                                                           $post_code
 * @property       integer                                                          $country_id
 * @property       string                                                           $contact_name
 * @property       string                                                           $contact_email
 * @property       string                                                           $contact_phone
 * @property       boolean                                                          $use_custom_text
 * @property       string                                                           $post_login_url
 * @property       integer                                                          $login_page_id
 * @property       integer                                                          $sms_provider_id
 * @property       boolean                                                          $is_default
 * @property       \Carbon\Carbon                                                   $deleted_at
 * @property       \Carbon\Carbon                                                   $created_at
 * @property       \Carbon\Carbon                                                   $updated_at
 * @property-read  Property                                                    $tenant
 * @property-read  Venue                                                       $defaultVenue
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\Zone[] $zones
 * @property-read  \App\Models\SmsProvider                                     $smsProvider
 * @property-read  \App\Models\ServiceProfile                                  $serviceProfiles
 * @property-read  mixed                                                       $country_name
 * @property-read  mixed                                                       $sms_provider_list
 * @property-read  mixed                                                       $post_login_url_value
 * @property-read  mixed                                                       $encrypted
 * @property-write mixed                                                      $encrypting
 * @property-write mixed                                                      $hashing
 * @method         static \Illuminate\Database\Query\Builder|Property tenants()
 * @method         static \Illuminate\Database\Query\Builder|Property venues()
 * @method         static \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model without($relations)
 * @property       integer                                                          $smart_login
 * @property       integer                                                          $smart_login_frequency
 * @property       integer                                                          $smart_login_frequency_type
 */
class Tenant extends Property
{
    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'address',
        'city',
        'state',
        'post_code',
        'country_code',
        'login_page_id',
        'post_login_page_id',
        'sms_provider_id',
        'post_login_url',
        'default_service_profile',
        'login_page_id',
        'post_login_page_id',
        'smart_login',
        'smart_login_frequency',
        'smart_login_frequency_type',
    ];

    /**
     * Default values for model attributes
     *
     * @var array
     */
    protected $attributes = [
        'use_custom_text'  => 1,
        'property_type_id' => Property::TYPE_TENANT,
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TenantScope);

        static::saving(
            function ($model) {
                /**
            * @var Zone $model 
            */
                if (!$model->login_page_id) {
                    $model->login_page_id = null;
                }

                if (!$model->sms_provider_id) {
                    $model->sms_provider_id = null;
                }

            }
        );
    }

    /**
     * Get default venue
     *
     * @return Venue
     */
    public function defaultVenue()
    {
        return $this->hasOne(Venue::class, 'parent_id')->where('is_default', 1);
    }
}