<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Media
 *
*@property      integer $id
 * @property      integer $tenant_id
 * @property      integer $venue_id
 * @property      integer $model_id
 * @property      string $model_type
 * @property      string $collection_name
 * @property      string $name
 * @property      string $file_name
 * @property      string $disk
 * @property      integer $size
 * @property      string $custom_properties
 * @property      integer $order_no
 * @property      \Carbon\Carbon $created_at
 * @property      \Carbon\Carbon $updated_at
 * @property-read mixed $type_from_extension
 * @property-read mixed $extension
 * @property-read mixed $human_readable_size
 * @method        static \Illuminate\Database\Query\Builder|Media whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereTenantId($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereVenueId($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereModelId($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereModelType($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereCollectionName($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereName($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereFileName($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereDisk($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereSize($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereCustomProperties($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereOrderNo($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereCreatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|Media whereUpdatedAt($value)
 * @mixin         \Eloquent
 */
class Media extends Model
{
    const TYPE_OTHER = 'other';
    const TYPE_IMAGE = 'image';
    const TYPE_PDF = 'pdf';

    protected $guarded = ['id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'custom_properties' => 'array',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(
            function (Media $model) {
                $model->created_by = \Auth::id();
            }
        );
    }

    /**
     * Determine the type of a file from its file extension.
     *
     * @return string
     */
    public function getTypeFromExtensionAttribute()
    {
        $extension = strtolower($this->extension);
        if (in_array($extension, ['png', 'jpg', 'jpeg', 'gif'])) {
            return static::TYPE_IMAGE;
        }
        if ($extension == 'pdf') {
            return static::TYPE_PDF;
        }
        return static::TYPE_OTHER;
    }

    public function getExtensionAttribute()
    {
        return pathinfo($this->file_name, PATHINFO_EXTENSION);
    }

    public function getHumanReadableSizeAttribute()
    {
        return human_filesize($this->size);
    }

    /**
     * Get disk driver name
     *
     * @return mixed
     */
    public function getDiskDriverName()
    {
        return config("filesystems.disks.{$this->disk}.driver");
    }

    /**
     * Determine if the media item has a custom property with the given name.
     *
     * @param string $propertyName
     *
     * @return bool
     */
    public function hasCustomProperty($propertyName)
    {
        return array_key_exists($propertyName, $this->custom_properties);
    }

    /**
     * Get if the value of custom property with the given name.
     *
     * @param string $propertyName
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getCustomProperty($propertyName, $default = null)
    {
        return $this->custom_properties[$propertyName] ?? $default;
    }

    /**
     * Set value for a custom property
     *
     * @param string $name
     * @param mixed  $value
     */
    public function setCustomProperty($name, $value)
    {
        $this->custom_properties = array_merge($this->custom_properties, [$name => $value]);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return asset_url('img/' . $this->file_name);
    }
}
