<?php

namespace App\Models;

use App\Presenters\UserPresenter;
use App\Traits\HashId;
use HipsterJazzbo\Landlord\BelongsToTenant;
use McCool\LaravelAutoPresenter\HasPresenter;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


/**
 * App\Models\User
 *
 * @property       integer                                                     $id
 * @property       integer                                                     $tenant_id
 * @property       integer                                                     $property_id
 * @property       string                                                      $name
 * @property       string                                                      $last_name
 * @property       string                                                      $email
 * @property       string                                                      $avatar
 * @property       int                                                         $status
 * @property       string                                                      $password
 * @property       string                                                      $remember_token
 * @property       string                                                      $api_token
 * @property       \Carbon\Carbon                                              $created_at
 * @property       \Carbon\Carbon                                              $updated_at
 * @property       string                                                      $banned_at
 * @property-read  int                                                         $role_id
 * @property-read  string                                                      $role
 * @property-read  string                                                      $role_display_name
 * @property-read  string                                                      $tenant_name
 * @property-read  string                                                      $status_text
 * @property-read  string                                                      $location_name
 * @property-read  bool                                                        $is_banned
 * @property-read  mixed                                                       $encrypted
 * @property-read  \App\Models\Property                                        $property
 * @property-read  \App\Models\Tenant                                          $tenant
 * @property-write mixed                                                       $encrypting
 * @property-write mixed                                                       $hashing
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @method         static \Illuminate\Database\Query\Builder|User whereId($value)
 * @method         static \Illuminate\Database\Query\Builder|User whereName($value)
 * @method         static \Illuminate\Database\Query\Builder|User whereLastName($value)
 * @method         static \Illuminate\Database\Query\Builder|User whereEmail($value)
 * @method         static \Illuminate\Database\Query\Builder|User wherePassword($value)
 * @method         static \Illuminate\Database\Query\Builder|User whereRememberToken($value)
 * @method         static \Illuminate\Database\Query\Builder|User whereCreatedAt($value)
 * @method         static \Illuminate\Database\Query\Builder|User whereUpdatedAt($value)
 * @method         static \Illuminate\Database\Query\Builder|User without($relations)
 * @method         static \Illuminate\Database\Query\Builder|User wherePropertyId($value)
 * @method         static \Illuminate\Database\Query\Builder|User whereTenantId($value)
 * @mixin          \Eloquent
 */
class User extends Authenticatable implements HasPresenter
{
    use Notifiable, HashId, BelongsToTenant, SoftDeletes, EntrustUserTrait {
        //EntrustUserTrait::can insteadof Authorizable;
        SoftDeletes::restore insteadof EntrustUserTrait;
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    const STATUS_PASSIVE = 0;

    const STATUS_ACTIVE = 1;

    const STATUS_BANNED = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'property_id',
        'tenant_id',
        'locale',
        'avatar',
        'status',
        'banned_at',
    ];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'locale' => 'en',
        'status' => self::STATUS_ACTIVE,
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * These are the attributes to hash before saving.
     *
     * @var array
     */
    protected $hashable = ['password'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(
            function(User $model) {
                $model->api_token = bin2hex(openssl_random_pseudo_bytes(16));
            }
        );
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return UserPresenter::class;
    }

    /**
     * Get the property the user belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Venue::class);
    }

    /**
     * Get the tenant the user belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    /**
     * Check if the user can manage the given venue
     *
     * @param int|Venue $venue
     *
     * @return bool
     */
    public function canManageVenue($venue)
    {
        $venueId = $venue;

        if ($venue instanceof Venue) {
            $venueId = $venue->id;
        }

        if (\Entrust::hasRole('venue') && $this->property_id != $venueId) {
            abort(404);
        }

        return true;
    }

    /**
     * Set impersonating user id
     *
     * @param int $id
     */
    public function setImpersonating($id)
    {
        session(['impersonate' => $id]);
    }

    /**
     * Stop impersonating a user
     */
    public function stopImpersonating()
    {
        session()->forget('impersonate');
    }

    /**
     * Check if the current user is impersonating
     *
     * @return bool
     */
    public function isImpersonating()
    {
        return session()->has('impersonate');
    }

    /**
     * Return if the user is banned
     *
     * @return bool
     */
    public function getIsBannedAttribute()
    {
        return $this->status == self::STATUS_BANNED;
    }
}
