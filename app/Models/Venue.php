<?php

namespace App\Models;


use App\Models\Scopes\VenueScope;
use App\Traits\HashId;

/**
 * App\Models\Venue
 *
 * @property       integer                                                          $id
 * @property       integer                                                          $parent_id
 * @property       integer                                                          $property_type_id
 * @property       string                                                           $name
 * @property       string                                                           $description
 * @property       string                                                           $address
 * @property       string                                                           $city
 * @property       string                                                           $state
 * @property       string                                                           $post_code
 * @property       integer                                                          $country_id
 * @property       string                                                           $contact_name
 * @property       string                                                           $contact_email
 * @property       string                                                           $contact_phone
 * @property       boolean                                                          $use_custom_text
 * @property       string                                                           $contents
 * @property       string                                                           $post_login_url
 * @property       integer                                                          $login_page_id
 * @property       integer                                                          $sms_provider_id
 * @property       boolean                                                          $is_default
 * @property       \Carbon\Carbon                                                   $deleted_at
 * @property       \Carbon\Carbon                                                   $created_at
 * @property       \Carbon\Carbon                                                   $updated_at
 * @property-read  Property                                                    $tenant
 * @property-read  Zone                                                        $defaultZone
 * @property-read  Zone[]                                                      $zone
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\Zone[] $zones
 * @property-read  \App\Models\SmsProvider                                     $smsProvider
 * @property-read  \App\Models\ServiceProfile                                  $serviceProfiles
 * @property-read  mixed                                                       $country_name
 * @property-read  mixed                                                       $sms_provider_list
 * @property-read  mixed                                                       $post_login_url_value
 * @property-read  mixed                     $encrypted
 * @property-write mixed                    $encrypting
 * @property-write mixed                    $hashing
 * @method         static \Illuminate\Database\Query\Builder|Property tenants()
 * @method         static \Illuminate\Database\Query\Builder|Property venues()
 * @method         static \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model without($relations)
 * @property       integer                        $smart_login
 * @property       integer                        $smart_login_frequency
 * @property       integer                        $smart_login_frequency_type
 * @property       string                         $hashed_id
 * @property       \Illuminate\Support\Collection $ssids
 */
class Venue extends Property
{
    use HashId;

    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'description',
        'address',
        'city',
        'state',
        'post_code',
        'country_code',
        'login_page_id',
        'post_login_page_id',
        'sms_provider_id',
        'post_login_url',
        'default_service_profile',
        'is_default',
        'smart_login',
        'smart_login_frequency',
        'smart_login_frequency_type',
        'ssids',
    ];

    /**
     * Default values for model attributes
     *
     * @var array
     */
    protected $attributes = [
        'use_custom_text'  => 1,
        'property_type_id' => Property::TYPE_VENUE,
    ];

    /**
     * Get list of venues
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getAll()
    {
        /**
         * @var Venue[] $venues
         */
        $query = Venue::with('zones')->where('parent_id', activeTenantId())->orderBy('name');

        if (\Entrust::hasRole('venue')) {
            $query->where('id', \Auth::user()->property_id);
        }

        return $query->get();
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new VenueScope);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'parent_id');
    }

    /**
     * Get default zone
     *
     * @return Zone
     */
    public function defaultZone()
    {
        return $this->hasOne(Zone::class, 'parent_id')->where('is_default', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function zones()
    {
        return $this->hasMany(Zone::class, 'parent_id');
    }

    /**
     * Get venue name with tenant name prefixed
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->tenant->name . ' - ' . $this->name;
    }
}