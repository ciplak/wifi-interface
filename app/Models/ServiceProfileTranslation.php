<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ServiceProfileTranslation
 *
 * @property       integer     $id
 * @property       integer     $service_profile_id
 * @property       string      $locale
 * @property       string      $name
 * @property       string      $description
 * @property-read  mixed  $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method         static \Illuminate\Database\Query\Builder|Model without($relations)
 */
class ServiceProfileTranslation extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * Validation rules
     *
     * @var array
     */
    protected $rules = [
        'name' => 'required',
    ];
}
