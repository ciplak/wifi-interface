<?php

namespace App\Models;

use App\Date;
use App\Models;
use App\Presenters\ServiceProfilePresenter;
use App\Traits\HashId;
use Carbon\Carbon;
use HipsterJazzbo\Landlord\BelongsToTenant;
use McCool\LaravelAutoPresenter\HasPresenter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\ServiceProfile
 *
 * @property       integer                                                         $id
 * @property       integer                                                         $tenant_id
 * @property       string                                                          $name
 * @property       string                                                          $description
 * @property       boolean                                                         $status
 * @property       Carbon                                                          $expiry_date
 * @property       boolean                                                         $type
 * @property       boolean                                                         $renewal_frequency
 * @property       boolean                                                         $renewal_frequency_type
 * @property       boolean                                                         $model
 * @property       float                                                           $price
 * @property       boolean                                                         $service_pin
 * @property       boolean                                                         $access_code_display_page
 * @property       int                                                             $duration
 * @property       int                                                             $duration_total_minutes
 * @property       int                                                             $duration_type
 * @property       int                                                             $max_download_bandwidth
 * @property       int                                                             $max_upload_bandwidth
 * @property       int                                                             $qos_level
 * @property       integer                                                         $created_by
 * @property       integer                                                         $updated_by
 * @property       Carbon                                                          $created_at
 * @property       Carbon                                                          $updated_at
 * @property       Carbon                                                          $deleted_at
 * @property-read  \Illuminate\Database\Eloquent\Collection|AccessCode[]           $accessCodes
 * @property-read  string                                                          $name_with_duration
 * @property-read  string                                                          $duration_value
 * @property-read  int                                                             $duration_total_seconds
 * @property-read  string                                                          $type_value
 * @property-read  string                                                          $expiry_date_value
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\Property[] $venues
 * @property-read  mixed                                                           $service_pin_value
 * @property-read  bool                                                            $is_expired
 * @property-read  string                                                          $status_icon
 * @property-read  string                                                          $price_html
 * @property-read  string                                                          $model_name
 * @property-read  mixed                                                           $encrypted
 * @property-write mixed                                                           $encrypting
 * @property-write mixed                                                           $hashing
 * @method         static Builder|ServiceProfile whereId($value)
 * @method         static Builder|ServiceProfile whereName($value)
 * @method         static Builder|ServiceProfile whereDescription($value)
 * @method         static Builder|ServiceProfile whereStatus($value)
 * @method         static Builder|ServiceProfile whereExpiryDate($value)
 * @method         static Builder|ServiceProfile whereType($value)
 * @method         static Builder|ServiceProfile whereRenewalFrequency($value)
 * @method         static Builder|ServiceProfile whereRenewalFrequencyTypeId($value)
 * @method         static Builder|ServiceProfile whereModel($value)
 * @method         static Builder|ServiceProfile wherePrice($value)
 * @method         static Builder|ServiceProfile whereServicePin($value)
 * @method         static Builder|ServiceProfile whereAccessCodeDisplayPage($value)
 * @method         static Builder|ServiceProfile whereDuration($value)
 * @method         static Builder|ServiceProfile whereDurationType($value)
 * @method         static Builder|ServiceProfile whereCreatedBy($value)
 * @method         static Builder|ServiceProfile whereEditedBy($value)
 * @method         static Builder|ServiceProfile whereCreatedAt($value)
 * @method         static Builder|ServiceProfile whereUpdatedAt($value)
 * @method         static \Illuminate\Database\Query\Builder|ServiceProfile active()
 * @method         static \Illuminate\Database\Query\Builder|ServiceProfile notExpired()
 * @method         static \Illuminate\Database\Query\Builder|Model without($relations)
 */
class ServiceProfile extends Model implements HasPresenter
{
    use SoftDeletes, BelongsToTenant, HashId;

    const TYPE_ONE_TIME = 1;
    const TYPE_RECURRING = 2;
    const TYPE_UNRESTRICTED = 3;

    const MODEL_FREE = 1;
    const MODEL_PAID = 2;

    const STATUS_PASSIVE = 0;
    const STATUS_ACTIVE = 1;

    const SERVICE_PIN_DISABLED = 0;
    const SERVICE_PIN_ENABLED = 1;

    const OPERATOR_EQUALS = 0;
    const OPERATOR_LESS_THAN = 1;
    const OPERATOR_GREATER_THAN = 2;
    const OPERATOR_LESS_THAN_OR_EQUAL = 3;
    const OPERATOR_GREATER_THAN_OR_EQUAL = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'name',
        'description',
        'duration',
        'duration_type',
        'type',
        'renewal_frequency',
        'renewal_frequency_type',
        'model',
        'price',
        'max_download_bandwidth',
        'max_upload_bandwidth',
        'qos_level',
        'expiry_date',
        'service_pin',
        'status',
    ];

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public static function filter()
    {
        $request = request();
        $operators = self::getOperators();

        /**
         * @var \Illuminate\Database\Query\Builder $query
         */
        $query = \App\Models\ServiceProfile::orderBy('service_profiles.id', 'DESC')
            ->when($request->has('expiry_date'), function ($query) use ($request) {
                return $query->whereBetween('expiry_date', date_ranges($request->input('expiry_date')));
            })
            ->when($request->name, function ($query) use ($request) {
                return $query->where(function ($query) use ($request) {
                    $query->where('name', 'LIKE', "%{$request->name}%")
                        ->orWhere('description', 'LIKE', "%{$request->name}%");
                });
            })
            ->when($request->has('duration'), function ($query) use ($request, $operators) {
                $operator = $operators[$request->duration_operator] ?? '=';

                return $query->where('duration_total_minutes', $operator, $request->duration);     // duration in minutes
            })
            ->if($request->has('type'), 'type', $request->type)
            ->if($request->has('service_pin'), 'service_pin', $request->service_pin)
            ->if($request->has('model'), 'model', $request->model)
            ->if($request->has('status'), 'status', $request->status);

        return $query;
    }

    /**
     * Default attribute values
     *
     * @var array
     */
    protected $attributes = [
        'type'                   => ServiceProfile::TYPE_UNRESTRICTED,
        'status'                 => ServiceProfile::STATUS_ACTIVE,
        'service_pin'            => ServiceProfile::SERVICE_PIN_ENABLED,
        'renewal_frequency'      => 1,
        'renewal_frequency_type' => Date::DURATION_TYPE_HOURS,
        'model'                  => ServiceProfile::MODEL_FREE,
        'price'                  => 0,
        'max_download_bandwidth' => 0,
        'max_upload_bandwidth'   => 0,
    ];

    /**
     * The attributes that should be casted to native types
     *
     * @var array
     */
    protected $casts = [
        'duration'               => 'integer',
        'duration_type'          => 'integer',
        'type'                   => 'integer',
        'status'                 => 'integer',
        'service_pin'            => 'integer',
        'renewal_frequency'      => 'integer',
        'renewal_frequency_type' => 'integer',
        'model'                  => 'integer',
        'price'                  => 'float',
        'max_download_bandwidth' => 'integer',
        'max_upload_bandwidth'   => 'integer',
        'qos_level'              => 'integer',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        /**
         * Set created by on create
         */
        static::creating(
            function (ServiceProfile $model) {
                $model->created_by = \Auth::id();
            }
        );

        /**
         * Set attributes on save
         */
        static::saving(
            function (ServiceProfile $model) {
                $model->updated_by = \Auth::id();
                $model->duration_total_minutes = $model->calculateDurationMinutes();
                $model->expiry_date = $model->expiry_date ?: null;
            }
        );
    }

    /**
     * Return service profile duration in minutes
     *
     * @return int
     */
    public function calculateDurationMinutes()
    {
        $duration = $this->duration;

        switch ($this->duration_type) {
            case Date::DURATION_TYPE_HOURS:
                $duration *= 60;
                break;

            case Date::DURATION_TYPE_DAYS:
                $duration *= 24 * 60;
        }

        return $duration;
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return ServiceProfilePresenter::class;
    }

    /**
     * Get access codes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accessCodes()
    {
        return $this->hasMany(Models\AccessCode::class);
    }

    /**
     * Get venues
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function venues()
    {
        return $this->belongsToMany(Models\Property::class)->withPivot(['is_default']);
    }

    /**
     * Scope a query to only include active service profiles
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('status', static::STATUS_ACTIVE);
    }

    /**
     * Scope a query to only include not expired service profiles
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNotExpired(Builder $query)
    {
        return $query->where(
            function ($query) {
                $query->whereNull('expiry_date')
                    ->orWhere('expiry_date', '>', Carbon::today()->toDateString());
            }
        );
    }

    /**
     * Return service profile renewal frequency in hours
     *
     * @return int
     */
    public function calculateRenewalFrequencyHours()
    {
        $frequency = $this->renewal_frequency;

        switch ($this->renewal_frequency_type) {

            case Date::DURATION_TYPE_DAYS:
                $frequency *= 24;
                break;

            case Date::DURATION_TYPE_WEEKS:
                $frequency *= 7 * 24;
                break;
        }

        return $frequency;
    }

    /**
     * @return int
     */
    public function getDurationTotalSecondsAttribute()
    {
        return $this->duration_total_minutes * 60;
    }

    /**
     * @todo create enumeration class
     *
     * @param bool $includeEquals
     *
     * @return array
     */
    public static function getOperators($includeEquals = false)
    {
        $operators = [];

        if ($includeEquals) {
            $operators[self::OPERATOR_EQUALS] = '=';
        }

        $operators[self::OPERATOR_LESS_THAN_OR_EQUAL] = '<=';
        $operators[self::OPERATOR_GREATER_THAN_OR_EQUAL] = '>=';

        return $operators;
    }
}