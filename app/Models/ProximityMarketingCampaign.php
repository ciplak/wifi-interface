<?php

namespace App\Models;

use App\Presenters\ProximityMarketingCampaignPresenter;
use App\Traits\HashId;
use Carbon\Carbon;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * App\Models\ProximityMarketingCampaign
 *
 * @property integer        $id
 * @property string         $title
 * @property boolean        $location_id
 * @property string         $start_date
 * @property string         $end_date
 * @property boolean        $status
 * @property boolean        $event_type
 * @property integer        $event_delay
 * @property boolean        $parameter_type
 * @property integer        $parameter_visit_based
 * @property string         $parameter_date_time_based
 * @property string         $frequency
 * @property boolean        $gender
 * @property boolean        $age
 * @property string         $communication_channel
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereId($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereTitle($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereLocationId($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereStartDate($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereEndDate($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereStatus($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereEventType($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereEventDelay($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereParameterType($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereParameterVisitBased($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereParameterDateTimeBased($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereFrequency($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereGender($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereAge($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereCommunicationChannel($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereCreatedAt($value)
 * @method   static \Illuminate\Database\Query\Builder|ProximityMarketingCampaign whereUpdatedAt($value)
 * @mixin    \Eloquent
 */
class ProximityMarketingCampaign extends Model implements HasPresenter
{
    use HashId, SoftDeletes, BelongsToTenant;

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    const FREQUENCY_TYPE_NONE = 0;
    const FREQUENCY_TYPE_DAY = 1;
    const FREQUENCY_TYPE_WEEK = 2;
    const FREQUENCY_TYPE_MONTH = 3;
    const FREQUENCY_TYPE_YEAR = 4;

    const EVENT_TYPE_DETECTED = 1;
    const EVENT_TYPE_CONNECTED = 2;
    const EVENT_TYPE_EXIT = 3;

    const PARAMETER_TYPE_VISIT = 1;
    const PARAMETER_TYPE_DATE_TIME = 2;

    const GENDER_TYPE_UNKNOWN = 0;
    const GENDER_TYPE_MALE = 1;
    const GENDER_TYPE_FEMALE = 2;

    const AGE_TYPE_UNKNOWN_AGE = 0;
    const AGE_TYPE_LESS_THAN = 1;
    const AGE_TYPE_MORE_THAN = 2;
    const AGE_TYPE_RANGE = 3;

    const PARAMETER_TYPE_VISIT_INCLUDE_VISITORS = 0;
    const PARAMETER_TYPE_VISIT_NUMBER_OF_VISITS = 1;

    const PARAMETER_TYPE_NUMBER_OF_VISITS_IS = 0;
    const PARAMETER_TYPE_NUMBER_OF_VISITS_MORE_THAN = 1;
    const PARAMETER_TYPE_NUMBER_OF_VISITS_EVERY_X_VISIT = 2;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['start_date', 'end_date'];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return ProximityMarketingCampaignPresenter::class;
    }


    /**
     * @param bool $gender
     * @param bool $ageGroup
     * @param int $ageStart
     * @param int $ageEnd
     */
    public function calculateAudience($gender = false, $ageGroup = false, $ageStart = 0, $ageEnd = 0)
    {
        $query = DB::table('guests')->select('birthday', 'gender');

        if ($gender)
            $query->where('gender', $gender);

        if ($ageGroup == ProximityMarketingCampaign::AGE_TYPE_RANGE)
            $query->whereRaw( 'timestampdiff(year, birthday, curdate()) between ? and ?', [$ageStart, $ageEnd] );

        if ($ageGroup == ProximityMarketingCampaign::AGE_TYPE_LESS_THAN)
            $query->whereRaw( 'timestampdiff(year, birthday, curdate()) < ?', [$ageStart] );

        if ($ageGroup == ProximityMarketingCampaign::AGE_TYPE_MORE_THAN)
            $query->whereRaw( 'timestampdiff(year, birthday, curdate()) > ?', [$ageStart] );

        return $query->count();
    }
}
