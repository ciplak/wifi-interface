<?php

namespace App\Models;

use Carbon\Carbon;
use HipsterJazzbo\Landlord\BelongsToTenant;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ActivityLog
 *
 * @property      integer            $id
 * @property      integer            $tenant_id
 * @property      integer            $user_id
 * @property      string             $description
 * @property      string             $ip_address
 * @property      string             $user_agent
 * @property      string             $created_at
 * @property-read \App\Models\User   $user
 * @method        static \Illuminate\Database\Query\Builder|ActivityLog whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|ActivityLog whereUserId($value)
 * @method        static \Illuminate\Database\Query\Builder|ActivityLog whereDescription($value)
 * @method        static \Illuminate\Database\Query\Builder|ActivityLog whereIpAddress($value)
 * @method        static \Illuminate\Database\Query\Builder|ActivityLog whereUserAgent($value)
 * @method        static \Illuminate\Database\Query\Builder|ActivityLog whereCreatedAt($value)
 * @mixin         \Eloquent
 * @property-read \App\Models\Tenant $tenant
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\ActivityLog whereTenantId($value)
 */
class ActivityLog extends Model
{
    use BelongsToTenant;

    public $fillable = [
        'tenant_id',
        'user_id',
        'description',
        'ip_address',
        'user_agent',
        'created_at'
    ];

    public $timestamps = false;

    private $userId = null;

    /**
     * Set id of the user
     *
     * @param int $id
     */
    public function setUserId($id)
    {
        $this->userId = $id;
    }

    /**
     * Get activity log tenant
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id')->select('id', 'name');
    }

    /**
     * Get activity log user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->withoutGlobalScope(\HipsterJazzbo\Landlord\Landlord::class)
            ->withTrashed()
            ->select('id', 'name');
    }

    /**
     * Create an activity log record
     *
     * @param $description
     */
    public function log($description)
    {
        $this->create([
            'tenant_id'   => activeTenantId(),
            'user_id'     => $this->getUserId(),
            'description' => $description,
            'ip_address'  => request()->ip(),
            'user_agent'  => $this->getUserAgent(),
            'created_at'  => Carbon::now(),
        ]);
    }

    /**
     * Get id if the user for who we want to log this action.
     *
     * @return int|mixed|null
     */
    private function getUserId()
    {
        return $this->userId;
    }

    /**
     * Get user agent from request headers.
     *
     * @return string
     */
    private function getUserAgent()
    {
        return substr((string)\Request::header('User-Agent'), 0, 500);
    }
}
