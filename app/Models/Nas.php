<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Nas
 *
 * @property       integer $id
 * @property       string $nasname
 * @property       string $shortname
 * @property       string $type
 * @property       integer $ports
 * @property       string $secret
 * @property       string $server
 * @property       string $community
 * @property       string $description
 * @property-read  mixed $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method         static \Illuminate\Database\Query\Builder|Nas whereId($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas whereNasname($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas whereShortname($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas whereType($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas wherePorts($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas whereSecret($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas whereServer($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas whereCommunity($value)
 * @method         static \Illuminate\Database\Query\Builder|Nas whereDescription($value)
 * @method         static \Illuminate\Database\Query\Builder|Model without($relations)
 */
class Nas extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    public $connection = 'radius';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nasname', 'shortname', 'type', 'ports', 'secret', 'server', 'community', 'description'];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'type'        => 'other',
        'ports'       => null,
        'secret'      => '1P3R4#wifi',
        'server'      => null,
        'community'   => null,
        'description' => 'RADIUS Client',
    ];

}
