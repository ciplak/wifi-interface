<?php

namespace App\Models;


use App\Models\Scopes\ZoneScope;
use App\Traits\HashId;

/**
 * App\Models\Zone
 *
 * @property       integer                         $id
 * @property       integer                         $parent_id
 * @property       integer                         $property_type_id
 * @property       string                          $name
 * @property       string                          $description
 * @property       string                          $address
 * @property       string                          $city
 * @property       string                          $state
 * @property       string                          $post_code
 * @property       integer                         $country_id
 * @property       string                          $contact_name
 * @property       string                          $contact_email
 * @property       string                          $contact_phone
 * @property       boolean                         $use_custom_text
 * @property       string                          $contents
 * @property       string                          $post_login_url
 * @property       integer                         $login_page_id
 * @property       integer                         $sms_provider_id
 * @property       boolean                         $is_default
 * @property       \Carbon\Carbon                  $deleted_at
 * @property       \Carbon\Carbon                  $created_at
 * @property       \Carbon\Carbon                  $updated_at
 * @property-read  Property                   $venue
 * @property-read  \App\Models\SmsProvider    $smsProvider
 * @property-read  \App\Models\ServiceProfile $serviceProfiles
 * @property-read  string                     $parent_id_hashed
 * @property-read  mixed                      $country_name
 * @property-read  mixed                      $sms_provider_list
 * @property-read  mixed                     $post_login_url_value
 * @property-read  mixed                     $encrypted
 * @property-write mixed                    $encrypting
 * @property-write mixed                    $hashing
 * @method         static \Illuminate\Database\Query\Builder|Property tenants()
 * @method         static \Illuminate\Database\Query\Builder|Venue venues()
 * @method         static \Illuminate\Database\Query\Builder|Zone zones()
 * @method         static \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model without($relations)
 * @property       integer                        $smart_login
 * @property       integer                        $smart_login_frequency
 * @property       integer                        $smart_login_frequency_type
 * @property       string                         $hashed_id
 * @property       \Illuminate\Support\Collection $ssids
 */
class Zone extends Property
{
    use HashId;

    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'description',
        'login_page_id',
        'post_login_page_id',
        'sms_provider_id',
        'post_login_url',
        'use_custom_text',
        'is_default',
        'smart_login',
        'smart_login_frequency',
        'smart_login_frequency_type',
        'ssids',
    ];

    /**
     * Default values for model attributes
     *
     * @var array
     */
    protected $attributes = [
        'property_type_id' => Property::TYPE_ZONE,
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ZoneScope);

        static::saving(
            function ($model) {
                /**
            * @var Zone $model 
            */
                if (!$model->login_page_id) {
                    $model->login_page_id = null;
                }

                if (!$model->post_login_page_id) {
                    $model->post_login_page_id = null;
                }

                if (!$model->smart_login) {
                    $model->smart_login = null;
                }

                if (!$model->sms_provider_id) {
                    $model->sms_provider_id = null;
                }
            }
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function venue()
    {
        return $this->belongsTo(Venue::class, 'parent_id');
    }

    /**
     * Return hash id for the parent model
     *
     * @return string
     */
    public function getParentIdHashedAttribute()
    {
        return $this->exists ? hashids_encode($this->parent_id) : request('venue');
    }

}