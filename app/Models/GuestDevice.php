<?php

namespace App\Models;

use App\Presenters\GuestDevicePresenter;
use Carbon\Carbon;
use McCool\LaravelAutoPresenter\HasPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\GuestDevice
 *
 * @property       integer                                                            $id
 * @property       integer                                                            $guest_id
 * @property       integer                                                            $device_type_id
 * @property       integer                                                            $service_profile_id
 * @property       string                                                             $description
 * @property       string                                                             $platform
 * @property       string                                                             $platform_version
 * @property       string                                                             $browser
 * @property       string                                                             $browser_version
 * @property       string                                                             $mac
 * @property       string                                                             $ip
 * @property       integer                                                            $connection_status
 * @property       integer                                                            $status
 * @property       string                                                             $status_info
 * @property       int                                                                $login_type
 * @property       \Carbon\Carbon                                                     $login_time
 * @property       \Carbon\Carbon                                                     $logout_time
 * @property       int                                                                $duration
 * @property       int                                                                $total_duration
 * @property       int                                                                $downloaded_data
 * @property       int                                                                $uploaded_data
 * @property       int                                                                $total_downloaded_data
 * @property       int                                                                $total_uploaded_data
 * @property       \Carbon\Carbon                                                     $created_at
 * @property       \Carbon\Carbon                                                     $updated_at
 * @property-read  ServiceProfile                                                     $serviceProfile
 * @property-read  int                                                                remaining_time
 * @property-read  Guest                                                              $owner
 * @property-read  bool                                                               $is_connected
 * @property-read  string                                                             $connection_status_icon
 * @property-read  mixed                                                              $encrypted
 * @property-write mixed                                                              $encrypting
 * @property-write mixed                                                              $hashing
 * @method         static Builder|GuestDevice whereId($value)
 * @method         static Builder|GuestDevice whereGuestId($value)
 * @method         static Builder|GuestDevice whereDeviceTypeId($value)
 * @method         static Builder|GuestDevice whereServiceProfileId($value)
 * @method         static Builder|GuestDevice whereDescription($value)
 * @method         static Builder|GuestDevice whereMac($value)
 * @method         static Builder|GuestDevice whereIp($value)
 * @method         static Builder|GuestDevice whereConnectionStatus($value)
 * @method         static Builder|GuestDevice whereStatus($value)
 * @method         static Builder|GuestDevice whereStatusInfo($value)
 * @method         static Builder|GuestDevice whereLoginType($value)
 * @method         static Builder|GuestDevice whereLoginTime($value)
 * @method         static Builder|GuestDevice whereLogoutTime($value)
 * @method         static Builder|GuestDevice whereCreatedAt($value)
 * @method         static Builder|GuestDevice whereUpdatedAt($value)
 * @method         static Builder|Model without($relations)
 * @property       string                                                             $deleted_at
 * @property-read  \Illuminate\Database\Eloquent\Collection|\App\Models\WifiSession[] $sessions
 * @property-read  mixed                                                              $type_name
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice connected()
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice wherePlatform($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice wherePlatformVersion($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereBrowser($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereBrowserVersion($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereDuration($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereTotalDuration($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereDownloadedData($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereUploadedData($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereTotalDownloadedData($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereTotalUploadedData($value)
 * @method         static \Illuminate\Database\Query\Builder|GuestDevice whereDeletedAt($value)
 * @mixin          \Eloquent
 */
class GuestDevice extends Model implements HasPresenter
{
    const DEVICE_TYPE_UNDEFINED = 0;

    const DEVICE_TYPE_MOBILE = 1;

    const DEVICE_TYPE_TABLET = 2;

    const DEVICE_TYPE_DESKTOP = 3;

    const CONNECTION_STATUS_DISCONNECTED = 0;

    const CONNECTION_STATUS_CONNECTED = 1;

    /**
     * Default values for model attributes
     *
     * @var array
     */
    public $attributes = [
        'status'            => 1,
        'connection_status' => false,
    ];

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'guest_id',
        'device_type_id',
        'service_profile_id',
        'description',
        'platform',
        'platform_version',
        'browser',
        'browser_version',
        'mac',
        'ip',
        'connection_status',
        'status',
        'status_info',
        'login_type',
        'login_time',
        'logout_time',
    ];

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['login_time', 'logout_time'];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return GuestDevicePresenter::class;
    }

    /**
     * Get connection status of the guest device with given mac address
     *
     * @param string $mac
     *
     * @return bool
     */
    public static function checkIfConnected($mac)
    {
        return \App\ConnectionStatus::CONNECTED == (int)GuestDevice::whereMac($mac)->value('connection_status');
    }

    /**
     * Get service profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function serviceProfile()
    {
        return $this->belongsTo(ServiceProfile::class);
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(WifiSession::class)
            ->orderBy('start_time', 'DESC')
            ->orderBy('connection_status', 'DESC');
    }

    /**
     * Get the guest that owns the guest device
     *
     * @return Guest|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(Guest::class, 'guest_id');
    }

    /**
     * Get remaining connection duration in seconds
     *
     * @return int
     */
    public function getRemainingTimeAttribute()
    {
        $remainingTime = Carbon::now()->diffInSeconds($this->logout_time, false);

        //\Log::info('Guest Device Remaining time', ['remaining time' => $remainingTime, 'logout_time' => $this->logout_time, 'device' => $this->toArray()]);
        return $remainingTime > 0 ? $remainingTime : 0;
    }

    /**
     * Scope a query to only include guest devices that have an active connection
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeConnected($query)
    {
        return $query->where('connection_status', \App\ConnectionStatus::CONNECTED);
    }

    /**
     * Update guest device connection info
     *
     * @param Guest               $guest
     * @param ServiceProfile|null $serviceProfile
     * @param int|null            $loginType
     * @param null                $logoutTime
     *
     * @return bool|int
     */
    public function updateConnectionInfo(Guest $guest, ServiceProfile $serviceProfile = null, $loginType = null, $logoutTime = null)
    {
        $data = [
            'ip'       => session('client_ip') ?: request()->ip(),
            'guest_id' => $guest->id,
        ];

        if ($serviceProfile) {
            $data['service_profile_id'] = $serviceProfile->id;

            $data['login_time'] = Carbon::now();
            $data['logout_time'] =
                $logoutTime ?: (
                $serviceProfile
                    ? Carbon::now()->addMinutes($serviceProfile->duration_total_minutes)
                    : null
                );
        }

        if ($loginType) {
            $data['login_type'] = $loginType;
        }

        log_info('Guest Device connection info updated', [
            'data'            => $data,
            'guest device id' => $this->id,
        ]);

        return $this->update($data);
    }

    /**
     * Set connection status of the guest device
     *
     * @param int  $connectionStatus
     * @param bool $resetRemainingTime
     *
     * @return bool|int
     */
    public function setConnectionStatus(int $connectionStatus, $resetRemainingTime = false)
    {
        log_info('Guest device connection status updated', [
            'connection status' => $connectionStatus,
            'guest device id'   => $this->id,
        ]);

        \Log::debug('Guest device connection status updated', ['guest device' => $this]);

        if ($connectionStatus) {
            event(new \App\Events\Guest\Connected($this->owner, $this));
        }

        $attributes = ['connection_status' => $connectionStatus];

        if ($resetRemainingTime) {
            $attributes['logout_time'] = null;
        }

        return $this->update($attributes);
    }

    /**
     * Get device type name
     *
     * @return string
     */
    public function getTypeNameAttribute()
    {
        $types = static::getDeviceTypes();

        return $types[$this->device_type_id] ?? '';
    }

    /**
     * Get list of guest device types
     *
     * @return array
     */
    public static function getDeviceTypes()
    {
        return [
            null                          => trans('guest_device.type.all'),
            static::DEVICE_TYPE_UNDEFINED => trans('guest_device.type.undefined'),
            static::DEVICE_TYPE_MOBILE    => trans('guest_device.type.mobile'),
            static::DEVICE_TYPE_TABLET    => trans('guest_device.type.tablet'),
            static::DEVICE_TYPE_DESKTOP   => trans('guest_device.type.desktop'),
        ];
    }

    /**
     * Check if the guest device is connected
     *
     * @return bool
     */
    public function getIsConnectedAttribute()
    {
        return \App\ConnectionStatus::CONNECTED == $this->attributes['connection_status'];
    }

    /**
     * Checks if a device has reconnecting and has remaining connection time
     *
     * @return bool
     */
    public function isReturningWithRemainingTime()
    {
        return $this->logout_time >= Carbon::now()->addMinute(1);
    }

    /**
     * Check if direct login is enabled for the owner of the guest device
     *
     * @return bool
     */
    public function canDirectLogin()
    {
        return $this->owner && $this->owner->direct_login_enabled;
    }
}
