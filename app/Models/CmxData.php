<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmxData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cmx_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'device_id', 'mac', 'type', 'data'];
}
