<?php

namespace App\Models;

use App\Models\Scopes\OnlyParentSessionsScope;
use App\Presenters\WifiSessionPresenter;
use HipsterJazzbo\Landlord\BelongsToTenant;
use McCool\LaravelAutoPresenter\HasPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\WifiSession
 *
 * @property       integer $id
 * @property       string $timestamp
 * @property       string $parent_session_id
 * @property       string $session_id
 * @property       string $username
 * @property       string $group_name
 * @property       string $nas_ip_addr
 * @property       string $nas_identifier
 * @property       string $nas_port
 * @property       string $nas_port_type
 * @property       string $nas_port_id
 * @property       string $service_type
 * @property       string $framed_ip_addr
 * @property       string $calling_station_id
 * @property       string $called_station_id
 * @property       string $audit_session_id
 * @property       \Carbon\Carbon $start_time
 * @property       \Carbon\Carbon $end_time
 * @property       integer $guest_device_id
 * @property       string $guest_device_ip
 * @property       string $guest_device_mac
 * @property       string $access_code
 * @property       integer $tenant_id
 * @property       integer $venue_id
 * @property       integer $zone_id
 * @property       integer $device_id
 * @property       integer $duration
 * @property       integer $downloaded_data
 * @property       integer $uploaded_data
 * @property       integer $session_status
 * @property       integer $guest_id
 * @property       integer $service_profile_id
 * @property       integer $connection_status
 * @property       integer $login_type
 * @property       string $last_updated_time
 * @property-read  Guest $guest
 * @property-read  GuestDevice $guestDevice
 * @property-read  Device $device
 * @property-read  ServiceProfile $serviceProfile
 * @property-read  mixed $encrypted
 * @property-write mixed $encrypting
 * @property-write mixed $hashing
 * @method         static Builder|WifiSession whereId($value)
 * @method         static Builder|WifiSession whereTimestamp($value)
 * @method         static Builder|WifiSession whereSessionId($value)
 * @method         static Builder|WifiSession whereUsername($value)
 * @method         static Builder|WifiSession whereGroupName($value)
 * @method         static Builder|WifiSession whereNasIpAddr($value)
 * @method         static Builder|WifiSession whereNasIdentifier($value)
 * @method         static Builder|WifiSession whereNasPort($value)
 * @method         static Builder|WifiSession whereNasPortType($value)
 * @method         static Builder|WifiSession whereNasPortId($value)
 * @method         static Builder|WifiSession whereServiceType($value)
 * @method         static Builder|WifiSession whereFramedIpAddr($value)
 * @method         static Builder|WifiSession whereCallingStationId($value)
 * @method         static Builder|WifiSession whereCalledStationId($value)
 * @method         static Builder|WifiSession whereAuditSessionId($value)
 * @method         static Builder|WifiSession whereStartTime($value)
 * @method         static Builder|WifiSession whereEndTime($value)
 * @method         static Builder|WifiSession whereGuestDeviceId($value)
 * @method         static Builder|WifiSession whereDeviceId($value)
 * @method         static Builder|WifiSession whereDuration($value)
 * @method         static Builder|WifiSession whereDownloadedData($value)
 * @method         static Builder|WifiSession whereUploadedData($value)
 * @method         static Builder|WifiSession whereSessionStatus($value)
 * @method         static Builder|WifiSession whereGuestId($value)
 * @method         static Builder|WifiSession whereServiceProfileId($value)
 * @method         static Builder|WifiSession whereConnectionStatus($value)
 * @method         static Builder|WifiSession whereLastUpdatedTime($value)
 * @method         static Builder|Model without($relations)
 * @property       string $guest_device_logout_time
 * @property-read  mixed $total_duration
 * @property-read  string $device_name
 * @property-read  string $property_name
 * @property-read  string $downloaded_data_human_readable
 * @property-read  string $uploaded_data_human_readable
 * @property-read  string $connection_status_icon
 * @property-read  string $start_time_formatted
 * @property-read  string $mac
 * @property-read  string $is_online
 * @property-read  string $connection_status_value
 * @property-read  string $connection_status_class
 */
class WifiSession extends Model implements HasPresenter
{
    use BelongsToTenant;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_time', 'end_time'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OnlyParentSessionsScope);
    }

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return WifiSessionPresenter::class;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function guest()
    {
        return $this->hasOne(Guest::class, 'id', 'guest_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function guestDevice()
    {
        return $this->hasOne(GuestDevice::class, 'id', 'guest_device_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function serviceProfile()
    {
        return $this->hasOne(ServiceProfile::class, 'id', 'service_profile_id')->withTrashed();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int|null $location
     * @param bool $includeChildren
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByLocation($query, $location = null, $includeChildren = false)
    {
        if ($location) {
            $query->where('venue_id', $location);

            if ($includeChildren) {
                $query->orWhereIn(
                    'venue_id', function ($query) use ($location) {
                    $query->select('id')->from('properties')->where('parent_id', $location);
                }
                );
            }
        }

        return $query;
    }

}
