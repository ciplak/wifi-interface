<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailLog
 *
 * @property integer $id
 * @property string $to
 * @property string $cc
 * @property string $bcc
 * @property string $subject
 * @property string $body
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereId($value)
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereTo($value)
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereCc($value)
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereBcc($value)
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereSubject($value)
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereBody($value)
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereCreatedAt($value)
 * @method   static \Illuminate\Database\Query\Builder|EmailLog whereUpdatedAt($value)
 * @mixin    \Eloquent
 */
class EmailLog extends Model
{
    protected $guarded = ['id'];
}
