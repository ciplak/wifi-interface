<?php

namespace App\Models;

use App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DeviceVendor
 *
 * @property      int $id
 * @property      string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|DeviceModel[] $models
 * @method        static \Illuminate\Database\Query\Builder|DeviceVendor whereId($value)
 * @method        static \Illuminate\Database\Query\Builder|DeviceVendor whereName($value)
 * @mixin         \Eloquent
 */
class DeviceVendor extends Model
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get device models
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function models()
    {
        return $this->hasMany(DeviceModel::class, 'device_vendor_id');
    }
}
