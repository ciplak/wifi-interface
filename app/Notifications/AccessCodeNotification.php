<?php

namespace App\Notifications;

use App\Channels\Messages\SmsMessage;
use App\Channels\SmsChannel;
use App\Models\SmsProvider;
use App\Models\SplashPage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AccessCodeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var string
     */
    private $accessCode;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var SmsProvider
     */
    private $smsProvider;

    /**
     * @var SplashPage
     */
    private $loginPage;

    /**
     * @var bool
     */
    private $canSendSms;

    /**
     * @var string
     */
    private $smsContent;

    /**
     * Create a new notification instance.
     *
     * @param string      $accessCode
     * @param SplashPage  $loginPage
     * @param bool        $canSendSms
     * @param SmsProvider $smsProvider
     * @param string      $subject
     * @param string      $smsContent
     */
    public function __construct(string $accessCode, SplashPage $loginPage, bool $canSendSms, SmsProvider $smsProvider = null, string $subject = 'WiFi Access Code', string $smsContent)
    {
        $this->accessCode = $accessCode;
        $this->loginPage = $loginPage;
        $this->smsProvider = $smsProvider;
        $this->subject = $subject;
        $this->canSendSms = $canSendSms;
        $this->smsContent = $smsContent;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $via = [];

        if ($this->canSendSms && $this->smsProvider && $this->loginPage->send_sms) {
            $via[] = SmsChannel::class;
        }

        if ($this->loginPage->send_email) {
            $via[] = 'mail';
        }

        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->view('vendor.notifications.access_code')
            ->subject($this->subject)
            ->line('Thank you for visiting us.')
            ->line('Please enter access code below or click on "Validate my e-mail" link to access to the Internet')
            ->line(trans('login.access_code') . ": {$this->accessCode}")
            ->action('Validate my e-mail', url('/'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return SmsMessage
     */
    public function toSms($notifiable)
    {
        $smsContent = strtr($this->smsContent, [
            '[[code]]' => $this->accessCode,
            '[[url]]' => url('/')
        ]);

        return (new SmsMessage($this->smsProvider))
            ->content($smsContent);
    }
}
