/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

eval("var eventHub = new Vue();\n\nvar app = new Vue({\n    el: '#app',\n\n    /**\n     * Holds the timestamp for the last time we updated the API token.\n     */\n    lastRefreshedApiTokenAt: null,\n\n    data: {\n        loading: false\n    },\n\n    /**\n     * Prepare the application.\n     */\n    ready: function ready() {\n        console.log('Application Ready.');\n\n        this.whenReady();\n        this.listen();\n    },\n\n    /**\n     * The component has been created by Vue.\n     */\n    created: function created() {\n        //this.refreshApiTokenEveryFewMinutes();\n    },\n\n    methods: {\n        /**\n         * Finish bootstrapping the application.\n         */\n\n        whenReady: function whenReady() {\n            //\n        },\n\n        listen: function listen() {\n            if (App.echo.enabled) {\n                Echo.private('guest.' + App.user.tenantId)\n                    .listen('Guest.Registered', function (event) {\n                        toastr.info('A new guest has registered: ' + event.guest_name);\n                    });\n            }\n        },\n\n        /**\n         * Refresh the current API token every few minutes.\n         */\n        refreshApiTokenEveryFewMinutes: function refreshApiTokenEveryFewMinutes() {\n            var _this = this;\n\n            this.lastRefreshedApiTokenAt = moment();\n\n            setInterval(function () {\n                _this.refreshApiToken();\n            }, 240000);\n\n            setInterval(function () {\n                if (_this.lastRefreshedApiTokenAt.diff(moment(), 'minutes') >= 5) {\n                    _this.refreshApiToken();\n                }\n            }, 5000);\n        },\n\n        /**\n         * Refresh the current API token.\n         */\n        refreshApiToken: function refreshApiToken() {\n            this.lastRefreshedApiTokenAt = moment();\n\n            this.$http.put('token').then(function (response) {\n                console.log('API Token Refreshed.');\n            });\n        },\n\n        /*\n         * Get the current user of the application.\n         */\n        getUser: function getUser() {\n            this.$http.get('users/current').then(function (response) {\n                App.user = response.data;\n            });\n        },\n    }\n});//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL2JhY2tlbmQvdnVlLWFwcC5qcz81ZjlmIl0sInNvdXJjZXNDb250ZW50IjpbImNvbnN0IGV2ZW50SHViID0gbmV3IFZ1ZSgpO1xuXG5jb25zdCBhcHAgPSBuZXcgVnVlKHtcbiAgICBlbDogJyNhcHAnLFxuXG4gICAgLyoqXG4gICAgICogSG9sZHMgdGhlIHRpbWVzdGFtcCBmb3IgdGhlIGxhc3QgdGltZSB3ZSB1cGRhdGVkIHRoZSBBUEkgdG9rZW4uXG4gICAgICovXG4gICAgbGFzdFJlZnJlc2hlZEFwaVRva2VuQXQ6IG51bGwsXG5cbiAgICBkYXRhOiB7XG4gICAgICAgIGxvYWRpbmc6IGZhbHNlXG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFByZXBhcmUgdGhlIGFwcGxpY2F0aW9uLlxuICAgICAqL1xuICAgIHJlYWR5KCkge1xuICAgICAgICBjb25zb2xlLmxvZygnQXBwbGljYXRpb24gUmVhZHkuJyk7XG5cbiAgICAgICAgdGhpcy53aGVuUmVhZHkoKTtcbiAgICAgICAgdGhpcy5saXN0ZW4oKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogVGhlIGNvbXBvbmVudCBoYXMgYmVlbiBjcmVhdGVkIGJ5IFZ1ZS5cbiAgICAgKi9cbiAgICBjcmVhdGVkKCkge1xuICAgICAgICAvL3RoaXMucmVmcmVzaEFwaVRva2VuRXZlcnlGZXdNaW51dGVzKCk7XG4gICAgfSxcblxuICAgIG1ldGhvZHM6IHtcbiAgICAgICAgLyoqXG4gICAgICAgICAqIEZpbmlzaCBib290c3RyYXBwaW5nIHRoZSBhcHBsaWNhdGlvbi5cbiAgICAgICAgICovXG5cbiAgICAgICAgd2hlblJlYWR5KCkge1xuICAgICAgICAgICAgLy9cbiAgICAgICAgfSxcblxuICAgICAgICBsaXN0ZW4oKSB7XG4gICAgICAgICAgICBpZiAoQXBwLmVjaG8uZW5hYmxlZCkge1xuICAgICAgICAgICAgICAgIEVjaG8ucHJpdmF0ZSgnZ3Vlc3QuJyArIEFwcC51c2VyLnRlbmFudElkKVxuICAgICAgICAgICAgICAgICAgICAubGlzdGVuKCdHdWVzdC5SZWdpc3RlcmVkJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdHIuaW5mbygnQSBuZXcgZ3Vlc3QgaGFzIHJlZ2lzdGVyZWQ6ICcgKyBldmVudC5ndWVzdF9uYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFJlZnJlc2ggdGhlIGN1cnJlbnQgQVBJIHRva2VuIGV2ZXJ5IGZldyBtaW51dGVzLlxuICAgICAgICAgKi9cbiAgICAgICAgcmVmcmVzaEFwaVRva2VuRXZlcnlGZXdNaW51dGVzKCkge1xuICAgICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgICAgICAgICAgdGhpcy5sYXN0UmVmcmVzaGVkQXBpVG9rZW5BdCA9IG1vbWVudCgpO1xuXG4gICAgICAgICAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMucmVmcmVzaEFwaVRva2VuKCk7XG4gICAgICAgICAgICB9LCAyNDAwMDApO1xuXG4gICAgICAgICAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKF90aGlzLmxhc3RSZWZyZXNoZWRBcGlUb2tlbkF0LmRpZmYobW9tZW50KCksICdtaW51dGVzJykgPj0gNSkge1xuICAgICAgICAgICAgICAgICAgICBfdGhpcy5yZWZyZXNoQXBpVG9rZW4oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCA1MDAwKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVmcmVzaCB0aGUgY3VycmVudCBBUEkgdG9rZW4uXG4gICAgICAgICAqL1xuICAgICAgICByZWZyZXNoQXBpVG9rZW4oKSB7XG4gICAgICAgICAgICB0aGlzLmxhc3RSZWZyZXNoZWRBcGlUb2tlbkF0ID0gbW9tZW50KCk7XG5cbiAgICAgICAgICAgIHRoaXMuJGh0dHAucHV0KCd0b2tlbicpLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0FQSSBUb2tlbiBSZWZyZXNoZWQuJyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKlxuICAgICAgICAgKiBHZXQgdGhlIGN1cnJlbnQgdXNlciBvZiB0aGUgYXBwbGljYXRpb24uXG4gICAgICAgICAqL1xuICAgICAgICBnZXRVc2VyKCkge1xuICAgICAgICAgICAgdGhpcy4kaHR0cC5nZXQoJ3VzZXJzL2N1cnJlbnQnKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgIEFwcC51c2VyID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgIH1cbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyByZXNvdXJjZXMvYXNzZXRzL2pzL2JhY2tlbmQvdnVlLWFwcC5qcyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7O0FBRUE7QUFDQTtBQUNBOzs7OztBQUtBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9");

/***/ }
/******/ ]);