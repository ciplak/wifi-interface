@setup
require __DIR__.'/vendor/autoload.php';
(new \Dotenv\Dotenv(__DIR__, '.env'))->load();

$server = 'Server Name';
$servers = ['ubuntu@52.57.5.111 -i ~/.ssh/wifi-web.pem'];

$repository = "git@bitbucket.org:iperasolutions-wifi/wifi-interface.git";
$branch = "develop";

$baseDir = "/var/www/wifi";
$releasesDir = "{$baseDir}/releases";
$currentDir = "{$baseDir}/current";
$newReleaseName = date('Ymd-His');
$newReleaseDir = "{$releasesDir}/{$newReleaseName}";
$user = get_current_user();

function logMessage($message) {
return "echo '\033[32m" .$message. "\033[0m';\n";
}
@endsetup

@servers(['local' => '127.0.0.1', 'remote' => $servers])

@macro('deploy')
cloneRepository
runComposer
updateSymlinks
optimizeInstallation
updatePermissions
migrateDatabase
blessNewRelease
cleanOldReleases
finishDeploy
@endmacro

@task('cloneRepository', ['on' => 'remote'])
{{ logMessage("\u{1F300}  Cloning repository...") }}
[ -d {{ $releasesDir }} ] || sudo mkdir {{ $releasesDir }};
cd {{ $releasesDir }};

# Create the release dir
mkdir {{ $newReleaseDir }};

# Clone the repo
git clone -b {{ $branch }} --single-branch --depth 1 {{ $repository }} {{ $newReleaseName }}

# Configure sparse checkout
cd {{ $newReleaseDir }}
git config core.sparsecheckout true
echo "*" > .git/info/sparse-checkout
echo "!storage" >> .git/info/sparse-checkout
echo "!public/build" >> .git/info/sparse-checkout
git read-tree -mu HEAD

# Mark release
cd {{ $newReleaseDir }}
echo "{{ $newReleaseName }}" > public/release-name.txt
@endtask

@task('runComposer', ['on' => 'remote'])
{{ logMessage("\u{1F69A}  Running Composer...") }}
cd {{ $newReleaseDir }};
composer install --prefer-dist --no-scripts --no-dev -q -o;
@endtask

@task('updateSymlinks', ['on' => 'remote'])
{{ logMessage("\u{1F517} Updating symlinks to persistent data...") }}
# Remove the storage directory and replace with persistent data
rm -rf {{ $newReleaseDir }}/storage;
cd {{ $newReleaseDir }};
ln -nfs {{ $baseDir }}/storage storage;

# Import the environment config
cd {{ $newReleaseDir }};
ln -nfs {{ $baseDir }}/.env .env;
@endtask

@task('optimizeInstallation', ['on' => 'remote'])
{{ logMessage("\u{2728} Optimizing installation...") }}
cd {{ $newReleaseDir }};
php artisan clear-compiled;
php artisan optimize;
@endtask

@task('updatePermissions', ['on' => 'remote'])
{{ logMessage("\u{1F512}  Updating permissions...") }}
cd {{ $newReleaseDir }};
find . -type d -exec chmod 775 {} \;
find . -type f -exec chmod 664 {} \;
@endtask

@task('migrateDatabase', ['on' => 'remote'])
{{ logMessage("\u{1F648}  Migrating database...") }}
cd {{ $newReleaseDir }};
php artisan migrate --force;
@endtask

@task('blessNewRelease', ['on' => 'remote'])
{{ logMessage("\u{1F64F}  Blessing new release...") }}
ln -nfs {{ $newReleaseDir }} {{ $currentDir }};
cd {{ $newReleaseDir }}
php artisan cache:clear
sudo service php7.0-fpm restart
#sudo supervisorctl restart all
@endtask

@task('cleanOldReleases', ['on' => 'remote'])
{{ logMessage("\u{1F6BE}  Cleaning up old releases...") }}
# Delete all but the 5 most recent.
cd {{ $releasesDir }}
ls -dt {{ $releasesDir }}/* | tail -n +6 | xargs -d "\n" sudo chown -R ubuntu .;
ls -dt {{ $releasesDir }}/* | tail -n +6 | xargs -d "\n" rm -rf;
@endtask

@task('finishDeploy', ['on' => 'local'])
{{ logMessage("\u{1F680}  Application deployed!") }}
@endtask

@error
echo $task;
exit;
@enderror

@after
@slack(env('SLACK_DEPLOYMENT_WEBHOOK_URL'), '#deployments', "Deployment on {$server}: {$baseDir} {$newReleaseName} by {$user}: {$task} done")
@endafter